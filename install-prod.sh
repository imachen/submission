#!/bin/bash -e


umask 002

function test {
    echo "ERROR $1"
    exit 1
}
cd /webfs/klchu/repos/submission

mkdir -p /webfs/vhosts/img.jgi.doe.gov/cgi-bin/submit
mkdir -p /webfs/vhosts/img.jgi.doe.gov/htdocs/submit
cp -ruv cgi-bin/* /webfs/vhosts/img.jgi.doe.gov/cgi-bin/submit
cp -ruv htdocs/* /webfs/vhosts/img.jgi.doe.gov/htdocs/submit

cp -rv WebEnv-prod.pm  /webfs/vhosts/img.jgi.doe.gov/cgi-bin/submit/lib/WebEnv.pm
