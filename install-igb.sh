#!/bin/bash -e


umask 002

function test {
    echo "ERROR $1"
    exit 1
}
cd /webfs/klchu/repos/submission
mkdir -p /webfs/vhosts/img-dev.jgi.doe.gov/cgi-bin/submit
mkdir -p /webfs/vhosts/img-dev.jgi.doe.gov/htdocs/submit
cp -ruv cgi-bin/* /webfs/vhosts/img-dev.jgi.doe.gov/cgi-bin/submit

cp -ruv htdocs/* /webfs/vhosts/img-dev.jgi.doe.gov/htdocs/submit
 