var mySelects=["selectedGenome1",
               "selectedGenome2",
               "selectedGenome3"];
var errorMsg1=["Please select at least one Find In genome", 
               "Please select at least one With Query Genomes genome",
               "Please select at least one Without Query Genomes genome"];

/**
 * Add button action
 * @param id
 * @param maxSelect - max number of genomes that can be added value of -1 it will ignore the max check
 *                    to be used for selection selectedGenome2 and selectedGenome3, selectedGenome1 is always max of 1
 * @param domainType - restrict add to domainType: '' (means all domains default), isolate, metagenome
 * @returns {Boolean}
 */
function addOption(id, maxSelect, domainType) {
    // checks
    /*
    if (id == 'selectedGenome1') {
        // we can only select one genome
        var x = document.getElementById(id);
        if (x != undefined && x != null) {
            var cnt = x.options.length;
            if (cnt > 0) {
                alert('You cannot select more than one genome.');
                return;
            }
        }
    }
    */
    
    var count = 0;
    if (maxSelect != undefined && maxSelect != null && 
	maxSelect != '' && maxSelect > 0) {

        var x = document.getElementById(id);
        if (x != undefined && x != null) {
            var cnt = x.options.length;
            count = cnt;
            if (cnt >= maxSelect) {
                alert('You cannot select more than ' + maxSelect + ' genome.');
                return;
            }
        }
    }
    
    var errorMsg = '';
    var f = document.getElementById('displayType2').checked;
    if (f) {
        // tree
        if (tree1 === undefined || tree1 == null) {
            alert('Please select a genome');
            return true;
        }        

        var hiLit = tree1.getNodesByProperty('highlightState', 1);
        if (YAHOO.lang.isNull(hiLit)) {
            alert('Please select a genome');
            return true;
        } else {
            var cnt = 0;
            for ( var i = 0; i < hiLit.length; i++) {
                if (hiLit[i].children == '') {
                    // leaf node
                    var taxonOid = hiLit[i].labelElId
                    var taxonName = hiLit[i].label;
                    var boo = false;
                    for (var j=0; j<mySelects.length; j++ ) {
                        boo = boo || optionExists(mySelects[j], taxonOid);
                        if (boo == true) {
                            errorMsg = errorMsg + taxonName + '\n';
                            break;
                        }                        
                    }    
                    if (boo == false) {
                        //if (cnt >= 1 && id == 'selectedGenome1') {
			//alert('You can only select one genome. Your first selection was added.');
			//break;
                        //}
                        
                        if (maxSelect != undefined && maxSelect != null && 
			    maxSelect != '' && maxSelect > 0 && 
			    count >= maxSelect) {

                            alert('You can only select ' + maxSelect + ' genomes.');
                            break;                        
                        }
                        
                        // create new obejct
                        var x = document.getElementById(id);
                        var option = document.createElement("option");
                        option.text= taxonName; // add to
                        option.value= taxonOid;
                        option.title= option.text;
                        if (isDomainType(domainType, taxonName)) {
                            try {
                                // for IE earlier than version 8
                                x.add(option,x.options[null]);
                            } catch (e) {
                                x.add(option,null);
                            }
                            cnt++;
                            count++;
                        } else {
                            alert('You can only add ' + domainType);
                        }
                    }                        
                }
            }
        }
    } else {
        var select1 = document.getElementById('tax');
        var cnt = 0;
        for (var i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) {
                var v = select1.options[i].value;
                var boo = false;
                for (var j=0; j<mySelects.length; j++ ) {
                // check if genome already selected
                // any trues we'll reject the add
                // it will still add those genomes not added already if
                // multi-select included previously added genomes
                    boo = boo || optionExists(mySelects[j], v);
                    if (boo == true) {
                        errorMsg = errorMsg + select1.options[i].text + '\n';
                        break;
                    }
                }
            
                if (boo == false) {
                    //if (cnt >= 1 && id == 'selectedGenome1') {
		    //alert('You can only select one genome. Your first selection was added.');
		    //break;
                    //}
		    
                    if (maxSelect != undefined && maxSelect != null && 
			maxSelect != '' && maxSelect > 0 && 
			count >= maxSelect) {

                        alert('You can only select ' + maxSelect + ' genomes.');
                        break;                        
                    }
                    
                    // create new obejct
                    var x = document.getElementById(id);
                    var option = document.createElement("option");
                    option.text = select1.options[i].text; // add to
                    option.value = select1.options[i].value; // add to
                    option.title = option.text;
                    
                    if (isDomainType(domainType, select1.options[i].text)) {
                        try {
                            // for IE earlier than version 8
                            x.add(option,x.options[null]);
                        } catch (e) {
                            x.add(option,null);
                        }
                        cnt++;
                        count++;
                    } else {
                        alert('You can only add ' + domainType);
                    }
                }
            }
        }
    }
    
    if (errorMsg != '') {
        alert("Genome(s) already added:\n" + errorMsg);
    }

    // update the count
    updateSelectedGenomeXCount(id);
}

/**
 * check the domain type
 * @param domainType
 * @param text
 */
function isDomainType(domainType, text) {
    if (domainType == null || domainType == '') {
        return true;
    }
    
    var n;
    if (domainType == 'isolate') {
        // (A), (B), (E), (G), (P), (V)
        var types = ['(A)', '(B)', '(E)', '(G)', '(P)', '(V)'];
        for (var i=0;i<types.length;i++) {
            n = text.indexOf(types[i]);
            if (n > -1) {
                break;
            }
        }
    } else {
        // domainType == 'metagenome'
        n = text.indexOf('(*)');
    }
    
    if (n > -1) {
        return true;
    } else {
        return false;
    }
}


/**
 * update selected genome section 2 or 3 count
 * @param id
 */
function updateSelectedGenomeXCount(id) {
    if (id == 'selectedGenome2' || id == 'selectedGenome3') {
        var obj = document.getElementById(id + 'Counter');
        if (obj != undefined && obj != null) {
            var selectX = document.getElementById(id);
            obj.innerHTML = selectX.length;
        }
    }    
}

function optionExists(id, opt) {
    var x = document.getElementById(id);
    if (x == null) {
        return false;
    }
    for (var i = 0; i < x.options.length; i++) { 
        var v = x.options[i].value;
        if (opt == v ) {
            return true;
        }
    } 
    return false;
}

/*
 * to remove options object start with the last one and work our way up to
 * remove objects
 */
function removeOption(id) {
    var x = document.getElementById(id);
    var removeCnt = 0;
    for (var i = x.length - 1; i>=0; i--) {
        if (x.options[i].selected) {
          x.remove(i);
          removeCnt++;
        }
     }
    
    // update the count
    updateSelectedGenomeXCount(id);
    
    if (removeCnt == 0) {
        alert("Please select genome(s) to be removed");
    }
}

/*
 * select all the genomes before the form is submitted
 */
function selectAllOption() {
    for (var j=0; j<mySelects.length; j++ ) {
        var selectBox = document.getElementById(mySelects[j]);
        if (selectBox == null) {
            continue;
        }
        if (selectBox.options.length < 1) {
            // alert(errorMsg1[j]);
            // return false;
            continue;
        } else if (selectBox.options.length == 1) {
            selectBox.options[0].selected = true;
            continue;
        } else if (selectBox.type == "select-multiple") { 
            for (var i = 0; i < selectBox.options.length; i++) { 
                 selectBox.options[i].selected = true; 
            } 
        }    
    }    
    return true;
}

function resetForm() {
    for (var j=0; j<mySelects.length; j++ ) {
        var selectBox = document.getElementById(mySelects[j]);
        if (selectBox == null) {
            continue;
        }        
        if (selectBox.options != null) {
            selectBox.options.length = 0;  // That's it!
        }   
    }
}

function mySubmitWithCheck(section, page, id1, minSelect1, id2, minSelect2) {
    if (minSelect1 == undefined || minSelect1 == null || minSelect1 == '') {
	minSelect1 = 0;
    }
    if (minSelect2 == undefined || minSelect2 == null || minSelect2 == '') {
	minSelect2 = 0;
    }

    if (minSelect1 > 0) {
        var x = document.getElementById(id1);
        if (x != undefined && x != null) {
            var cnt = x.options.length;
            if (cnt < minSelect1) {
                alert('Please select at least ' + minSelect1 + ' query genome(s).');
                return;
            }
        }
    }
    if (minSelect2 > 0) {
        var x = document.getElementById(id2);
        if (x != undefined && x != null) {
            var cnt = x.options.length;
            if (cnt < minSelect2) {
                alert('Please select at least ' + minSelect2 + ' reference genome(s).');
                return;
            }
        }
    }

    mySubmitJsonXDiv(section, page);
}

function mySubmitJsonXDiv(section, page) {
    selectAllOption();
    if (section != '') {
        document.mainForm.section.value = section;
    }
    if (page != '') {
        document.mainForm.page.value = page;
	document.getElementById("page").value = page;
    }
    document.mainForm.submit();    
}
