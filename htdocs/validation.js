
/*
 * validateOnDubmit for Artemis Form
 */
function validateOnSubmitForArtemis(switchLimit, maxLimit) {
	var selectedCount = 0
	
    var formObj = document.mainForm;
    var selectObj = formObj.elements['scaffold_oid'];
    if(selectObj.options.length > 1 && selectObj.options[0].selected) { 
        selectedCount = selectObj.options.length - 1;
    }
    else {
        for(j = 0; j < selectObj.options.length; j++) { 
            if(selectObj.options[j].selected) { 
                selectedCount++;
            }
        }
    }
    
    if (selectedCount == 0) {
        window.alert("Please select at least one scaffold.");
        return false;
    }
    if (selectedCount > maxLimit) {
        window.alert("Please select at most " + maxLimit + " scaffolds.");
        return false;
    }
    if (selectedCount > switchLimit) {
    	var emailStr = formObj.elements['myEmail'].value;
    	if (emailStr == null || emailStr.length < 1) {
            window.alert("Please enter your email since you have selected over " + switchLimit + " entries.");
            return false;
    	}
        if ( !(testEmail(emailStr)) ) {
        	window.alert("Please enter a valid email address.");
            return false;
       }
    }
    return true;
}

function testEmail(emailStr) {
	//var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
	//var emailRegEx = /^[a-zA-Z]([a-zA-Z0-9-_])*(\.[a-zA-Z0-9-_])*@[a-zA-Z0-9](([a-zA-Z0-9\-\_\.])*)+\.[a-zA-Z]{2,4}$/;
    //var emailRegEx = /^[\\w-_\.+]*[\\w-_\.]\@([\\w]+\\.)+[\\w]+[\\w]$/i;
	//var emailRegEx = /^((\w+\+*\-*)+\.?)+@((\w+\+*\-*)+\.?)*[\w-]+\.[a-z]{2,6}$/i;
	var emailRegEx = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    return emailRegEx.test(emailStr);
}
