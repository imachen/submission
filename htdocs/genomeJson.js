// store the json object of genome list
var taxonsList;
// yui root tree node
var tree1;
/**
 * Sort by taxon name
 * 
 * @param a
 * @param b
 * @returns {Number}
 */
function Comparator(a, b) {
    if (a[1] < b[1])
        return -1;
    if (a[1] > b[1])
        return 1;
    return 0;
}
/**
 * Genome list selection update the number of user selected genomes
 * 
 * @returns {Boolean}
 */
function genomeCountSelected() {
    var f = document.getElementById('displayType2').checked;
    if (f) {
        // tree
        //alert('here in tree');
    } else {
        var el = document.mainForm.genomeFilterSelections;
        if (el == null) {
            el = document.mainForm.referenceGenomes;
        }
        if (el == null) {
            el = document.mainForm.genomesToSelect;
        }
        if (el == null) {
            el = document.mainForm.queryGenomes;
        }
        if (el == null) {
            el = document.mainForm.imgBlastDb;
        }
        if (el == null) {
            return true;
        }
    }
    var oList = document.getElementById("genomeListCounter");
    if (oList == null) {
        return true;
    }
    var cnt = getSelectedGenomeCount();
    if (cnt > 0) {
        oList.innerHTML = "Selected: " + cnt;
    } else {
        oList.innerHTML = "";
    }
}
/**
 * gets number of selected genomes
 */
function getSelectedGenomeCount() {
    var cnt = 0;
    var f = document.getElementById('displayType2').checked;
    if (f) {
        // tree
        if (tree1 === undefined || tree1 == null) {
            return 0;
        }
        var hiLit = tree1.getNodesByProperty('highlightState', 1);
        if (YAHOO.lang.isNull(hiLit)) {
            return 0;
        } else {
            for ( var i = 0; i < hiLit.length; i++) {
                if (hiLit[i].children == '') {
                    // leaf node
                    cnt++;
                }
            }
        }
    } else {
        var el = document.mainForm.genomeFilterSelections;
        if (el == null || el === undefined) {
            return 0;
        }
        for ( var i = 0; i < el.options.length; i++) {
            if (el.options[i].selected) {
                cnt++;
            }
        }
    }
    return cnt;
}
/**
 * Custom form submit. For tree view find all users genomes and create hidden
 * inputs in div hiddenlist
 * 
 * @param section
 * @param page
 * @returns {Boolean}
 */
function mySubmitJson(section, page) {
    if (section != '') {
        document.mainForm.section.value = section;
    }
    if (page != '') {
        document.mainForm.page.value = page;
        var paramMatchJson = document.getElementById("paramMatchJson");
        paramMatchJson.value = page;
        paramMatchJson.name = page;
    }
    var f = document.getElementById('displayType2').checked;
    if (f) {
        // tree
        if (tree1 === undefined || tree1 == null) {
            alert('Please select a genome');
            return true;
        }
        var hiLit = tree1.getNodesByProperty('highlightState', 1);
        var hiddenDiv = document.getElementById("hiddenlist");
        if (YAHOO.lang.isNull(hiLit)) {
            alert('Please select a genome');
            return true;
        } else {
            var str = ''; // define it s.t. the undefined is not printed in
            // the ui
            for ( var i = 0; i < hiLit.length; i++) {
                if (hiLit[i].children == '') {
                    // leaf node
                    str += "<input type='hidden' name='genomeFilterSelections' value='"
                            + hiLit[i].labelElId + "'> ";
                }
            }
            hiddenDiv.innerHTML = str;
        }
    } else {
        // list
        var cnt = getSelectedGenomeCount();
        if (cnt == 0) {
            alert('Please select a genome');
            return true;
        }
    }
    document.mainForm.submit();
}
/**
 * User presses the "Show" button
 * 
 * @param baseUrl
 * @param prefix
 *            "t:" or "b:" without quotes
 * @param from - which form the call can from optional
 * @returns {Boolean}
 */
function printSelectType(xmlCgiUrl, prefix, style, from) {
    // reset user selected count
    var oList = document.getElementById("genomeListCounter");
    oList.innerHTML = "";
    var e = document.getElementById("seqstatus");
    var seqstatus = e.options[e.selectedIndex].value;

    var f = document.getElementById('displayType2').checked;
    var dt = 'list';
    if (f) {
        dt = 'tree';
    } else {
        if (tree1 != null) {
            tree1.destroy();
        }
        tree1 = null;
    }

    // find domain type user selected and load the correct json file
    var e = document.getElementById("domainfilter");
    var file = e.options[e.selectedIndex].value;
    var url = xmlCgiUrl + "?section=GenomeListJSON&page=json&displayType=" + dt
            + "&domainfilter=" + file + "&seqstatus=" + seqstatus;

    if (from != '' && from != null) {
        url = url + "&from=" + from;
    }
    document.getElementById("showButton").style.cursor = "wait";
    //document.body.style.cursor = "wait";

    var callbacks = {
        success : function(o) {
            try {
                taxonsList = YAHOO.lang.JSON.parse(o.responseText);
                if (f) {
                    printTree(prefix);
                } else {
                    printSelect(prefix, style);
                }
                cartSelectAll(file, f);
                document.getElementById("showButton").style.cursor = "default";
		//document.body.style.cursor = "default";
            } catch (x) {
                alert("JSON Parse failed! " + x);
                alert(o.responseText);
                document.getElementById("showButton").style.cursor = "default";
                //document.body.style.cursor = "default";
                return;
            }
        },
        failure : function(o) {
            if (!YAHOO.util.Connect.isCallInProgress(o)) {
                alert("Async call failed!");
                alert(o.responseText);
            }
            document.getElementById("showButton").style.cursor = "default";
	    //document.body.style.cursor = "default";
        },
        timeout : 60000
    }
    // Make the call to the server for JSON data
    YAHOO.util.Connect.asyncRequest('GET', url, callbacks);
}

function cartSelectAll(file, listOrTree) {
    if (file == 'cart') {
        var selectBox = document.getElementById('selectedGenome1');
        if (selectBox !== undefined && selectBox != null) {
	    // this is XDiv form
	    return;
        } 	
    	
    	if (listOrTree) {
	    // tree
	    //treeCheckAll();
    	} else {
	    // list - 'genomeFilterSelections'
            var selectBox = document.getElementById('tax');
            if (selectBox == null) {
                return;
            }
            if (selectBox.options.length < 1) {
                return;
            } else { 
                for (var i = 0; i < selectBox.options.length; i++) { 
                     selectBox.options[i].selected = true;
                } 
            }
    	}
    	genomeCountSelected();
    }
}

/**
 * print genome list view
 */
function printSelect(prefix, style) {
    if (style === undefined || style == null || style == '') {
        style = 'resize: horizontal; width: 600px; overflow:scroll;';
    }
    var genomeFilterSelections = 'genomeFilterSelections';
    var s = document.getElementById("domainfilter");
    var d = s.options[s.selectedIndex].value;
    var text = "<select id='tax' size='10'  style='" + style  + "' name='"
	+ genomeFilterSelections
	+ "' onchange='return genomeCountSelected();'  multiple='multiple'>";
    var cnt = 0;
    // sort by taxon name if d != All
    // if (d != 'All') {
    // taxonsList = taxonsList.sort(Comparator);
    // }
    for ( var i = 0; i < taxonsList.length; i++) {
        var taxon_oid = prefix + taxonsList[i][0];
        var taxon_name = taxonsList[i][1];
        var domain = taxonsList[i][2];
        var phylum = taxonsList[i][3];
        var ir_class = taxonsList[i][4];
        var ir_order = taxonsList[i][5];
        var family = taxonsList[i][6];
        var genus = taxonsList[i][7];
        var species = taxonsList[i][8];
        var seq_status = taxonsList[i][9]; // Draft, Finished, Permanent Draft
        var sub = domain.substring(0, 1);
        var sub2 = seq_status.substr(0, 1);
        cnt++;
        var title = taxon_name + ' (' + sub + ') [' + sub2 + ']';
        text += "<option value='" + taxon_oid + "' title='" + title + "'>";
        text += taxon_name + ' (' + sub + ') [' + sub2 + ']';
        text += "</option>";
    }
    text += "</select>";
    var g = document.getElementById("genomelist");
    g.innerHTML = text;
    document.getElementById("treeDiv1").innerHTML = "";
}
/**
 * expand tree
 */
function myExpandAll() {
    document.getElementById("expandAll").style.cursor = "wait";
    document.body.style.cursor = "wait";
    tree1.expandAll();
    document.getElementById("expandAll").style.cursor = "default";
    document.body.style.cursor = "default";
}
/**
 * collapse tree
 */
function myCollapseAll() {
    document.getElementById("collapseAll").style.cursor = "wait";
    document.body.style.cursor = "wait";
    tree1.collapseAll();
    document.getElementById("collapseAll").style.cursor = "default";
    document.body.style.cursor = "default";
}
/**
 * print tree view
 */
function printTree(prefix) {
    var g = document.getElementById("genomelist");
    var str = "<br/> <input id='expandAll' type='button' value='Expand All' onclick='myExpandAll();'>";
    str += "&nbsp;&nbsp;";
    str += "<input id='collapseAll' type='button' value='Collapse All' onclick='myCollapseAll();'>";
    g.innerHTML = str;
    tree1 = new YAHOO.widget.TreeView("treeDiv1");
    var parent = tree1.getRoot();
    var cnt = 0;
    for ( var i = 0; i < taxonsList.length; i++) {
        var taxon_oid = prefix + taxonsList[i][0];
        var taxon_name = taxonsList[i][1];
        var domain = taxonsList[i][2];
        var phylum = taxonsList[i][3];
        var ir_class = taxonsList[i][4];
        var ir_order = taxonsList[i][5];
        var family = taxonsList[i][6];
        var genus = taxonsList[i][7];
        var species = taxonsList[i][8];
        var seq_status = taxonsList[i][9]; // Draft, Finished, Permanent Draft
        var sub = domain.substring(0, 1);
        var sub2 = seq_status.substr(0, 1)
        var lastdomain;
        var lastphylum;
        var lastir_class;
        var lastir_order;
        var lastfamily;
        var lastgenus;
        var lastspecies;
        var tmpNodeDomain;
        var tmpNodePhylum;
        var tmpNodeIr_class;
        var tmpNodeIr_order;
        var tmpNodeFamily;
        var tmpNodeGenus;
        cnt++;
        if (domain != lastdomain) {
            // new domain
            tmpNodeDomain = new YAHOO.widget.TextNode(domain, parent);
            //tmpNodeDomain = new YAHOO.widget.TaskNode(domain, parent, false);
            lastdomain = domain;
            tmpNodePhylum = '';
            tmpNodeIr_class = '';
            tmpNodeIr_order = '';
            tmpNodeFamily = '';
            lastphylum = '';
            lastir_class = '';
            lastir_order = '';
            lastfamily = '';
            lastgenus = '';
            lastspecies = '';
        }
        if (phylum != lastphylum) {
            tmpNodePhylum = new YAHOO.widget.TextNode(phylum, tmpNodeDomain);
            //tmpNodeDomain = new YAHOO.widget.TaskNode(phylum, tmpNodeDomain, false);
            lastphylum = phylum;
            tmpNodeIr_class = '';
            tmpNodeIr_order = '';
            tmpNodeFamily = '';
            lastir_class = '';
            lastir_order = '';
            lastfamily = '';
            lastgenus = '';
            lastspecies = '';
        }
        if (ir_class != lastir_class) {
            tmpNodeIr_class = new YAHOO.widget.TextNode(ir_class, tmpNodePhylum);
            //tmpNodeIr_class = new YAHOO.widget.TaskNode(ir_class, tmpNodePhylum, false);
            lastir_class = ir_class;
            tmpNodeIr_order = '';
            tmpNodeFamily = '';
            lastir_order = '';
            lastfamily = '';
            lastgenus = '';
            lastspecies = '';
        }
        if (ir_order != lastir_order) {
            tmpNodeIr_order = new YAHOO.widget.TextNode(ir_order, tmpNodeIr_class);
            //tmpNodeIr_order = new YAHOO.widget.TaskNode(ir_order, tmpNodeIr_class, false);
            lastir_order = ir_order;
            tmpNodeFamily = '';
            lastfamily = '';
            lastgenus = '';
            lastspecies = '';
        }
        if (family != lastfamily) {
            tmpNodeFamily = new YAHOO.widget.TextNode(family, tmpNodeIr_order);
            //tmpNodeFamily = new YAHOO.widget.TaskNode(family, tmpNodeIr_order, false);
            lastfamily = family;
            lastgenus = '';
            lastspecies = '';
        }
        if (lastgenus != genus) {
            tmpNodeGenus = new YAHOO.widget.TextNode(genus, tmpNodeFamily);
            //tmpNodeGenus = new YAHOO.widget.TaskNode(genus, tmpNodeFamily, false);
            lastgenus = genus;
            lastspecies = species;
        }
        var tmpNodeTaxon = new YAHOO.widget.TextNode(taxon_name + ' (' + sub + ') [' + sub2 + ']', tmpNodeGenus);
        //var tmpNodeTaxon = new YAHOO.widget.TaskNode(taxon_name + ' (' + sub + ') [' + sub2 + ']', tmpNodeGenus, false);
        var title = domain + ' : ' + seq_status;
        tmpNodeTaxon.labelElId = taxon_oid;
        tmpNodeTaxon.title = title;
    }
    tree1.setNodesProperty('propagateHighlightUp', true);
    tree1.setNodesProperty('propagateHighlightDown', true);
    tree1.subscribe('clickEvent', tree1.onEventToggleHighlight);
    // listener to display the number of user selected genomes
    tree1.subscribe('highlightEvent', function(node) {
        genomeCountSelected();
        });
    tree1.render();
    //tree.draw(); // For TaskNode.js
}

/*
 * change the selection box to another value
 */
function selectItemByValue(elmnt, value) {
    for (var i=0; i < elmnt.options.length; i++) {
	if (elmnt.options[i].value == value)
	    elmnt.selectedIndex = i;
    }
  }


