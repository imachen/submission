// document element start location of Find radio button
var findButton = 3;
// document element start location of Collocated radio button
var collButton = 4;
var notCollButton = 5;

// max number a user can select
var maxFind = 1;
var maxColl = 999;

// number of radio button cols
var numOfCols = 4;

/*
 * When user selects a radio button highlight in blue, 'a parent taxon' not a
 * child / leaf taxon param begin item number offest by findButton param end last
 * radio button param offset which column param type which column type
 */
function selectGroupProfile(begin, end, offset, type) {
	var f = document.mainForm;
	var count = 0;
	var idx1 = begin * numOfCols;
	var idx2 = end * numOfCols;
	for ( var i = idx1; i < f.length && i < idx2; i++) {
		var e = f.elements[i + findButton];
		if (e.type == "radio" && i % numOfCols == offset) {
			e.checked = true;
		}
	}

	/*
	 * now count the number of leafs selected max is 10
	 */
	if (type == 'find' && !checkFindCount(null)) {
		selectGroupProfile(begin, end, (numOfCols - 1), 'ignore');
	} else if (type == 'collocated' && !checkCollCount(null)) {
		selectGroupProfile(begin, end, (numOfCols - 1), 'ignore');
	} else if (type == 'notCollocated' && !checkNotCollCount(null)) {
		selectGroupProfile(begin, end, (numOfCols - 1), 'ignore');
	}	
}

/*
 */
function checkFindCount(obj) {
	var f = document.mainForm;
	var count = 0;

	// I KNOW where the objects are located in the form
	for ( var i = findButton; i < f.length; i = i + numOfCols) {
		var e = f.elements[i];
		var name = e.name;
		if (e.type == "radio" && e.checked == true
				&& name.indexOf("profile") > -1) {
			// alert("radio button is checked " + name);
			count++;
			if (count > maxFind) {
				alert("Please select only " + maxFind + " genome");
				if (obj != null) {
					// i know which taxon leaf to un-check
					obj[0].checked = false;
					obj[numOfCols - 1].checked = true;
				}
				return false;
			}
		}
	}
	return true;
}

/*
 * 
 */
function checkCollCount(obj) {
	var f = document.mainForm;
	var count = 0;

	// I KNOW where the objects are located in the form
	for ( var i = collButton; i < f.length; i = i + numOfCols) {
		var e = f.elements[i];
		var name = e.name;
		if (e.type == "radio" && e.checked == true
				&& name.indexOf("profile") > -1) {
			// alert("radio button is checked " + name);
			count++;
			if (count > maxColl) {
				alert("Please select " + maxColl + " or less genomes");
				if (obj != null) {
					obj[1].checked = false;
					obj[numOfCols - 1].checked = true;
				}
				return false;
			}
		}
	}
	return true;
}

function checkNotCollCount(obj) {
	var f = document.mainForm;
	var count = 0;

	// I KNOW where the objects are located in the form
	for ( var i = notCollButton; i < f.length; i = i + numOfCols) {
		var e = f.elements[i];
		var name = e.name;
		if (e.type == "radio" && e.checked == true
				&& name.indexOf("profile") > -1) {
			// alert("radio button is checked " + name);
			count++;
			if (count > maxColl) {
				alert("Please select " + maxColl + " or less genomes");
				if (obj != null) {
					obj[3].checked = false;
					obj[numOfCols - 1].checked = true;
				}
				return false;
			}
		}
	}
	return true;
}
