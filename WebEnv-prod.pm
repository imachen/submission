# Put configuration parameters here.
#
#
package WebEnv;
require Exporter;
@ISA = qw( Exporter );
@EXPORT = qw( getEnv );

use JSON;

# on img-edge*
#
# /opt/img/temp
# and
# /opt/img/logs
# are speical locations where the img ui can write to.
# - ken


##
# getEnv - Get environment
#     Customize this subroutine.
#
$ENV{ORACLE_HOME}     = "/app/Oracle";

$ENV{TNS_ADMIN}       = "/webfs/img_rdbms/oracle/network/admin";

sub getEnv {
    my $e = { };

    # Companion systems url
    $e->{ domain_name } = "img.jgi.doe.gov";
    $e->{ http } = "https://"; 
    $e->{ top_base_url } = $e->{ http } . $e->{ domain_name } . "/"; 

    my $urlTag = "submit"; 
    my $dbTag = "img_core_v400"; # database config file to use

    $e->{ base_url } = $e->{ http } . $e->{ domain_name } . "/$urlTag";
    $e->{ arch } = "x86_64-linux";

    $e->{ base_cgi_url } = $e->{ top_base_url } . "/cgi-bin/$urlTag";
    
    #
    # DO NOT update base_dir base_dir_stage cgi_url cgi_dir cgi_dir_stage
    # - ken
    #
    my $webfsVhostDir = '/webfs/vhosts/'; 
    my $apacheVhostDir = '/webfs/vhosts/';
    $e->{ top_base_dir } = $apacheVhostDir . $e->{ domain_name } . "/htdocs/";
    $e->{ base_dir } = $apacheVhostDir . $e->{ domain_name } . "/htdocs/$urlTag";
    $e->{ base_dir_stage } = $webfsVhostDir . $e->{ domain_name } . "/htdocs/$urlTag";

    $e->{ cgi_url } = $e->{ http } . $e->{ domain_name } . "/cgi-bin/$urlTag";
    $e->{ cgi_dir } = $apacheVhostDir . $e->{ domain_name } . "/cgi-bin/$urlTag";
    $e->{ cgi_dir_stage } = $webfsVhostDir . $e->{ domain_name } . "/cgi-bin/$urlTag";

    $e->{ main_cgi } = "main.cgi";
    $e->{ inner_cgi } = "inner.cgi";
    
##    my $log_dir = "/opt/img/logs";
    my $log_dir = "/app/log";

    $e->{ log_dir } = $log_dir;
    $e->{ web_log_file } = "$log_dir/" . $dbTag . "_" . $urlTag . ".log";
    $e->{ err_log_file } = "$log_dir/" . $dbTag . "_" . $urlTag . ".err.log";

    $e->{ cgi_tmp_dir } = "/app/tmp/" . $e->{ domain_name } .  "_"  . $urlTag;

                # Common tmp directory between Web UI and service.
                # For passing report files rather than funneling them
                # through http, which can be very slow.
    $e->{common_tmp_dir} = "/webfs/scratch/wwwimg_tmp";
                
    $e->{ upload_dir } = "/webfs/img_web_submissions/";
    $e->{ storage_dir } = "/global/dna/projectdirs/microbial/img_web_submissions/";
    $e->{ copy_sync_url };# = "http://jgi-img-int-1.nersc.gov:9080/cgi-bin/runCmd/syncSubmissionFile.cgi?file=";

    $e->{ max_file_size } = 1000000000000; 

    $e->{ user_guide_url } = $e->{ base_url } . "/doc/IMGSubmissionUserGuide.pdf"; 
 
    $e->{ ncbi_eutils_url } = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?";


    $e->{oracle_config_dir} = "/webfs/img_rdbms/config/";

    $e->{ oracle_config } = $e->{ oracle_config_dir } . "web.imgsg_dev.config";
    $e->{ img_er_oracle_config } = $e->{ oracle_config_dir } . "web.img_core_v400.config"; 
    $e->{ img_gold_oracle_config } = $e->{ oracle_config_dir } . "web.imgsg_dev.config"; 
    $e->{ img_i_taxon_oracle_config } = $e->{ oracle_config_dir } . "oracle.img_i_taxon.config"; 
    $e->{ img_ext_oracle_config } = $e->{ oracle_config_dir } . "oracle.img_ext.config";

    # ---------------------------------------------
    # 
    # Google
    #
    # ---------------------------------------------
    ## Google Map Keys
    my %googleMapKeys = (
        'jgi-psf.org' => 'ABQIAAAAYxJnF8y-nhPss9h0Mp_SmBRudsuYU0E-IvTrpz0eeItbCuKkEBQYbqxO7d1kC-KDKb1JTl78SHX6Cg',
	    'jgi.doe.gov' => 'ABQIAAAAYxJnF8y-nhPss9h0Mp_SmBTg30WwySNRyLvcO7oFZfs2Agm6xRTuB05OZiGo4MATGpJmYVYP4vxVHQ',
	    'hmpdacc-resources.org' => 'ABQIAAAAoyF356P0_F8RJgEsRg73bBSvpfy4IrTf2S0Jpw1e1K8i8S6yrxSlI4YBFXgRBnDNPn81Jl5m4_Ki5w'
    );
    $e->{google_map_keys} = \%googleMapKeys;
    
    # Google Analytics
    #
    $e->{enable_google_analytics} = 1;
    my %googleAnalyticsKeys = (
        'jgi-psf.org' => 'UA-15689341-3',
        'jgi.doe.gov' => 'UA-15689341-4',
        'hmpdacc-resources.org' => 'UA-15689341-5',
    );
    $e->{google_analytics_keys} = \%googleAnalyticsKeys;
    
    # Google Recaptcha
    #
    # http://www.google.com/recaptcha
    # http://code.google.com/apis/recaptcha/intro.html
    my %recaptcha_private_key = (
        'jgi-psf.org' => '6LfPjr4SAAAAAG6P8xA9VK0RTKgmAGrfk9A2c4i5',
        'jgi.doe.gov' => '6LfQjr4SAAAAAI18Ydpv5Xb9Z-QOPl9x1MUc14Yl',
        'hmpdacc-resources.org' => '6LfRjr4SAAAAACHJ9oF3IAzpsQI69AJxbwJXiiyz',    
    );
    my %recaptcha_public_key = (
        'jgi-psf.org' => '6LfPjr4SAAAAAEcnSNPTE-GtiPYTdUqkYK07BzYC',
        'jgi.doe.gov' => '6LfQjr4SAAAAAHKn-Yd-QR55idf73Pnnxt6avsBh',
        'hmpdacc-resources.org' => '6LfRjr4SAAAAAD1o2hZI5nbSPydG00pb1b7Mb92S',    
    );
    $e->{google_recaptcha_private_key} = \%recaptcha_private_key;
    $e->{google_recaptcha_public_key} = \%recaptcha_public_key;
    
    ## Robot patterns to block.
    my @bot_patterns;
    push( @bot_patterns, 
          "FirstGov",  "Jeeves", "BecomeBot", "AI-Agent", 
	  "ysearch", "crawler", "Slurp", 
          "accelobot", "NimbleCrawler", "Java", "bot",
	  "slurp",   "libwww",  "lwp", 
          "LWP", "Mechanize",  "Sphider",  "Wget",  "wget", ); 
    $e->{ bot_patterns } = \@bot_patterns;

    ##################  
    $e->{ bin_dir } = $e->{ cgi_dir } . "/bin/" . $e->{ arch };
 
    $e->{ perl_bin } = "/usr/bin/perl";

    $e->{ grep_bin } = $e->{ bin_dir } . "/grep";
    $e->{ mview_bin } = $e->{ bin_dir } . "/mview";
    $e->{ gs_bin } =  "/usr/local/bin/gs"; 
    $e->{ ma_bin } = $e->{ bin_dir } . "/ma"; 
    $e->{ seqret_bin } = $e->{ bin_dir } . "/lt-seqret"; 
    $e->{ raxml_bin } = $e->{ bin_dir } . "/raxml"; 
    $e->{ r_bin } = "/usr/local/bin/R"; 

    $e->{ tmp_url } = $e->{ base_url } . "/tmp";
    $e->{ tmp_dir } = $e->{ base_dir } . "/tmp";
    $e->{ small_color_array_file } = $e->{ cgi_dir } . "/color_array.txt";
    $e->{ large_color_array_file } = $e->{ cgi_dir } . "/rgb.scrambled.txt";
    $e->{dark_color_array_file} = $e->{cgi_dir} . "/dark_color.txt";
    $e->{ green2red_array_file } = $e->{ cgi_dir } . "/green2red_array.txt";

    # ---------------------------------------------
    # 
    # Yahoo UI
    #
    # ---------------------------------------------
    $e->{ yui_tables } = 1; 
    # Yahhoo UI directory.
    # 2.5
    $e->{ yui_dir } = "$base_dir/../yui"; 
    # 2.7
    $e->{ yui_dir_27 } = "$base_dir/../yui270/yui"; 
    # 2.8
    $e->{ yui_dir_28 } = "$base_dir/../yui282/yui"; 

    $e->{ use_yahoo_table } = 1;
    $e->{ helpballoon_dir } = $e->{ http } . $e->{ domain_name } . "/helpballoon";

    $e->{ myimg_job } = 1;

    $e->{ verbose } = 1;
    		# General verbosity level. 0 - very little, 1 - normal,
		#   >1 more verbose.

    ## Charting parameters

    # charting /usr/local/j2se 
    $e->{java_home}    = "/app/java"; 

    # CVS location of the java lib directory
    $e->{chart_lib} = "/webfs/klchu/repos/jgi-img-igb/webUI/lib";

    # location of the runchart.sh script
    # IF blank "" the charting feature will be DISABLED
    $e->{ chart_exe } = $e->{ cgi_dir } . "/bin/runchart.sh";

    # chart script logging feature - used only of debugging
    # best to leave it blank "" or "/dev/null"
    $e->{ chart_log } = "";

    ## database connections
    my $filename = "/webfs/img_rdbms/config/oracle.submit.json";
    my $json_text;
    open(FH, '<', $filename) or die $!;
    while(<FH>){
        $json_text .= $_;
    }
    close(FH);
    my $dbs_ref = from_json($json_text);
    $e->{ db_connect } = $dbs_ref;

    # new for 3.2
    # decorator.sh
    #
    $e->{ decorator_exe } = $e->{ cgi_dir } . "/bin/decorator.sh";
    # location of jar files 
    $e->{ decorator_lib } = $e->{ base_dir };
    # decorator script logging feature - used only of debugging
    # best to leave it blank "" or "/dev/null"
    $e->{ decorator_log } = "";
    #$e->{neighbor_bin} = "/jgi/tools/bin/neighbor";
    #$e->{protdist_bin} = "/jgi/tools/bin/protdist"; 
    $e->{neighbor_bin} = $e->{img_tools_dir} . "arb/bin/neighbor"; 
    $e->{protdist_bin} = $e->{img_tools_dir} . "arb/bin/protdist"; 

    #$e->{newick_all} = "/home/aratner/newick-all.txt";
    $e->{workspace_dir} =  "/webfs/web_data/workspace";
    $e->{web_data_dir} = "/global/dna/projectdirs/microbial/img_web_data";
    $e->{newick_all} = $e->{ web_data_dir } . "/newick/newick-all.3.3.txt";


    # bugmaster system to send email from questions and comments section
    $e->{ sendmail } = "/usr/sbin/sendmail";

    # JIRA - replaces bugmaster
    # url to where to submit the form
    $e->{jira_submit_url} = '';#$e->{ http } . "contact.jgi-psf.org/cgi-bin/support/contact_send.pl";
    
    # if jira form submit fails display error message with the following email
    $e->{ jira_email_error } = 'jgi-imgsupp@lbl.gov';

    # ssl enable - only for er and mer on merced - new for v3.3 - ken
    #
    # see https_cgi_url
    $e->{ssl_enabled} = 0;

    # new for 3.3 only for img system: mer and er and server merced. Its not for spock
    # because spock has no ssl cert. - Ken
    #
    # see ssl_enabled 
    $e->{ https_cgi_url } = $e->{ http } . $e->{ domain_name } . "/cgi-bin/$urlTag/" . $e->{ main_cgi };
    
    # to put a special message in the message area below the menu
    # leave it blank to display no message
    $e->{message} = "developer test site";
    
    # message_file - /home/ken/message_file.txt
    $e->{message_file} = '';#"/house/homedirs/k/klchu/message_file.txt";

    # caliban sso           
    # if null do not use sso
    #
    # for stage web farm MUST change url to .jgi-psf.org
    # for prodution use ".jgi.doe.gov" 
    #
    $e->{sso_enabled} = 1; 

    $e->{sso_domain} = ".jgi.doe.gov"; 
    $e->{sso_url} = $e->{ http } . "signon" . $e->{sso_domain}; 
    $e->{sso_cookie_name} = "jgi_return"; 
    $e->{sso_session_cookie_name} = "jgi_session"; # cookie that stores the caliban session id is has the format of "/api/sessions/30a6fa0dc58d3708"
    $e->{sso_api_url} =  $e->{sso_url} . "/api/sessions/"; # "https://signon.jgi.doe.gov/api/sessions/"; # session id from cookie jgi_session  
    $e->{sso_user_info_url} = $e->{sso_url} . '/api/users/';     
    return $e;
}


1;
