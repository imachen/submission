#!/usr/bin/perl

use strict;
use JSON;
umask 0002;

    ## database connections
    my $imgsg_ref = {
    user => 'imgsg_dev',
        pw => 'VHVlc2RheQ==',
    service => 'imgiprdpdb',
    ora_host => 'oracle-vm-1.jgi.lbl.gov',
    ora_port => '1521',
    ora_sid => 'imgiprdpdb',
    };

    my $imgext_ref = {
    user => 'img_ext',
        pw => 'aW1nX2V4dDk4Nw==',
    service => 'imgiprdpdb',
    ora_host => 'oracle-vm-1.jgi.lbl.gov',
    ora_port => '1521',
    ora_sid => 'imgiprdpdb',
    };

    my $imger_ref = {
       user => 'img_core_v400',
       pw => 'aW1nQ29yZUMwc00wczE=',
       service => 'gemini1pdb',
       ora_host => 'oracle-vm-2.jgi.lbl.gov',
       ora_port => '1521',
       ora_sid => 'gemini1pdb',
    };

    my $imgmer_ref = {
       user => 'img_core_v400',
       pw => 'aW1nQ29yZUMwc00wczE=',
       service => 'gemini1pdb',
       ora_host => 'oracle-vm-2.jgi.lbl.gov',
       ora_port => '1521',
       ora_sid => 'gemini1pdb',
    };

    my $imgw_ref = {
       user => 'img_core_v400',
       pw => 'aW1nQ29yZUMwc00wczE=',
       service => 'gemini1pdb',
       ora_host => 'oracle-vm-2.jgi.lbl.gov',
       ora_port => '1521',
       ora_sid => 'gemini1pdb',
    };

    my $imgtaxon_ref = { 
        user => 'img_i_taxon', 
        pw => 'aW1nX2lfdGF4b245ODc=',
        service => 'imgiprdpdb', 
    ora_host => 'oracle-vm-1.jgi.lbl.gov',
        ora_port => '1521', 
        ora_sid => 'imgiprdpdb', 
    }; 

    my $imgi_ref = { 
        user => 'img_i_v350', 
        pw => 'aW1nX2lfdjM1MDk4Nw==',
        service => 'imgiprdpdb',
    ora_host => 'oracle-vm-1.jgi.lbl.gov',
        ora_port => '1521', 
        ora_sid => 'imgiprdpdb',
    }; 
 
    my $imgpg_ref = { 
        user => 'img_pg_v330', 
        pw => 'aW1nX3BnX3YzMzAxMjM=', 
        service => 'imgiprdpdb', 
    ora_host => 'oracle-vm-1.jgi.lbl.gov',
        ora_port => '1521', 
        ora_sid => 'imgiprdpdb', 
    }; 
 
    my $dbs_ref = { 
        imgsg => $imgsg_ref, 
        imgext => $imgext_ref, 
        imger => $imger_ref, 
        imgmer => $imgmer_ref, 
        imgw => $imgw_ref, 
        imgi => $imgi_ref, 
        imgtaxon => $imgtaxon_ref, 
        imgpg => $imgpg_ref, 
    }; 
 
my $json_text = to_json( $dbs_ref, { utf8 => 1, pretty => 1 } ); #encode_json $dbs_ref;
print "$json_text \n";
my $filename = "/webfs/img_rdbms/config/oracle.submit.json";
print "Create file: $filename\n";
open (FH, '>', $filename) or die $!;
print FH $json_text;
close(FH);
exit 0;