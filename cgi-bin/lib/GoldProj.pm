package GoldProj;

use strict; 
#use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
use lib 'lib'; 
use WebEnv;
use WebFunctions; 
use RelSchema;
use ProjectInfo;
use TabHTML;

use POSIX;

my $env = getEnv(); 
my $base_url = $env->{ base_url }; 
my $use_yahoo_table = 0;  # $env->{ use_yahoo_table }; 

my $default_max_row = 50;


#########################################################################
# dispatch 
#########################################################################
sub dispatch { 
    my ($page) = @_; 
 

    if ( $page eq 'newProject' ) {
        NewGoldProject();
    } 
    elsif ( $page eq 'dbNewGoldProject' ) {
        my $msg = ProjectInfo::dbNewProjectInfo(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'updateProject' ) {
        my $project_oid = param1('project_oid');
        if ( blankStr($project_oid) ) {
            printError("No project has been selected."); 
            print end_form(); 
            return; 
        } 
 
        UpdateGoldProject($project_oid); 
    } 
    elsif ( $page eq 'dbUpdateGoldProject' ) { 

        my $msg = ProjectInfo::dbUpdateProject(); 

        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
	}
    } 
    elsif ( $page eq 'deleteProject' ) { 
        DeleteGoldProject(); 
    } 
    elsif ( $page eq 'dbDeleteGoldProject' ) {
        my $msg = ProjectInfo::dbDeleteProject(); 
 
        if ( ! blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    } 
    elsif ( $page eq 'copyProject' ) {
        my $project_oid = param1('project_oid');
        CopyGoldProject($project_oid); 
    } 
    elsif ( $page eq 'showPage' ) {
	if ( $use_yahoo_table ) {
	    ShowPage_yui();
	}
	else {
	    ShowPage();
	}
    }
    elsif ( $page eq 'displayProject' ) {
        my $project_oid = param1('project_oid');
        ProjectInfo::DisplayProject($project_oid); 
    }
    elsif ( $page eq 'printableData' ) {
        my $project_oid = param1('project_oid');
        if ( $project_oid ) {
            ProjectInfo::ShowPrintableData($project_oid); 
        } 
    }
#    elsif ( $page eq 'displaySample' ) {
#        my $sample_oid = param1('sample_oid');
#        EnvSample::DisplaySample($sample_oid);
#    }
    elsif ( $page eq 'showCategory' ) {
	my $web_code = '';
	if ( defined param1('web_page_code') ) {
	    $web_code = param1('web_page_code');
	}

	ShowNextPage($web_code);
    }
    elsif ( $page eq 'searchId' ) {
	SearchByID();
    }
    elsif ( $page eq 'searchIdResult' ) {
	ShowSearchIdResultPage();
    }
    elsif ( $page eq 'advSearch' ) {
	AdvancedSearch();
    }
    elsif ( $page eq 'advSearchResult' ) {
	ShowAdvSearchResultPage();
    }
    elsif ( $page eq 'filterProject' ) {
	FilterGoldProject();
    }
    elsif ( $page eq 'applyGoldProjectFilter' ) {
        ApplyGoldProjectFilter(); 

	if ( $use_yahoo_table ) {
	    ShowPage_yui();
	}
	else {
	    ShowPage();
	}
    } 
    elsif ( $page eq 'changeContact' ) {
        my $msg = ProjectInfo::dbChangeProjectContact();
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowNextPage();
        }
    }
    elsif ( $page eq 'grantEdit' ) {
	ProjectInfo::GrantEditPrivilege();
      } 
    elsif ( $page eq 'dbGrantEditPrivilege' ) {
        my $msg = ProjectInfo::dbGrantEditPrivilege(); 
 
        if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    } 
    elsif ( $page eq 'setGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        ProjectInfo::SetGoldStampId($project_oid); 
    } 
    elsif ( $page eq 'dbSetGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        my $gold_type = param1('gold_type'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = ProjectInfo::dbSetGoldStampId($project_oid, $gold_type,
						$gold_stamp_id); 
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
        } 
    } 
    elsif ( $page eq 'delGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        ProjectInfo::DeleteGoldStampId($project_oid); 
    } 
    elsif ( $page eq 'dbDelGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        my $del_gold_id_option = param1('del_gold_id_option'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = ProjectInfo::dbDelGoldStampId($project_oid, $del_gold_id_option, 
						$gold_stamp_id);
 
        if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'mergeGoldProj' ) {
	PrintMergeGoldProjPage();
    }
    elsif ( $page eq 'mergeProjects' ) { 
        my $project_oid = param1('project_oid'); 
        if ( blankStr($project_oid) ) { 
            printError("No project has been selected."); 
            print end_form(); 
            return; 
        } 
 
        my $merged_project = param1('merged_project'); 
        if ( blankStr($merged_project) ) { 
            printError("Please select a project to merge."); 
            print end_form(); 
            return; 
        } 
 
        if ( $project_oid == $merged_project ) { 
            printError("The two projects are the same. Please select a different project to merge."); 
            print end_form(); 
            return; 
        } 
 
        ProjectInfo::MergeProjects($project_oid, $merged_project);
    } 
    elsif ( $page eq 'dbMergeProjects' ) {
        my $msg = ProjectInfo::dbMergeProjects(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    } 
    else {
	if ( $use_yahoo_table ) {
	    ShowPage_yui();
	}
	else {
	    ShowPage();
	}
    }
}


#########################################################################
# ShowNextPage
##########################################################################
sub ShowNextPage {
    my ($new_code) = @_;

    my $page = param1('page');
    my $web_code2 = param1('web_code');

    if ( $web_code2 && $web_code2 > 4 ) {
	if ( $use_yahoo_table ) {
	    ShowCategoryPage_yui($web_code2);
	}
	else {
	    ShowCategoryPage($web_code2);
	}
    }
    elsif ( $page eq 'showCategory' && length($new_code) > 0 ) {
	if ( $use_yahoo_table ) {
	    ShowCategoryPage_yui($new_code);
	}
	else {
	    ShowCategoryPage($new_code);
	}
    }
    elsif ( $page eq 'showCategory' && defined param1('web_code') ) {
	my $web_code = param1('web_code');
	if ( length($web_code) > 0 ) {
	    if ( $use_yahoo_table ) {
		ShowCategoryPage_yui($web_code);
	    }
	    else {
		ShowCategoryPage($web_code);
	    }
	}
	else {
	    if ( $use_yahoo_table ) {
		ShowPage_yui();
	    }
	    else {
		ShowPage();
	    }
	}
    }
    else {
	if ( $use_yahoo_table ) {
	    ShowPage_yui();
	}
	else {
	    ShowPage();
	}
    }

#    if ( $next_page eq 'searchIdResult' ) {
#	ShowPage();
#    }
#    else {
#	ShowCategoryPage();    
#    }
}


#########################################################################
# ShowPage_yui -- show projects (Yahoo Table display)
##########################################################################
sub ShowPage_yui {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 

    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h2>IMG-GOLD Genome Projects (All)</h2>\n";

    my $cnt = 0; 
    my $select_cnt = 0; 

    print "<h4>Only those projects you have permission to update are selectable.</h4>\n";

    $cnt = allGoldProjectCount($contact_oid);
    $select_cnt = filteredGoldProjectCount($contact_oid);

    print "<p>Selected Project Count: $select_cnt (Total: $cnt)";
    if ( $select_cnt != $cnt ) {
	print nbsp(3);
	print "<font color='red'>(Filter is on. Not all projects are displayed.)</font></p>\n";
    }
    else {
	print "</p>\n";
    }

    printGoldProjButtons(1);

    if ( $select_cnt > 0 ) {
	listGoldProjects_yui($contact_oid, '', $select_cnt); 

	printGoldProjButtons(1);
    }

    if ( $isAdmin eq 'Yes' ) {
	print hr;

	print "<h2>Search GOLD Project</h2>\n";
	print '<input type="submit" name="_section_GoldProj:searchId" value="Search By GOLD ID" class="medbutton" />'; 
#    print "&nbsp; \n"; 
#    print '<input type="submit" name="_section_GoldProj:advSearch" value="Advanced Search" class="medbutton" />'; 

	printAdditionalProjSection();
    }

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


#########################################################################
# ShowPage
##########################################################################
sub ShowPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 

    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h2>IMG-GOLD Genome Projects (All)</h2>\n";

    my $cnt = 0; 
    my $select_cnt = 0; 

    print "<h4>Only those projects you have permission to update are selectable.</h4>\n";

    $cnt = allGoldProjectCount($contact_oid);
    $select_cnt = filteredGoldProjectCount($contact_oid);

    print "<p>Selected Project Count: $select_cnt (Total: $cnt)";
    if ( $select_cnt != $cnt ) {
	print nbsp(3);
	print "<font color='red'>(Filter is on. Not all projects are displayed.)</font></p>\n";
    }
    else {
	print "</p>\n";
    }

    # save orderby param
    my $orderby = param1('goldproj_orderby');
    my $desc = param1('project_desc');
 
    # max display 
    my $max_display = getSessionParam('proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    # display page numbers 
    my $curr_page = param1('goldproj_page_no'); 
    my $url1 = url() . "?section=GoldProj&page=showPage&goldproj_page_no=";
    my $url2 = "";
    if ( ! blankStr($orderby) ) { 
	$url2 .= "&goldproj_orderby=$orderby"; 
	if ( ! blankStr($desc) ) { 
	    $url2 .= "&project_desc=$desc"; 
	} 
    } 
    DisplayProjPageNo($curr_page, $max_display, $select_cnt, $url1, $url2);

#    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
#        $curr_page = 1; 
#    } 
#    my $i = 0; 
#    my $page = 1; 
#    print "<p>\n"; 
#    while ( $i < $select_cnt ) { 
#        my $s = $page; 
#        if ( $page == $curr_page ) { 
#            $s = "<b>[$page]</b>"; 
#        } 
#        my $link = "<a href='" . url() . 
#            "?section=GoldProj&page=showPage" . 
#            "&goldproj_page_no=$page"; 
#        if ( ! blankStr($orderby) ) { 
#            $link .= "&goldproj_orderby=$orderby"; 
#            if ( ! blankStr($desc) ) { 
#                $link .= "&project_desc=$desc"; 
#            } 
#        } 
#        $link .= "' >" . $s . "</a>";
#        print $link . nbsp(1);
#        $i += $max_display; 
#        $page++; 
#        if ( $page > 1000 ) { 
#            last;
#        }
#    } 
 
    printGoldProjButtons(1);

    if ( $select_cnt > 0 ) {
	listGoldProjects($contact_oid, $curr_page, '', $select_cnt); 

	printGoldProjButtons(1);
    }

    if ( $isAdmin eq 'Yes' ) {
	print hr;

	print "<h2>Search GOLD Project</h2>\n";
	print '<input type="submit" name="_section_GoldProj:searchId" value="Search By GOLD ID" class="medbutton" />'; 
#    print "&nbsp; \n"; 
#    print '<input type="submit" name="_section_GoldProj:advSearch" value="Advanced Search" class="medbutton" />'; 

	printAdditionalProjSection();
    }

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


#########################################################################
# DisplayProjPageNo
##########################################################################
sub DisplayProjPageNo {
    my ($curr_page, $per_page, $total_cnt, $url1, $url2) = @_;

    # make sure curr_page is defined
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 

    # start and end pages
    my $cnt0 = 20;
    my $start = $cnt0 * floor(($curr_page-1) / $cnt0) + 1;
    my $end = $start + $cnt0 - 1;
    my $last = ceil($total_cnt / $per_page);
    if ( $end > $last ) {
	$end = $last;
    }
    my $link = "";

#    print "<p>curr_page: $curr_page, start: $start, end: $end, last: $last</p>\n";

    print "<p>\n";

    # show first?
    if ( $start > 1 ) {
        $link = "<a href='" . $url1 . "1" . $url2;
        $link .= "' >" . "First" . "</a>";
        print $link . nbsp(1);
    }

    # show previous?
    if ( $start > ($cnt0 + 1) ) {
	my $prev = $start - $cnt0;
        $link = "<a href='" . $url1 . $prev . $url2;
        $link .= "' >" . "Prev" . "</a>";
        print $link . nbsp(1);
    }

    my $i = 0; 
    my $page = 1; 
    for ( $page = $start; $page <= $end && $page <= $last; $page++) {
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>[$page]</b>"; 
        } 
        $link = "<a href='" . $url1 . $page . $url2;
        $link .= "' >" . $s . "</a>";
        print $link . nbsp(1);
    }   # end for loop

    # show next?
    $page = $start + $cnt0;
    if ( $page <= $last ) {
        $link = "<a href='" . $url1 . $page . $url2;
        $link .= "' >" . "Next" . "</a>";
        print $link . nbsp(1);
    }

    # show last
    if ( $last > $end ) {
        $link = "<a href='" . $url1 . $last . $url2;
        $link .= "' >" . "Last" . "</a>";
        print $link . nbsp(1);
    }
}


#########################################################################
# ShowGoldProjects
##########################################################################
sub ShowGoldProjects {
    my %web_page_code = getGoldWebPageCode();

    my $dbh = Connect_IMG();

    my $sql = qq{
	select web_page_code, count(*) from project_info
	    group by web_page_code
	    order by web_page_code
	};

webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $row_cnt = 0;

    print "<p>\n"; 
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Category</th>\n";
    print "<th class='img'>Count</th>\n";

    for (;;) { 
        my ( $code, $count ) = $cur->fetchrow( );
	if ( ! (defined $code) || length($code) == 0 ) {
	    last;
	}

	$row_cnt++;
	if ( $row_cnt > 10 ) {
	    last;      # just in case
	}

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
	if ( $web_page_code{$code+1} ) {
	    print escapeHTML($web_page_code{$code+1});
	}
	else {
	    print escapeHTML($code);
	}
        print "</td>\n"; 

        print "<td class='img'>\n";
        my $link = "<a href='" . url() .
            "?section=GoldProj&page=showCategory" .
	    "&web_code=$code" .
            "&category_page_no=1";
        $link .= "' >" . $count . "</a>";
	print $link;
        print "</td>\n"; 

	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();
}


######################################################################### 
# allGoldProjectCount - count the number of projects 
########################################################################## 
sub allGoldProjectCount { 
    my ($contact_oid) = @_; 

    my $isAdmin = 'No';
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    } 

    my $dbh=WebFunctions::Connect_IMG; 
 
    my $sql = "select count(*) from project_info p" .
	" where p.domain != 'MICROBIAL'";

#    if ( $contact_oid ) { 
#	if ( getCanEditImgGold($contact_oid) ) {
#	    $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid)"; 
#	}
#	else {
#	    $sql .= " and p.contact_oid = $contact_oid"; 
#	}
#    } 

    if ( $isAdmin eq 'Yes' ) {
	# super user can view all projects
    }
    else {
	# only GOLD projects or user's own projects
        $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))"; 
    }
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish(); 
    $dbh->disconnect();
 
    if ( ! $cnt ) {
        return 0; 
    } 
    return $cnt; 
} 

 
######################################################################### 
# filteredGoldProjectCount - count the number of filtered projects
########################################################################## 
sub filteredGoldProjectCount {
    my ($contact_oid) = @_; 
 
    my $isAdmin = 'No';
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    } 

    my $dbh=WebFunctions::Connect_IMG;
 
    my $filter_cond = ProjectInfo::projectFilterCondition();
    my $sql = "select count(*) from project_info p" .
	" where p.domain != 'MICROBIAL'";

    my $only_my_project = getSessionParam('proj_filter:only_my_project');
    if ( $only_my_project ) { 
	# there is already a condition on owner
    }
    elsif ( $isAdmin eq 'Yes' ) {
	# super user can view all projects
    }
    else {
	# only GOLD projects or user's own projects
        $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";

    }

    # filter condition
    if ( ! blankStr($filter_cond) ) {
	$sql .= " and " . $filter_cond;
    } 

#    if ( $contact_oid == 312 ) {
#	print "<p>SQL: $sql</p>\n";
#    }

webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 


#########################################################################
# getGoldWebPageCode
#
# (Note: need to use term_oid+1 because code starts with 0)
##########################################################################
sub getGoldWebPageCode {
    my %h;

    my $dbh = Connect_IMG();
    my $sql = "select term_oid+1, description from web_page_codecv";
webLog("$sql\n");    
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    for (;;) { 
        my ( $id, $val ) = $cur->fetchrow( );
	last if !$id;

	$h{$id} = $val;
    }
    $cur->finish();
    $dbh->disconnect();

    return %h;
}

###################################################################################
# ShowCategoryPage_yui -- show category page (Yahoo Table display)
###################################################################################
sub ShowCategoryPage_yui {
    my ($new_code) = @_;

    my $code = $new_code;
    if ( length($code) == 0 ) {
	$code = param1('web_code');
	if ( length($code) == 0 ) {
	    ShowPage_yui();
	    return;
	}
    }

    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    my $cat_sql = "";
    if ( $code > 4 ) { 
        $cat_sql = qq{ 
            select count(*) from project_info p 
                where p.domain != 'MICROBIAL' 
                and (p.contact_oid = $contact_oid 
                     or p.project_oid in (select cpp.project_permissions 
                                          from contact_project_permissions cpp 
                                          where cpp.contact_oid = $contact_oid)) 
            }; 
    }
    else {
	$cat_sql = "select count(*) from project_info where web_page_code = $code";
	if ( $isAdmin ne 'Yes' ) {
	    $cat_sql .= " and gold_stamp_id is not null";
	}
    }

    my $category_count = db_getValue($cat_sql);
    if ( $code > 4 ) {
	print "<h2>My Genome Projects (Count: $category_count)</h2>\n";
    }
    else {
	my $category = db_getValue("select description from web_page_codecv where term_oid=$code");
	print "<h2>IMG-GOLD $category (GOLD Projects Only)</h2>\n";

	print "<h4>Only those projects you have permission to update are selectable. (Count: $category_count)</h4>\n";
    }

    # save parameters
    print hiddenVar('web_code', $code);

    printGoldProjButtons(0);

    listGoldProjects_yui($contact_oid, $code, $category_count);

    printGoldProjButtons(0);

    printAdditionalProjSection();

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


#########################################################################
# ShowCategoryPage
##########################################################################
sub ShowCategoryPage {
    my ($new_code) = @_;

    my $code = $new_code;
    if ( length($code) == 0 ) {
	$code = param1('web_code');
	if ( length($code) == 0 ) {
	    ShowPage();
	    return;
	}
    }

    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    if ( param1('page') ) {
	print hiddenVar('page', param1('page'));
    }
    else {
	print hiddenVar('page', 'showCategory');
    }

 
    my $cat_sql = "";
    if ( $code > 4 ) { 
        $cat_sql = qq{ 
            select count(*) from project_info p 
                where p.domain != 'MICROBIAL' 
                and (p.contact_oid = $contact_oid 
                     or p.project_oid in (select cpp.project_permissions 
                                          from contact_project_permissions cpp 
                                          where cpp.contact_oid = $contact_oid)) 
            }; 
    }
    else {
	$cat_sql = "select count(*) from project_info where web_page_code = $code";
	if ( $isAdmin ne 'Yes' ) {
	    $cat_sql .= " and gold_stamp_id is not null";
	}
    }

    my $category_count = db_getValue($cat_sql);
    if ( $code > 4 ) {
	print "<h2>My Genome Projects (Count: $category_count)</h2>\n";
    }
    else {
	my $category = db_getValue("select description from web_page_codecv where term_oid=$code");
	print "<h2>IMG-GOLD $category (GOLD Projects Only)</h2>\n";

	print "<h4>Only those projects you have permission to update are selectable. (Count: $category_count)</h4>\n";
    }

    my $curr_page = param1('category_page_no');
    my $orderby = param1('goldproj_orderby'); 
    my $desc = param1('project_desc'); 
    my $max_display = getSessionParam('proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 

    # save parameters
    print hiddenVar('web_code', $code);
    print hiddenVar('category_page_no', $curr_page);
    if ( ! blankStr($orderby) ) {
	print hiddenVar('goldproj_orderby', $orderby);
    }
    if ( ! blankStr($desc) ) {
	print hiddenVar('project_desc', $desc);
    }

    # display page numbers
    my $url1 = url() . "?section=GoldProj&page=showCategory" . 
	    "&web_code=$code" .
            "&category_page_no=";
    my $url2 = "";
    if ( ! blankStr($orderby) ) { 
	$url2 .= "&goldproj_orderby=$orderby"; 
	if ( ! blankStr($desc) ) { 
	    $url2 .= "&project_desc=$desc"; 
	} 
    } 
    DisplayProjPageNo($curr_page, $max_display, $category_count,
		      $url1, $url2);

#    my $i = 0; 
#    my $page = 1; 
#    while ( $i < $category_count ) { 
#        my $s = $page; 
#        if ( $page == $curr_page ) { 
#            $s = "<b>$page</b>"; 
#        } 
#        my $link = "<a href='" . url() . 
#            "?section=GoldProj&page=showCategory" . 
#	    "&web_code=$code" .
#            "&category_page_no=$page";
#
#        if ( ! blankStr($orderby) ) { 
#            $link .= "&goldproj_orderby=$orderby"; 
#            if ( ! blankStr($desc) ) { 
#                $link .= "&project_desc=$desc"; 
#            } 
#        } 

#        $link .= "' >" . $s . "</a>"; 
#        print $link . nbsp(1); 
#        $i += $max_display; 
#        $page++; 
#        if ( $page > 1000 ) {
#	    print " ...\n";
#            last; 
#        } 
#    } 
 
    printGoldProjButtons(0);

    listGoldProjects($contact_oid, $curr_page, $code, $category_count);

    printGoldProjButtons(0);

    printAdditionalProjSection();

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


##########################################################################
# listGoldProjects_yui - list projects (Yahoo datatable display)
# 
# admin does not have condition on contact_oid
##########################################################################
sub listGoldProjects_yui {
    my ($contact_oid, $code, $cnt) = @_; 

#    my $cond = "where p.web_page_code = 0";
    my $cond = "where p.domain != 'MICROBIAL'";

    if ( length($code) == 0 ) {
	my $filter_cond = ProjectInfo::projectFilterCondition();
	if ( ! blankStr($filter_cond) ) {
	    $cond .= " and " . $filter_cond;
	}
    }
    elsif ( $code > 4 ) {
        $cond .= " and (p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
    }
    else {
	$cond = "where p.web_page_code = $code";
    }

#    my $my_c_oid = getContactOid(); 
    my $isAdmin = 'No'; 
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    } 
    my $can_edit = getCanEditImgGold($contact_oid); 

    my $only_my_project = getSessionParam('proj_filter:only_my_project');
    if ( $only_my_project || $code > 4 ) {
	# there is already a condition on project owners
    }
    elsif ( $isAdmin eq 'Yes' ) {
	# all projects
    }
    else {
        my $perm_cond = "(p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))"; 
        if ( blankStr($cond) ) {
            $cond = "where " . $perm_cond;
        } 
        else { 
            $cond .= " and " . $perm_cond; 
        } 
    }

    my $selected_proj = param1('project_oid');
 
    # max display 
    my $max_display = getSessionParam('proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    my $dbh=WebFunctions::Connect_IMG;

    my %contact_list;
    my $sql = "select contact_oid, username from contact";
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
    for (;;) {
	my ($id2, $name2) = $cur->fetchrow();
	last if ! $id2;

	$contact_list{$id2} = $name2;
    }
    $cur->finish();

    my %project_access;
    $sql = "select contact_oid, project_permissions from contact_project_permissions where contact_oid = $contact_oid";
    $cur=$dbh->prepare($sql);
    $cur->execute(); 
    for (;;) {
	my ($id2, $pr2) = $cur->fetchrow();
	last if ! $id2;

	$project_access{$pr2} = $pr2;
    }
    $cur->finish();

    my $cpp_sql = "select count(*) from contact_project_permissions where contact_oid = $contact_oid and project_permissions = ";

    $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.ncbi_project_id, 
	to_char(p.add_date, 'yyyy-mm-dd'), p.contact_name, 
	to_char(p.mod_date, 'yyyy-mm-dd'), p.contact_oid
            from project_info p 
            $cond
            order by 1
        }; 
 
    if ( $contact_oid == 312 ) {
        print "<p>SQL: $sql</p>";
    } 
#webLog("$sql\n");
    $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    print "<p>\n";

    my $it = new InnerTable( 1, "Gold$$", "Gold", 0);
    my $sd = $it->getSdDelim();    # sort delimiter
    $it->addColSpec( "Select" ); 

    $it->addColSpec("ER Project ID", "number asc", "right", "", "ER Project ID");
    $it->addColSpec("Project Display Name", "char asc", "left", "", "Project Display Name");
    $it->addColSpec("GOLD ID", "char asc", "left", "", "GOLD ID");
    $it->addColSpec("Phylogeny", "char asc", "left", "", "Phylogeny");
    $it->addColSpec("NCBI Project ID", "number asc", "right", "", "NCBI Project ID");
    $it->addColSpec("Contact Name", "char asc", "left", "", "Contact Name");
    $it->addColSpec("IMG Contact", "char asc", "left", "", "IMG Contact");
    $it->addColSpec("Add Date", "char asc", "left", "", "Add Date");
    $it->addColSpec("Last Mod Date", "char asc", "left", "", "Last Mod Date");
 
    my $cnt2 = 0; 
    my $trunc = 0;
    my $max_display_limit = 1000;

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $ncbi_project_id, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
	if ( $cnt2 > $max_display_limit ) {
	    $trunc = 1;
	    last;
	}

	my $row = "";

	# check edit permission
#	if ( $c_oid == $contact_oid || $can_edit ||
#	     db_getValue($cpp_sql . $proj_id) ) {
	if ( $c_oid == $contact_oid || $can_edit || $project_access{$proj_id} ) {
	    $row .= $sd . "<input type='radio' name='project_oid' " .
		"value='$proj_id' />\t";
	}
	else {
	    # blank
	    $row .= " " . $sd . " \t";
	}
 
        my $proj_link = getProjectLink($proj_id);
	$row .= $proj_id . $sd . $proj_link . "\t";
 
	$row .= $proj_name . $sd . $proj_name . "\t";

        my $gold_link = getGoldLink($gold_stamp_id);
	$row .= $gold_stamp_id . $sd . $gold_link . "\t";
 
	$row .= $phylo . $sd . $phylo . "\t";

	if ( $ncbi_project_id ) {
	    my $ncbi_pid_link = getNcbiProjLink($ncbi_project_id);
	    $row .= $ncbi_project_id . $sd . $ncbi_pid_link . "\t";
	}
	else {
	    $row .= $ncbi_project_id . $sd . $ncbi_project_id . "\t";
	}

	$row .= $c_name . $sd . $c_name . "\t";
 
        if ( $contact_list{$c_oid} ) {
	    my $username = $contact_list{$c_oid};
	    if ( $isAdmin eq 'Yes' ) {
		my $user_link = "<a href='" . url() .
		    "?section=UserTool&page=showContactInfo" . 
		    "&selected_contact_oid=$c_oid' >" . $username . "</a>";
		$row .= $username . $sd . $user_link . "\t";
	    }
	    else {
		$row .= $username . $sd . $username . "\t";
	    }
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
	    if ( $isAdmin eq 'Yes' ) {
		my $user_link = "<a href='" . url() .
		    "?section=UserTool&page=showContactInfo" . 
		    "&selected_contact_oid=$c_oid' >" . $contact_str . "</a>";
		$row .= $contact_str . $sd . $user_link . "\t";
	    }
	    else {
		$row .= $contact_str . $sd . $contact_str . "\t";
	    }
        }
 
	$row .= $add_date . $sd . $add_date . "\t";
	$row .= $mod_date . $sd . $mod_date . "\t";

	$it->addRow($row);
    }

    if ( $cnt2 > 0 ) { 
	if ( $trunc ) {
	    print "<p><font color='red'>Too many projects. Only $max_display_limit projects are displayed. Please use filter to limit the projects.</font>\n";
	}

        $it->printOuterTable(1); 
    } 
    else { 
        print "<h5>No GOLD projects.</h5>\n";
    } 
 
    $cur->finish(); 
    $dbh->disconnect(); 

    return $cnt2;
}

#########################################################################
# listGoldProjects - list projects 
# 
# admin does not have condition on contact_oid
##########################################################################
sub listGoldProjects {
    my ($contact_oid, $curr_page, $code, $cnt) = @_; 

#    my $cond = "where p.web_page_code = 0";
    my $cond = "where p.domain != 'MICROBIAL'";

    if ( length($code) == 0 ) {
	my $filter_cond = ProjectInfo::projectFilterCondition();
	if ( ! blankStr($filter_cond) ) {
	    $cond .= " and " . $filter_cond;
	}
    }
    elsif ( $code > 4 ) {
        $cond .= " and (p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
    }
    else {
	$cond = "where p.web_page_code = $code";
    }

#    my $my_c_oid = getContactOid(); 
    my $isAdmin = 'No'; 
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    } 
    my $can_edit = getCanEditImgGold($contact_oid); 

    my $only_my_project = getSessionParam('proj_filter:only_my_project');
    if ( $only_my_project || $code > 4 ) {
	# there is already a condition on project owners
    }
    elsif ( $isAdmin eq 'Yes' ) {
	# all projects
    }
    else {
        my $perm_cond = "(p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))"; 
        if ( blankStr($cond) ) {
            $cond = "where " . $perm_cond;
        } 
        else { 
            $cond .= " and " . $perm_cond; 
        } 
    }

    my $selected_proj = param1('project_oid');
 
    # max display 
    my $max_display = getSessionParam('proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    my $dbh=WebFunctions::Connect_IMG;

    my %contact_list;
    my $sql = "select contact_oid, username from contact";
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
    for (;;) {
	my ($id2, $name2) = $cur->fetchrow();
	last if ! $id2;

	$contact_list{$id2} = $name2;
    }
    $cur->finish();

    my %project_access;
    $sql = "select contact_oid, project_permissions from contact_project_permissions where contact_oid = $contact_oid";
    $cur=$dbh->prepare($sql);
    $cur->execute(); 
    for (;;) {
	my ($id2, $pr2) = $cur->fetchrow();
	last if ! $id2;

	$project_access{$pr2} = $pr2;
    }
    $cur->finish();

    my $orderby = param1('goldproj_orderby');
    my $desc = param1('project_desc');
    if ( blankStr($orderby) ) {
        $orderby = "p.project_oid $desc";
    }
    else {
        if ( $orderby eq 'project_oid' ) {
            $orderby = "p.project_oid $desc"; 
        } 
        else {
            $orderby = "p." . $orderby . " $desc, p.project_oid";
        } 
    } 
 
    $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.ncbi_project_id, 
	p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
            $cond
            order by $orderby
        }; 
 
    if ( $contact_oid == 312 ) {
        print "<p>SQL: $sql</p>";
    } 
webLog("$sql\n");
    $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
#    print "<h5>Click the column name to have the data order by the selected column. Click (Rev) to order by the same column in reverse order.</h5>\n";

    print "<p>\n";
    my $color1 = "#eeeeee";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";

    my %names_h = (
	'project_oid' => 'ER Project ID',
	'display_name' => 'Project Display Name',
	'gold_stamp_id' => 'GOLD ID',
	'phylogeny' => 'Phylogeny',
	'ncbi_project_id' => 'NCBI Project ID',
	'add_date' => 'Add Date',
	'mod_date' => 'Last Mod Date' );

    my $orderby_attr = param1('goldproj_orderby');

    for my $attr1 ( 'project_oid', 'display_name', 'gold_stamp_id',
	'phylogeny', 'ncbi_project_id' ) {
        my $sort_flag = 0;
        if ( $attr1 eq $orderby_attr ) {
            if ( $desc eq 'desc' ) {
                $sort_flag = -1;
            }
            else {
                $sort_flag = 1;
            } 
        } 

	print "<th class='img' bgcolor='$color1'>" . 
	    getGoldProjOrderByLink($attr1, $names_h{$attr1}, $code, $cnt, $sort_flag) .
	    "</th>\n";
    }

    print "<th class='img' bgcolor='$color1'>Contact Name</th>\n";
    print "<th class='img' bgcolor='$color1'>IMG Contact</th>\n";

    for my $attr1 ( 'add_date', 'mod_date' ) {
        my $sort_flag = 0;
        if ( $attr1 eq $orderby_attr ) {
            if ( $desc eq 'desc' ) {
                $sort_flag = -1;
            }
            else {
                $sort_flag = 1;
            } 
        } 

	print "<th class='img' bgcolor='$color1'>" . 
	    getGoldProjOrderByLink($attr1, $names_h{$attr1}, $code, $cnt, $sort_flag) .
	    "</th>\n";
    }

    my $cnt2 = 0; 
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);
    my $cpp_sql = "select count(*) from contact_project_permissions where contact_oid = $contact_oid and project_permissions = ";

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $ncbi_project_id, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 <= $skip ) {
            next; 
        } 
 
        $disp_cnt++; 
        if ( $disp_cnt > $max_display ) {
            last;
        } 

        if ( $disp_cnt % 2 ) { 
            print "<tr class='img'>\n"; 
        } 
        else { 
            print "<tr class='img' bgcolor='#ddeeee'>\n"; 
        } 
 
        print "<td class='img'>\n";

	# check edit permission
#	if ( $c_oid == $contact_oid || $can_edit ||
#	     db_getValue($cpp_sql . $proj_id) ) {
	if ( $c_oid == $contact_oid || $can_edit || $project_access{$proj_id} ) {
	    print "<input type='radio' "; 
	    print "name='project_oid' value='$proj_id'";
	    if ( $proj_id == $selected_proj ) {
		print " checked ";
	    }
	    print "/>";
	}
        print "</td>\n"; 
 
        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
	if ( $ncbi_project_id ) {
	    PrintAttribute(getNcbiProjLink($ncbi_project_id)); 
	}
	else {
	    PrintAttribute($ncbi_project_id);
	}

        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
	    my $username = $contact_list{$c_oid};
	    if ( $isAdmin eq 'Yes' ) {
		my $link = "<a href='" . url() .
		    "?section=UserTool&page=showContactInfo" . 
		    "&selected_contact_oid=$c_oid' >";
		PrintAttribute($link . $username . "</a>");
	    }
	    else {
		PrintAttribute($username);
	    }
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
	    if ( $isAdmin eq 'Yes' ) {
		my $link = "<a href='" . url() .
		    "?section=UserTool&page=showContactInfo" . 
		    "&selected_contact_oid=$c_oid' >";
		PrintAttribute($link . $contact_str . "</a>");
	    }
	    else {
		PrintAttribute($contact_str);
	    }
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 
 
    $cur->finish(); 
    $dbh->disconnect(); 

    return $cnt2;
}


##########################################################################
# getGoldProjOrderByLink
##########################################################################
sub getGoldProjOrderByLink {
    my ($attr, $label, $code, $category_count, $sort) = @_;

    my $sort_icon = $base_url . "/images/sort_icon.png"; 
    my $up_icon = $base_url . "/images/up_icon.png"; 
    my $down_icon = $base_url . "/images/down_icon.png"; 

    my $sort_title = "click to sort in ascending order"; 
 
    my $link = escapeHTML($label) .
	"<a href='" . url() . "?section=GoldProj";

    if ( length($code) > 0 ) {
	$link .= "&page=showCategory&web_code=$code";
    }
    else {
	$link .= "&page=showPage";
    }

    $link .= "&category_page_no=1&goldproj_orderby=$attr";
    my $icon2 = $sort_icon;

    if ( $sort < 0 ) {
        $link .= "&project_desc=asc";
	$icon2 = $up_icon;
    }
    elsif ( $sort > 0 ) { 
        $link .= "&project_desc=desc";
	$icon2 = $down_icon;
	$sort_title = "click to sort in descending order";
    } 
    else {
        $link .= "&project_desc=asc";
    } 

    $link .= "&goldproj_page_no=1' >" . 
        "<img src='" . $icon2 . 
        "' width='11' height='11' border='0'" . 
        " alt='$sort_title' title='$sort_title' />" . "</a>"; 
 
    return $link; 
} 


######################################################################### 
# printGoldProjButtons - print New, Update, Delete and Copy buttons 
######################################################################### 
sub printGoldProjButtons {
    my ($with_filter) = @_;

    print "<br/>\n";

    # New, Update, Delete and Copy buttons 
    print '<input type="submit" name="_section_GoldProj:newProject" value="New" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:updateProject" value="Update" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:deleteProject" value="Delete" class="smbutton" />'; 
 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:copyProject" value="Copy" class="smbutton" />'; 

    if ( $with_filter ) {
	# project selection filter 
	print "&nbsp; \n"; 
	print '<input type="submit" name="_section_GoldProj:filterProject" value="Filter Projects" class="smbutton" />';
    }
}


######################################################################### 
# NewGoldProject 
######################################################################### 
sub NewGoldProject { 
 
    my $url=url(); 
    print start_form(-name=>'newProject',-method=>'post',action=>"$url"); 

#    if ( param1('next_page') ) {
#	print hiddenVar('next_page', param1('next_page'));
#    }

    print hiddenVar('page_name', 'newProject');
    my %db_val; 
    if ( defined param1('web_code') ) {
        $db_val{'web_page_code'} = param1('web_code');
    }

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid); 

    # tab view 
    my @tabIndex; 
    TabHTML::printTabAPILinks("newProject"); 
    if ($isAdmin eq 'Yes') {
	@tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", "#tab5", "#tab6" , 
		      "#tab7", "#tab8" ); 
    } else {
	@tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", "#tab5", "#tab6" ); 
    }
    my @tabNames;

    if ($isAdmin eq 'Yes') {
	@tabNames = 
	    ( "Organism Info (*)", "Project Info (*)", 
	      "Sequencing Info", "Environment Metadata",
	      "Host Metadata", "Organism Metadata", "HMP Metadata",
	    "JGI Info"); 
    } else {
	@tabNames = 
	    ( "Organism Info (*)", "Project Info (*)", 
	      "Sequencing Info", "Environment Metadata",
	      "Host Metadata", "Organism Metadata"); 
    } 
    TabHTML::printTabDiv( "newProject", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    ProjectInfo::printProjectTab("Organism", "", \%db_val); 
    print "</p></div>\n"; 
 
    # tab 2 
    print "<div id='tab2'><p>\n"; 
    ProjectInfo::printProjectTab("Project", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_project_relevance', '');
    ProjectInfo::printProjectSetValTab ('project_info_data_links', '');
    print "</p></div>\n";
 
    # tab 3 
    print "<div id='tab3'><p>\n"; 
    ProjectInfo::printProjectTab("Sequencing", "", \%db_val); 
#    ProjectInfo::printProjectTab("Links", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_seq_method', '');

    print "</p></div>\n";
 
    # tab 4
    print "<div id='tab4'><p>\n";
    ProjectInfo::printProjectTab("EnvMeta", "", \%db_val);
    print "</p></div>\n"; 

    # tab 5
    print "<div id='tab5'><p>\n"; 
    ProjectInfo::printProjectTab("HostMeta", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_body_sites', '');
    ProjectInfo::printProjectSetValTab ('project_info_body_products', '');
    print "</p></div>\n";

    # tab 6 
    print "<div id='tab6'><p>\n"; 
    ProjectInfo::printProjectTab("OrganMeta", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_cell_arrangement', '');
    ProjectInfo::printProjectSetValTab ('project_info_diseases', ''); 
    ProjectInfo::printProjectSetValTab ('project_info_habitat', '');
    ProjectInfo::printProjectSetValTab ('project_info_metabolism', '');
    ProjectInfo::printProjectSetValTab ('project_info_phenotypes', '');
    ProjectInfo::printProjectSetValTab ('project_info_energy_source', '');
    ProjectInfo::printProjectCyanoSubTab ('project_info_cyano_metadata', '', \%db_val);
    print "</p></div>\n";
 
    # tab 7
    if ($isAdmin eq 'Yes') {
	print "<div id='tab7'><p>\n"; 
	ProjectInfo::printProjectTab("HMP", "", \%db_val); 
	print "</p></div>\n";

        print "<div id='tab8'><p>\n";
	ProjectInfo::printProjectTab("JGI", "", \%db_val);
	ProjectInfo::printProjectSetValTab ('project_info_jgi_url', ''); 
        print "</p></div>\n"; 
    }
    TabHTML::printTabDivEnd();
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:dbNewGoldProject" value="Add Project" class="medbutton" />';

    printHomeLink(); 
 
    print end_form();
} 
 
######################################################################### 
# UpdateGoldProject 
######################################################################### 
sub UpdateGoldProject { 
    my ($project_oid) = @_;

    my %db_val = ProjectInfo::SelectProjectInfo($project_oid);
 
    my $url=url(); 
    print start_form(-name=>'updateProject',-method=>'post',action=>"$url"); 

    print "<h2>Update Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid);
#    if ( param1('next_page') ) {
#	print hiddenVar('next_page', param1('next_page'));
#    }

    # save parameters
    for my $p ('page', 'web_code', 'category_page_no',
	       'goldproj_orderby', 'project_desc') {
	if ( param1($p) ) {
	    print hiddenVar($p, param1($p));
	}
	elsif ( $p eq 'web_code' ) {
	    print hiddenVar('web_code', 0);
	}
    }

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid); 

    # tab view 
    my @tabIndex;
    TabHTML::printTabAPILinks("updateProject"); 
    if ($isAdmin eq 'Yes') {
	@tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", "#tab5", "#tab6", "#tab7", "#tab8" ); 
    } else {
	@tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", "#tab5", "#tab6"); 
    } 

    my @tabNames; 
    if ($isAdmin eq 'Yes') {
	@tabNames = 
	    ( "Organism Info (*)", "Project Info (*)", 
	      "Sequencing Info", "Environment Metadata",
	      "Host Metadata", "Organism Metadata", "HMP Metadata", "JGI Info"); 
    } else {
	@tabNames = 
	    ( "Organism Info (*)", "Project Info (*)", 
	      "Sequencing Info", "Environment Metadata",
	      "Host Metadata", "Organism Metadata"); 
    } 
    TabHTML::printTabDiv( "updateProject", \@tabIndex, \@tabNames ); 
    
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    ProjectInfo::printProjectTab("Organism", $project_oid, \%db_val); 
    print "</p></div>\n"; 
 
    # tab 2 
    print "<div id='tab2'><p>\n"; 
    ProjectInfo::printProjectTab("Project", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_project_relevance', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_data_links', $project_oid);
    print "</p></div>\n";
 
    # tab 3 
    print "<div id='tab3'><p>\n"; 
    ProjectInfo::printProjectTab("Sequencing", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_seq_method', $project_oid);
    print "</p></div>\n";
 
    # tab 4
    print "<div id='tab4'><p>\n";
    ProjectInfo::printProjectTab("EnvMeta", $project_oid, \%db_val);
    print "</p></div>\n"; 

    # tab 5
    print "<div id='tab5'><p>\n"; 
    ProjectInfo::printProjectTab("HostMeta", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_body_sites', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_body_products', $project_oid);
    print "</p></div>\n"; 

    # tab 6 
    print "<div id='tab6'><p>\n"; 
    ProjectInfo::printProjectTab("OrganMeta", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_cell_arrangement', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_diseases', $project_oid); 
    ProjectInfo::printProjectSetValTab ('project_info_habitat', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_metabolism', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_phenotypes', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_energy_source', $project_oid);
    ProjectInfo::printProjectCyanoSubTab ('project_info_cyano_metadata', $project_oid, \%db_val);
    print "</p></div>\n"; 
 
    # tab 7
    if ($isAdmin eq 'Yes') {
	print "<div id='tab7'><p>\n"; 
	ProjectInfo::printProjectTab("HMP", $project_oid, \%db_val); 
	print "</p></div>\n";

	print "<div id='tab8'><p>\n"; 
	ProjectInfo::printProjectTab("JGI", $project_oid, \%db_val); 
	ProjectInfo::printProjectSetValTab ('project_info_jgi_url', $project_oid);
	print "</p></div>\n";
    }
    TabHTML::printTabDivEnd();
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:dbUpdateGoldProject" value="Update Project" class="meddefbutton" />';
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:showCategory" value="Cancel" class="smbutton" />';

    printHomeLink(); 
 
    print end_form();
} 
 

############################################################################# 
# DeleteGoldProject - delete project 
############################################################################# 
sub DeleteGoldProject { 
    my $url=url(); 
 
    print start_form(-name=>'deleteProject',-method=>'post',action=>"$url"); 
 
    my $project_oid = param1('project_oid'); 
    if ( blankStr($project_oid) ) { 
        printError("No project has been selected."); 
        print end_form(); 
        return; 
    } 
 
    print "<h2>Delete Project $project_oid</h2>\n"; 
    print hidden('project_oid', $project_oid); 
#    if ( param1('next_page') ) {
#	print hiddenVar('next_page', param1('next_page'));
#    }

    # save parameters
    for my $p ('web_code', 'category_page_no',
	       'goldproj_orderby', 'project_desc') {
	if ( defined param1($p) ) {
	    print hiddenVar($p, param1($p));
	}
    }

    # check whether there are any FK 
    # check submission 
    my $cnt = db_getValue("select count(*) from submission where project_info = $project_oid"); 
    if ( $cnt > 0 ) { 
        printError("This project is linked to an IMG submission. You cannot delete a submitted project."); 
        print end_form(); 
        return; 
    } 
 
    # show project, if any 
    ProjectInfo::showProjectSample($project_oid); 
 
    print "<p>Please click the Delete button to confirm the deletion, or cancel the deletion.</p>\n"; 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:dbDeleteGoldProject" value="Delete" class="smdefbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:showCategory" value="Cancel" class="smbutton" />'; 
 
    print "<p>\n"; 
    printHomeLink(); 
 
    print end_form(); 
} 


#########################################################################
# CopyGoldProject 
######################################################################### 
sub CopyGoldProject { 
    my ($project_oid) = @_;
 
    # check project oid 
    if ( blankStr($project_oid) ) {
        printError("No project has been selected."); 
        print end_form(); 
        return;
    }
 
    my $gold_stamp_id = db_getValue("select gold_stamp_id from project_info where project_oid = $project_oid");
 
    my $url=url(); 
    print start_form(-name=>'copyProject',-method=>'post',action=>"$url");
 
    print "<h2>Copy Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid); 
 
    my $web_code = param1('web_code');
    if ( length($web_code) > 0 ) {
	print hiddenVar('web_code', $web_code);
    }

    # get new project oid 
    my $new_proj_oid = 0;
    if ( $gold_stamp_id ) {
        # GOLD project
        # use higher range project oid for GOLD entry
        $new_proj_oid = db_findMaxID('project_info', 'project_oid') + 1;
    } 
    else { 
        $new_proj_oid = ProjectInfo::getNewProjectId();
    }

    if ( $new_proj_oid <= 0 ) {
        # error checking -- this shouldn't happen 
        printError("Incorrect new project ID value: $new_proj_oid"); 
        print end_form(); 
        return;
    } 
 
    print "<h3>New Project ID: $new_proj_oid</h3>\n";
 
#    $new_proj_oid = 2; 
    my @sqlList = ();
 
    my $contact_oid = getContactOid();
    my $ins = "insert into project_info(project_oid, contact_oid, add_date";
    my $sel = "select $new_proj_oid, $contact_oid, sysdate";
 
    # get Project_Info definition 
    my $def_project = def_Project_Info();
    my @attrs = @{$def_project->{attrs}};
    for my $attr ( @attrs ) { 
        if ( $attr->{name} eq 'pmo_project_id' ||
                $attr->{name} eq 'jgi_dir_number' ||
                $attr->{name} eq 'hmp_id' ||
                $attr->{name} eq 'ncbi_project_id' ) { 
	    # skip pmo_project_id etc.
	    next;
	}
        elsif ( $attr->{name} =~ /^ncbi\_/ ) {
            # ok - NCBI taxon info
        } 
        elsif ( ! $attr->{can_edit} ) { 
            # skip non-editable attribute
            next;
        }
 
        $ins .= ", " . $attr->{name}; 
        $sel .= ", " . $attr->{name};
    } 
 
    my $sql = $ins . ") " . $sel . " from project_info " .
        "where project_oid = " . $project_oid;
    push @sqlList, ( $sql ); 
 
    # set-valued attributes
    my @tables = getProjectAuxTables();
    for my $tname ( @tables ) { 

        my $def_aux = def_Class($tname); 
        if ( ! $def_aux ) { 
            next; 
        } 

	if ($tname eq 'project_info_cyano_metadata') {
	    next;	
	}
        my @aux_attrs = @{$def_aux->{attrs}};
        $ins = "insert into $tname (" . $def_aux->{id};
        $sel = "select $new_proj_oid";
        for my $attr2 ( @aux_attrs ) {
            if ( $attr2->{name} eq $def_aux->{id} ) {
                next; 
            } 
 
            $ins .= ", " . $attr2->{name};
            $sel .= ", " . $attr2->{name}; 
        } 
        $sql = $ins . ") " . $sel . " from " . $tname .
            " where " . $def_aux->{id} . " = " . $project_oid;
        push @sqlList, ( $sql );
    } 

# add cyano data to copy
    my $tname = 'project_info_cyano_metadata'; 
    my $def_aux = def_Class($tname);
    
    my @aux_attrs = @{$def_aux->{attrs}};
    $ins = "insert into $tname (" . $def_aux->{id};
    $sel = "select $new_proj_oid";
    for my $attr2 ( @aux_attrs ) {
	if ( $attr2->{name} eq $def_aux->{id} ) {
	    next; 
	} 
	
	$ins .= ", " . $attr2->{name};
	$sel .= ", " . $attr2->{name}; 
    } 
    $sql = $ins . ") " . $sel . " from " . $tname .
	" where " . $def_aux->{id} . " = " . $project_oid;

    push @sqlList, ( $sql );
    
    db_sqlTrans(\@sqlList); 
 
    print "<p>New project has been created by copying project " .
        $project_oid . ".</p>\n";
 
    print "<p>Note: Samples of this project (if any) are not copied.</p>\n";
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:showCategory" value="OK" class="smbutton" />'; 
 
    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
} 


############################################################################# 
# SearchByID - search by GOLD Stamp ID
############################################################################# 
sub SearchByID {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h3>Search GOLD Projects by GOLD Stamp ID</h3>\n"; 

    print "<p>Enter a GOLD Stamp ID or select from the list.</p>\n";

    print "<h4>Enter a GOLD Stamp ID</h4>\n";
    print "<input type='text' name='gold_stamp_id_1' value='" .
	"' size='10' maxLength='10'/>\n";

    print "<h4>Select a GOLD Stamp ID</h4>\n";
    my @gold_ids = db_getValues("select gold_stamp_id from project_info order by gold_stamp_id");
    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
    for my $id0 ( @gold_ids ) {
        print "    <option value='$id0' >$id0</option>\n";
    }
    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:searchIdResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 

}


#########################################################################
# ShowSearchIdResultPage
# 
# admin does not have condition on contact_oid
##########################################################################
sub ShowSearchIdResultPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Stamp ID Result Page</h2>\n"; 

#    print hiddenVar('next_page', 'searchIdResult');

    my $contact_oid = getContactOid();

    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    my $cond = "";
    my $gold_stamp_id_1 = param1('gold_stamp_id_1');
    $gold_stamp_id_1 =~ s/'/''/g;   # replace ' with '', just in case
    my $gold_stamp_id_2 = param1('gold_stamp_id_2');
    $gold_stamp_id_2 =~ s/'/''/g;   # replace ' with '', just in case

    if ( ! blankStr($gold_stamp_id_1) ) {
	$cond = " where lower(p.gold_stamp_id) in ( '" .
	    lc($gold_stamp_id_1) . "'";
	if ( ! blankStr($gold_stamp_id_2) ) {
	    $cond .= ", '" . lc($gold_stamp_id_2) . "'";
	}
	$cond .= " )";
    }
    elsif ( ! blankStr($gold_stamp_id_2) ) {
	$cond = " where lower(p.gold_stamp_id) = '" .
	    lc($gold_stamp_id_2) . "'";
    }
    else {
        printError("Please enter or select a GOLD Stamp ID."); 
        print end_form(); 
        return;
    }

    if ( $isAdmin ne 'Yes' ) {
        $cond = " and p.contact_oid = $contact_oid";
    } 
 
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
            $cond
        }; 
 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>ER Project ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Phylogeny</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 > $default_max_row ) {
            last;   # just in case
        } 
 
        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 

    print "<font color='blue'>(Number of projects found: $cnt2)</font>\n";

    $cur->finish(); 
    $dbh->disconnect(); 

    if ( $cnt2 > 0 ) {
	printGoldProjButtons();

	printAdditionalProjSection();
    }

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}


############################################################################# 
# AdvancedSearch - advanced search
############################################################################# 
sub AdvancedSearch {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Projects by Query Condition</h2>\n"; 

    print "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    # gold stamp ID 
    ProjectInfo::printFilterCond('GOLD Stamp ID', 'gold_filter:gold_stamp_id',
				 'text', '', '', '');

    # common_name
    ProjectInfo::printFilterCond('Common Name', 'gold_filter:common_name', 
				 'text', '', '', '');

    # ncbi_project_id
    ProjectInfo::printFilterCond('NCBI Project ID',
				 'gold_filter:ncbi_project_id', 
				 'number', '', '', '');

    # ncbi_project_name 
    ProjectInfo::printFilterCond('NCBI Project Name',
				 'gold_filter:ncbi_project_name', 
				 'text', '', '', '');

    # gold web page code 
    ProjectInfo::printFilterCond('GOLD Web Page Code', 'gold_filter:web_page_code',
				 'select', 'query',
				 "select term_oid+1, description " .
				 "from web_page_codecv order by term_oid",
				 '');

    # project type 
    ProjectInfo::printFilterCond('Project Type', 'gold_filter:project_type', 
				 'select', 'query', 
				 'select cv_term from project_typecv order by cv_term', 
				 '');
 
    # project status 
    ProjectInfo::printFilterCond('Project Status', 'gold_filter:project_status', 
				 'select', 'query', 
				 'select cv_term from project_statuscv order by cv_term', 
				 '');
 
    # domain 
    ProjectInfo::printFilterCond('Domain', 'gold_filter:domain',
				 'select', 'list',
				 'ARCHAEAL|BACTERIAL|EUKARYAL|PLASMID|VIRUS',
				 '');
 
    # seq status 
    ProjectInfo::printFilterCond('Sequencing Status', 'gold_filter:seq_status',
				 'select', 'query', 
				 'select cv_term from seq_statuscv order by cv_term',
				 '');
 
    # seq method 
    ProjectInfo::printFilterCond('Sequencing Method', 'gold_filter:seq_method',
				 'select', 'query',
				 'select cv_term from seq_methodcv order by cv_term',
				 '');
 
    # availability: Proprietary, Public
    ProjectInfo::printFilterCond('Availability', 'gold_filter:availability', 
				 'select', 'list', 'Proprietary|Public', '');

    # phylogeny 
    ProjectInfo::printFilterCond('Phylogeny', 'gold_filter:phylogeny', 
				 'select', 'query', 
				 'select cv_term from phylogenycv order by cv_term', 
				 '');

    # project display name 
    ProjectInfo::printFilterCond('Project Display Name', 'gold_filter:display_name', 
				 'text', '', '', '');
 
    # genus 
    ProjectInfo::printFilterCond('Genus', 'gold_filter:genus', 
				 'text', '', '', '');
 
    # species 
    ProjectInfo::printFilterCond('Species', 'gold_filter:species', 
				 'text', '', '', '');
 
    # common_name 
    ProjectInfo::printFilterCond('Common Name', 'gold_filter:common_name', 
				 'text', '', '', '');
 
    # add date
    ProjectInfo::printFilterCond('Add Date', 'gold_filter:add_date',
				 'date', '', '', '');
 
    # mod date
    ProjectInfo::printFilterCond('Last Mod Date', 'gold_filter:mod_date',
				 'date', '', '', '');

#    print "<p>GOLD Stamp ID:\n";
#    print nbsp(3);
#    print "<input type='text' name='gold_stamp_id' value='" .
#	"' size='10' maxLength='10'/>\n";

#    print "<h4>Select a GOLD Stamp ID</h4>\n";
#    my @gold_ids = db_getValues("select gold_stamp_id from project_info order by gold_stamp_id");
#    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
#    for my $id0 ( @gold_ids ) {
#        print "    <option value='$id0' >$id0</option>\n";
#    }
#    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:advSearchResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}


######################################################################### 
# goldFilterParams - get all gold project filter parameters 
######################################################################### 
sub goldFilterParams { 
    my @all_params = ( 'gold_filter:project_type', 
                       'gold_filter:project_status', 
                       'gold_filter:domain', 
                       'gold_filter:seq_status', 
                       'gold_filter:seq_method', 
                       'gold_filter:availability', 
                       'gold_filter:phylogeny', 
                       'gold_filter:display_name', 
                       'gold_filter:genus', 
                       'gold_filter:species', 
                       'gold_filter:common_name', 
                       'gold_filter:ncbi_project_name', 
                       'gold_filter:ncbi_project_id', 
                       'gold_filter:gold_stamp_id', 
                       'gold_filter:add_date', 
                       'gold_filter:mod_date', 
                       'gold_filter:web_page_code' ); 
 
    return @all_params; 
} 


######################################################################### 
# goldFilterCondition 
########################################################################## 
sub goldFilterCondition { 
    my $cond = ""; 
 
    # filter condition ??? 
    my @all_params = goldFilterParams(); 
    for my $p0 ( @all_params ) { 
        my $cond1 = ""; 
	my ($tag, $fld_name) = split(/\:/, $p0); 

	my $op_name = $p0 . ":op";
	my $op1 = "=";
	if ( param1($op_name) ) {
	    $op1 = param1($op_name);
	}
	if ( $op1 eq 'is null' || $op1 eq 'is not null' ) {
	    $cond1 ="p.$fld_name $op1";
	}
        elsif ( param1($p0) ) { 
            my @vals = split(/\,/, param1($p0)); 
 
            if ( lc($fld_name) eq 'display_name' ||
                 lc($fld_name) eq 'genus' || 
                 lc($fld_name) eq 'species' ||
                 lc($fld_name) eq 'common_name' ||
                 lc($fld_name) eq 'ncbi_project_name' ||
		 lc($fld_name) eq 'gold_stamp_id' ) {
                my $s1 = param1($p0); 
                if ( length($s1) > 0 ) { 
                    $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
                } 
            } 
            elsif ( lc($fld_name) eq 'ncbi_project_id' ) {
                my $s1 = param1($p0); 
                if ( length($s1) > 0 && isInt($s1) ) { 
                    $cond1 = "p.$fld_name $op1 $s1";
                } 
            } 
            elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) { 
                    $cond1 = "p.project_oid in (select pism.project_oid " .
                        "from project_info_seq_method pism " .
                        "where lower(pism.$fld_name) = '" . lc($s1) . "')";
                } 
            }
            elsif ( lc($fld_name) eq 'web_page_code' ) {
                for my $s2 ( @vals ) {
                    my $i = $s2 - 1;
                    if ( $i >= 0 ) { 
                        $cond1 = "p.$fld_name = $i";
                    } 
                } 
            } 
            elsif ( lc($fld_name) eq 'add_date' ||
                    lc($fld_name) eq 'mod_date' ) {
                my $op_name = $p0 . ":op"; 
                my $op1 = param1($op_name);
                my $d1 = param1($p0);
                if ( !blankStr($op1) && !blankStr($d1) ) {
                    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
                } 
            }
            else { 
                for my $s2 ( @vals ) { 
                    $s2 =~ s/'/''/g;   # replace ' with ''
                    if ( blankStr($cond1) ) { 
                        $cond1 = "p.$fld_name in ('" . $s2 . "'";
                    } 
                    else { 
                        $cond1 .= ", '" . $s2 . "'";
                    }
                } 
 
                if ( ! blankStr($cond1) ) {
                    $cond1 .= ")"; 
                } 
            } 
        } 
 
        if ( ! blankStr($cond1) ) { 
            if ( blankStr($cond) ) { 
                $cond = $cond1; 
            } 
            else {
                $cond .= " and " . $cond1;
            } 
        } 
    }  # end for p0 
 
    return $cond; 
} 


##########################################################################
# selectedGoldProjCount
##########################################################################
sub selectedGoldProjCount { 
    my ($contact_oid, $filter_cond) = @_; 
 
    my $dbh = Connect_IMG();
 
    my $sql = "select count(*) from project_info p"; 
    if ( $contact_oid ) { 
        $sql .= " where p.contact_oid = $contact_oid"; 
        if ( ! blankStr($filter_cond) ) { 
            $sql .= " and " . $filter_cond; 
        } 
    } 
    else { 
        if ( ! blankStr($filter_cond) ) { 
            $sql .= " where " . $filter_cond; 
        } 
    } 
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 


########################################################################## 
# ShowAdvSearchResultPage
########################################################################## 
sub ShowAdvSearchResultPage{
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Search Result</h2>\n"; 

    my $filter_cond = goldFilterCondition(); 
 
    my $select_cnt = 0;
    if ( $isAdmin eq 'Yes' ) {
        $select_cnt = selectedGoldProjCount( '', $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 
    else { 
        $select_cnt = selectedGoldProjCount($contact_oid, $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 

    if ( $select_cnt == 0 ) {
	print "<p>\n";
	printHomeLink();

	print end_form(); 
	return;
    }

    printGoldProjButtons();

    # show results
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
        }; 

    if ( ! blankStr($filter_cond) ) {
	$sql .= " where " . $filter_cond;
    }

    print "<p>\n";
webLog("$sql\n");    
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>ER Project ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Phylogeny</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";

        if ( $cnt2 >= 1000 ) {
            last;
        } 
    }
    print "</table>\n"; 

    if ( $cnt2 < $select_cnt ) {
	print "<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    }
    else {
	print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    }

    $cur->finish(); 
    $dbh->disconnect(); 

    printGoldProjButtons();

    printAdditionalProjSection();

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}


########################################################################## 
# printAdditionalProjSection
########################################################################## 
sub printAdditionalProjSection {
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 

    # allow admin to change IMG contact 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
        print "<h3>Change IMG Contact or Grant Edit Privilege for Selected Project</h3>\n"; 
        print "<p>New IMG Contact:\n"; 
        print "<select name='new_contact' class='img' size='1'>\n"; 
 
        my $sql2 = "select contact_oid, username from contact order by username, contact_oid"; 
        my $dbh2 = Connect_IMG_Contact();
webLog("$sql2\n");        
        my $cur2=$dbh2->prepare($sql2); 
        $cur2->execute(); 
 
        for (my $j2 = 0; $j2 <= 10000; $j2++) { 
            my ($id2, $name2) = $cur2->fetchrow_array(); 
            if ( ! $id2 ) { 
                last; 
            } 
 
            print "    <option value='$id2'"; 
            if ( $contact_oid == $id2 ) { 
                print " selected "; 
            } 
            print ">$name2 (OID: $id2)</option>\n"; 
        } 
        print "</select>\n"; 
        $cur2->finish(); 
        $dbh2->disconnect(); 
 
        print nbsp(3); 
        print '<input type="submit" name="_section_GoldProj:changeContact" value="Change Contact" class="medbutton" />'; 
 
        print "<p>\n"; 
        print '<input type="submit" name="_section_GoldProj:grantEdit" value="Grant Edit Privilege" class="medbutton" />'; 
    } 

    # allow admin to change GOLD id 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
 
        print "<h3>Assign or Change GOLD Stamp ID for Selected Project</h3>\n";
        print "<p>Note: GCAT ID and HMP ID (for HMP projects only) will be automatically assigned when the selected project gets a GOLD Stamp ID. GCAT ID and HMP ID (if any) will be automatically deleted when the project GOLD Stamp ID is deleted.</p>\n"; 
	print "<br/>\n";
        print '<input type="submit" name="_section_GoldProj:setGoldStampId" value="Assign GOLD Stamp ID" class="medbutton" />';
        print "&nbsp; \n";
        print '<input type="submit" name="_section_GoldProj:delGoldStampId" value="Delete GOLD Stamp ID" class="medbutton" />';
    } 
 
    # allow admin to merge projects
    if ( $isAdmin eq 'Yes' ) {
	print hr;
	print "<h3>Merge Projects</h3>\n"; 
	print "<p>Merge the selected project with another project.</p>\n";
	print "<p>\n";
	print '<input type="submit" name="_section_GoldProj:mergeGoldProj" value="Merge Projects" class="medbutton" />';
    }
}


######################################################################### 
# PrintMergeGoldProjPage
######################################################################### 
sub PrintMergeGoldProjPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Merge Projects</h2>\n"; 

    my $project_oid = param1('project_oid');
    if ( ! $project_oid ) {
        printError("Please select a project first."); 
        print end_form(); 
        return;
    }
    print "<h3>Selected Project: $project_oid</h3>\n";
    print hiddenVar('project_oid', $project_oid);

    print "<p>Merge the selected project with the following project:</p>\n"; 
 
    my $sql2 = "select project_oid, display_name from project_info " . 
        "order by project_oid"; 
    my $dbh2 = Connect_IMG (); 
webLog("$sql2\n");    
    my $cur2=$dbh2->prepare($sql2); 
    $cur2->execute(); 
 
    print "<select name='merged_project' class='img' size='1'>\n"; 
    for (my $j2 = 0; $j2 <= 100000; $j2++) {
        my ($id2, $name2) = $cur2->fetchrow_array();
        if ( ! $id2 ) { 
            last; 
        } 
 
	if ( $id2 == $project_oid ) {
	    next;
	}

        print "    <option value='$id2'>";
        print "$id2 - $name2</option>\n";
    } 
    $cur2->finish(); 
    $dbh2->disconnect(); 
    print "</select>\n"; 
 
    print "<p>\n";
    print '<input type="submit" name="_section_GoldProj:mergeProjects" value="Merge Projects" class="medbutton" />';

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
} 
 

##########################################################################
# FilterGoldProject - set filter on displaying projects
#             so that there won't be too many to display
##########################################################################
sub FilterGoldProject { 
    my $url=url(); 
 
    print start_form(-name=>'filterProject',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid); 
    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h3>Set GOLD Project Filter for $uname</h3>\n";
 
    # only show my projects?
    my $only_my_project = getSessionParam('proj_filter:only_my_project');
    print "<p>Only display my projects? \n"; 
    print "<input type='checkbox' name='proj_filter:only_my_project' value='1' ";
    if ( $only_my_project ) {
	print " checked "; 
    } 
    print ">Yes\n";
    print "</p>\n";

    # max display 
    my $max_display = getSessionParam('proj_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = "$default_max_row"; 
    } 
    print "<p>Maximal Rows of Display:\n"; 
    print nbsp(3); 
    print "<select name='proj_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
                   '300', '500', '800', '1000', '2000',
                   '3000', '4000', '5000', '10000' ) {
        print "    <option value='$cnt0' ";
        if ( $cnt0 eq $max_display ) {
            print " selected ";
        }
        print ">$cnt0</option>\n";
    }
    print "</select>\n";
 
    my $def_project = def_Project_Info();
    my @attrs = @{$def_project->{attrs}};

    my @tabs =  ( 'Organism', 'HMP', 'Project', 'Sequencing',
		  'EnvMeta', 'HostMeta', 'OrganMeta' );

    # tab display label
    my %tab_display = (
		       'Organism' => 'Organism Info',
		       'HMP' => 'HMP Metadata',
		       'Metagenome' => 'Metagenome Info',
		       'Project' => 'Project Info',
		       'Sequencing' => 'Sequencing Info',
		       'EnvMeta' => 'Environment Metadata',
		       'HostMeta' => 'Host Metadata',
		       'OrganMeta' => 'Organism Metadata' 
		       );

    if ( $isAdmin eq 'Yes' ) {
	push @tabs, ( 'JGI' );
	$tab_display{'JGI'} = 'JGI Info';
    }

    my @aux_tables = getProjectAuxTables();

    print "<table class='img' border='1'>\n";
    for my $tab ( @tabs ) {
	my $tab_label = $tab;
	if ( $tab_display{$tab} ) {
	    $tab_label = $tab_display{$tab};
	}

        print "<tr class='img' >\n";
        print "  <th class='subhead' colspan='3' align='right' bgcolor='lightblue'>" .
            "<font color='darkblue'>" . $tab_label . "</font></th>\n";
        print "</tr>\n";

	for my $k ( @attrs ) { 
	    if ( $k->{tab} ne $tab ) {
		next;
	    }
	    elsif ( ! $k->{filter_cond} ) {
		next;
	    }

	    # show filter condition
	    my $filter_name = "proj_filter:" . $k->{name};
	    my $ui_type = 'text';
	    my $select_type = '';
	    my $select_method = '';

	    if ( $k->{name} eq 'contact_oid' ) {
		# special for contact_oid
		my $perm_cond = " ";
		if ( $isAdmin ne 'Yes' ) {
		    $perm_cond = " where p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid)"; 
		}

		$ui_type = 'select';
		$select_type = 'query';
		$select_method = qq{
		    select c.contact_oid, c.username
			from contact c
			where c.contact_oid in
			(select p.contact_oid from project_info p $perm_cond)
			order by 2
		    };
	    }
	    elsif ( $k->{data_type} eq 'cv' ) {
		$ui_type = 'select';
		$select_type = 'query';
		$select_method = $k->{cv_query};
	    }
	    elsif ( $k->{data_type} eq 'list' ) {
		$ui_type = 'select';
		$select_type = 'list';
		$select_method = $k->{list_values};
	    }
	    elsif ( $k->{data_type} eq 'int' ||
		    $k->{data_type} eq 'number' ) {
		$ui_type = 'number';
	    }
	    elsif ( $k->{data_type} eq 'date' ) {
		$ui_type = 'date';
	    }
	    ProjectInfo::printFilterCondRow($k->{display_name}, $filter_name,
			       $ui_type, $select_type, $select_method,
			       getSessionParam($filter_name));
	}  # end for my k

#print cyano metadata
	if ($tab_label eq 'Organism Metadata') {
	    my $def_aux = def_Class('project_info_cyano_metadata');
	    my @aux_attrs = @{$def_aux->{attrs}};
	    for my $k4 ( @aux_attrs ) {
	    my $filter_name = "proj_filter:" . $k4->{name} . ":Cyano";
	    my $ui_type = 'text';
	    my $select_type = '';
	    my $select_method = '';
		if ( ! $k4->{filter_cond} ) {
		    next;
		}
		if ( $k4->{data_type} eq 'cv' ) {
		$ui_type = 'select';
		$select_type = 'query';
		$select_method = $k4->{cv_query};
	    }
	    elsif ( $k4->{data_type} eq 'list' ) {
		$ui_type = 'select';
		$select_type = 'list';
		$select_method = $k4->{list_values};
	    }
	    elsif ( $k4->{data_type} eq 'int' ||
		    $k4->{data_type} eq 'number' ) {
		$ui_type = 'number';
	    }
	    elsif ( $k4->{data_type} eq 'date' ) {
		$ui_type = 'date';
	    }
	    ProjectInfo::printFilterCondRow($k4->{display_name}, $filter_name,
			       $ui_type, $select_type, $select_method,
			       getSessionParam($filter_name));
	    }
	}

	for my $k2 ( @aux_tables ) {
	    my $def_aux = def_Class($k2);
	    if ( ! $def_aux ) {
		next;
	    }
	    if ( $def_aux->{tab} ne $tab ) {
		next;
	    }
	    if ($k2 eq 'project_info_cyano_metadata') {
		next;
	    }


	    # special condition for body_sites
	    if ( $k2 eq 'project_info_body_sites' ) {
		for my $typ ( 'Sample Body Site', 'Sample Body Subsite' ) {
		    my $filter_name = "proj_filter:" . $k2 . ":" . $typ;
		    my $filter_disp = $typ;
		    my $filter_sql = '';		    
		    if ($typ eq 'Sample Body Site') {
			$filter_sql = qq{
			    select distinct cv_term
				from body_sitecv
			    };
		    } elsif  ($typ eq 'Sample Body Subsite') {
			$filter_sql = qq{
			    select distinct cv_term
				from body_subsitecv
			    };
		    }
		    # $filter_sql = "select cv_term from db_namecv";
		    ProjectInfo::printFilterCondRow($filter_disp,
						    $filter_name,
						    'select', 'query',
						    $filter_sql,
						    getSessionParam($filter_name));
		}
	    }


	    # special condition for data links
	    if ( $k2 eq 'project_info_data_links' ) {
		for my $typ ( 'Funding', 'Seq Center' ) {
		    my $filter_name = "proj_filter:" . $k2 . ":" . $typ;
		    my $filter_disp = "Data Links: " . $typ;
		    my $filter_sql = qq{
			select distinct db_name
			    from project_info_data_links
			    where link_type = '$typ'
			    and db_name is not null
			    order by 1
			};
		    # $filter_sql = "select cv_term from db_namecv";
		    ProjectInfo::printFilterCondRow($filter_disp,
						    $filter_name,
						    'select', 'query',
						    $filter_sql,
				   getSessionParam($filter_name));
		}
	    }

	    my @aux_attrs = @{$def_aux->{attrs}};
	    for my $k3 ( @aux_attrs ) {
		if ( ! $k3->{filter_cond} ) {
		    next;
		}
		my $filter_name = "proj_filter:" . $k3->{name};
		ProjectInfo::printFilterCondRow($k3->{display_name},
						$filter_name,
						'select', 'query',
						$k3->{cv_query},
				   getSessionParam($filter_name));
	    }
	}  # end for my k2
    } # end for my tab
    print "</table>\n";

    # relevance
#    printFilterCond('Project Relevance', 'proj_filter:project_relevance',
#                    'select', 'query',
#                    'select cv_term from relevancecv order by cv_term',
#                    getSessionParam('proj_filter:project_relevance'));
 
    # habitat
#    my $def_project_habitat = def_project_info_habitat();
#    my $def_attr = $def_project_habitat->findAttr('habitat');
#    if ( $def_attr ) {
#	my $filter_name = "proj_filter:" . $def_attr->{name};
#	printFilterCond($def_attr->{display_name}, $filter_name,
#			'select', 'query',
#			$def_attr->{cv_query},
#			getSessionParam($filter_name));
#    }

    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:applyGoldProjectFilter" value="Apply Project Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print end_form();

    return;

    # gold stamp ID
    printFilterCond('GOLD Stamp ID', 'proj_filter:gold_stamp_id',
                    'select', 'query', 
                    'select distinct gold_stamp_id from project_info order by gold_stamp_id', 
                    getSessionParam('proj_filter:gold_stamp_id'));

    # ncbi_project_id
    printFilterCond('NCBI Project ID', 'proj_filter:ncbi_project_id', 
                    'number', '', '', 
                    getSessionParam('proj_filter:ncbi_project_id'));

    # ncbi_project_name
    printFilterCond('NCBI Project Name', 'proj_filter:ncbi_project_name', 
                    'text', '', '', 
                    getSessionParam('proj_filter:ncbi_project_name')); 
 
    # gold web page code 
    printFilterCond('GOLD Web Page Code', 'proj_filter:web_page_code',
                    'select', 'query', 
                    'select term_oid+1, description from web_page_codecv where term_oid < 4 order by term_oid', 
                    getSessionParam('proj_filter:web_page_code'));
 
    # project type
    printFilterCond('Project Type', 'proj_filter:project_type',
                    'select', 'query', 
                    'select cv_term from project_typecv order by cv_term',
                    getSessionParam('proj_filter:project_type'));
 
    # project status
    printFilterCond('Project Status', 'proj_filter:project_status',
                    'select', 'query',
                    'select cv_term from project_statuscv order by cv_term',
                    getSessionParam('proj_filter:project_status'));
 
    # domain 
    printFilterCond('Domain', 'proj_filter:domain',
                    'select', 'list', 'ARCHAEAL|BACTERIAL|EUKARYAL|PLASMID|VIRUS',
                    getSessionParam('proj_filter:domain'));
 
    # seq status
    printFilterCond('Sequencing Status', 'proj_filter:seq_status',
                    'select', 'query',
                    'select cv_term from seq_statuscv order by cv_term',
                    getSessionParam('proj_filter:seq_status')); 
 
    # seq method 
    printFilterCond('Sequencing Method', 'proj_filter:seq_method',
                    'select', 'query', 
                    'select cv_term from seq_methodcv order by cv_term',
                    getSessionParam('proj_filter:seq_method')); 
 
    # availability: Proprietary, Public 
    printFilterCond('Availability', 'proj_filter:availability',
                    'select', 'list', 'Proprietary|Public',
                    getSessionParam('proj_filter:availability'));
 
    # phylogeny 
    printFilterCond('Phylogeny', 'proj_filter:phylogeny', 
                    'select', 'query', 
                    'select cv_term from phylogenycv order by cv_term',
                    getSessionParam('proj_filter:phylogeny'));
 
    # project display name 
    printFilterCond('Project Display Name', 'proj_filter:display_name', 
                    'text', '', '', 
                    getSessionParam('proj_filter:display_name'));
 
    # genus 
    printFilterCond('Genus', 'proj_filter:genus', 
                    'text', '', '', 
                    getSessionParam('proj_filter:genus'));
 
    # species 
    printFilterCond('Species', 'proj_filter:species',
                    'text', '', '', 
                    getSessionParam('proj_filter:species')); 
    # common_name
    printFilterCond('Common Name', 'proj_filter:common_name', 
                    'text', '', '', 
                    getSessionParam('proj_filter:common_name')); 

    # relevance
    printFilterCond('Project Relevance', 'proj_filter:project_relevance',
                    'select', 'query',
                    'select cv_term from relevancecv order by cv_term',
                    getSessionParam('proj_filter:project_relevance'));
 
    # habitat
#    my $def_project_habitat = def_project_info_habitat();
#    my $def_attr = $def_project_habitat->findAttr('habitat');
#    if ( $def_attr ) {
#	my $filter_name = "proj_filter:" . $def_attr->{name};
#	printFilterCond($def_attr->{display_name}, $filter_name,
#			'select', 'query',
#			$def_attr->{cv_query},
#			getSessionParam($filter_name));
#    }

    # add date 
    printFilterCond('Add Date', 'proj_filter:add_date',
                    'date', '', '', 
                    getSessionParam('proj_filter:add_date')); 
 
    # mod date 
    printFilterCond('Last Mod Date', 'proj_filter:mod_date', 
                    'date', '', '', 
                    getSessionParam('proj_filter:mod_date')); 
 
    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:applyGoldProjectFilter" value="Apply Project Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print end_form();
} 


###########################################################################
# printFilterCond - print filter condition 
###########################################################################
sub printFilterCond { 
    my ($title, $param_name, $ui_type, $select_type, $select_method,
        $def_val_str) = @_; 

  ProjectInfo::printFilterCond($title, $param_name, $ui_type, $select_type,
			       $select_method, $def_val_str);
}


########################################################################## 
# ApplyGoldProjectFilter - save filter info into database and apply filter 
########################################################################## 
sub ApplyGoldProjectFilter { 
    # only my projects?
    my $only_my_project = param1('proj_filter:only_my_project'); 
    if ( $only_my_project ) {
        setSessionParam('proj_filter:only_my_project', $only_my_project); 
    } 

    # max display 
    my $max_display = param1('proj_filter:max_display'); 
    if ( ! blankStr($max_display) ) { 
        setSessionParam('proj_filter:max_display', $max_display); 
    } 
 
    # show filter selection 
    my @all_params = ProjectInfo::projectFilterParams(); 
    for my $p0 ( @all_params ) { 
#	print "<p>p0: $p0, " . param1($p0) . "</p>\n";

        if ( $p0 eq 'proj_filter:add_date' || 
             $p0 eq 'proj_filter:mod_date' ) { 
            # date 
            my $op_name = $p0 . ":op"; 
            my $op1 = param1($op_name); 
            my $d1 = param1($p0); 
            if ( !blankStr($d1) && !blankStr($op1) ) { 
                if ( ! isDate($d1) ) { 
                    print "<p>Incorrect Date (" . escapeHTML($d1) . 
                        ") -- Filter condition is ignored.</p>\n"; 
                } 
                else { 
                    setSessionParam($op_name, $op1); 
                    setSessionParam($p0, $d1); 
                } 
            } 
            else { 
                # clear condition 
                setSessionParam($op_name, ''); 
                setSessionParam($p0, ''); 
            } 

	    next;
        } 

	# op
	my $op_name = $p0 . ":op"; 
	if ( param1($op_name) ) {
	    setSessionParam($op_name, param1($op_name));
	}
	else {
	    # clear
	    setSessionParam($op_name, ''); 
	}

	# value
        if ( param($p0) ) {
            # filter
            my @options = param($p0);
 
            my $s0 = "";
            for my $s1 ( @options ) {
                if ( blankStr($s0) ) { 
                    $s0 = $s1;
                } 
                else {
                    $s0 .= "," . $s1;
                }
            } 
            setSessionParam($p0, $s0); 
        } 
        else { 
            setSessionParam($p0, ''); 
        } 

	# type?
	my $type_name = $p0 . ":type"; 
	my $type_val = param1($type_name); 
	if ( !blankStr($type_val) ) {
	    setSessionParam($type_name, $type_val);
	}
    }  # end for p0
} 



1;



