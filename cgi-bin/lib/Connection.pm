package Connection;

use strict;
use warnings;
use DBI;

$ENV{'ORACLE_HOME'} = "/jgi/tools/oracle_client/DEFAULT";

# us this to define the database that is used for pangenomes
#


sub Connect_IMG_pangenomes{
        my $dbh=&Connect_IMG_personal;
	my $db='img_mi_v200';
	return ($dbh,$db);

}



sub Connect_IMG_Rfam{
        my $dbh=&Connect_IMG_personal;
	my $db='img_i_v230';
	return ($dbh,$db);

}


#use this to define the database that the operons procedures
# are using
sub Connect_IMG_Operons{
	my $dbh=&Connect_IMG;
	return $dbh;

}
sub Connect_IMG
{
	$ENV{'ORACLE_HOME'} = "/jgi/tools/oracle_client/DEAFULT";
	my $user2 = "konstantin";
	my $pw2 = "konstantin123";
	my $service2 = "imgi.prod";
	my $dsn2 = "dbi:Oracle:$service2";
	my $dbh2 = DBI->connect( $dsn2, $user2, $pw2 );
	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
	$dbh2->{ LongReadLen } = 50000;
	$dbh2->{ LongTruncOk } = 1;
	return $dbh2;
}

sub Connect_IMG_safe
{
	$ENV{'ORACLE_HOME'} = "/jgi/tools/oracle_client/DEFAULT";
	my $user2 = "imgi_v220_ro";
	my $pw2 = "imgi_v220_ro";
	my $service2 = "imgi.prod";
	my $dsn2 = "dbi:Oracle:$service2";
	my $dbh2 = DBI->connect( $dsn2, $user2, $pw2 );
	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
	$dbh2->{ LongReadLen } = 50000;
	$dbh2->{ LongTruncOk } = 1;
	return $dbh2;
}

sub Connect_IMG_dev
{
	$ENV{'ORACLE_HOME'} = "/jgi/tools/oracle_client/DEFAULT";
	my $user2 = "img_dev_200";
	my $pw2 = "htgmar07";
	my $service2 = "img2dev.dev";
	my $dsn2 = "dbi:Oracle:$service2";
	my $dbh2 = DBI->connect( $dsn2, $user2, $pw2 );
	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
	$dbh2->{ LongReadLen } = 50000;
	$dbh2->{ LongTruncOk } = 1;
	return $dbh2;
}

sub Connect_IMG_mi
{
	$ENV{'ORACLE_HOME'} = "/jgi/tools/oracle_client/DEFAULT";
	my $user2 = "img_mi_v200_ro";
	my $pw2 = "img_mi_v200_ro";
	my $service2 = "imgi.prod";
	my $dsn2 = "dbi:Oracle:$service2";
	my $dbh2 = DBI->connect( $dsn2, $user2, $pw2 );
	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
	$dbh2->{ LongReadLen } = 50000;
	$dbh2->{ LongTruncOk } = 1;
	return $dbh2;
}


sub Connect_Operons
{
	my $user2="kmavromm";
	my $service2="Operons_v200";
	my $server2="helios.jgi-psf.org";
	my $dsn2="dbi:mysql:$service2:$server2";
	my $dbh2 = DBI->connect( $dsn2, $user2 );
	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
	$dbh2->{ LongReadLen } = 50000;
	$dbh2->{ LongTruncOk } = 1;
	
	
	return $dbh2;
}

#Connect to the database with my personal tablespace
sub Connect_IMG_personal
{	
	$ENV{'ORACLE_HOME'} = "/jgi/tools/oracle_client/DEFAULT";
        my $user2="kostas";
	my $pwd2="kostas123";
	my $service2="imgi.prod";
	my $dsn2="dbi:Oracle:$service2";
	my $dbh2 = DBI->connect( $dsn2, $user2 ,$pwd2 );
	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
	$dbh2->{ LongReadLen } = 50000;
	$dbh2->{ LongTruncOk } = 1;
return $dbh2;
}


# sub Connect_Operons_dev
# {
#         my $user2="kmavromm";
# 	my $service2="Operons_v200F";
# 	my $server2="helios.jgi-psf.org";
# 	my $dsn2="dbi:mysql:$service2:$server2";
# 	my $dbh2 = DBI->connect( $dsn2, $user2 );
# 	if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
# 	$dbh2->{ LongReadLen } = 50000;
# 	$dbh2->{ LongTruncOk } = 1;
# return $dbh2;
# }
sub Connect_Operons_dev
{	
	my $dbh2 = &Connect_IMG_personal;
return $dbh2;
}

sub Connect_Synthetic
{
        my $user2="kmavromm";
        my $service2="Synthetic";
        my $server2="helios.jgi-psf.org";
        my $dsn2="dbi:mysql:$service2:$server2";
        my $dbh2 = DBI->connect( $dsn2, $user2 );
        if( !defined( $dbh2 ) ) {die( "cannot login to oracle $user2 \@ $service2\n" );}
        $dbh2->{ LongReadLen } = 50000;
        $dbh2->{ LongTruncOk } = 1;
        return $dbh2;
}
									

sub dumpTable
{
	my ($dbh, $table_name)=@_;
	my @return_array;	
	my @data;
	my $columns;
	my $sql=qq{select * from $table_name};
	my $cur=$dbh->prepare($sql);
	$cur->execute();
	while (my @data=$cur->fetchrow_array)
	{
		$columns=scalar(@data);
		push @return_array,[@data];
	}
	$cur->finish();
	return(\@return_array,$columns);
}

sub printTable
{
	my ($dbh, $table_name)=@_;
	my @return_array;	
	my @data;
	my $columns;
	my $sql=qq{select * from $table_name};
	my $cur=$dbh->prepare($sql);
	$cur->execute();
	while (my @data=$cur->fetchrow_array)
	{
		$columns=scalar(@data);
		for(my $d=0;$d<$columns;$d++){if(!defined($data[$d])){$data[$d]="";}}
		print join("\t",@data),"\n";
	}
	$cur->finish();
	return $columns;
}

1;
