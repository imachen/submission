package ImgJob;
my $section = "ImgJob";
 
use strict; 
use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
use POSIX qw(ceil floor); 
 
use lib 'lib'; 
use WebEnv; 
use WebFunctions; 
use RelSchema; 
use ProjectInfo; 
use EnvSample; 
use TabHTML; 
 
 
my $env = getEnv(); 
my $main_cgi             = $env->{main_cgi};
my $section_cgi          = "$main_cgi?section=$section";
my $base_url = $env->{ base_url }; 
my $illumina = $env->{ illumina }; 
my $use_yahoo_table = $env->{ use_yahoo_table }; 

my $default_max_imgjob_row = 30;

 
#########################################################################
# dispatch 
#########################################################################
sub dispatch { 
    my ($page) = @_;
 
    if ( $page eq 'showPage' ) {
        ShowPage();
    } 
    elsif ( $page eq 'jobDetail' ) {
	printViewJobDetail(0);
    }
    elsif ( $page eq 'updateJobDetail' ) {
	printViewJobDetail(1);
    }
    elsif ( $page eq 'dbUpdateImgJob' ) { 
        my $msg = dbUpdateImgJob();
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowPage();
        } 
    } 
    else {
	ShowPage();
    }
}


########################################################################## 
# ShowPage - show the Home page 
########################################################################## 
sub ShowPage{ 
    my ($type) = @_; 
 
    print start_form(-name=>'imgJobMain',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 

    my $uname = db_getContactName($contact_oid);
    my $isAdmin = getIsAdmin($contact_oid);
 
    if ( $isAdmin eq 'Yes' ) {
        print "<h3>You log in as Admin $uname</h3>\n";
    }
    else { 
        print "<h3>You log in as User $uname</h3>\n";
    } 

    my $test = 0;
    if ( $use_yahoo_table && $test ) {
	ShowJobSection_yui($contact_oid, $isAdmin);
    }
    else {
	# get current page
        my $curr_page = param1('imgjob_page_no');
        if ( blankStr($curr_page) || $curr_page <= 0 ) {
            $curr_page = 1; 
        }

	# display page numbers
	my $max_display = getSessionParam('sub_filter:max_display'); 
	if ( blankStr($max_display) ) { 
	    $max_display = $default_max_imgjob_row; 
	} 

	my $dbh = Connect_IMG('imgext');
	my $cond = " ";
	if ( $isAdmin ne 'Yes' ) { 
	    $cond = " and j.contact = $contact_oid " . 
		" or j.contact in " . 
		"(select u.users from myimg_job_users u ". 
		"where u.img_job_id = j.img_job_id)"; 
	} 

	my $sql = qq{ 
	    select count(*)
		from myimg_job j, contact c 
		where j.contact = c.contact_oid 
		$cond
	    }; 

	my $cur=$dbh->prepare($sql); 
	$cur->execute(); 
	my ($total_cnt) = $cur->fetchrow();
	$cur->finish();
	$dbh->disconnect();

	ShowPageNumberSection($curr_page, $max_display, $total_cnt);

	# display table
	ShowJobSection($contact_oid, $isAdmin, $curr_page);
    }

    printHomeLink(); 
 
    print end_form(); 
} 


######################################################################## 
# ShowPageNumberSection
######################################################################## 
sub ShowPageNumberSection {
    my ($curr_page, $max_display, $total_cnt) = @_;

    my $i = 0; 
    my $page = 1;
    print "<p>\n";
    while ( $i < $total_cnt ) {
        my $s = $page; 
        if ( $page == $curr_page ) {
            $s = "<b>\[$page\]</b>"; 
        }
        my $link = "<a href='" . $main_cgi;
	$link .= "?section=ImgJob&page=showPage";
        $link .= "&imgjob_page_no=$page";
        $link .= "' >" . $s . "</a>"; 
        print $link . nbsp(1); 
        $i += $max_display; 
        $page++;
        if ( $page > 1000 ) {
            last; 
        } 
    } 
    print "<p>\n"; 
}
 

######################################################################## 
# ShowJobSection
######################################################################## 
sub ShowJobSection {
    my ($contact_oid, $isAdmin, $curr_page) = @_; 

    print start_form(-name=>'showJob',-method=>'post',action=>"$section_cgi"); 

    # max display 
    my $max_display = getSessionParam('sub_filter:max_display'); 
    if ( blankStr($max_display) ) { 
        $max_display = $default_max_imgjob_row; 
    } 

    my $dbh = Connect_IMG('imgext');

    my $cond = " ";
    if ( $isAdmin ne 'Yes' ) { 
        $cond = " and j.contact = $contact_oid " . 
            " or j.contact in " . 
            "(select u.users from myimg_job_users u ". 
            "where u.img_job_id = j.img_job_id)"; 
    } 

    my $sql = qq{ 
        select j.img_job_id, c.username, j.job_type, j.database, j.status, 
        to_char( j.add_date, 'yyyy-mm-dd HH24:MI:SS' ), 
        j.user_notes 
            from myimg_job j, contact c 
            where j.contact = c.contact_oid 
	    $cond
	    order by 1
        }; 

    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $color1 = "#eeeeee";
    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 
    print "<th class='img' bgcolor='$color1'>Select</th>\n";
    print "<th class='img' bgcolor='$color1'>Job ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Submitter</th>\n";
    print "<th class='img' bgcolor='$color1'>Job Type</th>\n";
    print "<th class='img' bgcolor='$color1'>Database</th>\n";
    print "<th class='img' bgcolor='$color1'>Job Status</th>\n";
    print "<th class='img' bgcolor='$color1'>Add Date</th>\n";
    print "<th class='img' bgcolor='$color1'>User Notes</th>\n";
 
    my $count = 0; 
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);

    for ( ; ; ) { 
        my ( $my_job_id, $username, $job_type, $database, $status,
             $add_date, $notes )
            = $cur->fetchrow();
        last if !$my_job_id; 
        $count++; 
	if ( $count <= $skip ) {
	    next;
	}

	if ( $count > 1000000 ) {
	    last;
	}

	$disp_cnt++;
        if ( $disp_cnt > $max_display ) { 
            last; 
        } 
 
        if ( $disp_cnt % 2 ) { 
            print "<tr class='img'>\n"; 
        } 
        else { 
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        } 

	print "<td class='img'>\n";
	print "<input type='radio' name='my_job_id' value='$my_job_id' />\t" ;
	print "</td>\n";
	PrintAttribute($my_job_id);
	PrintAttribute($username);
	PrintAttribute($job_type);
	PrintAttribute($database);
	PrintAttribute($status);
	PrintAttribute($add_date);
	PrintAttribute($notes);
	print "</tr>\n";
    } 
    $cur->finish(); 
    if ( $count == 0 ) { 
        print "<h4>You do not have any requested recomputation jobs.</h4>\n";
    } 
    else { 
	print "</table>\n";
        print "<p>\n"; 
    } 

    print "<p>\n";
    if ( $isAdmin ne 'Yes' ) {
	print '<input type="submit" name="_section_ImgJob:jobDetail" value="View Detail" class="meddefbutton" />'; 
    }
    else {
	print '<input type="submit" name="_section_ImgJob:updateJobDetail" value="Update" class="meddefbutton" />'; 
    }

    $dbh->disconnect();
}


######################################################################## 
# ShowJobSection_yui (Yahoo database display) 
######################################################################## 
sub ShowJobSection_yui { 
    my ($contact_oid, $isAdmin) = @_; 

    print start_form(-name=>'showJob',-method=>'post',action=>"$section_cgi"); 

    my $dbh = Connect_IMG('imgext');

    my $cond = " ";
    if ( $isAdmin ne 'Yes' ) { 
        $cond = " and j.contact = $contact_oid " . 
            " or j.contact in " . 
            "(select u.users from myimg_job_users u ". 
            "where u.img_job_id = j.img_job_id)"; 
    } 

    my $sql = qq{ 
        select j.img_job_id, c.username, j.job_type, j.database, j.status, 
        to_char( j.add_date, 'yyyy-mm-dd HH24:MI:SS' ), 
        j.user_notes 
            from myimg_job j, contact c 
            where j.contact = c.contact_oid 
	    $cond
	    order by 1
        }; 

    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();    # sort delimiter
    $it->addColSpec( "Select" ); 
    $it->addColSpec( "Job ID", "char asc", "left" ); 
    $it->addColSpec( "Submitter", "char asc", "left" ); 
    $it->addColSpec( "Job Type", "char asc", "left" ); 
    $it->addColSpec( "Database", "char asc", "left" ); 
    $it->addColSpec( "Job Status", "char asc", "left" );
    $it->addColSpec( "Add Date", "char asc", "left" );
    $it->addColSpec( "User Notes", "char asc", "left" );
 
    my $count = 0; 
    for ( ; ; ) { 
        my ( $my_job_id, $username, $job_type, $database, $status,
             $add_date, $notes )
            = $cur->fetchrow();
        last if !$my_job_id; 
        $count++; 
 
        my $r .= $sd
            . "<input type='radio' name='my_job_id' value='$my_job_id' />\t" ;
        $r .= "$my_job_id\t"; 
        $r .= $username . "\t";
        $r .= $job_type . "\t"; 
        $r .= $database . "\t"; 
        $r .= $status . "\t";
        $r .= $add_date ."\t";
        $r .= $notes . "\t";
        $it->addRow($r); 
    } 
    $cur->finish(); 
    if ( $count == 0 ) { 
        print "<h4>You do not have any requested recomputation jobs.</h4>\n";
    } 
    else { 
        print "<p>\n"; 
        $it->printOuterTable(1); 
    } 

    print "<p>\n";
    if ( $isAdmin ne 'Yes' ) {
	print '<input type="submit" name="_section_ImgJob:jobDetail" value="View Detail" class="meddefbutton" />'; 
    }
    else {
	print '<input type="submit" name="_section_ImgJob:updateJobDetail" value="Update" class="meddefbutton" />'; 
    }

    $dbh->disconnect();
}


############################################################################ 
# printViewJobDetail 
############################################################################ 
sub printViewJobDetail { 
    my ($canUpdate) = @_;

    print start_form(-name=>'jobDetail',-method=>'post',action=>"$section_cgi"); 

    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 

    my $isAdmin = getIsAdmin($contact_oid);
 
    # connect to database
    my $dbh = Connect_IMG('imgext');
 
    my $my_job_id = param1('my_job_id');
    if ( ! $my_job_id ) {
	print "Error: No request has been selected.\n";
	return;
    }

    my $sql = qq{
        select j.img_job_id, c.username, c.email,
        j.job_type, j.database, j.status,
        j.log_file, j.data_path,
        to_char( j.add_date, 'yyyy-mm-dd HH24:MI:SS' ),
        j.user_notes, 
        j.mod_date, j.modified_by, j.admin_notes
            from myimg_job j, contact c
            where j.img_job_id = $my_job_id
            and j.contact = c.contact_oid
        };
 
    if ( $isAdmin ne 'Yes' ) {
        $sql .= " and j.contact = $contact_oid or j.contact in " .
            "(select u.users from myimg_job_users u " .
            "where u.img_job_id = j.img_job_id)"; 
    } 
    $sql .= " order by 1";
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my ( $id2, $username, $email,
         $job_type, $database, $status,
         $log_file, $data_path, $add_date, 
         $notes, $mod_date, $modified_by, $admin_notes )
        = $cur->fetchrow(); 
    $cur->finish();
    if ( ! $id2 ) { 
        print "<p>Error: No detail information can be found for job $my_job_id\n";
        $dbh->disconnect(); 
        print end_form();
        return; 
    }
 
    print "<h2>Job Detail (ID: $my_job_id)</h2>\n";

    print hidden('my_job_id', $my_job_id);

    print "<table class='img' border='1'>\n";
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right' bgcolor='lightblue'>Job Request Detail</th>\n";
    print "</tr>\n";

    printAttrRow("Job ID", $my_job_id);
    printAttrRow("Submitter", $username . " (" . $email . ")");
    printAttrRow("Job Type", $job_type);
    printAttrRow("Database", $database); 

    if ( $canUpdate ) {
	print "<tr class='img' >\n";
	print "  <th class='subhead' align='right'>Status</th>\n";
	print "  <td class='img'   align='left'>";
	print "  <select name='status' class='img' size='1'>\n";

	my $sql2 = "select cv_term from img_job_statuscv order by 1";
	my $cur2=$dbh->prepare($sql2);
	$cur2->execute(); 
	for (;;) {
	    my ($v2) = $cur2->fetchrow();
	    last if ! $v2;

	    print "    <option value='$v2' ";
	    if ( $v2 eq $status ) {
		print " selected ";
	    }
	    print ">$v2</option>\n"; 
	}
	$cur2->finish();
	print "  </select>\n";
	print "</td>\n";
	print "</tr>\n";
    }
    else {
	printAttrRow("Status", $status); 
    }

    if ( $canUpdate ) {
	print "<tr class='img' >\n";
	print "  <th class='subhead' align='right'>Log File</th>\n";
	print "  <td class='img'   align='left'>";
        print "<input type='text' name='log_file' value='" .
	    escapeHTML($log_file) . "'" .
            "size='60' maxLength='1000'/>"; 
	print "</td>\n";
	print "</tr>\n";
    }
    elsif ( $log_file ) { 
        printAttrRow("Log File", $log_file);
    } 

    if ( $canUpdate ) {
	print "<tr class='img' >\n";
	print "  <th class='subhead' align='right'>Data Directory</th>\n";
	print "  <td class='img'   align='left'>";
        print "<input type='text' name='data_path' value='" .
	    escapeHTML($data_path) . "'" .
            "size='60' maxLength='1000'/>"; 
	print "</td>\n";
	print "</tr>\n";
    }
    elsif ( $data_path ) {
        printAttrRow("Data Directory", $data_path);
    } 
    printAttrRow("Add Date", $add_date); 
    printAttrRow("User Notes", $notes); 
    if ( $mod_date ) {
        printAttrRow("Last Mod Date", $mod_date);
    } 
    if ( $modified_by ) {
        my $name2 = db_getValue("select username from contact where contact_oid = $modified_by");
        printAttrRow("Modified By", $name2);
    } 

    if ( $canUpdate ) {
	print "<tr class='img' >\n";
	print "  <th class='subhead' align='right'>Admin Notes</th>\n";
	print "  <td class='img'   align='left'>";
        print "<input type='text' name='admin_notes' value='" .
	    escapeHTML($admin_notes) . "'" .
            "size='60' maxLength='1000'/>"; 
	print "</td>\n";
	print "</tr>\n";
    }
    elsif ( $admin_notes ) { 
        printAttrRow("Admin Notes", $admin_notes);
    } 
 
    # parameters 
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right' bgcolor='lightblue'>Parameters</th>\n";
    print "</tr>\n";

    $sql = "select img_job_id, param_type, param_value from myimg_job_parameters where img_job_id = $my_job_id order by 1, 2, 3"; 
    $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    for (;;) { 
        my ( $id2, $param_type, $param_value) = $cur->fetchrow();
        last if ! $id2;
 
        $param_type =~ s/'/''/g;   # replace ' with ''
        $param_value =~ s/'/''/g;   # replace ' with ''
        printAttrRow("Parameter: " . $param_type, $param_value);
    } 
    $cur->finish(); 
 
    print "</table>\n"; 
 
    $dbh->disconnect(); 

    if ( $canUpdate ) {
	print "<p>\n";
	print '<input type="submit" name="_section_ImgJob:dbUpdateImgJob" value="Update" class="smdefbutton" />'; 
    }

    printHomeLink(); 
 
    print end_form(); 
} 


############################################################################
# printAttrRow - Print one attribute row in a table.
############################################################################
sub printAttrRow {
    my( $attrName, $attrVal, $url ) = @_;
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>$attrName</th>\n";
#    my $val = attrValue( $attrVal );
    my $val = $attrVal;
    if( $url ne "" ) {
	$val = alink( $url, $attrVal );
    }
    print "  <td class='img'   align='left'>" . $val . "</td>\n";
    print "</tr>\n";
}


##########################################################################
# dbUpdateImgJob
##########################################################################
sub dbUpdateImgJob {
    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid);

    print start_form(-name=>'updateImgJob',-method=>'post',action=>"$section_cgi"); 

    my $my_job_id = param1('my_job_id');
    if ( ! $my_job_id ) {
	return "No IMG Job ID is provided.";
    }

    if ( $isAdmin ne 'Yes' ) {
	# not super-user
	return "You cannot update this request.";
    }

    my $status = param1('status');
    my $log_file = param1('log_file');
    $log_file =~ s/'/''/g;    # replace ' with ''
    my $data_path = param1('data_path');
    $data_path =~ s/'/''/g;    # replace ' with ''
    my $admin_notes = param1('admin_notes');
    $admin_notes =~ s/'/''/g;    # replace ' with ''

    my $sql = "update myimg_job set status = '" . $status . "', " .
	"log_file = '" . $log_file . "', " .
	"data_path = '" . $data_path . "', " .
	"admin_notes = '" . $admin_notes . "', " .
	"modified_by = $contact_oid, mod_date = sysdate " .
	"where img_job_id = $my_job_id";

    my @sqlList = ( $sql );
 
#    db_sqlTrans(\@sqlList); 
 
    # update to database
    my $dbh = Connect_IMG('imgext');
    $dbh->{AutoCommit} = 0; 
 
    my $last_sql = 0; 
 
    # perform database update 
    eval { 
        for my $sql (@sqlList) 
        { 
            $last_sql++; 
            my $cur = $dbh->prepare( $sql ) ||
                dienice( "execSql: cannot preparse statement: $DBI::errstr\n" ); 
            $cur->execute( ) || 
                dienice( "execSql: cannot execute: $DBI::errstr\n" );

        } 
    }; 
 
    if ($@) { 
        $dbh->rollback(); 
        $dbh->disconnect(); 
        return $last_sql; 
    } 
 
    $dbh->commit(); 
    $dbh->disconnect(); 
 
    return;
} 
 


1;

