package EnvSample;
my $section = "EnvSample";

use strict;
use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';
use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;
use TabHTML;
use GoldMetaProj;
use InnerTable;


my $env = getEnv();
my $main_cgi             = $env->{main_cgi};
my $section_cgi          = "$main_cgi?section=$section";

my $is_test = 0;

my $default_max_sample_row = 100;



#########################################################################
# dispatch
#########################################################################
sub dispatch {
    my ($page) = @_;

    if ( $page eq 'newSample' ) {
	NewSample();
    }
    elsif ( $page eq 'dbNewSample' ) {
	my $msg = dbNewSample();
 	if ( ! blankStr($msg) ) {
 	    WebFunctions::showErrorPage($msg);
 	  }
 	else {
 	    ShowSamples();
 	}
    }
    elsif ( $page eq 'updateSample' ) {
	my $sample_oid = param1('sample_oid');

	if ( blankStr($sample_oid) ) {
	    printError("No sample has been selected.");
	    print end_form();
	    return;
	}

	UpdateSample($sample_oid);
    }
    elsif ( $page eq 'dbUpdateSample' ) {
	my $msg = dbUpdateSample();

	if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
	  }
	else {
	    ShowSamples();
	}
    }
    elsif ( $page eq 'deleteSample' ) {
	DeleteSample();
    }
    elsif ( $page eq 'dbDeleteSample' ) {
	my $msg = dbDeleteSample();

	if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
	  }
	else {
	    ShowSamples();
	}
    }
    elsif ( $page eq 'copySample' ) {
        my $sample_oid = param1('sample_oid');
        CopySample($sample_oid);
    } 
    elsif ( $page eq 'copyToOtherSample' ) {
        my $sample_oid = param1('sample_oid');
        CopyToOtherSample($sample_oid);
    } 
    elsif ( $page eq 'dbCopySample' ) {
        my $sample_oid = param1('sample_oid');
        my $project_oid = param1('project_oid');
        dbCopySample($sample_oid, $project_oid);
    } 
    elsif ( $page eq 'moveSample' ) {
        my $sample_oid = param1('sample_oid');
        MoveSample($sample_oid);
    } 
    elsif ( $page eq 'selectCopySample' ) {
	ShowSelectCopySample();
    }
    elsif ( $page eq 'selectMoveSample' ) {
	ShowSelectMoveSample();
    }
    elsif ( $page eq 'dbMoveSample' ) {
        my $sample_oid = param1('sample_oid');
        dbMoveSample($sample_oid);
    } 
    elsif ( $page eq 'changeContact' ) {
        my $msg = dbChangeSampleContact(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowSamples(); 
        } 
    } 
    elsif ( $page eq 'defaultSample' ) {
        CreateDefaultSample();
 
	ShowSamples();
    }
    elsif ( $page eq 'filterSample' ) {
        FilterSample();
    }
    elsif ( $page eq 'applySampleFilter' ) {
        ApplySampleFilter();
    }
    elsif ( $page eq 'displaySample' ) {
        my $sample_oid = param1('sample_oid');
        DisplaySample($sample_oid);
    }
    elsif ( $page eq 'setGoldStampId' ) {
	my $sample_oid = param1('sample_oid');
	SetGoldStampId($sample_oid);
    }
    elsif ( $page eq 'dbSetGoldStampId' ) {
	my $sample_oid = param1('sample_oid');
	my $gold_type = param1('gold_type');
	my $gold_stamp_id = param1('gold_stamp_id');
        my $msg = dbSetGoldStampId($sample_oid, $gold_type, $gold_stamp_id);
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    GoldSample::ShowNextPage();
#            ShowSamples(); 
        } 
    }
    elsif ( $page eq 'delGoldStampId' ) {
	my $sample_oid = param1('sample_oid');
	DeleteGoldStampId($sample_oid);
    }
    elsif ( $page eq 'dbDelGoldStampId' ) {
	my $sample_oid = param1('sample_oid');
	my $del_gold_id_option = param1('del_gold_id_option');
	my $gold_stamp_id = param1('gold_stamp_id');
        my $msg = dbDelGoldStampId($sample_oid, $del_gold_id_option,
				   $gold_stamp_id);
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
            ShowSamples(); 
        } 
    }
    else {
	ShowSamples();
    }
}


#########################################################################
# sampleCount - count the number of samples
##########################################################################
sub sampleCount {
    my ($contact_oid) = @_;

    my $dbh=WebFunctions::Connect_IMG;

    my $sql = "select count(*) from env_sample";
    if ( $contact_oid ) {
	$sql .= " where contact = $contact_oid";
    }

webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( ! $cnt ) {
	return 0;
    }
    return $cnt;
}

#########################################################################
# selectedSampleCount - count the number of selected samples
##########################################################################
sub selectedSampleCount {
    my ($contact_oid) = @_;
 
    my $dbh=WebFunctions::Connect_IMG;
 
    my $filter_cond = sampleFilterCondition(); 
    my $sql = "select count(*) from env_sample s";
    if ( $contact_oid ) { 
        $sql .= " where s.contact = $contact_oid";
        if ( ! blankStr($filter_cond) ) {
            $sql .= " and " . $filter_cond;
        } 
    } 
    else { 
        if ( ! blankStr($filter_cond) ) {
            $sql .= " where " . $filter_cond; 
        } 
    } 
 
#    my $my_c_oid = getContactOid();
#    if ( $my_c_oid == 312 ) {
#	print <"p>SQL: $sql</p>\n";
#    }
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) {
        return 0;
    } 
    return $cnt;
} 
 

##########################################################################
# ShowSamples - show all env samples of project
##########################################################################
sub ShowSamples {
    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) ) { 
	$project_oid =getSessionParam('project_oid');     
    }

    if ( $project_oid ) {
	ShowProjectSamples($project_oid);
    }
    else {
	require GoldSample;
	GoldSample::ShowPage();
    }
}


##########################################################################
# ShowSamples_old - show all env samples by this user
##########################################################################
sub ShowSamples_old {
	
    print start_form(-name=>'showSamples',-method=>'post',action=>"$section_cgi");

    my $contact_oid = getContactOid(); # checkAccess();

    if ( ! $contact_oid ) {
	dienice("Unknown username / password");
    }

#    saveAppHiddenVar();

    my $cnt = 0;
    my $select_cnt = 0;
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
	print "<h2>All Samples</h2>\n";
	$cnt = sampleCount();
        $select_cnt = selectedSampleCount();
        print "<p>Selected Sample Count: $select_cnt (Total: $cnt)</p>\n";
    }
    else {
	print "<h2>Your Samples</h2>\n";
	$cnt = sampleCount($contact_oid);
        $select_cnt = selectedSampleCount($contact_oid);
        print "<p>Selected Sample Count: $select_cnt (Total: $cnt)</p>\n";
    }

    # save orderby param 
    my $orderby = param1('sample_orderby'); 
    my $desc = param1('sample_desc'); 
 
    # max display 
    my $max_display = getSessionParam('sample_filter:max_display'); 
    if ( blankStr($max_display) ) { 
        $max_display = $default_max_sample_row; 
    } 

    # display page numbers
    my $curr_page = param1('sample_page_no'); 
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 
    my $i = 0; 
    my $page = 1; 
    print "<p>\n"; 
    while ( $i < $select_cnt ) { 
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>$page</b>"; 
        } 
        my $link = "<a href='" . $main_cgi . 
            "?section=EnvSample&page=showSamples" . 
            "&sample_page_no=$page"; 
        if ( ! blankStr($orderby) ) { 
            $link .= "&sample_orderby=$orderby"; 
            if ( ! blankStr($desc) ) {
                $link .= "&sample_desc=desc";
            } 
        } 
        $link .= "' >" . $s . "</a>";
        print $link . nbsp(1);
        $i += $max_display;
        $page++; 
        if ( $page > 1000 ) { 
            last; 
        }
    } 
    print "<p>\n"; 

    # list samples
    if ( $select_cnt > 0 ) {
	if ( $isAdmin eq 'Yes' ) {
	    listSamples('', $curr_page);
	}
	else {
	    listSamples($contact_oid, $curr_page);
	}
    }

    # New, Update and Delete buttons
    print '<input type="submit" name="_section_EnvSample:newSample" value="New" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:updateSample" value="Update" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:deleteSample" value="Delete" class="smbutton" />';

#    my $my_c_oid = getContactOid();
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_EnvSample:copySample" value="Copy" class="smbutton" />'; 

    # sample selection filter
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:filterSample" value="Filter Samples" class="smbutton" />';

    # allow admin to change IMG contact 
    if ( $isAdmin eq 'Yes' ) { 
	print hr;
        print "<h3>Change IMG Contact for Selected Sample</h3>\n"; 
        print "<p>New IMG Contact:\n"; 
        print "<select name='new_contact' class='img' size='1'>\n"; 
 
        my $sql2 = "select contact_oid, username from contact order by username, contact_oid"; 
        my $dbh2 = Connect_IMG_Contact(); 
webLog("$sql2\n");        
        my $cur2=$dbh2->prepare($sql2); 
        $cur2->execute(); 
 
        for (my $j2 = 0; $j2 <= 10000; $j2++) { 
            my ($id2, $name2) = $cur2->fetchrow_array(); 
            if ( ! $id2 ) { 
                last; 
            } 
 
            print "    <option value='$id2'"; 
            if ( $contact_oid == $id2 ) { 
                print " selected "; 
            } 
            print ">$name2 (OID: $id2)</option>\n"; 
        } 
        print "</select>\n"; 
        $cur2->finish(); 
        $dbh2->disconnect(); 
 
        print "<p>\n"; 
        print '<input type="submit" name="_section_EnvSample:changeContact" value="Change Contact" class="medbutton" />';
    } 

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();	
}


#########################################################################
# printSampleButtons - add, update or delete
#########################################################################
sub printSampleButtons {
    # New, Update and Delete buttons
    print '<input type="submit" name="_section_EnvSample:newSample" value="New" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:updateSample" value="Update" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:deleteSample" value="Delete" class="smbutton" />';

#    my $my_c_oid = getContactOid();
#    if ( $my_c_oid == 312 ) {
        print "&nbsp; \n"; 
        print '<input type="submit" name="_section_EnvSample:copySample" value="Copy" class="smbutton" />'; 
#    } 

    # sample selection filter
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:filterSample" value="Filter Samples" class="smbutton" />';
}

##########################################################################
# ShowProjectSamples - show all env samples of a project
##########################################################################
sub ShowProjectSamples {
    my ($project_oid) = @_;

    print start_form(-name=>'showSamples',-method=>'post',action=>"$section_cgi");

    my $contact_oid = getContactOid(); # checkAccess();

    if ( ! $contact_oid ) {
	dienice("Unknown username / password");
    }

    if ( ! $project_oid ) {
	printError("No project has been selected.");
	print end_form();
	return;
    }
    print hiddenVar('project_oid', $project_oid); 

    saveProjectPageInfo();

    my $cnt = db_getValue("select count(*) from env_sample where project_info = $project_oid");

    my $proj_name = db_getValue("select display_name from project_info where project_oid = $project_oid");

    print "<h2>Samples of Project $project_oid (" .
	escapeHTML($proj_name) . ") </h2>\n";
    if ( $cnt > 0 ) {
	print "<p><font color='blue'>(Sample Count: $cnt)</font></p>\n";
    }
    else {
	print "<p>There are no existing samples of this project. " .
	    "Click the 'New' button to add samples, " .
	    "or click the 'Create Default Sample' to create a default sample for the project.</p>\n";
    }

    my $isAdmin = getIsAdmin($contact_oid);

    # list samples
    if ( $cnt > 0 ) {
	if ( $isAdmin eq 'Yes' ) {
	    listProjectSamples_yui('', $project_oid);
	}
	else {
	    listProjectSamples_yui($contact_oid, $project_oid);
	}
    }

    # New, Update and Delete buttons
    print "<p>\n";
    print "<input type='submit' name='_section_EnvSample:newSample' value='New' class='smbutton' />";

    if ( $cnt > 0 ) {
	print "&nbsp; \n";
	print "<input type='submit' name='_section_EnvSample:updateSample' value='Update' class='smbutton' />";
	print "&nbsp; \n";
	print "<input type='submit' name='_section_EnvSample:deleteSample' value='Delete' class='smbutton' />";

	print "&nbsp; \n"; 
	print "<input type='submit' name='_section_EnvSample:copySample' value='Copy' class='smbutton' />"; 
    }

    # create default sample
    print "&nbsp; \n"; 
    print "<input type='submit' name='_section_EnvSample:defaultSample' value='Create Default Sample' class='medbutton' />"; 

    # allow admin to change IMG contact 
    if ( $cnt > 0 && $isAdmin eq 'Yes' ) { 
	print hr;
        print "<h3>Change IMG Contact for Selected Sample</h3>\n"; 
        print "<p>New IMG Contact:\n"; 
        print "<select name='new_contact' class='img' size='1'>\n"; 
 
        my $sql2 = "select contact_oid, username from contact order by username, contact_oid"; 
        my $dbh2 = Connect_IMG_Contact();
webLog("$sql2\n");
        my $cur2=$dbh2->prepare($sql2); 
        $cur2->execute(); 
 
        for (my $j2 = 0; $j2 <= 10000; $j2++) { 
            my ($id2, $name2) = $cur2->fetchrow_array(); 
            if ( ! $id2 ) { 
                last; 
            } 
 
            print "    <option value='$id2'"; 
            if ( $contact_oid == $id2 ) { 
                print " selected "; 
            } 
            print ">$name2 (OID: $id2)</option>\n"; 
        } 
        print "</select>\n"; 
        $cur2->finish(); 
        $dbh2->disconnect(); 
 
        print "<p>\n"; 
        print "<input type='submit' name='_section_EnvSample:changeContact' value='Change Contact' class='medbutton' />";
    } 

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();	
}


######################################################################### 
# sampleFilterCondition 
########################################################################## 
sub sampleFilterCondition { 
    my $cond = ""; 

    # filter condition ??? 
    my @all_params = sampleFilterParams(); 
    for my $p0 ( @all_params ) { 
        my $cond1 = ""; 
        if ( getSessionParam($p0) ) { 
            my @vals = split(/\,/, getSessionParam($p0)); 
            my ($tag, $fld_name) = split(/\:/, $p0); 
 
            if ( lc($fld_name) eq 'sample_display_name' ) {
                my $s1 = getSessionParam($p0); 
                if ( length($s1) > 0 ) { 
                    $cond1 = "lower(s.$fld_name) like '%" . lc($s1) . "%'"; 
                } 
            } 
            elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = getSessionParam($p0); 
                if ( length($s1) > 0 ) { 
                    $cond1 = "s.sample_oid in (select essm.sample_oid " .
			"from env_sample_seq_method essm " .
			"where lower(essm.$fld_name) = '" . lc($s1) . "')"; 
                } 
	    }
            elsif ( lc($fld_name) eq 'add_date' ||
                    lc($fld_name) eq 'mod_date' ) {
                my $op_name = $p0 . ":op"; 
                my $op1 = getSessionParam($op_name); 
                my $d1 = getSessionParam($p0);
                if ( !blankStr($op1) && !blankStr($d1) ) {
                    $cond1 = "s.$fld_name $op1 '" . $d1 . "'";
                } 
            } 
            else { 
                for my $s2 ( @vals ) { 
                    $s2 =~ s/'/''/g;   # replace ' with '' 
                    if ( blankStr($cond1) ) { 
                        $cond1 = "s.$fld_name in ('" . $s2 . "'"; 
                    } 
                    else { 
                        $cond1 .= ", '" . $s2 . "'"; 
                    } 
                } 
 
                if ( ! blankStr($cond1) ) { 
                    $cond1 .= ")"; 
                } 
            } 
        } 
 
        if ( ! blankStr($cond1) ) {
            if ( blankStr($cond) ) {
                $cond = $cond1;
            } 
            else { 
                $cond .= " and " . $cond1;
            } 
        } 
    }  # end for p0

    return $cond;
}


#########################################################################
# listSamples - list samples
##########################################################################
sub listSamples {
    my ($contact_oid, $curr_page) = @_;

    my $cond = "";
    if ( $contact_oid ) {
	$cond = " where s.contact = $contact_oid";
    }

    my $my_c_oid = getContactOid();

    my $selected_sample = param1('sample_oid');

    # max display 
    my $max_display = getSessionParam('sample_filter:max_display');
    if ( blankStr($max_display) ) {
        $max_display = $default_max_sample_row;
    }

    my $orderby = param1('sample_orderby');
    my $desc = param1('sample_desc');
    if ( blankStr($orderby) ) { 
        $orderby = "s.sample_oid $desc"; 
    } 
    elsif ( $orderby eq 'sample_oid' ) { 
        $orderby = "s.sample_oid $desc"; 
    } 
    else {
        $orderby = "s." . $orderby . " $desc, s.sample_oid"; 
    } 

    # filter condition ??? 
    my $filter_cond = sampleFilterCondition(); 
    if ( ! blankStr($filter_cond) ) { 
        if ( blankStr($cond) ) { 
            $cond = " where " . $filter_cond; 
        } 
        else { 
            $cond .= " and " . $filter_cond; 
        } 
    } 

    my $dbh=WebFunctions::Connect_IMG;

    my $sql = qq{
	select s.sample_oid, s.sample_display_name, s.project_info,
	s.sample_site, s.contact, s.add_date, s.mod_date
	    from env_sample s
	    $cond
	    order by $orderby
	};

    if ( $my_c_oid == 312 ) { 
        print "<p>SQL: $sql</p>";
    } 
 
    if ( ! blankStr($filter_cond) ) {
        print "<p><font color='red'>Filter is on. Some samples may be hidden. Reset the filter to see more samples.</font></p>\n";
    } 

    printSampleButtons();
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();

    my %contact_list;

    print "<h5>Click the column name to have the data order by the selected column. Click (Rev) to order by the same column in reverse order.</h5>\n";
    print "<p>\n";
    print "<table class='img' border='1'>\n"; 
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>" . 
        getSampleOrderByLink('sample_oid', 'ER Submission Sample ID', 0) .
	"<br/>" .
        getSampleOrderByLink('sample_oid', '(Rev)', 0) .
        "</th>\n";
    print "<th class='img'>" . 
        getSampleOrderByLink('sample_display_name', 'Sample Display Name', 0) .
	"<br/>" .
        getSampleOrderByLink('sample_display_name', '(Rev)', 1) .
        "</th>\n";
    print "<th class='img'>" . 
        getSampleOrderByLink('project_info', 'ER Submission Project ID', 0) .
	"<br/>" .
        getSampleOrderByLink('project_info', '(Rev)', 1) .
        "</th>\n";
#    print "<th class='img'>ER Submission ID</th>\n"; 
    print "<th class='img'>" . 
        getSampleOrderByLink('sample_site', 'Sample Site', 0) .
	"<br/>" .
        getSampleOrderByLink('sample_site', '(Rev)', 1) .
        "</th>\n";
    print "<th class='img'>IMG Contact</th>\n"; 
    print "<th class='img'>" . 
        getSampleOrderByLink('add_date', 'Add Date', 0) .
	"<br/>" .
        getSampleOrderByLink('add_date', '(Rev)', 1) .
	"</th>\n";
    print "<th class='img'>" . 
        getSampleOrderByLink('mod_date', 'Last Mod Date', 0) .
	"<br/>" .
        getSampleOrderByLink('mod_date', '(Rev)', 1) .
	"</th>\n";

    my $cnt = 0;
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);
    for (;;) {
	my ( $sample_id, $sample_name, $project_info, $site, $contact,
	     $add_date, $mod_date ) =
	    $cur->fetchrow_array();
	if ( ! $sample_id ) {
	    last;
	}

	$cnt++;
	if ( $cnt <= $skip ) {
	    next;
	}

	$disp_cnt++;
        if ( $disp_cnt > $max_display ) { 
            last; 
        } 

	print "<tr class='img'>\n";

        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='sample_oid' value='$sample_id'";
	if ( $sample_id eq $selected_sample ) {
	    print " checked ";
	}
        print "/></td>\n";

	my $sample_link = getSampleLink($sample_id);
	PrintAttribute($sample_link);

	PrintAttribute($sample_name);

	my $proj_link = getProjectLink($project_info);
	PrintAttribute($proj_link);

	# ER submission ID
#        my @subs = db_getValues("select submission_id from submission where sample_oid = $sample_id");
#        my $sub_str = "";
#        for my $s1 ( @subs ) {
#            my $submit_link = getSubmissionLink($s1);
#            $sub_str .= $submit_link . " ";
#        }
#        PrintAttribute($sub_str);

	PrintAttribute($site);

        if ( $contact_list{$contact} ) { 
            PrintAttribute($contact_list{$contact});
        }
        else { 
            my $contact_str = db_getContactName($contact);
            $contact_list{$contact} = $contact_str; 
            PrintAttribute($contact_str);
        } 

	PrintAttribute($add_date);
	PrintAttribute($mod_date);
	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();

#    if ( $cnt > $max_display ) { 
#        print "<p><font color='blue'>Too many rows. Only $max_display rows are displayed. (Use 'Filter Samples' to set the number of rows to be displayed.)</font></p>\n"; 
#    } 

    return $cnt;
}


#########################################################################
# listProjectSamples - list samples of a project
##########################################################################
sub listProjectSamples {
    my ($contact_oid, $project_oid) = @_;

    my $cond = " where s.project_info = $project_oid";
    if ( $contact_oid ) {
	$cond .= " and s.contact = $contact_oid";
    }

    my $my_c_oid = getContactOid();

    my $selected_sample = param1('sample_oid');

    my $dbh=WebFunctions::Connect_IMG;

    my $sql = qq{
	select s.sample_oid, s.sample_display_name, s.project_info,
	s.sample_site, s.contact, s.add_date, s.mod_date
	    from env_sample s
	    $cond
	};

    if ( $my_c_oid == 312 ) { 
        print "<p>SQL: $sql</p>";
    } 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute();

    my %contact_list;

    print "<p>\n";
    print "<table class='img' border='1'>\n"; 
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>" . "ER Submission Sample ID" . "</th>\n";
    print "<th class='img'>" . "Sample Display Name" . "</th>\n";
    print "<th class='img'>" . "Sample Site" . "</th>\n";
    print "<th class='img'>IMG Contact</th>\n"; 
    print "<th class='img'>" . "Add Date" . "</th>\n";
    print "<th class='img'>" . "Last Mod Date" . "</th>\n";

    for (;;) {
	my ( $sample_id, $sample_name, $project_info, $site, $contact,
	     $add_date, $mod_date ) =
	    $cur->fetchrow_array();
	if ( ! $sample_id ) {
	    last;
	}

	print "<tr class='img'>\n";

        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='sample_oid' value='$sample_id'";
	if ( $sample_id eq $selected_sample ) {
	    print " checked ";
	}
        print "/></td>\n";

	my $sample_link = getSampleLink($sample_id);
	PrintAttribute($sample_link);

	PrintAttribute($sample_name);

#	my $proj_link = getProjectLink($project_info);
#	PrintAttribute($proj_link);

	PrintAttribute($site);

        if ( $contact_list{$contact} ) { 
            PrintAttribute($contact_list{$contact});
        }
        else { 
            my $contact_str = db_getContactName($contact);
            $contact_list{$contact} = $contact_str; 
            PrintAttribute($contact_str);
        } 

	PrintAttribute($add_date);
	PrintAttribute($mod_date);
	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();
}



#########################################################################
# listProjectSamples_yui - list samples of a project
# (use Yahoo data table)
##########################################################################
sub listProjectSamples_yui {
    my ($contact_oid, $project_oid) = @_;

    my $cond = " where s.project_info = $project_oid";
    if ( $contact_oid ) {
	$cond .= " and s.contact = $contact_oid";
    }

    my $my_c_oid = getContactOid();

    my $selected_sample = param1('sample_oid');

    my $dbh=WebFunctions::Connect_IMG;

    my $sql = qq{
	select s.sample_oid, s.sample_display_name, s.gold_id, s.project_info,
	s.sample_site, s.contact, 
        to_char(s.add_date, 'DD-MON-YYYY'), to_char(s.mod_date, 'DD-MON-YYYY')
	    from env_sample s
	    $cond
	};

    if ( $my_c_oid == 312 ) { 
        print "<p>SQL: $sql</p>";
    } 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute();

    my %contact_list;

    print "<p>\n";
    my $it = new InnerTable(1, "GoldProjSample$$", "GoldProjSample", 1);
    my $sd = $it->getSdDelim();    # sort delimiter
    $it->addColSpec( "Select" ); 
 
    $it->addColSpec("Sample ID", "number asc", "right", "", 
		    "Sample ID"); 
    $it->addColSpec("Sample Display Name", "char asc", "left", "", "Sample Display Name"); 
    $it->addColSpec("Sample GOLD ID", "char asc", "left", "", "Sample GOLD ID"); 
    $it->addColSpec("Sample Site", "char asc", "left", "", "Sample Site"); 
    $it->addColSpec("IMG Contact", "char asc", "left", "", "IMG Contact"); 
    $it->addColSpec("Add Date", "char asc", "left", "", "Add Date");
    $it->addColSpec("Last Mod Date", "char asc", "left", "", "Last Mod Date"); 

    my $cnt = 0;
    for (;;) {
	my ( $sample_id, $sample_name, $gold_id, $project_info, $site, $contact,
	     $add_date, $mod_date ) =
	    $cur->fetchrow_array();
	if ( ! $sample_id ) {
	    last;
	}

	$cnt++;
	my $row = "";
	$row .= $sd . "<input type='radio' name='sample_oid' " . 
	    "value='$sample_id' ";
	if ( $sample_id eq $selected_sample ) {
	    $row .= " checked ";
	}
	$row .= " />\t";

	my $sample_link = getSampleLink($sample_id);
	$row .= $sample_id . $sd . $sample_link . "\t";

	$row .= $sample_name . $sd . $sample_name . "\t";

#	my $proj_link = getProjectLink($project_info);
#	PrintAttribute($proj_link);

	my $gold_link = getGoldLink($gold_id);
	$row .= $gold_id . $sd . $gold_link . "\t";
	$row .= $site . $sd . $site . "\t";

        if ( $contact_list{$contact} ) { 
	    $row .= $contact_list{$contact} . $sd . $contact_list{$contact} . "\t";
        }
        else { 
            my $contact_str = db_getContactName($contact);
            $contact_list{$contact} = $contact_str; 
	    $row .= $contact_str . $sd . $contact_str . "\t";
        } 

        $row .= $add_date . $sd . $add_date . "\t";
        $row .= $mod_date . $sd . $mod_date . "\t"; 
 
        $it->addRow($row);
    }

    if ( $cnt > 0 ) {
	$it->printOuterTable(1);
    }

    $cur->finish();
    $dbh->disconnect();
}


#############################################################################
# NewSample - enter new sample
#############################################################################
sub NewSample {

    my %db_val;

    print start_form(-name=>'newSample',-method=>'post',action=>"$section_cgi");

    print "<h2>New Sample</h2>\n";

    my $project_oid = param1('project_oid');
    if ( $project_oid ) {
	my $proj_name = db_getValue("select display_name from project_info where project_oid = $project_oid");
	print "<h3>Project $project_oid: (" . escapeHTML($proj_name) .
	    ")</h3>\n";
	print hiddenVar('project_oid', $project_oid);
    }

    # tab view
    TabHTML::printTabAPILinks("newSample");
    my @tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4" );
    my @tabNames = 
        ( "Sample Information (*)", "Sequencing Information", "Environmental Metadata", "Host Metadata" );

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
        push @tabIndex, ( "#tab5" ); 
        push @tabNames, ( "JGI Info" ); 
    }

    TabHTML::printTabDiv( "newSample", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    print "<p>Required fields are marked with (*). Mouse over MIGS ID to see field description.</p>\n"; 

    printSampleTab("Sample Information", "", \%db_val);
    print "</p></div>\n"; 

    # tab 2
    print "<div id='tab2'><p>\n"; 
    printSampleTab("Sequencing Information", "", \%db_val);
    printSampleSetValTab ('env_sample_seq_method', '');
    print "</p></div>\n"; 

    # tab 3
    print "<div id='tab3'><p>\n"; 
    printSampleTab("Environmental Metadata", "", \%db_val);
#    printSampleSetValTab ('env_sample_misc_meta_data', '');
    printSampleSetValTab ('env_sample_phenotypes', '');
    printSampleSetValTab ('env_sample_habitat_type', '');
    print "</p></div>\n"; 

    # tab 4
    print "<div id='tab4'><p>\n"; 
    printSampleTab("Host Metadata", "", \%db_val);
#   printSampleSetValTab ('env_sample_diseases', '');
#    printSampleSetValTab ('env_sample_metabolism', '');
#    printSampleSetValTab ('env_sample_energy_source', '');
    print "</p></div>\n"; 

    if ( $isAdmin eq 'Yes' ) { 
	# tab 5
        print "<div id='tab5'><p>\n";
        printSampleTab("JGI", "", \%db_val); 
        printSampleSetValTab ('env_sample_jgi_url', '');
        print "</p></div>\n";
    } 

    TabHTML::printTabDivEnd();

    print "<p>\n"; 
    print '<input type="submit" name="_section_EnvSample:dbNewSample" value="Add" class="smbutton" />'; 
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="Cancel" class="smbutton" />';
 
    printHomeLink();
 
    print end_form(); 
 
}


###########################################################################
# printSampleTab
###########################################################################
sub printSampleTab {
    my ($tab, $sample_oid, $db_val) = @_;

    # get Env_Sample definition
    my $def_sample = def_Env_Sample();
    my @attrs = @{$def_sample->{attrs}};

    my $use_ecocv = 0;

    print "<table class='img' border='1'>\n"; 

    for my $k ( @attrs ) { 
        my $attr_val = ""; 
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};

	# show only the attributes in this tab
	if ( blankStr($k->{tab}) ||
	     $k->{tab} ne $tab ) {
	    next;
	}

	# skip non-editable for new
	if ( blankStr($sample_oid) && ! $k->{can_edit} ) {
	    next;
	}

        # print more header?
#        if ( $k->{header} && length($k->{header}) > 0 ) {
#            print "<tr class='highlight' >\n";
#            print "  <th class='subhead' align='right'>" .
#                "<font color='darkblue'>" .
#                escapeHTML($k->{header}) . "</font></th>\n";
#            PrintAttribute("   "); 
#            print "</tr>\n"; 
#        } 

        print "<tr class='img' >\n";

        # MIGS 
        printCellTooltip($k->{migs_id}, $k->{migs_name}, 70);

        print "  <th class='subhead' align='right'>";

	if ( $k->{is_required} ) {
            print escapeHTML($disp_name) . " (*) </th>\n"; 
        }
	elsif ( $k->{url} ) {
	    print alink($k->{url}, $disp_name, 'target', 1);
	}
	else {
	    print escapeHTML($disp_name);
	}
	print "</th>\n";

	# get attribute value, if any
	if ( $sample_oid ) {
	    if ( $db_val &&
		 $db_val->{ $attr_name } ) {
		$attr_val = $db_val->{ $attr_name };
	    }
	}

	# print attribute value
	print "  <td class='img'   align='left'>\n";

	if ( $data_type =~ /\|/ ) {
	    # selection
	    my @selects = split(/\|/, $data_type);
	    print "<select name='$attr_name' class='img' size='1'>\n"; 
	    if ( ! $k->{is_required} ) {
		print "   <option value=''> </option>\n";
	    } 
	    for my $s0 ( @selects ) {
		print "    <option value='" . escapeHTML($s0) . "'"; 
		if ( ! blankStr($attr_val) && $attr_val eq $s0 ) {
		    print " selected ";
		}
		print ">$s0</option>\n"; 
	    } 
	    print "</select>\n"; 
	}
	elsif ( $data_type eq 'cv2' ) {
	    print "<select name='$attr_name' class='img' size='1'>\n"; 
	    if ( ! $k->{is_required} ) {
		print "   <option value=''> </option>\n";
	    } 

	    my $sql2 = $k->{cv_query};

	    # special case for project_info
	    if ( $attr_name eq 'project_info' ) {
		my $contact_oid = getContactOid();
		$sql2 = "select project_oid, display_name " .
		    "from project_info " .
		    "where gold_stamp_id is not null " .
		    "or contact_oid = $contact_oid " .
		    "order by display_name";
	    }

	    my $dbh2 = Connect_IMG(); 
webLog("$sql2\n");	    
	    my $cur2=$dbh2->prepare($sql2); 
	    $cur2->execute(); 

	    for (my $j2 = 0; $j2 <= 10000; $j2++) { 
		my ($id2, $name2) = $cur2->fetchrow_array(); 
		if ( ! $id2 ) {
		    last;
		}

		print "    <option value='" . escapeHTML($id2) . "'"; 
		if ( ! blankStr($attr_val) && $attr_val eq $id2 ) {
		    print " selected ";
		}

		if ( $attr_name eq 'project_info' ) {
		    print ">$name2 (ID: $id2)</option>\n"; 
		}
		else {
		    print ">$id2 - $name2</option>\n"; 
		}
	    } 
	    print "</select>\n"; 
	    $cur2->finish();
	    $dbh2->disconnect();
	}
	elsif ( $data_type eq 'cv' && $use_ecocv && $k->{cv_query} ) {
# If UpdateSample/NewSample page then use javascript chained menu dynamic population
# for metagenomic classification
# else for updating use normal interface
    my $currentPage = GoldMetaProj::getPage();

		if (($attr_name eq 'ecosystem') && ($currentPage eq 'newSample')) {
#		    print "<select id='ecosystem' name='ecosystem' style=\"width:180px;\"\">
		    print "<select name='ecosystem' style=\"width:180px;\"\">
                    <option value=\"Make a selection\" selected=yes>Make a selection</option>
                    <option value=\"1\">Host-associated</option>
                    <option value=\"364\">Engineered</option>
                    <option value=\"489\">Environmental</option>
                    <option value=\"573\">Unclassified</option>";
		    print "</select> &nbsp; <input type=\"button\" onClick=\"myPopup()\" value=\"New\"></td>";
		}
		elsif ((($attr_name eq 'ecosystem_category') ||
			($attr_name eq 'ecosystem_type') ||
			($attr_name eq 'ecosystem_subtype') ||
			($attr_name eq 'specific_ecosystem') ) && ($currentPage eq 'newSample'))
		    {
			print "
<select name='$attr_name' style=\"width:180px;\" disabled>
<option selected=yes>Make a selection</option>";
 		    }
		elsif ((($attr_name eq 'ecosystem') ||
			($attr_name eq 'ecosystem_category') ||
			($attr_name eq 'ecosystem_type') ||
			($attr_name eq 'ecosystem_subtype') ||
			($attr_name eq 'specific_ecosystem') ) && ($currentPage eq 'updateSample'))
		    {

# get the names from the ecosystem classification ids
	my %ecosystemMap;
	my $dbh=WebFunctions::Connect_IMG;
	my $ecoSQL = qq{select mc_node_id, node, node_type from metagenomic_class_nodes};
	my $sth = $dbh->prepare($ecoSQL) or die "Couldn't prepare statement: " . $dbh->errstr;
	$sth->execute() or die "Couldn't execute statement: " . $sth->errstr;
	
	my @results = @{$sth->fetchall_arrayref()};
	for my $row (@results) {
	    my ($mc_node_id, $node, $node_type )  = @$row;
	    $ecosystemMap{$node}{$node_type} = $mc_node_id;
	}
	my @db_vals = db_getValues($k->{cv_query});

	if ( ! $attr_val && $k->{default_value} ) {
	    $attr_val = $k->{default_value}; 
	} 
	
	print "<select name='$attr_name' class='img' size='1'>\n";
	if ($attr_val eq '') {
	    print "   <option value='' selected=\"yes\">Make Selection</option>\n";
		if ($attr_name eq 'ecosystem')  {
		    print "</select> &nbsp; <input type=\"button\" onClick=\"myPopup()\" value=\"New\"></td>";
		}
	}
	for my $val ( sort @db_vals ) { 
	    print "   <option value='$ecosystemMap{$val}{$attr_name}'";
	    if ( $val eq $attr_val ) {
		print " selected ";
	    } 
	    print ">" . escapeHTML($val) . "</option>\n";
	} 

	if ($attr_name eq 'ecosystem')  {
	    print "</select> &nbsp; <input type=\"button\" onClick=\"myPopup()\" value=\"New\"></td>";
	}
	print "</select></td>\n"; 
    } else {
	my @selects = db_getValues($k->{cv_query});
	    print "<select name='$attr_name' class='img' size='1'>\n"; 
	    if ( ! $k->{is_required} ) {
		print "   <option value=''> </option>\n";
	    } 
	    for my $s0 ( @selects ) {
		print "    <option value='" . escapeHTML($s0) . "'"; 
		if ( ! blankStr($attr_val) && $attr_val eq $s0 ) {
		    print " selected ";
		}
		print ">$s0</option>\n"; 
	    } 
	    print "</select>\n"; 
}
}
	elsif ( $data_type eq 'cv' && $k->{cv_query} ) {
	    if ( ! $attr_val && $k->{default_value} ) {
		$attr_val = $k->{default_value}; 
	    } 

	    my @selects = db_getValues($k->{cv_query});
	    print "<select name='$attr_name' class='img' size='1'>\n"; 
	    if ( ! $k->{is_required} ) {
		print "   <option value=''> </option>\n";
	    } 
	    for my $s0 ( @selects ) {
		print "    <option value='" . escapeHTML($s0) . "'"; 
		if ( ! blankStr($attr_val) && $attr_val eq $s0 ) {
		    print " selected ";
		}
		print ">$s0</option>\n"; 
	    } 
	    print "</select>\n"; 
	}
	elsif ( $k->{can_edit} ) {
	    my $size = 80;
	    if ( $len && $size > $len ) {
		$size = $len; 
	    }
 
	    print "<input type='text' name='$attr_name' value='";
	    if ( ! blankStr($attr_val) ) {
		print escapeHTML($attr_val);
	    }
	    print "' size='$size'" . " maxLength='$len'/>"; 
	}
	else {
	    # display only
	    if ( ! blankStr($attr_val) ) {
		print escapeHTML($attr_val);
	    }
	}

	print "</td>\n";

	print "</tr>\n"; 
    } 

    if ( $tab eq 'JGI' ) {
	# show project jgi_proposal_id, if any
	my $jgi_proposal_id = '';
	if ( $sample_oid ) {
	    $jgi_proposal_id = db_getValue("select p.jgi_proposal_id from project_info p, env_sample s where s.project_info = p.project_oid and s.sample_oid = $sample_oid");
	}
	if ( $jgi_proposal_id ) {
	    print "<tr class='img' >\n";
	    print "<td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>Project JGI Proposal ID</th>\n";
	    print "<td class='img'>$jgi_proposal_id</td>\n";
	    print "</tr>\n";
	}
    }

    print "</table>\n"; 

#    print "<p>\n";

#    print '<input type="submit" name="dbNewSample" value="Add" class="smbutton" />'; 
#    print "&nbsp; \n";
#    print '<input type="submit" name="dbNewUpdSample" value="Add and Update Attributes" class="medbutton" />';
#    print "&nbsp; \n";
#    print '<input type="submit" name="showSamples" value="Cancel" class="smbutton" />';

#    print "<p>\n";
#    printHomeLink();

#    print end_form();	
}


######################################################################### 
# printSampleSetValTab - set-valued attributes 
######################################################################### 
sub printSampleSetValTab { 
    my ($tname, $sample_oid) = @_; 
 
    my $def_aux = def_Class($tname); 
    if ( ! $def_aux ) { 
        print "<p>Cannot find $tname</p>\n"; 
        return; 
    } 
    my @aux_attrs = @{$def_aux->{attrs}}; 
    my $extra_row = $def_aux->{new_rows};

    print "<h2";
    if ( $def_aux->{migs_name} ) {
        print " title='" . $def_aux->{migs_name} . "'"; 
    }
    print ">" . $def_aux->{display_name};
    if ( $def_aux->{migs_id} ) { 
        print " (" . $def_aux->{migs_id} . ")"; 
    } 
    print "</h2>\n"; 
 
    print "<h4>Please edit the following list of values:</h4>\n";

    my $default_size = 60; 
    if ( scalar(@aux_attrs) > 2 ) {
        $default_size = 40;
    } 
 
    # print table header
    print "<table class='img' border='1'>\n";
    for my $k ( @aux_attrs ) {
        if ( $k->{can_edit} ) {
            print "<th class='img'>" . escapeHTML($k->{display_name}) .
                "</th>\n";
        } 
    } 

    my $i2 = 0; 

    # show old values ??? 
    if ( ! blankStr($sample_oid) && isInt($sample_oid) ) {
	my $sql = ""; 
	for my $k ( @aux_attrs ) {
	    if ( ! $k->{can_edit} ) { 
		next; 
	    } 
 
	    if ( blankStr($sql) ) { 
		$sql = "select " . $k->{name}; 
	    } 
	    else { 
		$sql .= ", " . $k->{name} 
	    } 
	} 
	if ( ! $sample_oid ) {
	    $sample_oid = 0;
	}
	$sql .= " from $tname where " . $def_aux->{id} . " = $sample_oid"; 
	my $dbh = Connect_IMG(); 
webLog("$sql\n");	
	my $cur=$dbh->prepare($sql); 
	$cur->execute(); 

	for ($i2 = 0; $i2 <= 10000; $i2++) { 
	    my @flds = $cur->fetchrow_array(); 
	    if ( scalar(@flds) == 0 ) { 
		last; 
	    } 
	    if ( ! $flds[0] ) { 
		last; 
	    } 
 
	    print "<tr class='img'>\n"; 
 
	    my $j = 0; 
	    for my $k ( @aux_attrs ) { 
		if ( $j >= scalar(@aux_attrs) ) { 
		    last; 
		} 
 
		my $attr_val = $flds[$j]; 
 
		if ( ! $k->{can_edit} ) { 
		    next; 
		} 
 
		my $fld_name = $tname . "|" . $k->{name} . "|" . $i2; 
 
		if ( $k->{data_type} eq 'cv' || 
		     $k->{data_type} eq 'list' ) { 
                # selection 
		    my @db_vals = (); 
		    if ( $k->{data_type} eq 'cv' ) { 
			@db_vals = db_getValues($k->{cv_query}); 
		    } 
		    elsif ( $k->{data_type} eq 'list' ) {
			@db_vals = split(/\|/, $k->{list_values});
		    } 

		    if ( ! $attr_val && $k->{default_value} ) {
			$attr_val = $k->{default_value}; 
		    } 

		    print "  <td class='img'   align='left'>\n";
		    print "<select name='$fld_name' class='img' size='1'>\n";
		    if ( ! $k->{is_required} ) {
			print "   <option value=''> </option>\n";
		    } 
		    for my $val ( sort @db_vals ) {
			print "   <option value='" . escapeHTML($val) . "'";
			if ( $val eq $attr_val ) {
			    print " selected ";
			}
			print ">" . escapeHTML($val) . "</option>\n";
		    } 
		    print "</select></td>\n";
		} 
		else {
		    my $len = $k->{length};
		    my $size = $default_size;
		    if ( $k->{default_size} ) {
			$size = $k->{default_size};
		    }
 
		    if ( $len && $size > $len ) { 
			$size = $len;
		    } 
 
                print "  <td class='img'   align='left'>" .
                    "<input type='text' name='$fld_name'  value='" .
                    escapeHTML($attr_val) . 
                    "' size='$size' maxLength='$len'/>" . 
                    "</td>\n";
		} 
 
		$j++; 
	    } 
 
	    print "</tr>\n"; 
	} 
	$cur->finish();
	$dbh->disconnect(); 
    }
 
    # show entry for new 
    for (my $j = 0; $j < $extra_row; $j++) {
        print "<tr class='img'>\n"; 
        for my $k ( @aux_attrs ) {
            if ( ! $k->{can_edit} ) {
                next; 
            }
 
            my $fld_name = $tname . "|" . $k->{name} . "|" . $i2;
 
            if ( $k->{data_type} eq 'cv' ||
                 $k->{data_type} eq 'list' ) {
                # selection
                my @db_vals = ();
                if ( $k->{data_type} eq 'cv' ) {
                    @db_vals = db_getValues($k->{cv_query});
                } 
                elsif ( $k->{data_type} eq 'list' ) {
                    @db_vals = split(/\|/, $k->{list_values}); 
                } 

                print "  <td class='img'   align='left'>\n";
                print "<select name='$fld_name' class='img' size='1'>\n";
                if ( ! $k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }
                for my $val ( sort @db_vals ) {
                    print "   <option value='" . escapeHTML($val) . "'";
                    print ">" . escapeHTML($val) . "</option>\n";
                } 
                print "</select></td>\n";
            } 
            else { 
                my $len = $k->{length};
                my $size = $default_size;
                if ( $k->{default_size} ) { 
                    $size = $k->{default_size};
                } 
 
                if ( $len && $size > $len ) {
                    $size = $len;
                } 
                print "  <td class='img'   align='left'>" . 
                    "<input type='text' name='$fld_name'  value='' " .
                    "' size='$size' maxLength='$len'/>" . "</td>\n";
            } 
        } 
        print "</tr>\n"; 
 
        $i2++; 
    } 

    print "</table>\n"; 

    my $s2 = "_count_" . $tname;
    print hiddenVar($s2, $i2);
} 



#############################################################################
# dbNewSample - insert a new sample into database
#############################################################################
sub getNewSampleId { 
    my $dbh = Connect_IMG(); 
    my $id0 = 10000; 
 
    # SQL statement 
    my $sql = "select max(sample_oid) from env_sample " . 
        "where sample_oid <= " . $id0; 
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    my $max_id = 0; 
    for (;;) { 
        my ( $val ) = $cur->fetchrow( ); 
        last if !$val; 
 
        # set max ID 
        $max_id = $val; 
    } 
 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( $max_id >= $id0 ) { 
        $max_id = db_findMaxID('env_sample', 'sample_oid'); 
    } 
 
    $max_id++; 
    return $max_id; 
} 
 

sub dbNewSample {
    # get user info
    my $contact_oid = getContactOid();

    # get Env_Sample definition
    my $def_sample = def_Env_Sample();
    my @attrs = @{$def_sample->{attrs}};

    my $msg = "";

    my $sample_oid = 0; 
    my $gold_id = param1('gold_id'); 
    my $project_info = param1('project_oid');
    if ( blankStr($project_info) ) {
	return "No project has been selected.";
    }

    if ( blankStr($gold_id) && $project_info <= 10000 ) { 
        # no GOLD id is given 
	$sample_oid = getNewSampleId();
    } 
    else { 
        # use higher range sample oid for GOLD entry
	$sample_oid = db_findMaxID('env_sample', 'sample_oid') + 1;
    } 
    if ( $sample_oid <= 0 ) {
        # error checking -- this shouldn't happen
        return "Incorrect sample ID value: $sample_oid";
    } 

    my $ins = "insert into env_sample (sample_oid, project_info, contact, add_date";
    my $vals = "values ($sample_oid, $project_info, $contact_oid, sysdate";

    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $edit = $k->{can_edit};

	if ( ! $edit ) {
	    next;
	}

# get the names from the ecosystem classification ids
	my %ecosystemMap;
	my $dbh=WebFunctions::Connect_IMG;
	my $ecoSQL = qq{select mc_node_id, node from metagenomic_class_nodes};
	my $sth = $dbh->prepare($ecoSQL) or die "Couldn't prepare statement: " . $dbh->errstr;
	$sth->execute() or die "Couldn't execute statement: " . $sth->errstr;
	
	my @results = @{$sth->fetchall_arrayref()};
	for my $row (@results) {
	    my ($mc_node_id, $node )  = @$row;
	    $ecosystemMap{$mc_node_id} = $node;
	}
	my $val = '';
	if (($attr_name eq 'ecosystem') || ($attr_name eq 'ecosystem_category') || ($attr_name eq 'ecosystem_type') || ($attr_name eq 'ecosystem_subtype') || ($attr_name eq 'specific_ecosystem')) {
	    $val = $ecosystemMap{param1($attr_name)};
	} else {
        $val = param1($attr_name); 
    }


#	if ( blankStr($val) ) {
	if ( ! (defined $val) || length($val) == 0 ) {
	    next;
	}

	$ins .= ", " . $attr_name;

	if ( $data_type eq 'int' ) {
	    if ( ! blankStr($val) && ! isInt($val) ) {
		$msg = "$disp_name must be an integer.";
		last;
	    }

	    $vals .= ", " . $val;
	}
	else {
	    $val =~ s/'/''/g;   # replace ' with ''
	    $vals .= ", '" . $val . "'";
	}
    }

    if ( ! blankStr($msg) ) {
	return $msg;
    }

    my @sqlList = ();
    my $sql = $ins . ") " . $vals . ")";
    push @sqlList, ( $sql );

    # insert set-valued attribute
    my $isAdmin = getIsAdmin($contact_oid);
    my @setnames = getSampleAuxTables();
    if ( $isAdmin eq 'Yes' ) {
	push @setnames, ( 'env_sample_jgi_url' );
    }
    for my $sname ( @setnames ) {
	$msg = dbUpdateSampleSetValues($sample_oid, $sname, 0, \@sqlList);
	if ( ! blankStr($msg) ) {
	    return $msg;
	}
    }

    if ( $is_test ) {
	for $sql ( @sqlList ) {
	    $msg .= " SQL: " . $sql;
	}
    }
    else {
	db_sqlTrans(\@sqlList);
    }

    return $msg;
}


#############################################################################
# UpdateSample - update sample
#############################################################################
sub UpdateSample {
    my ( $sample_oid ) = @_;

    # get database value
    my %db_val = SelectEnvSample($sample_oid);

    print start_form(-name=>'updateSample',-method=>'post',action=>"$section_cgi");
    my $currentPage = GoldMetaProj::getPage();
    print hiddenVar('sample_oid', $sample_oid);

    my $project_oid = param1('project_oid');
    if ( $project_oid ) {
	my $proj_name = db_getValue("select display_name from project_info where project_oid = $project_oid");
	print "<h3>Project $project_oid: (" . escapeHTML($proj_name) .
	    ")</h3>\n";
	print hiddenVar('project_oid', $project_oid);
    }

    print "<h2>Update Sample $sample_oid</h2>\n";

    # tab view
    TabHTML::printTabAPILinks("updateSample");
    my @tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4" );
    my @tabNames = 
        ( "Sample Information (*)", "Sequencing Information",
	  "Environmental Metadata", "Host Metadata" );

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
	push @tabIndex, ( "#tab5" );
	push @tabNames, ( "JGI Info" );
    }

    TabHTML::printTabDiv( "updateSample", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    print "<p>Required fields are marked with (*). Mouse over MIGS ID to see field description.</p>\n";

    printSampleTab("Sample Information", $sample_oid, \%db_val);
    print "</p></div>\n"; 

    # tab 2
    print "<div id='tab2'><p>\n"; 
    printSampleTab("Sequencing Information", $sample_oid, \%db_val);
    printSampleSetValTab ('env_sample_seq_method', $sample_oid);
    print "</p></div>\n"; 

    # tab 3
    print "<div id='tab3'><p>\n"; 
    printSampleTab("Environmental Metadata", $sample_oid, \%db_val);
    printSampleSetValTab ('env_sample_habitat_type', $sample_oid);
    printSampleSetValTab ('env_sample_phenotypes', $sample_oid);
#    printSampleSetValTab ('env_sample_misc_meta_data', $sample_oid);
    print "</p></div>\n"; 

    # tab 4
    print "<div id='tab4'><p>\n"; 
    printSampleTab("Host Metadata", $sample_oid, \%db_val);
#   printSampleSetValTab ('env_sample_diseases', $sample_oid);
#    printSampleSetValTab ('env_sample_metabolism', $sample_oid);
#    printSampleSetValTab ('env_sample_energy_source', $sample_oid);
    print "</p></div>\n"; 

    if ( $isAdmin eq 'Yes' ) {
	# tab 5
	print "<div id='tab5'><p>\n"; 
        printSampleTab("JGI", $sample_oid, \%db_val);
        printSampleSetValTab ('env_sample_jgi_url', $sample_oid);
        print "</p></div>\n"; 
    }

    TabHTML::printTabDivEnd();

    print "<p>\n"; 
    print '<input type="submit" name="_section_EnvSample:dbUpdateSample" value="Update" class="smbutton" />'; 
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="Cancel" class="smbutton" />';
 
    printHomeLink();
 
    print end_form(); 
 
}


###########################################################################
# dbUpdateSampleSetValues
#
# upd = 0 (new), = 1 (update)
###########################################################################
sub dbUpdateSampleSetValues {
    my ($sample_oid, $tname, $upd, $sqlList) = @_;

    my $msg = "";

    my $def_aux = def_Class($tname);
    if ( ! $def_aux ) {
	$msg = "Cannot find definition for $tname";
	return $msg;
    }
    my $extra_row = $def_aux->{new_rows}; 

    my $sql;

    if ( $upd ) {
	$sql = "delete from $tname where " . $def_aux->{id} .
	    " = $sample_oid";
	push @$sqlList, ( $sql );
    }

    # get count
    my $s2 = "_count_" . $tname;
    my $cnt = param1($s2);
    if ( blankStr($cnt) ) {
	$cnt = $extra_row;
    }

    my @aux_attrs = @{$def_aux->{attrs}};
    my $i = 0;
    while ( $i < $cnt ) {
	my $ins = "";
	my $vals = "";

	for my $k ( @aux_attrs ) {
	    if ( ! $k->{can_edit} ) {
		next;
	    }

	    my $fld_name = $tname . "|" . $k->{name} . "|" . $i;
	    my $attr_val = param1($fld_name);
#	    if ( blankStr($attr_val) ) {
	    if ( ! (defined $attr_val) || length($attr_val) == 0 ) {
		next;
	    }

	    if ( blankStr($ins) ) {
		$ins = "insert into $tname (" . $def_aux->{id} . ", ";
		$vals = "values ($sample_oid, ";
	    }
	    else {
		$ins .= ", ";
		$vals .= ", ";
	    }

	    $ins .= $k->{name};
	    if ( $k->{data_type} eq 'int' ||
		 $k->{data_type} eq 'number' ) {
		$vals .= $attr_val;
	    }
	    else {
		my $db_val = $attr_val;
		$db_val =~ s/'/''/g;   # replace ' with '';
		$vals .= "'" . $db_val . "'";
	    }
	}

	if ( ! blankStr($ins) && !blankStr($vals) ) {
	    $sql = $ins . ") " . $vals . ")";
	    push @$sqlList, ( $sql );
	}

	$i++;
    }

    return $msg;
}


############################################################################
# showSampleProject - show project associated with this sample
############################################################################
sub showSampleProject {
    my ( $sample_oid ) = @_;

    # check project
    my $dbh=WebFunctions::Connect_IMG;

    my $sql = "select p.project_oid, p.display_name" .
	" from env_sample s, project_info p" .
	" where s.project_info = p.project_oid" .
	" and s.sample_oid = $sample_oid";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my ( $project_oid, $project_name ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( $project_oid && isInt($project_oid) ) {
	print "<h4>Sample is associated with Project $project_oid";
	if ( !blankStr($project_name) ) {
	    print " (" . escapeHTML($project_name) . ")";
	}
	print "</h4>\n";
    }
    else {
	print "<h4>Sample is not associated with any project.</h4>\n";
    }
}


####################################################################
# select Env_Sample data given an ID
####################################################################
sub SelectEnvSample {
    my ( $sample_oid ) = @_;
 
    my %db_val; 
    if ( ! $sample_oid || ! isInt($sample_oid) ) {
	return %db_val;
    }

    my $dbh = Connect_IMG(); 
 
    my $db_id = $sample_oid;
 
    # get Env_Sample definition
    my $def_sample = def_Env_Sample();
    my @attrs = @{$def_sample->{attrs}};

    my $sql = "";
    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
	$attr_name = 's.' . $attr_name;

        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } 
        else {
            $sql .= ", " . $attr_name;
        }
    } 
 
    $sql .= " from env_sample s where s.sample_oid = $db_id";
 
    # print "<p>SQL: $sql</p>\n"; 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my @flds = $cur->fetchrow_array(); 
 
    # save result 
    my $j = 0; 
    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
        if ( scalar(@flds) < $j ) { 
            last;
        }
        $db_val{$attr_name} = $flds[$j];
        $j++; 
    } 
 
    # finish
    $cur->finish(); 
    $dbh->disconnect();

    # contact info
    if ( $db_val{'contact'} ) {
	$db_val{'contact'} = db_getContact($db_val{'contact'});
    }

    # modified_by
    if ( $db_val{'modified_by'} ) {
	$db_val{'modified_by'} = db_getContact($db_val{'modified_by'});
    }

    return %db_val; 
} 


#############################################################################
# dbUpdateSample - update sample info in database
#############################################################################
sub dbUpdateSample {
    # get user info
    my $contact_oid = getContactOid();

    # get Env_Sample definition
    my $def_sample = def_Env_Sample();
    my @attrs = @{$def_sample->{attrs}};

    my $msg = "";

    # get sample_oid
    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) || ! isInt($sample_oid) ) {
	$msg = "No sample has been selected for update.";
	return $msg;
    }

    my $sql = "update env_sample set modified_by = $contact_oid, " .
	"mod_date = sysdate";

    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $edit = $k->{can_edit};

	if ( ! $edit ) {
	    next;
	}

	my $val = param1($attr_name);

#	if ( blankStr($val) ) {
	if ( ! (defined $val) || length($val) == 0 ) {
	    $sql .= ", $attr_name = null";
	    next;
	}

#	if (($attr_name eq 'ecosystem') || ($attr_name eq 'ecosystem_category') || ($attr_name eq 'ecosystem_type') || ($attr_name eq 'ecosystem_subtype') || ($attr_name eq 'specific_ecosystem') ) {
# get the names from the ecosystem classification ids
#	my %ecosystemMap;
#	my $dbh=WebFunctions::Connect_IMG;
#	my $ecoSQL = qq{select mc_node_id, node from metagenomic_class_nodes};
#	my $sth = $dbh->prepare($ecoSQL) or die "Couldn't prepare statement: " . $dbh->errstr;
#	$sth->execute() or die "Couldn't execute statement: " . $sth->errstr;
	
#	my @results = @{$sth->fetchall_arrayref()};
#	for my $row (@results) {
#	    my ($mc_node_id, $node )  = @$row;
#	    $ecosystemMap{$mc_node_id} = $node;
#	}
#            $val = $ecosystemMap{$val};
#
#	}

	if ( $data_type eq 'int' ) {
	    if ( ! blankStr($val) && ! isInt($val) ) {
		$msg = "$disp_name must be an integer.";
		last;
	    }

	    $sql .= ", $attr_name = $val";
	}
	else {
	    $val =~ s/'/''/g;   # replace ' with ''
	    $sql .= ", $attr_name = '" . $val . "'";
	}
    }

    $sql .= " where sample_oid = $sample_oid";

    if ( ! blankStr($msg) ) {
	return $msg;
    }

    my @sqlList = ();
    push @sqlList, ( $sql );

    # set valued
    my $isAdmin = getIsAdmin($contact_oid);
    my @setnames = getSampleAuxTables();
    if ( $isAdmin eq 'Yes' ) {
	push @setnames, ( 'env_sample_jgi_url' );
    }

    for my $sname ( @setnames ) {
	$msg = dbUpdateSampleSetValues($sample_oid, $sname, 1, \@sqlList);
	if ( ! blankStr($msg) ) {
	    return $msg;
	}
    }

    if ( $is_test ) {
	for $sql ( @sqlList ) {
	    $msg .= " SQL: " . $sql;
	}
    }
    else {
	db_sqlTrans(\@sqlList);
    }

    return $msg;
}


#############################################################################
# DeleteSample - delete sample
#############################################################################
sub DeleteSample {
	
    print start_form(-name=>'deleteSample',-method=>'post',action=>"$section_cgi");

    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) || ! isInt($sample_oid) ) {
	printError("No sample has been selected.");
	print end_form();
	return;
    }

    print "<h2>Delete Sample $sample_oid</h2>\n";
    print hiddenVar('sample_oid', $sample_oid);
    print hiddenVar('project_oid', param1('project_oid'));

    # check submission 
    my $cnt = db_getValue("select count(*) from submission where sample_oid = $sample_oid");
    if ( $cnt > 0 ) {
        printError("You cannot delete a submitted sample.");
        print end_form(); 
        return; 
    } 

    # check whether there are any FK
    # show project, if any
    showSampleProject($sample_oid);

    print "<p>Please click the Delete button to confirm the deletion, or cancel the deletion.</p>\n";
    print "<p>\n";
    print '<input type="submit" name="_section_EnvSample:dbDeleteSample" value="Delete" class="smbutton" />'; 
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="Cancel" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();	
}


#############################################################################
# dbDeleteSample - delete sample from database
#############################################################################
sub dbDeleteSample {
    my $msg = "";

    # get sample_oid
    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) || ! isInt($sample_oid) ) {
	$msg = "No sample has been selected for deletion.";
	return $msg;
    }

    my @sqlList = ( );
    my $sql = "";
    $sql = "delete from env_sample_seq_method where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_misc_meta_data where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_diseases where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_habitat_type where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_metabolism where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_phenotypes where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_energy_source where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample_jgi_url where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "delete from env_sample where sample_oid = $sample_oid";
    push @sqlList, ( $sql );

    db_sqlTrans(\@sqlList);

    return $msg;
}


#############################################################################
# dbChangeSampleContact
#############################################################################
sub dbChangeSampleContact {
    # get user info 
    my $contact_oid = getContactOid();
 
    my $msg = "";
 
    # get sample_oid
    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) || ! isInt($sample_oid) ) {
        $msg = "No sample has been selected for update.";
        return $msg;
    }
 
    # get new contact 
    my $new_contact = param1('new_contact');
    if ( blankStr($new_contact) ) {
        $msg = "No new contact has been selected for update."; 
        return $msg; 
    } 
 
    my $sql = "update env_sample set modified_by = $contact_oid, " .
        "mod_date = sysdate, contact = $new_contact " . 
        "where sample_oid = $sample_oid";
 
    if ( ! blankStr($msg) ) {
        return $msg;
    } 
 
    my @sqlList = ( );
    push @sqlList, ( $sql );
 
    db_sqlTrans(\@sqlList);
 
    return $msg; 
} 


#########################################################################
# sampleFilterParams - get all sample filter parameters
#########################################################################
sub sampleFilterParams {
    my @all_params = ( 'sample_filter:sample_display_name',
                       'sample_filter:oxygen_req',
                       'sample_filter:seq_method',
		       'sample_filter:project_info',
		       'sample_filter:gold_id',
		       'sample_filter:add_date',
		       'sample_filter:mod_date' );

    return @all_params;
}


########################################################################## 
# FilterSample - set filter on displaying samples
#             so that there won't be too many to display 
########################################################################## 
sub FilterSample {
 
    print start_form(-name=>'filterSample',-method=>'post',action=>"$section_cgi");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid);
 
    print "<h3>Set Sample Filter for $uname</h3>\n";
 
    # max display
    my $max_display = getSessionParam('sample_filter:max_display');
    if ( blankStr($max_display) ) {
	$max_display = $default_max_sample_row;
    }
    print "<p>Maximal Rows of Display:\n";
    print nbsp(3); 
    print "<select name='sample_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
		   '300', '500', '800', '1000', '2000',
		   '3000', '4000', '5000', '10000' ) {
	print "    <option value='$cnt0' ";
	if ( $cnt0 eq $max_display ) {
	    print " selected ";
	}
	print ">$cnt0</option>\n";
    }
    print "</select>\n";

    # sample display name
    printSampleFilterCond('Sample Display Name',
		    'sample_filter:sample_display_name',
		    'text', '', '',
		    getSessionParam('sample_filter:sample_display_name'));

    # oxygen requirement
    printSampleFilterCond('Oxygen Requirement', 'sample_filter:oxygen_req',
		    'select', 'query',
		    'select cv_term from oxygencv order by cv_term',
		    getSessionParam('sample_filter:oxygen_req'));

    # seq method
    printSampleFilterCond('Sequencing Method', 'sample_filter:seq_method',
		    'select', 'query',
		    'select cv_term from seq_methodcv order by cv_term',
		    getSessionParam('sample_filter:seq_method'));

    # project ID
    my $sql = "select project_oid, project_oid || ' - ' || display_name " .
	"from project_info " .
	"where project_oid in (select project_info from env_sample) " .
	"order by project_oid";
    printSampleFilterCond('Project', 'sample_filter:project_info',
		    'select', 'query', $sql,
		    getSessionParam('sample_filter:gold_id'));

    # gold ID
    printSampleFilterCond('GOLD Stamp ID', 'sample_filter:gold_id',
		    'select', 'query',
		    'select distinct gold_id from env_sample order by gold_id',
		    getSessionParam('sample_filter:gold_id'));

    # add date 
    printSampleFilterCond('Add Date', 'sample_filter:add_date', 
			  'date', '', '',
			  getSessionParam('sample_filter:add_date'));
 
    # mod date 
    printSampleFilterCond('Last Mod Date', 'sample_filter:mod_date',
			  'date', '', '',
			  getSessionParam('sample_filter:mod_date'));
 

    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_EnvSample:applySampleFilter" value="Apply Sample Filter" class="medbutton" />';
 
    print "<p>\n";
 
    printHomeLink();
 
    print end_form(); 
}


###########################################################################
# printSampleFilterCond - print filter condition
###########################################################################
sub printSampleFilterCond {
    my ($title, $param_name, $ui_type, $select_type, $select_method,
	$def_val_str) = @_;

    print "<p>" . escapeHTML($title) . "\n";

    my @vals = ();
    my @def_vals = ();
    if ( ! blankStr($def_val_str) ) {
	@def_vals = split(/\,/, $def_val_str);
    }

    if ( $select_type eq 'query' ) {
	my $dbh = Connect_IMG();
webLog("$select_method\n");	
	my $cur = $dbh->prepare($select_method);
	$cur->execute(); 
 
	for (my $j = 0;$j <= 100000; $j++) { 
	    my ( $v2, $cv_term ) = $cur->fetchrow_array(); 
	    if ( ! $v2 && ! $cv_term ) { 
		last; 
	    } 
 
	    my $v3 = $v2 . "|" . $cv_term;
	    push @vals, ( $v3 );
	} 
	$cur->finish(); 
	$dbh->disconnect(); 
    }
    elsif ( $select_type eq 'list' ) {
	@vals = split(/\|/, $select_method);
    }

    if ( $ui_type eq 'text' ) {
	print nbsp(3); 
	my $val2 = $def_val_str;
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='60' maxLength='255'/>" . "</td>\n";
    }
    elsif ( $ui_type eq 'date' ) { 
        print nbsp(3); 
        my $op_name = $param_name . ":op"; 
        my $comp1 = getSessionParam($op_name); 
        print "<select name='$op_name' class='img' size='1'>\n"; 
        print "   <option value='0'> </option>\n"; 
        for my $k ( '=', '!=', '>', '>=', '<', '<=' ) { 
            my $k2 = escapeHTML($k); 
            print "   <option value='$k2'"; 
            if ( !blankStr($comp1) && $k eq $comp1 ) { 
                print " selected "; 
            } 
            print ">$k2</option>\n"; 
        } 
        print "</select></td>\n"; 
        print nbsp( 1 ); 
 
        my $val2 = $def_val_str;
        print "<input type='text' name='$param_name' value='$val2'" .
            " size='20' maxLength='20'/>" . "</td>\n";
        print nbsp(6); 
        print "<b>(Use 'DD-MON-YY' for Date format.)</b>\n";
    } 
    elsif ( $ui_type eq 'checkbox' ) {
	for my $s2 ( @vals ) {
	    my ($id2, $val2) = split(/\|/, $s2);
	    if ( blankStr($val2) ) {
		$val2 = $id2;
	    }

	    print "<br/>\n"; 
	    print nbsp(3); 
	    print "<input type='checkbox' name='$param_name' value='$id2'";
	    if ( scalar(@def_vals) == 0 ||
		 inArray($id2, @def_vals) ) {
		print " checked ";
	    }
	    print ">". escapeHTML($val2) . "\n"; 
	}
    }
    elsif ( $ui_type eq 'select' ) {
	# print "<br/>\n"; 
	print nbsp(3); 
	print "<select name='$param_name' class='img' size='1'>\n"; 
	print "    <option value='' > </option>\n";
	for my $s2 ( @vals ) {
	    my ($id2, $val2) = split(/\|/, $s2);
	    if ( blankStr($val2) ) {
		$val2 = $id2;
	    }

	    print "    <option value='" . escapeHTML($id2) . "'";
	    if ( scalar(@def_vals) > 0 &&
		 inArray($id2, @def_vals) ) {
		print " selected ";
	    }
	    print ">$val2</option>\n";
	}
	print "</select>\n";
    }
}


########################################################################## 
# ApplySampleFilter - save filter info into database and apply filter 
########################################################################## 
sub ApplySampleFilter { 
 
    print start_form(-name=>'applySampleFilter',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    # max display
    my $max_display = param1('sample_filter:max_display');
    if ( ! blankStr($max_display) ) {
	setSessionParam('sample_filter:max_display', $max_display);
    }

    # show filter selection 
    my @all_params = sampleFilterParams();
    for my $p0 ( @all_params ) {
        if ( $p0 eq 'sample_filter:add_date' ||
             $p0 eq 'sample_filter:mod_date' ) {
            # date
            my $op_name = $p0 . ":op";
            my $op1 = param1($op_name);
            my $d1 = param1($p0);
            if ( !blankStr($d1) && !blankStr($op1) ) { 
                if ( ! isDate($d1) ) { 
                    print "<p>Incorrect Date (" . escapeHTML($d1) . 
                        ") -- Filter condition is ignored.</p>\n";
                } 
                else {
                    setSessionParam($op_name, $op1); 
                    setSessionParam($p0, $d1);
                }
            }
	    else {
		# clear condition
		setSessionParam($op_name, ''); 
		setSessionParam($p0, '');
	    }
        } 
	elsif ( param($p0) ) { 
	    # filter
	    my @options = param($p0); 

#	    if ( scalar(@options) == 0 ) {
#		my ($tag1, $val1) = split(/\:/, $p0);
#		print "<p>No selection on $val1. Default is set to selecting all.</p>\n";
#	    }

	    my $s0 = "";
	    for my $s1 ( @options ) {
		if ( blankStr($s0) ) {
		    $s0 = $s1;
		}
		else {
		    $s0 .= "," . $s1;
		}
	    }
	    setSessionParam($p0, $s0);
	}
	else { 
	    my ($tag1, $val1) = split(/\:/, $p0);
#	    print "<p>No selection on $val1. Default is set to selecting all.</p>\n";
	    setSessionParam($p0, '');
	}
    }

    # save parameters
    print "<h4>Sample Filter is applied.</h4>\n"; 
 
    print "<p>\n"; 
 
    print '<input type="submit" name="_section_EnvSample:showPage" value="OK" class="smbutton" />';
 
    printHomeLink();
 
    print end_form();
} 


#########################################################################
# listProjectSamples_old - list samples of a project
##########################################################################
sub listProjectSamples_old {
    my ($project_oid) = @_;

    if ( blankStr($project_oid) ) {
	return 0;
    }

    my $dbh = Connect_IMG();

    my $sql = qq{
	select s.sample_oid, s.sample_display_name,
	s.gold_id, s.sample_site, s.add_date, s.mod_date
	    from env_sample s
	    where project_info = $project_oid
	    order by s.sample_oid
	};
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();

    print "<p>\n";
    print "<table class='img' border='1'>\n"; 
#    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>Sample ID</th>\n";
    print "<th class='img'>Sample Display Name</th>\n"; 
    print "<th class='img'>GOLD ID</th>\n"; 
    print "<th class='img'>Sample Site</th>\n"; 
    print "<th class='img'>Add Date</th>\n"; 
    print "<th class='img'>Last Mod Date</th>\n"; 

    my $selected_sample = param1('sample_oid');

    my $cnt = 0;
    for (;;) {
	my ( $sample_id, $sample_name, $gold_id, $site,
	     $add_date, $mod_date ) =
	    $cur->fetchrow_array();
	if ( ! $sample_id ) {
	    last;
	}

	$cnt++;

	print "<tr class='img'>\n";

#        print "<td class='img'>\n";
#        print "<input type='radio' ";
#        print "name='sample_oid' value='$sample_id'";
#	if ( $sample_id eq $selected_sample ) {
#	    print " checked ";
#	}
#        print "/></td>\n";

	PrintAttribute($sample_id);
	PrintAttribute($sample_name);

        my $gold_url = "http://genomesonline.org/cgi-bin/GOLD/bin/GOLDCards.cgi?goldstamp=$gold_id";
        my $gold_link = "<a href='" . $gold_url . 
            " target='view_link'>" .
            $gold_id . "</a>";
        PrintAttribute($gold_link);

	PrintAttribute($site);
	PrintAttribute($add_date);
	PrintAttribute($mod_date);
	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();

#    printSampleButtons();
    print hr();

    return $cnt;
}


########################################################################## 
# DisplaySample
########################################################################## 
sub DisplaySample { 
    my ($sample_oid) = @_; 
 
    print start_form(-name=>'displaySample',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 
 
    if ( ! $sample_oid || ! isInt($sample_oid) ) { 
        return; 
    } 

    # check whether user can view this sample
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'No' ) {
	printError ( "Obsolete page." );
	return;
    }

    my $sample_gold_id = db_getValue("select gold_id from env_sample where sample_oid = $sample_oid"); 
    if ( $isAdmin eq 'No' && ! $sample_gold_id ) { 
        # contact must be the owner of this sample
        my $sample_contact = db_getValue("select contact from env_sample where sample_oid = $sample_oid"); 
        if ( $sample_contact == $contact_oid ) { 
            # fine 
        } 
        else { 
            my $email = db_getValue("select email from contact where contact_oid = $contact_oid"); 
            my $pi_email = db_getValue("select p.contact_email from project_info p, env_sample s where s.sample_oid = $sample_oid and s.project_info = p.project_oid");
            if ( lc($email) eq lc($pi_email) ) {
                # fine -- PI
            } 
	    else {
		printError ( "You cannot view this sample.");
		return;
	    }
        } 
    }
 
    my $sample_name = db_getValue("select sample_display_name from env_sample where sample_oid = $sample_oid");
    print "<h2>Sample $sample_oid (" . escapeHTML($sample_name) .
	")</h2>\n"; 
 
    # get Env_sample definition 
    my $def_sample = def_Env_Sample(); 
    my @attrs = @{$def_sample->{attrs}}; 
 
    # get data from database 
    my %db_val = SelectEnvSample($sample_oid); 

    if ( $isAdmin eq 'Yes' ) {
	my $mod_by = $db_val{'modified_by'};
	if ( $mod_by =~ /IMG\_PIPELINE/ ) {
	    print "<h5>Created by IMG_PIPELINE from New GOLD API.</h5>\n";
	}
    }

    print "<p>\n"; 
    print "<p>\n"; 
    print "<table class='img' border='1'>\n";

    my @tabs =  ( 'Sample Information',
		  'Sequencing Information',
		  'Environmental Metadata', 'Host Metadata' );
    if ( $isAdmin eq 'Yes' ) {
	push @tabs, ( 'JGI' );
    }

    for my $tab ( @tabs ) {
	my $tab_name = $tab;
	if ( $tab eq 'JGI' ) {
	    $tab_name = 'JGI Info';
	}
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
            "<font color='darkblue'>" . "MIGS-ID" . "</font></th>\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
            "<font color='darkblue'>" . $tab_name . "</font></th>\n";
        print "  <td class='img'   align='left' bgcolor='lightblue'>" .
	    "</td>\n";
        print "</tr>\n";
 
        for my $k ( @attrs ) {
            if ( $k->{tab} ne $tab ) { 
                next;
            } 
 
            my $attr_val = "";
            my $attr_name = $k->{name};
 
            if ( $db_val{$attr_name} ) {
                $attr_val = $db_val{$attr_name};
            } 
 
            if ( ! $attr_val || blankStr($attr_val) ) {
                next;
            }
 
            my $disp_name = $k->{display_name};
 
            print "<tr class='img' >\n";

            # MIGS 
            printCellTooltip($k->{migs_id}, $k->{migs_name}, 70); 
 
            # IMG-GOLD attribute 
            print "  <th class='subhead' align='right'>" . $disp_name .
                "</th>\n";
 
            if ( $attr_name eq 'gold_id' ) {
                # gold 
                print "  <td class='img'   align='left'>" .
                    getGoldLink($attr_val) . "</td>\n";
            }
            elsif ( $attr_name eq 'project_info' ) {
                print "  <td class='img'   align='left'>" .
                    getProjectLink($attr_val) . "</td>\n";
            }
            elsif ( $attr_name eq 'ncbi_project_id' ) {
                # NCBI project
                print "  <td class='img'   align='left'>" .
                    getNcbiProjLink($attr_val) . "</td>\n";
            }
            elsif ( $attr_val =~ /^http\:\/\// ||
		    $attr_val =~ /^https\:\/\// ) {
                # url 
                print "  <td class='img'   align='left'>" .
                    alink($attr_val, 'URL', 'target', 1) . "</td>\n";
            } 
            else {
                # regular attribute value 
                print "  <td class='img'   align='left'>" .
                    escapeHTML($attr_val) . "</td>\n";
            } 
            print "</tr>\n"; 
        } 

	if ( $tab eq 'JGI' ) {
	    # show project jgi_proposal_id, if any
	    my $jgi_proposal_id = '';
	    if ( $sample_oid ) {
		$jgi_proposal_id = db_getValue("select p.jgi_proposal_id from project_info p, env_sample s where s.project_info = p.project_oid and s.sample_oid = $sample_oid");
	    }
	    if ( $jgi_proposal_id ) {
		print "<tr class='img' >\n";
		print "<td class='img'></td>\n";
		print "  <th class='subhead' align='right'>" . "Project JGI Proposal ID" .
		    "</th>\n";
		print "<td class='img'>$jgi_proposal_id</td>\n";
		print "</tr>\n";
	    }
	}
	if ( $tab eq 'Links' ) {
	    DisplaySampleSetAttr($sample_oid, 'env_sample_meta_data',
				 ': ', '<br/>');
	}
    }  # end for tab
 
    if ( $isAdmin eq 'Yes' ) {
        DisplaySampleSetAttr($sample_oid, 'env_sample_jgi_url', ', ', ', ');
    }

    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	"<font color='darkblue'>" . "MIGS-ID" . "</font></th>\n";
    print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	"<font color='darkblue'>" . "Additional Info" . "</font></th>\n";
    print "  <td class='img'   align='left' bgcolor='lightblue'>" .
	"</td>\n";
    print "</tr>\n";

    for my $tname ( 'env_sample_seq_method',
                    'env_sample_diseases',
                    'env_sample_habitat_type',
                    'env_sample_metabolism',
                    'env_sample_phenotypes',
                    'env_sample_energy_source' ) {
        DisplaySampleSetAttr($sample_oid, $tname, ', ', ', ');
    } 

    print "</table>\n"; 
 
    printHomeLink(); 
 
    print end_form(); 
}

 
########################################################################## 
# DisplaySampleSetAttr 
# 
# aux_name: set-valued table name 
# t1: field/attr separator 
# t2: record/row separator 
########################################################################## 
sub DisplaySampleSetAttr { 
    my ($sample_oid, $aux_name, $t1, $t2) = @_; 
 
    my $def_aux = def_Class($aux_name); 
    if ( ! $def_aux ) { 
        return; 
    } 
 
    my $id_attr = $def_aux->{id}; 
    my $sql = "select $id_attr"; 
    my $order_by = " order by $id_attr"; 
 
    my @aux_attrs = @{$def_aux->{attrs}}; 
    for my $attr ( @aux_attrs ) { 
        if ( $attr->{name} eq $id_attr ) { 
            next; 
        } 
 
        $sql .= ", " . $attr->{name}; 
        $order_by .= ", " . $attr->{name}; 
    } 
 
    $sql .= " from " . $aux_name . " where " . $id_attr . 
        " = " . $sample_oid . $order_by; 
    my $dbh = Connect_IMG(); 
webLog("$sql\n");    
    my $cur = $dbh->prepare($sql); 
    $cur->execute(); 
 
    my $j = 0; 
    for ($j = 0;$j <= 100000; $j++) { 
        my ($id_val, @vals) = $cur->fetchrow_array(); 
        last if !$id_val; 
 
        if ( $j == 0 ) { 
            # first value
            print "<tr class='img' >\n";
	    printCellTooltip($def_aux->{migs_id},
			     $def_aux->{migs_name}, 70);

            print "  <th class='subhead' align='right'>" .
                $def_aux->{display_name} . "</th>\n";
            print "  <td class='img'   align='left'>";
        } 
        else { 
            # print row separator 
            print $t2;
        } 
 
        for (my $k = 0; $k < scalar(@vals); $k++ ) {
            my $val = $vals[$k];
 
            if ( $val =~ /^http\:\/\// ||
		 $val =~ /^https\:\/\// ) {
                # url 
                print alink($val, 'URL', 'target', 1);
            }
            else { 
                print escapeHTML($val);
            } 
            if ( $k < scalar(@vals) - 1 ) {
                print $t1;
            }
        } 
    } 
 
    if ( $j > 0 ) {
        print "</td></tr>\n"; 
    } 
 
    $cur->finish(); 
    $dbh->disconnect();
}
 

#############################################################################
# MoveSample - move sample
#############################################################################
sub MoveSample {
    print start_form(-name=>'moveSample',-method=>'post',action=>"$section_cgi");

    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) || ! isInt($sample_oid) ) {
	printError("No sample has been selected.");
	print end_form();
	return;
    }

    print "<h2>Move Sample $sample_oid</h2>\n";
    print hiddenVar('sample_oid', $sample_oid);
    print hiddenVar('project_oid', param1('project_oid'));

    # check submission 
    my $cnt = db_getValue("select count(*) from submission where sample_oid = $sample_oid");
    if ( $cnt > 0 ) {
#        printError("You cannot move a submitted sample.");
#        print end_form(); 
#        return; 

	print "<p>This sample is associated with $cnt submission(s).\n";
    } 

    # check whether there are any FK
    # show project, if any
    showSampleProject($sample_oid);
    ShowKeywordSearchMoveSample();
    print end_form();	
}


#############################################################################
# CopyToOtherSample - copy sample to another project
#############################################################################
sub CopyToOtherSample {
    print start_form(-name=>'copyToOtherSample',-method=>'post',action=>"$section_cgi");

    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) || ! isInt($sample_oid) ) {
	printError("No sample has been selected.");
	print end_form();
	return;
    }

    print "<h2>Copy Sample $sample_oid</h2>\n";
    print hiddenVar('sample_oid', $sample_oid);
    print hiddenVar('project_oid', param1('project_oid'));

    # check submission 
    my $cnt = db_getValue("select count(*) from submission where sample_oid = $sample_oid");
    if ( $cnt > 0 ) {
        printError("You cannot copy a submitted sample.");
        print end_form(); 
        return; 
    } 

    # check whether there are any FK
    # show project, if any
    showSampleProject($sample_oid);
    ShowKeywordSearchCopySample();
    print end_form();	
}


######################################################################### 
# dbMoveSample
######################################################################### 
sub dbMoveSample { 
    my ($sample_oid) = @_; 
 
    # check sample oid 
    if ( blankStr($sample_oid) || ! $sample_oid ) { 
        printError("No sample has been selected."); 
        print end_form(); 
        return; 
    } 

    my $to_project_oid = param1('to_project_oid'); 
 
    print start_form(-name=>'moveSample',-method=>'post',action=>"$section_cgi"); 
  
    if ( ! $to_project_oid ) {
        printError("No project has been selected."); 
        print end_form(); 
        return; 
    } 
			       
    print "<h2>Move Sample $sample_oid to Project $to_project_oid</h2>\n"; 
    print hiddenVar('sample_oid', $sample_oid); 
    print hiddenVar('project_oid', $to_project_oid);

    my $contact_oid = getContactOid(); 

    my @sqlList = ();
    my $sql = "update env_sample set project_info = $to_project_oid where sample_oid = $sample_oid";
    push @sqlList, ( $sql );
    $sql = "update submission set project_info = $to_project_oid where sample_oid = $sample_oid";
    push @sqlList, ( $sql );

    db_sqlTrans(\@sqlList);

#    print hiddenVar('project_oid', param1('project_oid'));

    print "<p>Sample has been successfully moved.\n";

    print "<p>\n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="OK" class="smbutton" />';
 
    print "<p>\n";
    printHomeLink();
 
    print end_form();
}

sub dbMoveSample_old { 
    my ($sample_oid) = @_; 
 
    # check sample oid 
    if ( blankStr($sample_oid) ) { 
        printError("No sample has been selected."); 
        print end_form(); 
        return; 
    } 

    my $gold_stamp_id = db_getValue("select gold_id from env_sample where sample_oid = $sample_oid"); 
#    my $project_oid = db_getValue("select project_info from env_sample where sample_oid = $sample_oid"); 
    my $to_project_oid = param1('to_project_oid'); 
 
    print start_form(-name=>'moveSample',-method=>'post',action=>"$section_cgi"); 
  
    print "<h2>Move Sample $sample_oid</h2>\n"; 
    print hiddenVar('sample_oid', $sample_oid); 

    # get new sample oid 
    my $new_samp_oid = 0; 
    if ( $gold_stamp_id ) { 
        # GOLD project 
        # use higher range sample oid for GOLD entry 
        $new_samp_oid = db_findMaxID('env_sample', 'sample_oid') + 1; 
    } 
    else { 
        $new_samp_oid = getNewSampleId(); 
    } 
    if ( $new_samp_oid <= 0 ) { 
        # error checking -- this shouldn't happen 
        printError("Incorrect new sample ID value: $new_samp_oid");
        print end_form(); 
        return; 
    } 
 
    print "<h3>New Sample ID: $new_samp_oid</h3>\n";
 
#    $new_samp_oid = 2; 
 
    my $contact_oid = getContactOid(); 

    dbCopyOneSample($new_samp_oid, $to_project_oid, $sample_oid, $contact_oid);
    dbDeleteSample();

    print "<p>New sample has been created by moving sample " . 
        $sample_oid . ".</p>\n"; 
    print hiddenVar('project_oid', param1('project_oid'));

    print "<p>\n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="OK" class="smbutton" />';
 
    print "<p>\n";
    printHomeLink();
 
    print end_form();
}



###########################################################################
# ShowKeywordSearchMoveSample
###########################################################################
sub ShowKeywordSearchMoveSample {

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    print "<h2>Search Metagenome Project to move Sample to</h2>\n";

    print hiddenVar('proj_search_option', 'keyword');

    print "<h5>Text searches are based on case-insensitive substring match.</h5>\n"; 
    print "<table class='img' border='1'>\n"; 

    # search filter
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>Search Filter</td>\n";
    print "<td class='img'>\n";
    print "<select name='search_filter' class='img' size='1'>\n";
    my $s0;

my @sel_fields = ( 'Project name', 'GOLD ID', 'Ecosystem', 
			'Ecosystem category', 'Ecosystem type',
			'Ecosystem subtype', 'Specific ecosystem');
    for $s0 ( @sel_fields ) {
	print "    <option value='" . escapeHTML($s0) . "'"; 
	print ">$s0</option>\n";
    }
    print "</select>\n";
    print "</td></tr>\n";
    print "</td></tr>\n";

    # search keyword
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>Search Keyword</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_keyword' value=''" .
	" size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    print "</table>\n";
 
	print '<input type="submit" name="_section_EnvSample:selectMoveSample" value="Search Projects" class="meddefbutton" />';


    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";

    print "<p>\n";
    printHomeLink(); 

    print end_form();
}


###########################################################################
# ShowKeywordSearchCopySample
###########################################################################
sub ShowKeywordSearchCopySample {

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    print "<h2>Search Metagenome Project to Copy Sample to</h2>\n";

    print hiddenVar('proj_search_option', 'keyword');

    print "<h5>Text searches are based on case-insensitive substring match.</h5>\n"; 
    print "<table class='img' border='1'>\n"; 

    # search filter
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>Search Filter</td>\n";
    print "<td class='img'>\n";
    print "<select name='search_filter' class='img' size='1'>\n";
    my $s0;

my @sel_fields = ( 'Project name', 'GOLD ID', 'Ecosystem', 
			'Ecosystem category', 'Ecosystem type',
			'Ecosystem subtype', 'Specific ecosystem');
    for $s0 ( @sel_fields ) {
	print "    <option value='" . escapeHTML($s0) . "'"; 
	print ">$s0</option>\n";
    }
    print "</select>\n";
    print "</td></tr>\n";
    print "</td></tr>\n";

    # search keyword
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>Search Keyword</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_keyword' value=''" .
	" size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    print "</table>\n";
 

	print '<input type="submit" name="_section_EnvSample:selectCopySample" value="Search Projects" class="meddefbutton" />';


    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";

    print "<p>\n";
    printHomeLink(); 

    print end_form();
}


###########################################################################
# ShowSelectCopySample  (use HTML table)
###########################################################################
sub ShowSelectCopySample {

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h2>Project Search Result </h2>\n"; 

    my $database = param1('database');
    print hiddenVar('database', $database);
    my $sample_oid = param1('sample_oid');
    print hiddenVar('sample_oid', $sample_oid); 

    my $proj_search_option = param1('proj_search_option');
    my $cond = getKeywordProjectSearchCond($isAdmin, $contact_oid);


    # show results
    my $dbh = Connect_IMG();
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.mod_date
            from project_info p 
        };
 
    if ( ! blankStr($cond) ) {
        $sql .= $cond;
    } 
    $sql .= " order by p.project_oid";

#    if ( $contact_oid == 312 ) {
#	print "<p>SQL: $sql</p>";
#    }

    print "<input type='submit' name='_section_EnvSample:dbMoveSample' value='Move Sample' class='meddefbutton' />";

    # show search results
    print "<p>\n";
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my $color1 = '#eeeeee';

    print "<p>\n"; 
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";
    print "<th class='img' bgcolor='$color1'>ER Project ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Project Display Name</th>\n";
    print "<th class='img' bgcolor='$color1'>GOLD ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Project Category</th>\n";
    print "<th class='img' bgcolor='$color1'>Add Date</th>\n";
    print "<th class='img' bgcolor='$color1'>Last Mod Date</th>\n";
 
    my $cnt2 = 0; 
    my $too_many = 0;

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id,
             $phylo, $add_date, $mod_date ) =
                 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last;
        } 
 
	$cnt2++; 
 
	if ( $cnt2 % 2 ) {
	    print "<tr class='img'>\n";
	}
	else {
	    print "<tr class='img' bgcolor='#ddeeee'>\n";
	}
 
	print "<td class='img'>\n";
	print "<input type='radio' "; 
	print "name='project_oid' value='$proj_id' />";
	print "</td>\n"; 
 
	my $proj_link = getProjectLink($proj_id);
	PrintAttribute($proj_link); 
 
	PrintAttribute($proj_name); 
	my $gold_link = getGoldLink($gold_stamp_id);
	PrintAttribute($gold_link); 
 
	PrintAttribute($phylo);
 
	PrintAttribute($add_date);
	PrintAttribute($mod_date); 
	print "</tr>\n";
 
	if ( $cnt2 >= 20000 ) { 
	    $too_many = 1;
	    last; 
	} 
    }
    print "</table>\n"; 
 
    print "<p>\n";
    if ( $too_many ) {
	print "<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    } 
    else { 
	print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    } 
 
    $cur->finish();
    $dbh->disconnect(); 

    print "<p>\n";

    if ( $cnt2 == 0 ) {
	printHomeLink(); 
	print end_form();
	return;
    }

    print "<input type='submit' name='_section_EnvSample:dbCopySample' value='Copy Sample' class='meddefbutton' />";

    print "<p>\n";
    printHomeLink(); 

    print end_form();
}

###########################################################################
# ShowSelectMoveSample  (use HTML table)
###########################################################################
sub ShowSelectMoveSample {

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h2>Project Search Result </h2>\n"; 

    my $database = param1('database');
    print hiddenVar('database', $database);
    my $sample_oid = param1('sample_oid');
    print hiddenVar('sample_oid', $sample_oid);

    my $proj_search_option = param1('proj_search_option');
    my $cond = getKeywordProjectSearchCond($isAdmin, $contact_oid);


    # show results
    my $dbh = Connect_IMG();
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.mod_date
            from project_info p 
        };
 
    if ( ! blankStr($cond) ) {
        $sql .= $cond;
    } 
    $sql .= " order by p.project_oid";

#    if ( $contact_oid == 312 ) {
#	print "<p>SQL: $sql</p>";
#    }

    print "<input type='submit' name='_section_EnvSample:dbMoveSample' value='Move Sample' class='meddefbutton' />";

    # show search results
    print "<p>\n";
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my $color1 = '#eeeeee';

    print "<p>\n"; 
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";
    print "<th class='img' bgcolor='$color1'>ER Project ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Project Display Name</th>\n";
    print "<th class='img' bgcolor='$color1'>GOLD ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Project Category</th>\n";
    print "<th class='img' bgcolor='$color1'>Add Date</th>\n";
    print "<th class='img' bgcolor='$color1'>Last Mod Date</th>\n";
 
    my $cnt2 = 0; 
    my $too_many = 0;

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id,
             $phylo, $add_date, $mod_date ) =
                 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last;
        } 
 
	$cnt2++; 
 
	if ( $cnt2 % 2 ) {
	    print "<tr class='img'>\n";
	}
	else {
	    print "<tr class='img' bgcolor='#ddeeee'>\n";
	}
 
	print "<td class='img'>\n";
	print "<input type='radio' "; 
	print "name='to_project_oid' value='$proj_id' />";
	print "</td>\n"; 
 
	my $proj_link = getProjectLink($proj_id);
	PrintAttribute($proj_link); 
 
	PrintAttribute($proj_name); 
	my $gold_link = getGoldLink($gold_stamp_id);
	PrintAttribute($gold_link); 
 
	PrintAttribute($phylo);
 
	PrintAttribute($add_date);
	PrintAttribute($mod_date); 
	print "</tr>\n";
 
	if ( $cnt2 >= 20000 ) { 
	    $too_many = 1;
	    last; 
	} 
    }
    print "</table>\n"; 
 
    print "<p>\n";
    if ( $too_many ) {
	print "<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    } 
    else { 
	print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    } 
 
    $cur->finish();
    $dbh->disconnect(); 

    print "<p>\n";

    if ( $cnt2 == 0 ) {
	printHomeLink(); 
	print end_form();
	return;
    }

    print "<input type='submit' name='_section_EnvSample:dbMoveSample' value='Move Sample' class='meddefbutton' />";

    print "<p>\n";
    printHomeLink(); 

    print end_form();
}

###########################################################################
# getKeywordProjectSearchCond
###########################################################################
sub getKeywordProjectSearchCond {
    my ($isAdmin, $contact_oid) = @_;

    my $cond = "";

    if ( $isAdmin ne 'Yes' ) {
	$cond = " where ( p.contact_oid = $contact_oid" .
	    " or p.gold_stamp_id is not null )";
    }

    # condition on domain
    my $domain_cond = "p.domain = 'MICROBIAL'";

    if ( blankStr($cond) ) {
	$cond = " where " . $domain_cond;
    }
    else {
	$cond .= " and " . $domain_cond;
    }

    my $search_filter = param1('search_filter');
    my $search_keyword = param1('search_keyword');

    if ( blankStr($search_filter) || blankStr($search_keyword) ) {
	return $cond;
    }

    if ( $search_filter eq 'Project name' ) {
	$cond .= " and lower(p.display_name) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( $search_filter eq 'GOLD ID' ) {
	$cond .= " and lower(p.gold_stamp_id) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( $search_filter eq 'Genus' ) {
	$cond .= " and lower(p.genus) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( $search_filter eq 'Species' ) {
	$cond .= " and lower(p.species) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( lc($search_filter) eq 'ecosystem' ) {
	$cond .= " and lower(p.ecosystem) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( lc($search_filter) eq 'ecosystem category' ) {
	$cond .= " and lower(p.ecosystem_category) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( lc($search_filter) eq 'ecosystem type' ) {
	$cond .= " and lower(p.ecosystem_type) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( lc($search_filter) eq 'ecosystem subtype' ) {
	$cond .= " and lower(p.ecosystem_subtype) like '%" . lc($search_keyword) . "%'";
    }
    elsif ( lc($search_filter) eq 'specific ecosystem' ) {
	$cond .= " and lower(p.specific_ecosystem) like '%" . lc($search_keyword) . "%'";
    }

    return $cond;
}




#############################################################################
# CopySample - copy sample
#############################################################################
sub CopySample {
	
    print start_form(-name=>'copySample',-method=>'post',action=>"$section_cgi");

    my $sample_oid = param1('sample_oid');
    if ( blankStr($sample_oid) ) {
	printError("No sample has been selected.");
	print end_form();
	return;
    }

    print "<h2>Copy Sample $sample_oid</h2>\n";
    print hiddenVar('sample_oid', $sample_oid);
    my $project_oid = db_getValue("select project_info from env_sample where sample_oid = $sample_oid"); 
    print hiddenVar('project_oid', $project_oid);
#    print hiddenVar('to_project_oid', param1('to_project_oid'));

    # check submission 
    my $cnt = db_getValue("select count(*) from submission where sample_oid = $sample_oid");
    if ( $cnt > 0 ) {
        printError("You cannot copy a submitted sample.");
        print end_form(); 
        return; 
    } 

    # check whether there are any FK
    # show project, if any
#    showSampleProject($sample_oid);

    print "<p>Please click the Copy button to confirm the copy of this sample for the destination project, or cancel the copy.</p>\n";
    print "<p>\n";
    print '<input type="submit" name="_section_EnvSample:dbCopySample" value="Copy to same Project" class="smbutton" />'; 
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:copyToOtherSample" value="Copy to other Project" class="smbutton" />'; 
    print "&nbsp; \n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="Cancel" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();	
}


######################################################################### 
# dbCopySample
######################################################################### 
sub dbCopySample { 
    my ($sample_oid, $to_project_oid) = @_; 
 
    # check sample oid 
    if ( blankStr($sample_oid) ) { 
        printError("No sample has been selected."); 
        print end_form(); 
        return; 
    } 

    if ( ! $to_project_oid ) {
        printError("No project has been selected."); 
        print end_form(); 
        return; 
    } 

    my $gold_stamp_id = db_getValue("select gold_id from env_sample where sample_oid = $sample_oid"); 
 
    print start_form(-name=>'copySample',-method=>'post',action=>"$section_cgi"); 
  
    print "<h2>Copy Sample $sample_oid</h2>\n"; 
    print hiddenVar('sample_oid', $sample_oid); 
 
    # get new sample oid 
    my $new_samp_oid = 0; 
    if ( $gold_stamp_id ) { 
        # GOLD project 
        # use higher range sample oid for GOLD entry 
        $new_samp_oid = db_findMaxID('env_sample', 'sample_oid') + 1; 
    } 
    else { 
        $new_samp_oid = getNewSampleId(); 
    } 
    if ( $new_samp_oid <= 0 ) { 
        # error checking -- this shouldn't happen 
        printError("Incorrect new sample ID value: $new_samp_oid");
        print end_form(); 
        return; 
    } 
 
    print "<h3>New Sample ID: $new_samp_oid</h3>\n";
 
#    $new_samp_oid = 2; 
 
    my $contact_oid = getContactOid(); 

    dbCopyOneSample($new_samp_oid, $to_project_oid, $sample_oid, $contact_oid);

    print "<p>New sample has been created by copying sample " . 
        $sample_oid . ".</p>\n"; 

    print "<p>\n";
    print '<input type="submit" name="_section_EnvSample:showSamples" value="OK" class="smbutton" />';
 
    print "<p>\n";
    printHomeLink();
 
    print end_form();
}



######################################################################### 
# dbCopyOneSample
######################################################################### 
sub dbCopyOneSample { 
    my ($new_samp_oid, $project_oid, $sample_oid, $contact_oid) = @_; 

    my @sqlList = ();

    my $ins = "insert into env_sample(sample_oid, project_info, contact, add_date";
    my $sel = "select $new_samp_oid, $project_oid, $contact_oid, sysdate"; 
 
    # get Env_Sample definition 
    my $def_sample = def_Env_Sample(); 
    my @attrs = @{$def_sample->{attrs}};
    for my $attr ( @attrs ) {
        if ( $attr->{name} =~ /^ncbi\_/ ) {
            # ok - NCBI taxon info
        } 
        elsif ( ! $attr->{can_edit} ) { 
            # skip non-editable attribute 
            next;
        } 
 
        $ins .= ", " . $attr->{name};
        $sel .= ", " . $attr->{name};
    } 
 
    my $sql = $ins . ") " . $sel . " from env_sample " .
        "where sample_oid = " . $sample_oid;
   push @sqlList, ( $sql );

    # set-valued attributes 
    my @tables = getSampleAuxTables(); 
    push @tables, ( 'env_sample_jgi_url' );

    for my $tname ( @tables ) { 
        my $def_aux = def_Class($tname); 
        if ( ! $def_aux ) { 
            next; 
        } 
 
        my @aux_attrs = @{$def_aux->{attrs}}; 
        $ins = "insert into $tname (" . $def_aux->{id}; 
        $sel = "select $new_samp_oid"; 
        for my $attr2 ( @aux_attrs ) { 
            if ( $attr2->{name} eq $def_aux->{id} ) { 
                next; 
            } 
 
            $ins .= ", " . $attr2->{name}; 
            $sel .= ", " . $attr2->{name}; 
        } 
        $sql = $ins . ") " . $sel . " from " . $tname . 
            " where " . $def_aux->{id} . " = " . $sample_oid; 
        push @sqlList, ( $sql ); 
    } 
    print hiddenVar('project_oid', $project_oid); 
    db_sqlTrans(\@sqlList); 
}
 

############################################################################# 
# CreateDefaultSample 
############################################################################# 
sub CreateDefaultSample { 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    my $project_oid = param1('project_oid'); 
    if ( blankStr($project_oid) ) { 
	$project_oid =getSessionParam('project_oid');     
    }

    if ( blankStr($project_oid) ) { 
        return; 
    } 
    print hiddenVar('project_oid', $project_oid); 
 
    # get new sample oid 
    my $gold_stamp_id = db_getValue("select gold_stamp_id from project_info where project_oid = $project_oid"); 
    my $new_samp_oid = 0; 
    if ( $gold_stamp_id ) { 
        # GOLD project 
        # use higher range sample oid for GOLD entry 
        $new_samp_oid = db_findMaxID('env_sample', 'sample_oid') + 1; 
    } 
    else { 
        $new_samp_oid = getNewSampleId(); 
    } 
 
    my @sqlList = (); 
 
    my $ins = "insert into env_sample(sample_oid, contact, add_date, " . 
        "project_info, sample_display_name, " . 
        "sample_site, geo_location, " . 
        "sample_isolation, " . 
        "oxygen_req, temp, salinity, pressure, " . 
        "ph, host_ncbi_taxid, " . 
        "library_method, est_size, binning_method, " . 
        "contig_count, singlet_count, units, " . 
        "gene_count, comments, ecosystem, ecosystem_category, ecosystem_type, ecosystem_subtype, specific_ecosystem"; 
 
    my $sel = "select $new_samp_oid, $contact_oid, sysdate, " . 
#        "$project_oid, 'Sample $new_samp_oid', " . 
        "$project_oid, display_name, " . 
        "substr(geo_location, 1, 255), geo_location, " . 
        "isolation, " . 
        "oxygen_req, temp_range, salinity, pressure, " . 
        "ph, ncbi_taxon_id, " . 
        "library_method, est_size, binning_method, " . 
        "contig_count, singlet_count, units, " . 
        "gene_count, comments, ecosystem, ecosystem_category, ecosystem_type, ecosystem_subtype, specific_ecosystem"; 
 
    my $sql = $ins . ") " . $sel . " from project_info " .
        "where project_oid = " . $project_oid;
    push @sqlList, ( $sql );
 
    # set-valued attributes 
    # ENV_SAMPLE_ENERGY_SOURCE
    $sql = "insert into env_sample_energy_source (sample_oid, " .
        "energy_source) select $new_samp_oid, energy_source " .
        "from project_info_energy_source " .
        "where project_oid = " . $project_oid;
    push @sqlList, ( $sql ); 
 
    # ENV_SAMPLE_DISEASES
    $sql = "insert into env_sample_diseases (sample_oid, " .
        "diseases) select $new_samp_oid, diseases " .
        "from project_info_diseases " . 
        "where project_oid = " . $project_oid; 
    push @sqlList, ( $sql );

    # ENV_SAMPLE_HABITAT_TYPE
    $sql = "insert into env_sample_habitat_type (sample_oid, " .
        "habitat_type) select $new_samp_oid, habitat " .
        "from project_info_habitat " . 
        "where project_oid = " . $project_oid; 
    push @sqlList, ( $sql );
 
    # ENV_SAMPLE_METABOLISM
    $sql = "insert into env_sample_metabolism (sample_oid, " .
        "metabolism) select $new_samp_oid, metabolism " .
        "from project_info_metabolism " . 
        "where project_oid = " . $project_oid; 
    push @sqlList, ( $sql );

    # ENV_SAMPLE_PHENOTYPES
    $sql = "insert into env_sample_phenotypes (sample_oid, " .
        "phenotypes) select $new_samp_oid, phenotypes " .
        "from project_info_phenotypes " . 
        "where project_oid = " . $project_oid; 
    push @sqlList, ( $sql );

    # ENV_SAMPLE_MISC_META_DATA 
 
    # ENV_SAMPLE_SEQ_METHOD
    $sql = "insert into env_sample_seq_method (sample_oid, " .
        "seq_method) select $new_samp_oid, seq_method " .
        "from project_info_seq_method " . 
        "where project_oid = " . $project_oid; 
    push @sqlList, ( $sql ); 
 
#    for $sql ( @sqlList ) { 
#       print "<p>SQL: $sql</p>\n";
#    } 
 
    db_sqlTrans(\@sqlList); 

    return $new_samp_oid;
}
 



1;
