package ProjectInfo;
my $section = "ProjectInfo";

use strict;
#use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';
use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;
use TabHTML;
use JSON;

my $is_test = 0;

my $env = getEnv(); 
my $main_cgi             = $env->{main_cgi};
my $section_cgi          = "$main_cgi?section=$section";
my $base_url = $env->{ base_url }; 

# my $extra_row = 5;

my $default_max_row = 100;


######################################################################### 
# dispatch 
######################################################################### 
sub dispatch { 
    my ($page) = @_; 

    if ( $page eq 'newProject' ) { 
        NewProject(); 
    }
    elsif ( $page eq 'dbNewProjectInfo' ) { 
        my $msg = dbNewProjectInfo(); 
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
            ShowProjects(); 
        } 
    } 
    elsif ( $page eq 'updateProject' ) {
        my $project_oid = param1('project_oid'); 
 
        if ( blankStr($project_oid) ) {
            printError("No project has been selected.");
            print end_form(); 
            return; 
        }
 
        UpdateProject($project_oid);
    }
    elsif ( $page eq 'dbUpdateProject' ) {
        my $msg = dbUpdateProject(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowProjects(); 
        } 
    } 
    elsif ( $page eq 'deleteProject' ) { 
        DeleteProject(); 
    } 
    elsif ( $page eq 'dbDeleteProject' ) { 
        my $msg = dbDeleteProject(); 
 
        if ( ! blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
          }
        else {
            ShowProjects(); 
        } 
    }
    elsif ( $page eq 'copyProject' ) { 
        my $project_oid = param1('project_oid'); 
        CopyProject($project_oid); 
    } 
    elsif ( $page eq 'changeContact' ) {
        my $msg = dbChangeProjectContact(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowProjects(); 
        } 
    } 
    elsif ( $page eq 'filterProject' ) {
        FilterProject();
    } 
    elsif ( $page eq 'applyProjectFilter' ) {
        ApplyProjectFilter(); 
    } 
    elsif ( $page eq 'displayProject' ) {
	my $project_oid = param1('project_oid');
	DisplayProject($project_oid);
    }
    elsif ( $page eq 'printableData' ) { 
        my $project_oid = param1('project_oid');
	if ( $project_oid ) {
	    ShowPrintableData($project_oid);
	}
    }
    elsif ( $page eq 'hmpPrintableData' ) { 
        my $project_oid = param1('project_oid');
	if ( $project_oid ) {
	    ShowHmpPrintableData($project_oid);
	}
    }
#    elsif ( $page eq 'dbGrantEditPrivilege' ) {
#        my $msg = ProjectInfo::dbGrantEditPrivilege(); 
# 
#        if ( ! blankStr($msg) ) { 
#            WebFunctions::showErrorPage($msg);
#          } 
#        else {
#            ShowProjects(); 
#        } 
#    }
    elsif ( $page eq 'setGoldStampId' ) {
	my $project_oid = param1('project_oid');
	SetGoldStampId($project_oid);
    }
    elsif ( $page eq 'dbSetGoldStampId' ) {
	my $project_oid = param1('project_oid');
	my $gold_type = param1('gold_type');
	my $gold_stamp_id = param1('gold_stamp_id');
        my $msg = dbSetGoldStampId($project_oid, $gold_type, $gold_stamp_id);
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
            ShowProjects(); 
        } 
    }
    elsif ( $page eq 'delGoldStampId' ) {
	my $project_oid = param1('project_oid');
	DeleteGoldStampId($project_oid);
    }
    elsif ( $page eq 'dbDelGoldStampId' ) {
	my $project_oid = param1('project_oid');
	my $del_gold_id_option = param1('del_gold_id_option');
	my $gold_stamp_id = param1('gold_stamp_id');
        my $msg = dbDelGoldStampId($project_oid, $del_gold_id_option,
				   $gold_stamp_id);
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
            ShowProjects(); 
        } 
    }
    elsif ( $page eq 'mergeProjects' ) {
        my $project_oid = param1('project_oid'); 
        if ( blankStr($project_oid) ) {
            printError("No project has been selected.");
            print end_form(); 
            return; 
        }
 
        my $merged_project = param1('merged_project'); 
        if ( blankStr($merged_project) ) {
            printError("Please select a project to merge.");
            print end_form(); 
            return; 
        }

	if ( $project_oid == $merged_project ) {
            printError("The two projects are the same. Please select a different project to merge.");
            print end_form(); 
            return; 
	}

        MergeProjects($project_oid, $merged_project);
    }
    elsif ( $page eq 'dbMergeProjects' ) { 
        my $msg = dbMergeProjects(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowProjects(); 
        } 
    }
    elsif ( $page eq 'analysisProject' ) {
	my $analysis_project_id = param1('analysis_project_id');
	AnalysisProject($analysis_project_id);
    }
    elsif ( $page eq 'sequencingProject' ) {
	my $sequencing_project_id = param1('sequencing_project_id');
	SequencingProject($sequencing_project_id);
    }
    elsif ( $page eq 'imgGoldSequencingProject' ) {
	my $gold_id = param1('gold_id');
	SequencingProject_ImgGold($gold_id);
    }
    else { 
        ShowProjects(); 
    } 
} 
 

#########################################################################
# projectCount - count the number of projects
##########################################################################
sub projectCount {
    my ($contact_oid) = @_;

    my $dbh=WebFunctions::Connect_IMG;

    # special condition to filter out non-submission projects
    my $c_oid = getContactOid();
    my $cond2 = "";
    if ( $c_oid != 11 ) {
	$cond2 = "(project_oid in (select s2.project_info from submission s2 where s2.project_info is not null or contact_oid = $c_oid))";
    }

    my $sql = "select count(*) from project_info";
    if ( $contact_oid ) {
	$sql .= " where contact_oid = $contact_oid";
	if ( ! blankStr($cond2) ) {
	    $sql .= " and " . $cond2;
	}
    }
    elsif ( ! blankStr($cond2) ) {
	$sql .= " where " . $cond2;
    }
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( ! $cnt ) {
	return 0;
    }
    return $cnt;
}


#########################################################################
# selectedProjectCount - count the number of selected projects
##########################################################################
sub selectedProjectCount {
    my ($contact_oid) = @_;

    # special condition to filter out non-submission projects
    my $c_oid = getContactOid();
    my $cond2 = "";
    if ( $c_oid != 11 ) {
	$cond2 = "(p.project_oid in (select s2.project_info from submission s2 where s2.project_info is not null or p.contact_oid = $c_oid))";
    }

    my $dbh=WebFunctions::Connect_IMG;

    my $filter_cond = projectFilterCondition();
    my $sql = "select count(*) from project_info p";
    if ( $contact_oid ) {
	$sql .= " where p.contact_oid = $contact_oid";
	if ( ! blankStr($filter_cond) ) {
	    $sql .= " and " . $filter_cond;
	}
	if ( ! blankStr($cond2) ) {
	    $sql .= " and " . $cond2;
	}
    }
    else {
	if ( ! blankStr($filter_cond) ) {
	    $sql .= " where " . $filter_cond;
	    if ( ! blankStr($cond2) ) {
		$sql .= " and " . $cond2;
	    }
	}
	elsif ( ! blankStr($cond2) ) {
	    $sql .= " where " . $cond2;
	}
    }
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( ! $cnt ) {
	return 0;
    }
    return $cnt;
}


##########################################################################
# ShowProjects - show all projects by this user
##########################################################################
sub ShowProjects {
	
    print start_form(-name=>'showProjects',-method=>'post',action=>"$section_cgi");
 
    my $contact_oid = getContactOid();

    if ( ! $contact_oid ) {
	dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    my $title = "Your Projects";
    if ( $isAdmin eq 'Yes' ) {
	$title = "All Projects";
    }
    print "<h2>$title</h2>\n";

    my $cnt = 0;
    my $select_cnt = 0;
    if ( $isAdmin eq 'Yes' ) {
	$cnt = projectCount();
	$select_cnt = selectedProjectCount();
	print "<p>Selected Project Count: $select_cnt (Total: $cnt)</p>\n";
    }
    else {
	$cnt = projectCount($contact_oid);
	$select_cnt = selectedProjectCount($contact_oid);
	print "<p>Selected Project Count: $select_cnt (Total: $cnt)</p>\n";
    }

    # save orderby param 
    my $orderby = param1('project_orderby');
    my $desc = param1('project_desc');
 
    # max display 
    my $max_display = getSessionParam('proj_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = $default_max_row;
    } 
 
    # display page numbers
    my $curr_page = param1('project_page_no');
    if ( blankStr($curr_page) || $curr_page <= 0 ) {
        $curr_page = 1; 
    } 
    my $i = 0; 
    my $page = 1;
    print "<p>\n"; 
    while ( $i < $select_cnt ) { 
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>$page</b>"; 
        } 
        my $link = "<a href='" . $main_cgi .
            "?section=ProjectInfo&page=showProjects" . 
            "&project_page_no=$page"; 
        if ( ! blankStr($orderby) ) { 
            $link .= "&project_orderby=$orderby"; 
            if ( ! blankStr($desc) ) {
                $link .= "&project_desc=desc";
            } 
        } 
        $link .= "' >" . $s . "</a>"; 
        print $link . nbsp(1);
        $i += $max_display;
        $page++; 
        if ( $page > 1000 ) { 
            last; 
        }
    } 

    print "<p>\n";
    printProjectButtons();

    if ( $isAdmin eq 'Yes' ) {
	listProjects('', $curr_page);
    }
    else {
	listProjects($contact_oid, $curr_page);
    }

    printProjectButtons();

    # allow admin to change IMG contact
    if ( $isAdmin eq 'Yes' ) {
	print hr;
	print "<h3>Change IMG Contact for Selected Project</h3>\n";
	print "<p>New IMG Contact:\n";
	print "<select name='new_contact' class='img' size='1'>\n"; 
 
	my $sql2 = "select contact_oid, username from contact order by username, contact_oid";
	my $dbh2 = Connect_IMG_Contact();
webLog("$sql2\n");
	my $cur2=$dbh2->prepare($sql2); 
	$cur2->execute(); 
 
	for (my $j2 = 0; $j2 <= 10000; $j2++) { 
	    my ($id2, $name2) = $cur2->fetchrow_array(); 
	    if ( ! $id2 ) { 
		last; 
	    } 
 
	    print "    <option value='$id2'"; 
	    if ( $contact_oid == $id2 ) {
		print " selected "; 
	    } 
	    print ">$name2 (OID: $id2)</option>\n"; 
	} 
	print "</select>\n"; 
	$cur2->finish(); 
	$dbh2->disconnect(); 

	print "<p>\n";
	print '<input type="submit" name="_section_ProjectInfo:changeContact" value="Change Contact" class="medbutton" />';
    }

    # allow admin to change GOLD id
    if ( $isAdmin eq 'Yes' ) {
	print hr;

	print "<h3>Assign or Change GOLD Stamp ID for Selected Project</h3>\n";
	print "<p>Note: GCAT ID and HMP ID (for HMP projects only) will be automatically assigned when the selected project gets a GOLD Stamp ID. GCAT ID and HMP ID (if any) will be automatically deleted when the project GOLD Stamp ID is deleted.</p>\n";
	print "<p>\n";
	print '<input type="submit" name="_section_ProjectInfo:setGoldStampId" value="Assign GOLD Stamp ID" class="medbutton" />';
	print "&nbsp; \n";
	print '<input type="submit" name="_section_ProjectInfo:delGoldStampId" value="Delete GOLD Stamp ID" class="medbutton" />';

    }

    # allow admin to merge projects
    PrintMergeProjectSection($contact_oid, $isAdmin);

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();	
}


#########################################################################
# printProjectButtons - print New, Update, Delete buttons
#########################################################################
sub printProjectButtons {
    # New, Update and Delete buttons
    print '<input type="submit" name="_section_ProjectInfo:newProject" value="New" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:updateProject" value="Update" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:deleteProject" value="Delete" class="smbutton" />';

#    my $my_c_oid = getContactOid();
#    if ( $my_c_oid == 312 ) {
	print "&nbsp; \n";
	print '<input type="submit" name="_section_ProjectInfo:copyProject" value="Copy" class="smbutton" />';
#    }

    # project selection filter
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:filterProject" value="Filter Projects" class="smbutton" />';
}

#########################################################################
# PrintMergeProjectSection
#########################################################################
sub PrintMergeProjectSection {
    my ($contact_oid, $isAdmin) = @_;

    if ( $isAdmin ne 'Yes' ) {
	return;
    }

    print hr;

    print "<h3>Merge Projects</h3>\n";
    print "<p>Merge the selected project with the following project:</p>\n";

    my $sql2 = "select project_oid, display_name from project_info " .
	"order by project_oid";
    my $dbh2 = Connect_IMG ();
webLog("$sql2\n");     
    my $cur2=$dbh2->prepare($sql2); 
    $cur2->execute(); 

    print "<select name='merged_project' class='img' size='1'>\n";  
    for (my $j2 = 0; $j2 <= 100000; $j2++) { 
	my ($id2, $name2) = $cur2->fetchrow_array(); 
	if ( ! $id2 ) { 
	    last; 
	} 
 
	print "    <option value='$id2'>"; 
	print "$id2 - $name2</option>\n"; 
    } 
    $cur2->finish(); 
    $dbh2->disconnect(); 
    print "</select>\n"; 

    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:mergeProjects" value="Merge Projects" class="medbutton" />';
}


#########################################################################
# projectFilterParams - get all project filter parameters
#########################################################################
sub projectFilterParams {
    my ($filter_pre) = @_;

#    my @all_params = ( 'proj_filter:only_my_project',
#		       'proj_filter:project_type',
#		       'proj_filter:project_status',
#		       'proj_filter:domain',
#		       'proj_filter:seq_status',
#		       'proj_filter:seq_method',
#		       'proj_filter:availability',
##		       'proj_filter:pub_journal',
#		       'proj_filter:phylogeny',
#		       'proj_filter:display_name',
#		       'proj_filter:genus',
#		       'proj_filter:species',
#		       'proj_filter:common_name',
#		       'proj_filter:gold_stamp_id',
#		       'proj_filter:ncbi_project_id',
#		       'proj_filter:ncbi_project_name',
##		       'proj_filter:project_relevance',
#		       'proj_filter:add_date',
#		       'proj_filter:mod_date',
#		       'proj_filter:web_page_code' );

    my $filter_type = 'proj_filter:';
    if ( $filter_pre ) {
	$filter_type = $filter_pre . 'proj_filter:';
    }

    my @all_params = ( $filter_type . "only_my_project" );
    my $def_project = def_Project_Info();
    my @attrs = @{$def_project->{attrs}};
    for my $attr1 ( @attrs ) {
	if ( $attr1->{filter_cond} ) {
	    my $filter_name = $filter_type . $attr1->{name};
	    push @all_params, ( $filter_name );
	}
    }

    push @all_params, ( "$filter_type" . "project_info_data_links:Funding" );
    push @all_params, ( "$filter_type" . "project_info_data_links:Seq Center" );

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid);
    my @tables = getProjectAuxTables();
    if ( $isAdmin eq 'Yes' ) {
	push @tables, ( 'project_info_jgi_url' );
    }

    for my $table ( @tables ) {
	my $def_aux = def_Class($table);
	if ( ! $def_aux ) {
	    next;
	}

	my @attrs = @{$def_aux->{attrs}};
	for my $attr ( @attrs ) {
	    if ( $attr->{filter_cond} ) {
		my $filter_name = $filter_type . $attr->{name};
		if ($table eq 'project_info_cyano_metadata') {
		    $filter_name = $filter_type . $attr->{name} . ":Cyano";
		}
		push @all_params, ( $filter_name );
	    }
	}
    }
    
    return @all_params;
}

#########################################################################
# projectFilterCondition
#
# cond_type = meta_ for metagenome
##########################################################################
sub projectFilterCondition {
    my ($cond_type) = @_;
    my $cond = "";

    my @all_params = projectFilterParams($cond_type);
    for my $p0 ( @all_params ) {
#	if ( ! blankStr($cond_type) ) {
#	    $p0 = $cond_type . $p0;
#	}
	my $cond1 = "";
	my ($tag, $fld_name, $typ1) = split(/\:/, $p0);
        my $op_name = $p0 . ":op"; 
        my $op1 = "="; 
        if ( getSessionParam($op_name) ) { 
            $op1 = getSessionParam($op_name);
        } 

        if ( $op1 eq 'is null' || $op1 eq 'is not null' ) { 
            $cond1 ="p.$fld_name $op1"; 
        } 
        elsif ( $op1 eq 'match' && defined getSessionParam($p0) ) {
	    my $s1 = getSessionParam($p0);
	    $s1 =~ s/'/''/g;    # replace ' with ''
	    if ( length($s1) > 0 ) {
		$cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
	    }
	}
	elsif ( defined getSessionParam($p0) ) {
	    my @vals = split(/\,/, getSessionParam($p0));
	    if ( lc($fld_name) eq 'only_my_project' ) {
		if ( getSessionParam($p0) ) {
		    my $contact_oid = getContactOid();
		    $cond1 = "(p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
		}
	    }
	    elsif ( lc($fld_name) eq 'project_info_data_links' ) {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 ) {
		    $cond1 = "p.project_oid in (select project_oid " .
			"from project_info_data_links" .
			" where link_type = '$typ1' " .
			" and db_name = '$s1')";
		}
	    }
	    elsif ( $typ1 eq 'Cyano' ) {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 ) {
		    $cond1 = "p.project_oid in (select project_oid " .
			"from project_info_cyano_metadata" .
			" where $fld_name  like '%" . $s1 . "%')";
		}
	    }
	    elsif ( lc($fld_name) eq 'display_name' ||
		 lc($fld_name) eq 'genus' ||
		 lc($fld_name) eq 'species' ||
		 lc($fld_name) eq 'common_name' ||
		 lc($fld_name) eq 'ncbi_project_name' ) {
		my $s1 = getSessionParam($p0);
		$s1 =~ s/'/''/g;    # replace ' with ''
		if ( length($s1) > 0 ) {
		    $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
		}
	    }
	    elsif ( getAuxTableName($fld_name) ) {
		my $aux_table_name = getAuxTableName($fld_name);
		my $aux_def = def_Class($aux_table_name);
		if ( $aux_def ) {
		    my $s1 = getSessionParam($p0);
		    if ( length($s1) > 0 ) {
			$cond1 = "p.project_oid in (select project_oid " .
			    "from " . $aux_table_name .
			    " where " . lc($fld_name) .
			    " = '$s1')";
		    }
		}
	    }
	    elsif ( lc($fld_name) eq 'ncbi_project_id' ) {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 && isInt($s1) ) {
		    $cond1 = "p.$fld_name $op1 $s1";
		}
	    }
            elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = getSessionParam($p0);
                if ( length($s1) > 0 ) { 
                    $cond1 = "p.project_oid in (select pism.project_oid " .
                        "from project_info_seq_method pism " .
                        "where lower(pism.$fld_name) = '" . lc($s1) . "')";
                } 
            }
            elsif ( lc($fld_name) eq 'web_page_code' ) {
		for my $s2 ( @vals ) {
		    my $i = $s2 - 1;
		    if ( $i >= 0 ) {
			$cond1 = "p.$fld_name = $i";
		    }
		}
	    }
            elsif ( lc($fld_name) eq 'add_date' ||
		    lc($fld_name) eq 'mod_date' ) {
		my $op_name = $p0 . ":op";
		my $op1 = getSessionParam($op_name);
                my $d1 = getSessionParam($p0);
		if ( !blankStr($op1) && !blankStr($d1) ) {
		    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
		}
	    }
	    else {
		for my $s2 ( @vals ) {
		    $s2 =~ s/'/''/g;   # replace ' with ''
		    if ( blankStr($cond1) ) {
			$cond1 = "p.$fld_name in ('" . $s2 . "'";
		    }
		    else {
			$cond1 .= ", '" . $s2 . "'";
		    }
		}

		if ( ! blankStr($cond1) ) {
		    $cond1 .= ")";
		}
	    }
	}

	if ( ! blankStr($cond1) ) {
	    if ( blankStr($cond) ) {
		$cond = $cond1;
	    }
	    else {
		$cond .= " and " . $cond1;
	    }
	}
    }  # end for p0

    return $cond;
}


#########################################################################
# listProjects - list projects
#
# admin does not have condition on contact_oid
##########################################################################
sub listProjects {
    my ($contact_oid, $curr_page) = @_;

    my $cond = "";
    if ( $contact_oid ) {
	$cond = " where p.contact_oid = $contact_oid";
    }

    my $my_c_oid = getContactOid();

    my $selected_proj = param1('project_oid');

    # max display
    my $max_display = getSessionParam('proj_filter:max_display');
    if ( blankStr($max_display) ) {
	$max_display = $default_max_row;
    }

    # filter condition ???
    my $filter_cond = projectFilterCondition();

    if ( ! blankStr($filter_cond) ) {
	if ( blankStr($cond) ) {
	    $cond = " where " . $filter_cond;
	}
	else {
	    $cond .= " and " . $filter_cond;
	}
    }

    # special condition to filter out non-submission projects
    my $c_oid = getContactOid();
    if ( $c_oid != 11 ) {
	my $cond2 = "(p.project_oid in (select s2.project_info from submission s2 where s2.project_info is not null or p.contact_oid = $c_oid))";
	if ( blankStr($cond) ) {
	    $cond = " where " . $cond2;
	}
	else {
	    $cond .= " and " . $cond2;
	}
    }

    my $dbh=WebFunctions::Connect_IMG;
    my $orderby = param1('project_orderby');
    my $desc = param1('project_desc');
    if ( blankStr($orderby) ) {
	$orderby = "p.project_oid $desc";
    }
    else {
	if ( $orderby eq 'project_oid' ) {
	    $orderby = "p.project_oid $desc";
	}
	else {
	    $orderby = "p." . $orderby . " $desc, p.project_oid";
	}
    }

    my $sql = qq{
	select p.project_oid, p.display_name, p.gold_stamp_id,
	p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
	    from project_info p
	    $cond
	    order by $orderby
	};

    if ( $my_c_oid == 312 ) {
	print "<p>SQL: $sql</p>";
    }

    if ( ! blankStr($filter_cond) ) {
        print "<font color='red'>Filter is on. Some projects may be hidden. Reset the filter to see more projects.</font><br/>\n";
    }
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();

    my %contact_list;

    print "<h5>Click the column name to have the data order by the selected column. Click (Rev) to order by the same column in reverse order.</h5>\n";

    print "<table class='img' border='1'>\n"; 
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>" .
	getProjectOrderByLink('project_oid', 'ER Submission Project ID', 0) .
	"<br/>" .
	getProjectOrderByLink('project_oid', '(Rev)', 1) .
	"</th>\n";
    print "<th class='img'>" .
	getProjectOrderByLink('display_name', 'Project Display Name', 0) .
	"<br/>" .
	getProjectOrderByLink('display_name', '(Rev)', 1) .
	"</th>\n";
    print "<th class='img'>" .
	getProjectOrderByLink('gold_stamp_id', 'GOLD ID', 0) .
	"<br/>" .
	getProjectOrderByLink('gold_stamp_id', '(Rev)', 1) .
	"</th>\n";

#    print "<th class='img'>ER Submission ID</th>\n"; 

    print "<th class='img'>" .
	getProjectOrderByLink('phylogeny', 'Phylogeny', 0) .
	"<br/>" .
	getProjectOrderByLink('phylogeny', '(Rev)', 1) .
	"</th>\n";

    print "<th class='img'>Contact Name</th>\n"; 
    print "<th class='img'>IMG Contact</th>\n"; 

    print "<th class='img'>" .
	getProjectOrderByLink('add_date', 'Add Date', 0) .
	"<br/>" .
	getProjectOrderByLink('add_date', '(Rev)', 1) .
	"</th>\n"; 
    print "<th class='img'>" .
	getProjectOrderByLink('mod_date', 'Last Mod Date', 0) .
	"<br/>" .
	getProjectOrderByLink('mod_date', '(Rev)', 1).
	"</th>\n"; 

    my $cnt = 0;
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);
    for (;;) {
	my ( $proj_id, $proj_name, $gold_stamp_id,
	     $phylo, $add_date, $c_name,
	     $mod_date, $c_oid ) =
	    $cur->fetchrow_array();
	if ( ! $proj_id ) {
	    last;
	}

	$cnt++;
	if ( $cnt <= $skip ) {
	    next;
	}

	$disp_cnt++;
	if ( $disp_cnt > $max_display ) {
	    last;
	}

	print "<tr class='img'>\n";

        print "<td class='img'>\n";
	print "<input type='radio' ";
	print "name='project_oid' value='$proj_id'";
	print "/>";
	print "</td>\n";

	my $proj_link = getProjectLink($proj_id);
	PrintAttribute($proj_link);
#	PrintAttribute($proj_id);

	PrintAttribute($proj_name);
	my $gold_link = getGoldLink($gold_stamp_id);
	PrintAttribute($gold_link);

	# ER Submission ID
#	my @subs = db_getValues("select submission_id from submission where project_info = $proj_id");
#	my $sub_str = "";
#	for my $s1 ( @subs ) {
#	    my $submit_link = getSubmissionLink($s1);
#	    $sub_str .= $submit_link . " ";
#	}
#	PrintAttribute($sub_str);

	PrintAttribute($phylo);
	PrintAttribute($c_name);

	if ( $contact_list{$c_oid} ) {
	    PrintAttribute($contact_list{$c_oid});
	}
	else {
	    my $contact_str = db_getContactName($c_oid);
	    $contact_list{$c_oid} = $contact_str;
	    PrintAttribute($contact_str);
	}

	PrintAttribute($add_date);
	PrintAttribute($mod_date);
	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();

#    if ( $cnt > $max_display ) {
#	print "<p><font color='blue'>Too many rows. Only $max_display rows are displayed. (Use 'Filter Projects' to set the number of rows to be displayed.)</font></p>\n";
#    }

    return $cnt;
}


######################################################################### 
# NewProject
######################################################################### 
sub NewProject {
 
    print start_form(-name=>'newProject',-method=>'post',action=>"$section_cgi"); 

    my %db_val;
 
    # tab view 
    TabHTML::printTabAPILinks("newProject"); 
    my @tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4" );

    my @tabNames = 
        ( "Organism (*)", "Project (*)", 
          "Links", "Metadata" );

    TabHTML::printTabDiv( "newProject", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    printProjectTab("Organism", "", \%db_val);
    print "</p></div>\n"; 
 
    # tab 2
    print "<div id='tab2'><p>\n"; 
    printProjectTab("Project", "", \%db_val); 

    printProjectSetValTab ('project_info_seq_method', '');
    print "</p></div>\n";
 
    # tab 3
    print "<div id='tab3'><p>\n"; 
    printProjectTab("Links", "", \%db_val); 

    printProjectSetValTab ('project_info_data_links', '');
    print "</p></div>\n";
 
    # tab 4
    print "<div id='tab4'><p>\n";
    printProjectTab("Metadata", "", \%db_val); 

    printProjectSetValTab ('project_info_cell_arrangement', '');
    printProjectSetValTab ('project_info_diseases', '');
    printProjectSetValTab ('project_info_habitat', '');
    printProjectSetValTab ('project_info_metabolism', '');
    printProjectSetValTab ('project_info_phenotypes', '');
    printProjectSetValTab ('project_info_project_relevance', ''); 
    printProjectSetValTab ('project_info_energy_source', ''); 
    print "</p></div>\n"; 
 
    TabHTML::printTabDivEnd();
 
    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:dbNewProjectInfo" value="Add Project" class="medbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="Cancel" class="smbutton" />';
    printHomeLink();
 
    print end_form(); 
} 


 
##########################################################################
# printProjectTab - print project info in tab format 
##########################################################################
sub printProjectTab {
    my ($tab, $project_oid, $db_val) = @_;

    my $def_project = def_Project_Info();

    if ( $project_oid ) {
	my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

	# get Project_Info definition
	if ( $proj_domain eq 'MICROBIAL' ) {
	    $def_project = def_meta_Project_Info();
	}
    }
    else {
	my $page_name = param1('page_name');
	if ( $page_name eq 'newMetaProject' ) {
	    $def_project = def_meta_Project_Info();
	}
    }

    my @attrs = @{$def_project->{attrs}};
 
    if ( ! blankStr($project_oid) ) {
        # get data from database 
    } 
 
    # get NCBI Taxon info
    my %lineage; 
    my $ncbi_taxon_id = param1('ncbi_taxon_id'); 
    if ( blankStr($ncbi_taxon_id) && $db_val->{'ncbi_taxon_id'} ) {
        $ncbi_taxon_id = $db_val->{'ncbi_taxon_id'};
    } 
    if ( $tab eq 'Organism' && $ncbi_taxon_id ) {
        # print "<p>getNCBITaxonInfo: $ncbi_taxon_id</p>\n";
 
        %lineage = getNCBITaxonInfo($ncbi_taxon_id);
    } 
 
    # javascript for tooltips
    print "<script language='JavaScript' type='text/javascript' " .
	"src='" . $base_url . "/wz_tooltip.js'></script>\n";

    print "<p>Required fields are marked with <font color=red>(*)</font>. Mouse over MIGS ID to see field description.
              If you want to be MIGS compliant, fill in the required MIGS fields that are also marked with an asterisk.</p>\n"; 

    print "<table class='img' border='1'>\n";
 
    for my $k ( @attrs ) {
        # only show attributes in this tab
        if ( ! ($k->{tab}) || 
             $k->{tab} ne $tab ) {
            next;
        } 

	# skip non-editable for new
        if ( blankStr($project_oid) ) {
            if ( $k->{can_edit} ) {
                # show attribute
            }
            elsif ( $k->{name} =~ /^ncbi\_/ ) {
                # show attribute 
            } 
            else { 
                next;
            } 
        }
    
        my $attr_name = $k->{name}; 
        my $data_type = $k->{data_type};
        my $len = $k->{length};
        my $disp_name = $k->{display_name};
	my $hint = $k->{hint};
	my $definition = $k->{definition};
	my $tip = $k->{tip};
        if ( ! $disp_name ) { 
            $disp_name = $attr_name;
        }
 
        my $size = 80;
        if ( $len && $size > $len ) { 
            $size = $len; 
        } 
 
        my $attr_val = ""; 
#	if ( $attr_name eq 'contact_oid' ) { 
#	    $attr_val = db_getContact($contact_oid);
#	} 
 
        # get lineage info, if any
        if ( $attr_name eq 'ncbi_superkingdom' || 
             $attr_name eq 'ncbi_phylum' ||
             $attr_name eq 'ncbi_class' || 
             $attr_name eq 'ncbi_order' ||
             $attr_name eq 'ncbi_family' ||
             $attr_name eq 'ncbi_genus' ||
             $attr_name eq 'ncbi_species' ) {
            my ($tag, $rank) = split(/\_/, $attr_name);
            $attr_val = $lineage{$rank};
        } 
        else {
            # get previously saved values
            if ( param1($attr_name) ) {
                $attr_val = param1($attr_name);
            }
            elsif ( defined $db_val->{$attr_name} ) { 
                $attr_val = $db_val->{$attr_name};
            } 
        } 

        print "<tr class='img' >\n";

	# MIGS
	printCellTooltip($k->{migs_id}, $k->{migs_name}, 70);

        # print attribute display name
        if ( $k->{font_color} ) {
            print "  <th class='subhead' align='right'>" .
                "<font color='" . $k->{font_color} .
                "'>" . nbsp(3) . $disp_name . "</font></th>\n";
        } 
        elsif ( $k->{url} ) {
            print "  <th class='subhead' align='right'>";
            print alink($k->{url}, $disp_name, 'target', 1);
	    if ( $k->{is_required} ) {
		print " (*) ";
	    }
            print "</th>\n";
        }
        elsif ( $k->{is_required} ) {
            print "  <th class='subhead' align='right'>" .
                escapeHTML($disp_name) . "<font color=red> (*)</font> </th>\n";
        }
        else { 
            print "  <th class='subhead' align='right'>" .
                escapeHTML($disp_name) . " &nbsp; <span id=\"$tip\" title=\"$definition\"> <img src=\"" .
	      $base_url . "/images/help.gif\" /> </span></th>\n"; 
        } 
        if ( $k->{can_edit} ) { 
            # editable 
            if ( $data_type eq 'file' ) { 
                print "  <td class='img'   align='left'>" .
                    "<input type='file' name='$attr_name' size='$size'" .
                    " />" . "</td>\n"; 
            } 
            elsif ( $data_type eq 'cv' || $data_type eq 'list' ) {
                # controlled vocabularies - select 
                my @db_vals = (); 
                if ( $data_type eq 'cv' ) {
                    @db_vals = db_getValues($k->{cv_query});
                } 
                elsif ( $data_type eq 'list' ) {
                    @db_vals = split(/\|/, $k->{list_values});
                }

		if ( ! $attr_val && $k->{default_value} ) {
		    $attr_val = $k->{default_value};
		}

                print "  <td class='img'   align='left'>\n";
                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( ! $k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }
                for my $val ( sort @db_vals ) {
                    print "   <option value='" . escapeHTML($val) . "'";
                    if ( $val eq $attr_val ) {
                        print " selected "; 
                    } 
                    print ">" . escapeHTML($val) . "</option>\n";
                }
		if ($hint eq '') {
		    print "</select></td><td class='img'><font color=red>&nbsp;</font></td>\n";
		} else {
		    print "</select></td><td class='img'><font color=red>$hint</font></td>\n";
		}
            } elsif ( $data_type eq 'cv2' ) { 
                print "  <td class='img'   align='left'>\n";
		print "<select name='$attr_name' class='img' size='1'>\n";
		if ( ! $k->{is_required} ) {
		    print "   <option value=''> </option>\n";
		} 
 
		my $sql2 = $k->{cv_query};
		my $dbh2 = Connect_IMG(); 
webLog("$sql2\n");		
		my $cur2=$dbh2->prepare($sql2);
		$cur2->execute();
 
		for (my $j2 = 0; $j2 <= 10000; $j2++) {
		    my ($id2, $name2) = $cur2->fetchrow_array();
		    if ( ! $id2 && !$name2 ) {
			last; 
		    }
 
		    print "    <option value='" . escapeHTML($id2) . "'";
		    if ( length($attr_val) > 0 && $attr_val eq $id2 ) {
			print " selected ";
		    } 

		    print ">$id2 - $name2</option>\n"; 
		}
		print "</select></td>\n"; 
		$cur2->finish();
		$dbh2->disconnect(); 
	    } 
	    elsif ( $len >= 2000 && length($attr_val) > 60 ) {
		# use text area instead
                print "  <td class='img'   align='left'>" .
                    "<textarea id='$attr_name' name='$attr_name' " .
		    "rows='5' cols='60' maxLength='$len'>\n";
		print $attr_val;
		print "</textarea>\n";
	    }
	    else { 
                print "  <td class='img'   align='left'>" .
                    "<input type='text' name='$attr_name' value='";
                if ( length($attr_val) > 0 ) { 
                    print escapeHTML($attr_val); 
                } 
		if ( $k->{name} eq 'ncbi_taxon_id' ) {
			print "' size='$size' maxLength='$len'/>" . "</td>\n";
		} else {
		    if ($hint eq '') {
			print "' size='$size' maxLength='$len'/>" . "</td><td class='img'><font color=red>&nbsp;<font color=red></td>\n";
		    }
		    else {
			print "' size='$size' maxLength='$len'/>" . "</td><td class='img'><font color=red>$hint<font color=red></td>\n";
		    } 
		}
	    }
        } 
        elsif ( $k->{font_color} ) { 
            print "  <td class='img' align='left'>" .
                "<font color='" . $k->{font_color} . "'>";
            if ( $attr_val ) { 
                print escapeHTML($attr_val);
            } 
            print "</font></td><td class='img'> &nbsp; </td>\n"; 
        } 
        else { 
            # non-editable
            # display only field 
            PrintAttribute($attr_val); 
	    print "<td class='img'> &nbsp; </td>";
            print hidden($attr_name, $attr_val);
        } 
 
        if ( $k->{name} eq 'ncbi_taxon_id' ) {
            my $name0 = "_section_ProjectInfo:newProject";
            if ( ! blankStr($project_oid) ) {
                $name0 = "_section_ProjectInfo:updateProject";
            } 
            print "<th class='img'>\n"; 
            print '<input type="submit" name="'. $name0 . '"' .
                'value="Get Info" class="tinybutton" ' .
		'onMouseOver="this.T_WIDTH=10; return escape(\'get taxon ID from NCBI\')" ' .
		'/>'; 
            print "</th>\n";
        } 
 
        print "</tr>\n"; 
    } 
 
    print "</table>\n"; 
} 
 
 
#########################################################################
# printProjectSetValTab - set-valued attributes 
#########################################################################
sub printProjectSetValTab { 
    my ($tname, $project_oid) = @_;
 
    my $def_aux = def_Class($tname);
    if ( ! $def_aux ) {
        print "<p>Cannot find $tname</p>\n";
        return; 
    } 
    my @aux_attrs = @{$def_aux->{attrs}};
    my $extra_row = $def_aux->{new_rows};

    print "<h2";
    if ( $def_aux->{migs_name} ) {
	print " title='" . $def_aux->{migs_name} . "'";
    }
    print ">" . $def_aux->{display_name};
    if ( $def_aux->{migs_id} ) {
	print " (" . $def_aux->{migs_id} . ")";
    }
    print "</h2>\n";

    my $default_size = 60;
    if ( scalar(@aux_attrs) > 2 ) {
        $default_size = 40;
    } 
    if ( scalar(@aux_attrs) > 3 ) {
	$default_size = 30;
    }
 
    # print table header 
    print "<table class='img' border='1'>\n";
    for my $k ( @aux_attrs ) {
        if ( $k->{can_edit} ) { 
            print "<th class='img'>" . escapeHTML($k->{display_name}) .
                "</th>\n";
        } 
    } 
 
    my $i2 = 0; 
 
    # show old values ??? 
    if ( ! blankStr($project_oid) && isInt($project_oid) ) {
        my $sql = ""; 
        for my $k ( @aux_attrs ) {
            if ( ! $k->{can_edit} ) {
                next;
            } 
 
            if ( blankStr($sql) ) {
                $sql = "select " . $k->{name};
            } 
            else {
                $sql .= ", " . $k->{name}
            } 
        } 
        $sql .= " from " . $def_aux->{name} . " where " .
	    $def_aux->{id} . " = $project_oid";
	$sql .= " order by 1";

        my $dbh = Connect_IMG(); 
webLog("$sql\n");        
        my $cur=$dbh->prepare($sql);
        $cur->execute();
 
        for ($i2 = 0; $i2 <= 10000; $i2++) {
            my @flds = $cur->fetchrow_array();
            if ( scalar(@flds) == 0 ) {
                last; 
            } 

	    my $hasData = 0;
	    for (my $j2 = 0; $j2 < scalar(@flds); $j2++) {
		if ( $flds[$j2] ) {
		    $hasData = 1;
		}
	    }
            if ( ! $hasData ) {
                last;
            }
 
            print "<tr class='img'>\n";
 
            my $j = 0; 
            for my $k ( @aux_attrs ) { 
                if ( $j >= scalar(@aux_attrs) ) {
                    last; 
                } 
 
                my $attr_val = $flds[$j];
 
                if ( ! $k->{can_edit} ) {
                    next; 
                }
 
                my $fld_name = $def_aux->{name} . "|" . $k->{name} .
		    "|" . $i2;
 
                if ( $k->{data_type} eq 'cv' ||
                     $k->{data_type} eq 'list' ) { 
		    # selection 
                    my @db_vals = ();
                    if ( $k->{data_type} eq 'cv' ) {
                        @db_vals = db_getValues($k->{cv_query});
                    } 
                    elsif ( $k->{data_type} eq 'list' ) {
                        @db_vals = split(/\|/, $k->{list_values});
                    } 

		    if ( ! $attr_val && $k->{default_value} ) {
			$attr_val = $k->{default_value};
		    }

                    print "  <td class='img'   align='left'>\n";
                    print "<select name='$fld_name' class='img' size='1'>\n";
                    if ( ! $k->{is_required} ) {
                        print "   <option value=''> </option>\n";
                    }
                    for my $val ( sort @db_vals ) {
                        print "   <option value='" . escapeHTML($val) . "'";
                        if ( $val eq $attr_val ) {
                            print " selected ";
                        } 
                        print ">" . escapeHTML($val) . "</option>\n";
                    }
                    print "</select></td>\n";
                } 
                else {
                    my $len = $k->{length};
                    my $size = $default_size;
                    if ( $k->{default_size} ) {
                        $size = $k->{default_size};
                    } 
 
                    if ( $len && $size > $len ) {
                        $size = $len; 
                    } 
 
                print "  <td class='img'   align='left'>" .
                    "<input type='text' name='$fld_name'  value='" .
                    escapeHTML($attr_val) .
                    "' size='$size' maxLength='$len'/>" .
                    "</td>\n"; 
                } 
 
                $j++; 
            } 
 
            print "</tr>\n";
        } 
        $cur->finish(); 
        $dbh->disconnect(); 
    } 
 
    # show entry for new
    for (my $j = 0; $j < $extra_row; $j++) {
        print "<tr class='img'>\n";
        for my $k ( @aux_attrs ) {
            if ( ! $k->{can_edit} ) {
                next; 
            }
 
            my $fld_name =$def_aux->{name} . "|" .  $k->{name} . "|" . $i2;
 
            if ( $k->{data_type} eq 'cv' || 
                 $k->{data_type} eq 'list' ) { 
                # selection 
                my @db_vals = ();
                if ( $k->{data_type} eq 'cv' ) {
                    @db_vals = db_getValues($k->{cv_query});
                } 
                elsif ( $k->{data_type} eq 'list' ) {
                    @db_vals = split(/\|/, $k->{list_values});
                }
                print "  <td class='img'   align='left'>\n";
                print "<select name='$fld_name' class='img' size='1'>\n";
                if ( ! $k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }
                for my $val ( sort @db_vals ) {
                    print "   <option value='" . escapeHTML($val) . "'";
                    print ">" . escapeHTML($val) . "</option>\n";
                }
                print "</select></td>\n";
            } 
            else { 
                my $len = $k->{length};
                my $size = $default_size;
                if ( $k->{default_size} ) { 
		    if ( $k->{default_size} ) { 
			$size = $k->{default_size};
		    }
 
		    if ( $len && $size > $len ) {
			$size = $len;
		    }
                print "  <td class='img'   align='left'>" .
                    "<input type='text' name='$fld_name'  value='' " .
                    "' size='$size' maxLength='$len'/>" . "</td>\n";
		} 
	    } 
	}  # end for k loop

	print "</tr>\n"; 
 
	$i2++;
    }  # end for j

    print "</table>\n";

    my $s2 = "_count_" . $def_aux->{name}; 
    print hiddenVar($s2, $i2); 
}



#########################################################################
# printProjectCyanoSubTab - cyano metadata attributes 
#########################################################################
sub printProjectCyanoSubTab { 
    my ($tname, $project_oid, $db_val) = @_;
 
    my $def_aux = def_Class($tname);
    if ( ! $def_aux ) {
        print "<p>Cannot find $tname</p>\n";
        return; 
    } 
    my @aux_attrs = @{$def_aux->{attrs}};
    my $extra_row = $def_aux->{new_rows};

    print "<h2";
    if ( $def_aux->{migs_name} ) {
	print " title='" . $def_aux->{migs_name} . "'";
    }
    print ">" . $def_aux->{display_name};
    if ( $def_aux->{migs_id} ) {
	print " (" . $def_aux->{migs_id} . ")";
    }
    print "</h2>\n";

    # print table header 
    print "<table class='img' border='1'>\n";
    for my $k ( @aux_attrs ) {

#skip project oid
	if ($k->{display_name} eq 'Project ID') {
	    next;
	}
	# skip non-editable for new
        if ( blankStr($project_oid) ) {
            if ( $k->{can_edit} ) {
                # show attribute
            }
            elsif ( $k->{name} =~ /^ncbi\_/ ) {
                # show attribute 
            } 
            else { 
                next;
            } 
        }
    
        my $attr_name = $k->{name}; 
        my $data_type = $k->{data_type};
        my $len = $k->{length};
        my $disp_name = $k->{display_name};
        if ( ! $disp_name ) { 
            $disp_name = $attr_name;
        }
 
        my $size = 80;
        if ( $len && $size > $len ) { 
            $size = $len; 
        } 

         my $attr_val = $db_val->{$attr_name};
        print "<tr class='img' >\n";

	# MIGS
	printCellTooltip($k->{migs_id}, $k->{migs_name}, 70);
        # print attribute display name
        if ( $k->{font_color} ) {
            print "  <th class='subhead' align='right'>" .
                "<font color='" . $k->{font_color} .
                "'>" . nbsp(3) . $disp_name . "</font></th>\n";
        } 
        elsif ( $k->{url} ) {
            print "  <th class='subhead' align='right'>";
            print alink($k->{url}, $disp_name, 'target', 1);
	    if ( $k->{is_required} ) {
		print " (*) ";
	    }
            print "</th>\n";
        }
        elsif ( $k->{is_required} ) {
            print "  <th class='subhead' align='right'>" .
                escapeHTML($disp_name) . " (*) </th>\n";
        }
        else { 
            print "  <th class='subhead' align='right'>" .
                escapeHTML($disp_name) . "</th>\n"; 
        } 
        if ( $k->{can_edit} ) { 
            # editable 
            if ( $data_type eq 'file' ) { 
                print "  <td class='img'   align='left'>" .
                    "<input type='file' name='$attr_name' size='$size'" .
                    " />" . "</td>\n"; 
            } 
            elsif ( $data_type eq 'cv' || $data_type eq 'list' ) {
                # controlled vocabularies - select 
                my @db_vals = (); 
                if ( $data_type eq 'cv' ) {
                    @db_vals = db_getValues($k->{cv_query});
                } 
                elsif ( $data_type eq 'list' ) {
                    @db_vals = split(/\|/, $k->{list_values});
                }

		if ( ! $attr_val && $k->{default_value} ) {
		    $attr_val = $k->{default_value};
		}

                print "  <td class='img'   align='left'>\n";
                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( ! $k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }
                for my $val ( sort @db_vals ) {
                    print "   <option value='" . escapeHTML($val) . "'";
                    if ( $val eq $attr_val ) {
                        print " selected "; 
                    } 
                    print ">" . escapeHTML($val) . "</option>\n";
                }
                print "</select></td>\n";
            } 
	    elsif ( $len >= 2000 && length($attr_val) > 60 ) {
		# use text area instead
                print "  <td class='img'   align='left'>" .
                    "<textarea id='$attr_name' name='$attr_name' " .
		    "rows='5' cols='60' maxLength='$len'>\n";
		print $attr_val;
		print "</textarea>\n";
	    }
	    else { 
                print "  <td class='img'   align='left'>" .
                    "<input type='text' name='$attr_name' value='";
                if ( length($attr_val) > 0 ) { 
                    print escapeHTML($attr_val); 
                } 
                print "' size='$size' maxLength='$len'/>" . "</td>\n";
            } 
        } 
        elsif ( $k->{font_color} ) { 
            print "  <td class='img' align='left'>" .
                "<font color='" . $k->{font_color} . "'>";
            if ( $attr_val ) { 
                print escapeHTML($attr_val);
            } 
            print "</font></td>\n"; 
        } 
        else { 
            # non-editable
            # display only field 
            PrintAttribute($attr_val); 
            print hidden($attr_name, $attr_val);
        } 
 
        print "</tr>\n"; 
    } 

    print "</table>\n"; 

}



#############################################################################
# dbNewProjectInfo - insert a new project into database
#############################################################################
sub getNewProjectId {
    my $dbh = Connect_IMG(); 
    my $id0 = 10000;

    # SQL statement 
    my $sql = "select max(project_oid) from project_info " .
	"where project_oid <= " . $id0;
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    my $max_id = 0; 
    for (;;) { 
        my ( $val ) = $cur->fetchrow( ); 
        last if !$val; 
 
        # set max ID 
        $max_id = $val; 
    } 
 
    $cur->finish(); 
    $dbh->disconnect(); 

    if ( $max_id >= $id0 ) {
	$max_id = db_findMaxID('project_info', 'project_oid');
    }

    $max_id++;
    return $max_id;
}

sub dbNewProjectInfo {
    # get user info
    my $contact_oid = getContactOid();

    # get Project_Info definition
    my $def_project = def_Project_Info();
    my @attrs = @{$def_project->{attrs}};

    my $msg = "";

    my $project_oid = 0;
    my $gold_stamp_id = param1('gold_stamp_id');
    if ( blankStr($gold_stamp_id) ) {
	# no GOLD id is given
	$project_oid = getNewProjectId();
    }
    else {
	# use higher range project oid for GOLD entry
	$project_oid = db_findMaxID('project_info', 'project_oid') + 1;
    }
    if ( $project_oid <= 0 ) {
	# error checking -- this shouldn't happen
	return "Incorrect project ID value: $project_oid";
    }

    my $ins = "insert into project_info (project_oid, contact_oid, add_date";
    my $vals = "values ($project_oid, $contact_oid, sysdate";

    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $edit = $k->{can_edit};

	if ( ! $edit ) {
	    next;
	}

	my $val = param1($attr_name);

#	if ( blankStr($val) ) {
	if ( ! (defined $val) || length($val) == 0 ) {
	    if ( $k->{is_required} ) {
		$msg = "Attribute $disp_name cannot be null.";
		last;
	    }

	    next;
	}

	$ins .= ", " . $attr_name;

	if ( $data_type eq 'int' ) {
	    if ( ! blankStr($val) && ! isInt($val) ) {
		$msg = "$disp_name must be an integer.";
		last;
	    }

	    $vals .= ", " . $val;
	}
	elsif ( $data_type eq 'number' ) {
	    if ( ! blankStr($val) && ! isNumber($val) ) {
		$msg = "$disp_name must be a number.";
		last;
	    }

	    $vals .= ", " . $val;
	}
	elsif ( $data_type eq 'date' ) {
	    if ( ! blankStr($val) && ! isDate($val) ) {
		$msg = "$disp_name must be a date (DD-MON-YY).";
		last;
	    }

	    $vals .= ", '" . $val . "'";
	}
	else {
	    $val =~ s/'/''/g;   # replace ' with ''
	    $vals .= ", '" . $val . "'";
	}

	if ( $attr_name eq 'ncbi_taxon_id' &&
	     ! blankStr($val) && isInt($val) ) {
	    # update lineage
	    my %lineage = getNCBITaxonInfo($val);
	    for my $aname ( 'ncbi_superkingdom', 'ncbi_phylum',
			    'ncbi_class', 'ncbi_order',
			    'ncbi_family', 'ncbi_genus', 'ncbi_species' ) {
		my ($tag, $rank) = split(/\_/, $aname);
		my $val2 = $lineage{$rank};
		$val2 =~ s/'/''/g;   # replace ' with ''
		$ins .= ", " . $aname;
		$vals .= ", '" . $val2 . "'";
	    }
	}
    }

    if ( ! blankStr($msg) ) {
	return $msg;
    }

    my @sqlList = ();
    my $sql = $ins . ") " . $vals . ")";
    push @sqlList, ( $sql );

    # insert set-valued attribute 
    my $isAdmin = getIsAdmin($contact_oid);
    my @setnames = getProjectAuxTables();
    if ( $isAdmin eq 'Yes' ) {
	push @setnames, ( 'project_info_jgi_url' );
    }
    for my $sname ( @setnames ) {
        $msg = dbUpdateProjectSetValues($project_oid, $sname, 0, \@sqlList);
        if ( ! blankStr($msg) ) { 
            return $msg; 
        } 
    } 
 
        $msg = dbUpdateProjectCyanoSubTab($project_oid, 'project_info_cyano_metadata', 0, \@sqlList);
        if ( ! blankStr($msg) ) { 
            return $msg; 
        } 
  
  if ( $is_test ) { 
        for $sql ( @sqlList ) {
            $msg .= " SQL: " . $sql;
        }
    } 
    else { 
        db_sqlTrans(\@sqlList);
    } 

    return $msg;
}


######################################################################### 
# UpdateProject
######################################################################### 
sub UpdateProject {
    my ($project_oid) = @_;

    my %db_val = SelectProjectInfo($project_oid);

    print start_form(-name=>'updateProject',-method=>'post',action=>"$section_cgi"); 
 
    print "<h2>Update Project $project_oid</h2>\n"; 
    print hiddenVar('project_oid', $project_oid); 

    # tab view 
    TabHTML::printTabAPILinks("updateProject"); 
    my @tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", '#tab13' );

    my @tabNames = 
        ( "Organism (*)", "Project (*)", 
          "Links", "Metadata", "Samples" );

    TabHTML::printTabDiv( "updateProject", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    printProjectTab("Organism", $project_oid, \%db_val);
    print "</p></div>\n"; 
 
    # tab 2
    print "<div id='tab2'><p>\n"; 
    printProjectTab("Project", $project_oid, \%db_val); 

    printProjectSetValTab ('project_info_seq_method', $project_oid);
    print "</p></div>\n";
 
    # tab 3
    print "<div id='tab3'><p>\n"; 
    printProjectTab("Links", $project_oid, \%db_val); 

    printProjectSetValTab ('project_info_data_links', $project_oid);
    print "</p></div>\n";
 
    # tab 4
    print "<div id='tab4'><p>\n";
    printProjectTab("Metadata", $project_oid, \%db_val); 

    printProjectSetValTab ('project_info_cell_arrangement', $project_oid);
    printProjectSetValTab ('project_info_diseases', $project_oid);
    printProjectSetValTab ('project_info_habitat', $project_oid);
    printProjectSetValTab ('project_info_metabolism', $project_oid);
    printProjectSetValTab ('project_info_phenotypes', $project_oid);
    printProjectSetValTab ('project_info_project_relevance', $project_oid); 
    printProjectSetValTab ('project_info_energy_source', $project_oid);
    print "</p></div>\n"; 

    # tab 13
    print "<div id='tab13'><p>\n"; 
    printProjectSampleTab ($project_oid);
    print "</p></div>\n"; 
 
    TabHTML::printTabDivEnd();
 
    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:dbUpdateProject" value="Update Project" class="medbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="Cancel" class="smbutton" />';
 
    printHomeLink();
 
    print end_form(); 
} 


###########################################################################
# printProjectSampleTab - show all samples in this project
###########################################################################
sub printProjectSampleTab {
    my ($project_oid) = @_;

#    print "<h4><font color='red'>Under Construction</font></h4>\n";

    if ( blankStr($project_oid) ) {
	return;
    }

    print "<h2>Samples</h2>\n";
    my $cnt = db_getValue("select count(*) from env_sample where project_info = $project_oid");
    if ( $cnt ) {
	print "<h4>Number of samples in this project: $cnt</h4>\n";
    }
    else {
	print "<h4>No samples in this project.</h4>\n";
	return;
    }

    print "<p>To edit samples, first save your changes to the project, and then go to Samples tab to find all samples in this project.</p>\n";

    require EnvSample;
    EnvSample::listProjectSamples($project_oid);
}


###########################################################################
# dbUpdateProjectSetValues
#
# upd = 0 (new), = 1 (update)
###########################################################################
sub dbUpdateProjectSetValues {
    my ($project_oid, $aux_name, $upd, $sqlList ) = @_;

    my $msg = "";

    my $def_aux = def_Class($aux_name);
    if ( ! $def_aux ) {
	$msg = "Cannot find definition for $aux_name";
	return $msg;
    }
    my $extra_row = $def_aux->{new_rows};

    my $tname = $def_aux->{name};

    my $sql;

    if ( $upd ) {
	$sql = "delete from $tname where " . $def_aux->{id} .
	    " = $project_oid";
	push @$sqlList, ( $sql );
    }

    # get count
    my $s2 = "_count_" . $tname;
    my $cnt = param1($s2);
    if ( blankStr($cnt) ) {
	$cnt = $extra_row;
    }

    my @aux_attrs = @{$def_aux->{attrs}};
    my $i = 0;
    while ( $i < $cnt ) {
	my $ins = "";
	my $vals = "";

	for my $k ( @aux_attrs ) {
	    if ( ! $k->{can_edit} ) {
		next;
	    }

	    my $fld_name = $tname . "|" . $k->{name} . "|" . $i;
	    my $attr_val = param1($fld_name);
#	    if ( blankStr($attr_val) ) {
	    if ( ! (defined $attr_val) || length($attr_val) == 0 ) {
		next;
	    }

	    if ( blankStr($ins) ) {
		$ins = "insert into $tname (" . $def_aux->{id} . ", ";
		$vals = "values ($project_oid, ";
	    }
	    else {
		$ins .= ", ";
		$vals .= ", ";
	    }

	    $ins .= $k->{name};
	    if ( $k->{data_type} eq 'int' ||
		 $k->{data_type} eq 'number' ) {
		$vals .= $attr_val;
	    }
	    else {
		my $db_val = $attr_val;
		$db_val =~ s/'/''/g;   # replace ' with '';
		$vals .= "'" . $db_val . "'";
	    }
	}

	if ( ! blankStr($ins) && !blankStr($vals) ) {
	    $sql = $ins . ") " . $vals . ")";
	    push @$sqlList, ( $sql );
	}

	$i++;
    }

    return $msg;
}


############################################################################
# showProjectSample - show samples associated with this project
############################################################################
sub showProjectSample {
    my ( $project_oid ) = @_;

    # check project
    my $dbh=WebFunctions::Connect_IMG;

    my $sql = "select s.sample_oid, s.sample_display_name" .
        " from project_info p, env_sample s" .
        " where p.project_oid = s.project_info" .
        " and p.project_oid = $project_oid";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my $i = 0;
    for ($i = 0; $i <= 10000; $i++) {
	my ( $sample_oid, $sample_name ) = $cur->fetchrow_array();
	if ( ! $sample_oid ) {
	    last;
	}

	if ( $i == 0 ) {
	    print "<h4>Project is associated with the following sample(s):</h4>\n";
	    print "<ul>\n";
	}
	print "<li>Sample $sample_oid: " . escapeHTML($sample_name) .
	    "</li>\n";
    }
    $cur->finish();
    $dbh->disconnect();

    if ( $i == 0 ) {
	print "<h4>Project is not associated with any samples.</h4>\n";
    }
    else {
	print "</ul>\n";
    }
}


####################################################################
# select Project_Info data given an ID
####################################################################
sub SelectProjectInfo {
    my ( $project_oid ) = @_;
 
    my %db_val; 
 
    my $dbh = Connect_IMG(); 
 
    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

    # get Project_Info definition
    my $def_project = def_Project_Info();
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }
    my @attrs = @{$def_project->{attrs}};

    my $sql = "";
    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
	# $attr_name = 'p.' . $attr_name;

        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } 
        else {
            $sql .= ", " . $attr_name;
        }
    } 
 
    $sql .= " from project_info where project_oid = $project_oid";
 
#    print "<p>SQL: $sql</p>\n"; 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my @flds = $cur->fetchrow_array(); 
 
    # save result 
    my $j = 0; 
    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
        if ( scalar(@flds) < $j ) { 
            last;
        }
        $db_val{$attr_name} = $flds[$j];
        $j++; 
    } 
 
    # finish
    $cur->finish(); 

    # contact info
    if ( $db_val{'contact_oid'} ) {
	$db_val{'contact_oid'} = db_getContact($db_val{'contact_oid'});
    }

    # modified_by
    if ( $db_val{'modified_by'} ) {
	$db_val{'modified_by'} = db_getContact($db_val{'modified_by'});
    }

# cyano data
    # get Project_Info_cyano_metadata definition
    $def_project = def_project_info_cyano_metadata();
    @attrs = @{$def_project->{attrs}};

    $sql = "";
    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
	# $attr_name = 'p.' . $attr_name;

        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } 
        else {
            $sql .= ", " . $attr_name;
        }
    } 
 
    $sql .= " from project_info_cyano_metadata where project_oid = $project_oid";
 
#    print "<p>SQL: $sql</p>\n"; 
webLog("$sql\n"); 
    $cur=$dbh->prepare($sql);
    $cur->execute();
    @flds = $cur->fetchrow_array(); 
 
    # save result 
    $j = 0; 
    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
        if ( scalar(@flds) < $j ) { 
            last;
        }
        $db_val{$attr_name} = $flds[$j];
        $j++; 
    } 
 
    # finish
    $cur->finish(); 
    $dbh->disconnect();


    return %db_val; 
} 


#############################################################################
# dbUpdateProject
#############################################################################
sub dbUpdateProject {
    # get user info
    my $contact_oid = getContactOid();

    my $msg = "";

    # get project_oid
    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) ) {
	$msg = "No project has been selected for update.";
	return $msg;
    }

    my $can_update_proj = getCanUpdateProject($contact_oid, $project_oid);
    if ( ! $can_update_proj ) {
	$msg = "You are not allowed to update this project.";
	return $msg;
    }

    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

    # get Project_Info definition
    my $def_project = def_Project_Info();
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }
    my @attrs = @{$def_project->{attrs}};

    my $sql = "update project_info set modified_by = $contact_oid, " .
	"mod_date = sysdate";

    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $edit = $k->{can_edit};

	if ( ! $edit ) {
	    next;
	}

	my $val = param1($attr_name);
#	if ( blankStr($val) ) {
	if ( ! (defined $val) || length($val) == 0 ) {
	    if ( $k->{is_required} ) {
		$msg = "Attribute $disp_name cannot be null.";
		last;
	    }

	    $sql .= ", $attr_name = null";
	    next;
	}

	if ( $data_type eq 'int' ) {
	    if ( ! blankStr($val) && ! isInt($val) ) {
		$msg = "$disp_name must be an integer.";
		last;
	    }

	    $sql .= ", $attr_name = $val";
	}
	elsif ( $data_type eq 'number' ) {
	    if ( ! blankStr($val) && ! isNumber($val) ) {
		$msg = "$disp_name must be a number.";
		last;
	    }

	    $sql .= ", $attr_name = $val";
	}
	elsif ( $data_type eq 'date' ) {
	    if ( ! blankStr($val) && ! isDate($val) ) {
		$msg = "$disp_name must be a date (DD-MON-YY).";
		last;
	    }

	    $sql .= ", $attr_name = '" . $val . "'";
	}
	else {
	    $val =~ s/'/''/g;   # replace ' with ''
	    $sql .= ", $attr_name = '" . $val . "'";
	}

	if ( $attr_name eq 'ncbi_taxon_id' &&
	     ! blankStr($val) && isInt($val) ) {
	    # update lineage
	    my %lineage = getNCBITaxonInfo($val);
	    for my $aname ( 'ncbi_superkingdom', 'ncbi_phylum',
			    'ncbi_class', 'ncbi_order',
			    'ncbi_family', 'ncbi_genus', 'ncbi_species' ) {
		my ($tag, $rank) = split(/\_/, $aname);
		my $val2 = $lineage{$rank};
		$val2 =~ s/'/''/g;   # replace ' with ''
		$sql .= ", $aname = '" . $val2 . "'";
	    }
	}
    }

    $sql .= " where project_oid = $project_oid";

    if ( ! blankStr($msg) ) {
	return $msg;
    }

    if ( $is_test && $contact_oid == 312 ) {
	print "<p>SQL: $sql</p>\n";
    }

    my @sqlList = ( );
    push @sqlList, ( $sql );

    # set valued
    my $isAdmin = getIsAdmin($contact_oid);
    my @setnames = getProjectAuxTables();
    if ( $isAdmin eq 'Yes' ) {
	push @setnames, ( 'project_info_jgi_url' );
    }

    for my $sname ( @setnames ) { 
        $msg = dbUpdateProjectSetValues($project_oid, $sname, 1, \@sqlList);
        if ( ! blankStr($msg) ) { 
            return $msg;
        } 
    }

        $msg = dbUpdateProjectCyanoSubTab($project_oid, 'project_info_cyano_metadata', 1, \@sqlList);
        if ( ! blankStr($msg) ) { 
            return $msg;
        } 
 
    if ( $is_test ) { 
        for $sql ( @sqlList ) { 
            $msg .= " SQL: " . $sql;
        } 
    }
    else { 
        db_sqlTrans(\@sqlList);
    } 

    return $msg;
}

# dbUpdateProjectCyanoSubTab
#############################################################################
sub dbUpdateProjectCyanoSubTab {
    my ($project_oid, $aux_name, $upd, $sqlList ) = @_;

    my $msg = "";

    my $def_aux = def_Class($aux_name);
    if ( ! $def_aux ) {
	$msg = "Cannot find definition for $aux_name";
	return $msg;
    }

    my $tname = $def_aux->{name};

    my $sql;

    if ( $upd ) {
	$sql = "delete from $tname where " . $def_aux->{id} .
	    " = $project_oid";
	push @$sqlList, ( $sql );
    }

    my @aux_attrs = @{$def_aux->{attrs}};

    my $ins = "insert into project_info_cyano_metadata (project_oid, ";
    my $vals = "values ($project_oid, ";

    for my $k ( @aux_attrs ) { 
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $edit = $k->{can_edit};

	if ( ! $edit ) {
	    next;
	}

	my $val = param1($attr_name);
#	if ( blankStr($val) ) {
	if ( ! (defined $val) || length($val) == 0 ) {
	    if ( $k->{is_required} ) {
		$msg = "Attribute $disp_name cannot be null.";
		last;
	    }

	    next;
	}

	$ins .= $attr_name. ", ";

	if ( $data_type eq 'int' ) {
	    if ( ! blankStr($val) && ! isInt($val) ) {
		$msg = "$disp_name must be an integer.";
		last;
	    }

	    $vals .= $val . ", ";
	}
	elsif ( $data_type eq 'number' ) {
	    if ( ! blankStr($val) && ! isNumber($val) ) {
		$msg = "$disp_name must be a number.";
		last;
	    }

	    $vals .= $val . ", ";
	}
	else {
	    $val =~ s/'/''/g;   # replace ' with ''
	    $vals .= "'" . $val . "', ";
	}
    }

    if ( ! blankStr($msg) ) {
	return $msg;
    }
    $ins =~ s/, $//;
    $vals =~ s/, $//;
    $sql = $ins . ") " . $vals . ")";
    push @$sqlList, ( $sql );
    return $msg;

}


#############################################################################
# DeleteProject - delete project
#############################################################################
sub DeleteProject {
	
    print start_form(-name=>'deleteProject',-method=>'post',action=>"$section_cgi");

    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) || ! isInt($project_oid) ) {
	printError("No project has been selected.");
	print end_form();
	return;
    }

    print "<h2>Delete Project $project_oid</h2>\n";
    print hidden('project_oid', $project_oid);

    # check whether there are any FK
    # check submission
    my $cnt = db_getValue("select count(*) from submission where project_info = $project_oid");
    if ( $cnt > 0 ) {
	printError("You cannot delete a submitted project.");
	print end_form();
	return;
    }

    # show project, if any
    showProjectSample($project_oid);

    print "<p>Please click the Delete button to confirm the deletion, or cancel the deletion.</p>\n";
    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:dbDeleteProject" value="Delete" class="smbutton" />'; 
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="Cancel" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();	
}


#############################################################################
# dbDeleteProject - delete project from database
#############################################################################
sub dbDeleteProject {
    # get user info
    my $contact_oid = getContactOid();

    my $msg = "";

    # get project_oid
    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) || ! isInt($project_oid) ) {
	$msg = "No project has been selected for deletion.";
	return $msg;
    }

    my $can_update_proj = getCanUpdateProject($contact_oid, $project_oid);
    if ( ! $can_update_proj ) {
	$msg = "You are not allowed to delete this project.";
	return $msg;
    }

    my @sqlList = ( );
    my $sql = "";

    my @tables = getProjectAuxTables();
    push @tables, ( 'project_info_jgi_url' );
    for my $aux_name ( @tables ) {
	my $def_aux = def_Class($aux_name);
	if ( ! $def_aux ) {
	    next;
	}

	my $tname = $def_aux->{name};
	$sql = "delete from $tname where " . $def_aux->{id} .
	    " = " . $project_oid;
	push @sqlList, ( $sql );
    }

    $sql = "update env_sample set project_info = null " .
	"where project_info = $project_oid";
    push @sqlList, ( $sql );

    $sql = "delete from project_info where project_oid = $project_oid";
    push @sqlList, ( $sql );

#    for $sql ( @sqlList ) {
#	$msg .= " SQL: " . $sql;
#    }

    db_sqlTrans(\@sqlList);

    return $msg;
}


#############################################################################
# dbChangeProjectContact
#############################################################################
sub dbChangeProjectContact {
    # get user info
    my $contact_oid = getContactOid();

    my $msg = "";

    # get project_oid
    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) || ! isInt($project_oid) ) {
	$msg = "No project has been selected for update.";
	return $msg;
    }

    # get new contact
    my $new_contact = param1('new_contact');
    if ( blankStr($new_contact) ) {
	$msg = "No new contact has been selected for update.";
	return $msg;
    }

    my $sql = "update project_info set modified_by = $contact_oid, " .
	"mod_date = sysdate, contact_oid = $new_contact " .
	"where project_oid = $project_oid";

    if ( ! blankStr($msg) ) {
	return $msg;
    }

    my @sqlList = ( );
    push @sqlList, ( $sql );

    db_sqlTrans(\@sqlList);

    return $msg;
}

#############################################################################
# GrantEditPrivilege
#############################################################################
sub GrantEditPrivilege {

    print start_form(-name=>'grantEditPrivilege',-method=>'post',action=>"$section_cgi");
 
    # get project_oid
    my $project_oid = param1('project_oid');
    if ( ! $project_oid || ! isInt($project_oid) ) {
	printError ( "No project has been selected."); 
	return;
    }
 
    my @contact_list = ();
    my $dbh2 = Connect_IMG();
    my $sql2 = qq{
	select p.project_oid, p.display_name, p.domain,
	p.contact_oid, c.username
	    from project_info p, contact c
	    where p.project_oid = $project_oid
	    and p.contact_oid = c.contact_oid
	};
webLog("$sql2\n");	
    my $cur2=$dbh2->prepare($sql2);
    $cur2->execute(); 
    my ( $p_oid, $p_name, $p_domain, $p_contact, $c_name ) =
	$cur2->fetchrow_array();
    $cur2->finish();

    print "<h1>Project (ID: $project_oid): " .
	escapeHTML($p_name) . "</h1>\n";
    print "<h4>IMG Contact for Project: " . escapeHTML($c_name) . "</h4>\n";
    print hiddenVar('project_oid', $project_oid); 

    $sql2 = "select contact_oid from contact_project_permissions " .
        "where project_permissions = $project_oid";
webLog("$sql2\n");        
    $cur2=$dbh2->prepare($sql2);
    $cur2->execute(); 
    for (my $j = 0;$j <= 100000; $j++) {
        my ( $cid ) = $cur2->fetchrow_array();
        if ( ! $cid ) { 
            last; 
        } 
        push @contact_list, ( $cid );
    } 
    $cur2->finish();
    $dbh2->disconnect();

    print "<h3>Grant Edit Privilege to Contact(s):</h3>\n"; 

    my $dbh = Connect_IMG_Contact();
    my $sql = "select contact_oid, username, name from contact order by username";
webLog("$sql\n");     
    my $cur=$dbh->prepare($sql);
    $cur->execute();
 
    for (my $j = 0;$j <= 100000; $j++) {
        my ( $c_oid, $username, $name ) = $cur->fetchrow_array();
        if ( ! $c_oid ) {
            last;
        } 

	if ( $c_oid == $p_contact ) {
	    next;
	}

        if ( ! $username || $username =~ /^MISSING/ ||
	     $username =~ /^IMG\_/ || $username =~ /^img\_/ ) {
            next; 
        } 
 
        print nbsp(3); 
        print "<input type='checkbox' name='img_contact_oid' value='$c_oid'";
        if ( inIntArray($c_oid, @contact_list) ) {
            print " checked "; 
        } 
        print ">". escapeHTML($username) . " (" . escapeHTML($name) . ")\n";
        print "<br/>\n"; 
    } 
 
    $cur->finish(); 
 
    $dbh->disconnect();
 
    print "<p>\n"; 
    if ( $p_domain eq 'MICROBIAL' ) {
	print '<input type="submit" name="_section_GoldMetaProj:dbGrantEditPrivilege" value="Grant Edit Privilege" class="medbutton" />';
    }
    else {
	print '<input type="submit" name="_section_GoldProj:dbGrantEditPrivilege" value="Grant Edit Privilege" class="medbutton" />';
    }

    print "<p>\n";
    printHomeLink(); 
 
    print end_form();

}


############################################################################# 
# dbGrantEditPrivilege
############################################################################# 
sub dbGrantEditPrivilege{ 
 
    my $msg = ""; 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        $msg = "Unknown username / password"; 
        return $msg; 
    } 
 
    my $project_oid = param1('project_oid');
    if ( ! $project_oid || ! isInt($project_oid) ) {
	$msg = "No project has been selected.";
	return $msg;
    }

    my $isAdmin = getIsAdmin($contact_oid); 
    if ( $isAdmin ne 'Yes' ) { 
        $msg = "You do not have the privilege to grant edit privilege."; 
        return $msg; 
    } 
 
    my @contact_list = param('img_contact_oid');
    if ( scalar(@contact_list) == 0 ) {
	$msg = "No IMG contacts have been selected.";
	return $msg;
    }

    my @sqlList = ( );
 
    my $sql = "delete from contact_project_permissions " .
        "where project_permissions = $project_oid";
    push @sqlList, ( $sql ); 
 
    for my $c_oid ( @contact_list ) { 
        $sql = "insert into contact_project_permissions (" .
            "contact_oid, project_permissions) values (" .
            $c_oid . ", " . $project_oid . ")";
        push @sqlList, ( $sql );
    } 
 
#    for $sql ( @sqlList ) { 
#       $msg .= "SQL: $sql; ";
#    }

    if ( $msg ) { 
        return $msg; 
    } 
 
    # update database
    db_sqlTrans(\@sqlList);

    return ""; 
} 


########################################################################## 
# FilterProject - set filter on displaying projects
#             so that there won't be too many to display 
########################################################################## 
sub FilterProject {
 
    print start_form(-name=>'filterProject',-method=>'post',action=>"$section_cgi");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid);
 
    print "<h3>Set Project Filter for $uname</h3>\n";
 
    # max display
    my $max_display = getSessionParam('proj_filter:max_display');
    if ( blankStr($max_display) ) {
	$max_display = "$default_max_row";
    }
    print "<p>Maximal Rows of Display:\n";
    print nbsp(3); 
    print "<select name='proj_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
		   '300', '500', '800', '1000', '2000',
		   '3000', '4000', '5000', '10000' ) {
	print "    <option value='$cnt0' ";
	if ( $cnt0 eq $max_display ) {
	    print " selected ";
	}
	print ">$cnt0</option>\n";
    }
    print "</select>\n";

    # project type
    printFilterCond('Project Type', 'proj_filter:project_type',
		    'select', 'query',
		    'select cv_term from project_typecv order by cv_term',
		    getSessionParam('proj_filter:project_type'));

    # project status
    printFilterCond('Project Status', 'proj_filter:project_status',
		    'select', 'query',
		    'select cv_term from project_statuscv order by cv_term',
		    getSessionParam('proj_filter:project_status'));

    # domain
    printFilterCond('Domain', 'proj_filter:domain',
		    'select', 'list', 'ARCHAEAL|BACTERIAL|EUKARYAL|MICROBIAL|PLASMID|VIRUS',
		    getSessionParam('proj_filter:domain'));

    # seq status
    printFilterCond('Sequencing Status', 'proj_filter:seq_status',
		    'select', 'query',
		    'select cv_term from seq_statuscv order by cv_term',
		    getSessionParam('proj_filter:seq_status'));

    # seq method
    printFilterCond('Sequencing Method', 'proj_filter:seq_method',
		    'select', 'query',
		    'select cv_term from seq_methodcv order by cv_term',
		    getSessionParam('proj_filter:seq_method'));

    # availability: Proprietary, Public
    printFilterCond('Availability', 'proj_filter:availability',
		    'select', 'list', 'Proprietary|Public',
		    getSessionParam('proj_filter:availability'));

    # publication status
#    printFilterCond('Publication', 'proj_filter:pub_journal',
#		    'select', 'query',
#		    'select distinct pub_journal from project_info order by pub_journal',
#		    getSessionParam('proj_filter:pub_journal'));

    # phylogeny
    printFilterCond('Phylogeny', 'proj_filter:phylogeny',
		    'select', 'query',
		    'select cv_term from phylogenycv order by cv_term',
		    getSessionParam('proj_filter:phylogeny'));

    # project display name
    printFilterCond('Project Display Name', 'proj_filter:display_name',
		    'text', '', '',
		    getSessionParam('proj_filter:display_name'));

    # genus
    printFilterCond('Genus', 'proj_filter:genus',
		    'text', '', '',
		    getSessionParam('proj_filter:genus'));

    # species
    printFilterCond('Species', 'proj_filter:species',
		    'text', '', '',
		    getSessionParam('proj_filter:species'));

    # common_name
    printFilterCond('Common Name', 'proj_filter:common_name',
		    'text', '', '',
		    getSessionParam('proj_filter:common_name'));

    # ncbi_project_name
    printFilterCond('NCBI Project Name', 'proj_filter:ncbi_project_name',
		    'text', '', '',
		    getSessionParam('proj_filter:ncbi_project_name'));

    # ncbi_project_id
    printFilterCond('NCBI Project ID', 'proj_filter:ncbi_project_id',
		    'number', '', '',
		    getSessionParam('proj_filter:ncbi_project_id'));

    # gold stamp ID
    printFilterCond('GOLD Stamp ID', 'proj_filter:gold_stamp_id',
		    'select', 'query',
		    'select distinct gold_stamp_id from project_info order by gold_stamp_id',
		    getSessionParam('proj_filter:gold_stamp_id'));

    # gold web page code
    printFilterCond('GOLD Web Page Code', 'proj_filter:web_page_code',
		    'select', 'query',
		    'select term_oid+1, description from web_page_codecv order by term_oid',
		    getSessionParam('proj_filter:web_page_code'));

    # add date
    printFilterCond('Add Date', 'proj_filter:add_date',
		    'date', '', '',
		    getSessionParam('proj_filter:add_date'));

    # mod date
    printFilterCond('Last Mod Date', 'proj_filter:mod_date',
		    'date', '', '',
		    getSessionParam('proj_filter:mod_date'));

    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_ProjectInfo:applyProjectFilter" value="Apply Project Filter" class="medbutton" />';
 
    print "<p>\n";
 
    printHomeLink();
 
    print end_form(); 
}


###########################################################################
# printFilterCond - print filter condition
###########################################################################
sub printFilterCond {
    my ($title, $param_name, $ui_type, $select_type, $select_method,
	$def_val_str) = @_;

    print "<p>" . escapeHTML($title) . "\n";

    my @vals = ();
    my @def_vals = ();
    if ( ! blankStr($def_val_str) ) {
	@def_vals = split(/\,/, $def_val_str);
    }

    if ( $select_type eq 'query' ) {
	my $dbh = Connect_IMG();
webLog("$select_method \n");	
	my $cur = $dbh->prepare($select_method);
	$cur->execute(); 
 
	for (my $j = 0;$j <= 100000; $j++) { 
	    my ( $v2, $cv_term ) = $cur->fetchrow_array(); 
	    if ( ! $v2 && ! $cv_term ) { 
		last; 
	    } 
 
	    my $v3 = $v2 . "|" . $cv_term;
	    push @vals, ( $v3 );
	} 
	$cur->finish(); 
	$dbh->disconnect(); 
    }
    elsif ( $select_type eq 'list' ) {
	@vals = split(/\|/, $select_method);
    }

    if ( $ui_type eq 'text' ) {
	print nbsp(3); 
	my $val2 = $def_val_str;
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='60' maxLength='255'/>\n";
    }
    elsif ( $ui_type eq 'number' ) {
	print nbsp(3); 
	my $op_name = $param_name . ":op";
	my $comp1 = getSessionParam($op_name);
	print "<select name='$op_name' class='img' size='1'>\n"; 
	for my $k ( '=', '!=', '>', '>=', '<', '<=',
		    'is null', 'is not null' ) {
	    my $k2 = escapeHTML($k); 
	    print "   <option value='$k2'"; 
	    if ( !blankStr($comp1) && $k eq $comp1 ) { 
		print " selected "; 
	    } 
	    print ">$k2</option>\n";
	}
	print "</select>\n"; 
	print nbsp( 1 ); 

	my $val2 = $def_val_str;
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='60' maxLength='80'/>\n";
    }
    elsif ( $ui_type eq 'date' ) {
	print nbsp(3); 
	my $op_name = $param_name . ":op";
	my $comp1 = getSessionParam($op_name);
	print "<select name='$op_name' class='img' size='1'>\n"; 
	print "   <option value='0'> </option>\n"; 
	for my $k ( '=', '!=', '>', '>=', '<', '<=' ) {
	    my $k2 = escapeHTML($k); 
	    print "   <option value='$k2'"; 
	    if ( !blankStr($comp1) && $k eq $comp1 ) { 
		print " selected "; 
	    } 
	    print ">$k2</option>\n";
	}
	print "</select>\n"; 
	print nbsp( 1 ); 

	my $val2 = $def_val_str;
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='20' maxLength='20'/>\n";
	print nbsp(6);
	print "<b>(Use 'DD-MON-YY' for Date format.)</b>\n";
    }
    elsif ( $ui_type eq 'checkbox' ) {
	for my $s2 ( @vals ) {
	    my ($id2, $val2) = split(/\|/, $s2);
	    if ( blankStr($val2) ) {
		$val2 = $id2;
	    }

	    print "<br/>\n"; 
	    print nbsp(3); 
	    print "<input type='checkbox' name='$param_name' value='$id2'";
	    if ( scalar(@def_vals) == 0 ||
		 inArray($id2, @def_vals) ) {
		print " checked ";
	    }
	    print ">". escapeHTML($val2) . "\n"; 
	}
    }
    elsif ( $ui_type eq 'select' ) {
	# print "<br/>\n"; 
	print nbsp(3); 
	print "<select name='$param_name' class='img' size='1'>\n"; 
	print "    <option value='' > </option>\n";
	for my $s2 ( @vals ) {
	    my ($id2, $val2) = split(/\|/, $s2);
	    if ( blankStr($val2) ) {
		$val2 = $id2;
	    }

	    print "    <option value='" . escapeHTML($id2) . "'";
	    if ( scalar(@def_vals) > 0 &&
		 inArray($id2, @def_vals) ) {
		print " selected ";
	    }
	    print ">$val2</option>\n";
	}
	print "</select>\n";
    }
}


###########################################################################
# printFilterCondRow - print filter condition in a table row
###########################################################################
sub printFilterCondRow {
    my ($title, $param_name, $ui_type, $select_type, $select_method,
	$def_val_str) = @_;

    print "<tr class='img'>\n";
    print "<td class='img'>" . escapeHTML($title) . "</td>\n";

    my @vals = ();
    my @def_vals = ();
    if ( ! blankStr($def_val_str) ) {
	@def_vals = split(/\,/, $def_val_str);
    }

    if ( $select_type eq 'query' ) {
	my $dbh = Connect_IMG();
webLog("$select_method \n");	
	my $cur = $dbh->prepare($select_method);
#	print "$title $select_method, $param_name, $ui_type, $select_method, $select_type<br>";
	$cur->execute(); 
 
	for (my $j = 0;$j <= 100000; $j++) { 
	    my ( $v2, $cv_term ) = $cur->fetchrow_array(); 
	    if ( ! $v2 && ! $cv_term ) { 
		last; 
	    } 
 
	    my $v3 = $v2 . "|" . $cv_term;
	    push @vals, ( $v3 );
	} 
	$cur->finish(); 
	$dbh->disconnect(); 
    }
    elsif ( $select_type eq 'list' ) {
	@vals = split(/\|/, $select_method);
    }

    if ( $ui_type eq 'text' ) {
	# print "<td class='img'>match</td>\n";
	# print nbsp(3); 
	my $op_name = $param_name . ":op";
	my $comp1 = getSessionParam($op_name);
	print "<td class='img'>";
	print "<select name='$op_name' class='img' size='1'>\n"; 
	print "   <option value='0'> </option>\n"; 
	for my $k ( 'match', 'is null', 'is not null' ) {
	    my $k2 = escapeHTML($k); 
	    print "   <option value='$k2'"; 
	    if ( !blankStr($comp1) && $k eq $comp1 ) { 
		print " selected "; 
	    } 
	    print ">$k2</option>\n";
	}
	print "</select></td>\n"; 

	my $val2 = $def_val_str;
	print "<td class='img'>";
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='60' maxLength='255'/>" . "</td>\n";
    }
    elsif ( $ui_type eq 'number' ) {
	# print nbsp(3); 
	my $op_name = $param_name . ":op";
	my $comp1 = getSessionParam($op_name);
	print "<td class='img'>";
	print "<select name='$op_name' class='img' size='1'>\n"; 
	print "   <option value='0'> </option>\n"; 
	for my $k ( '=', '!=', '>', '>=', '<', '<=',
		    'is null', 'is not null' ) {
	    my $k2 = escapeHTML($k); 
	    print "   <option value='$k2'"; 
	    if ( !blankStr($comp1) && $k eq $comp1 ) { 
		print " selected "; 
	    } 
	    print ">$k2</option>\n";
	}
	print "</select></td>\n"; 
	# print nbsp( 1 ); 

	my $val2 = $def_val_str;
	print "<td class='img'>";
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='60' maxLength='80'/>" . "</td>\n";
    }
    elsif ( $ui_type eq 'date' ) {
	print nbsp(3); 
	my $op_name = $param_name . ":op";
	my $comp1 = getSessionParam($op_name);
	print "<td class='img'>";
	print "<select name='$op_name' class='img' size='1'>\n"; 
	print "   <option value='0'> </option>\n"; 
	for my $k ( '=', '!=', '>', '>=', '<', '<=' ) {
	    my $k2 = escapeHTML($k); 
	    print "   <option value='$k2'"; 
	    if ( !blankStr($comp1) && $k eq $comp1 ) { 
		print " selected "; 
	    } 
	    print ">$k2</option>\n";
	}
	print "</select></td>\n"; 
	print nbsp( 1 ); 

	my $val2 = $def_val_str;
	print "<td class='img'>";
	print "<input type='text' name='$param_name' value='$val2'" .
	    " size='20' maxLength='20'/>\n";
	print nbsp(3);
	print "<b>(Use 'DD-MON-YY' for Date format.)</b></td>\n";
    }
    elsif ( $ui_type eq 'checkbox' ) {
	print "<td class='img'>contain</td>\n";

	for my $s2 ( @vals ) {
	    my ($id2, $val2) = split(/\|/, $s2);
	    if ( blankStr($val2) ) {
		$val2 = $id2;
	    }

	    print "<br/>\n"; 
	    print nbsp(3); 
	    print "<td class='img'>";
	    print "<input type='checkbox' name='$param_name' value='$id2'";
	    if ( scalar(@def_vals) == 0 ||
		 inArray($id2, @def_vals) ) {
		print " checked ";
	    }
	    print ">". escapeHTML($val2) . "\n"; 
	    print "<td class='img'>";
	}
    }
    elsif ( $ui_type eq 'select' ) {
	print "<td class='img'>contain</td>\n";

	# print "<br/>\n"; 
	print nbsp(3); 
	print "<td class='img'>";
	print "<select name='$param_name' class='img' size='1'>\n"; 
	print "    <option value='' > </option>\n";
	for my $s2 ( @vals ) {
	    my ($id2, $val2) = split(/\|/, $s2);
	    if ( blankStr($val2) ) {
		$val2 = $id2;
	    }

	    print "    <option value='" . escapeHTML($id2) . "'";
	    if ( scalar(@def_vals) > 0 &&
		 inArray($id2, @def_vals) ) {
		print " selected ";
	    }
	    print ">$val2</option>\n";
	}
	print "</select>\n";
	print "</td>\n";
    }

    print "</tr>\n";
}


########################################################################## 
# ApplyProjectFilter - save filter info into database and apply filter 
########################################################################## 
sub ApplyProjectFilter { 
 
    print start_form(-name=>'applyProjectFilter',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    # max display
    my $max_display = param1('proj_filter:max_display');
    if ( ! blankStr($max_display) ) {
	setSessionParam('proj_filter:max_display', $max_display);
    }

    # show filter selection 
    my @all_params = projectFilterParams();
    for my $p0 ( @all_params ) {
	if ( $p0 eq 'proj_filter:add_date' ||
	     $p0 eq 'proj_filter:mod_date' ) {
	    # date
	    my $op_name = $p0 . ":op";
	    my $op1 = param1($op_name);
	    my $d1 = param1($p0);
	    if ( !blankStr($d1) && !blankStr($op1) ) {
		if ( ! isDate($d1) ) {
		    print "<p>Incorrect Date (" . escapeHTML($d1) .
			") -- Filter condition is ignored.</p>\n";
		}
		else {
		    setSessionParam($op_name, $op1);
		    setSessionParam($p0, $d1);
		}
	    }
            else {
                # clear condition
                setSessionParam($op_name, '');
                setSessionParam($p0, '');
            } 
	}
	elsif ( param($p0) ) { 
	    # filter
	    my @options = param($p0); 

	    my $s0 = "";
	    for my $s1 ( @options ) {
		if ( blankStr($s0) ) {
		    $s0 = $s1;
		}
		else {
		    $s0 .= "," . $s1;
		}
	    }
	    setSessionParam($p0, $s0);
	}
	else { 
	    my ($tag1, $val1) = split(/\:/, $p0);
#	    print "<p>No selection on $val1. Default is set to selecting all.</p>\n";
	    setSessionParam($p0, '');
	}
    }

    # save parameters
    print "<h4>Project Filter is applied.</h4>\n"; 
 
    print "<p>\n"; 
 
    print '<input type="submit" name="_section_ProjectInfo:showPage" value="OK" class="smbutton" />';
 
    printHomeLink();
 
    print end_form();
} 


########################################################################## 
# DisplayProject
########################################################################## 
sub DisplayProject {
    my ($project_oid) = @_;

    print start_form(-name=>'displayProject',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $project_oid || ! isInt($project_oid) ) {
	return;
    }

    # check whether user can view this project
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'No' ) {
	printError ( "Obsolete Page." );
	return;
    }

    my $project_gold_id = db_getValue("select gold_stamp_id from project_info where project_oid = ?");
    if ( $isAdmin eq 'No' && ! $project_gold_id ) {
	# contact must be the owner of this project
	my $sql0 = qq{
	    select count(*) from project_info p
		where p.project_oid = $project_oid
		and (p.gold_stamp_id is not null
		     or p.contact_oid = $contact_oid
		     or p.project_oid in
		     (select cpp.project_permissions
		      from contact_project_permissions cpp
		      where cpp.contact_oid = $contact_oid))
	    };
	my $cnt0 = db_getValue($sql0);
	if ( ! $cnt0 ) {
	    my $email = db_getValue("select email from contact where contact_oid = $contact_oid"); 
	    my $pi_email = db_getValue("select p.contact_email from project_info p where p.project_oid = $project_oid");
	    if ( lc($email) eq lc($pi_email) ) { 
		# fine -- PI                                               
	    }
	    else {
		printError ( "You cannot view this project.");
		return; 
	    }
	}
    }

    my $proj_name = db_getValue("select display_name from project_info where project_oid = $project_oid");
#    my $proj_type = db_getValue("select project_type from project_info where project_oid = $project_oid");
    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

#    if ( $proj_type eq 'Metagenome' ) {
    if ( $proj_domain eq 'MICROBIAL' ) {
	print "<h2>Metagenome Project ";
    }
    else {
	print "<h2>Project ";
    }
    print "$project_oid (" . escapeHTML($proj_name) . ")</h2>\n";
    print hiddenVar('project_oid', $project_oid); 

    # get Project_Info definition
    my $def_project = def_Project_Info();
#    if ( $proj_type eq 'Metagenome' ) {
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }
    my @attrs = @{$def_project->{attrs}};

    # get data from database
    my %db_val = SelectProjectInfo($project_oid);

    if ( $isAdmin eq 'Yes' ) { 
        my $mod_by = $db_val{'modified_by'};
        if ( $mod_by =~ /IMG\_PIPELINE/ ) { 
            print "<h5>Created by IMG_PIPELINE from New GOLD API.</h5>\n";
        } 
    } 
 
    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 

    my @tabs;
#    if ( $proj_type eq 'Metagenome' ) {
    if ( $proj_domain eq 'MICROBIAL' ) {
	@tabs =  ( 'Metagenome', 'Project', 'Sequencing',
		   'EnvMeta', 'HostMeta', 'OrganMeta', 'HMP' );
    }
    else {
	@tabs =  ( 'Organism', 'Project', 'Sequencing',
		   'EnvMeta', 'HostMeta', 'OrganMeta', 'HMP' );
    }

    if ( $isAdmin eq 'Yes' ) {
	push @tabs, ( 'JGI' );
    }

    # tab display label
    my %tab_display = (
		       'Organism' => 'Organism Info',
		       'Metagenome' => 'Metagenome Info',
		       'Project' => 'Project Info',
		       'Sequencing' => 'Sequencing Info',
		       'EnvMeta' => 'Environment Metadata',
		       'HostMeta' => 'Host Metadata',
		       'OrganMeta' => 'Organism Metadata',
		       'HMP'=> 'HMP Metadata',
	               'JGI' => 'JGI Info' );

    for my $tab ( @tabs ) {
	my $tab_label = $tab;
	if ( $tab_display{$tab} ) {
	    $tab_label = $tab_display{$tab};
	}

        print "<tr class='img' >\n"; 
        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	    "<font color='darkblue'>" . "MIGS-ID" . "</font></th>\n"; 
        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	    "<font color='darkblue'>" . $tab_label . "</font></th>\n"; 
        print "  <td class='img'   align='left' bgcolor='lightblue'>" .
	    "</td>\n"; 
        print "</tr>\n"; 

	for my $k ( @attrs ) { 
	    if ( $k->{tab} ne $tab ) {
		next;
	    }

	    my $attr_val = ""; 
	    my $attr_name = $k->{name}; 
 
	    if ( defined $db_val{$attr_name} ) { 
		$attr_val = $db_val{$attr_name}; 
	    } 
	    else {
		next; 
	    } 
 
	    my $disp_name = $k->{display_name}; 
 
	    print "<tr class='img' >\n"; 

	    # MIGS
	    printCellTooltip($k->{migs_id}, $k->{migs_name}, 70);

	    # IMG-GOLD attribute
	    print "  <th class='subhead' align='right'>" . $disp_name .
		"</th>\n"; 

	    if ( $attr_name eq 'gold_stamp_id' ) {
		# gold
		print "  <td class='img'   align='left'>" . 
		    getGoldLink($attr_val) . "</td>\n";
	    }
            elsif ( $attr_name eq 'ncbi_project_id' ) {
                # NCBI project 
                print "  <td class='img'   align='left'>" .
                    getNcbiProjLink($attr_val) . "</td>\n";
            }
            elsif ( $attr_name eq 'ncbi_taxon_id' ) {
                # NCBI taxon 
                print "  <td class='img'   align='left'>" .
                    getNcbiTaxonLink($attr_val) . "</td>\n";
            }
            elsif ( $attr_name eq 'homd_id' ) {
                # HOMD ID
                print "  <td class='img'   align='left'>" . 
                    getHomdLink($attr_val) . "</td>\n";
            } 
            elsif ( $attr_name eq 'greengenes_id' ) {
		# Greengenes
                print "  <td class='img'   align='left'>" .
                    getGreengenesLink($attr_val) . "</td>\n";
            }
	    elsif ( $attr_val =~ /^http\:\/\// ||
		$attr_val =~ /^https\:\/\// ) {
		# url
		print "  <td class='img'   align='left'>" .
		    alink($attr_val, 'URL', 'target', 1) . "</td>\n";
	    }
	    elsif ( $attr_val =~ /^ftp\:\/\// ) {
		# ftp
		print "  <td class='img'   align='left'>" .
		    alink($attr_val, 'FTP', 'target', 1) . "</td>\n";
	    }
	    else {
		# regular attribute value
		print "  <td class='img'   align='left'>" . 
		    escapeHTML($attr_val) . "</td>\n"; 
	    }
	    print "</tr>\n"; 
	}

	if ( $tab eq 'Project' ) {
	    DisplayProjectSetAttr($project_oid,
				  'project_info_project_relevance',
				  ', ', ', ', 0);
	    DisplayProjectSetAttr($project_oid, 'project_info_data_links',
				  ', ', "<br/>", 0);
	}
	elsif ( $tab eq 'Sequencing' ) {
	    DisplayProjectSetAttr($project_oid,
				  'project_info_seq_method',
				  ', ', ', ', 0);
	}
	elsif ( $tab eq 'OrganMeta' ) {
	    DisplayProjectSetAttr($project_oid,
				  'project_info_cell_arrangement',
				  ', ', ', ', 0);
	    DisplayProjectCyanoSubTab($project_oid, 'project_info_cyano_metadata', \%db_val);
	    DisplayProjectSetAttr($project_oid,
				  'project_info_diseases',
				  ', ', ', ', 0);
	    DisplayProjectSetAttr($project_oid,
				  'project_info_habitat',
				  ', ', ', ', 0);
	    DisplayProjectSetAttr($project_oid,
				  'project_info_energy_source',
				  ', ', ', ', 0);
	    DisplayProjectSetAttr($project_oid,
				  'project_info_metabolism',
				  ', ', ', ', 0);
	    DisplayProjectSetAttr($project_oid,
				  'project_info_phenotypes',
				  ', ', ', ', 0);
	}
	elsif ( $tab eq 'JGI' ) {
	    my $show_its_contact = 0;
	    if ( $project_oid && $show_its_contact ) {
		my ($its_id, $uname, $uemail) = getItsContact($project_oid);
		if ( $uemail ) {
		    print "<tr class='img' >\n"; 
		    print "  <th class='subhead' align='right'>" . 
			"</th>\n"; 
		    print "  <th class='subhead' align='right'>" . "ITS Contact Info" .
			"</th>\n"; 
		    print "  <td class='img'   align='left'>" . $its_id .
			": " . $uname . " (" . $uemail . ")" . "</td>\n";
		    print "</tr>\n";
		}
	    }
	}
    }  # end for tab

    if ( $isAdmin eq 'Yes' ) {
	DisplayProjectSetAttr($project_oid, 'project_info_jgi_url', ', ', ', ', 0);
    }

    my @aux_tables = getProjectAuxTables ();
    for my $tname ( @aux_tables ) {
	if ( $tname eq 'project_info_data_links' ||
	     $tname eq 'project_info_project_relevance' ||
	     $tname eq 'project_info_seq_method'||
	     $tname eq 'project_info_cell_arrangement' ||
	     $tname eq 'project_info_energy_source' ||
	     $tname eq 'project_info_metabolism' ||
	     $tname eq 'project_info_phenotypes' ||
	     $tname eq 'project_info_habitat' ||
	     $tname eq 'project_info_cyano_metadata' ) {
	    next;
	}

	DisplayProjectSetAttr($project_oid, $tname, ', ', ', ', 0);
    }

    # samples?
    my $sample_cnt = db_getValue("select count(*) from env_sample where project_info = $project_oid");
    if ( $sample_cnt ) {
	# show samples
	print "<tr class='img' >\n"; 
	print "  <td class='img'></th>\n";
	print "  <th class='subhead' align='right'>" .
	    "Samples" . "</th>\n"; 
	print "  <td class='img'   align='left'>";

	my $sql2 = "select sample_oid, sample_display_name " .
	    "from env_sample where project_info = $project_oid";
	my $dbh2 = Connect_IMG();
webLog("$sql2\n");	
	my $cur2 = $dbh2->prepare($sql2);
	$cur2->execute(); 
 
	for (my $j = 0;$j <= 100000; $j++) { 
	    my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
	    last if !$s_oid;

	    my $sample_link = getSampleLink($s_oid);
	    print $sample_link . " - ";
	    print escapeHTML($s_name);
	    print "<br/>\n";
	}
	$cur2->finish();
	$dbh2->disconnect();

	print "  </td>\n";

	print "</tr>\n";
    }

    # NPs
    my $np_cnt = db_getValue("select count(*) from project_info_natural_prods " .
			     "where project_oid = $project_oid");
    if ( $project_oid && $np_cnt ) {
	# show NPs
	print "<tr class='img'></tr>\n";
	print "<tr class='img' >\n"; 
        print "  <td class='img'   align='left' bgcolor='lightblue'>";
	if ( $np_cnt > 1 ) {
	    print "Natural Products" . "</th>\n"; 
	}
	else {
	    print "Natural Product" . "</th>\n"; 
	}
	print "</tr>\n";

	my $sql2 = "select gnp.gold_np_id, gnp.evidence, gnp.activity, " .
	    "np.np_id, np.np_product_name, np.np_product_link, " .
	    "np.np_type, np.np_activity, gnp.genbank_id " .
	    "from cvnatural_prods np, project_info_natural_prods gnp " .
	    "where gnp.project_oid = ? and gnp.np_id = np.np_id";
	my $dbh2 = Connect_IMG();
	my $cur2 = $dbh2->prepare($sql2);
	$cur2->execute($project_oid);

	for (;;) {
	    my ( $gold_np_id, $evd, $act, $np_id, $np_name, $np_link, 
		 $np_type, $np_act, $genbank_id) = $cur2->fetchrow_array(); 
	    last if !$gold_np_id;

	    print "<tr class='img'>\n";
	    print "  <td class='img'>Product " . $gold_np_id . "</td>\n";
	    print "  <th class='subhead' align='right'>Evidence</th>\n"; 
	    print "  <td class='img' align='left'>" . $evd . "</td>\n";
	    print "</tr>\n";
	    print "<tr class='img'>\n";
	    print "  <td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>Activity</th>\n"; 
	    print "  <td class='img' align='left'>" . $act . "</td>\n";
	    print "</tr>\n";

	    print "<tr class='img'>\n";
	    print "  <td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>NP Product Name</th>\n"; 
	    print "  <td class='img' align='left'>" . $np_name . "</td>\n";
	    print "</tr>\n";
	    print "<tr class='img'>\n";
	    print "  <td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>NP Product Link</th>\n"; 
	    print "  <td class='img' align='left'>" . $np_link . "</td>\n";
	    print "</tr>\n";
	    print "<tr class='img'>\n";
	    print "  <td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>NP Type</th>\n"; 
	    print "  <td class='img' align='left'>" . $np_type . "</td>\n";
	    print "</tr>\n";
	    print "<tr class='img'>\n";
	    print "  <td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>NP Activity</th>\n"; 
	    print "  <td class='img' align='left'>" . $np_act . "</td>\n";
	    print "</tr>\n";
	    print "<tr class='img'>\n";
	    print "  <td class='img'></td>\n";
	    print "  <th class='subhead' align='right'>Genbank ID</th>\n"; 
	    print "  <td class='img' align='left'>" . $genbank_id . "</td>\n";
	    print "</tr>\n";
	}
	$cur2->finish();
	$dbh2->disconnect();
    }

    # submissions?
#    my @subs = db_getValues("select submission_id from submission where project_info = $project_oid");
#    if ( scalar(@subs) > 0 ) {
	# show submissions
#        print "<tr class='img' >\n"; 
#        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
#	    "<font color='darkblue'>" . "ER Submission Info" . "</font></th>\n"; 
#        print "  <td class='img'   align='left' bgcolor='lightblue'>" .
#	    "</td>\n"; 
#        print "</tr>\n"; 

#	print "<tr class='img' >\n"; 
#	print "  <th class='subhead' align='right'>" .
#	    "Submissions" . "</th>\n"; 

#	my $sub_str = "";
#	for my $s1 ( @subs ) {
#	    my $submit_link = getSubmissionLink($s1);
#	    $sub_str .= $submit_link . " ";
#	}

#	print "  <td class='img'   align='left'>";
#	print $sub_str;
#	print "  </td></tr>\n";
#    }
	
    print "</table>\n"; 

    print "<p>\n"; 

    my $url = url();
    if ( $url =~ /hmp\.cgi/ ) {
	print '<input type="submit" name="_section_ProjectInfo:hmpPrintableData" value="Show HMP Metadata in Printable Format" class="lgbutton" />';
    }
    else {
	print '<input type="submit" name="_section_ProjectInfo:printableData" value="Show Metadata in Printable Format" class="lgbutton" />';
    }

    printHomeLink();

    print end_form();
}

sub getItsContact {
    my ($project_oid) = @_;

    my $dbh = Connect_IMG();
    my $sql = qq{
        select "c".CONTACT_ID, "c".FIRST_NAME, "c".MIDDLE_NAME,  
        "c".LAST_NAME, "c".EMAIL_ADDRESS 
        from project_info pi 
        inner join DW.PROPOSAL\@itslink p ON pi.ITS_PROPOSAL_ID = p.PROPOSAL_ID 
        LEFT OUTER JOIN DW.CONTACT\@itslink "c" 
        ON p.PRINCIPAL_INVESTIGATOR_ID = "c".CONTACT_ID where project_oid = ?
        };
    my $cur=$dbh->prepare($sql); 
    $cur->execute($project_oid); 
    my ($id, $first, $middle, $last, $email) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect();
    my $name = $first . " " . $middle . " " . $last;
    return ($id, $name, $email);
}


########################################################################## 
# SetGoldStampId
########################################################################## 
sub SetGoldStampId {
    my ($project_oid) = @_;

    print start_form(-name=>'setGoldStampId',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $project_oid || ! isInt($project_oid) ) {
        printError ( "No project is selected." );
	return;
    }

    print "<h2>Assign GOLD Stamp ID to Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid); 

    # get project info
    my $dbh = Connect_IMG(); 

    # SQL statement 
    my $sql = "select project_type, project_status, web_page_code," .
	" gold_stamp_id from project_info" .
	" where project_oid = $project_oid";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $p_type, $p_status, $w_code, $gold_stamp_id ) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect(); 

    my $gold_type = getGoldType($p_type, $p_status, $w_code);
    my $old_gold_type = "";
    if ( ! blankStr($gold_stamp_id) && length($gold_stamp_id) >= 2 ) {
	$old_gold_type = substr($gold_stamp_id, 0, 2);
    }

    if ( ! blankStr($gold_stamp_id) ) {
	print "<h4>Existing GOLD Stamp ID for this project: " .
	    escapeHTML($gold_stamp_id) . "</h4>\n";
	print hiddenVar('gold_stamp_id', $gold_stamp_id); 
    }

    if ( blankStr($gold_type) ) {
	# cannot assign gold stamp ID
	print "<h4>Cannot assign GOLD Stamp ID to this project:</h4>\n";
	print "<p>Project Type: " . escapeHTML($p_type) . "</p>\n";
	print "<p>Project Status: " . escapeHTML($p_status) . "</p>\n";
	print "<p>Web Page Code: " . escapeHTML($w_code) . "</p>\n";
	return;
    }
    elsif ( $gold_type eq $old_gold_type ) {
	# ask confirmation
	print "<h4>There are no changes to project status. Do you still want to assign a new GOLD Stamp ID (with type $gold_type) to this project?</h4>\n";
    }
    else {
	# assign new Gold stamp ID
	print "<h4>Proceed to assign GOLD Stamp ID (with type $gold_type) to this project?</h4>\n";
    }

    print hiddenVar('gold_type', $gold_type); 

    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:dbSetGoldStampId" value="OK" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="Cancel" class="smbutton" />';

    printHomeLink();

    print end_form();
}

########################################################################## 
# getGoldType - determine Gold Stamp ID type based on Nikos' rules
########################################################################## 
sub getGoldType {
    my ($project_type, $project_status, $web_page_code) = @_;

    if ( inArray( $web_page_code, (1, 2, 3) ) &&
	 inArray( $project_type, ( 'Genome-Isolate', 'Genome-Uncultured', 'Draft-Genome',
				   'Genome-Regions', 'Genome-Survey',
				   'Genome-Engineered', 'Genome-Population Enrichment',
				   'Genome-Single Cell', 
				   'Metagenome')) &&
	 inArray( $project_status, ( 'complete', 'incomplete', 'Permanent Draft',
				      'targeted' )))  {
	return 'Gi';
    }
    elsif ( inArray( $web_page_code, (1, 2, 3) ) &&
	    $project_type eq 'EST' &&
	    inArray( $project_status, ( 'complete', 'incomplete',
					 'targeted' )) ) {
	return 'Ge';
    }
    elsif ( inArray( $web_page_code, (1, 2, 3) ) &&
	    $project_type eq 'Resequencing' &&
	    inArray( $project_status, ( 'complete', 'incomplete',
					 'targeted' )) ) {
	return 'Gr';
    }
    elsif ( inArray( $web_page_code, (1, 2, 3) ) &&
	    $project_type eq 'Transcriptome' &&
	    inArray( $project_status, ( 'complete', 'incomplete',
					 'targeted' )) ) {
	return 'Gt';
    }
#    elsif ( ($web_page_code == 4 &&
    elsif (
	    $project_type eq 'Metagenome' &&
	    inArray( $project_status, ( 'complete', 'incomplete',
					 'targeted' )) ) {
	return 'Gm';
    }
    elsif ( $web_page_code == 0 &&
	    inArray( $project_type, ( 'Genome-Isolate', 'Genome-Uncultured', 'Draft-Genome',
				      'Genome-Regions', 'Genome-Survey',
				      'Genome-Engineered', 'Genome-Population Enrichment',
				      'Genome-Single Cell', 
				      'Metagenome', 'Resequencing', 'Transcriptome' )) &&
	    $project_status eq 'Complete and Published' ) {
	return 'Gc';
    }

    return '';
}


########################################################################## 
# dbSetGoldStampId - update database to set gold stamp id
########################################################################## 
sub dbSetGoldStampId {
    my ($project_oid, $gold_type, $gold_stamp_id) = @_;

    if ( blankStr($project_oid) ) {
	return "No project OID is given.";
    }
    if ( blankStr($gold_type) ) {
	return "No GOLD Type is given.";
    }

    my $new_gold_id = getNewCounterVal($gold_type);
    if ( blankStr($new_gold_id) ) {
	return "Cannot get a new GOLD Stamp ID.";
    }

    # get GCAT ID
    my $gcat_id = db_getValue("select gcat_id from project_info where project_oid = $project_oid");
    my $new_gcat_id = "";
    if ( blankStr($gcat_id) ) {
	$new_gcat_id = getNewGcatId();
	if ( blankStr($new_gcat_id) ) {
	    return "Cannot get a new GCAT ID.";
	}
    }

    # get HMP ID?
    my $new_hmp_id = "";
    if ( getIsHmpProject($project_oid) ) {
	my $hmp_id = db_getValue("select hmp_id from project_info where project_oid = $project_oid");
	if ( blankStr($hmp_id) ) {
	    $new_hmp_id = getNewHmpId();
	    if ( blankStr($new_hmp_id) ) {
		return "Cannot get a new HMP ID.";
	    }
	}
    }

    my $contact_oid = getContactOid();
    my $sql = "update project_info set modified_by = $contact_oid, " .
	"mod_date = sysdate, gold_stamp_id = '" . $new_gold_id . "'";

    if ( ! blankStr($gold_stamp_id) ) {
	$gold_stamp_id =~ s/'/''/g;   # replace ' with ''
	$sql .= ", gold_id_old = '" . $gold_stamp_id . "'";
    }

    if ( ! blankStr($new_gcat_id) ) {
	$new_gcat_id =~ s/'/''/g;   # replace ' with ''
	$sql .= ", gcat_id = '" . $new_gcat_id . "'";
    }

    if ( ! blankStr($new_hmp_id) ) {
	$sql .= ", hmp_id = " . $new_hmp_id;
    }

    $sql .= " where project_oid = $project_oid";

    my @sqlList = ( );
    push @sqlList, ( $sql );

#    return $sql;

    db_sqlTrans(\@sqlList);

    return "";
}

########################################################################## 
# DeleteGoldStampId
########################################################################## 
sub DeleteGoldStampId {
    my ($project_oid) = @_;

    print start_form(-name=>'deleteGoldStampId',-method=>'post',action=>"$section_cgi"); 

    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $project_oid || ! isInt($project_oid) ) {
        printError ( "No project is selected." );
	return;
    }

    print "<h2>Delete GOLD Stamp ID from Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid); 

    # get project info
    my $dbh = Connect_IMG(); 

    # SQL statement 
    my $sql = "select gold_stamp_id, gold_id_old" .
	" from project_info" .
	" where project_oid = $project_oid";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $gold_stamp_id, $gold_id_old ) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect(); 

    print "<p>GOLD Stamp ID: " . escapeHTML($gold_stamp_id) . "</p>\n";
    print "<p>GOLD Stamp Old ID: " . escapeHTML($gold_id_old) . "</p>\n";

    if ( ! blankStr($gold_stamp_id) ) {
	print hiddenVar('gold_stamp_id', $gold_stamp_id); 
    }

    print "<h4>Please select one of the following options:</h4>\n";
    print "<input type='radio' name='del_gold_id_option' " .
	"value='1' />Delete both GOLD Stamp ID and Old ID<br/>\n";
    print "<input type='radio' name='del_gold_id_option' " .
	"value='2' />Delete GOLD Stamp ID, but save this ID in Old ID<br/>\n";
    print "<input type='radio' name='del_gold_id_option' " .
	"value='3' />Delete GOLD Stamp ID, but do not change Old ID<br/>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:dbDelGoldStampId" value="OK" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="Cancel" class="smbutton" />';

    printHomeLink();

    print end_form();
}

########################################################################## 
# dbDelGoldStampId - delete GOLD ID for a project
########################################################################## 
sub dbDelGoldStampId {
    my ($project_oid, $del_option, $gold_stamp_id) = @_;

    if ( blankStr($project_oid) || ! isInt($project_oid) ) {
	return "No project OID is given.";
    }
    if ( blankStr($del_option) ) {
	return "No deletion option is given.";
    }

    my $contact_oid = getContactOid();
    my $sql = "update project_info set modified_by = $contact_oid, " .
	"mod_date = sysdate, gcat_id = null, " .
	"hmp_id = null, gold_stamp_id = null";

    if ( $del_option == 1 ) {
	$sql .= ", gold_id_old = null";
    }
    elsif ( $del_option == 2 ) {
	if ( ! blankStr($gold_stamp_id) ) {
	    $gold_stamp_id =~ s/'/''/g;   # replace ' with ''
	    $sql .= ", gold_id_old = '" . $gold_stamp_id . "'";
	}
	else {
	    $sql .= ", gold_id_old = null";
	}
    }

    $sql .= " where project_oid = $project_oid";

    my @sqlList = ( );
    push @sqlList, ( $sql );

    db_sqlTrans(\@sqlList);

    return "";
}

########################################################################## 
# DisplayProjectSetAttr
#
# aux_name: set-valued table name
# t1: field/attr separator
# t2: record/row separator
# value_only: no attribute name display
########################################################################## 
sub DisplayProjectSetAttr {
    my ($project_oid, $aux_name, $t1, $t2, $value_only) = @_;
    my $def_aux = def_Class($aux_name);
    if ( ! $def_aux ) {
	return;
    }

    my $id_attr = $def_aux->{id};
    my $sql = "select $id_attr";
    my $order_by = " order by $id_attr";

    my @aux_attrs = @{$def_aux->{attrs}};
    for my $attr ( @aux_attrs ) {
	if ( $attr->{name} eq $id_attr ) {
	    next;
	}

	$sql .= ", " . $attr->{name};
	$order_by .= ", " . $attr->{name};
    }

    $sql .= " from " . $aux_name . " where " . $id_attr .
	" = " . $project_oid . $order_by;

    my $dbh = Connect_IMG();
webLog("$sql\n");    
    my $cur = $dbh->prepare($sql);
    $cur->execute(); 

    my $j = 0;
    for ($j = 0;$j <= 100000; $j++) { 
	my ($id_val, @vals) = $cur->fetchrow_array();
	last if !$id_val;

	if ( $j == 0 ) {
	    # first value
	    if ( ! $value_only ) {
		print "<tr class='img' >\n"; 
		printCellTooltip($def_aux->{migs_id},
				 $def_aux->{migs_name}, 70);

		print "  <th class='subhead' align='right'>" .
		    $def_aux->{display_name} . "</th>\n"; 
	    }
	    print "  <td class='img'   align='left'>";
	}
	else {
	    # print row separator
	    print $t2;
	}

	for (my $k = 0; $k < scalar(@vals); $k++ ) {
	    my $val = $vals[$k];

	    if ( $val =~ /^http\:\/\// ||
		 $val =~ /^https\:\/\// ) {
		# url
		print alink($val, 'URL', 'target', 1);
	    }
	    elsif ( $val =~ /^ftp\:\/\// ) {
		# ftp
		print alink($val, 'FTP', 'target', 1);
	    }
	    else {
		print escapeHTML($val);
	    }
	    if ( $k < scalar(@vals) - 1 ) {
		print $t1;
	    }
	}
    }

    if ( $j > 0 ) {
	# has data
	print "</td>";
	if ( ! $value_only ) {
	    print "</tr>\n";
	}
    }
    elsif ( $value_only ) {
	# show empty cell
	print "  <td class='img'   align='left'></td>\n";
    }

    $cur->finish();
    $dbh->disconnect();
}

########################################################################## 
# DisplayProjectSetAttr
#
# aux_name: set-valued table name
# t1: field/attr separator
# t2: record/row separator
# value_only: no attribute name display
########################################################################## 
sub DisplayProjectCyanoSubTab {
    my ($project_oid, $aux_name, $db_val) = @_;
    my $def_aux = def_Class($aux_name);
    if ( ! $def_aux ) {
	return;
    }

    my $id_attr = $def_aux->{id};
    my $sql = "select $id_attr";
    my $order_by = " order by $id_attr";

    my @aux_attrs = @{$def_aux->{attrs}};
    for my $attr ( @aux_attrs ) {
	if ( $attr->{name} eq $id_attr ) {
	    next;
	}

	$sql .= ", " . $attr->{name};
	$order_by .= ", " . $attr->{name};
    }

    $sql .= " from " . $aux_name . " where " . $id_attr .
	" = " . $project_oid . $order_by;
    my $dbh = Connect_IMG();
webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute(); 

	for my $k ( @aux_attrs ) { 

	    my $attr_val = ""; 
	    my $attr_name = $k->{name}; 
	    if ($attr_name eq 'project_oid') {
		next;
	    }
	    if ( defined $db_val->{$attr_name} ) { 
		$attr_val = $db_val->{$attr_name}; 
	    } 
	    else {
		next; 
	    } 
 
	    my $disp_name = $k->{display_name}; 
 
	    print "<tr class='img' >\n"; 

	    # MIGS
	    printCellTooltip($k->{migs_id}, $k->{migs_name}, 70);

	    # IMG-GOLD attribute
	    print "  <th class='subhead' align='right'>" . $disp_name .
		"</th>\n"; 
	    
	    if ( $attr_val =~ /^http\:\/\// ||
		$attr_val =~ /^https\:\/\// ) {
		# url
		print "  <td class='img'   align='left'>" .
		    alink($attr_val, 'URL', 'target', 1) . "</td>\n";
	    }
	    elsif ( $attr_val =~ /^ftp\:\/\// ) {
		# ftp
		print "  <td class='img'   align='left'>" .
		    alink($attr_val, 'FTP', 'target', 1) . "</td>\n";
	    }
	    else {
		# regular attribute value
		print "  <td class='img'   align='left'>" . 
		    escapeHTML($attr_val) . "</td>\n"; 
	    }
	    print "</tr>\n"; 
	}

    $cur->finish();
    $dbh->disconnect();
}

######################################################################### 
# CopyProject
######################################################################### 
sub CopyProject {
    my ($project_oid) = @_;

    # check project oid
    if ( blankStr($project_oid) ) {
	printError("No project has been selected.");
	print end_form();
	return;
    }

#    my %db_val = SelectProjectInfo($project_oid);
    my $gold_stamp_id = db_getValue("select gold_stamp_id from project_info where project_oid = $project_oid");

    print start_form(-name=>'copyProject',-method=>'post',action=>"$section_cgi"); 

    print "<h2>Copy Project $project_oid</h2>\n"; 
    print hiddenVar('project_oid', $project_oid); 

    # get new project oid
    my $new_proj_oid = 0;
    if ( $gold_stamp_id ) {
	# GOLD project
	# use higher range project oid for GOLD entry
	$new_proj_oid = db_findMaxID('project_info', 'project_oid') + 1;
    }
    else {
	$new_proj_oid = getNewProjectId();
    }
    if ( $new_proj_oid <= 0 ) {
	# error checking -- this shouldn't happen
	printError("Incorrect new project ID value: $new_proj_oid");
	print end_form();
	return;
    }

    print "<h3>New Project ID: $new_proj_oid</h3>\n";

#    $new_proj_oid = 2;
    my @sqlList = ();

    my $contact_oid = getContactOid(); 
    my $ins = "insert into project_info(project_oid, contact_oid, add_date";
    my $sel = "select $new_proj_oid, $contact_oid, sysdate";

    # get Project_Info definition
    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

    # get Project_Info definition
    my $def_project = def_Project_Info();
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }

    my @attrs = @{$def_project->{attrs}};
    for my $attr ( @attrs ) {
	if ( $attr->{name} eq 'pmo_project_id' ||
		$attr->{name} eq 'jgi_dir_number' ||
		$attr->{name} eq 'hmp_id' ||
		$attr->{name} eq 'ncbi_project_id' ) {
	    # skip pmo_project_id etc.
	    next;
	}
	elsif ( $attr->{name} =~ /^ncbi\_/ ) {
	    # ok - NCBI taxon info
	}
	elsif ( ! $attr->{can_edit} ) {
	    # skip non-editable attribute
	    next;
	}

	$ins .= ", " . $attr->{name};
	$sel .= ", " . $attr->{name};
    }

    my $sql = $ins . ") " . $sel . " from project_info " .
	"where project_oid = " . $project_oid;
    push @sqlList, ( $sql );

    # set-valued attributes
    my @tables = getProjectAuxTables();
    push @tables, ( 'project_info_jgi_url' );

    for my $tname ( @tables ) {
	my $def_aux = def_Class($tname);
	if ( ! $def_aux ) {
	    next;
	}

	my @aux_attrs = @{$def_aux->{attrs}};
	$ins = "insert into $tname (" . $def_aux->{id};
	$sel = "select $new_proj_oid";
	for my $attr2 ( @aux_attrs ) {
	    if ( $attr2->{name} eq $def_aux->{id} ) {
		next;
	    }

	    $ins .= ", " . $attr2->{name};
	    $sel .= ", " . $attr2->{name};
	}
	$sql = $ins . ") " . $sel . " from " . $tname .
	    " where " . $def_aux->{id} . " = " . $project_oid;
	print $sql;
	push @sqlList, ( $sql );
    }

    db_sqlTrans(\@sqlList);

    print "<p>New project has been created by copying project " .
	$project_oid . ".</p>\n";

    print "<p>Note: Samples of this project (if any) are not copied.</p>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="OK" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}


######################################################################### 
# MergeProjects
######################################################################### 
sub MergeProjects {
    my ($project_oid, $merged_project) = @_;

    # check project oid
    if ( blankStr($project_oid) || blankStr($merged_project) ) {
	printError("No project has been selected.");
	print end_form();
	return;
    }

    my %proj_val = SelectProjectInfo($project_oid);
    my %m_proj_val = SelectProjectInfo($merged_project);

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    if ( $proj_val{'project_type'} eq 'Metagenome' ) {
	print "<h2>Merge Metagenome Projects $project_oid and $merged_project</h2>\n"; 
    }
    else {
	print "<h2>Merge Projects $project_oid and $merged_project</h2>\n"; 
    }
    print hiddenVar('project_oid', $project_oid); 
    print hiddenVar('merged_project', $merged_project);

    # get Project_Info definition
    my $def_project;
    if ( $proj_val{'project_type'} eq 'Metagenome' ) {
	$def_project = def_meta_Project_Info();
    }
    else {
	$def_project = def_Project_Info();
    }

    my @attrs = @{$def_project->{attrs}};

    # add javascript function
    print "\n<script language=\"javascript\" type=\"text/javascript\">\n\n";
    print "function setSelectMerge( x ) {\n";
    print "   var f = document.mainForm;\n";
    print "   for ( var i = 0; i < f.length; i++ ) {\n";
    print "         var e = f.elements[ i ];\n";
    print "         if ( e.name.match( /^select:/ ) ) {\n";
    print "              if ( x == 0 ) {\n";
    print "                 if ( e.value == 'union' ) {\n";
    print "                      e.checked = true;\n";
    print "                    }\n";
    print "                 }\n";

    print "              if ( e.value == x && ! e.disabled ) {\n";
    print "                   e.checked = true;\n";
    print "                 }\n";

    print "            }\n";
    print "         }\n";
    print "   }\n";
    print "\n</script>\n\n";

    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 
    print "<th class='img'>Field Name</th>\n";
    print "<th class='img'>";
    print "<input type='button' value='Select $project_oid' " .
	"Class='tinybutton'\n";
    print "  onClick='setSelectMerge ($project_oid)' />\n";
    print "</th>\n";
    print "<th class='img'>Project $project_oid</th>\n";

    print "<th class='img'>";
    print "<input type='button' value='Select $merged_project' " .
	"Class='tinybutton'\n";
    print "  onClick='setSelectMerge ($merged_project)' />\n";
    print "</th>\n";
    print "<th class='img'>Project $merged_project</th>\n";

    print "<th class='img'>\n";
    print "<input type='button' value='Union Both' Class='tinybutton'\n";
    print "  onClick='setSelectMerge (0)' />\n";
    print "</th>\n";

    my @tabs =  ( 'Project', 'Organism', 'Links', 'Metadata' );
    if ( $proj_val{'project_type'} eq 'Metagenome' ) {
	@tabs =  ( 'Project', 'Metagenome', 'Links', 'Metadata' );
    }
    for my $tab ( @tabs ) {
        print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	    "<font color='darkblue'>" . $tab . "</font></th>\n"; 
        # print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n"; 
        print "</tr>\n"; 

	for my $k ( @attrs ) { 
	    if ( $k->{tab} ne $tab ) {
		next;
	    }

	    my $attr_name = $k->{name}; 
	    if ( $attr_name eq 'modified_by' ||
		 $attr_name eq 'mod_date' ) {
		# these attribute values are system controlled
		next;
	    }

	    my $attr_val = ""; 
	    my $m_attr_val = "";
 
	    if ( defined $proj_val{$attr_name} ) {
		$attr_val = $proj_val{$attr_name};
	    }
	    if ( defined $m_proj_val{$attr_name} ) { 
		$m_attr_val = $m_proj_val{$attr_name}; 
	    } 

	    if ( blankStr($attr_val) && blankStr($m_attr_val) ) {
		next; 
	    } 
 
	    my $disp_name = $k->{display_name}; 
 
	    print "<tr class='img' >\n"; 
	    if ( $k->{font_color} ) {
		print "  <th class='subhead' align='right'>" .
		    "<font color='" . $k->{font_color} .
		    "'>" . nbsp(5) . $disp_name . "</font></th>\n";
	    } 
	    else {
		print "  <th class='subhead' align='right'>" . $disp_name;
	    }
	    print "</th>\n"; 

	    # display select button
	    print "<td class='img'>\n";
	    if ( $attr_name eq 'ncbi_superkingdom' || 
		 $attr_name eq 'ncbi_phylum' ||
		 $attr_name eq 'ncbi_class' || 
		 $attr_name eq 'ncbi_order' ||
		 $attr_name eq 'ncbi_family' ||
		 $attr_name eq 'ncbi_genus' ||
		 $attr_name eq 'ncbi_species' ) {
		# not allowing selection
	    }
	    else {
		print "<input type='radio' name='select:$attr_name' " .
		    "value='$project_oid' checked />\n";
	    }
	    print "</td>\n";

	    # display attribute value
	    if ( $attr_name eq 'gold_stamp_id' ) {
		# gold
		if ( ! blankStr($attr_val) ) {
		    print "  <td class='img'   align='left'>" . 
			getGoldLink($attr_val) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}

		print "<td class='img'>\n";
		print "<input type='radio' name='select:$attr_name' value='$merged_project' />\n";
		print "</td>\n";
		if ( ! blankStr($m_attr_val) ) {
		    print "  <td class='img'   align='left'>" . 
			getGoldLink($m_attr_val) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}
	    }
	    elsif ( $attr_val =~ /^http\:\/\// ||
		$attr_val =~ /^https\:\/\// ||
		$attr_val =~ /^ftp\:\/\// ) {
		# url or ftp
		my $url_type = "URL";
		if ( $attr_val =~ /^ftp\:\/\// ) {
		    $url_type = "FTP";
		}

		if ( ! blankStr($attr_val) ) {
		    print "  <td class='img'   align='left'>" .
			alink($attr_val, $url_type, 'target', 1) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}

		print "<td class='img'>\n";
		print "<input type='radio' name='select:$attr_name' value='$merged_project' />\n";
		print "</td>\n";
		if ( ! blankStr($m_attr_val) ) {
		    print "  <td class='img'   align='left'>" .
			alink($m_attr_val, 'URL', 'target', 1) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}
	    }
	    else {
		# regular attribute value
		if ( ! blankStr($attr_val) ) {
		    if ( $attr_name eq 'ncbi_project_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getNcbiProjLink($attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'ncbi_taxon_id' ) {
			# NCBI taxon 
			print "  <td class='img'   align='left'>" .
			    getNcbiTaxonLink($attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'homd_id' ) {
			# HOMD ID
			print "  <td class='img'   align='left'>" . 
			    getHomdLink($attr_val) . "</td>\n";
		    } 
		    elsif ( $attr_name eq 'greengenes_id' ) {
			# Greengenes
			print "  <td class='img'   align='left'>" .
			    getGreengenesLink($attr_val) . "</td>\n";
		    }
		    else {
			print "  <td class='img'   align='left'>" . 
			    escapeHTML($attr_val) . "</td>\n"; 
		    }
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}

		# display select button
		print "<td class='img'>\n";
		if ( $attr_name eq 'ncbi_superkingdom' || 
		     $attr_name eq 'ncbi_phylum' ||
		     $attr_name eq 'ncbi_class' || 
		     $attr_name eq 'ncbi_order' ||
		     $attr_name eq 'ncbi_family' ||
		     $attr_name eq 'ncbi_genus' ||
		     $attr_name eq 'ncbi_species' ) {
		    # not allowing selection
		}
		else {
		    print "<input type='radio' name='select:$attr_name' " .
			"value='$merged_project' />\n";
		}
		print "</td>\n";
		if ( ! blankStr($m_attr_val) ) {
		    if ( $attr_name eq 'ncbi_project_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getNcbiProjLink($m_attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'ncbi_taxon_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getNcbiTaxonLink($m_attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'homd_id' ) {
			# HOMD ID
			print "  <td class='img'   align='left'>" . 
			    getHomdLink($m_attr_val) . "</td>\n";
		    } 
		    elsif ( $attr_name eq 'greengenes_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getGreengenesLink($m_attr_val) . "</td>\n";
		    }
		    else {
			print "  <td class='img'   align='left'>" . 
			    escapeHTML($m_attr_val) . "</td>\n"; 
		    }
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}
	    }
	    print "<td class='img'>\n";
	    # print "<input type='radio' name='select:$attr_name' value='union' />\n";

	    print "</td>\n";

	    print "</tr>\n"; 
	}

	if ( $tab eq 'Links' ) {
	    print "<tr class='img' >\n"; 
	    print "  <th class='subhead' align='right'>" . 
		"Data Links" . "</th>\n"; 

	    my $tname = 'project_info_data_links';
	    print "<td class='img'>\n";
	    print "<input type='radio' name='select:$tname' value='$project_oid' checked />\n";
	    print "</td>\n";
	    DisplayProjectSetAttr($project_oid, 'project_info_data_links',
				  ', ', "<br/>", 1);
	    print "<td class='img'>\n";
	    print "<input type='radio' name='select:$tname' value='$merged_project' />\n";
	    print "</td>\n";
	    DisplayProjectSetAttr($merged_project, 'project_info_data_links',
				  ', ', "<br/>", 1);
	    print "<td class='img'>\n";
	    print "<input type='radio' name='select:$tname' value='union' />\n";
	    print "</td>\n";
	    print "</tr>\n";
	}
    }  # end for tab

    my @aux_tables = getProjectAuxTables ();
    push @aux_tables, ( 'project_info_jgi_info' );
    for my $tname ( @aux_tables ) {
	if ( $tname eq 'project_info_data_links' ) {
	    next;
	}

	my $def_aux = def_Class($tname);
	if ( ! $def_aux ) {
	    next;
	}

	if ($tname eq 'project_info_cyano_metadata') {
	    next;	
	}

	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right'>" . 
	    $def_aux->{display_name} . "</th>\n"; 

	print "<td class='img'>\n";
	print "<input type='radio' name='select:$tname' value='$project_oid' checked />\n";
	print "</td>\n";
	DisplayProjectSetAttr($project_oid, $tname, ', ', ', ', 1);

	print "<td class='img'>\n";
	print "<input type='radio' name='select:$tname' value='$merged_project' />\n";
	print "</td>\n";
	DisplayProjectSetAttr($merged_project, $tname, ', ', ', ', 1);

	print "<td class='img'>\n";
	print "<input type='radio' name='select:$tname' value='union' />\n";
	print "</td>\n";
	print "</tr>\n";
    }

# cyano metadata
	print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	    "<font color='darkblue'>" . "Cyano Metadata" . "</font></th>\n"; 
        # print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n"; 
        print "</tr>\n"; 

    my $tname = 'project_info_cyano_metadata';
    my $def_aux = def_Class($tname);
    @attrs = @{$def_aux->{attrs}};
    for my $k ( @attrs ) { 
	my $attr_name = $k->{name}; 
	if ( $attr_name eq 'modified_by' ||
	     $attr_name eq 'mod_date' ) {
	    # these attribute values are system controlled
	    next;
	}
	
	if ( $attr_name eq 'project_oid' ) {
	    # already printed from project_info table
	    next;
	}
	
	my $attr_val = ""; 
	my $m_attr_val = "";
	
	if ( defined $proj_val{$attr_name} ) {
	    $attr_val = $proj_val{$attr_name};
	}
	if ( defined $m_proj_val{$attr_name} ) { 
	    $m_attr_val = $m_proj_val{$attr_name}; 
	} 
	
	if ( blankStr($attr_val) && blankStr($m_attr_val) ) {
	    next; 
	} 
	
	my $disp_name = $k->{display_name}; 
	
	print "<tr class='img' >\n"; 
	if ( $k->{font_color} ) {
	    print "  <th class='subhead' align='right'>" .
		"<font color='" . $k->{font_color} .
		"'>" . nbsp(5) . $disp_name . "</font></th>\n";
	} 
	else {
	    print "  <th class='subhead' align='right'>" . $disp_name;
	}
	print "</th>\n"; 
	
	# display select button
	print "<td class='img'>\n";
	print "<input type='radio' name='select:$attr_name' " .
	    "value='$project_oid' checked />\n";
	print "</td>\n";
	
	# display attribute value
	if ( ! blankStr($attr_val) ) {
	    print "  <td class='img'   align='left'>" . 
		escapeHTML($attr_val) . "</td>\n"; 
	    
	}
	else {
	    print "  <td class='img'   align='left'></td>\n";
	}
	
	# display select button
	print "<td class='img'>\n";
	print "<input type='radio' name='select:$attr_name' " .
	    "value='$merged_project' />\n";
	print "</td>\n";
	if ( ! blankStr($m_attr_val) ) {
	    print "  <td class='img'   align='left'>" . 
		escapeHTML($m_attr_val) . "</td>\n"; 
	}
	else {
	    print "  <td class='img'   align='left'></td>\n";
	}
    
    print "<td class='img'>\n";
    # print "<input type='radio' name='select:$attr_name' value='union' />\n";
    
    print "</td>\n";
    
    print "</tr>\n"; 

    }    
    # samples?
    my $sample_cnt = db_getValue("select count(*) from env_sample where project_info = $project_oid");
    my $m_sample_cnt = db_getValue("select count(*) from env_sample where project_info = $merged_project");
    if ( $sample_cnt || $m_sample_cnt ) {
	# show samples
	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right'>" .
	    "Samples" . "</th>\n"; 

	print "<td class='img'>\n";
	print "<input type='radio' name='select:Samples' value='$project_oid'  checked />\n";
	print "</td>\n";

	print "  <td class='img'   align='left'>";

	if ( $sample_cnt ) {
	    my $sql2 = "select sample_oid, sample_display_name " .
		"from env_sample where project_info = $project_oid";
	    my $dbh2 = Connect_IMG();
webLog("$sql2\n");	    
	    my $cur2 = $dbh2->prepare($sql2);
	    $cur2->execute(); 
 
	    for (my $j = 0;$j <= 100000; $j++) { 
		my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
		last if !$s_oid;

		my $sample_link = getSampleLink($s_oid);
		print $sample_link . " - ";
		print escapeHTML($s_name);
		print "<br/>\n";
	    }
	    $cur2->finish();
	    $dbh2->disconnect();
	}
	print "  </td>\n";

	print "<td class='img'>\n";
	print "<input type='radio' name='select:Samples' value='$merged_project' />\n";
	print "</td>\n";

	print "  <td class='img'   align='left'>";

	if ( $m_sample_cnt ) {
	    my $sql2 = "select sample_oid, sample_display_name " .
		"from env_sample where project_info = $merged_project";
	    my $dbh2 = Connect_IMG();
webLog("$sql2\n");	    
	    my $cur2 = $dbh2->prepare($sql2);
	    $cur2->execute(); 
 
	    for (my $j = 0;$j <= 100000; $j++) { 
		my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
		last if !$s_oid;

		my $sample_link = getSampleLink($s_oid);
		print $sample_link . " - ";
		print escapeHTML($s_name);
		print "<br/>\n";
	    }
	    $cur2->finish();
	    $dbh2->disconnect();
	}
	print "  </td>\n";

	print "<td class='img'>\n";
	print "<input type='radio' name='select:Samples' value='union' />\n";
	print "</td>\n";

	print "</tr>\n";
    }
	
    print "</table>\n"; 

    print "<p>\n";
    print '<input type="submit" name="_section_ProjectInfo:dbMergeProjects" value="Merge" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_ProjectInfo:showProjects" value="Cancel" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}


#############################################################################
# dbMergeProjects
#############################################################################
sub dbMergeProjects {
    # get user info
    my $contact_oid = getContactOid();

    my $msg = "";

    # get project ids
    my $project_oid = param1('project_oid');
    my $merged_project = param1('merged_project');

    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

    # get Project_Info definition
    my $def_project = def_Project_Info();
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }

    my @attrs = @{$def_project->{attrs}};

    # get new project_oid
    my $tag = "select:" . $def_project->{id};
    my $new_project_oid = param1($tag);
    if ( blankStr($new_project_oid) ) {
#	$msg = "No project ID has been selected for merge.";
#	return $msg;
	$new_project_oid = $project_oid;
    }

    my $can_update_proj = getCanUpdateProject($contact_oid, $project_oid);
    if ( ! $can_update_proj ) {
	$msg = "You are not allowed to merge this project.";
	return $msg;
    }

    my $new_merge_oid = $merged_project;
    if ( $new_merge_oid == $new_project_oid ) {
	$new_merge_oid = $project_oid;
    }

    my %proj_val = SelectProjectInfo($new_project_oid);
    my %m_proj_val = SelectProjectInfo($new_merge_oid);

    my $sql = "update project_info set modified_by = $contact_oid, " .
	"mod_date = sysdate";

    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};
	my $data_type = $k->{data_type};

	$tag = "select:" . $attr_name;
	my $select_id = param1($tag);
	if ( blankStr($select_id) ) {
	    # no selection
	    next;
	}
	if ( $select_id == $new_project_oid ) {
	    # no need to update
	    next;
	}

	if ( defined $m_proj_val{$attr_name} ) { 
	    my $new_attr_val = $m_proj_val{$attr_name};

	    if ( $attr_name eq 'contact_oid' ) {
		# special case for contact_oid
		my $m_contact_oid = db_getValue("select contact_oid from project_info where project_oid = $new_merge_oid");
		$sql .= ", contact_oid = " . $m_contact_oid;
	    }
	    elsif ($data_type eq 'int' || $data_type eq 'number' ) {
		$sql .= ", $attr_name = $new_attr_val";
	    }
	    else {
		$new_attr_val =~ s/'/''/g;  # replace ' with ''
		$sql .= ", $attr_name = '" . $new_attr_val . "'";
	    }
	}
	else {
	    $sql .= ", $attr_name = null";
	}

	if ( $attr_name eq 'ncbi_taxon_id' ) {
	    # need the entire taxon lineage
	    my $ncbi_taxon_id = $m_proj_val{$attr_name};
	    my %lineage = getNCBITaxonInfo($ncbi_taxon_id);
	    for my $aname ( 'ncbi_superkingdom', 'ncbi_phylum',
			    'ncbi_class', 'ncbi_order',
			    'ncbi_family', 'ncbi_genus', 'ncbi_species' ) {
		my ($tag, $rank) = split(/\_/, $aname);
		my $val2 = $lineage{$rank};
		$val2 =~ s/'/''/g;   # replace ' with ''
		$sql .= ", $aname = '" . $val2 . "'";
	    }
	}

	my $disp_name = $k->{display_name};
	my $len = $k->{length};
	my $edit = $k->{can_edit};
    }

    $sql .= " where project_oid = $new_project_oid";

    my @sqlList = ();
    push @sqlList, ( $sql );

    # set-valued attributes
    my @aux_tables = getProjectAuxTables();
    push @aux_tables, ( 'project_info_jgi_url' );
    for my $tname ( @aux_tables ) {
	my $def_aux = def_Class($tname);
	if ( ! $def_aux ) {
	    next;
	}

	$tag = "select:" . $tname;
	my $select_id = param1($tag);
	if ( blankStr($select_id) ) {
	    # no selection
	    next;
	}
	if ( $select_id == $new_project_oid ) {
	    # no need to update
	    next;
	}

	if ( $select_id ne 'union' ) {
	    # deleting existing values
	    $sql = "delete from $tname where " . $def_aux->{id} . " = " .
		$new_project_oid;
	    push @sqlList, ( $sql );
	}

	# upset project_oid of these attribute values to new_project_oid
	$sql = "update $tname set " . $def_aux->{id} . " = " .
	    $new_project_oid . " where " . $def_aux->{id} . " = " .
	    $new_merge_oid;
	push @sqlList, ( $sql );
    }

# cyano metadata 
    $sql = "update project_info_cyano_metadata set project_oid = $new_project_oid ";
# get cayno metadata definition
    $def_project = def_project_info_cyano_metadata();
    @attrs = @{$def_project->{attrs}};
    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};
	my $data_type = $k->{data_type};

	$tag = "select:" . $attr_name;
	my $select_id = param1($tag);
	if ( blankStr($select_id) ) {
	    # no selection
	    next;
	}
	if ( $select_id == $new_project_oid ) {
	    # no need to update
	    next;
	}

	if ( defined $m_proj_val{$attr_name} ) { 
	    my $new_attr_val = $m_proj_val{$attr_name};

	    if ($data_type eq 'int' || $data_type eq 'number' ) {
		$sql .= ", $attr_name = $new_attr_val";
	    }
	    else {
		$new_attr_val =~ s/'/''/g;  # replace ' with ''
		$sql .= ", $attr_name = '" . $new_attr_val . "'";
	    }
	}
	else {
	    $sql .= ", $attr_name = null";
	}

	my $disp_name = $k->{display_name};
	my $len = $k->{length};
	my $edit = $k->{can_edit};
    }

    $sql .= " where project_oid = $new_project_oid";

    @sqlList = ();
    push @sqlList, ( $sql );

    
    # samples
    $tag = "select:Samples";
    my $select_id = param1($tag);
    if ( blankStr($select_id) ) {
	# no selection
	# no need to do anything
    }
    elsif ( $select_id == $new_project_oid ) {
	    # no need to update
	}
    else {
	if ( $select_id ne 'union' ) {
	    # deleting existing values
	    $sql = "update env_sample set project_info = null " .
		"where project_info = $new_project_oid";
	    push @sqlList, ( $sql );
	}

	# add new samples
	$sql = "update env_sample set project_info = $new_project_oid " .
	    "where project_info = $new_merge_oid";
	push @sqlList, ( $sql );
    }

    # project_info in submission
    $sql = "update submission set project_info = " .
	$new_project_oid . " where project_info = " .
	$new_merge_oid;
    push @sqlList, ( $sql );

    # delete the merged project
    for my $tname ( @aux_tables ) {
	my $def_aux = def_Class($tname);
	if ( ! $def_aux ) {
	    next;
	}
	$sql = "delete from $tname where " . $def_aux->{id} .
	    " = " . $new_merge_oid;
	push @sqlList, ( $sql );
    }

    $sql = "update env_sample set project_info = $new_project_oid " .
	"where project_info = $new_merge_oid";
    push @sqlList, ( $sql );

    $sql = "delete from project_info where project_oid = $new_merge_oid";
    push @sqlList, ( $sql );

    db_sqlTrans(\@sqlList);

    return $msg;
}


########################################################################## 
# ShowPrintableData
########################################################################## 
sub ShowPrintableData {
    my ($project_oid) = @_;

    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $project_oid ) {
	return;
    }

    # check whether user can view this project
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'No' ) {
	# contact must be the owner of this project
	my $project_contact = db_getValue("select contact_oid from project_info where project_oid = $project_oid");
	my $project_gold_stamp_id = db_getValue("select gold_stamp_id from project_info where project_oid = $project_oid");
	if ( $project_contact == $contact_oid ||
	     ! blankStr($project_gold_stamp_id) ) {
	    # fine
	}
	else {
	    printError ( "You cannot view this project.");
	    return; 
	}
    }

    my $proj_name = db_getValue("select display_name from project_info where project_oid = $project_oid");
#    my $proj_type = db_getValue("select project_type from project_info where project_oid = $project_oid");
    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

    # get Project_Info definition
    my $def_project = def_Project_Info();
#    if ( $proj_type eq 'Metagenome' ) {
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }

    # get data from database
    my %db_val = SelectProjectInfo($project_oid);
    my $sql = "";
    my $val = "";

    print "<table border='0'>\n";
    print "<tr>\n";
    print "<td>Organism Display Name</td>";
    print "<td>" . escapeHTML($proj_name) . "</td></tr>\n";

    my @genbank_sub_flds = ( 'culture_collection', 'gold_stamp_id',
			     'greengenes_id', 'funding_program',
			     'no_of_reads', 'assembly_method',
			     'seq_depth', 'gene_calling_method',
			     'isolation', 'iso_source',
			     'iso_comments', 'iso_year',
			     'iso_country', 'latitude',
			     'longitude', 'altitude',
			     'depth', 'host_name',
			     'host_gender', 'host_age',
			     'host_health', 'body_sample_site',
			     'body_sample_subsite', 'oxygen_req',
			     'cell_shape', 'motility',
			     'sporulation', 'temp_range',
			     'temp_optimum', 'salinity',
			     'ph', 'gram_stain',
			     'biotic_rel', 'symbiotic_rel' );

    for my $fld1 ( @genbank_sub_flds ) {
	if ( blankStr($db_val{$fld1}) ) {
	    next;
	}

	my $aname = $fld1;
	my $attr1 = $def_project->findAttr($fld1);
	if ( $attr1 && ! blankStr($attr1->{display_name}) ) {
	    $aname = $attr1->{display_name};
	}

	# output value
	print "<tr>\n";
	print "<td>$aname</td>\n";
	if ( $fld1 eq 'temp_optimum' && length($db_val{$fld1}) > 0 ) {
	    print "<td>" . escapeHTML($db_val{$fld1}) . "C</td>";
	}
	else {
	    print "<td>" . escapeHTML($db_val{$fld1}) . "</td>";
	}

	if ( $fld1 eq 'gold_stamp_id' ) {
	    # GOLD ID
	    my $gold_id = $db_val{$fld1};
	    print "<td>" . escapeHTML("http://genomesonline.org/cgi-bin/GOLD/bin/GOLDCards.cgi?goldstamp=$gold_id") . "</td>";
	}

	print "</tr>\n";
    }  # end for

    # sequencing platforms
#    $sql = "select seq_method from project_info_seq_method where project_oid = $project_oid";
#    $val = db_getValuesToString($sql);
#    print "<tr>\n";
#    print "<td>Sequencing Platforms</td>\n";
#    print "<td>" . escapeHTML($val) . "</td></tr>\n";

    # diseases
    $sql = "select diseases from project_info_diseases where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Diseases</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    # habitat
    $sql = "select habitat from project_info_habitat where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Habitat</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    # phenotype
    $sql = "select phenotypes from project_info_phenotypes where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Phenotypes</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    # energy source
    $sql = "select energy_source from project_info_energy_source where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Energy Source</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    print "</table>\n";
    return;

    my @aux_tables = getProjectAuxTables ();
    for my $tname ( @aux_tables ) {
	if ( $tname eq 'project_info_data_links' ) {
	    next;
	}

	DisplayProjectSetAttr($project_oid, $tname, ', ', ', ', 0);
    }

    # samples?
    my $sample_cnt = db_getValue("select count(*) from env_sample where project_info = $project_oid");
    if ( $sample_cnt ) {
	# show samples
	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right'>" .
	    "Samples" . "</th>\n"; 
	print "  <td class='img'   align='left'>";

	my $sql2 = "select sample_oid, sample_display_name " .
	    "from env_sample where project_info = $project_oid";
	my $dbh2 = Connect_IMG();
webLog("$sql2\n");	
	my $cur2 = $dbh2->prepare($sql2);
	$cur2->execute(); 
 
	for (my $j = 0;$j <= 100000; $j++) { 
	    my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
	    last if !$s_oid;

	    my $sample_link = getSampleLink($s_oid);
	    print $sample_link . " - ";
	    print escapeHTML($s_name);
	    print "<br/>\n";
	}
	$cur2->finish();
	$dbh2->disconnect();

	print "  </td>\n";

	print "</tr>\n";
    }

    # submissions?
#    my @subs = db_getValues("select submission_id from submission where project_info = $project_oid");
#    if ( scalar(@subs) > 0 ) {
	# show submissions
#        print "<tr class='img' >\n"; 
#        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
#	    "<font color='darkblue'>" . "ER Submission Info" . "</font></th>\n"; 
#        print "  <td class='img'   align='left' bgcolor='lightblue'>" .
#	    "</td>\n"; 
#        print "</tr>\n"; 

#	print "<tr class='img' >\n"; 
#	print "  <th class='subhead' align='right'>" .
#	    "Submissions" . "</th>\n"; 

#	my $sub_str = "";
#	for my $s1 ( @subs ) {
#	    my $submit_link = getSubmissionLink($s1);
#	    $sub_str .= $submit_link . " ";
#	}

#	print "  <td class='img'   align='left'>";
#	print $sub_str;
#	print "  </td></tr>\n";
#    }
	
    print "</table>\n"; 

    print "<p>\n"; 
 
#    print '<input type="submit" name="_section_ProjectInfo:printableData" value="Show Metadata in Printable Format" class="lgbutton" />';

    printHomeLink();

    print end_form();
}


########################################################################## 
# ShowHmpPrintableData
########################################################################## 
sub ShowHmpPrintableData {
    my ($project_oid) = @_;

    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $project_oid ) {
	printError ( "No project has been selected.");
	return;
    }

    # check whether user can view this project
    if ( ! getIsHmpUser($contact_oid) ) {
	printError ( "You cannot view this project.");
	return; 
    }

    my $proj_name = db_getValue("select display_name from project_info where project_oid = $project_oid");
#    my $proj_type = db_getValue("select project_type from project_info where project_oid = $project_oid");
    my $proj_domain = db_getValue("select domain from project_info where project_oid = $project_oid");

    # get Project_Info definition
    my $def_project = def_Project_Info();
#    if ( $proj_type eq 'Metagenome' ) {
    if ( $proj_domain eq 'MICROBIAL' ) {
	$def_project = def_meta_Project_Info();
    }

    # get data from database
    my %db_val = SelectProjectInfo($project_oid);
    my $sql = "";
    my $val = "";

    print "<table border='0'>\n";
    print "<tr>\n";
    print "<td>Organism Display Name</td>";
    print "<td>" . escapeHTML($proj_name) . "</td></tr>\n";

    my @genbank_sub_flds = ( 'hmp_id',
			     'greengenes_id',
			     'no_of_reads', 'assembly_method',
			     'seq_depth', 'gene_calling_method',
			     'isolation', 'iso_source',
			     'iso_comments', 'iso_year',
			     'host_name',
			     'host_gender', 'host_age',
			     'host_health', 'body_sample_site',
			     'body_sample_subsite', 'oxygen_req',
			     'cell_shape', 'motility',
			     'sporulation', 'temp_range',
			     'temp_optimum', 'gram_stain' );

    for my $fld1 ( @genbank_sub_flds ) {
	if ( blankStr($db_val{$fld1}) ) {
	    next;
	}

	my $aname = $fld1;
	my $attr1 = $def_project->findAttr($fld1);
	if ( $attr1 && ! blankStr($attr1->{display_name}) ) {
	    $aname = $attr1->{display_name};
	}

	# output value
	print "<tr>\n";
	print "<td>$aname</td>\n";
	print "<td>" . escapeHTML($db_val{$fld1}) . "</td>";

	if ( $fld1 eq 'gold_stamp_id' ) {
	    # GOLD ID
	    my $gold_id = $db_val{$fld1};
	    print "<td>" . escapeHTML("http://genomesonline.org/cgi-bin/GOLD/bin/GOLDCards.cgi?goldstamp=$gold_id") . "</td>";
	}

	print "</tr>\n";
    }  # end for

    # sequencing technology
    $sql = "select seq_method from project_info_seq_method where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Sequencing Technology</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    # diseases
    $sql = "select diseases from project_info_diseases where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Diseases</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    # habitat
    $sql = "select habitat from project_info_habitat where project_oid = $project_oid";
    $val = db_getValuesToString($sql);
    if ( ! blankStr($val) ) {
	print "<tr>\n";
	print "<td>Habitat</td>\n";
	print "<td>" . escapeHTML($val) . "</td></tr>\n";
    }

    print "</table>\n";
    return;

    my @aux_tables = getProjectAuxTables ();
    for my $tname ( @aux_tables ) {
	if ( $tname eq 'project_info_data_links' ) {
	    next;
	}

	DisplayProjectSetAttr($project_oid, $tname, ', ', ', ', 0);
    }

    # samples?
    my $sample_cnt = db_getValue("select count(*) from env_sample where project_info = $project_oid");
    if ( $sample_cnt ) {
	# show samples
	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right'>" .
	    "Samples" . "</th>\n"; 
	print "  <td class='img'   align='left'>";

	my $sql2 = "select sample_oid, sample_display_name " .
	    "from env_sample where project_info = $project_oid";
	my $dbh2 = Connect_IMG();
webLog("$sql2\n");	
	my $cur2 = $dbh2->prepare($sql2);
	$cur2->execute(); 
 
	for (my $j = 0;$j <= 100000; $j++) { 
	    my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
	    last if !$s_oid;

	    my $sample_link = getSampleLink($s_oid);
	    print $sample_link . " - ";
	    print escapeHTML($s_name);
	    print "<br/>\n";
	}
	$cur2->finish();
	$dbh2->disconnect();

	print "  </td>\n";

	print "</tr>\n";
    }

    # submissions?
#    my @subs = db_getValues("select submission_id from submission where project_info = $project_oid");
#    if ( scalar(@subs) > 0 ) {
	# show submissions
#        print "<tr class='img' >\n"; 
#        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
#	    "<font color='darkblue'>" . "ER Submission Info" . "</font></th>\n"; 
#        print "  <td class='img'   align='left' bgcolor='lightblue'>" .
#	    "</td>\n"; 
#        print "</tr>\n"; 

#	print "<tr class='img' >\n"; 
#	print "  <th class='subhead' align='right'>" .
#	    "Submissions" . "</th>\n"; 

#	my $sub_str = "";
#	for my $s1 ( @subs ) {
#	    my $submit_link = getSubmissionLink($s1);
#	    $sub_str .= $submit_link . " ";
#	}

#	print "  <td class='img'   align='left'>";
#	print $sub_str;
#	print "  </td></tr>\n";
#    }
	
    print "</table>\n"; 

    print "<p>\n"; 
 
    print '<input type="submit" name="_section_ProjectInfo:printableData" value="Show Metadata in Printable Format" class="lgbutton" />';

    printHomeLink();

    print end_form();
}


########################################################################## 
# AnalysisProject
########################################################################## 
sub AnalysisProject {
    my ($project_oid) = @_;

    print start_form(-name=>'analysisProject',-method=>'post',action=>"$section_cgi"); 

    if ( ! $project_oid ) {
	return;
    }

    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 
    my $role = "";
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
        $role = "superuser";
    }
    else {
	# check permission
	$role = WebFunctions::getGoldAPRole($contact_oid, $project_oid);
	if ( ! $role || blankStr($role) ) {
	    print "<h5>Cannot find the analysis project.</h5>\n";
	    return;
	}
    }

    my $def_project = def_Analysis_Project();

    my $decode;
    my $res = "";
    my %data_h;
    my $has_data = getAPfromImgGold($project_oid, \%data_h);

    if ( ! $has_data ) {
	$res = getGoldAnalysisProject($project_oid);

	if ( ! $res ) {
	    print "<h5>Cannot find the analysis project.</h5>\n";
	    return;
	}

	$decode = eval { decode_json($res); };
	if ( $@ ) {
	    print "<p>decode_json failed. error: $@\n";
	    print "<p>(Input: $res)\n";
	    return;
	}
    }
    else {
	$decode = \%data_h;
    }

    my @keys = (keys %$decode);

    my $debug = 0;
    if ( $debug && $contact_oid == 312 ) {
	for my $key ( @keys ) {
	    my $val = $decode->{$key};
	    print "<p>*** $key: $val\n";

            if ( $key eq 'projects' ) {
                my @arr = @$val; 
                for my $a1 ( @arr ) {
                    my %h = %$a1; 
                    for my $k2 (keys %h) {
                        print "<p>***** $k2 => " . $h{$k2} . "\n";
                    } 
                } 
            } 
	}
    }

    print "<h1>GOLD Analysis Project ID: $project_oid</h1>\n";
    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 

    my $curr_tab = "None";
    my $show_contact = 0;
    my $bg_color = 'lightblue';
    my @attrs = @{$def_project->{attrs}};
    for my $attr1 ( @attrs ) {
	my $key = $attr1->{name};

	if ( $key eq 'goldId' ) {
	    next;
	}
	if ( $key eq 'projects' ) {
	    next;
	}
	my $aname = $attr1->{display_name};
	my $val = $decode->{$key};
	if ( ! $val ) {
	    next;
	}
	if ( $key eq 'submitterContactOid' &&
	     isInt($val) ) {
	    $val = db_getValue("select name from contact where contact_oid = $val");
	}
	if ( $key eq 'referenceGoldId' &&
	     $val =~ /Ga/ ) {
	    my $gold_ap_id = $val;
	    $val = "<a href='" . $main_cgi .
		"?section=ProjectInfo&page=analysisProject" . 
		"&analysis_project_id=$gold_ap_id' >" .
		$gold_ap_id . "</a>";
	}
	my $tab = $attr1->{tab};
	if ( $tab ne $curr_tab ) {
	    if ( $curr_tab eq 'Contact Info' ) {
		# show users
		$show_contact = 1;
		my $key = 'users';
		my $val = $decode->{$key};
		my @arr = @$val;

		if ( scalar(@arr) > 0 ) {
		    print "<tr class='img' >\n"; 
		    print "  <td class='img' align='left' bgcolor='$bg_color'>" .
			"Users" . "</td>\n"; 
		    print "  <td class='img' align='left'>";
		    for my $a3 ( @arr ) {
			my $role = $a3->{'role'};
			if ( $role ) {
			    print "<b>" . $role . ":</b> ";
			}
			print $a3->{'name'};
			print " (" . $a3->{'email'} . ")";
			if ( $isAdmin eq 'Yes' && $a3->{'calibanId'} ) {
			    print " (Caliban ID: " . $a3->{'calibanId'} . ")";
			}
			print "<br/>\n";
		    }
		    print "</td></tr>\n"; 
		}
	    }

	    $bg_color = $attr1->{bg_color};
	    print "<tr class='img' >\n"; 
	    print "  <th class='subhead' align='right' bgcolor='$bg_color'>" .
		"<b>" . $tab . "</b></th>\n"; 
	    print "  <td class='img'   align='left' bgcolor='$bg_color'>" .
		"</td>\n"; 
	    print "</tr>\n"; 
	    $curr_tab = $tab;
	}

	print "<tr class='img' >\n"; 
	print "<td class='img' bgcolor='$bg_color'>" . 
	    $aname . "</td>\n";
	print "<td class='img'>" . $val . "</td>\n";
	print "</tr>\n";
    }   # end for attr1

    if ( ! $show_contact ) {
	$show_contact = 1;
	my $bg_color = 'yellow';
	my $tab = "Contact Info";
	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right' bgcolor='$bg_color'>" .
	    "<b>" . $tab . "</b></th>\n"; 
	print "  <td class='img'   align='left' bgcolor='$bg_color'>" .
	    "</td>\n"; 
	print "</tr>\n"; 
	$curr_tab = $tab;

	my $key = 'users';
	my $val = $decode->{$key};
	my @arr = ();
	if ( $val ) {
	    @arr = @$val;
	}

	if ( scalar(@arr) > 0 ) {
	    print "<tr class='img' >\n"; 
	    print "  <td class='img' align='left' bgcolor='$bg_color'>" .
		"Users" . "</td>\n"; 
	    print "  <td class='img' align='left'>";
	    for my $a3 ( @arr ) {
		my $role = $a3->{'role'};
		if ( $role ) {
		    print "<b>" . $role . ":</b> ";
		}
		print $a3->{'name'};
		print " (" . $a3->{'email'} . ")";
		if ( $isAdmin eq 'Yes' && $a3->{'calibanId'} ) {
		    print " (Caliban ID: " . $a3->{'calibanId'} . ")";
		}
		print "<br/>\n";
	    }
	    print "</td></tr>\n"; 
	}
    }

    ## genbank
    my $key = 'genbanks';
    my $val = $decode->{$key};
    my @arr = ();
    if ( $val ) {
	@arr = @$val;
    }

    if ( scalar(@arr) > 0 ) {
	print "<td class='img' bgcolor='lightgreen'>" . 
	    "Genbank Information" . "</td>\n";
	print "<td class='img'>\n";
	for my $a1 ( @arr ) {
	    my $pr_id2 = 0;
	    my $sam_id2 = 0;
	    if ( $a1->{'genbankId'} ) {
		print "Genbank ID: " . $a1->{'genbankId'} . "; ";
	    }
	    if ( $a1->{'assemblyAccession'} ) {
		print "Assembly Accession: " . 
		    $a1->{'assemblyAccession'} . ".";
	    }
	    print "<br/>\n";
	}
	print "</td>\n";
	print "</tr>\n";
    }

    ## projects and samples
    $key = 'projects';
    $val = $decode->{$key};

    print "<td class='img' bgcolor='lightblue'>" . 
	"Projects and Samples" . "</td>\n";
    print "<td class='img'>\n";
    @arr = ();
    if ( $val ) {
	@arr = @$val;
    }
    for my $a1 ( @arr ) {
	my $pr_id2 = 0;
	my $sam_id2 = 0;
	if ( $a1->{'goldId'} ) {
	    my $gold_gp_id = $a1->{'goldId'};
	    my $link = "<a href='" . $main_cgi .
		"?section=ProjectInfo&page=sequencingProject" .
		"&sequencing_project_id=$gold_gp_id' >" .
		$gold_gp_id . "</a>";
	    print "GOLD ID " . $link . ". ";
	}
#	if ( $a1->{'projectOid'} ) {
#	    $pr_id2 = $a1->{'projectOid'};
#	    my $proj_link = getProjectLink($a1->{'projectOid'});
#	    print "Project " . $proj_link;
#	    if ( isInt($a1->{'projectOid'}) ) {
#		my $proj_name = db_getValue("select display_name from project_info where project_oid = " . $a1->{'projectOid'});
#		print " " . $proj_name;
#	    }
#	    print "\n";
#	}
#	if ( $a1->{'sampleOid'} ) {
#	    $sam_id2 = $a1->{'sampleOid'};
#	    my $sample_link = getSampleLink($a1->{'sampleOid'});
#	    print "Sample " . $sample_link;
#	    if ( isInt($a1->{'sampleOid'}) ) {
#		my $sample_name = db_getValue("select sample_display_name from env_sample where sample_oid = " . $a1->{'sampleOid'});
#		print " " . $sample_name;
#	    }
#	    print "\n";
#	}
	if ( $a1->{'itsSpid'} ) {
	    print " (ITS SPID: " . $a1->{'itsSpid'} . ")";
	}
	if ( $a1->{'pmoProjectId'} ) {
	    print " (PMO Project ID: " . $a1->{'pmoProjectId'} . ")";
	}

#	if ( ! $pr_id2 && ! $sam_id2 ) {
#	    ($pr_id2, $sam_id2) = getErId($project_oid, $a1->{'goldId'});
#	    if ( $pr_id2 ) {
#		print " (<b>NEW</b> ER Project ID: " .	
#		    getProjectLink($pr_id2) .  ")";
#	    }
#	    elsif ( $sam_id2 ) {
#		print " (<b>NEW</b> ER Sample ID: " .
#		    getSampleLink($sam_id2) . ")";
#	    }
#	}
	print "<br/>\n";
    }
    print "</td>\n";
    print "</tr>\n";

    print "</table>\n";

    if ( ! $res && $contact_oid == 312 ) {
	$res = getGoldAnalysisProject($project_oid);
    }

    if ( $isAdmin eq 'Yes' && $res ) {
	print "<h5>Raw Result from API Call (for Admin users only)</h5>\n";
	print "<p><pre>";
	my @str = split(/\,/, $res);
	my $cnt2 = 0;
	for my $s2 ( @str ) {
	    if ( $cnt2 ) {
		print ",\n";
	    }
	    print $s2;
	    $cnt2++;
	}
	print "</pre>\n";
    }

    print "<p>\n"; 

    printHomeLink();

    print end_form();
}


sub getAPfromImgGold {
    my ($gold_ap_id, $data) = @_;

    my $has_data = 0;
    $data->{goldId} = $gold_ap_id;

    my $sql2 = "select gold_id, gold_proposal_name, analysis_project_name, gold_analysis_project_type, submission_type, genome_type, analysis_product_name, comments, visibility, review_status, its_analysis_project_id, its_annotation_at_id, its_assembly_at_id, pi_name, pi_email, ncbi_tax_id, domain, ncbi_phylum, ncbi_class, ncbi_order, ncbi_family, ncbi_genus, ncbi_species, ecosystem, ecosystem_category, ecosystem_type, ecosystem_subtype, specific_ecosystem, img_dataset_id, pipeline_version, reference_gold_id from gold_analysis_project where gold_id = ?";

    my $dbh2 = Connect_IMG();
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_ap_id); 
 
    my ( $id2, @res ) = $cur2->fetchrow_array(); 
    if ( $id2 ) {
	$has_data = 1;
##	$data->{goldId} = $id2;
	$data->{goldProposalName} = $res[0];
	$data->{analysisProjectName} = $res[1];
	$data->{goldAnalysisProjectType} = $res[2];
	$data->{submissionType} = $res[3];
	$data->{genomeType} = $res[4];
	$data->{analysisProductName} = $res[5];
	$data->{comments} = $res[6];
	$data->{visibility} = $res[7];
	$data->{reviewStatus} = $res[8];
	$data->{itsAnalysisProjectId} = $res[9];
	$data->{itsAnnotationAtId} = $res[10];
	$data->{itsAssemblyAtId} = $res[11];
	$data->{piName} = $res[12];
	$data->{piEmail} = $res[13];
	$data->{ncbiTaxonId} = $res[14];
	$data->{domain} = $res[15];
	$data->{ncbiPhylum} = $res[16];
	$data->{ncbiClass} = $res[17];
	$data->{ncbiOrder} = $res[18];
	$data->{ncbiFamily} = $res[19];
	$data->{ncbiGenus} = $res[20];
	$data->{ncbiSpecies} = $res[21];
	$data->{ecosystem} = $res[22];
	$data->{ecosystemCategory} = $res[23];
	$data->{ecosystemType} = $res[24];
	$data->{ecosystemSubtype} = $res[25];
	$data->{specificEcosystem} = $res[26];
	$data->{imgDatasetId} = $res[27];
	$data->{pipelineVersion} = $res[28];
	$data->{referenceGoldId} = $res[29];
    }

    $cur2->finish();

    ## users
    $sql2 = "select gold_id, name, email, role, caliban_id from gold_analysis_project_users where gold_id = ? ";
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_ap_id); 

    for (;;) {
	my ( $id2, $name, $email, $role, $caliban_id ) = $cur2->fetchrow_array(); 
	last if !$id2;

	my %h2;
	$h2{name} = $name;
	$h2{email} = $email;
	$h2{role} = $role;
	$h2{calibanId} = $caliban_id;
	if ( $data->{users} ) {
	    push @{$data->{users}}, ( \%h2 );
	}
	else {
	    my @arr = ( \%h2 );
	    $data->{users} = \@arr;
	}
    }
    $cur2->finish();

    ## genbank
    $sql2 = "select gold_id, genbank_id, assembly_accession from gold_analysis_project_genbanks where gold_id = ? ";
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_ap_id); 

    for (;;) {
	my ( $id2, $genbank_id, $assembly_accession ) = $cur2->fetchrow_array(); 
	last if !$id2;

	my %h2;
	$h2{genbankId} = $genbank_id;
	$h2{assemblyAccession} = $assembly_accession;
	if ( $data->{genbanks} ) {
	    push @{$data->{genbanks}}, ( \%h2 );
	}
	else {
	    my @arr = ( \%h2 );
	    $data->{genbanks} = \@arr;
	}
    }
    $cur2->finish();

    ## projects
    $sql2 = "select gold_id, its_spid, pmo_project_id, sp_gold_id from gold_analysis_project_lookup2 where gold_id = ? ";
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_ap_id); 

    for (;;) {
	my ( $id2, $its_spid, $pmo_project_id, $sp_gold_id) = $cur2->fetchrow_array(); 
	last if !$id2;

	my %h2;
	$h2{itsSpid} = $its_spid;
	$h2{pmoProjectId} = $pmo_project_id;
	$h2{goldId} = $sp_gold_id;
	if ( $data->{projects} ) {
	    push @{$data->{projects}}, ( \%h2 );
	}
	else {
	    my @arr = ( \%h2 );
	    $data->{projects} = \@arr;
	}
    }
    $cur2->finish();

    $dbh2->disconnect();

    return $has_data;
}


sub getErId {
    my ($gold_ap_id, $gold_sp_id) = @_;

    my $project_oid = 0;
    my $sample_oid = 0;

    if ( ! $gold_ap_id || ! $gold_sp_id ) {
	return ($project_oid, $sample_oid);
    }

    my $dbh = Connect_IMG(); 
    my $sql = "select count(*) from gold_analysis_project_lookup2 " .
	"where gold_id = ? and sp_gold_id = ?";
    my $cur = $dbh->prepare($sql);
    $cur->execute($gold_ap_id, $gold_sp_id); 
    my ($cnt1) = $cur->fetchrow();
    $cur->finish();
    if ( ! $cnt1 ) {
	$dbh->disconnect();
	return ($project_oid, $sample_oid);
    }

    $sql = "select project_oid from project_info where gold_stamp_id = ?";
    $cur = $dbh->prepare($sql);
    $cur->execute($gold_sp_id); 
    ($project_oid) = $cur->fetchrow();
    $cur->finish();
    if ( $project_oid ) {
	$dbh->disconnect();
	return ($project_oid, $sample_oid);
    }

    $sql = "select sample_oid from env_sample where gold_id = ?";
    my $cur = $dbh->prepare($sql);
    $cur->execute($gold_sp_id); 
    ($sample_oid) = $cur->fetchrow();
    $cur->finish();
    $dbh->disconnect();
    return ($project_oid, $sample_oid);
}


########################################################################## 
# SequencingProject
########################################################################## 
sub SequencingProject {
    my ($project_oid) = @_;

    print start_form(-name=>'sequencingProject',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $project_oid ) {
	return;
    }

    my $def_project = def_Sequencing_Project();

    my $decode;
    my $res = "";
    my %data_h;
    my $has_data = getSPfromImgGold($project_oid, \%data_h);

    if ( ! $has_data ) {
	$res = getNewGoldMetadata($project_oid); 

	$decode = eval { decode_json($res); };
	if ( $@ ) {
	    print "<p>decode_json failed. error: $@\n";
	    print "<p>(Input: $res)\n";
	    return;
	}
    }
    else {
	$decode = \%data_h;
    }

    my @keys = (keys %$decode);

    # check permission
    my $canAccess = 0;
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
	$canAccess = 1;
    }
    elsif ( $decode->{'visibility'} eq 'public' ) {
	$canAccess = 1;
    }
    elsif ( $decode->{'submitterContactOid'} == $contact_oid ) {
	$canAccess = 1;
    }
    elsif ( $decode->{'piEmail'} ) {
	my $email = db_getValue("select email from contact where contact_oid = $contact_oid");
	if ( lc($email) eq lc ($decode->{'piEmail'}) ) {
	    $canAccess = 1;
	}
    }


    if(! $canAccess){
        # user may have Ga access if so they should Gp access
        
        # get all their Ga
        # --- $project_oid
        # 
        my $sql = qq{
select gold_id, sp_gold_id from gold_analysis_project_lookup2 
where  sp_gold_id = ? and gold_id in (
select analysis_project_id 
from submission where img_taxon_oid in (            
select taxon_permissions from contact_taxon_permissions where contact_oid = $contact_oid)
) 
        };
        
        my $dbh = Connect_IMG_Contact();

    webLog("$sql\n");
        my $cur=$dbh->prepare($sql);
        $cur->execute($project_oid);
        my($g, $s) = $cur->fetchrow_array();
        $cur->finish();
        $dbh->disconnect();        
        
        if($g) {
           $canAccess = 1; 
        }
    }


    if ( ! $canAccess ) {
	print "<h5>Cannot find the project</h5>\n";
	return;
    }

    my $debug = 0;
    if ( $debug && $contact_oid == 312 ) {
	for my $key ( @keys ) {
	    my $val = $decode->{$key};
	    print "<p>*** $key: $val\n";

            if ( $key eq 'projects' ) {
                my @arr = @$val; 
                for my $a1 ( @arr ) {
                    my %h = %$a1; 
                    for my $k2 (keys %h) {
                        print "<p>***** $k2 => " . $h{$k2} . "\n";
                    } 
                } 
            } 
	}
    }

    print "<h1>GOLD Sequencing Project ID: $project_oid</h1>\n";
    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 

    my $curr_tab = "None";
    my $bg_color = 'lightblue';
    my @attrs = @{$def_project->{attrs}};
    for my $attr1 ( @attrs ) {
	my $key = $attr1->{name};
	if ( $key eq 'goldId' ) {
	    next;
	}
	if ( $key eq 'projects' ) {
	    next;
	}
	my $aname = $attr1->{display_name};
	my $val = $decode->{$key};

	if ( ! $val ) {
	    next;
	}

	if ( $key eq 'submitterContactOid' &&
	     isInt($val) ) {
	    $val = db_getValue("select name from contact where contact_oid = $val");
	}
	if ( $key eq 'referenceGoldId' &&
	     $val =~ /Ga/ ) {
	    my $gold_ap_id = $val;
	    $val = "<a href='" . $main_cgi .
		"?section=ProjectInfo&page=analysisProject" . 
		"&analysis_project_id=$gold_ap_id' >" .
		$gold_ap_id . "</a>";
	}

	my $tab = $attr1->{tab};
	if ( $tab ne $curr_tab ) {
	    $bg_color = $attr1->{bg_color};
	    print "<tr class='img' >\n"; 
	    print "  <th class='subhead' align='right' bgcolor='$bg_color'>" .
		"<b>" . $tab . "</b></th>\n"; 
	    print "  <td class='img'   align='left' bgcolor='$bg_color'>" .
		"</td>\n"; 
	    print "</tr>\n"; 
	    $curr_tab = $tab;
	}

	print "<tr class='img' >\n"; 
	print "<td class='img' bgcolor='$bg_color'>" . 
	    $aname . "</td>\n";
	print "<td class='img'>" . $val . "</td>\n";
	print "</tr>\n";
    }   # end for attr1

    for my $key3 ( 'sequencingCenters', 'seqMethod', 'collaborators',
		   'cellArrangement', 'diseases', 'energySource', 
		   'habitat', 'metabolism', 'phenotypes',
		   'projectRelevance' ) {
	my @arr3 = ();
	my $val3 = $decode->{$key3};

	if ( $val3 ) {
	    if ( ref($val3) eq 'ARRAY' ) {
		@arr3 = @$val3;
	    }
	    elsif ( ref($val3) eq 'SCALAR' ) {
		@arr3 = ( $val3 );
	    }
	    elsif ( ! ref($val3) ) {
		@arr3 = ( $val3 );
	    }
	}
	if ( scalar(@arr3) == 0 ) {
	    next;
	}

	my $name3 = $key3;
	if ( $key3 eq 'cellArrangement' ) {
	    $name3 = 'Cell Arrangement';
	}
	elsif ( $key3 eq 'diseases' ) {
	    $name3 = 'Diseases';
	}
	elsif ( $key3 eq 'energySource' ) {
	    $name3 = 'Energy Source';
	}
	elsif ( $key3 eq 'habitat' ) {
	    $name3 = 'Habitat';
	}
	elsif ( $key3 eq 'metabolism' ) {
	    $name3 = 'Metabolism';
	}
	elsif ( $key3 eq 'phenotypes' ) {
	    $name3 = 'Phenotypes';
	}
	elsif ( $key3 eq 'projectRelevance' ) {
	    $name3 = 'Project Relevance';
	}
	elsif ( $key3 eq 'seqMethod' ) {
	    $name3 = 'Sequencing Method';
	}
	elsif ( $key3 eq 'sequencingCenters' ) {
	    $name3 = 'Sequencing Center';
	}
	elsif ( $key3 eq 'collaborators' ) {
	    $name3 = 'Collaborators';
	}
	print "<tr class='img' >\n"; 
	if ( $key3 eq 'cellArrangement' || $key3 eq 'diseases' || 
	     $key3 eq 'energySource' || $key3 eq 'habitat' || 
	     $key3 eq 'metabolism' || $key3 eq 'phenotypes' ||
	     $key3 eq 'projectRelevance' ) {
	    $bg_color = 'lightblue';
	}
	print "<td class='img' bgcolor='$bg_color'>" . 
	    $name3 . "</td>\n";
	print "<td class='img'>\n";
	for my $a3 ( @arr3 ) {
	    if ( $key3 eq 'sequencingCenters' ||
		$key3 eq 'collaborators' ) {
		$a3 = $a3->{'name'};
	    }
	    print $a3 . "<br/>\n";
	}
	print "</td>\n";
	print "</tr>\n";
    }

    for my $pub_type ( 'genomePublications', 'isolationPublications',
		       'otherPublications' ) {
	my $disp_name = $pub_type;
	if ( $pub_type eq 'genomePublications' ) {
	    $disp_name = "Genome Publications";
	}
	elsif ( $pub_type eq 'isolationPublications' ) {
	    $disp_name = "Isolation Publications";
	}
	elsif ( $pub_type eq 'otherPublications' ) {
	    $disp_name = "Other Publications";
	}

	my $val3 = $decode->{$pub_type};

	if ( $val3 ) {
	    my @arr3 = ();
	    if ( ref($val3) eq 'ARRAY' ) {
		@arr3 = @$val3;
	    }

	    if ( scalar(@arr3) > 0 ) {
		print "<tr class='img' >\n"; 
		print "<td class='img' bgcolor='lightblue'>" . 
		    $disp_name . "</td>\n";
		print "<td class='img'>\n";
		print "<table class='img'>\n";

		for my $a3 ( @arr3 ) {
		    for my $fld3 ( 'pubmedId', 'journalName',
				   'volume', 'issue', 'page',
				   'title', 'publicationDate', 'doi' ) {
			my $name3 = ucfirst($fld3);
			if ( $fld3 eq 'pubmedId' ) {
			    $name3 = 'Pubmed ID';
			}
			elsif ( $fld3 eq 'journalName' ) {
			    $name3 = 'Journal Name';
			}
			elsif ( $fld3 eq 'publicationDate' ) {
			    $name3 = 'Publication Date';
			}

			if ( $a3->{$fld3} ) {
			    print "<tr class='img'>\n";
			    print "<td class='img'>$name3</td>\n";
			    print "<td class='img'>" . 
				$a3->{$fld3} . "</td>\n";
			    print "</tr>\n";
			}
		    }
		}

		print "</table>\n";
		print "</td>\n";
		print "</tr>\n";
	    }
	}
    }

    my $key = 'studies';
    my $val = $decode->{$key};
    if ( $val ) {
	# has studies
	print "<tr class='img' >\n"; 
	print "<td class='img' bgcolor='lightblue'>" . 
	    "Study" . "</td>\n";
	print "<td class='img'>\n";
	print "<table class='img'>\n";

	my @arr = @$val;
	for my $a1 ( @arr ) {
	    if ( ! $a1 ) {
		next;
	    }

	    my $decode2;
	    my $res2 = "";
	    my %data2_h;
	    my $has_data2 = getStudyfromImgGold($a1, \%data2_h);

	    if ( ! $has_data2 ) {
		$res2 = getNewGoldMetadata($a1);

		if ( ! $res2 || blankStr($res2) ) {
		    print "Gold ID: " . $a1 . "<br/>\n";
		    next;
		}

		$decode2 = decode_json($res2);
	    }
	    else {
		$decode2 = \%data2_h;
	    }

#	    my @key2 = (keys %$decode2);
	    # specific order
	    my @key2 = ( 'goldId', 'studyName', 
##			 'ownerId', 'ownerEmail',
			 'ecosystem', 'ecosystemCategory',
			 'ecosystemType', 'ecosystemSubtype',
			 'specificEcoSystem' );

	    for my $k2 ( @key2 ) {
		if ( $decode2->{$k2} ) {
		    my $name2 = $k2;
		    my $val2 = $decode2->{$k2};
		    if ( $k2 eq 'goldId' ) {
			$name2 = "Study GOLD ID";
		    }
		    elsif ( $k2 eq 'studyName' ) {
			$name2 = "Study Name";
		    }
		    elsif ( $k2 eq 'ecosystem' ) {
			$name2 = "Ecosystem";
		    }
		    elsif ( $k2 eq 'ecosystemCategory' ) {
			$name2 = "Ecosystem Category";
		    }
		    elsif ( $k2 eq 'ecosystemType' ) {
			$name2 = "Ecosystem Type";
		    }
		    elsif ( $k2 eq 'ecosystemSubtype' ) {
			$name2 = "Ecosystem Subtype";
		    }
		    elsif ( $k2 eq 'specificEcosystem' ) {
			$name2 = "Specific Ecosystem";
		    }
		    elsif ( $k2 eq 'ownerEmail' ) {
			$name2 = "Owner Email";
		    }
		    elsif ( $k2 eq 'ownerId' ) {
			$name2 = "Owner";
			if ( isInt($val2) ) {
			    my $sql2 = "select name from contact " .
				"where contact_oid = $val2 ";
			    $val2 = db_getValue($sql2);
			}
		    }

		    print "<tr class='img'>\n";
		    print "<td class='img'>$name2</td>\n";
		    print "<td class='img'>" . $val2 . "</td>\n";
		    print "</tr>\n";
		}
	    }  # end for k2
	}   # end a1

	print "</table>\n";
	print "</td>\n";
	print "</tr>\n";
    }

#    my $er_id = db_getValue("select project_oid from project_info where gold_stamp_id = '" . $project_oid . "'");
#    if ( $er_id ) {
#	print "<tr class='img'>\n";
#	print "<td class='img'>IMG Project ID</td>\n";
#	my $proj_link = getProjectLink($er_id); 
#	print "<td class='img'>" . $proj_link . "</td>\n";
#	print "</tr>\n";
#    }
#    else {
#	$er_id = db_getValue("select sample_oid from env_sample where gold_id = '" . $project_oid . "'");
#	if ( $er_id ) {
#	    print "<tr class='img'>\n";
#	    print "<td class='img'>IMG Sample ID</td>\n";
#	    my $samp_link = getSampleLink($er_id);
#	    print "<td class='img'>" . $samp_link . "</td>\n";
#	    print "</tr>\n";
#	}
#    }

##    my $sql2 = "select gold_id from gold_sequencing_project where gold_id = '" . $project_oid . "'";
##    my $g2_id = db_getValue($sql2);
##    if ( $g2_id ) {
##	print "<tr class='img'>\n";
##	print "<td class='img'>GOLD SP in IMG-GOLD</td>\n";
##	my $proj_link = "<a href='" . $main_cgi .
##	    "?section=ProjectInfo&page=imgGoldSequencingProject" . 
##	    "&gold_id=$g2_id' >" .
##	    $g2_id . "</a>";
##	print "<td class='img'>" . $proj_link . "</td>\n";
##	print "</tr>\n";
##    }
#    else {
#	print "<tr class='img'>\n";
#	print "<td class='img'>GOLD SP in IMG-GOLD</td>\n";
#	print "<td class='img'>(not found)</td></tr>\n";
#    }

    print "</table>\n";

    if ( $isAdmin eq 'Yes' && $res ) {
	print "<h5>Raw Result from API Call (for Admin users only)</h5>\n";
	print "<p><pre>";
	my @str = split(/\,/, $res);
	my $cnt2 = 0;
	for my $s2 ( @str ) {
	    if ( $cnt2 ) {
		print ",\n";
	    }
	    print $s2;
	    $cnt2++;
	}
	print "</pre>\n";
    }

    print "<p>\n"; 

    printHomeLink();

    print end_form();
}


sub getSPfromImgGold {
    my ($gold_sp_id, $data) = @_;

    my $has_data = 0;
    $data->{goldId} = $gold_sp_id;

    my $sql2 = "select gold_id, project_status, its_spid, its_proposal_id, funding_program, domain, ncbi_kingdom, ncbi_phylum, ncbi_class, ncbi_order, ncbi_family, ncbi_genus, ncbi_species, type_strain, clade, viral_group, viral_subgroup, ecosystem, ecosystem_category, ecosystem_type, ecosystem_subtype, specific_ecosystem, pi_name, pi_email, pm_name, pm_email, isolation, iso_country, geo_location, latitude_n, longitude_n, altitude, depth, ecotype, sequencing_status, sequencing_strategy, host_name, host_gender, host_ncbi_taxid, cultured, culture_type, uncultured_type, altitude_in_meters, altitude_in_meters2, elevation_in_meters, elevation_in_meters2, depth_in_meters, depth_in_meters2, subsurface_depth_in_meters, subsurface_depth_in_meters2 from gold_sequencing_project where gold_id = ? ";

    my $dbh2 = Connect_IMG();
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_sp_id); 
 
    my ( $id2, @res ) = $cur2->fetchrow_array(); 
    if ( $id2 ) {
	$has_data = 1;
##	$data->{goldId} = $id2;
	$data->{projectStatus} = $res[0];
	$data->{itsSpid} = $res[1];
	$data->{itsProposalId} = $res[2];
	$data->{fundingProgram} = $res[3];
	$data->{ncbiSuperkingdom} = $res[4];
	$data->{ncbiKingdom} = $res[5];
	$data->{ncbiPhylum} = $res[6];
	$data->{ncbiClass} = $res[7];
	$data->{ncbiOrder} = $res[8];
	$data->{ncbiFamily} = $res[9];
	$data->{ncbiGenus} = $res[10];
	$data->{ncbiSpecies} = $res[11];
	$data->{typeStrain} = $res[12];
	$data->{clade} = $res[13];
	$data->{viralGroup} = $res[14];
	$data->{viralSubgroup} = $res[15];
	$data->{ecosystem} = $res[16];
	$data->{ecosystemCategory} = $res[17];
	$data->{ecosystemType} = $res[18];
	$data->{ecosystemSubtype} = $res[19];
	$data->{specificEcosystem} = $res[20];
	$data->{piName} = $res[21];
	$data->{piEmail} = $res[22];
	$data->{pmName} = $res[23];
	$data->{pmEmail} = $res[24];
	$data->{isolation} = $res[25];
	$data->{isoCountry} = $res[26];
	$data->{geoLocation} = $res[27];
	$data->{latitude} = $res[28];
	$data->{longitude} = $res[29];
	$data->{altitude} = $res[30];
	$data->{depth} = $res[31];
	$data->{ecotype} = $res[32];
	$data->{sequencingStatus} = $res[33];
	$data->{sequencingStrategy} = $res[34];
	$data->{hostName} = $res[35];
	$data->{hostGender} = $res[36];
	$data->{hostNcbiTaxid} = $res[37];
	$data->{cultured} = $res[38];
	$data->{cultureType} = $res[39];
	$data->{unculturedType} = $res[40];
        $data->{altitude_in_meters} = $res[41];
        $data->{altitude_in_meters2} = $res[42];
        $data->{elevation_in_meters} = $res[43];
        $data->{elevation_in_meters2} = $res[44];
        $data->{depth_in_meters} = $res[45];
        $data->{depth_in_meters2} = $res[46];
        $data->{subsurface_depth_in_meters} = $res[47];
        $data->{subsurface_depth_in_meters2} = $res[48];
    }

    $cur2->finish();

    ## collaborator
    $sql2 = "select gold_id, name, country, url from gold_sp_collaborator where gold_id = ? ";
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_sp_id); 

    for (;;) {
	my ( $id2, $name, $country, $url ) = $cur2->fetchrow_array(); 
	last if !$id2;

	my %h2;
	$h2{name} = $name;
	$h2{country} = $country;
	$h2{url} = $url;
	if ( $data->{collaborators} ) {
	    push @{$data->{collaborators}}, ( \%h2 );
	}
	else {
	    my @arr = ( \%h2 );
	    $data->{collaborators} = \@arr;
	}
    }
    $cur2->finish();

    ## cell_arrangement, habitat, etc.
    for my $attr ('cell_arrangement', 'disease', 'energy_source', 'habitat',
	'metabolism', 'phenotype', 'relevance', 'seq_method', 'study_gold_id' ) {
	my $attr2 = $attr;
	if ( $attr eq 'cell_arrangement' ) {
	    $attr2 = 'cellArrangement';
	}
	elsif ( $attr eq 'disease' ) {
	    $attr2 = 'diseases';
	}
	elsif ( $attr eq 'energy_source' ) {
	    $attr2 = 'energySource';
	}
	elsif ( $attr eq 'phenotype' ) {
	    $attr2 = 'phenotypes';
	}
	elsif ( $attr eq 'relevance' ) {
	    $attr2 = 'projectRelevance';
	}
	elsif ( $attr eq 'seq_method' ) {
	    $attr2 = 'seqMethod';
	}
	elsif ( $attr eq 'study_gold_id' ) {
	    $attr2 = 'studies';
	}

	my $sql2 = "select gold_id, " . $attr . " from gold_sp_" . $attr . " where gold_id = ? ";
	my $cur2 = $dbh2->prepare($sql2);
	$cur2->execute($gold_sp_id); 

	for (;;) {
	    my ( $id2, $val2 ) = $cur2->fetchrow_array(); 
	    last if !$id2;

	    if ( $data->{$attr2} ) {
		push @{$data->{$attr2}}, ( $val2 );
	    }
	    else {
		my @arr = ( $val2 );
		$data->{$attr2} = \@arr;
	    }
	}
	$cur2->finish();
    }

    ## seq center
    $sql2 = "select gold_id, name, country, url from gold_sp_seq_center where gold_id = ? ";
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_sp_id); 

    for (;;) {
	my ( $id2, $name, $country, $url ) = $cur2->fetchrow_array(); 
	last if !$id2;

	my %h2;
	$h2{name} = $name;
	$h2{country} = $country;
	$h2{url} = $url;
	if ( $data->{sequencingCenters} ) {
	    push @{$data->{sequencingCenters}}, ( \%h2 );
	}
	else {
	    my @arr = ( \%h2 );
	    $data->{sequencingCenters} = \@arr;
	}
    }
    $cur2->finish();

    ## seq genome publications
    $sql2 = "select gold_id, pubmed_id, journal_name, volume, issue, page, title, publication_date, doi from gold_sp_genome_publications where gold_id = ? ";
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_sp_id); 

    for (;;) {
	my ( $id2, $pubmed_id, $journal_name, $volume, $issue, $page, $title, $publication_date, $doi ) = 
	    $cur2->fetchrow_array(); 
	last if !$id2;

	my %h2;
	$h2{pubmedId} = $pubmed_id;
	$h2{journalName} = $journal_name;
	$h2{volume} = $volume;
	$h2{issue} = $issue;
	$h2{page} = $page;
	$h2{title} = $title;
	$h2{publicationDate} = $publication_date;
	$h2{doi} = $doi;
	if ( $data->{genomePublications} ) {
	    push @{$data->{genomePublications}}, ( \%h2 );
	}
	else {
	    my @arr = ( \%h2 );
	    $data->{genomePublications} = \@arr;
	}
    }
    $cur2->finish();

    $dbh2->disconnect();

    return $has_data;
}


########################################################################## 
# SequencingProject_ImgGold: GOLD SP in IMG-GOLD
########################################################################## 
sub SequencingProject_ImgGold {
    my ($gold_id) = @_;

    print start_form(-name=>'sequencingProject',-method=>'post',action=>"$section_cgi"); 
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        printError ( "Unknown username / password" ); 
        return; 
    } 

    if ( ! $gold_id ) {
	return;
    }

    my $def_project = def_ImgGold_Sequencing_Project();
    my @attrs = @{$def_project->{attrs}};
    my $id_fld = $def_project->{id};
    my $sql = "select p." . $def_project->{id};
    for my $attr ( @attrs ) {
	if ( $attr->{name} eq $id_fld ) {
	    next;
	}

	$sql .= ", p." . $attr->{name};
    }
    $sql .= " from gold_sequencing_project p where p." . $id_fld .
	" = ?";
    my $dbh = Connect_IMG(); 
    my $cur = $dbh->prepare($sql);

    $cur->execute($gold_id);
    my @data = $cur->fetchrow();
    $cur->finish();
    my %data_h;
    my $i = 0;
    for my $attr ( @attrs ) {
	if ( $i > scalar(@data) ) {
	    last;
	}
	$data_h{$attr->{name}} = $data[$i];
	$i++;
    }

    # check permission
    my $canAccess = 0;
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
	$canAccess = 1;
    }
    elsif ( $data_h{'contact_oid'} == $contact_oid ) {
	$canAccess = 1;
    }
    elsif ( $data_h{'pi_email'} ) {
	my $email = db_getValue("select email from contact where contact_oid = $contact_oid");
	if ( lc($email) eq lc ($data_h{'pi_email'}) ) {
	    $canAccess = 1;
	}
    }

    if ( ! $canAccess ) {
	print "<h5>Cannot find the project</h5>\n";
	return;
    }

    print "<h1>GOLD Sequencing Project in IMG-GOLD: $gold_id</h1>\n";
    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 

    my $curr_tab = "None";
    my $bg_color = 'lightblue';
    for my $attr1 ( @attrs ) {
	my $key = $attr1->{name};
	if ( $key eq 'gold_id' ) {
	    next;
	}

	my $aname = $attr1->{display_name};
	my $val = $data_h{$key};

	if ( ! $val ) {
	    next;
	}

	if ( ($key eq 'contact_oid' || $key eq 'modified_by') &&
	     isInt($val) ) {
	    $val = db_getValue("select name from contact where contact_oid = $val");
	}

	my $tab = $attr1->{tab};
	if ( $tab ne $curr_tab ) {
	    $bg_color = $attr1->{bg_color};
	    print "<tr class='img' >\n"; 
	    print "  <th class='subhead' align='right' bgcolor='$bg_color'>" .
		"<b>" . $tab . "</b></th>\n"; 
	    print "  <td class='img'   align='left' bgcolor='$bg_color'>" .
		"</td>\n"; 
	    print "</tr>\n"; 
	    $curr_tab = $tab;
	}

	print "<tr class='img' >\n"; 
	print "<td class='img' bgcolor='$bg_color'>" . 
	    $aname . "</td>\n";
	print "<td class='img'>" . $val . "</td>\n";
	print "</tr>\n";
    }   # end for attr1

    for my $key3 ( 'cell_arrangement', 'disease', 'energy_source', 
		   'habitat', 'metabolism', 'phenotype', 'relevance',
		   'seq_method' ) {
	my @arr3 = ();
	my $sql3 = "select gold_id, $key3 from gold_sp_" . $key3 .
	    " where gold_id = ?";

	my $cur3 = $dbh->prepare($sql3);
	$cur3->execute($gold_id);
	for (;;) {
	    my ($g3_id, $val3) = $cur3->fetchrow();
	    last if ! $g3_id;

	    push @arr3, ( $val3 );
	}
	$cur3->finish();

	if ( scalar(@arr3) == 0 ) {
	    next;
	}

	my $name3 = ucfirst($key3);
	if ( $key3 eq 'cell_arrangement' ) {
	    $name3 = 'Cell Arrangement';
	}
	elsif ( $key3 eq 'energy_source' ) {
	    $name3 = 'Energy Source';
	}
	elsif ( $key3 eq 'relevance' ) {
	    $name3 = 'Project Relevance';
	}
	elsif ( $key3 eq 'seq_method' ) {
	    $name3 = 'Sequencing Method';
	}

	print "<tr class='img' >\n"; 
	$bg_color = 'aquamarine';
	print "<td class='img' bgcolor='$bg_color'>" . 
	    $name3 . "</td>\n";
	print "<td class='img'>\n";
	for my $a3 ( @arr3 ) {
	    print $a3 . "<br/>\n";
	}
	print "</td>\n";
	print "</tr>\n";
    }

    ## collaborator and seq_center
    for my $key3 ( 'collaborator', 'seq_center' ) {
	my $sql3 = "select gold_id, name, country, url from gold_sp_" . 
	    $key3 . " where gold_id = ?";
	my $cur3 = $dbh->prepare($sql3);
	$cur3->execute($gold_id);
	my $k = 0;
	for (;;) {
	    my ($g3_id, $name3, $country3, $url3) = $cur3->fetchrow();
	    last if ! $g3_id;

	    if ( $k == 0 ) {
		my $name3 = ucfirst($key3);
		if ( $key3 eq 'seq_center' ) {
		    $name3 = 'Sequencing Center';
		}

		print "<tr class='img' >\n"; 
		$bg_color = 'aquamarine';
		print "<td class='img' bgcolor='$bg_color'>" . 
		    $name3 . "</td>\n";
		print "<td class='img'>\n";
	    }

	    if ( $url3 ) {
		print alink($url3, $name3);
	    }
	    else {
		print $name3;
	    }
	    if ( $country3 ) {
		print " ($country3)";
	    }
	    print "<br/>\n";

	    $k++;
	}
	$cur3->finish();
	print "</td>\n";
	print "</tr>\n";
    }

    ## publications
    for my $key3 ( 'genome_publications' ) {
	my @key4 = ( 'pubmed_id', 'journal_name', 'volume', 'issue',
		     'page', 'title', 'publication_date', 'doi' );

	my $sql3 = "select gold_id, " . join(", ", @key4) .
	    " from gold_sp_" . $key3 . " where gold_id = ?";
	my $cur3 = $dbh->prepare($sql3);
	$cur3->execute($gold_id);
	for (;;) {
	    my ($g3_id, @rest) = $cur3->fetchrow();
	    last if ! $g3_id;

	    print "<tr class='img' >\n"; 
	    print "<td class='img' bgcolor='lightblue'>" . 
		"Genome Publications" . "</td>\n";
	    print "<td class='img'>\n";
	    print "<table class='img'>\n";

	    my $k = 0;
	    for my $k2 ( @key4 ) {
		my $k2_val = $rest[$k];
		if ( ! $k2_val ) {
		    $k++;
		    next;
		}

		my $name2 = ucfirst($k2);
		if ( $k2 eq 'pubmed_id' ) {
		    $name2 = "Pubmed ID";
		}
		elsif ( $k2 eq 'journal_name' ) {
		    $name2 = "Journal Name";
		}
		elsif ( $k2 eq 'publication_date' ) {
		    $name2 = "Publication Date";
		}

		print "<tr class='img'>\n";
		print "<td class='img'>$name2</td>\n";
		print "<td class='img'>" . $k2_val . "</td>\n";
		print "</tr>\n";

		$k++;
	    }  # end for k2
	    print "</table>\n";
	    print "</td></tr>\n";
	}
	$cur3->finish();
	print "</td>\n";
	print "</tr>\n";
    }

    ## Study
    my @arr3 = ();
    my $sql3 = "select gold_id, study_gold_id from gold_sp_study_gold_id" .
	" where gold_id = ?";
    my $cur3 = $dbh->prepare($sql3);
    $cur3->execute($gold_id);
    for (;;) {
	my ($g3_id, $val3) = $cur3->fetchrow();
	last if ! $g3_id;

	push @arr3, ( $val3 );
    }
    $cur3->finish();
    if ( scalar(@arr3) > 0 ) {
	# has studies
	print "<tr class='img' >\n"; 
	print "<td class='img' bgcolor='lightblue'>" . 
	    "Study" . "</td>\n";
	print "<td class='img'>\n";
	print "<table class='img'>\n";

	for my $a1 ( @arr3 ) {
	    if ( ! $a1 ) {
		next;
	    }

	    my $res2 = getNewGoldMetadata($a1);

	    if ( ! $res2 || blankStr($res2) ) {
		print "Gold ID: " . $a1 . "<br/>\n";
		next;
	    }

	    my $decode2 = decode_json($res2);

	    # specific order
	    my @key2 = ( 'goldId', 'studyName', 
##			 'ownerId', 'ownerEmail',
			 'ecosystem', 'ecosystemCategory',
			 'ecosystemType', 'ecosystemSubtype',
			 'specificEcoSystem' );

	    for my $k2 ( @key2 ) {
		if ( $decode2->{$k2} ) {
		    my $name2 = $k2;
		    my $val2 = $decode2->{$k2};
		    if ( $k2 eq 'goldId' ) {
			$name2 = "Study GOLD ID";
		    }
		    elsif ( $k2 eq 'studyName' ) {
			$name2 = "Study Name";
		    }
		    elsif ( $k2 eq 'ecosystem' ) {
			$name2 = "Ecosystem";
		    }
		    elsif ( $k2 eq 'ecosystemCategory' ) {
			$name2 = "Ecosystem Category";
		    }
		    elsif ( $k2 eq 'ecosystemType' ) {
			$name2 = "Ecosystem Type";
		    }
		    elsif ( $k2 eq 'ecosystemSubtype' ) {
			$name2 = "Ecosystem Subtype";
		    }
		    elsif ( $k2 eq 'specificEcosystem' ) {
			$name2 = "Specific Ecosystem";
		    }
		    elsif ( $k2 eq 'ownerEmail' ) {
			$name2 = "Owner Email";
		    }
		    elsif ( $k2 eq 'ownerId' ) {
			$name2 = "Owner";
			if ( isInt($val2) ) {
			    my $sql2 = "select name from contact " .
				"where contact_oid = $val2 ";
			    $val2 = db_getValue($sql2);
			}
		    }

		    print "<tr class='img'>\n";
		    print "<td class='img'>$name2</td>\n";
		    print "<td class='img'>" . $val2 . "</td>\n";
		    print "</tr>\n";
		}
	    }  # end for k2
	}   # end a1

	print "</table>\n";
	print "</td>\n";
	print "</tr>\n";
    }

    print "</table>\n";

    my $res = getNewGoldMetadata($gold_id);
    if ( ! $res || blankStr($res) ) {
	print "<h5>Cannot find the sequencing project.</h5>\n";
	return;
    }

#    my $decode = decode_json($res);
#    my @keys = (keys %$decode);

    if ( $isAdmin eq 'Yes' ) {
	print "<h5>Raw Result from API Call (for Admin users only)</h5>\n";
	print "<p><pre>";
	my @str = split(/\,/, $res);
	my $cnt2 = 0;
	for my $s2 ( @str ) {
	    if ( $cnt2 ) {
		print ",\n";
	    }
	    print $s2;
	    $cnt2++;
	}
	print "</pre>\n";
    }

    print "<p>\n"; 

    printHomeLink();

    print end_form();
}


sub getStudyfromImgGold {
    my ($gold_sp_id, $data) = @_;

    my $has_data = 0;
    $data->{goldId} = $gold_sp_id;

    my $sql2 = "select gold_id, study_name, ecosystem, ecosystem_category, ecosystem_type, ecosystem_subtype, specific_ecosystem from gold_study where gold_id = ? ";

    my $dbh2 = Connect_IMG();
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($gold_sp_id); 
 
    my ( $id2, @res ) = $cur2->fetchrow_array(); 
    if ( $id2 ) {
	$has_data = 1;
##	$data->{goldId} = $id2;
	$data->{studyName} = $res[0];
	$data->{ecosystem} = $res[16];
	$data->{ecosystemCategory} = $res[17];
	$data->{ecosystemType} = $res[18];
	$data->{ecosystemSubtype} = $res[19];
	$data->{specificEcosystem} = $res[20];
    }

    $cur2->finish();

    return $has_data;
}



1;
