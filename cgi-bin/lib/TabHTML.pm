#
# $Id: TabHTML.pm,v 1.2 2007/10/11 20:01:02 klchu Exp $
#
package TabHTML;

use strict;
use CGI qw( :standard );
use Data::Dumper;

use WebEnv;

$| = 1;

my $section = "TabHTML";
my $env = getEnv( );
my $base_url  = $env->{base_url};
my $yui_dir = $env->{yui_dir};
my $YUI     = "$yui_dir";

#
# print yui depended js and css files
# param $tabName used for yahoo api
#
sub printTabAPILinks {
    my($tabName) =@_;
    
	print <<EOF;
<!-- Dependencies -->

<script type="text/javascript" src="$base_url/GeneDetailTab.js"></script>

<!-- core CSS -->
<!--
<link rel="stylesheet" type="text/css" href="$YUI/build/tabview/assets/tabview.css">
-->
<!-- optional skin for border tabs -->

<link rel="stylesheet" type="text/css" href="$base_url/img_border_tabs.css">


<script type="text/javascript" src="$YUI/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="$YUI/build/element/element-beta-min.js"></script>

<!-- Source file -->
<script type="text/javascript" src="$YUI/build/tabview/tabview-min.js"></script>
<script type="text/javascript" src="$YUI/build/connection/connection-min.js"></script> 

<script type="text/javascript">
var myTabs = new YAHOO.widget.TabView("$tabName");
</script> 

EOF
  
}

# create the html div for the tabs
# BUT it DOES NOT print the </div> yet, its up to you to print
# your code here
#
# 
# param $tabName used for yahoo api
# param $tabIndex_aref array list of tab index names
# param $tabNames_aref array list of tab names
# param $width optional - width of tab size - default is 790
sub printTabDiv {
    my ($tabName, $tabIndex_aref, $tabNames_aref, $width) = @_;
    
    print "<div id=\"$tabName\" class=\"yui-navset\">\n";
    print "<ul class=\"yui-nav\">\n";


    for(my $i=0; $i<=$#$tabIndex_aref; $i++) {
        if($i == 0 ) {
            # select the first tab
            print qq{<li class="selected"><a href="$tabIndex_aref->[$i]"><em>$tabNames_aref->[$i]</em></a></li>\n};
        } else {
            print qq{<li><a href="$tabIndex_aref->[$i]"><em>$tabNames_aref->[$i]</em></a></li>};
        }
    }
    
    print "</ul> \n";
    
    $width = 790 if($width eq "");
    
    print "<div class='yui-content' style='width: $width". "px;'>\n";
    #print "<div class='yui-content'>\n";
    # now print your code!
}

# end of tab div
sub printTabDivEnd {
    # yui-content div
    print "</div>\n";
    # tabName div
    print "</div>\n";
}



1;
