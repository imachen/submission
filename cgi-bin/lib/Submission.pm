package Submission;
my $section = "Submission";

use strict;
use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';
use POSIX qw(ceil floor);
use LWP::UserAgent;
use HTTP::Request::Common qw( GET POST PUT);
use REST::Client;
use MIME::Base64;
use JSON;
use Data::Dumper;
use Carp qw(longmess);

use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;
use ProjectInfo;
use EnvSample;
use TabHTML;
use JSON;

my $env             = getEnv();
my $main_cgi        = $env->{main_cgi};
my $section_cgi     = "$main_cgi?section=$section";
my $base_url        = $env->{base_url};
my $illumina        = $env->{illumina};
my $use_yahoo_table = $env->{use_yahoo_table};
my $workspace_dir   = $env->{workspace_dir};

my $new_gold_url = "https://gold.jgi.doe.gov/index";

my $helpballoon_dir = $env->{helpballoon_dir};
my $HELPBALLOON     = $helpballoon_dir;

my $default_max_sub_row = 30;
my $test_mode           = 0;

#my $test_mode = 3;

#########################################################################
# dispatch
#########################################################################
sub dispatch {
    my ($page) = @_;

    if ( $page eq 'showERPage' ) {
        ShowPage('ER');
    } elsif ( $page eq 'showMPage' ) {
        ShowPage('M');
    } elsif ( $page eq 'showMethylomics' ) {
        ShowPage('Methylomics');
    } elsif ( $page eq 'showRnaSeq' ) {
        ShowPage('RnaSeq');
    } elsif ( $page eq 'showMerRnaSeq' ) {
        ShowPage('MerRnaSeq');
    } elsif ( $page eq 'newMain' ) {

        #	ShowNewSubmissionMainPage();
        ShowNewApSubmissionMainPage();
    } elsif ( $page eq 'signAgreement' ) {
        my $msg          = "";
        my $gold_ap_id   = param1('gold_ap_id');
        my $project_type = param1('project_type_option');
        if (
             !$gold_ap_id
             && (    $project_type eq 'combined'
                  || $project_type eq 'extract' )
          )
        {
            ( $gold_ap_id, $msg ) = printCreateAP();
        }

        if ( blankStr($msg) ) {
            printSignAgreement($gold_ap_id);
        } else {
            WebFunctions::showErrorPage($msg);
        }
    } elsif ( $page eq 'submitProject' ) {
        my $msg = checkIsolateGenomeProject();
        if ( blankStr($msg) ) {
#            NewSubmission('Project');
	    NewSubmission_v52('Project');
        } else {
            WebFunctions::showErrorPage($msg);
        }
    } elsif ( $page eq 'submitSample' ) {
        my $msg = checkMetagenomeSample();
        if ( blankStr($msg) ) {
            #NewSubmission('Sample');
	    NewSubmission_v52('Sample');
        } else {
            WebFunctions::showErrorPage($msg);
        }
    } elsif ( $page eq 'checkSubmission' ) {
        CheckSubmission();
    } elsif ( $page eq 'checkV52Submission' ) {
        CheckV52Submission();
    } elsif ( $page eq 'checkStatus' ) {
        ShowStatus();
    } elsif ( $page eq 'showQuickSearch' ) {
        my $subm_id = param1('qs_value');
        if ( !$subm_id || !isInt($subm_id) ) {
            WebFunctions::showErrorPage(
                                       "Please enter a correct submission ID.");
        } elsif (
            !db_getValue(
                "select count(*) from submission where submission_id = $subm_id"
            )
          )
        {
            WebFunctions::showErrorPage(
                                    "Cannot find submission with ID $subm_id.");
        } else {
            DisplaySubmission($subm_id);
        }
    } elsif ( $page eq 'updateQuickSearch' ) {
        my $subm_id = param1('qs_value');
        if ( !$subm_id || !isInt($subm_id) ) {
            WebFunctions::showErrorPage(
                                       "Please enter a correct submission ID.");
        } elsif (
            !db_getValue(
                "select count(*) from submission where submission_id = $subm_id"
            )
          )
        {
            WebFunctions::showErrorPage(
                                    "Cannot find submission with ID $subm_id.");
        } else {
            ShowStatus($subm_id);
        }
    } elsif ( $page eq 'filterSubmission' ) {
        FilterSubmission();
    } elsif ( $page eq 'applySubmissionFilter' ) {
        my $msg = ApplySubmissionFilter();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage();
        }
    } elsif ( $page eq 'updateStatus' ) {
        my $msg = dbUpdateStatus();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage();
        }
    } elsif ( $page eq 'displaySubmission' ) {
        my $submission_id = param1('submission_id');
        DisplaySubmission($submission_id);
    } elsif ( $page eq 'displayFile' ) {
        my $submission_id = param1('submission_id');
        my $file_type     = param1('file_type');
        DisplayFile( $submission_id, $file_type );
    } elsif ( $page eq 'updateContact' ) {
        my $submission_id = param1('submission_id');
        UpdateContact($submission_id);
    } elsif ( $page eq 'dbUpdateIMGContact' ) {
        my $msg = dbUpdateIMGContact();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage();
        }
    } elsif ( $page eq 'cancelSubmission' ) {
        CancelSubmission();
    } elsif ( $page eq 'setIMGPermission' ) {
        my $submission_id = param1('sub_id_permission');
        SetIMGPermission($submission_id);
    } elsif ( $page eq 'dbUpdatePermission' ) {
        my $msg = dbUpdatePermission();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage();
        }
    } elsif ( $page eq 'dbUpdateApproval' ) {
        my $msg = dbUpdateApproval();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage();
        }
    } elsif ( $page eq 'viewProjectInfo' ) {
        my $project_oid = param1('project_oid');
        if ( blankStr($project_oid) ) {
            WebFunctions::showErrorPage("No project is selected.");
        } else {
            require ProjectInfo;
            ProjectInfo::DisplayProject($project_oid);
        }
    } elsif ( $page eq 'submitToER' ) {
        my $project_type = param('project_type_option');
        if ( param1('proj_search_option') eq 'keyword' ) {
            if ( $project_type eq 'extract' ) {
                ShowKeywordSearchImgMetagenome( 'IMG ER', 'M' );
            } else {
                ShowKeywordSearchSubmissionProject('IMG ER');
            }
        } elsif ( param1('proj_search_option') eq 'gold_ap' ) {
            ShowGoldAPSubmissionProject( 'IMG ER', 'G' );
        } else {
            if ( $project_type eq 'extract' ) {
                ShowQuerySearchImgMetagenome( 'IMG ER', 'M' );
            } else {
                ShowSearchSubmissionProject('IMG ER');
            }
        }
    } elsif ( $page eq 'submitMToM' ) {
        if ( param1('proj_search_option') eq 'keyword' ) {
            ShowKeywordSearchSubmissionProject( 'IMG/M ER', 'M' );
        } elsif ( param1('proj_search_option') eq 'gold_ap' ) {
            ShowGoldAPSubmissionProject( 'IMG/M ER', 'M' );
        } else {
            ShowSearchSubmissionProject( 'IMG/M ER', 'M' );
        }
    } elsif ( $page eq 'submitGToM' ) {
        if ( param1('proj_search_option') eq 'keyword' ) {
            ShowKeywordSearchSubmissionProject( 'IMG/M ER', 'G' );
        } else {
            ShowSearchSubmissionProject( 'IMG/M ER', 'G' );
        }
    } elsif ( $page eq 'selectSubProj' ) {
        my $msg = checkUserInput();
        if ($msg) {
            WebFunctions::showErrorPage($msg);
        } elsif ($use_yahoo_table) {
            ShowSelectSubmissionProject_yui();
        } else {
            ShowSelectSubmissionProject();
        }
    } elsif ( $page eq 'selectSubSample' ) {
        my $msg = checkMetagenomeProject();
        if ( blankStr($msg) ) {
            ShowSelectSubmissionSample();
        } else {
            WebFunctions::showErrorPage($msg);
        }
    } elsif ( $page eq 'selectImgMetagenome' ) {
        my $msg = checkUserInput();
        if ($msg) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowSelectImgMetagenome_yui();
        }
    } elsif ( $page eq 'selectSubProposal' ) {
        my $msg = checkMetagenomeProject();
        if ( blankStr($msg) ) {
            ShowSelectProposalToSubmit();
        } else {
            WebFunctions::showErrorPage($msg);
        }
    } elsif ( $page eq 'defaultSample' ) {
        my $msg = checkMetagenomeProject();
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            my $new_s_oid = EnvSample::CreateDefaultSample();

            my $msg = "New default sample (OID = $new_s_oid) created.";
            ShowSelectSubmissionSample($msg);
        }
    } elsif ( $page eq 'exportProjectList' ) {
        ExportProjectList();
    } elsif ( $page eq 'showStats' ) {
        ShowStatisticsInfo();
    } elsif ( $page eq 'ownerGrantAccess' ) {
        ShowGrantAccessResult();
    } elsif ( $page eq 'releaseGenome' ) {
        ShowReleaseResult(1);
    } elsif ( $page eq 'makePrivate' ) {
        ShowReleaseResult(0);
    } elsif ( $page eq 'exportList' ) {
        ExportList();
    } else {
        ShowPage();
    }
}

##########################################################################
# ShowPage - show the Home page
##########################################################################
sub ShowPage {
    my ($type) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;

    my $omics = 0;

    my $isAdmin = getIsAdmin($contact_oid);
    my $uname   = db_getContactName($contact_oid);

    if ( $isAdmin eq 'Yes' ) {
        ShowQuickSearchSection($database);
        print
"<h5>You log in as Admin $uname. You can switch 'Data Type' to see different types of submissions.";
        print "</h5>\n";

    } else {
        print
"<h5>You log in as User $uname. You can switch 'Data Type' to see different types of submissions.</h5>\n";
    }

    my $new_url = $main_cgi . "?section=ERSubmission&page=showPage";
    $database = ShowDataSetSelectionSection( $isAdmin, $database, $new_url );

    # ignore type
    $type = "";

    if ( $type eq 'ER' || $database eq 'IMG ER' ) {

        #	$database = 'IMG ER';
        print "<h2>Isolate \& Single Cell Genome Submissions</h2>\n";
    } elsif ( $type eq 'M' || $database eq 'IMG/M ER' ) {

        #	$database = 'IMG/M ER';
        print "<h2>Metagenome Submissions</h2>\n";
    } elsif (    $type eq 'Methylomics'
              || $database eq 'IMG Methylomics' )
    {
        #	$database = 'IMG Methylomics';
        print "<h2>Methylome Submissions</h2>\n";
        $omics = 1;
    } elsif (    $type eq 'RnaSeq'
              || $database eq 'IMG_ER_RNASEQ' )
    {
        #	$database = 'IMG_ER_RNASEQ';
        print "<h2>Transcriptome Submissions</h2>\n";
        $omics = 2;
    } elsif (    $type eq 'MerRnaSeq'
              || $database eq 'IMG_MER_RNASEQ' )
    {
        #	$database = 'IMG_MER_RNASEQ';
        print "<h2>Metatranscriptome Submissions</h2>\n";
        $omics = 2;
    }
    print hiddenVar( 'database', $database );

    my $debug = 0;
    if ( $contact_oid == 312 && $debug ) {
        my @pageParams = (
                           'browse:submission_page_no',
                           'browse:submission_orderby',
                           'browse:submission_desc'
        );

        for my $p1 (@pageParams) {
            if ( getSessionParam($p1) ) {
                print "<p>$p1: " . getSessionParam($p1) . "</p>\n";
            }
        }
    }

    if ($use_yahoo_table) {
        ShowSubmissionSection_yui( $contact_oid, $isAdmin, $database );
    } else {
        ShowSubmissionSection( $contact_oid, $isAdmin, $database );
    }

    # new submission
    #    if ( $omics ) {
    #	# we don't allow omics submissions at this moment
    #    }
    #    else {
    #	ShowNewSubmissionSection ($contact_oid, $isAdmin, $database);
    #    }

    if ( $isAdmin eq 'Yes' ) {
        print hr();
        ShowSetApproval($database);
    }

    if ($omics) {
        printHomeLink();
        print end_form();
        return;
    }

    if (    $contact_oid == 312
         || $contact_oid == 16
         || $contact_oid == 19
         || $contact_oid == 100023
         || $contact_oid == 313
         || $contact_oid == 3034
         || $contact_oid == 100440
         || $contact_oid == 100575
         || $contact_oid == 11
         || $contact_oid == 17
         || $contact_oid == 1225 )
    {
        print hr();
        ShowSetIMGPermission($database);
    }

    printHomeLink();

    print end_form();
}

########################################################################
# ShowQuickSearch
########################################################################
sub ShowQuickSearchSection {
    my ($database) = @_;

    print
"<div style='background-color:lightgreen; border:1px solid darkgreen;'>\n";
    print "<table>\n";
    print "<tr>\n";
    print "<td>Submission ID: </td>\n";
    print "<td>\n";
    print "<input type='text' id='qs_value' name='qs_value' ";
    print " size='20' maxLength= '20'>\n";
    print "</td>\n";
    print "<td>\n";

    print
"<input type='submit' name='_section_ERSubmission:showQuickSearch' value='View' />\n";
    print nbsp(1);
    print
"<input type='submit' name='_section_ERSubmission:updateQuickSearch' value='Update' />\n";

    print "</td>\n";
    print "</tr>\n";
    print "</table>\n";
    print "</div>\n";
}

########################################################################
# ShowSubmissionSection (HTML table display)
########################################################################
sub ShowSubmissionSection {
    my ( $contact_oid, $isAdmin, $database ) = @_;

    my $cnt   = submissionCount( $contact_oid, $isAdmin, $database, '' );
    my $cond2 = submissionFilterCondition();
    my $cnt2  = submissionCount( $contact_oid, $isAdmin, $database, $cond2 );

    print "<p>Selected Submission Count: $cnt2 (Total: $cnt)\n";

    if ( $cnt != $cnt2 ) {
        print
"  <font color='red'>Filter is on. Some submissions may be hidden. Reset the filter to see more submissions.</font>\n";
    }
    print "</p>\n";

    # save orderby param
    my $orderby = param1('submission_orderby');
    if (    blankStr($orderby)
         && getSessionParam('browse:submission_orderby') )
    {
        $orderby = getSessionParam('browse:submission_orderby');
    }
    my $desc = param1('submission_desc');
    if ( blankStr($desc) && getSession('browse:submission_desc') ) {
        $desc = getSessionParam('browse:submission_desc');
    }

    # max display
    my $max_display = getSessionParam('sub_filter:max_display');
    if ( blankStr($max_display) ) {
        $max_display = $default_max_sub_row;
    }

    # display page numbers
    my $curr_page = param1('submission_page_no');
    if ( blankStr($curr_page) || $curr_page <= 0 ) {
        if ( getSessionParam('browse:submission_page_no') ) {
            $curr_page = getSessionParam('browse:submission_page_no');
        }

        if ( blankStr($curr_page) || $curr_page <= 0 ) {
            $curr_page = 1;
        }
    }

    # last page
    my $last = ceil( $cnt2 / $max_display );

    # reset page number if too large
    if ( $curr_page > $last ) {
        $curr_page = $last;
    }

    # start and end pages
    my $cnt0  = 20;
    my $start = $cnt0 * floor( ( $curr_page - 1 ) / $cnt0 ) + 1;
    my $end   = $start + $cnt0 - 1;
    if ( $end > $last ) {
        $end = $last;
    }

    # save browse history
    setSessionParam( 'browse:submission_page_no', $curr_page );
    setSessionParam( 'browse:submission_orderby', $orderby );
    setSessionParam( 'browse:submission_desc',    $desc );

    if ( $cnt2 == 0 ) {
        print "<h5>No submissions to be displayed.</h5>\n";
        return;
    }

    my $link0 = "<a href='" . $main_cgi;
    if ( $database eq 'IMG/M ER' ) {
        $link0 .= "?section=MSubmission&page=showMPage";
    } else {
        $link0 .= "?section=ERSubmission&page=showERPage";
    }

    print "<p>\n";

    # show first?
    if ( $start > 1 ) {
        my $link = $link0 . "&submission_page_no=1";
        if ( !blankStr($orderby) ) {
            $link .= "&submission_orderby=$orderby";
            if ( !blankStr($desc) ) {
                $link .= "&submission_desc=$desc";
            }
        }
        $link .= "' >" . "First" . "</a>";
        print $link . nbsp(1);
    }

    # show previous?
    if ( $start > $cnt0 ) {
        my $prev = $start - $cnt0;
        my $link = $link0 . "&submission_page_no=$prev";
        if ( !blankStr($orderby) ) {
            $link .= "&submission_orderby=$orderby";
            if ( !blankStr($desc) ) {
                $link .= "&submission_desc=$desc";
            }
        }
        $link .= "' >" . "Prev" . "</a>";
        print $link . nbsp(1);
    }

    my $i    = 0;
    my $page = 1;
    for ( $page = $start ; $page <= $end && $page <= $last ; $page++ ) {
        my $s = $page;
        if ( $page == $curr_page ) {
            $s = "<b>\[$page\]</b>";
        }
        my $link = $link0 . "&submission_page_no=$page";
        if ( !blankStr($orderby) ) {
            $link .= "&submission_orderby=$orderby";
            if ( !blankStr($desc) ) {
                $link .= "&submission_desc=$desc";
            }
        }
        $link .= "' >" . $s . "</a>";
        print $link . nbsp(1);
        $i += $max_display;
        if ( $page > 1000 ) {
            last;
        }
    }

    # show next
    $page = $start + $cnt0;
    if ( $page <= $last ) {
        my $link = $link0 . "&submission_page_no=$page";
        if ( !blankStr($orderby) ) {
            $link .= "&submission_orderby=$orderby";
            if ( !blankStr($desc) ) {
                $link .= "&submission_desc=$desc";
            }
        }
        $link .= "' >" . "Next" . "</a>";
        print $link . nbsp(1);
    }

    # show last
    if ( $last > $end ) {
        my $link = $link0 . "&submission_page_no=$last";
        if ( !blankStr($orderby) ) {
            $link .= "&submission_orderby=$orderby";
            if ( !blankStr($desc) ) {
                $link .= "&submission_desc=$desc";
            }
        }
        $link .= "' >" . "Last" . "</a>";
        print $link . nbsp(1);
    }

    print "<p>\n";

    if ( $cnt == 0 ) {
        return;
    }

    if ( $cnt2 == 0 ) {
        return;
    }

    ShowSubmissionButtons( $isAdmin, $database );

    my $cond = param1('submit_cond');
    listSubmissions( $contact_oid, $isAdmin, $database, $cond, $curr_page, 0 );

    ShowSubmissionButtons( $isAdmin, $database );

    #    if ( $isAdmin eq 'Yes' ) {
    ShowGrantAccessSection( $contact_oid, $isAdmin, $database );

    #    }
}

########################################################################
# ShowSubmissionSection_yui (Yahoo database display)
########################################################################
sub ShowSubmissionSection_yui {
    my ( $contact_oid, $isAdmin, $database ) = @_;

    my $cnt   = submissionCount( $contact_oid, $isAdmin, $database, '' );
    my $cond2 = submissionFilterCondition();
    my $cnt2  = submissionCount( $contact_oid, $isAdmin, $database, $cond2 );

    print "<p>Selected Submission Count: $cnt2 (Total: $cnt)\n";

    if ( $cnt != $cnt2 ) {
        print
"  <font color='red'>Filter is on. Some submissions may be hidden. Reset the filter to see more submissions.</font>\n";
    }
    print "</p>\n";

    # max display
    my $max_display = getSessionParam('sub_filter:max_display');
    if ( blankStr($max_display) ) {
        $max_display = $default_max_sub_row;
    }

    if ( $cnt == 0 ) {
        return;
    }

    if ( $cnt2 == 0 ) {
        return;
    }

    ShowSubmissionButtons( $isAdmin, $database );

    my $cond = param1('submit_cond');
    listSubmissions_yui( $contact_oid, $isAdmin, $database, $cond, 0 );

    ShowSubmissionButtons( $isAdmin, $database );

    #    if ( $isAdmin eq 'Yes' ) {
    ShowGrantAccessSection( $contact_oid, $isAdmin, $database );

    #    }

    #    if ( $isAdmin eq 'Yes' ) {
    ShowReleaseGenomeSection( $contact_oid, $isAdmin, $database );

    #    }
}

############################################################################
# ShowGrantAccessSection {
############################################################################
sub ShowGrantAccessSection {
    my ( $contact_oid, $isAdmin, $database ) = @_;

    if ( $database =~ /Methylomics/ ) {
        return;
    }

    if ( $isAdmin ne 'Yes' && $database =~ /RNASEQ/ ) {
        return;
    }

    print "<h4>Grant Access to:</h4>\n";

    print "<p>IMG user names or JGI SSO user names (separated by ,): \n";
    print nbsp(3);
    print
"<input type='text' name='img_user_names' value='' size='80' maxLength='200'/>\n";
    print "</p>\n";

    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    } elsif ( $database eq 'IMG_ER_RNASEQ' ) {
        $section = 'RnaSeq';
    }
    if ( $database eq 'IMG_MER_RNASEQ' ) {
        $section = 'MerRnaSeq';
    }
    print
"<input type='submit' name='_section_$section:ownerGrantAccess' value='Grant Access' class='medbutton' />";
}

############################################################################
# ShowReleaseGenomeSection {
############################################################################
sub ShowReleaseGenomeSection {
    my ( $contact_oid, $isAdmin, $database ) = @_;

    if (    $database =~ /Methylomics/
         || $database =~ /RNASEQ/ )
    {
        return;
    }

    print "<h4>Release / Unrelease Genomes</h4>\n";

    #    if ( $contact_oid != 11 && $contact_oid != 17 &&
    #	$contact_oid != 10 ) {
    #	print "<p><font color='red'>" .
##	    "Per Victor, only Nikos, Natalia and himself can release genomes. " .
    #	    "Please contact Victor, Nikos or Natalia re. genome release." .
    #	    "</font><br/>\n";
    #	return;
    #    }

    print qq{
          <p><font color='red'><b>Note:</b> All JGI sequenced genomes are
          controlled by JGI ITS. Please contact JGI regarding all
          genome releasing issues. Changes in IMG will be overridden by ITS.
          </font>
          };

    print "<p>Release genomes associated with selected submission(s).\n";

    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }

    print "<p>Release Note: \n";
    print nbsp(3);
    print
"<input type='text' name='release_note' value='' size='80' maxLength='200'/>\n";
    print "</p>\n";

    print "<p>\n";
    print
"<input type='submit' name='_section_$section:releaseGenome' value='Release Genome(s)' class='medbutton' />";
    print "<p>\n";
    print "<h6>Please contact us if a genome was released by mistake.</h6>\n";

#    print "<input type='submit' name='_section_$section:makePrivate' value='Make Private' class='medbutton' />";
}

############################################################################
# ShowNewSubmissionMainPage
############################################################################
sub ShowNewSubmissionMainPage {

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }
    my $isAdmin = getIsAdmin($contact_oid);

    print "<h1>New Submission</h1>\n";

    my $database = param1('database');
    $database =~ s/%20/ /g;

    #    if ( $isAdmin eq 'Yes' ) {
    #	print "<p><font color='red'>Analysis project submission is still " .
    #	    "in testing mode. It's available for super users only.</font>\n";
    #    }

    my $new_url = $main_cgi . "?section=NewSubmission&page=newMain";
    $database = ShowDataSetSelectionSection( $isAdmin, $database, $new_url );

    if (    $database ne 'IMG ER'
         && $database ne 'IMG/M ER' )
    {
        print "<p><b>Error: Submission UI does not support "
          . getDataSetDisplayName($database)
          . " submissions. "
          . "Please change dataset selection to "
          . getDataSetDisplayName('IMG ER') . " or "
          . getDataSetDisplayName('IMG/M ER')
          . ".</b>\n";

        printHomeLink();

        print end_form();

        return;
    }

    if ( $database eq 'IMG ER' ) {
        print "<p>Project type: \n";
        print nbsp(3);
        print "<input type='radio' name='project_type_option' value='isolate'";
        print " checked";
        print " />Isolate Genome\n";
        print nbsp(1);
        print
          "<input type='radio' name='project_type_option' value='single_cell'";
        print " />Single Cell";

        #	if ( $isAdmin eq 'Yes' ) {
        print nbsp(1);
        print "<input type='radio' name='project_type_option' value='combined'";
        print " />Re-assembly, Combined Assembly\n";
        print nbsp(1);
        print "<input type='radio' name='project_type_option' value='extract'";
        print " />Genome Extracted from Metagenome\n";

        #	}
    } else {
        print "<p>Project type: \n";
        print nbsp(3);
        print
          "<input type='radio' name='project_type_option' value='metagenome'";
        print " checked";
        print " />Metagenome\n";

        #	if ( $isAdmin eq 'Yes' ) {
        print nbsp(1);
        print "<input type='radio' name='project_type_option' value='combined'";
        print " />Re-assembly, Combined Assembly\n";

        #	}
    }

    print "<p>Project search option: \n";
    print nbsp(3);
    print "   <input type='radio' name='proj_search_option' value='keyword'";
    print " checked";
    print " />Keyword Search\n";
    print nbsp(1);
    print "   <input type='radio' name='proj_search_option' value='query'";
    print " />Query Search\n";

    #    if ( $isAdmin eq 'Yes' ) {
    print nbsp(1);
    print "   <input type='radio' name='proj_search_option' value='gold_ap'";
    print " />AP ID\n";
    print nbsp(1);
    print
"<input type='text' name='input_ap_id' value='' size='20' maxLength='40'/>";

    #    }

    print "</p>\n";

    if ( $database eq 'IMG ER' ) {
        print "<h4>Submit Isolate or Single Cell Genome to IMG ER</h4>\n";
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_NewSubmission:submitToER" value="Submit Dataset to IMG ER" class="medbutton" />';

#	print nbsp(1);
#	print '<input type="submit" name="_section_ERSubmission:exportProjectList_noHeader" value="Export Project List" class="medbutton" />';
    }

    if ( $database eq 'IMG/M ER' ) {
        print "<h4>Submit Metagenome to IMG/M ER</h4>\n";
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_NewSubmission:submitMToM" value="Submit Metagenome Dataset to IMG/M ER" class="lgbutton" />';
    }

    printHomeLink();

    print end_form();
}

############################################################################
# ShowNewApSubmissionMainPage: only accept AP submissions
############################################################################
sub ShowNewApSubmissionMainPage {

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }
    my $isAdmin = getIsAdmin($contact_oid);

    print "<h1>New Submission</h1>\n";

#    print "<p><font color='red'>New submission is temporarily unavailable due to file system problem. Please check back later. Sorry for the inconvenience.</font>\n";
#    print end_form();
#    return;

    printNewGoldMsg();

    my $database = param1('database');
    $database =~ s/%20/ /g;

    my $apId = param1('apId');
    print hiddenVar( 'proj_search_option', 'gold_ap' );
    print "<p><b>AP ID: </b>\n";
    print nbsp(1);
    print
"<input type='text' name='input_ap_id' value='$apId' size='20' maxLength='40'/>";

    print "</p>\n";

    print
'<p><input type="submit" name="_section_NewSubmission:submitToER" value="Submit Dataset to IMG" class="medbutton" />';

    printHomeLink();

    print end_form();
}

sub printNewGoldMsg() {
    my $new_gold_link = alink( $new_gold_url, "GOLD" );
    my $new_msg       = qq{
        As per the new four level classification system implemented by GOLD 
        for genome and metagenome projects, all future submissions to IMG 
        will be based on GOLD Analysis Projects. 
        <font color='red'>All IMG submissions now require an 
        Analysis Project in GOLD. </font>
        Please go to $new_gold_link to define an Analysis Project. 
        Refer to the GOLD's latest publication
        (<a href='https://doi.org/10.1093/nar/gkaa983'>https://doi.org/10.1093/nar/gkaa983</a>)
        describing the four level classification system and/or the help 
        document in how to define a new project and obtain analysis 
        project id from GOLD.<br/>
        };

    printHint3($new_msg);
}

############################################################################
# ShowNewSubmissionSection {
############################################################################
sub ShowNewSubmissionSection {
    my ( $contact_oid, $isAdmin, $database ) = @_;

    print "<h3>New Submission</h3>\n";

    #    if ( $isAdmin eq 'Yes' ) {
    #	print "<p><font color='red'>Analysis project submission is still " .
    #	    "in testing mode. It's available for super users only.</font>\n";
    #    }

    if ( $database eq 'IMG ER' ) {
        print "<p>Project type: \n";
        print nbsp(3);
        print "<input type='radio' name='project_type_option' value='isolate'";
        print " checked";
        print " />Isolate Genome\n";
        print nbsp(1);
        print
          "<input type='radio' name='project_type_option' value='single_cell'";
        print " />Single Cell";

        #	if ( $isAdmin eq 'Yes' ) {
        print nbsp(1);
        print "<input type='radio' name='project_type_option' value='combined'";
        print " />Re-assembly, Combined Assembly\n";
        print nbsp(1);
        print "<input type='radio' name='project_type_option' value='extract'";
        print " />Genome Extracted from Metagenome\n";

        #	}
    } else {
        print "<p>Project type: \n";
        print nbsp(3);
        print
          "<input type='radio' name='project_type_option' value='metagenome'";
        print " checked";
        print " />Metagenome\n";

        #	if ( $isAdmin eq 'Yes' ) {
        print nbsp(1);
        print "<input type='radio' name='project_type_option' value='combined'";
        print " />Re-assembly, Combined Assembly\n";

        #	}
    }

    print "<p>Project search option: \n";
    print nbsp(3);
    print "   <input type='radio' name='proj_search_option' value='keyword'";
    print " checked";
    print " />Keyword Search\n";
    print nbsp(1);
    print "   <input type='radio' name='proj_search_option' value='query'";
    print " />Query Search\n";

    if ( $isAdmin eq 'Yes' ) {
        print nbsp(1);
        print
          "   <input type='radio' name='proj_search_option' value='gold_ap'";
        print " />AP ID\n";
        print nbsp(1);
        print
"<input type='text' name='input_ap_id' value='' size='20' maxLength='40'/>";
    }

    print "</p>\n";

    if ( $database eq 'IMG ER' ) {
        print "<h4>Submit Isolate or Single Cell Genome to IMG ER</h4>\n";
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_ERSubmission:submitToER" value="Submit Dataset to IMG ER" class="meddefbutton" />';

#	print nbsp(1);
#	print '<input type="submit" name="_section_ERSubmission:exportProjectList_noHeader" value="Export Project List" class="medbutton" />';
    }

    if ( $database eq 'IMG/M ER' ) {
        print "<h4>Submit Metagenome to IMG/M ER</h4>\n";
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_MSubmission:submitMToM" value="Submit Metagenome Dataset to IMG/M ER" class="lgbutton" />';

#	print "<h4>Submit Genome to IMG/M ER</h4>\n";
#	print "&nbsp; \n";
#	print '<input type="submit" name="_section_MSubmission:submitGToM" value="Submit Genome Dataset to IMG/M ER" class="lgbutton" />';
    }
}

############################################################################
# ShowNewSubmissionSection2 {
############################################################################
sub ShowNewSubmissionSection2 {
    my ( $contact_oid, $isAdmin ) = @_;

    print hr();
    print "<h3>New Submission</h3>\n";
    print qq{
	<p>Please select a project or a sample to submit.
	    If you do not find any existing project to submit,
	    then go to the Projects tab to create your own new project.
	    For metagenome submission, you should also go to the Samples
	    tab to enter sample information.</p>
	};

    if ( $isAdmin ne 'Yes' ) {
        print " You can submit any of your own projects or GOLD projects.";
    }
    print "</p>\n";

    # select project
    my $cond = "";
    if ( $isAdmin ne 'Yes' ) {
        $cond =
            " where p.contact_oid = $contact_oid"
          . " or p.gold_stamp_id is not null "
          . " or p.project_oid in (select cpp.project_permissions from"
          . " contact_project_permissions cpp where cpp.contact_oid = $contact_oid)";
    }

    my $dbh = WebFunctions::Connect_IMG;

    my $sql = qq{ 
        select p.project_oid, p.display_name
	    from project_info p
	    $cond
            order by p.display_name, p.project_oid
        };

webLog("510-- $sql\n");        
        
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    print "<h4>Select a project to submit:</h4>";
    print "&nbsp; &nbsp; &nbsp;\n";
    print "<select name='project_oid' class='img' size='1'>\n";

    print "    <option value=''></option>\n";
    for ( my $j = 0 ; $j <= 10000000000 ; $j++ ) {
        my ( $proj_id, $proj_name ) = $cur->fetchrow_array();
        if ( !$proj_id ) {
            last;
        }

        print
          "    <option value='$proj_id'>$proj_name (ID: $proj_id)</option>\n";
    }

    $cur->finish();

    #    $dbh->disconnect();

    print "</select>\n";
    print "<p/>\n";

#    print '<input type="submit" name="_section_Submission:submitProject" value="Select Project" class="medbutton" />';
    print
'<input type="submit" name="_section_Submission:signAgreement" value="Select Project 1" class="medbutton" />';
    print "&nbsp; \n";
    print
'<input type="submit" name="_section_Submission:viewProjectInfo" value="View Project Info" class="medbutton" />';

    # submit sample
    $cond = " where e.project_info is not null";
    if ( $isAdmin ne 'Yes' ) {
        $cond .= " and e.contact = $contact_oid ";
    }

    #    my $dbh=WebFunctions::Connect_IMG;

    $sql = qq{ 
        select e.sample_oid, e.sample_display_name
	    from env_sample e
	    $cond
            order by e.sample_display_name, e.sample_oid
        };

webLog("520-- $sql\n");

    $cur = $dbh->prepare($sql);
    $cur->execute();

    print
"<h4>Select a sample to submit: (Only samples that are associated with a project can be submitted.)</h4>";
    print "&nbsp; &nbsp; &nbsp;\n";
    print "<select name='sample_oid' class='img' size='1'>\n";

    print "    <option value=''></option>\n";
    for ( my $j = 0 ; $j <= 10000 ; $j++ ) {
        my ( $s_id, $s_name ) = $cur->fetchrow_array();
        if ( !$s_id ) {
            last;
        }

        print "    <option value='$s_id'>$s_name (ID: $s_id)</option>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "</select>\n";
    print "<p/>\n";

#    print '<input type="submit" name="_section_Submission:submitSample" value="Submit Sample" class="medbutton" />';
    print
'<input type="submit" name="_section_NewSubmission:signAgreement" value="Submit Sample" class="medbutton" />';
}

############################################################################
# ShowSubmissionButtons
############################################################################
sub ShowSubmissionButtons {
    my ( $isAdmin, $database ) = @_;

    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }

    my $contact_oid = getContactOid();
    my $canUpdate   = getCanUpdateSubmission($contact_oid);

    #    if ( $isAdmin eq 'Yes' ) {
    if ($canUpdate) {
        print
"<p>(<b>Note:</b> Check/Update Status and Update IMG Contact(s) functions only apply to the first selected submission.)<br/>\n";
        print
"<input type='submit' name='_section_$section:checkStatus' value='Check / Update Status' class='smbutton' />";
        print "&nbsp; \n";
        print
"<input type='submit' name='_section_$section:updateContact' value='Update IMG Contact(s)' class='smbutton' />";
    } else {
        print
"<p>(<b>Note:</b> Check Status only applies to the first selected submission.)<br/>\n";
        print
"<input type='submit' name='_section_$section:checkStatus' value='Check Status' class='smbutton' />";
    }

    print "&nbsp; \n";
    print
"<input type='submit' name='_section_$section:cancelSubmission' value='Cancel Submissions' class='smbutton' />";

#    print "&nbsp; \n";
#    print "<input type='submit' name='_section_$section:filterSubmission' value='Filter Submissions' class='smbutton' />";

#    if ( $isAdmin eq 'Yes' ) {
#	print "&nbsp; \n";
#	print "<input type='submit' name='_section_$section:exportList_noHeader' value='Export List' class='smbutton' />";
#    }
}

############################################################################
# ShowGoldAPSubmissionProject
############################################################################
sub ShowGoldAPSubmissionProject {
    my ( $database, $p_type ) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

#    if ( $isAdmin ne 'Yes' ) {
#	print "<h4>Error: You do not have permission to submit analysis projects.</h4>\n";
#	print end_form();
#	return;
#    }

    my $input_ap_id = param('input_ap_id');
    if ( !$input_ap_id ) {
        print "<h4>Error: No AP ID is provided.</h4>\n";
        print end_form();
        return;
    }
    my $res = getGoldAnalysisProject($input_ap_id);

    if ( !$res || blankStr($res) ) {
        print "<h4>Error: Incorrect GOLD AP ID.</h4>\n";
        print end_form();
        return;
    }

    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin ne 'Yes' ) {

        # check permission
        my $role = WebFunctions::getGoldAPRole( $contact_oid, $input_ap_id );
        if ( !$role ) {
            print
"<h4>Error: Cannot find AP ID $input_ap_id. (Either the GOLD AP does not exist, or the AP was created using a different JGI SSO account. Make sure that the JGI SSO is the same as the email you used to register your IMG account.)</h4>\n";
            print end_form();
            return;
        }
    }

    my $def_project = def_Analysis_Project();
    my $decode      = decode_json($res);
    my @keys        = ( keys %$decode );

    print "<h1>Submit GOLD Analysis Project</h1>\n";

    my $err_msg    = "";
    my $gold_ap_id = $decode->{'goldId'};
    if ( !$gold_ap_id ) {
        print "<h4>Error: This project does not have GOLD AP ID.</h4>\n";
        print end_form();
        return;
    }

    print hiddenVar( 'gold_ap_id',        $gold_ap_id );
    print hiddenVar( 'select_gold_ap_id', $gold_ap_id );

##    my $ap_domain = $decode->{'goldDomain'};
    my $ap_domain = $decode->{'domain'};
    if ( !$ap_domain ) {
        $ap_domain = $decode->{'goldDomain'};
    }
    my $ap_name     = $decode->{'analysisProjectName'};
    my $ncbi_tax_id = $decode->{'ncbiTaxonId'};

    print hiddenVar( 'gold_ap_id', $gold_ap_id );
    print hiddenVar( 'ap_domain',  $ap_domain );
    print hiddenVar( 'ap_name',    $ap_name );
    if ($ncbi_tax_id) {
        print hiddenVar( 'ncbi_tax_id', $ncbi_tax_id );
    }

    my $genome_type = $decode->{'genomeType'};
    my $specimen    = $decode->{'specimen'};
    if ( !$genome_type ) {
        if ( $specimen eq 'Organism' ) {
            $genome_type = 'genome';
        } elsif ( $specimen eq 'Biome' ) {
            $genome_type = 'metagenome';
        }
    }
    if ( !$genome_type ) {
        if ( uc($ap_domain) eq 'MICROBIAL' ) {
            $genome_type = 'metagenome';
        } else {
            $genome_type = 'genome';
        }
    }

    if ( $genome_type eq 'metagenome' ) {
        $database = 'IMG/M ER';
    } else {
        $database = 'IMG ER';
    }
    print hiddenVar( 'database', $database );

    print "<table class='img' border='1'>\n";

    print "<tr class='img' >\n";

    print "<td class='img' bgcolor='lightblue'>" . "GOLD AP ID</td>\n";
    my $link =
        "<a href='"
      . $main_cgi
      . "?section=ProjectInfo&page=analysisProject"
      . "&analysis_project_id=$gold_ap_id' >"
      . $gold_ap_id . "</a>";
    print "<td class='img'>" . $link . "</td>\n";

    print "</tr>\n";

    print "<tr class='img' >\n";
    print "<td class='img' bgcolor='lightblue'>"
      . "Analysis Project Name</td>\n";
    print "<td class='img'>" . $ap_name . "</td>\n";
    print "</tr>\n";

    my $ap_type = $decode->{'goldAnalysisProjectType'};
    if ($ap_type) {
        print "<tr class='img' >\n";
        print "<td class='img' bgcolor='lightblue'>"
          . "Analysis Project Type</td>\n";
        print "<td class='img'>" . $ap_type . "</td>\n";
        print "</tr>\n";
    }

    if ($genome_type) {
        print "<tr class='img' >\n";
        print "<td class='img' bgcolor='lightblue'>" . "Genome Type</td>\n";
        print "<td class='img'>" . $genome_type . "</td>\n";
        print "</tr>\n";
    } else {
        $err_msg = "This analysis project does not have genome type.";
    }

    print "<tr class='img' >\n";
    print "<td class='img' bgcolor='lightblue'>" . "Domain</td>\n";
    print "<td class='img'>" . $ap_domain . "</td>\n";
    print "</tr>\n";
#    if ( $ap_domain eq 'EUKARYAL' && $isAdmin ne 'Yes' ) {
#        $err_msg = "IMG cannot handle Eukaryal submissions at this moment.";
#    }

    if ($ncbi_tax_id) {
        print "<tr class='img' >\n";
        print "<td class='img' bgcolor='lightblue'>" . "NCBI Taxon ID</td>\n";
        print "<td class='img'>" . $ncbi_tax_id . "</td>\n";
        print "</tr>\n";
    }

    my $img_oid = $decode->{'imgTaxonOid'};
    if ($img_oid) {
        print "<tr class='img' >\n";
        print "<td class='img' bgcolor='lightblue'>" . "IMG Taxon OID</td>\n";
        print "<td class='img'>" . $img_oid . "</td>\n";
        print "</tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    if ($img_oid) {
        $err_msg = "This analysis project is already associated with "
          . "a genome in the IMG database.";
    }

    # check required fields
    if ( $genome_type eq 'genome' ) {
        if ( !$ncbi_tax_id ) {
            $err_msg = "NCBI Taxon ID is missing.";
        } elsif ( !$ap_domain ) {
            $err_msg = "Domain information is missing.";
        } elsif ( $ap_domain eq 'MICROBIAL' ) {
            $err_msg = "Domain is incorrect.";
        }
    } else {
        my $eco = $decode->{'ecosystem'};
        if ( !$eco || lc($eco) eq 'unclassified' ) {
            $err_msg = "Ecosystem information is missing.";
        }
    }

    if ($err_msg) {
        print "<h5>You cannot submit this GOLD analysis project: " . $err_msg
          . "</h5>\n";
        print end_form();
        return;
    }

    my $p_val       = $decode->{'projects'};
    my $ref_gold_id = $decode->{'referenceGoldId'};
    if ($ref_gold_id) {

        # ok -- it has reference GOLD AP
    } elsif ( !$p_val ) {
        $err_msg =
"This analysis project is not associated with any sequencing project(s) or reference GOLD analysis project.";
    } else {
        my @arr = @$p_val;
        if ( scalar(@arr) == 0 ) {
            $err_msg =
"This analysis project is not associated with any sequencing project(s) or reference GOLD analysis project.";
        } else {
            $err_msg = checkGoldGp( $p_val, $genome_type );
        }
    }

    if ($err_msg) {
        print "<h5>You cannot submit this GOLD analysis project: " . $err_msg
          . "</h5>\n";
        print end_form();
        return;
    }

    ## check submissions
    $err_msg = checkApSubmissions($gold_ap_id);

    if ($err_msg) {
        print "<h5>$err_msg</h5>\n";
        print end_form();
        return;
    }

#    if ( $isAdmin eq 'Yes' && $ap_domain eq 'EUKARYAL' ) {
#        print "<p><input type='checkbox' ";
#        print "name='special_euk' value='1' />\n";
#	print nbsp(1) . "This is a special Eukaryal genome submission. (for super users only)<br/>\n";
#	print "<p>\n";
#    }

    print
'<input type="submit" name="_section_ERSubmission:signAgreement" value="Submit Analysis Project" class="medbutton" />';
    print end_form();
}

sub checkApSubmissions {
    my ($gold_ap_id) = @_;

    my $err_msg = "";

    ## check submissions
    my $dbh = Connect_IMG_Contact();

    my $sql = qq{ 
       select s.submission_id, s.status, cv.cv_term, cv.cv_type, s.img_taxon_oid
       from submission s, submission_statuscv cv
       where s.analysis_project_id = ?
       and s.status = cv.term_oid
   };
   

webLog("530-- $sql\n");   
   
    my $cur = $dbh->prepare($sql);
    $cur->execute($gold_ap_id);

    for ( ; ; ) {
        my ( $s_id, $status, $cv_status, $cv_type, $img_oid ) =
          $cur->fetchrow();
        last if !$s_id;

        if (
            $cv_type eq 'canceled'
            ||

            #	     $cv_type eq 'error' ||
            #	     $status == 4 || $status == 5 || $status == 14 ||
            #	     $status == 16 || $status == 23 || $status == 24 ||
            #	     $status == 30 || $status == 32 || $status == 44 ||
            $status == 90
          )
        {
            # failed or canceled submission: ok
            next;
        }

        $err_msg = "This analysis project has already been submitted "
          . "as Submission $s_id with status: $cv_status";
        if ($img_oid) {
            $err_msg .= " (IMG Taxon OID: $img_oid)";
        }
        last;
    }
    $cur->finish();

    if ($err_msg) {
        $dbh->disconnect();
        return $err_msg;
    }

    $sql =
"select gold_id, img_dataset_id from gold_analysis_project where gold_id = ? and img_dataset_id is not null";
    
webLog("540-- $sql\n");
    
    $cur = $dbh->prepare($sql);
    $cur->execute($gold_ap_id);
    my ( $id2, $img_dataset_id ) = $cur->fetchrow();
    $cur->finish();

    if ( $id2 && $img_dataset_id ) {
        $err_msg = "This analysis project is already associated with "
          . "a genome in the IMG database.";
    }

    $dbh->disconnect();

    return $err_msg;
}

sub checkGoldGp {
    my ( $p_val, $genome_type ) = @_;

    for my $p2 (@$p_val) {
        my $gold_id = $p2->{goldId};

        if ( !$gold_id ) {
            next;
        }

        my $res = getNewGoldMetadata($gold_id);
        if ( !$res || blankStr($res) ) {
            return "Cannot find the sequencing project $gold_id.";
        }

        my $decode = decode_json($res);

        #	if ( ! $decode->{'seqMethod'} ) {
        #	    return "Sequencing method information is missing in $gold_id.";
        #	}

        my $val3 = $decode->{'sequencingCenters'};
        if ( !$val3 ) {
            return "Sequencing center information is missing in $gold_id.";
            my @arr3 = @$val3;
            if ( scalar(@arr3) == 0 ) {
                return "Sequencing center information is missing in $gold_id.";
            }
        }

        #	if ( $genome_type eq 'metagenome' &&
        #	     (! $decode->{'latitude'} || ! $decode->{'longitude'}) ) {
        #	    return "Latitude/longitude information is missing in $gold_id.";
        #	}

        #	if ( $genome_type eq 'metagenome' && ! $decode->{'geoLocation'} ) {
        #	    return "Geo location information is missing in $gold_id.";
        #	}

        #	if ( $genome_type eq 'metagenome' && ! $decode->{'isolation'} ) {
        #	    return "Sample isolation information is missing in $gold_id.";
        #	}
    }

    return "";
}

############################################################################
# getCanUpdateSubmission
############################################################################
sub getCanUpdateSubmission {
    my ($contact_oid) = @_;
    return 0 if $contact_oid eq "";

    if ( getIsAdmin($contact_oid) eq 'Yes' ) {
        return 1;
    }

    my $dbh = Connect_IMG_Contact();

    my $sql = qq{ 
       select img_editing_level 
       from contact 
       where contact_oid = $contact_oid 
   };
   
webLog("550-- $sql\n");   
   
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($editing_level) = $cur->fetchrow();
    $cur->finish();
    $dbh->disconnect();

    if ( !$editing_level ) {
        return 0;
    } elsif ( $editing_level =~ /img\-submit/ ) {
        return 1;
    }

    return 0;
}

############################################################################
# printHint2 - Print hint box with message.
############################################################################
sub printHint2 {
    my ($txt) = @_;

#    print "<div id='hint' style='border: 2px solid rgb(0, 0, 0); width: 150px; height: 50px; overflow: auto; width: 600px;'>\n";
    print "<div style='bgcolor: lightblue; width: 600px;'>\n";
    print "<table>\n";
    print "<tr><td>\n";
    print "<img src='$base_url/images/hint.gif' "
      . "width='67' height='32' alt='Hint' />";
    print "</td>\n";
    print "<td>\n";
    print "<font color='darkblue'><i>";
    print $txt;
    print "</i></font>\n";
    print "</td></tr>\n";
    print "</table>\n";
    print "</div>\n";

    #    print "<div class='clear'></div>\n";
}

############################################################################
# printHint3 - Print hint box with message.
############################################################################
sub printHint3 {
    my ($txt) = @_;

    #    print "<div id='hint'>\n";
    print
"<div id='hint' style='border: 1px solid rgb(100, 100, 255); width: 200px; overflow: auto; width: 700px; font-family: Arial, Helvetica, sans-serif; font-style: italic; font-size: .75em;'>\n";

    print "<tr>\n";
    print "<th>\n";
    print "<img src='$base_url/images/hint.gif' "
      . "width='56' height='22' alt='Hint' />";
    print "</th>\n";
    print "<td align='left'><i>\n";
    print $txt;
    print "</i></td>\n";
    print "</tr>\n";

    print "</div>\n";
    print "<div class='clear'></div>\n";
}

###########################################################################
# NewSubmission (v5)
###########################################################################
sub NewSubmission {
    my ($submit_type) = @_;

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    if ( !$submit_type ) {
        return;
    }

    my $isAdmin   = getIsAdmin($contact_oid);
    my $isJgiUser = getIsJgiUser($contact_oid);

    #if ( $isAdmin eq 'Yes' ) {
        NewSubmission_v52($submit_type);
        return;
    #}

    # get Submission definition
    my $def_submission = def_Submission();
    my @attrs          = @{ $def_submission->{attrs} };

    #generate a form
    if ( $def_submission->{multipart_form} ) {
        print start_multipart_form(
                                    -name   => 'mainForm',
                                    -method => 'post',
                                    -action => "$section_cgi"
        );
    } else {
        print start_form(
                          -name   => 'mainForm',
                          -method => 'post',
                          action  => "$section_cgi"
        );
    }

    print
"<script type=\"text/javascript\" src=\"$HELPBALLOON/lib/prototype/prototype.js\"></script>\n";

#	<title>HelpBalloon.js 2.0 Examples</title>
#	<link rel="stylesheet" type="text/css" href="$HELPBALLOON/doc/assets/style.css"></link>

    print <<EOF;
	<script type="text/javascript" src="$HELPBALLOON/lib/scriptaculous/scriptaculous.js"></script>
	<script type="text/javascript" src="$HELPBALLOON/src/HelpBalloon.js"></script>
	<script type="text/javascript">
	<!--
	//
	// Override the default settings to point to the parent directory
	//
	HelpBalloon.Options.prototype = Object.extend(HelpBalloon.Options.prototype, {
	  icon: '$HELPBALLOON/images/icon.gif',
	  button: '$HELPBALLOON/images/button.png',
	  balloonPrefix: '$HELPBALLOON/images/balloon-'
	  });
    
    //-->
	</script>

EOF

    my $special_euk = param1('special_euk');
    if ($special_euk) {
        print hiddenVar( 'special_euk', $special_euk );
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    if ( $database eq 'IMG/M ER' ) {
        print "<h1>New Metagenome Dataset Submission</h1>\n";

#	print "<p>Submit isolate genome or metagenome for inclusion into IMG/M ER system.</p>\n";
        print "<p>Submit metagenome for inclusion into IMG/M ER system.</p>\n";
        print hiddenVar( 'database', $database );
    } elsif ( $database eq 'IMG ER' ) {
        print "<h1>New Genome Dataset Submission</h1>\n";
        if ($special_euk) {
            print
"<p>Submit Euk genome through special data loading pipeline for inclusion into IMG ER system.</p>\n";
        } else {
            print
"<p>Submit isolate or single cell genome for inclusion into IMG ER system. <u>Please note that we only accept fasta files.</u></p>\n";
        }
        print hiddenVar( 'database', $database );
    } else {

        # shouldn't happen (this is the old version)
        print "<h1>New $submit_type Submission</h1>\n";
        print
"<p>Submit isolate genome to IMG ER database, and metagenome to IMG/M ER database.</p>\n";
    }

    print "<p>All fields marked with (*) are required fields.</p>\n";

    saveSubmissionPageInfo();

    my $project_oid = '';
    my $sample_oid  = '';
    my @samples;

    # project oid
    my $gold_ap_id = param1('gold_ap_id');
    if ($gold_ap_id) {
        print hiddenVar( 'gold_ap_id', $gold_ap_id );
    }

    $project_oid = param1('project_oid');
    if ( blankStr($project_oid) && blankStr($gold_ap_id) ) {
        print "<h4>Error: No project has been selected.</h4>\n";
        print end_form();
        return;
    }
    print hiddenVar( 'project_oid', $project_oid );

    if ( $submit_type eq 'Sample' && !$gold_ap_id ) {

        # sample oid
        @samples = param('sample_oid');
        if ( scalar(@samples) == 0 ) {
            print "<h4>Error: No Sample has been selected.</h4>\n";
            print end_form();
            return;
        }
        $sample_oid = $samples[0];
        for my $s2 (@samples) {
            print hiddenVar( 'sample_oid', $s2 );
        }
    }

    # show additional project info
    my $pname2   = param1('ap_name');
    my $gold_id2 = $gold_ap_id;

    my $dbh = Connect_IMG();
    my $sql;
    my $cur;
    if ( $project_oid && blankStr($gold_ap_id) ) {
        $sql =
"select display_name, gold_stamp_id from project_info where project_oid = $project_oid";

webLog("560-- $sql\n");

        $cur = $dbh->prepare($sql);
        $cur->execute();
        ( $pname2, $gold_id2 ) = $cur->fetchrow_array();
        $cur->finish();
    }

    my $sname2 = "Sample Name";
    if ( scalar(@samples) > 1 ) {
        $sname2 =
          $pname2 . ": combined assembly " . scalar(@samples) . " samples";
    } elsif ($sample_oid) {
        $sql =
"select sample_display_name from env_sample where sample_oid = $sample_oid";

webLog("570-- $sql\n");

        $cur = $dbh->prepare($sql);
        $cur->execute();
        ($sname2) = $cur->fetchrow_array();

        #	if ( ! blankStr($sname2) ) {
        #	    print "<h5>Submission Sample ($sample_oid): " .
        #		escapeHTML($sname2) . "</h5>\n";
        #	}
        $cur->finish();
    }
    $dbh->disconnect();

#    print "<h5>Note: You can submit compressed data files with .gz or .zip file extension.</h5>\n";

    if ($gold_ap_id) {
        print hiddenVar( 'species_code', $gold_ap_id );
        print hiddenVar( 'project_info', $gold_ap_id );
    }

    if ( $database eq 'IMG/M ER' ) {
        print hiddenVar( 'gene_calling_method',  'Metagenome Gene Calling' );
        print hiddenVar( 'default_gene_calling', 'Metagenome Gene Calling' );
    } else {
        print hiddenVar( 'gene_calling_method', 'Isolate Genome Gene Calling' );
        print hiddenVar( 'default_gene_calling',
                         'Isolate Genome Gene Calling' );
    }

    print hiddenVar( 'img_product_flag', 'Yes' );
    print hiddenVar( 'img_ec_flag',      'Yes' );

    # tab view
    # remove Advanced options per Marcel (2/17/16)
    # remove Functional annotation options per Marcel (1/17/17)
    TabHTML::printTabAPILinks("newSubmission");
    my @tabIndex = (
        "#tab1",
##		     "#tab2",
        "#tab3"
    );
##                     "#tab4" );
##                     "#tab5" );

    my @tabNames = (
        'Submission Information',
##		     'Submit annotated file',
        'Submit sequence file'
    );
##		     'Functional annotation options' );
##		     'Advanced options' );

    if ( $database eq 'IMG/M ER' ) {

        # we don't allow genbank submission for metagenomes
        @tabIndex = ( "#tab1", "#tab2" );
##                    "#tab3" );
##		      "#tab4" );

        @tabNames = ( 'Submission Information', 'Submit sequence file' );
##		      'Functional annotation options' );
##		      'Advanced options' );
    }

    if ($special_euk) {
        @tabIndex = ("#tab1");
        @tabNames = ('Submission Information');
    }

    TabHTML::printTabDiv( "newSubmission", \@tabIndex, \@tabNames );

    # additional data files for metagenome
    #    if ( $database eq 'IMG/M ER' ) {
    #	push @tabs, ( 'Additional Metagenome Files' );
    #    }

    my $show_project_name = 1;

    my $tab_cnt = 0;
    for my $tab (@tabNames) {
        if ( $special_euk && $tab ne 'Submission Information' ) {
            next;
        }

        $tab_cnt++;
        my $tab_id = "tab" . $tab_cnt;

        print "<div id='" . $tab_id . "'><p>\n";

        if ( $tab eq 'Submission Information' ) {
            my $new_text = qq{
		Please provide general information about your submitted project.
		    IMG sequencing status and the topology of the replicons 
		    can be related to the Finishing Level according
		    to Chain et al. (Science  9 October 2009:Vol. 326 no. 5950 pp. 236-237) 
		    using the following table. 

		    <table class='img'>
		    <th class='img'>Finishing level per Chain et al.</th>
		    <th class='img'>IMG Seq status</th>
		    <th class='img'>Topology</th>
		    <tr class='img'><td class='img'>Standard Draft (SD)</td>
		    <td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		    <tr><td class='img'>High Quality Draft (HQD)</td>
		    <td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		    <tr><td class='img'>Improved High Quality Draft (IHQD)</td>
		    <td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		    <tr><td class='img'>Annotation Directed Improvement (ADI)</td>
		    <td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		    <tr><td class='img'>Non-contiguous Finished (NF)</td>
		    <td class='img'>Permanent Draft</td><td class='img'>circular</td></tr>
		    <tr><td class='img'>Finished (F)</td>
		    <td class='img'>Finished</td><td class='img'>circular</td></tr>
		    </table>

		    If you have reasons to believe that this correspondence
		    does not apply to this project (e.g., you have a project that is 
		    Finished but the replicons are linear),
		    use the corresponding options to set Sequencing status and Topology
		    at your own risk.
		  <br/><b>Remember: With great power comes great responsibility.</b>
	    };

            if ( $database eq 'IMG/M ER' ) {
                $new_text =
"Please provide general information about your submitted project.";
            }

            printHint3($new_text);
        } elsif ( $tab eq 'Submit annotated file' ) {
            printHint3(
" Please submit either a <b><u>genbank</u></b> or <b><u>embl</u></b> format file. <u>Compressed files</u> (.gz or .zip) are also accepted. You can either submit a file or type an ftp or http URL with the location of the file. After filling the information in this section, you can skip 'Submit sequence file' section and go to the 'Functional annotation options' section."
            );
        } elsif ( $tab eq 'Submit sequence file' ) {
            printHint3(
"Please submit a <b><u>fasta</u></b> format file. <u>Compressed files</u> (.gz or .zip) are also accepted."
            );
            my $new_locus_tag_note = qq{
                  For datasets submitted for IMG annotation and integration, 
                  IMG issues native (automatically generated) locus tags. 
                  Please note that:

                  <ul>
                  <li>IMG locus tags are different from and therefore do not 
                      match NCBI issued locus tags.</li>
                  <li>IMG locus tags cannot be replaced for datasets processed 
                      using IMG's annotation and integration pipelines.</li>
                  <li>Before submission to Genbank, IMG datasets need to be 
                      prepared following NCBI locus tag and other 
                      format requirements.</li>
                  <ul>
                };

            #	    printHint3($new_locus_tag_note);
        } elsif ( $tab eq 'Functional annotation options' ) {
            printHint3(
"Default option is for IMG to compute both product name and E.C. number. If you change these options to 'No,' then product name and E.C. number will not be computed."
            );
        }
##	elsif ( $tab eq 'Advanced options' ) {
##	    printHint3( "Change these options only if you want to change the standard behavior of the pipeline.");
##	}

        print "<table class='img' border='1'>\n";

     #        print "<tr class='img' >\n";
     #        print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
     #            "<font color='darkblue'>" . $tab . "</font></th>\n";
     #        print "  <td class='img'   align='left' bgcolor='lightblue'>";

        #	print "</td>\n";
        #        print "</tr>\n";

        for my $k (@attrs) {
            if ( $k->{tab} ne $tab ) {
                next;
            }

            #	    if ( $k->{name} eq 'jgi_project_id' ) {
            # hide this one per Kostas' request
            #		next;
            #	    }

            if (    $k->{name} eq 'project_info'
                 || $k->{name} eq 'species_code'
                 || $k->{name} eq 'gene_calling_method' )
            {
                # hide this one per Marcel's request
                next;
            }

            if ( $tab eq 'Submission Information' && $show_project_name ) {

                # show project name
                print "<tr class='img' >\n";
                print "  <th class='subhead' align='right'>";
                print "Submission project</th>\n";

                print "  <td class='img'   align='left'>\n";
                print escapeHTML($pname2);
                print "</td>\n";

                # show sample name
                if ( $submit_type eq 'Sample' ) {
                    print "<tr class='img' >\n";
                    print "  <th class='subhead' align='right'>";
                    print "ER submission sample</th>\n";

                    print "  <td class='img'   align='left'>\n";
                    print escapeHTML($sname2);
                    print "</td>\n";
                }

                # show GOLD ID
                print "<tr class='img' >\n";
                print "  <th class='subhead' align='right'>";
                print "GOLD ID</th>\n";

                print "  <td class='img'   align='left'>\n";
                if ( $gold_id2 =~ /Ga/ ) {

                    # analysis project
                    my $link =
                        "<a href='"
                      . $main_cgi
                      . "?section=ProjectInfo&page=analysisProject"
                      . "&analysis_project_id=$gold_id2' >"
                      . $gold_id2 . "</a>";
                    print $link;
                } elsif ($gold_id2) {
                    my $gold_link = getGoldLink($gold_id2);
                    print $gold_link;
                }
                print "</td>\n";

                $show_project_name = 0;
            }

            if (    $tab eq 'Submit sequence file'
                 && $k->{name} eq 'species_code' )
            {
                # special case
                print "<tr class='img' >\n";
                print "  <th class='subhead' align='left'>";
                print "Gene calling method (default) ";
                print nbsp(1);
                print '<script type="text/javascript">';

                my $tooltip2 =
"Select the gene calling pipeline you want to use. Isolate gene calling pipelie is described here(URL to a web page within the IMG space). Use this option if you are submitting contigs longer than 1KB to IMG ER (i.e. isolate genome), or to IMG MER (in cases of reference isolate genomes or contigs predicted to belong to the same organism). Metagenome gene caling is described here (URL to a web page within the IMG space). Use this option if you submit metagenomic samples, either assembled or unassembled sequences or combinations of both.";
                print
"new HelpBalloon({ title: 'Gene calling method (default)', content: '"
                  . $tooltip2 . "', ";
                print "icon: '$base_url/images/information-balloon.png', \n";
                print
"iconStyle: { 'cursor': 'pointer', 'verticalAlign': 'middle' } }); ";
                print "</script> \n";
                print "</th>\n";
                print "  <td class='img'   align='left'>\n";
                print
                  "<select name='default_gene_calling' class='img' size='1'>\n";
                my @select2 = ('Isolate Genome Gene Calling');
                my $val2    = 'Isolate Genome Gene Calling';

                if ( $database eq 'IMG/M ER' ) {
                    $val2 = 'Metagenome Gene Calling';
                    push @select2, ($val2);
                }
                for my $s0 (@select2) {
                    print "    <option value='" . escapeHTML($s0) . "'";
                    if ( $s0 eq $val2 ) {
                        print " selected ";
                    }
                    print ">$s0</option>\n";
                }

                print "</select>\n";
                print "</td></tr>\n";
            }

            if (    $tab eq 'Submit sequence file'
                 && $k->{name} eq 'seq_coverage_file'
                 && $database ne 'IMG/M ER' )
            {
                ## skip for isolate
                next;
            }

            if (    $tab eq 'Submit sequence file'
                 && $k->{name} eq 'jgi_project_id' )
            {
                # special case
                print "<tr class='img' >\n";
                print "</tr>\n";
            }

            my $attr_val  = "";
            my $attr_name = $k->{name};
            my $disp_name = $k->{display_name};

            if (    $disp_name =~ /JGI/
                 && $disp_name =~ /only/
                 && $isAdmin ne 'Yes' )
            {
                # skip
                next;
            }

            if ( $attr_name eq 'gene_calling_flag' ) {
                $disp_name .= " (override)";
                $k->{tooltip} =
"Customize the gene calling method used. Use this option if you believe that the standard gene calling pipeline does not satisfy your needs.";
            }
            my $data_type = $k->{data_type};
            my $len       = $k->{length};

            # hide Illumina fields for now?
            if (   !$illumina
                 && $attr_name =~ 'unassembled\_illum' )
            {
                next;
            }

            # hide pacbio
            if ( $attr_name =~ 'unassembled\_pacbio' ) {
                next;
            }

            # hide unasselbled 454 for ER
            if (    $database eq 'IMG ER'
                 && $attr_name =~ 'unassembled\_454' )
            {
                next;
            }

            # skip non-editable for new
            if ( $submit_type eq 'Project' && $attr_name eq 'project_info' ) {
                if ($gold_ap_id) {
                    $attr_val = $gold_ap_id;
                } else {
                    $attr_val = $project_oid;
                }
            }

        #	    elsif ( $submit_type eq 'Sample' && $attr_name eq 'sample_oid' ) {
        #		$attr_val = $sample_oid;
        #	    }
            elsif (    $attr_name eq 'img_ec_flag'
                    || $attr_name eq 'img_product_flag'
                    || $attr_name eq 'cluster_seq'
                    || $attr_name eq 'quality_based_trim' )
            {
                # set default to yes
                $attr_val = 'Yes';

                # special for GEBA
                if ( $contact_oid == 3031 ) {
                    $attr_val = 'No';
                }
            } elsif ( $attr_name eq 'is_img_public' ) {

                # set default to no
                $attr_val = 'No';

                # special for GEBA
                if ( $contact_oid == 3031 ) {
                    $attr_val = 'Yes';
                }
            } elsif ( $attr_name eq 'img_dev_flag' ) {

                # not used anymore
                next;

                # this feature is for admin only
                if ( $isAdmin eq 'Yes' ) {

                    # set default to no
                    $attr_val = 'No';
                } else {
                    next;
                }
            } elsif ( $attr_name eq 'mol_topology' ) {

                # set default to linear
                $attr_val = 'linear';
            } elsif ( $attr_name eq 'remove_duplicate_flag' ) {

                # this feature is for admin only
                if ( $isAdmin eq 'Yes' ) {

                    # set default to yes
                    $attr_val = 'Yes';
                } else {
                    next;
                }
            } elsif ( !$k->{can_edit} ) {
                next;
            } elsif (    $attr_name eq 'stats_file'
                      || $attr_name eq 'error_file'
                      || $attr_name eq 'error_msg' )
            {
                # error file or message is used by admin only
                next;
            } elsif ( $attr_name eq 'admin_notes' ) {

                # admin notes is used by admin only
                next;
            } elsif ( $attr_name eq 'img_taxon_oid' ) {

                # IMG Taxon OID is used by admin only
                next;
            } elsif (    $attr_name eq 'approval_status'
                      || $attr_name eq 'approved_by'
                      || $attr_name eq 'approval_date' )
            {
                # approval status is used by admin only
                next;
            } elsif (    $attr_name eq 'binning_file'
                      || $attr_name eq 'bin_method' )
            {
                if ( $database ne 'IMG/M ER' ) {

                    # these are for IMG/M ER only
                    next;
                }
            }

            print "<tr class='img' >\n";
            print "  <th class='subhead' align='left' style='width:200px'>";

            if ( $k->{indent} ) {
                print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }

            if ( $k->{url} ) {
                print alink( $k->{url}, $disp_name, 'target', 1 );
            } elsif ( $k->{tooltip} ) {

                #		printTooltip($disp_name, $k->{tooltip});
                #		print '<a href="javascript: alert(' . "'" .
                #		    $k->{tooltip} . "'" . ')">';
                print escapeHTML($disp_name);

                #		print "</a>\n";
            } else {
                print escapeHTML($disp_name);
            }

            if ( $attr_name eq 'species_code' ) {

                # print "<br/>(* required if gene calling is needed) \n";
                if ($gold_ap_id) {
                    $attr_val = $gold_ap_id;
                }
            } elsif ( $attr_name eq 'gene_calling_flag' ) {

                # special case
            } elsif ( $k->{is_required} ) {
                print " (*) ";
            }

            if ( $k->{tooltip_file} ) {
                print nbsp(1);
                print '<script type="text/javascript">';
                print "new HelpBalloon({dataURL: '$base_url/doc/tooltip/"
                  . $k->{tooltip_file} . "', ";
                print "icon: '$base_url/images/information-balloon.png', \n";
                print
"iconStyle: { 'cursor': 'pointer', 'verticalAlign': 'middle' } }); ";
                print "</script> \n";
            } elsif ( $k->{tooltip} ) {
                print nbsp(1);
                print '<script type="text/javascript">';
                print "new HelpBalloon({ title: '$disp_name', content: '"
                  . $k->{tooltip} . "', ";
                print "icon: '$base_url/images/information-balloon.png', \n";
                print
"iconStyle: { 'cursor': 'pointer', 'verticalAlign': 'middle' } }); ";
                print "</script> \n";
            }

            print "</th>\n";

            # print attribute value
            print "  <td class='img'   align='left'>\n";

            # special for database
            if ( $attr_name eq 'database' && !blankStr($database) ) {
                my $show_select = 0;
                if ( $isAdmin eq 'Yes' && $show_select ) {
                    print
                      "<select name='target_db_name' class='img' size='1'>\n";
                    print "    <option value='$database'>$database</option>\n";

              #		    print "    <option value='IMG/M HMP'>IMG/M HMP</option>\n";
                    print "</select>\n";
                } else {
                    print escapeHTML($database);
                }
            } elsif ( $attr_name eq 'species_code' && $gold_ap_id ) {

                # special case for locus tag from AP
                print escapeHTML($gold_ap_id);
            } elsif ( $data_type =~ /\|/ ) {

                # selection
                my @selects = split( /\|/, $data_type );
                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( !$k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }
                for my $s0 (@selects) {
                    print "    <option value='" . escapeHTML($s0) . "'";
                    if ( !blankStr($attr_val) && $attr_val eq $s0 ) {
                        print " selected ";
                    }
                    print ">$s0</option>\n";
                }
                print "</select>\n";
            } elsif ( $data_type eq 'list' ) {

                # selection
                my @selects = split( /\|/, $k->{list_values} );
                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( !$k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }
                for my $s0 (@selects) {
                    print "    <option value='" . escapeHTML($s0) . "'";
                    if ( !blankStr($attr_val) && $attr_val eq $s0 ) {
                        print " selected ";
                    }
                    print ">$s0</option>\n";
                }
                print "</select>\n";
            } elsif ( $data_type eq 'file' ) {
                print
"<input type='file' name='$attr_name' size='120' maxlength='$len'"
                  . " />";
            } elsif ( $data_type eq 'cv2' ) {
                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( !$k->{is_required} ) {
                    print "   <option value=''> </option>\n";
                }

                my $sql2 = $k->{cv_query};
                my $dbh2 = Connect_IMG();
                
webLog("580-- $sql2\n");                
                
                my $cur2 = $dbh2->prepare($sql2);
                $cur2->execute();

                for ( my $j2 = 0 ; $j2 <= 10000 ; $j2++ ) {
                    my ( $id2, $name2 ) = $cur2->fetchrow_array();
                    if ( !$id2 ) {
                        last;
                    }

                    print "    <option value='" . escapeHTML($id2) . "'";
                    if ( !blankStr($attr_val) && $attr_val eq $id2 ) {
                        print " selected ";
                    }
                    print ">$id2 - $name2</option>\n";
                }
                print "</select>\n";
                $cur2->finish();
                $dbh2->disconnect();
            } elsif ( $data_type eq 'cv' && $k->{cv_query} ) {
                if ( blankStr($attr_val) ) {
                    $attr_val = 'No';
                }
                my @selects = db_getValues( $k->{cv_query} );

                #		if ( $attr_name eq 'gene_calling_flag' &&
                #		     $isAdmin eq 'Yes' ) {
                #		    push @selects, ( 'Glimmer', 'RNA gene calling' );
                #		}

                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( !$k->{is_required} || $attr_name eq 'gene_calling_flag' ) {
                    print "   <option value=''> </option>\n";
                }
                for my $s0 (@selects) {
                    if ( $attr_name eq 'gene_calling_flag' ) {
                        if ( $s0 eq 'No gene calling' ) {

                            # skip this one here
                            next;
                        }
                    }

                    print "    <option value='" . escapeHTML($s0) . "'";
                    if ( !blankStr($attr_val) && $attr_val eq $s0 ) {
                        print " selected ";
                    }
                    print ">$s0</option>\n";
                }

                print "</select>\n";
            } elsif ( $k->{can_edit} ) {
                my $size = 80;
                if ( $len && $size > $len ) {
                    $size = $len;
                }

                print "<input type='text' name='$attr_name' value='";
                if ( !blankStr($attr_val) ) {
                    print escapeHTML($attr_val);
                }
                print "' size='$size'" . " maxLength='$len'/>";
            } else {

                # display only
                if ( !blankStr($attr_val) ) {
                    print escapeHTML($attr_val);
                }
            }

            print "</td>\n";

            print "</tr>\n";

            # add JGI file name after 'file'
            my $len80    = 120;
            my $file_len = 255;

            #	    if ( $data_type eq 'file' && $isJgiUser eq 'Yes' ) {
            if ( $data_type eq 'file' ) {

                # admin user can specify file name on JGI file system
                print "<tr class='img' >\n";
                print "  <th class='subhead' align='left' style='width:200px'>";
                my $tt =
"Type the location of the (multi) fasta or (multi) genbank file using ftp, http, or in the UNIX NFS in JGI. The last option is available only for users that access this web page from the JGI intranet";

                #		printTooltip("Or, file name in JGI file system", $tt);

                #		print "JGI file name";
                print "</th>\n";

                print "  <td class='img'   align='left'>\n";
                if ( $isJgiUser eq 'Yes' ) {
                    print "URL (ftp or http) or JGI file name:";
                } else {
                    print "URL (ftp or http):";
                }
                print "&nbsp;&nbsp;&nbsp;";
                my $jname = "jgi_file:" . $attr_name;
                print "<input type='text' name='$jname' value=''";
                print " size='$len80'" . " maxLength='$file_len'/>";
                print "</td> </tr>\n";
            }
        }    # end for my k

        # covered in Submit sequence file
        #	if ( $tab eq 'Advanced options' ) {
        # show READS Files for editing
        #	    if ( $database eq 'IMG/M ER' ) {
        #		EditReadsFiles(0);
        #	    }
        #	}

        print "</table>\n";

        if ( $tab eq 'Submission Information' ) {
            print "<p>\n";
##	    if ( $database eq 'IMG/M ER' ) {
            printHint3(
"Go to 'Submit sequence file' section to submit you <b><u>fasta</u></b> format file(s)."
            );
##	    }
##	    else {
##		printHint3("Go to 'Submit annotated file' section to submit your <b><u>genbank</u></b> or <b><u>embl</u></b> format file, or go to 'Submit sequence file' section to submit you <b><u>fasta</u></b> format file(s).");
##	    }
        }

        print "</p></div>\n";
    }    # end for my tab

    print "<p>\n";

    if ($test_mode) {
        print "<font color='red'>Under Construction. DO NOT SUBMIT!</font>\n";
        print "</p>\n";
    }

    # super user can submit for others
    if ( $isAdmin eq 'Yes' ) {
        print "<p>Submit for (IMG Contact OID or JGI SSO User Name):\n";
        print "&nbsp; \n";
        print "<input type='text' name='submit_for_contact' value=''";
        print " size='60'" . " maxLength='80'/>";

##	print "<select name='submit_for_contact' class='img' size='1'>\n";
        print "</p>\n";
    }

    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:checkSubmission" value="Submit" class="smdefbutton" />';
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_MSubmission:showMPage" value="Cancel" class="smbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:checkSubmission" value="Submit" class="smdefbutton" />';
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_ERSubmission:showERPage" value="Cancel" class="smbutton" />';
    }

    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# NewSubmission (v5.2)
###########################################################################
sub NewSubmission_v52 {
    my ($submit_type) = @_;

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    if ( !$submit_type ) {
        return;
    }

    my $isAdmin   = getIsAdmin($contact_oid);
    my $isJgiUser = getIsJgiUser($contact_oid);

    # get Submission definition
    my $def_submission = def_Submission();
    my @attrs          = @{ $def_submission->{attrs} };

    #generate a form
    if ( $def_submission->{multipart_form} ) {
        print start_multipart_form(
                                    -name   => 'mainForm',
                                    -method => 'post',
                                    -action => "$section_cgi"
        );
    } else {
        print start_form(
                          -name   => 'mainForm',
                          -method => 'post',
                          action  => "$section_cgi"
        );
    }

    print
"<script type=\"text/javascript\" src=\"$HELPBALLOON/lib/prototype/prototype.js\"></script>\n";

#	<title>HelpBalloon.js 2.0 Examples</title>
#	<link rel="stylesheet" type="text/css" href="$HELPBALLOON/doc/assets/style.css"></link>

    print <<EOF;
	<script type="text/javascript" src="$HELPBALLOON/lib/scriptaculous/scriptaculous.js"></script>
	<script type="text/javascript" src="$HELPBALLOON/src/HelpBalloon.js"></script>
	<script type="text/javascript">
	<!--
	//
	// Override the default settings to point to the parent directory
	//
	HelpBalloon.Options.prototype = Object.extend(HelpBalloon.Options.prototype, {
	  icon: '$HELPBALLOON/images/icon.gif',
	  button: '$HELPBALLOON/images/button.png',
	  balloonPrefix: '$HELPBALLOON/images/balloon-'
	  });
    
    //-->
	</script>

EOF

    my $special_euk = param1('special_euk');
    if ($special_euk) {
        print hiddenVar( 'special_euk', $special_euk );
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    if ( $database eq 'IMG/M ER' ) {
        print "<h1>New Metagenome Dataset Submission</h1>\n";
        print "<p>Submit metagenome for inclusion into IMG/M ER system.</p>\n";
        print hiddenVar( 'database', $database );
    } elsif ( $database eq 'IMG ER' ) {
        print "<h1>New Genome Dataset Submission</h1>\n";
        print
"<p>Submit isolate or single cell genome for inclusion into IMG ER system.</p>\n";
        print hiddenVar( 'database', $database );
    }

    print
"<p>All fields marked with (*) are required fields. Mouse over the field name to find more information,</p>\n";

    saveSubmissionPageInfo();

    ## get GOLD AP ID
    my $gold_ap_id = param1('gold_ap_id');
    if ($gold_ap_id) {
        print hiddenVar( 'gold_ap_id', $gold_ap_id );
    } else {
        print "<h4>Error: No GOLD AP ID has been selected.</h4>\n";
        print end_form();
        return;
    }

    # show additional project info
    my $pname2   = param1('ap_name');
    my $gold_id2 = $gold_ap_id;

    if ($gold_ap_id) {
        print hiddenVar( 'species_code', $gold_ap_id );
        print hiddenVar( 'project_info', $gold_ap_id );
    }

    if ( $database eq 'IMG/M ER' ) {
        print hiddenVar( 'gene_calling_method',  'Metagenome Gene Calling' );
        print hiddenVar( 'default_gene_calling', 'Metagenome Gene Calling' );
    } else {
        print hiddenVar( 'gene_calling_method', 'Isolate Genome Gene Calling' );
        print hiddenVar( 'default_gene_calling',
                         'Isolate Genome Gene Calling' );
    }

    print hiddenVar( 'img_product_flag', 'Yes' );
    print hiddenVar( 'img_ec_flag',      'Yes' );

    my $hint_text =
"Please submit a <b><u>fasta</u></b> format file. <u>Compressed files</u> (.gz or .zip) are also accepted.";
    if ( $database eq 'IMG ER' ) {
        $hint_text .=
" You can also submit a GFF file with gene definition if you don't wish to use IMG gene calling.";
    }
    printHint3($hint_text);

    print "<table class='img' border='1'>\n";

    # show project name
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    print "Submission project</th>\n";

    print "  <td class='img'   align='left'>\n";
    print escapeHTML($pname2);
    print "</td></tr>\n";

    # show GOLD ID
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    print "GOLD ID</th>\n";

    print "  <td class='img'   align='left'>\n";
    if ( $gold_id2 =~ /Ga/ ) {

        # analysis project
        my $link =
            "<a href='"
          . $main_cgi
          . "?section=ProjectInfo&page=analysisProject"
          . "&analysis_project_id=$gold_id2' >"
          . $gold_id2 . "</a>";
        print $link;
    } elsif ($gold_id2) {
        my $gold_link = getGoldLink($gold_id2);
        print $gold_link;
    }
    print "</td></tr>\n";

    ## database
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    print "Database</th>\n";

    print "  <td class='img'   align='left'>\n";
    print escapeHTML($database);
    print "</td></tr>\n";

    ## is_img_public
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    my $text =
"Select Yes if the genome/sample you submit will be visible by anyone logged in IMG. If you select No, this genome/sample will be visible only by the submitter and other users that the submitter specifies.";
    print "<span title='$text'>Is public in IMG database (*)</span>\n";
    print "</th>\n";
    print "  <td class='img'   align='left'>\n";
    print "<select name='is_img_public' class='img' size='1'>\n";
    print "    <option value='No' selected >No</option>\n";
    print "    <option value='Yes' >Yes</option>\n";
    print "</select>\n";
    print "</td></tr>\n";

    ## seq_status
    #    print "<tr class='img' >\n";
    #    print "  <th class='subhead' align='right'>";
    #    print "Sequencing Status (*)</th>\n";
    #    print "  <td class='img'   align='left'>\n";
    #    print "<select name='seq_status' class='img' size='1'>\n";
    #    print "    <option value='Draft' selected >Draft</option>\n";
    #    if ( $database eq 'IMG ER' ) {
    #	print "    <option value='Finished' >Finished</option>\n";
    #	print "    <option value='Permanent Draft' >Permanent Draft</option>\n";
    #    }
    #    print "</select>\n";
    #    print "</td></tr>\n";

    ## mol_topology
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    my $text =
"Select circular if all the sequences in the submitted files correspond to circular replicons. In any other case leave this field as linear.";
    print "<span title='$text'>Topology (*)</span></th>\n";
    print "  <td class='img'   align='left'>\n";
    print "<select name='mol_topology' class='img' size='1'>\n";
    print "    <option value='linear' selected >linear</option>\n";

    if ( $database eq 'IMG ER' ) {
        print "    <option value='circular' >circular</option>\n";
    }
    print "</select>\n";
    print "</td></tr>\n";

    ## replace_taxon_oid
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    my $text =
"Provide the taxon_oid (a numeric identifier) that corresponds to an existing version of the same genome/sample in IMG ER/IMG MER, which will be replaced by the current submission. IMG will try to map existing manual annotation and curation from the old version to the new version of the dataset.";
    print "<span title='$text'>Replace Taxon OID</span>\n";
    print "</th>\n";
    print "  <td class='img'   align='left'>\n";
    print "<input type='text' name='replace_taxon_oid' value='' size='40'"
      . " maxLength='40'/>";
    print "</td></tr>\n";

    ## gene_calling_flag
    my $ap_domain = param1('ap_domain');
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>";
    my $text =
"Select Yes then IMG will perform gene calling on the fasta input file. Select No then IMG will use the gene definition in the required GFF file. IMG cannot perform gene calling on Eukaryal genomes. Metagenome submissions always require gene calling. ";
    print "<span title='$text'>IMG gene calling? (*)</span>\n";
    print "</th>\n";
    print "  <td class='img'   align='left'>\n";
    print "<select name='gene_calling_flag' class='img' size='1'>\n";

    if ( $ap_domain ne 'EUKARYAL' ) {
        print "    <option value='Yes' selected >Yes</option>\n";
    }
    if ( $database eq 'IMG ER' ) {
        print "    <option value='No' >No</option>\n";
    }
    print "</select>\n";
    print "</td></tr>\n";

    ## img_product_flag
    #    print "<tr class='img' >\n";
    #    print "  <th class='subhead' align='right'>";
    #    print "IMG product assignment? (*)</th>\n";
    #    print "  <td class='img'   align='left'>\n";
    #    print "<select name='img_product_flag' class='img' size='1'>\n";
    #    print "    <option value='Yes' selected >Yes</option>\n";
    #    if ( $database eq 'IMG ER' ) {
    #	print "    <option value='No' >No</option>\n";
    #    }
    #    print "</select>\n";
    #    print "</td></tr>\n";

    ## input files
    my @input_files = ('assembled_seq_file');
    if ( $database eq 'IMG ER' ) {
        push @input_files, ( 'gff_file', 'protein_faa_file' );
    } else {
        push @input_files, ('seq_coverage_file');
    }

    for my $file_type (@input_files) {
        my $label = "Assembled sequence file (*)";
        my $text =
"Provide a fasta (http://www.ncbi.nlm.nih.gov/BLAST/fasta.shtml)  file with the nucleotide sequences of the assembled contigs.";
        if ( $file_type eq 'seq_coverage_file' ) {
            $label = "Coverage file";
            $text =
"Provide a file with the coverage of each assembled sequence. The file should be tab delimited with the following two columns: 1. contig name (ID), and 2. the coverage for this contig (Avg_fold). We assume uniform coverage for contigs. Headers are required.";
        } elsif ( $file_type eq 'gff_file' ) {
            $label = "GFF file (required if no gene calling)";
            $text  = "Provide a GFF input file with gene definition.";
        } elsif ( $file_type eq 'protein_faa_file' ) {
            $label = "Protein faa file (required if no gene calling)";
            $text  = "Provide a protein faa file for protein coding genes.";
        }

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        print "<span title='$text'>$label</span></th>\n";
        print "  <td class='img'   align='left'>\n";
        
        #
        # 2021-09-03 - ken 
        # NEW issue Cloudfare and LBL net security max upload is now 500MB
        #        
        
        print qq{
          500MB file size limit.
          <br>  
        };
        print "<input type='file' name='$file_type' size='120' maxlength='255'"
          . " />";
        print "</td></tr>\n";

        # add JGI file name after 'file'
        my $len80    = 120;
        my $file_len = 255;
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='left' style='width:200px'>";
        my $tt =
"Type the location of the (multi) fasta or (multi) genbank file using ftp, http, or in the UNIX NFS in JGI. The last option is available only for users that access this web page from the JGI intranet";
        print "</th>\n";

        print "  <td class='img'   align='left'>\n";
        
        #
        # 2021-09-03 - ken 
        # NEW issue Cloudfare and LBL net security max upload is now 500MB
        #
        my $text500mb = qq{
(Recommended: for a zip file greater than <b>500+MB</b>)
        };
        
        if ( $isJgiUser eq 'Yes' ) {
            print "URL (ftp or http) or JGI file name: " . $text500mb;
        } else {
            print "URL (ftp or http): " . $text500mb;
        }
        print "&nbsp;&nbsp;&nbsp;";
        my $jname = "jgi_file:" . $file_type;
        print "<input type='text' name='$jname' value=''";
        print " size='$len80'" . " maxLength='$file_len'/>";
        print "</td> </tr>\n";
    }

    print "</table>\n";

    # super user can submit for others
    if ( $isAdmin eq 'Yes' ) {
        print "<p>Submit for (IMG Contact OID or JGI SSO User Name):\n";
        print "&nbsp; \n";
        print "<input type='text' name='submit_for_contact' value=''";
        print " size='60'" . " maxLength='80'/>";

##	print "<select name='submit_for_contact' class='img' size='1'>\n";
        print "</p>\n";
    }

    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:checkV52Submission" value="Submit" class="smdefbutton" />';
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_MSubmission:showMPage" value="Cancel" class="smbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:checkV52Submission" value="Submit" class="smdefbutton" />';
        print "&nbsp; \n";
        print
'<input type="submit" name="_section_ERSubmission:showERPage" value="Cancel" class="smbutton" />';
    }

    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";
    printHomeLink();

    print end_form();
}

#########################################################################
# CheckSubmission
#########################################################################
sub CheckSubmission {

    #generate a form
    print start_form( -method => 'post', -action => "$section_cgi" );

    print "<h1>Check Submission</h1>\n";

    saveSubmissionPageInfo();

    my $copy_to_storage = 0;

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    #    print "<p>Species code: " . param1('species_code');
    my $msg = "";

    my $contact_oid        = getContactOid();
    my $submit_for_contact = param1('submit_for_contact');
    if ($submit_for_contact) {
        my $dbh = WebFunctions::Connect_IMG;
        my $sql = "";
        if ( isInt($submit_for_contact) ) {
            ## IMG contact oid
            $sql = "select contact_oid from contact where contact_oid = ?";
        } else {
            ## JGI SSO
            $sql =
"select contact_oid from contact where lower(caliban_user_name) = ?";
            $submit_for_contact = lc($submit_for_contact);
        }


webLog("590-- $sql\n");

        my $cur = $dbh->prepare($sql);
        $cur->execute($submit_for_contact);
        my ($cid) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();
        if ( !$cid ) {
            $msg =
              "Incorrect contact information in 'Submit for' was provided.";
            print "<h4>Error: $msg </h4>";
            print end_form();
            return;
        }

        if ( !isInt($submit_for_contact) ) {
            $submit_for_contact = $cid;
        }
    } else {
        $submit_for_contact = $contact_oid;
    }

    my @notes = ();

    my $db_option = param1('database');
    $db_option =~ s/%20/ /g;
    my $gold_ap_id  = param1('gold_ap_id');
    my $project_oid = param1('project_oid');
    my @samples     = param('sample_oid');
    my $sample_oid  = 0;
    if ( scalar(@samples) > 0 ) {
        $sample_oid = $samples[0];
    }
    if ( blankStr($project_oid) && blankStr($gold_ap_id) ) {
        $msg = "No Project ID has been provided.";
        print "<h4>Error: $msg </h4>";
        print end_form();
        return;
    }

    # metagenome project must have samples
    if (    $db_option eq 'IMG/MI'
         && scalar(@samples) == 0
         && blankStr($gold_ap_id) )
    {
        $msg = "No Sample ID has been provided.";
        print "<h4>Error: $msg </h4>";
        print end_form();
        return;
    }

    my $isAdmin     = getIsAdmin($contact_oid);
    my $special_euk = param1('special_euk');
    if ( $isAdmin ne 'Yes' ) {
        $special_euk = 0;
    }

    if ($gold_ap_id) {
        $msg = checkApSubmissions($gold_ap_id);
        if ($msg) {
            print "<h4>Error: $msg </h4>";
            print end_form();
            return;
        }
    }

    # get submission definition
    my $def_subm = def_Submission();
    my @attrs    = @{ $def_subm->{attrs} };

    # check input
    my $sub_id = 3;
    if ( !$test_mode ) {

        # get the real submission id
        $sub_id = db_findMaxID( 'submission', 'submission_id' ) + 1;
    }

    my %upload_file_name;
    my %use_jgi_file;
    my %new_file_name;

    # check whether it's genbank or fasta submission
    my $genbank_submit = 0;
    my $fasta_submit   = 0;

    # check genbank file name
    my $genbank_filename = param1('genbank_file');
    if ( blankStr($genbank_filename) ) {
        my $n2 = param1('jgi_file:genbank_file');
        if ( !blankStr($n2) ) {

            # use JGI file
            $upload_file_name{'genbank_file'} = $n2;
            $use_jgi_file{'genbank_file'}     = 1;
            $new_file_name{'genbank_file'}    = $n2;
            $genbank_submit                   = 1;
        }
    } else {

        # file upload
        $upload_file_name{'genbank_file'} = $genbank_filename;
        $genbank_submit                   = 1;
        $new_file_name{'genbank_file'}    = $genbank_filename;
    }

    # check fasta file names
    for my $file2 (
                    'assembled_seq_file',   'reads_for_assembly',
                    'unassembled_454_file', 'unassembled_illumina_file',
                    'unassembled_pacbio_file'
      )
    {
        if ( !blankStr( param1($file2) ) ) {

            # file upload
            $upload_file_name{$file2} = param1($file2);
            $fasta_submit = 1;
        } else {
            my $n2 = "jgi_file:" . $file2;
            if ( !blankStr( param1($n2) ) ) {

                # use JGI file
                $upload_file_name{$file2} = param1($n2);
                $use_jgi_file{$file2}     = 1;
                $new_file_name{$file2}    = param1($n2);
                $fasta_submit             = 1;
            }
        }
    }

    # check additional files
    for my $file2 ( 'seq_coverage_file', 'binning_file' ) {
        if ( !blankStr( param1($file2) ) ) {

            # file upload
            $upload_file_name{$file2} = param1($file2);
        } else {
            my $n2 = "jgi_file:" . $file2;
            if ( !blankStr( param1($n2) ) ) {

                # use JGI file
                $upload_file_name{$file2} = param1($n2);
                $use_jgi_file{$file2}     = 1;
                $new_file_name{$file2}    = param1($n2);
            }
        }
    }

    if ( $genbank_submit && $fasta_submit ) {
        $msg = "You cannot submit both genbank/embl file and fasta file(s).";
        print "<h4>Error: $msg </h4>";
        print end_form();
        return;
    } elsif ( !$genbank_submit && !$fasta_submit && !$special_euk ) {
        $msg =
"No file name is provided. You must submit at least one genbank, embl or fasta file.";
        print "<h4>Error: $msg </h4>";
        print end_form();
        return;
    }

    # check attribute values
    for my $k (@attrs) {
        if ($special_euk) {
            next;
        }

        if ( !$k->{can_edit} ) {
            next;
        }

        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};
        my $v         = param1($attr_name);

        if (    $attr_name eq 'genbank_file'
             || $attr_name eq 'assembled_seq_file'
             || $attr_name eq 'reads_for_assembly'
             || $attr_name eq 'unassembled_454_file'
             || $attr_name eq 'unassembled_illumina_file'
             || $attr_name eq 'unassembled_pacbio_file'
             || $attr_name eq 'seq_coverage_file'
             || $attr_name eq 'binning_file' )
        {
            # already checked
            next;
        }

        if ( $attr_name eq 'img_dev_flag' ) {

            # don't check
            next;
        }

        if ( $attr_name eq 'remove_duplicate_flag' ) {

            # don't check
            next;
        }

        if ( $attr_name eq 'gene_calling_flag' ) {

            # check at the end
            next;
        }

        if ( $k->{is_required} && blankStr($v) ) {
            $msg = "Please enter a value in " . $k->{display_name} . ".";
            print "<h4>Error: $msg </h4>";
            print end_form();
            return;
        }

        if ( !blankStr($v) ) {
            if ( $data_type eq 'int' ) {
                if ( !isInt($v) ) {
                    $msg = $k->{display_name} . " must be an integer.";
                    print "<h4>Error: $msg </h4>";
                    print end_form();
                    return;
                }
            } elsif ( $data_type eq 'number' ) {
                if ( !isNumber($v) ) {
                    $msg = $k->{display_name} . " must be a number.";
                    print "<h4>Error: $msg </h4>";
                    print end_form();
                    return;
                }
            } elsif ( $data_type eq 'file' ) {
                my $upload_name = $upload_file_name{$attr_name};
                if ( !blankStr($upload_name) ) {
                    $v = $upload_name;
                }

                my $new_name = validPathName($v);
                if ( $new_name ne $v ) {
                    $msg = $k->{display_name} . " contains illegal characters.";
                    print "<h4>Error: $msg </h4>";
                    
            my $dump = longmess();
            print qq{
                <pre>
                ========== 2222
                
                $dump
                
                ===========
                </<pre>>
            };
                    
                    
                    print end_form();
                    return;
                }
            }
        }
    }    # end for k

    # check gene calling
    my $gc  = param1('default_gene_calling');
    my $gc0 = param1('gene_calling_flag');
    if ( $genbank_submit || $special_euk ) {
        $gc = "No gene calling";
    }
    if ($gc0) {

        # user override
        $gc = $gc0;
    }
    push @notes, ("Use gene calling method: $gc");

    # do not allow gene calling for Euk
    if ( $gc ne 'No' && $gc ne 'No gene calling' ) {
        my $domain = getDomain( $project_oid, $gold_ap_id );
        if ( $domain eq 'EUKARYAL' ) {
            print
"<h4>Error: IMG cannot perform gene calling on Eukaryal genomes.</h4>\n";
            print end_form();
            return;
        }
    }

    # check locus tag prefix
    if ($special_euk) {
        ## no need to check
    } elsif ( param1('species_code') ) {

        # has code
        if ( checkLocusTag( param1('species_code') ) ) {

            # correct locus tag prefix
        } else {
            print
"<h4>Error: The locus_tag prefix must be 3-12 alphanumeric characters and the first character may not be a digit. Do not put '_' at the end of the prefix.</h4>\n";
            print end_form();
            return;
        }
    }

#    else {
#	# check gene calling
#	if ( $gc ne 'No' && $gc ne 'Unknown' && $gc ne 'No gene calling' ) {
#	    print "<h4>Error: Please specify unique species code / locus_tag prefix. (Gene calling method: $gc)</h4>";
#	    print end_form();
#	    return;
#	}
#    }

    # all file upload
    for my $file3 (
                    'genbank_file',              'assembled_seq_file',
                    'reads_for_assembly',        'unassembled_454_file',
                    'unassembled_illumina_file', 'unassembled_pacbio_file',
                    'seq_coverage_file',         'binning_file'
      )
    {
        # get upload file name
        my $filename = $upload_file_name{$file3};
        if ( blankStr($filename) ) {

            # no file
            next;
        }

        push @notes, ("$file3: $filename");

        my $newname = $filename;

        # print "<p>$file3: use JGI file? " . $use_jgi_file{$file3} . "\n";

        my $copy_file_name = "";
        if ( !$use_jgi_file{$file3} ) {

            # file upload
            my $env           = WebEnv::getEnv();
            my $max_file_size = $env->{max_file_size};

            my $s2 = $sub_id . "_" . $contact_oid;
            if ( $s2 =~ /(\d+)\_(\d+)/ ) {
                if ($fasta_submit) {
                    $copy_file_name = $file3 . "_" . $1 . "_" . $2 . ".fna";
                    $copy_file_name =~ /([a-zA-Z0-9\_\.\-\/]+)/;
                    $newname = $env->{upload_dir} . $copy_file_name;
                } else {
                    $copy_file_name = $file3 . "_" . $1 . "_" . $2 . ".gb";
                    $copy_file_name =~ /([a-zA-Z0-9\_\.\-\/]+)/;
                    $newname = $env->{upload_dir} . $copy_file_name;
                }
            }

            ## untaint
            $newname =~ /([a-zA-Z0-9\_\.\-\/]+)/;
            if ( !open( FILE, '>', $newname ) ) {
                $msg = "Cannot open tmp $file3 file $newname.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            my $line_no = 0;
            my $line;
            while ( $line = <$filename> ) {

                # we don't want to process large files
                if ( $line_no <= $max_file_size ) {
                    print FILE $line;
                } else {
                    last;
                }

                $line_no++;
            }
            close(FILE);

            if ( $line_no > $max_file_size ) {
                $msg = "File is too large -- exceeding $max_file_size lines.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            if ( $line_no == 0 ) {
                $msg = "Empty data file -- file upload failed.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            # copy the file over?
            if ($copy_to_storage) {
                my $copy_url = $env->{copy_sync_url} . $copy_file_name;
                my $ua       = new LWP::UserAgent();
                my $req      = GET($copy_url);
                my $res2     = $ua->request($req);
                my $code     = $res2->code;

                $new_file_name{$file3} = $env->{storage_dir} . $copy_file_name;
            } else {

                # no copy
                $new_file_name{$file3} = $env->{upload_dir} . $copy_file_name;
            }

            my $file_size = fileSize( $new_file_name{$file3} );
            if ( !$file_size ) {
                $msg = "Empty data file -- file upload failed.";
                if ($copy_to_storage) {
                    $msg = "Empty data file -- file copy failed.";
                }
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }
        } else {

            # use JGI file
            if ( !blankStr($newname) ) {
                if ( $newname =~ /^http\:\/\// || $newname =~ /^https\:\/\// || $newname =~ /^ftp\:\/\// ) {
# bug fix https url - ken

                    # don't need to check http or ftp
                } elsif ( -e $newname ) {

                    # file exists
                    push @notes, ("Use JGI file: $newname");
                } else {
                    $msg = "File $newname does not exist.";
                    print "<h4>Error: $msg </h4>";
                    print end_form();
                    return;
                }
            } else {
                $msg = "Please specify a file name.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }
        }
    }

    #    $msg = "File $upload_file_name exists.";
    #    print "<h4>Error: $msg </h4>";
    #    print end_form();
    #    return;

    # get default submission status
    my $submit_status = 1;    # pending for validation
    if ($test_mode) {
        $submit_status = 5;
    }

    #    my $gc_flag = param1('gene_calling_flag');
    my $gc_flag = $gc;

    #    if ( !blankStr($gc_flag) && $gc_flag ne 'No' &&
    #	 $gc_flag ne 'Unknown' && $gc_flag ne 'No gene calling' ) {
    # check submission type instead of gene calling method
    if ($fasta_submit) {
        $submit_status = 50;    # pending for annotation
        if ($test_mode) {
            $submit_status = 4;
        }
    }

    if ($special_euk) {
        $submit_status = 45;
    }

    my $sql;
    my @sqlList = ();

    # enter a submission entry
    my $is_jgi      = 0;
    my $dbh         = WebFunctions::Connect_IMG;
    my $project_fld = 'project_info';
    if ($gold_ap_id) {
        $project_fld = 'analysis_project_id';
        if ( isJgiAnalysis( $dbh, $gold_ap_id ) ) {
            $is_jgi = 1;
        }
    }

    if ( blankStr($project_oid) && !blankStr($sample_oid) ) {
        $project_oid = db_getValue(
            "select project_info from env_sample where sample_oid = $sample_oid"
        );
    }

    if ( !$is_jgi ) {
        if ($sample_oid) {
            $is_jgi = isJgiSample( $dbh, $sample_oid );
        } elsif ($project_oid) {
            $is_jgi = isJgiProject( $dbh, $project_oid );
        }
    }

    for my $s2 (@samples) {
        if ( !$is_jgi ) {
            $is_jgi = isJgiSample( $dbh, $s2 );
        }
    }
    $dbh->disconnect();

    my $ins =
        "insert into submission (submission_id, "
      . "is_jgi, $project_fld, "
      . "sample_oid, contact, submission_date, status, approval_status";
    my $vals = "values ($sub_id, ";
    if ($is_jgi) {
        $vals .= "'Yes', ";
    } else {
        $vals .= "'No', ";
    }
    if ($gold_ap_id) {
        $vals .= "'$gold_ap_id'";
    } elsif ( blankStr($project_oid) ) {
        $vals .= "null";
    } else {
        $vals .= $project_oid;
    }
    if ( blankStr($sample_oid) || $gold_ap_id ) {
        $vals .= ", null";
    } else {
        $vals .= ", " . $sample_oid;
    }
    $vals .= ", $submit_for_contact, sysdate, $submit_status, 'pending review'";

    for my $k (@attrs) {
        if ($special_euk) {
            next;
        }

        if ( !$k->{can_edit} ) {
            next;
        }

        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};
        my $v         = param1($attr_name);

        if ( $attr_name eq 'database' ) {
            if ( param('target_db_name') ) {
                $v = param('target_db_name');
            }
        }

        if (    $attr_name eq 'genbank_file'
             || $attr_name eq 'assembled_seq_file'
             || $attr_name eq 'reads_for_assembly'
             || $attr_name eq 'unassembled_454_file'
             || $attr_name eq 'unassembled_illumina_file'
             || $attr_name eq 'unassembled_pacbio_file'
             || $attr_name eq 'seq_coverage_file'
             || $attr_name eq 'binning_file' )
        {
            $v = $new_file_name{$attr_name};
            if ( $test_mode > 1 ) {
                print "<p>New file name for $attr_name is: $v\n";
            }
        }

        if (    $attr_name eq 'unassembled_454_quality_trim'
             && $genbank_submit )
        {
            # skip this one
            next;
        }

        if ( $attr_name eq 'img_dev_flag' ) {
            if ( blankStr($v) ) {
                $v = 'No';
            }
        }

        if ( $attr_name eq 'remove_duplicate_flag' ) {
            if ( blankStr($v) ) {
                $v = 'Yes';
            }
        }

        if ( $attr_name eq 'gene_calling_flag' ) {
            $v = $gc;
        }

        # rule requested by Marcel
        if ( $attr_name eq 'permute' ) {
            my $subm_seq_status = param1('seq_status');
            my $subm_topology   = param1('mol_topology');
            if (    lc($subm_seq_status) eq 'finished'
                 && lc($subm_topology) eq 'circular' )
            {
                $v = 'Yes';
            }
        }

        if ( $k->{is_required} && blankStr($v) ) {
            $msg = "Please enter a value in " . $k->{display_name} . ".";
            print "<h4>Error: $msg </h4>";
            print end_form();
            return $msg;
        }

        if ( !blankStr($v) ) {
            $ins .= ", " . $attr_name;
            if ( $data_type eq 'int' ) {
                if ( !isInt($v) ) {
                    $msg = $k->{display_name} . " must be an integer.";
                    print "<h4>Error: $msg </h4>";
                    print end_form();
                    return $msg;
                }
                $vals .= ", " . $v;
            } elsif ( $data_type eq 'number' ) {
                if ( !isNumber($v) ) {
                    $msg = $k->{display_name} . " must be a number.";
                    print "<h4>Error: $msg </h4>";
                    print end_form();
                    return $msg;
                }
                $vals .= ", " . $v;
            } elsif ( $data_type eq 'file' ) {
                my $new_name = validPathName($v);
                if (    $use_jgi_file{$attr_name} == 0
                     && $new_name ne $v )
                {
                    $msg = $k->{display_name} . " contains illegal characters.";
                    print "<h4>Error: $msg </h4>";
                    
            my $dump = longmess();
            print qq{
                <pre>
                ========== 33333
                
                $dump
                
                ===========
                </<pre>>
            };                    
                    
                    print end_form();
                    return $msg;
                }

                $v =~ s/'/''/g;    # replace ' with '';
                $vals .= ", '" . $v . "'";
            } else {
                $v =~ s/'/''/g;    # replace ' with '';
                $vals .= ", '" . $v . "'";
            }
        }
    }    # end for k

    $sql = $ins . ") " . $vals . ")";

    push @sqlList, ($sql);

    # insert into the new submission_samples table
    if ( blankStr($gold_ap_id) ) {
        for my $s2 (@samples) {
            $sql = "insert into submission_samples (submission_id, sample_oid) "
              . "values ($sub_id, $s2)";
            push @sqlList, ($sql);
        }
    }

    # insert into submission_img_contacts table
    $sql = "insert into submission_img_contacts "
      . "(submission_id, img_contacts) values ($sub_id, $submit_for_contact)";
    push @sqlList, ($sql);

    # special for GEBAPCCs and PCCGEBA
    if ( $submit_for_contact == 100438 ) {
        $sql = "insert into submission_img_contacts "
          . "(submission_id, img_contacts) values ($sub_id, 100256)";
        push @sqlList, ($sql);
        $sql = "insert into submission_img_contacts "
          . "(submission_id, img_contacts) values ($sub_id, 3014)";
        push @sqlList, ($sql);
    } elsif ( $submit_for_contact == 100256 ) {
        $sql = "insert into submission_img_contacts "
          . "(submission_id, img_contacts) values ($sub_id, 3014)";
        push @sqlList, ($sql);
    }

    # insert reads file, if any
    for my $file4 ('reads_for_assembly') {
        if ( $new_file_name{$file4} ) {
            my $n = $new_file_name{$file4};
            $n =~ s/'/''/g;    # replace ' with ''
            $sql =
                "insert into submission_reads_file ("
              . "submission_id, reads_file) values ("
              . $sub_id . ", '"
              . $n . "')";
            push @sqlList, ($sql);
        }
    }

    #    $test_mode = 3;

    if ($test_mode) {

        # show SQL but don't update
        print "<p>Test mode\n";
        for my $sql2 (@sqlList) {
            print "<p>SQL: $sql2\n";
        }

        if ( $test_mode < 2 ) {
            db_sqlTrans( \@sqlList );
        }
    } else {
        db_sqlTrans( \@sqlList );
    }

    print
"<h4>New submission is added to the database (submission ID: $sub_id)</h4>\n";
    for my $s3 (@notes) {
        print "<p>$s3</p>\n";
    }

    if ( $db_option eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:showMPage" value="OK" class="smbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:showERPage" value="OK" class="smbutton" />';
    }

    print "<p>\n";
    printHomeLink();

    print end_form();

}

#########################################################################
# CheckV52Submission (for v.5.2)
#########################################################################
sub CheckV52Submission {

    #generate a form
    print start_form( -method => 'post', -action => "$section_cgi" );

    print "<h1>Check Submission</h1>\n";

    saveSubmissionPageInfo();

    my $copy_to_storage = 0;

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    my $msg = "";

    my $contact_oid        = getContactOid();
    my $submit_for_contact = param1('submit_for_contact');
    if ($submit_for_contact) {
        my $dbh = WebFunctions::Connect_IMG;
        my $sql = "";
        if ( isInt($submit_for_contact) ) {
            ## IMG contact oid
            $sql = "select contact_oid from contact where contact_oid = ?";
        } else {
            ## JGI SSO
            $sql =
"select contact_oid from contact where lower(caliban_user_name) = ?";
            $submit_for_contact = lc($submit_for_contact);
        }

webLog("600-- $sql\n");

        my $cur = $dbh->prepare($sql);
        $cur->execute($submit_for_contact);
        my ($cid) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();
        if ( !$cid ) {
            $msg =
              "Incorrect contact information in 'Submit for' was provided.";
            print "<h4>Error: $msg </h4>";
            print end_form();
            return;
        }

        if ( !isInt($submit_for_contact) ) {
            $submit_for_contact = $cid;
        }
    } else {
        $submit_for_contact = $contact_oid;
    }

    my @notes = ();

    my $isAdmin = getIsAdmin($contact_oid);

    ## check GOLD AP ID
    my $db_option = param1('database');
    $db_option =~ s/%20/ /g;
    my $gold_ap_id = param1('gold_ap_id');

    if ( blankStr($gold_ap_id) ) {
        $msg = "No GOLD Analysis Project ID has been provided.";
        print "<h4>Error: $msg </h4>";
        print end_form();
        return;
    } else {
        $msg = checkApSubmissions($gold_ap_id);
        if ($msg) {
            print "<h4>Error: $msg </h4>";
            print end_form();
            return;
        }
    }

    # check input
    my $sub_id = 3;
    if ( !$test_mode ) {

        # get the real submission id
        $sub_id = db_findMaxID( 'submission', 'submission_id' ) + 1;
    }

    my %upload_file_name;
    my %use_jgi_file;
    my %new_file_name;

    # check whether it's gff or fasta submission
    my $is_img_public     = param1('is_img_public');
    my $gene_calling_flag = param1('gene_calling_flag');
    my $img_product_flag  = param1('img_product_flag');

    my @file_types = ('assembled_seq_file');
    if ( $database eq 'IMG ER' && $gene_calling_flag eq 'No' ) {
        push @file_types, ( 'gff_file', 'protein_faa_file' );
    }

    # check required files
    for my $file2 (@file_types) {
        if ( !blankStr( param1($file2) ) ) {

            # file upload
            $upload_file_name{$file2} = param1($file2);
        } else {
            my $n2 = "jgi_file:" . $file2;
            if ( !blankStr( param1($n2) ) ) {

                # use JGI file
                $upload_file_name{$file2} = param1($n2);
                $use_jgi_file{$file2}     = 1;
                $new_file_name{$file2}    = param1($n2);
            }
        }
    }

    # check additional files, if any
    for my $file2 ('seq_coverage_file') {
        
        if ( !blankStr( param1($file2) ) ) {

            # file upload
            $upload_file_name{$file2} = param1($file2);
            push @file_types, ('seq_coverage_file');
        } else {
            my $n2 = "jgi_file:" . $file2;
            if ( !blankStr( param1($n2) ) ) {

                # use JGI file
                $upload_file_name{$file2} = param1($n2);
                $use_jgi_file{$file2}     = 1;
                $new_file_name{$file2}    = param1($n2);
                push @file_types, ('seq_coverage_file');
            }
        }
    }


#            print "<pre>\n";
#            print Dumper \%upload_file_name;
#            print "\n";
#            print Dumper \%use_jgi_file;
#            print "\n";
#            print Dumper \%new_file_name;
#            print "\n";
#            print "</pre>\n";

    # check attribute values
    ## check replace_taxon_oid
    my $replace_taxon_oid = param1('replace_taxon_oid');
    if ( $replace_taxon_oid && !isInt($replace_taxon_oid) ) {
        $msg = "Replacement Taxon OID must be an integer.";
        print "<h4>Error: $msg </h4>";
        print end_form();
        return;
    }

    ## check all upload file names
    for my $attr_name (@file_types) {
        my $upload_name = $upload_file_name{$attr_name};
        my $v           = "";
        if ( !blankStr($upload_name) ) {
            $v = $upload_name;
        }

#print "filename == $v<br>\n";

# bug fix https url - ken
if ($v =~ /^http\:\/\// || $v =~ /^https\:\/\// || $v =~ /^ftp\:\/\//) {
    #print "its a url next one<br>\n";
    next;
}

        my $new_name = validPathName($v);
        if ( $new_name ne $v ) {
            $msg = $attr_name . " contains illegal characters.";
            print "<h4>Error: (4) $msg </h4>";
            
#            my $dump = longmess();
#            print qq{
#                <pre>
#                ========== 44444
#                $v
#                
#                $dump
#                
#                ===========
#                </pre>
#            };
            
            print end_form();
            return;
        }
    }    # end for attr

    # all file upload
    for my $file3 (@file_types) {

        # get upload file name
        my $filename = $upload_file_name{$file3};
        #my $urlname = $use_jgi_file{$file3};
        if ( blankStr($filename) ) {

            # no file
            if ( $file3 eq 'assembled_seq_file' ) {
                $msg = "Assembled sequence file is required.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            if ( $gene_calling_flag eq 'No' && $file3 eq 'gff_file' ) {
                $msg = "If select no gene calling, then GFF file is required.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }
            if ( $gene_calling_flag eq 'No' && $file3 eq 'protein_faa_file' ) {
                $msg =
"If select no gene calling, then protein faa file is required.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }
            next;
        }

        push @notes, ("$file3: $filename");

        my $newname = $filename;

        # print "<p>$file3: use JGI file? " . $use_jgi_file{$file3} . "\n";

        my $copy_file_name = "";
        if ( !$use_jgi_file{$file3} ) {

            # file upload
            my $file_ext = "fna";
            if ( $file3 eq 'gff_file' ) {
                $file_ext = "gff";
            } elsif ( $file3 eq 'protein_faa_file' ) {
                $file_ext = "faa";
            } elsif ( $file3 eq 'seq_coverage_file' ) {
                $file_ext = "depth";
            }

            my $env           = WebEnv::getEnv();
            my $max_file_size = $env->{max_file_size};

            my $s2 = $sub_id . "_" . $contact_oid;
            if ( $s2 =~ /(\d+)\_(\d+)/ ) {
                $copy_file_name =
                  $file3 . "_" . $1 . "_" . $2 . "." . $file_ext;
                $copy_file_name =~ /([a-zA-Z0-9\_\.\-\/]+)/;
                $newname = $env->{upload_dir} . $copy_file_name;
            }

            ## untaint
            $newname =~ /([a-zA-Z0-9\_\.\-\/]+)/;
            if ( !open( FILE, '>', $newname ) ) {
                $msg = "Cannot open tmp $file3 file $newname.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            my $line_no = 0;
            my $line;
            while ( $line = <$filename> ) {

                # we don't want to process large files
                if ( $line_no <= $max_file_size ) {
                    print FILE $line;
                } else {
                    last;
                }

                $line_no++;
            }
            close(FILE);

            if ( $line_no > $max_file_size ) {
                $msg = "File is too large -- exceeding $max_file_size lines.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            if ( $line_no == 0 ) {
                $msg = "Empty data file -- file upload failed.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }

            # copy the file over?
            if ($copy_to_storage) {
                my $copy_url = $env->{copy_sync_url} . $copy_file_name;
                my $ua       = new LWP::UserAgent();
                my $req      = GET($copy_url);
                my $res2     = $ua->request($req);
                my $code     = $res2->code;

                $new_file_name{$file3} = $env->{storage_dir} . $copy_file_name;
            } else {

                # no copy
                $new_file_name{$file3} = $env->{upload_dir} . $copy_file_name;
            }

            my $file_size = fileSize( $new_file_name{$file3} );
            if ( !$file_size ) {
                $msg = "Empty data file -- file upload failed.";
                if ($copy_to_storage) {
                    $msg = "Empty data file -- file copy failed.";
                }
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }
        } else {

            # use JGI file
            if ( !blankStr($newname) ) {
                # bug fix https url - ken
                if ( $newname =~ /^http\:\/\// || $newname =~ /^https\:\/\// || $newname =~ /^ftp\:\/\// ) {

                    # don't need to check http or ftp
                } elsif ( -e $newname ) {

                    # file exists
                    push @notes, ("Use JGI file: $newname");
                } else {
                    $msg = "File $newname does not exist.";
                    print "<h4>Error: $msg </h4>";
                    print end_form();
                    return;
                }
            } else {
                $msg = "Please specify a file name.";
                print "<h4>Error: $msg </h4>";
                print end_form();
                return;
            }
        }
    }

#print "exit now - ken<br>\n";
#exit 0; 

    # get default submission status
    my $submit_status = 50;    # pending for annotation
    if ( $gene_calling_flag eq 'No' ) {
        $submit_status = 1;    # Submitted. File pending for validation.
    }

    ## testing?
    if ($test_mode) {
        $submit_status = 4;
    }

    my $sql;
    my @sqlList = ();

    # enter a submission entry
    my $is_jgi      = 0;
    my $dbh         = WebFunctions::Connect_IMG;
    my $project_fld = 'project_info';
    if ($gold_ap_id) {
        $project_fld = 'analysis_project_id';

        #	if ( isJgiAnalysis($dbh, $gold_ap_id ) ) {
        #	    $is_jgi = 1;
        #	}
    }
    $dbh->disconnect();

    my $ins = "insert into submission (submission_id, "
      . " analysis_project_id, contact, submission_date, status, approval_status, database";
    my $vals = "values ($sub_id, ";

    if ($gold_ap_id) {
        $vals .= "'$gold_ap_id'";
    } else {
        ## shouldn't happen
        $vals .= "null";
    }

    $vals .=
", $submit_for_contact, sysdate, $submit_status, 'pending review', '$database'";

    ## seq_status
    #    $ins .= ", seq_status";
    #    $vals .= ", '" . param1('seq_status') . "'";

    ## mol_topology
    $ins  .= ", mol_topology";
    $vals .= ", '" . param1('mol_topology') . "'";

    ## replace_taxon_oid
    if ( $replace_taxon_oid && isInt($replace_taxon_oid) ) {
        $ins  .= ", replace_taxon_oid";
        $vals .= ", $replace_taxon_oid";
    }

    ## is_img_public, gene_calling_flag
    $ins  .= ", is_img_public, gene_calling_flag";
    $vals .= ", '$is_img_public', '$gene_calling_flag'";

    ## file names
    for my $attr_name (@file_types) {
        my $v = $new_file_name{$attr_name};
        if ( $test_mode > 1 ) {
            print "<p>New file name for $attr_name is: $v\n";
        }

        my $new_name = validPathName($v);
        if (    $use_jgi_file{$attr_name} == 0
             && $new_name ne $v )
        {
            $msg = $attr_name . " contains illegal characters.";
            print "<h4>Error: (1) $msg </h4>";
            
#            my $dump = longmess();
#            print qq{
#                <pre>
#                ========== 11111
#                
#                $dump
#                
#                ===========
#                </<pre>>
#            };
            
            print end_form();
            return $msg;
        }

        $v =~ s/'/''/g;    # replace ' with '';
        $ins  .= ", " . $attr_name;
        $vals .= ", '" . $v . "'";
    }    # end for k

    $sql = $ins . ") " . $vals . ")";

    push @sqlList, ($sql);

    # insert into submission_img_contacts table
    $sql = "insert into submission_img_contacts "
      . "(submission_id, img_contacts) values ($sub_id, $submit_for_contact)";
    push @sqlList, ($sql);

    ## testing only?
    #$test_mode = 1;

    if ($test_mode) {

        # show SQL but don't update
        print "<p>Test mode\n";
        for my $sql2 (@sqlList) {
            print "<p>SQL: $sql2\n";
        }

        if ( $test_mode < 2 ) {
            db_sqlTrans( \@sqlList );
        }
    } else {
        db_sqlTrans( \@sqlList );
    }

    print
"<h4>New submission is added to the database (submission ID: $sub_id)</h4>\n";
    for my $s3 (@notes) {
        print "<p>$s3</p>\n";
    }

    if ( $db_option eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:showMPage" value="OK" class="smbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:showERPage" value="OK" class="smbutton" />';
    }

    print "<p>\n";
    printHomeLink();

    print end_form();

}

#########################################################################
# checkLocusTag: check whether locus tag prefix is valid
# The locus_tag prefix must be 3-12 alphanumeric characters
# and the first character may not be a digit.
#########################################################################
sub checkLocusTag {
    my ($locus_tag) = @_;

    my $res = 0;
    if ( $locus_tag =~ /^[A-Za-z][A-Za-z0-9]+$/ ) {
        my $len = length($locus_tag);
        if ( $len >= 3 && $len <= 12 ) {
            $res = 1;
        }
    }

    return $res;
}

#############################################################
# getDomain
#############################################################
sub getDomain {
    my ( $project_oid, $gold_ap_id ) = @_;

    my $domain = "";
    if ( $project_oid && isInt($project_oid) ) {
        $domain = db_getValue(
            "select domain from project_info where project_oid = $project_oid");
        return $domain;
    }

    if ($gold_ap_id) {
        my $res = getGoldAnalysisProject($gold_ap_id);
        if ($res) {
            my $decode = decode_json($res);
            $domain = $decode->{'goldDomain'};
            if ( !$domain ) {
                $domain = $decode->{'domain'};
            }
            return $domain;
        }
    }

    return $domain;
}

sub ShowGoldSection {
    my ($isAdmin) = @_;

    print hr();
    print "<h3>GOLD Data</h3>\n";
    print "<p>You can query, enter new data or modify data in GOLD.</p>\n";

    print
'<input type="submit" name="editGold" value="Query/Edit GOLD" class="medbutton" />';
    print "&nbsp; \n";
    print
'<input type="submit" name="newGold" value="New GOLD Entry" class="medbutton" />';

    print "<p>Or select a control vocabulary class to edit: ";
    print "&nbsp; &nbsp; &nbsp;\n";
    print "<select name='cv_option' class='img' size='1'>\n";
    print "    <option value='cell_arrcv'>Cell Arrangement CV</option>\n";
    print "    <option value='cell_shapecv'>Cell Shape CV</option>\n";
    print "    <option value='countrycv'>Country CV</option>\n";
    print "    <option value='diseasecv'>Disease CV</option>\n";
    print "    <option value='habitatcv'>Habitat CV</option>\n";
    print "    <option value='motilitycv'>Motility CV</option>\n";
    print "    <option value='oxygencv'>Oxygen Requirement CV</option>\n";
    print "    <option value='phenotypecv'>Phenotype CV</option>\n";

    if ( $isAdmin eq 'Yes' ) {
        print "    <option value='phylogenycv'>Phylogeny CV</option>\n";
    }

    print "    <option value='project_statuscv'>Project Status CV</option>\n";
    print "    <option value='project_typecv'>Project Type CV</option>\n";
    print "    <option value='relevancecv'>Relevance CV</option>\n";
    print "    <option value='salinitycv'>Salinity CV</option>\n";
    print "    <option value='seq_methodcv'>Sequencing Method CV</option>\n";
    print "    <option value='seq_statuscv'>Sequencing Status CV</option>\n";
    print "    <option value='sporulationcv'>Sporulation CV</option>\n";
    print "    <option value='unitcv'>Unit CV</option>\n";
    print "</select>\n";
    print "<p/>\n";

    print
'<input type="submit" name="goldCV" value="Edit Control Vocabularies" class="medbutton" />';
}

sub ShowRequestAdminSection {
    my ($isAdmin) = @_;

    if ( $isAdmin eq 'Yes' ) {
        print hr();
        require RequestAcct;
        my $request_cnt = RequestAcct::requestCount("");
        my $new_cnt     = RequestAcct::requestCount("status = 'new request'");
        print "<h3>User Requests</h3>\n";
        print "<p>Total Request Count: $request_cnt ";
        print nbsp(1);
        print " (New Request Count: $new_cnt)</p>\n";

        print
'<input type="submit" name="showRequest" value="View / Update User Requests" class="medbutton" />';
        print nbsp(1);
        print
'<input type="submit" name="showNewRequest" value="View / Update New Requests" class="medbutton" />';
    }
}

##########################################################################
# FilterSubmission - set filter on displaying submissions
#             so that there won't be too many to display
##########################################################################
sub FilterSubmission {

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);
    my $uname   = db_getContactName($contact_oid);

    print "<h3>Set Submission Filter for $uname</h3>\n";

    #    my $database = param1('database');
    #    print hiddenVar('database', $database);

    # print java script
    print qq{ 
	<script> 
	    function selectAllCheckBoxes( x ) {
		var f = document.mainForm;
		for( var i = 0; i < f.length; i++ ) {
		    var e = f.elements[ i ]; 
		    if( e.type == "checkbox" ) {
			e.checked = ( x == 0 ? false : true );
		    } 
		} 
	    } 
	</script> 
	};

    # max display
    if ( !$use_yahoo_table ) {
        my $max_display = getSessionParam('sub_filter:max_display');
        if ( blankStr($max_display) ) {
            $max_display = "$default_max_sub_row";
        }
        print "<p>Maximal Rows of Display:\n";
        print nbsp(3);
        print "<select name='sub_filter:max_display' class='img' size='1'>\n";
        for my $cnt0 (
                       '20',   '30',  '50',  '60',  '80',  '100',
                       '120',  '150', '200', '300', '500', '800',
                       '1000', '2000'
          )
        {
            print "    <option value='$cnt0' ";
            if ( $cnt0 eq $max_display ) {
                print " selected ";
            }
            print ">$cnt0</option>\n";
        }
        print "</select>\n";
    }

    # default IMG database
    my $default_db_option = getSessionParam('sub_disp:img_database');
    print "<p>Default Data Type:";
    print nbsp(3);
    print qq{
      <select name='database' size='1'>\n";
      };
    my @db_list = ( 'IMG ER', 'IMG/M ER' );

    if ( $isAdmin eq 'Yes' ) {
        push @db_list, ( 'IMG_ER_RNASEQ', 'IMG_MER_RNASEQ', 'IMG Methylomics' );
    }

    for my $db_name (@db_list) {
        print "    <option value='$db_name' ";
        if ( $db_name eq $default_db_option ) {
            print " selected ";
        }

        print ">" . getDataSetDisplayName($db_name) . "</option>\n";
    }
    print "</select>\n";
    print "<p/>\n";

    # show IMG contacts?
    my $sub_disp_option = getSessionParam('sub_disp:img_contacts');
    print "<p>Display IMG Contacts for each submission?\n";
    print nbsp(3);
    print "   <input type='radio' name='sub_disp:img_contacts' value='Yes'";
    if ( $sub_disp_option eq 'Yes' ) {
        print " checked";
    }
    print " />Yes\n";
    print "   <input type='radio' name='sub_disp:img_contacts' value='No'";
    if ( $sub_disp_option ne 'Yes' ) {
        print " checked";
    }
    print " />No\n";

    # check duplicate genomes
    if ( $isAdmin eq 'Yes' ) {
        my $duplicate_check_option =
          getSessionParam('sub_disp:duplicate_check');
        print "<p>Check duplicate (isolate) genomes?\n";
        print nbsp(3);
        print
          "   <input type='radio' name='sub_disp:duplicate_check' value='Yes'";
        if ( $duplicate_check_option eq 'Yes' ) {
            print " checked";
        }
        print " />Yes\n";
        print
          "   <input type='radio' name='sub_disp:duplicate_check' value='No'";
        if ( $duplicate_check_option ne 'Yes' ) {
            print " checked";
        }
        print " />No\n";
    }

    # get existing filters
    #    my @filter_databases = ();
    #    if ( getSessionParam('sub_filter:database') ) {
    #	@filter_databases = split(/\,/,getSessionParam('sub_filter:database'));
    #    }

    my @filter_statuses = ();
    if ( getSessionParam('sub_filter:status') ) {
        @filter_statuses = split( /\,/, getSessionParam('sub_filter:status') );
    }

    # show filter selection
    #    print "<p>Database: \n";
    #    for my $v1 ( 'IMG ER', 'IMG/M ER' ) {
    #	print nbsp(1);
    #	print "<input type='checkbox' name='sub_filter:database' value='$v1'";
    #	if ( scalar(@filter_databases) == 0 ||
    #	     inArray($v1, @filter_databases) ) {
    #	    print " checked ";
    #	}
    #	print ">$v1\n";
    #    }
    #    print "</p>\n";

    # img_dev_flag?
    # not needed anymore
    #    if ( $isAdmin eq 'Yes' ) {
    #	my $dev_flag = getSessionParam('sub_filter:dev_flag');
    #	print "<p>Submitted to development database only?\n";
    #	print nbsp(3);
    #	print "<select name='sub_filter:dev_flag' class='img' size='1'>\n";
    #	print "    <option value='Both'></option>\n";
    #	for my $val0 ( 'Yes', 'No' ) {
    #	    print "    <option value='$val0' ";
    #	    if ( $val0 eq $dev_flag ) {
    #		print " selected ";
    #	    }
    #	    print ">$val0</option>\n";
    #	}
    #	print "</select>\n";
    #    }

    print "</select></td>\n";

    my $old_vs_new = getSessionParam('old_vs_new');
    my $ck1        = " selected ";
    my $ck2        = "";
    if ( $old_vs_new eq 'new' ) {
        $ck1 = "";
        $ck2 = " selected ";
    }
    print "<p><select name='old_vs_new'>\n";
    print
"    <option value='old' $ck1>(For Old Submissions) Project/Study Name:</option>\n";
    print
"    <option value='new' $ck2>(For New Submissions) Analysis Project Name:</option>\n";
    print "</select>\n";
    print nbsp(3);
    my $label = "sub_filter:submission_proj_name";
    my $val2  = getSessionParam($label);
    print "<input type='text' name='$label' value='$val2' "
      . "size='60' maxLength='255'/>\n";
    print "<br/>\n";

    print "<p>Submission Status: \n";
    my $dbh = WebFunctions::Connect_IMG;

    my $sql =
      "select term_oid, cv_term from submission_statuscv order by term_oid";
      
webLog("610-- $sql\n");      
      
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( my $j = 0 ; $j <= 100 ; $j++ ) {
        my ( $v2, $cv_term ) = $cur->fetchrow_array();
        if ( !$v2 ) {
            last;
        }

        print "<br/>\n";
        print nbsp(3);
        print "<input type='checkbox' name='sub_filter:status' value='$v2'";
        if ( scalar(@filter_statuses) == 0
             || inArray( $v2, @filter_statuses ) )
        {
            print " checked ";
        }
        print ">" . $v2 . " - " . escapeHTML($cv_term) . "\n";
    }
    $cur->finish();
    $dbh->disconnect();

    print "<p>Submission ID\n";

    $label = "sub_filter:comp_submission_id";
    my $comp5 = getSessionParam($label);
    print nbsp(1);
    print "<select name='$label' class='img' size='1'>\n";
    print "   <option value='0'> </option>\n";
    for my $k ( '=', '!=', '>', '>=', '<', '<=', 'in' ) {
        my $k2 = escapeHTML($k);
        print "   <option value='$k2'";
        if ( !blankStr($comp5) && $k eq $comp5 ) {
            print " selected ";
        }
        print ">$k2</option>\n";
    }
    print "</select></td>\n";
    print nbsp(1);
    $label = "sub_filter:val_submission_id";
    my $val5 = getSessionParam($label);
    print "<input type='text' name='$label' value='$val5' "
      . "size='20' maxLength='800'/>\n";
    print " (comma separated list when use 'in')\n";
    print "<p>Submission Date\n";

    $label = "sub_filter:comp_submission_date";
    my $comp1 = getSessionParam($label);
    print nbsp(1);
    print "<select name='$label' class='img' size='1'>\n";
    print "   <option value='0'> </option>\n";
    for my $k ( '=', '!=', '>', '>=', '<', '<=', 'match', 'not match' ) {
        my $k2 = escapeHTML($k);
        print "   <option value='$k2'";
        if ( !blankStr($comp1) && $k eq $comp1 ) {
            print " selected ";
        }
        print ">$k2</option>\n";
    }
    print "</select></td>\n";
    print nbsp(1);
    $label = "sub_filter:val_submission_date";
    my $val1 = getSessionParam($label);
    print "<input type='text' name='$label' value='$val1' "
      . "size='60' maxLength='255'/>\n";

    print "<br/>\n";
    print "<h5>(Use 'DD-MON-YY' for Date format.)</h5>\n";

    print "<p>Approval Status: \n";
    $label = "sub_filter:approval_status";
    my $val3 = getSessionParam($label);
    print nbsp(3);
    print "<select name='$label' class='img' size='1'>\n";
    print "   <option value='all' ";
    if ( !$val3 || $val3 eq 'all' ) {
        print " selected ";
    }
    print ">All Statuses </option>\n";
    print "   <option value='approved'";
    if ( $val3 eq 'approved' ) {
        print " selected ";
    }
    print ">Approved Only </option>\n";
    print "   <option value='missing metadata'";
    if ( $val3 eq 'missing metadata' ) {
        print " selected ";
    }
    print ">Missing Metadata Only </option>\n";
    print "   <option value='pending review'";
    if ( $val3 eq 'pending review' ) {
        print " selected ";
    }
    print ">Pending Review Only </option>\n";
    print "   <option value='on hold'";
    if ( $val3 eq 'on hold' ) {
        print " selected ";
    }
    print ">On Hold Only </option>\n";
    print "</select>\n";

    print "<p>JGI Submissions? \n";
    $label = "sub_filter:is_jgi";
    my $val4 = getSessionParam($label);
    print nbsp(3);
    print "<select name='$label' class='img' size='1'>\n";
    print "   <option value='all' ";
    if ( !$val4 || $val4 eq 'all' ) {
        print " selected ";
    }
    print ">All Submissions </option>\n";
    print "   <option value='Yes'";
    if ( $val4 eq 'Yes' ) {
        print " selected ";
    }
    print ">JGI Submissions Only </option>\n";
    print "   <option value='No'";
    if ( $val4 eq 'No' ) {
        print " selected ";
    }
    print ">Non-JGI Submissions Only </option>\n";
    print "</select>\n";

    ## IMG Taxon OID
    print "<p>IMG Taxon OIDs (comma separated list): \n";
    print nbsp(2);
    $label = "sub_filter:val_img_taxon_oid";
    my $val6 = getSessionParam($label);
    print "<input type='text' name='$label' value='$val6' "
      . "size='40' maxLength='1200'/>\n";

    # ITS AP ID, SP ID and PM
    if ( $isAdmin eq 'Yes' ) {
        print "<p>ITS AP ID (comma separated list): \n";
        print nbsp(2);
        $label = "sub_filter:val_its_apid";
        $val1  = getSessionParam($label);
        print "<input type='text' name='$label' value='$val1' "
          . "size='40' maxLength='1200'/>\n";

        print "<p>ITS SP ID (comma separated list): \n";
        print nbsp(2);
        $label = "sub_filter:val_its_spid";
        $val1  = getSessionParam($label);
        print "<input type='text' name='$label' value='$val1' "
          . "size='40' maxLength='1200'/>\n";

        print "<p>Project Manager: \n";
        $label = "sub_filter:pm_name";
        my $val4 = getSessionParam($label);
        print nbsp(3);
        print "<select name='$label' class='img' size='1'>\n";
        print "   <option value='0'> </option>\n";

        my @pm_names = ();
        my $dbh      = Connect_IMG();
        my $sql4 =
"select unique pm_name from gold_sequencing_project where pm_name is not null ";

webLog("621-- $sql4\n");

        my $cur4 = $dbh->prepare($sql4);
        $cur4->execute();
        for ( ; ; ) {
            my ($n4) = $cur4->fetchrow_array();
            last if !$n4;
            push @pm_names, ($n4);
        }
        $cur4->finish();

        for my $name4 ( sort @pm_names ) {
            print "   <option value='$name4'";
            if ( $val4 && $val4 eq $name4 ) {
                print " selected ";
            }
            print ">$name4</option>\n";
        }
        print "</select>\n";
    }

    # JAT KEY ID
    if ( $isAdmin eq 'Yes' ) {
        my $show_jat_option = getSessionParam('sub_disp:show_jat');
        print "<p>Show JAT Key ID?\n";
        print nbsp(3);
        print "   <input type='radio' name='sub_disp:show_jat' value='Yes'";
        if ( $show_jat_option eq 'Yes' ) {
            print " checked";
        }
        print " />Yes\n";
        print "   <input type='radio' name='sub_disp:show_jat' value='No'";
        if ( $show_jat_option ne 'Yes' ) {
            print " checked";
        }
        print " />No\n";
    }

    print "<p>JAT Key ID\n";
    $label = "sub_filter:comp_jat_id";
    $comp1 = getSessionParam($label);

    print nbsp(1);
    print "<select name='$label' class='img' size='1'>\n";
    print "   <option value='0'> </option>\n";
    for my $k ( '=', 'match' ) {
        my $k2 = escapeHTML($k);
        print "   <option value='$k2'";
        if ( !blankStr($comp1) && $k eq $comp1 ) {
            print " selected ";
        }
        print ">$k2</option>\n";
    }
    print "</select></td>\n";
    print nbsp(1);
    $label = "sub_filter:val_jat_id";
    $val1  = getSessionParam($label);
    print "<input type='text' name='$label' value='$val1' "
      . "size='40' maxLength='80'/>\n";

    # buttons
    print "<p>\n";
    print
'<input type="submit" name="_section_ERSubmission:applySubmissionFilter" value="Apply Submission Filter" class="medbutton" />';

    print nbsp(1);
    print "<input type='button' name='selectAll' value='Select All' "
      . "onClick='selectAllCheckBoxes(1)' class='smbutton' />\n";
    print nbsp(1);
    print "<input type='button' name='clearAll' value='Clear All' "
      . "onClick='selectAllCheckBoxes(0)' class='smbutton' />\n";

    print "<p>\n";

    printHomeLink();

    print end_form();
}

##########################################################################
# ApplySubmissionFilter - save filter info into database and apply filter
##########################################################################
sub ApplySubmissionFilter {
    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        return "Unknown username / password";
    }

    my @filter_conds = ();
    my $database     = param1('database');
    $database =~ s/%20/ /g;

    # default database
    if ($database) {
        setSessionParam( 'sub_disp:img_database', $database );
        push @filter_conds, ( "sub_disp:img_database" . "\t" . $database );
    }

    # max display
    my $max_display = param1('sub_filter:max_display');
    if ( !blankStr($max_display) ) {
        setSessionParam( 'sub_filter:max_display', $max_display );
        push @filter_conds, ( "sub_filter:max_display" . "\t" . $max_display );
    }

    my $sub_disp_option = param1('sub_disp:img_contacts');
    if ( !blankStr($sub_disp_option) ) {
        setSessionParam( 'sub_disp:img_contacts', $sub_disp_option );
        push @filter_conds,
          ( "sub_disp:img_contacts" . "\t" . $sub_disp_option );
    }

    my $duplicate_check_option = param1('sub_disp:duplicate_check');
    if ( !blankStr($duplicate_check_option) ) {
        setSessionParam( 'sub_disp:duplicate_check', $duplicate_check_option );
        push @filter_conds,
          ( "sub_disp:duplicate_check" . "\t" . $duplicate_check_option );
    }

    my @all_status =
      db_getValues("select term_oid from submission_statuscv order by 1");
    if ( param('sub_filter:status') ) {
        my $cnt    = 0;
        my @status = param('sub_filter:status');
        for my $s (@all_status) {
            if ( inArray( $s, @status ) ) {
                $cnt++;
            }
        }
    }

    # submission id
    my $comp_submission_id = param1('sub_filter:comp_submission_id');
    my $val_submission_id  = param1('sub_filter:val_submission_id');

    if ( blankStr($comp_submission_id) ) {

        # no condition on submission id
    } else {
        if ( blankStr($val_submission_id) ) {
            return "Submission ID is not provided.";
        }

        # check ID
        if ( $comp_submission_id eq 'in' ) {
            my @ids = split( /\,/, $val_submission_id );
            if ( scalar(@ids) == 0 ) {
                return
"Incorrect Submission IDs (Must be a list of comma separated integers.)";
            }
            for my $val1 (@ids) {
                $val1 = strTrim($val1);
                if ( !isInt($val1) ) {
                    return
"Incorrect Submission IDs (Must be a list of comma separated integers.)";
                }
            }
        } elsif ( !isInt($val_submission_id) ) {
            return "Incorrect Submission ID (Must be an integer.)";
        }
    }

    # submission date
    my $comp_submission_date = param1('sub_filter:comp_submission_date');
    my $val_submission_date  = param1('sub_filter:val_submission_date');

    if ( blankStr($comp_submission_date) ) {

        # no condition on submission date
    } else {
        if ( blankStr($val_submission_date) ) {
            return "Submission Date is not provided.";
        }

        # check date
        my ( $d1, $d2, $d3 ) = split( /\-/, $val_submission_date );
        if (    blankStr($d1)
             || blankStr($d2)
             || blankStr($d3)
             || !isInt($d1)
             || !isInt($d3) )
        {
            return "Incorrect Date (Use 'DD-MON-YY' for Date format.)";
        }

        my @months = (
                       'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
                       'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'
        );
        if ( !inArray( uc($d2), @months ) ) {
            return "Incorrect Month (Use 'DD-MON-YY' for Date format.)";
        }
    }

    # IMG Taxon OID
    my $val_img_taxon_oid = param1('sub_filter:val_img_taxon_oid');
    if ( !blankStr($val_img_taxon_oid) && !isIntList($val_img_taxon_oid) ) {
        return
"IMG Taxon OID must be an integer or a list of integers separated by commas.";
    }

    # ITS APID
    my $val_its_apid = param1('sub_filter:val_its_apid');
    if ( !blankStr($val_its_apid) && !isIntList($val_its_apid) ) {
        return
"ITS AP ID must be an integer or a list of integers separated by commas. (Your input is: $val_its_apid)";
    }

    # ITS SPID
    my $val_its_spid = param1('sub_filter:val_its_spid');
    if ( !blankStr($val_its_spid) && !isIntList($val_its_spid) ) {
        return
"ITS SP ID must be an integer or a list of integers separated by commas. (Your input is: $val_its_spid)";
    }

    # JAT ID
    my $comp_jat_id = param1('sub_filter:comp_jat_id');
    my $val_jat_id  = param1('sub_filter:val_jat_id');

    if ( blankStr($comp_jat_id) ) {

        # no condition on JAT ID
    } else {
        if ( blankStr($val_jat_id) ) {
            return "JAT Key ID is not provided.";
        }

        # check ID
        if ( $val_jat_id =~ m/[^a-zA-Z0-9\_\-]/ ) {
            return "Incorrect JAT ID Value";
        }
    }

    # save variables
    my $f_db = "";
    if ( param('sub_filter:database') ) {
        my @vals = param('sub_filter:database');
        for my $v0 (@vals) {
            if ( blankStr($f_db) ) {
                $f_db = $v0;
            } else {
                $f_db .= "," . $v0;
            }
        }
    }
    setSessionParam( 'sub_filter:database', $f_db );
    push @filter_conds, ( "sub_filter:database" . "\t" . $f_db );

    my $f_st = "";
    if ( param('sub_filter:status') ) {
        my @vals = param('sub_filter:status');
        for my $v0 (@vals) {
            if ( blankStr($f_st) ) {
                $f_st = $v0;
            } else {
                $f_st .= "," . $v0;
            }
        }
    }
    setSessionParam( 'sub_filter:status', $f_st );
    push @filter_conds, ( "sub_filter:status" . "\t" . $f_st );
    setSessionParam( 'sub_filter:comp_submission_id', $comp_submission_id );
    push @filter_conds,
      ( "sub_filter:comp_submission_id" . "\t" . $comp_submission_id );
    setSessionParam( 'sub_filter:val_submission_id', $val_submission_id );
    push @filter_conds,
      ( "sub_filter:val_submission_id" . "\t" . $val_submission_id );

    setSessionParam( 'sub_filter:comp_submission_date', $comp_submission_date );
    push @filter_conds,
      ( "sub_filter:comp_submission_date" . "\t" . $comp_submission_date );
    setSessionParam( 'sub_filter:val_submission_date', $val_submission_date );
    push @filter_conds,
      ( "sub_filter:val_submission_date" . "\t" . $val_submission_date );

    my $sub_proj_name = param1('sub_filter:submission_proj_name');
    setSessionParam( 'sub_filter:submission_proj_name', $sub_proj_name );
    push @filter_conds,
      ( "sub_filter:submission_proj_name" . "\t" . $sub_proj_name );

    my $old_vs_new = param1('old_vs_new');
    setSessionParam( 'old_vs_new', $old_vs_new );

    if ( param1('sub_filter:approval_status') ) {
        setSessionParam( 'sub_filter:approval_status',
                         param1('sub_filter:approval_status') );
        push @filter_conds,
          ( "sub_filter:approval_status" . "\t"
            . param1('sub_filter:approval_status') );
    }

    if ( param1('sub_filter:is_jgi') ) {
        setSessionParam( 'sub_filter:is_jgi', param1('sub_filter:is_jgi') );
        push @filter_conds,
          ( "sub_filter:is_jgi" . "\t" . param1('sub_filter:is_jgi') );
    }

    if ( param1('sub_filter:dev_flag') ) {
        setSessionParam( 'sub_filter:dev_flag', param1('sub_filter:dev_flag') );
        push @filter_conds,
          ( "sub_filter:dev_flag" . "\t" . param1('sub_filter:dev_flag') );
    }

    setSessionParam( 'sub_filter:val_img_taxon_oid',
                     param1('sub_filter:val_img_taxon_oid') );
    push @filter_conds,
      ( "sub_filter:val_img_taxon_oid" . "\t"
        . param1('sub_filter:val_img_taxon_oid') );

    setSessionParam( 'sub_filter:val_its_apid',
                     param1('sub_filter:val_its_apid') );
    push @filter_conds,
      ( "sub_filter:val_its_apid" . "\t" . param1('sub_filter:val_its_apid') );

    setSessionParam( 'sub_filter:val_its_spid',
                     param1('sub_filter:val_its_spid') );
    push @filter_conds,
      ( "sub_filter:val_its_spid" . "\t" . param1('sub_filter:val_its_spid') );

    setSessionParam( 'sub_filter:pm_name', param1('sub_filter:pm_name') );
    push @filter_conds,
      ( "sub_filter:pm_name" . "\t" . param1('sub_filter:pm_name') );

    my $jat_id_option = param1('sub_disp:show_jat');
    if ( !blankStr($jat_id_option) ) {
        setSessionParam( 'sub_disp:show_jat', $jat_id_option );
        push @filter_conds, ( "sub_disp:show_jat" . "\t" . $jat_id_option );
    }
    setSessionParam( 'sub_filter:comp_jat_id', $comp_jat_id );
    push @filter_conds, ( "sub_filter:comp_jat_id" . "\t" . $comp_jat_id );
    setSessionParam( 'sub_filter:val_jat_id', $val_jat_id );
    push @filter_conds, ( "sub_filter:val_jat_id" . "\t" . $val_jat_id );

    if ($workspace_dir) {
        if ( !-e "$workspace_dir/$contact_oid" ) {
            mkdir "$workspace_dir/$contact_oid";
        }

        if ( -e "$workspace_dir/$contact_oid" ) {
            my $file_name =
              $workspace_dir . "/" . $contact_oid . "/" . "mySubmissionFilter";
            my $fh = newWriteFileHandle( $file_name, "mySubmissionFilter" );
            for my $cond1 (@filter_conds) {
                print $fh $cond1 . "\n";
            }
            close($fh);
        }
    }

    clearSubmissionPageInfo();

    return "";
}

#########################################################################
# submissionCount - count the number of submission
##########################################################################
sub submissionCount {
    my ( $contact_oid, $isAdmin, $database, $cond ) = @_;

    my $dbh = WebFunctions::Connect_IMG;

    my $uemail = lc( db_getContactEmail($contact_oid) );
    my $sql    = "select count(*) from submission s";

    if ( $contact_oid && $isAdmin ne 'Yes' ) {
        my $db_email = $uemail;
        $db_email =~ s/'/''/g;    # replace ' with ''
        $sql .= " where  s.contact = $contact_oid ";
        if ( length($database) > 0 ) {
            $sql .= " and s.database = '$database'";
        }

        if ( length($cond) > 0 ) {
            $sql .= " and " . $cond;
        }
    } else {
        if ( length($database) > 0 ) {
            $sql .= " where s.database = '$database'";
            if ( length($cond) > 0 ) {
                $sql .= " and " . $cond;
            }
        } else {
            if ( length($cond) > 0 ) {
                $sql .= " where " . $cond;
            }
        }
    }

##    print "SQL: $sql<br>";
webLog("620-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($cnt) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( !$cnt ) {
        return 0;
    }
    return $cnt;
}

#########################################################################
# submissionFilterCondition
##########################################################################
sub submissionFilterCondition {
    my $cond = "";

    my $db_cond = "";
    if ( getSessionParam('sub_filter:database') ) {
        my @databases = split( /\,/, getSessionParam('sub_filter:database') );
        if ( scalar(@databases) > 0 && scalar(@databases) < 2 ) {
            for my $s2 (@databases) {
                if ( blankStr($db_cond) ) {
                    $db_cond = " s.database in ( '" . $s2 . "'";
                } else {
                    $db_cond .= ", '" . $s2 . "'";
                }
            }
            if ( !blankStr($db_cond) ) {
                $db_cond .= " )";
            }
        }
    }

webLog("200-- $db_cond\n");

    my $status_cond = "";

#webLog("210-- select count(*) from submission_statuscv\n");
    
    
    my $status_cnt  = db_getValue("select count(*) from submission_statuscv");
    if ( getSessionParam('sub_filter:status') ) {
        my @statuses = split( /\,/, getSessionParam('sub_filter:status') );
        if ( scalar(@statuses) > 0 && scalar(@statuses) < $status_cnt ) {
            for my $s2 (@statuses) {
                if ( blankStr($status_cond) ) {
                    $status_cond = " s.status in ( $s2";
                } else {
                    $status_cond .= ", $s2";
                }
            }
        }

        if ( !blankStr($status_cond) ) {
            $status_cond .= " )";
        }
    }

    my $sub_id_cond = "";
    if (    getSessionParam('sub_filter:comp_submission_id')
         && getSessionParam('sub_filter:val_submission_id') )
    {
        my $da = uc( getSessionParam('sub_filter:val_submission_id') );
        my $op = getSessionParam('sub_filter:comp_submission_id');
        if ( $op eq 'in' ) {
            $sub_id_cond = " s.submission_id in ( $da ) ";
        } else {
            $sub_id_cond = " s.submission_id $op $da";
        }
    }

    my $date_cond = "";
    if (    getSessionParam('sub_filter:comp_submission_date')
         && getSessionParam('sub_filter:val_submission_date') )
    {
        my $da = uc( getSessionParam('sub_filter:val_submission_date') );
        my $op = getSessionParam('sub_filter:comp_submission_date');
        if ( $op eq 'match' ) {
            $date_cond = " s.submission_date like '%" . $da . "%'";
        } elsif ( $op eq 'not match' ) {
            $date_cond = " s.submission_date not like '%" . $da . "%'";
        } else {
            $date_cond = " s.submission_date $op '" . $da . "'";
        }
    }

    my $jat_cond = "";
    if (    getSessionParam('sub_filter:comp_jat_id')
         && getSessionParam('sub_filter:val_jat_id') )
    {
        my $da = uc( getSessionParam('sub_filter:val_jat_id') );
        my $op = getSessionParam('sub_filter:comp_jat_id');
        if ( $op eq 'match' ) {
            $jat_cond = " s.jat_key_id like '%" . $da . "%'";
        } else {
            $jat_cond = " s.jat_key_id $op '" . $da . "'";
        }
    }

    my $proj_name_cond = "";
    if ( getSessionParam('sub_filter:submission_proj_name') ) {
        my $na = lc( getSessionParam('sub_filter:submission_proj_name') );
        my $old_vs_new = getSessionParam('old_vs_new');
        if ( $old_vs_new eq 'new' ) {
            $proj_name_cond = qq{
	       s.analysis_project_id in
		(select g2.gold_id from gold_analysis_project g2
		 where lower(g2.analysis_project_name )like '%$na%')
	    };
        } else {
            $proj_name_cond = qq{
	       s.project_info in
		(select p2.project_oid from project_info p2
		 where lower(p2.display_name )like '%$na%')
	    };
        }
    }

    my $approval_cond = "";
    if ( getSessionParam('sub_filter:approval_status') ) {
        my $s2 = getSessionParam('sub_filter:approval_status');
        if ( $s2 eq 'approved' ) {
            $approval_cond = "s.approval_status = 'approved'";
        } elsif ( $s2 eq 'on hold' ) {
            $approval_cond = "s.approval_status = 'on hold'";
        } elsif ( $s2 eq 'missing metadata' ) {
            $approval_cond = "s.approval_status = 'missing metadata'";
        } elsif ( $s2 eq 'pending review' ) {
            $approval_cond = "s.approval_status = 'pending review'";
        }
    }

    my $is_jgi_cond = "";
    if ( getSessionParam('sub_filter:is_jgi') ) {
        my $s2 = getSessionParam('sub_filter:is_jgi');
        if ( $s2 eq 'Yes' ) {
            $is_jgi_cond = "s.is_jgi = 'Yes'";
        } elsif ( $s2 eq 'No' ) {
            $is_jgi_cond = "s.is_jgi = 'No'";
        }
    }

    my $dev_cond = "";
    if ( getSessionParam('sub_filter:dev_flag') ) {
        my $s2 = getSessionParam('sub_filter:dev_flag');
        if ( $s2 eq 'Yes' ) {
            $dev_cond = "s.img_dev_flag = 'Yes'";
        } elsif ( $s2 eq 'No' ) {
            $dev_cond = "s.img_dev_flag = 'No'";
        }
    }

    ## IMG taxon oid
    my $img_taxon_oid_cond = "";
    if ( getSessionParam('sub_filter:val_img_taxon_oid') ) {
        my $s2 = getSessionParam('sub_filter:val_img_taxon_oid');
        if ( $s2 && isIntList($s2) ) {
            my @arr = split( /\,/, $s2 );
            $s2                 = join( ", ", @arr );
            $img_taxon_oid_cond = "s.img_taxon_oid in ( $s2 ) ";
        }
    }

    ## ITS AP ID
    my $its_apid_cond = "";
    if ( getSessionParam('sub_filter:val_its_apid') ) {
        my $s2 = getSessionParam('sub_filter:val_its_apid');
        if ( $s2 && isIntList($s2) ) {
            my @arr = split( /\,/, $s2 );
            $s2 = join( ", ", @arr );
            $its_apid_cond =
"s.analysis_project_id in (select ap3.gold_id from gold_analysis_project ap3 where ap3.its_analysis_project_id in ( $s2 ))";
        }
    }

    ## ITS SP ID
    my $its_spid_cond = "";
    if ( getSessionParam('sub_filter:val_its_spid') ) {
        my $s2 = getSessionParam('sub_filter:val_its_spid');
        if ( $s2 && isIntList($s2) ) {
            my @arr = split( /\,/, $s2 );
            $s2 = join( ", ", @arr );
            $its_spid_cond =
"s.analysis_project_id in (select lookup3.gold_id from gold_analysis_project_lookup2 lookup3 where lookup3.its_spid in ( $s2 ))";
        }
    }

    my $pm_name_cond = "";
    if ( getSessionParam('sub_filter:pm_name') ) {
        my $s2 = getSessionParam('sub_filter:pm_name');
        if ($s2) {
            $s2 =~ s/'/''/g;    # replace ' with ''
            $pm_name_cond =
"s.analysis_project_id in (select lookup2.gold_id from gold_analysis_project_lookup2 lookup2, gold_sequencing_project sp where lookup2.sp_gold_id = sp.gold_id and sp.pm_name = '"
              . $s2 . "')";
        }
    }

    if ( !blankStr($db_cond) ) {
        $cond = $db_cond;
    }
    if ( !blankStr($status_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $status_cond;
        } else {
            $cond .= " and " . $status_cond;
        }
    }

    if ( !blankStr($sub_id_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $sub_id_cond;
        } else {
            $cond .= " and " . $sub_id_cond;
        }
    }

    if ( !blankStr($date_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $date_cond;
        } else {
            $cond .= " and " . $date_cond;
        }
    }

    if ( !blankStr($jat_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $jat_cond;
        } else {
            $cond .= " and " . $jat_cond;
        }
    }

    if ( !blankStr($proj_name_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $proj_name_cond;
        } else {
            $cond .= " and " . $proj_name_cond;
        }
    }

    if ( !blankStr($approval_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $approval_cond;
        } else {
            $cond .= " and " . $approval_cond;
        }
    }

    if ( !blankStr($is_jgi_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $is_jgi_cond;
        } else {
            $cond .= " and " . $is_jgi_cond;
        }
    }

    if ( !blankStr($dev_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $dev_cond;
        } else {
            $cond .= " and " . $dev_cond;
        }
    }

    if ( !blankStr($img_taxon_oid_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $img_taxon_oid_cond;
        } else {
            $cond .= " and " . $img_taxon_oid_cond;
        }
    }

    if ( !blankStr($its_apid_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $its_apid_cond;
        } else {
            $cond .= " and " . $its_apid_cond;
        }
    }

    if ( !blankStr($its_spid_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $its_spid_cond;
        } else {
            $cond .= " and " . $its_spid_cond;
        }
    }

    if ( !blankStr($pm_name_cond) ) {
        if ( blankStr($cond) ) {
            $cond = $pm_name_cond;
        } else {
            $cond .= " and " . $pm_name_cond;
        }
    }

    return $cond;
}

#########################################################################
# listSubmissions - list submissions (HTML table display)
##########################################################################
sub listSubmissions {
    my ( $contact_oid, $isAdmin, $database, $cond, $curr_page, $link_info,
         $ignore_filter )
      = @_;

    my $omics = 0;
    if ( $database eq 'IMG Methylomics' ) {
        $omics = 1;
    } elsif ( $database eq 'IMG_ER_RNASEQ' || $database eq 'IMG_MER_RNASEQ' ) {
        $omics = 2;
    }

    my $dbh = WebFunctions::Connect_IMG;

    my %submission_h;
    my $uname  = "";
    my $uemail = "";
    if ( $contact_oid && $isAdmin ne 'Yes' ) {
        $uname  = db_getContactName($contact_oid);
        $uemail = lc( db_getContactEmail($contact_oid) );

        my $sql2 = "select s2.submission_id from submission_img_contacts s2 "
          . "where s2.img_contacts = $contact_oid ";
          
webLog("700-- $sql2\n");          
          
        my $cur2 = $dbh->prepare($sql2);
        $cur2->execute();
        for ( ; ; ) {
            my ($sub_id) = $cur2->fetchrow_array();
            last if !$sub_id;
            $submission_h{$sub_id} = 1;
        }
        $cur2->finish();
    }

    my $long_disp       = 0;
    my $sub_disp_option = getSessionParam('sub_disp:img_contacts');
    if ( $sub_disp_option eq 'Yes' ) {
        $long_disp = 1;
    }

    # max display
    my $max_display = getSessionParam('sub_filter:max_display');
    if ( blankStr($max_display) ) {
        $max_display = $default_max_sub_row;
    }

    my $orderby = param1('submission_orderby');
    if (    blankStr($orderby)
         && getSessionParam('browse:submission_orderby') )
    {
        $orderby = getSessionParam('browse:submission_orderby');
    }
    if ( blankStr($orderby) ) {
        $orderby = 'submission_id';
    }
    my $orderby_attr = $orderby;

    my $desc = param1('submission_desc');
    if ( blankStr($desc) && getSession('browse:submission_desc') ) {
        $desc = getSessionParam('browse:submission_desc');
    }

    if ( blankStr($orderby) ) {
        $orderby = "s.submission_id $desc";
    } elsif ( $orderby eq 'submission_id' ) {
        $orderby = "s.submission_id $desc";
    } elsif (
        $orderby eq 'project_info'
        ||

        #	    $orderby eq 'database' ||
        $orderby eq 'img_taxon_oid' || $orderby eq 'submission_date'
      )
    {
        $orderby = "s." . $orderby . " $desc, s.submission_id";
    } elsif (    $orderby eq 'display_name'
              || $orderby eq 'gold_stamp_id' )
    {
        $orderby = "p." . $orderby . " $desc, s.submission_id";
    } elsif ( $orderby eq 'status' ) {
        $orderby = "v.cv_term $desc, s.submission_id";
    } elsif (    $orderby eq 'approval_status'
              || $orderby eq 'img_dev_flag'
              || $orderby eq 'gene_calling_flag'
              || $orderby eq 'img_product_flag' )
    {
        $orderby = "s." . $orderby . " $desc, s.submission_id";
    }

    my $sql = qq{
	select s.submission_id, s.project_info, p.display_name,
	    p.gold_stamp_id, s.sample_oid,
	    s.database, s.img_taxon_oid, s.is_img_public,
	    p.contact_name, s.contact, c.username,
	    s.submission_date, v.cv_term, s.approval_status,
	    s.img_dev_flag, s.img_product_flag, s.seq_status,
	    s.gene_calling_flag, s.subtitle, s.loaded_into_img, s.in_file,
            p.scope_of_work, p.culture_type, p.uncultured_type,
            s.is_jgi, p.its_spid
	    from submission s, submission_statuscv v, project_info p,
		contact c
	    where s.project_info = p.project_oid (+)
	    and s.contact = c.contact_oid
	    and s.status = v.term_oid (+)
	};

    if ( $contact_oid && $isAdmin ne 'Yes' ) {
        my $db_email = $uemail;
        $db_email =~ s/'/''/g;    # replace ' with ''
        $sql .=
            " and (s.contact = $contact_oid or "
          . "lower(p.contact_email) = '"
          . $db_email . "' or "
          . "s.submission_id in (select s2.submission_id from submission_img_contacts s2 where s2.img_contacts = $contact_oid)) ";
    }

    if ( $isAdmin eq 'Yes' && $database eq 'IMG/M ER' ) {
        $sql .= " and s.database in ('IMG/M ER', 'IMG/M HMP')";
    } elsif ( length($database) > 0 ) {
        $sql .= " and s.database = '$database'";
    }

    if ( length($cond) > 0 ) {
        $sql .= " and " . $cond;
    }
    my $cond2 = "";
    if ( !$ignore_filter ) {
        $cond2 = submissionFilterCondition();
    }

    if ( !blankStr($cond2) ) {

#	print "<p><font color='red'>Filter is on. Some submissions may be hidden. Reset the filter to see more submissions.</font></p>\n";

        $sql .= " and " . $cond2;
    }

    if ( !blankStr($orderby) ) {
        $sql .= " order by " . $orderby;
    }

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>";
    #    }


webLog("710-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();

#    print "<h5>Click the column name to have the data order by the selected column. Click (Rev) to order by the same column in reverse order.</h5>\n";

    # set default link_info
    if ( blankStr($link_info) ) {
        if ( $database eq 'IMG/M ER' ) {
            $link_info = "&section=MSubmission&page=showMPage";
        } else {
            $link_info .= "&section=ERSubmission&page=showERPage";
        }
    }

    my $color1 = "#eeeeee";
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";

    my @all_attrs = (
                      'submission_id', 'project_info',
                      'display_name',  'seq_status',
                      'gold_stamp_id', 'img_taxon_oid'
    );
    for my $attr1 (@all_attrs) {
        my $sort_flag = 0;
        if ( $attr1 eq $orderby_attr ) {
            if ( $desc eq 'desc' ) {
                $sort_flag = -1;
            } else {
                $sort_flag = 1;
            }
        }

        my $disp_name = getAttrDispName($attr1);
        if ( $attr1 eq 'img_taxon_oid' && $omics ) {
            $disp_name = "Reference Taxon OID";
        }

        print "<th class='img' bgcolor='$color1'>"
          . getSubmissionOrderByLink( $attr1, $disp_name,
                                      $link_info, $database, $sort_flag )
          . "</th>\n";
    }

    if ($long_disp) {
        print "<th class='img' bgcolor='lightblue'>IMG Contact Name(s)</th>\n";
    }

    print "<th class='img' bgcolor='$color1'>Submitter</th>\n";
    print "<th class='img' bgcolor='$color1'>JGI Genome?</th>\n";
    if ( $isAdmin eq 'Yes' ) {
        print "<th class='img' bgcolor='$color1'>Culture Type</th>\n";
        print "<th class='img' bgcolor='$color1'>Scope of Work</th>\n";
    }

    @all_attrs = (
                   'submission_date',   'status',
                   'approval_status',   'img_dev_flag',
                   'gene_calling_flag', 'img_product_flag'
    );
    for my $attr1 (@all_attrs) {
        if ( $attr1 eq 'img_dev_flag' && $isAdmin ne 'Yes' ) {
            next;
        }

        my $sort_flag = 0;
        if ( $attr1 eq $orderby_attr ) {
            if ( $desc eq 'desc' ) {
                $sort_flag = -1;
            } else {
                $sort_flag = 1;
            }
        }
        print "<th class='img' bgcolor='$color1'>"
          . getSubmissionOrderByLink( $attr1, getAttrDispName($attr1),
                                      $link_info, $database, $sort_flag )
          . "</th>\n";
    }

    print "<th class='img' bgcolor='$color1'>Predicted Gene Count</th>\n";

    print "<th class='img' bgcolor='$color1'>Load into IMG?</th>\n";

    if ( $database eq 'IMG/M ER' ) {
        print "<th class='img' bgcolor='$color1'>Load into MER-FS?</th>\n";
    }

    my $dbh2 = WebFunctions::Connect_IMG();

    # predicted gene count
    my %predicted_gene_count;
    my $sql2 = qq{
	select submission_id, sum(no_pred_features)
	    from submission_proc_stats
	    where step in ( 'GeneCalling', 
                          'assembled_structural_annotation',
                          'unassembled_structural_annotation' )
            and type_of_feature not like 'SequencesWith%'
            and type_of_feature not like 'sequences%'
            group by submission_id
	};

webLog("720-- $sql2\n");

    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute();
    for ( ; ; ) {
        my ( $sub_id, $cnt2 ) = $cur2->fetchrow_array();
        last if !$sub_id;
        $predicted_gene_count{$sub_id} = $cnt2;
    }
    $cur2->finish();


    $dbh2->disconnect();

    my %contact_h;
    my $cnt      = 0;
    my $disp_cnt = 0;
    my $skip     = $max_display * ( $curr_page - 1 );

    for ( ; ; ) {
        my (
             $sub_id,        $proj_id,          $proj_name,
             $gold_stamp_id, $sample_oid,       $db,
             $taxon,         $is_img_public,    $contact_name,
             $contact,       $contact_username, $sub_date,
             $status,        $app_status,       $img_dev_flag,
             $prod_flag,     $seq_status,       $gene_calling_flag,
             $subtitle,      $loaded_into_img,  $in_file,
             $scope_of_work, $culture_type,     $uncultured_type,
             $is_jgi,        $its_spid
        ) = $cur->fetchrow_array();
        if ( !$sub_id ) {
            last;
        }

        $cnt++;
        if ( $cnt <= $skip ) {
            next;
        }

        $disp_cnt++;
        if ( $disp_cnt > $max_display ) {
            last;
        }

        if ( $disp_cnt % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        print "<td class='img'>\n";

        #        print "<input type='radio' ";
        print "<input type='checkbox' ";
        print "name='submission_id' value='$sub_id' />\n";
        print "</td>\n";

        # submission link
        my $submit_link = getSubmissionLink( $sub_id, $database );
        PrintAttribute($submit_link);

        #	PrintAttribute($sub_id);

        # project link
        my $proj_link = getProjectLink($proj_id);

        if ($sample_oid) {
            my $samp_link = getSampleLink($sample_oid);
            my $ps = $proj_link . " <br/><br/>(Sample: " . $samp_link . ")";
            PrintAttribute($ps);
        } else {
            PrintAttribute($proj_link);
        }

        # project name
        if ( !blankStr($subtitle) ) {
            $subtitle =~ s/\#\#/\=\=/g;
            $proj_name .= " (" . $subtitle . ")";
        }
        PrintAttribute( escHtml($proj_name) );

        # seq status
        PrintAttribute($seq_status);

        # GOLD ID
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);

        # database
        # PrintAttribute($db);

        # IMG taxon oid
        PrintAttribute($taxon);

        # IMG contacts
        if ($long_disp) {
            my $canUpdate = getCanUpdateSubmission($contact_oid);
            my $con_names = "";

            #	    if ( $isAdmin eq 'Yes' || $contact == $contact_oid ) {
            if ( $canUpdate || $contact == $contact_oid ) {
                my $dbh2 = WebFunctions::Connect_IMG();
                my $sql2 = qq{
		    select c.contact_oid, c.username
			from contact c, submission_img_contacts sic
			where sic.submission_id = $sub_id
			and sic.img_contacts = c.contact_oid
		    };
		    
webLog("730-- $sql2\n");		    
                my $cur2 = $dbh2->prepare($sql2);
                $cur2->execute();
                my $cnt2 = 0;

                for ( ; ; ) {
                    my ( $cid, $cname ) = $cur2->fetchrow_array();
                    if ( !$cid ) {
                        last;
                    }

                    if ( $isAdmin eq 'Yes' ) {
                        $cname = getContactLink( $cid, $cname );
                    }

                    $cnt2++;
                    if ( $cnt2 > 100 ) {
                        last;
                    }

                    if ( length($con_names) == 0 ) {
                        $con_names = $cname;
                    } else {
                        $con_names .= ", " . $cname;
                    }
                }
                $cur2->finish();
                $dbh2->disconnect();
            } else {

                # hide info from non-admin
                $con_names = $uname;
            }
            PrintAttribute($con_names);
        }

        # submitter
        if ( $isAdmin eq 'Yes' ) {
            my $contact_link = getContactLink( $contact, $contact_username );
            PrintAttribute($contact_link);
        } else {
            PrintAttribute($contact_username);
        }

        # JGI genome?
        #	if ( $proj_id && $is_jgi_genome{$proj_id} ) {
        #	    PrintAttribute("Yes");
        #	}
        #	else {
        #	    PrintAttribute("No");
        #	}
        if ($its_spid) {
            $is_jgi = 'Yes';
        }
        PrintAttribute($is_jgi);

        # scope of work
        if ( $isAdmin eq 'Yes' ) {
            my $cul_type = "";
            if ($culture_type) {
                $cul_type = $culture_type;
            } elsif ($uncultured_type) {
                $cul_type = $uncultured_type;
            }
            PrintAttribute($cul_type);
            PrintAttribute($scope_of_work);
        }

        # date and status
        PrintAttribute($sub_date);
        PrintAttribute($status);

        # approval status
        PrintAttribute($app_status);

        if ( $isAdmin eq 'Yes' ) {
            PrintAttribute($img_dev_flag);
        }

        PrintAttribute($gene_calling_flag);
        PrintAttribute($prod_flag);

        # predicted gene count
        if ( $predicted_gene_count{$sub_id} ) {
            PrintAttribute( $predicted_gene_count{$sub_id} );
        } else {
            PrintAttribute("N/A");
        }

        PrintAttribute($loaded_into_img);

        if ( $database eq 'IMG/M ER' ) {
            PrintAttribute($in_file);
        }

        print "</tr>\n";
    }
    print "</table>\n";
    print "<p>\n";

    $cur->finish();
    $dbh->disconnect();

#    if ( $cnt > $max_display ) {
#	print "<p><font color='blue'>Too many rows. Only $max_display rows are displayed. User 'Set Filter' to filter display.</font></p>\n";
#    }

    return $cnt;
}

#########################################################################
# listSubmissions_yui - list submissions (Yahoo datatable display)
##########################################################################
sub listSubmissions_yui {
    my ( $contact_oid, $isAdmin, $database, $cond, $ignore_filter ) = @_;

    my $dbh = WebFunctions::Connect_IMG;

    my %submission_h;
    my $uname  = "";
    my $uemail = "";
    if (    $contact_oid
         && isInt($contact_oid)
         && $isAdmin ne 'Yes' )
    {
        $uname  = db_getContactName($contact_oid);
        $uemail = lc( db_getContactEmail($contact_oid) );

        my $sql2 = "select s2.submission_id from submission_img_contacts s2 "
          . "where s2.img_contacts = $contact_oid ";
          
webLog("1-- $sql2\n");  
          
        my $cur2 = $dbh->prepare($sql2);
        $cur2->execute();
        for ( ; ; ) {
            my ($sub_id) = $cur2->fetchrow_array();
            last if !$sub_id;
            $submission_h{$sub_id} = 1;
        }
        $cur2->finish();

        $sql2 =
"select s3.submission_id from submission s3, gold_analysis_project g3, contact c3 where c3.contact_oid = $contact_oid and c3.caliban_id is not null and c3.caliban_id = g3.caliban_id and g3.gold_id = s3.analysis_project_id ";

webLog("2-- $sql2\n");

        $cur2 = $dbh->prepare($sql2);
        $cur2->execute();
        for ( ; ; ) {
            my ($sub_id) = $cur2->fetchrow_array();
            last if !$sub_id;
            $submission_h{$sub_id} = 1;
        }
        $cur2->finish();
    }

    my $long_disp       = 0;
    my $sub_disp_option = getSessionParam('sub_disp:img_contacts');
    if ( $sub_disp_option eq 'Yes' ) {
        $long_disp = 1;
    }

    # max display
    my $max_display = getSessionParam('sub_filter:max_display');
    if ( blankStr($max_display) ) {
        $max_display = $default_max_sub_row;
    }

    my $omics = 0;
    if ( $database eq 'IMG Methylomics' ) {
        $omics = 1;
    } elsif ( $database eq 'IMG_ER_RNASEQ' || $database eq 'IMG_MER_RNASEQ' ) {
        $omics = 2;
    }

    my %sample_count_h;
    my %sample_h;
    if ( $database eq 'IMG/M ER' ) {
        my $sql = "select submission_id, sample_oid from submission_samples";
        
webLog("3-- $sql\n");

        my $cur = $dbh->prepare($sql);
        $cur->execute();
        for ( ; ; ) {
            my ( $sub_id, $sam_id ) = $cur->fetchrow();
            last if !$sub_id;

            if ( !$sam_id ) {
                next;
            }

            $sample_h{$sub_id} = $sam_id;
            if ( $sample_count_h{$sub_id} ) {
                $sample_count_h{$sub_id} += 1;
            } else {
                $sample_count_h{$sub_id} = 1;
            }
        }
        $cur->finish();
    } elsif ( $database eq 'IMG_MER_RNASEQ' ) {
        my $sql = "select submission_id, sample_oid from submission "
          . "union select submission_id, sample_oid from submission_samples ";

webLog("4-- $sql\n");          
          
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        for ( ; ; ) {
            my ( $sub_id, $sam_id ) = $cur->fetchrow();
            last if !$sub_id;

            if ( !$sam_id ) {
                next;
            }

            $sample_h{$sub_id} = $sam_id;
            if ( $sample_count_h{$sub_id} ) {
                $sample_count_h{$sub_id} += 1;
            } else {
                $sample_count_h{$sub_id} = 1;
            }
        }
        $cur->finish();
    }

    my $sql = qq{
	select s.submission_id, s.project_info,
            s.analysis_project_id,
            null, null, 
	    s.database, s.img_taxon_oid, s.is_img_public,
	    null, s.contact, c.username,
	    to_char(s.submission_date, 'yyyy-mm-dd'),
	    v.cv_term, s.approval_status,
	    s.img_dev_flag, s.img_product_flag, s.seq_status,
	    s.gene_calling_flag, s.subtitle, s.loaded_into_img, s.in_file,
            null, null, null, 
            null, s.replace_taxon_oid, s.is_jgi,
            s.jat_key_id, s.contact_email, null
	    from submission s, submission_statuscv v,
		contact c
	    where s.contact = c.contact_oid (+)
	    and s.status = v.term_oid (+)
	};

    if ( $contact_oid && $isAdmin ne 'Yes' ) {
        my $db_email = $uemail;
        $db_email =~ s/'/''/g;    # replace ' with ''
        $sql .=
            " and   "
          . "s.contact = $contact_oid  ";
    }

    if ( $isAdmin eq 'Yes' && $database eq 'IMG/M ER' ) {
        $sql .= " and s.database in ('IMG/M ER', 'IMG/M HMP')";
    } elsif ( length($database) > 0 ) {
        $sql .= " and s.database = '$database'";
    }

    if ( length($cond) > 0 ) {
        $sql .= " and " . $cond;
    }
    my $cond2 = "";
    if ( !$ignore_filter ) {
        $cond2 = submissionFilterCondition();
    }

    if ( !blankStr($cond2) ) {


        $sql .= " and " . $cond2;
    }

webLog("5-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();                          # sort delimiter
    $it->addColSpec("Select");

    my @all_attrs = (
        'submission_id',
##		      'project_info',
        'analysis_project_id', 'display_name',
        'seq_status',          'gold_stamp_id',
        'its_apid', 'its_spid', 'pm_name', 'img_taxon_oid'
    );
    for my $attr1 (@all_attrs) {
        my $disp_name = getAttrDispName($attr1);
        if ( $attr1 eq 'img_taxon_oid' && $omics ) {
            $disp_name = "Reference Taxon OID";
        }
        if ( $attr1 eq 'seq_status' && $omics ) {
            $disp_name = "Sample Name";
        }

        if ( $attr1 eq 'display_name' ) {
            $disp_name = "Project/Study Name";
        }

        if ( $attr1 eq 'gold_stamp_id' ) {
            $disp_name = "Sequencing Project ID";
        }

        if ( $attr1 eq 'its_apid' ) {
            if ( $isAdmin eq 'Yes' ) {
                $disp_name = "ITS AP ID";
            } else {

                # this is for super users only
                next;
            }
        }

        if ( $attr1 eq 'its_spid' ) {
            if ( $isAdmin eq 'Yes' ) {
                $disp_name = "ITS SP ID";
            } else {

                # this is for super users only
                next;
            }
        }

        if ( $attr1 eq 'pm_name' ) {
            if ( $isAdmin eq 'Yes' ) {
                $disp_name = "Project Manager";
            } else {

                # this is for super users only
                next;
            }
        }

        if ( $attr1 eq 'submission_id' ) {
            $it->addColSpec( $disp_name, "number desc", "right", "",
                             $disp_name );
        } elsif (    $attr1 eq 'project_info'
                  || $attr1 eq 'img_taxon_oid' )
        {
            $it->addColSpec( $disp_name, "number asc", "right", "",
                             $disp_name );
        } elsif ( $attr1 eq 'analysis_project_id' ) {
            $it->addColSpec( $disp_name, "char asc", "left", "", $disp_name );
            $it->addColSpec(
                             "Analysis Project Type",
                             "char asc",
                             "left",
                             "",
                             "Analysis Project Type"
            );
            $it->addColSpec(
                             "Submission Type",
                             "char asc",
                             "left",
                             "",
                             "Submission Type"
            );
            $it->addColSpec(
                             "GOLD Review Status",
                             "char asc",
                             "left",
                             "",
                             "GOLD Review Status"
            );
            if ( $isAdmin eq 'Yes' ) {
                $it->addColSpec(
                                 "Sequencing Strategy",
                                 "char asc",
                                 "left",
                                 "",
                                 "Sequencing Strategy"
                );
            }
        } else {
            $it->addColSpec( $disp_name, "char asc", "left", "", $disp_name );
        }
    }

    $it->addColSpec(
                     "Analysis Product Name",
                     "char asc",
                     "left",
                     "",
                     "Analysis Product Name"
    );

    if ($long_disp) {
        $it->addColSpec(
                         "IMG Contact Name(s)",
                         "char asc",
                         "left",
                         "",
                         "IMG Contact Name(s)"
        );
    }

    $it->addColSpec( "Submitter", "char asc", "left", "", "Submitter" );

    #    $it->addColSpec("JGI Genome?", "char asc", "left", "",
    #		    "JGI Genome?");
    $it->addColSpec( "Seq Center", "char asc", "left", "", "Seq Center" );

    my $show_jat_option = getSessionParam('sub_disp:show_jat');
    if (    $isAdmin eq 'Yes'
         && $show_jat_option eq 'Yes' )
    {
        $it->addColSpec( "JAT Key ID", "char asc", "left", "", "JAT Key ID" );
    }

    my $duplicate_check_option = getSessionParam('sub_disp:duplicate_check');
    if (    $database eq 'IMG ER'
         && $isAdmin eq 'Yes'
         && $duplicate_check_option eq 'Yes' )
    {
        $it->addColSpec( "Duplicate Genome",
                         "char asc", "left", "", "Duplicate Genome" );
    }

    if ( $isAdmin eq 'Yes' ) {
        $it->addColSpec( "Culture Type", "char asc", "left", "",
                         "Culture Type" );
        $it->addColSpec( "Scope of Work",
                         "char asc", "left", "", "Scope of Work" );
    }

    @all_attrs =
      ( 'submission_date', 'status', 'approval_status', 'gene_calling_flag' );

    #		   'gene_calling_flag', 'img_product_flag' );
    for my $attr1 (@all_attrs) {
        my $disp_name = getAttrDispName($attr1);
        $it->addColSpec( $disp_name, "char asc", "left", "", $disp_name );
    }

    $it->addColSpec(
                     "Predicted Gene Count",
                     "number asc",
                     "left",
                     "",
                     "Predicted Gene Count"
    );


    my $dbh2 = WebFunctions::Connect_IMG();

    # predicted gene count
    my %predicted_gene_count;
    my $sql2 = qq{
	select submission_id, sum(no_pred_features)
	    from submission_proc_stats
	    where step in ( 'GeneCalling', 
                          'assembled_structural_annotation',
                          'unassembled_structural_annotation' )
            and type_of_feature not like 'SequencesWith%'
            and type_of_feature not like 'sequences%'
            group by submission_id
	};

webLog("6-- $sql2\n");

    my $cur2 = $dbh2->prepare($sql2) or die("Error: $DBI::errstr\n"); ;
    $cur2->execute() or die("Error: 2 $DBI::errstr\n"); ;
    for ( ; ; ) {
        my ( $sub_id, $cnt2 ) = $cur2->fetchrow_array();
        last if !$sub_id;
        $predicted_gene_count{$sub_id} = $cnt2;
    }
    $cur2->finish();



    ## seq center
    my %proj_seq_center_h;
    $sql2 = qq{
        select unique pidl.project_oid, pidl.db_name
            from project_info_data_links pidl
            where pidl.link_type = 'Seq Center'
            and pidl.project_oid in
            (select project_info from submission)
         };

webLog("7-- $sql2\n");

    $cur2 = $dbh2->prepare($sql2);
    $cur2->execute();
    for ( ; ; ) {
        my ( $proj_id, $seq_center ) = $cur2->fetchrow_array();
        last if !$proj_id;

        if ($seq_center) {
            if ( $proj_seq_center_h{$proj_id} ) {
                $proj_seq_center_h{$proj_id} .= ", " . $seq_center;
            } else {
                $proj_seq_center_h{$proj_id} = $seq_center;
            }
        }
    }
    $cur2->finish();

    my $use_gold_api = 0;
    my %gold_id_ref_h;
    my %ap_name_h;
    my %ap_ref_gold_h;
    my %ap_ref_img_h;
    my %ap_sample_h;
    my %ap_prod_name_h;
    my %ap_type_h;
    my %submit_type_h;
    my %sp_gold_id_ref_h;
    my %its_apid_ref_h;
    my %its_spid_ref_h;
    my %review_status_h;

    if ( !$use_gold_api ) {
        $sql2 =
            "select ap.gold_analysis_project_id, ap.gold_id, "
          . "ap.analysis_project_name, ap.reference_gold_id, "
          . "ap.img_dataset_id, ap.analysis_product_name, "
          . "ap.gold_analysis_project_type, ap.submission_type, "
          . "ap.review_status, ap.its_analysis_project_id "
          . "from gold_analysis_project ap "
          . "where ap.gold_id is not null ";

webLog("8-- $sql2\n");
          
        $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();

        for ( ; ; ) {
            my (
                 $gold_ap_oid,   $gold_ap_id,   $ap_name, $ref_gold,
                 $ref_img,       $ap_prod_name, $ap_type, $submit_type,
                 $review_status, $its_apid
            ) = $cur2->fetchrow_array();
            if ( !$gold_ap_oid ) {
                last;
            }

            $gold_id_ref_h{$gold_ap_oid}  = $gold_ap_id;
            $ap_name_h{$gold_ap_id}       = $ap_name;
            $ap_ref_gold_h{$gold_ap_id}   = $ref_gold;
            $ap_ref_img_h{$gold_ap_id}    = $ref_img;
            $ap_prod_name_h{$gold_ap_id}  = $ap_prod_name;
            $ap_type_h{$gold_ap_id}       = $ap_type;
            $submit_type_h{$gold_ap_id}   = $submit_type;
            $review_status_h{$gold_ap_id} = $review_status;
            $its_apid_ref_h{$gold_ap_id}  = $its_apid;
        }
        $cur2->finish();

        $sql2 =
            "select ap.gold_analysis_project_id, ap.gold_id, "
          . "lookup.sp_gold_id, lookup.its_spid, "
          . "lookup.sample_oid, es.sample_display_name "
          . "from gold_analysis_project ap, "
          . "gold_analysis_project_lookup2 lookup, "
          . "env_sample es "
          . "where ap.gold_id is not null "
          . "and ap.gold_id = lookup.gold_id "
          . "and lookup.sample_oid = es.sample_oid (+) ";
          
webLog("9-- $sql2\n");          
        $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();

        for ( ; ; ) {
            my (
                 $gold_ap_oid, $gold_ap_id, $sp_gold_id,
                 $its_spid,    $sample_oid, $sample_name
            ) = $cur2->fetchrow_array();
            if ( !$gold_ap_oid ) {
                last;
            }

            if ($sp_gold_id) {
                if ( $sp_gold_id_ref_h{$gold_ap_id} ) {
                    $sp_gold_id_ref_h{$gold_ap_id} .= ",$sp_gold_id";
                } else {
                    $sp_gold_id_ref_h{$gold_ap_id} = $sp_gold_id;
                }
            }

            if ($its_spid) {
                if ( $its_spid_ref_h{$gold_ap_id} ) {
                    $its_spid_ref_h{$gold_ap_id} .= ",$its_spid";
                } else {
                    $its_spid_ref_h{$gold_ap_id} = $its_spid;
                }
            }

            if ($sample_name) {
                $ap_sample_h{$gold_ap_id} = $sample_name;
            }
        }
        $cur2->finish();
    }

    my %pm_name_h;
    my %seq_strategy_h;
    if ( $isAdmin eq 'Yes' ) {
        my $sql2 =
            "select ap.gold_id, sp.pm_name, "
          . "sp.sequencing_strategy "
          . "from gold_analysis_project ap, "
          . "gold_analysis_project_lookup2 lookup, "
          . "gold_sequencing_project sp "
          . "where ap.gold_id is not null "
          . "and ap.gold_id = lookup.gold_id "
          . "and lookup.sp_gold_id = sp.gold_id ";
          
webLog("10-- $sql2\n");          
        my $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();

        for ( ; ; ) {
            my ( $gold_ap_id, $pm_name, $seq_st ) = $cur2->fetchrow_array();
            if ( !$gold_ap_id ) {
                last;
            }

            if ($pm_name) {
                if ( $pm_name_h{$gold_ap_id} ) {
                    $pm_name_h{$gold_ap_id} .= ",$pm_name";
                } else {
                    $pm_name_h{$gold_ap_id} = $pm_name;
                }
            }

            if ($seq_st) {
                if ( $seq_strategy_h{$gold_ap_id} ) {
                    $seq_strategy_h{$gold_ap_id} .= ",$seq_st";
                } else {
                    $seq_strategy_h{$gold_ap_id} = $seq_st;
                }
            }
        }
        $cur2->finish();
    }

    my %ap_seq_center_h;
    $sql2 =
        "select unique ap.gold_id, sp.name "
      . "from gold_analysis_project ap, "
      . "gold_analysis_project_lookup2 lookup, "
      . "gold_sp_seq_center sp "
      . "where ap.gold_id is not null "
      . "and ap.gold_id = lookup.gold_id "
      . "and lookup.sp_gold_id = sp.gold_id "
      . "and sp.name is not null ";

webLog("11-- $sql2\n");      
      
    $cur2 = $dbh2->prepare($sql2);
    $cur2->execute();

    for ( ; ; ) {
        my ( $gold_ap_id, $seq_center ) = $cur2->fetchrow_array();
        if ( !$gold_ap_id ) {
            last;
        }

        if ($seq_center) {
            if ( $ap_seq_center_h{$gold_ap_id} ) {
                $ap_seq_center_h{$gold_ap_id} .= ", $seq_center";
            } else {
                $ap_seq_center_h{$gold_ap_id} = $seq_center;
            }
        }
    }
    $cur2->finish();

    my %contact_h;
    if ($long_disp) {
        $sql2 = qq{
	    select sic.submission_id, c.contact_oid, c.username
		from contact c, submission_img_contacts sic
		where sic.img_contacts = c.contact_oid
	    };

webLog("12-- $sql2\n");  	    
	    
        $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();
        my $cnt2 = 0;

        for ( ; ; ) {
            my ( $sub_id, $cid, $cname ) = $cur2->fetchrow_array();
            if ( !$sub_id || !$cid ) {
                last;
            }

            if ( $isAdmin eq 'Yes' ) {
                $cname = getContactLink( $cid, $cname );
            }

            $cnt2++;

            if ( $contact_h{$sub_id} ) {
                $contact_h{$sub_id} .= ", " . $cname;
            } else {
                $contact_h{$sub_id} = $cname;
            }
        }
        $cur2->finish();
    }

    $dbh2->disconnect();

    my %duplicate_proj_h;
    my %duplicate_gold_h;
    if (    $database eq 'IMG ER'
         && $isAdmin eq 'Yes'
         && $duplicate_check_option eq 'Yes' )
    {
        my $dbh3 = Connect_IMG_ER();
        my $sql3 = qq{
           select t.taxon_oid, t.gold_id, t.sequencing_gold_id,
                  s.project_info
           from taxon t, submission\@imgsg_dev s
           where t.obsolete_flag = 'No'
           and t.genome_type = 'isolate'
           and t.submission_id = s.submission_id (+)
           };

webLog("13-- $sql3\n");             
           
        my $cur3 = $dbh3->prepare($sql3);
        $cur3->execute();
        for ( ; ; ) {
            my ( $t_id, $g_id, $sp_id, $p_id ) = $cur3->fetchrow_array();
            last if !$t_id;
            if ( $sp_id && !$g_id ) {
                $g_id = $sp_id;
            }

            if ($g_id) {
                if ( $duplicate_gold_h{$g_id} ) {
                    $duplicate_gold_h{$g_id} .= "\t" . $t_id;
                } else {
                    $duplicate_gold_h{$g_id} = $t_id;
                }
            }
            if ($p_id) {
                if ( $duplicate_proj_h{$p_id} ) {
                    $duplicate_proj_h{$p_id} .= "\t" . $t_id;
                } else {
                    $duplicate_proj_h{$p_id} = $t_id;
                }
            }
        }
        $dbh3->disconnect();
    }

    my $cnt = 0;
    for ( ; ; ) {
        my (
             $sub_id,        $proj_id,              $analysis_project_id,
             $proj_name,     $gold_stamp_id,        $db,
             $taxon,         $is_img_public,        $contact_name,
             $contact,       $contact_username,     $sub_date,
             $status,        $app_status,           $img_dev_flag,
             $prod_flag,     $seq_status,           $gene_calling_flag,
             $subtitle,      $loaded_into_img,      $in_file,
             $scope_of_work, $culture_type,         $uncultured_type,
             $pi_email,      $replace_taxon_oid,    $is_jgi,
             $jat_key_id,    $submit_contact_email, $its_spid
        ) = $cur->fetchrow_array();
        if ( !$sub_id ) {
            last;
        }

        if ( $isAdmin eq 'Yes' ) {

            # super user
        } elsif ( $contact_oid == $contact ) {

            # submitter
        } elsif ( $submission_h{$sub_id} ) {

            # has access permission
        } elsif ( lc($pi_email) eq lc($uemail) ) {

            # PI
        } else {

            # no permission
            next;
        }

        $cnt++;

        my $row = "";

        #        $row .= $sd . "<input type='radio' name='submission_id' " .
        $row .=
            $sd
          . "<input type='checkbox' name='submission_id' "
          . "value='$sub_id' />\t";

        # submission link
        my $submit_link = getSubmissionLink( $sub_id, $database );
        $row .= $sub_id . $sd . $submit_link . "\t";

        # project link
        my $proj_id_disp = "";
        if ($proj_id) {
            $proj_id_disp = sprintf( "%05d", $proj_id );
        }
        my $proj_link = getProjectLink($proj_id);


        # analysis project id
        my $jgi_ap_id;
        my $ref_ap_id;
        my $ap_sample_oid;
        my $ap_sample_name;
        my $ap_prod_name;
        my $ap_type;
        my $submit_type;
        my $review_status;

        if ($analysis_project_id) {
            my $link =
                "<a href='"
              . $main_cgi
              . "?section=ProjectInfo&page=analysisProject"
              . "&analysis_project_id=$analysis_project_id' >"
              . $analysis_project_id . "</a>";
            $row .= $analysis_project_id . $sd . $link . "\t";

            # get project name
            if ( $use_gold_api || !$ap_name_h{$analysis_project_id} ) {
                my $res = getGoldAnalysisProject($analysis_project_id);
                if ($res) {
                    my $decode = decode_json($res);
                    if ($decode) {
                        $proj_name = $decode->{'analysisProjectName'};
                        $jgi_ap_id = $decode->{'itsAnalysisProjectId'};
                        if ( !$jgi_ap_id ) {
                            $jgi_ap_id =
                              $decode->{'itsSourceAnalysisProjectId'};
                        }
                        $ref_ap_id    = $decode->{'referenceGoldId'};
                        $ap_prod_name = $decode->{'analysisProductName'};
                        my $ap_projects = $decode->{'projects'};
                        $ap_type       = $decode->{'goldAnalysisProjectType'};
                        $submit_type   = $decode->{'submissionType'};
                        $review_status = $decode->{'reviewStatus'};
                        for my $a1 (@$ap_projects) {
                            if ( isInt( $a1->{'sampleOid'} ) ) {
                                $ap_sample_oid = $a1->{sampleOid};
                                if ( $ap_sample_oid > 0 ) {
                                    last;
                                }
                            }
                            if ( $a1->{'goldId'} ) {
                                if ( $sp_gold_id_ref_h{$analysis_project_id} ) {
                                    $sp_gold_id_ref_h{$analysis_project_id} .=
                                      "," . $a1->{'goldId'};
                                } else {
                                    $sp_gold_id_ref_h{$analysis_project_id} =
                                      $a1->{'goldId'};
                                }
                            }
                            if ( $a1->{'itsSpid'} ) {
                                $is_jgi = 'Yes';
                            }
                        }
                    }
                }
            } else {
                $proj_name = $ap_name_h{$analysis_project_id};

                $ref_ap_id      = $ap_ref_gold_h{$analysis_project_id};
                $ap_sample_name = $ap_sample_h{$analysis_project_id};
                $ap_prod_name   = $ap_prod_name_h{$analysis_project_id};
                $ap_type        = $ap_type_h{$analysis_project_id};
                $submit_type    = $submit_type_h{$analysis_project_id};
                $review_status  = $review_status_h{$analysis_project_id};
            }
        } else {
            $row .= "-" . $sd . "-" . "\t";
        }

        # analysis project type
        if ( !$ap_type ) {
            $ap_type = "-";
        }
        $row .= $ap_type . $sd . $ap_type . "\t";

        # submission type
        if ( !$submit_type ) {
            $submit_type = "-";
        }
        $row .= $submit_type . $sd . $submit_type . "\t";

        # review status
        if ( !$review_status ) {
            $review_status = "-";
        }
        $row .= $review_status . $sd . $review_status . "\t";

        ## sequencing stratgey
        if ( $isAdmin eq 'Yes' ) {
            my $seq_st = $seq_strategy_h{$analysis_project_id};
            if ( !$seq_st ) {
                $seq_st = "-";
            } else {
                $seq_st = strUniqueValue($seq_st);
            }
            $row .= $seq_st . $sd . $seq_st . "\t";
        }

        # project name
        $proj_name = strTrim($proj_name);
        if ( !blankStr($subtitle) ) {
            $subtitle = strTrim($subtitle);
            $subtitle =~ s/\#\#/\=\=/g;
            $proj_name .= " (" . $subtitle . ")";
        }

        $row .= escHtml($proj_name) . $sd . escHtml($proj_name) . "\t";

        # seq status (or sample name for omics)
        if ($omics) {
            my $sample_name = "";
            if ( $analysis_project_id && $ap_sample_h{$analysis_project_id} ) {
                $sample_name = $ap_sample_h{$analysis_project_id};
            } elsif ( $ap_sample_oid && isInt($ap_sample_oid) ) {
                $sample_name = db_getValue(
"select sample_display_name from env_sample where sample_oid = $ap_sample_oid"
                );
            }
            $sample_name = strTrim($sample_name);
            $row .= $sample_name . $sd . $sample_name . "\t";
        } else {
            $row .= $seq_status . $sd . $seq_status . "\t";
        }

        # GOLD ID
        if ($analysis_project_id) {
            my @sp_arr = ();
            if ( $sp_gold_id_ref_h{$analysis_project_id} ) {
                @sp_arr =
                  split( /\,/, $sp_gold_id_ref_h{$analysis_project_id} );
            }
            if ( scalar(@sp_arr) == 0 ) {
                $row .= "" . $sd . "" . "\t";
            } elsif ( scalar(@sp_arr) > 1 ) {
                my $s2 = scalar(@sp_arr) . " projects";
                $row .= $s2 . $sd . $s2 . "\t";
            } else {
                my $sp_gold_id = $sp_arr[0];
                my $gold_link =
                    "<a href='"
                  . $main_cgi
                  . "?section=ProjectInfo&page=sequencingProject"
                  . "&sequencing_project_id=$sp_gold_id' >"
                  . $sp_gold_id . "</a>";
                $row .= $sp_gold_id . $sd . $gold_link . "\t";
            }
        } else {
            my $gold_link = getGoldLink($gold_stamp_id);
            if ( $gold_stamp_id =~ /Gp/ ) {
                $gold_link =
                    "<a href='"
                  . $main_cgi
                  . "?section=ProjectInfo&page=sequencingProject"
                  . "&sequencing_project_id=$gold_stamp_id' >"
                  . $gold_stamp_id . "</a>";
            }
            $row .= $gold_stamp_id . $sd . $gold_link . "\t";
        }

        # ITS APID
        if ( $isAdmin eq 'Yes' ) {
            my $its_apid = $its_apid_ref_h{$analysis_project_id};
            if ( !$its_apid ) {
                $its_spid = "-";
            } else {
                $its_apid = strUniqueValue($its_apid);
            }
            $row .= $its_apid . $sd . $its_apid . "\t";
        }

        # ITS SPID
        if ( $isAdmin eq 'Yes' ) {
            my $its_spid = $its_spid_ref_h{$analysis_project_id};
            if ( !$its_spid ) {
                $its_spid = "-";
            } else {
                $its_spid = strUniqueValue($its_spid);
            }
            $row .= $its_spid . $sd . $its_spid . "\t";
        }

        # PM
        if ( $isAdmin eq 'Yes' ) {
            my $pm_names = $pm_name_h{$analysis_project_id};
            if ( !$pm_names ) {
                $pm_names = "-";
            } else {
                $pm_names = strUniqueValue($pm_names);
            }
            $row .= $pm_names . $sd . $pm_names . "\t";
        }

        # IMG taxon oid
        if ( $omics && $ref_ap_id ) {
            my $ref_taxon;
            if ($use_gold_api) {
                $ref_taxon = getGoldAnalysisField( $ref_ap_id, 'imgTaxonOid' );
            } else {
                $ref_taxon = $ap_ref_img_h{$ref_ap_id};
            }
            $row .= $ref_taxon . $sd . $ref_taxon . "\t";
        } else {
            if ( !$taxon ) {
                $taxon = "";
            }
            $row .= $taxon . $sd . $taxon . "\t";
        }

        # IMG contacts
        if ($long_disp) {
            my $canUpdate = getCanUpdateSubmission($contact_oid);
            my $con_names = "";
            if ( $canUpdate || $contact == $contact_oid ) {
                $con_names = $contact_h{$sub_id};
            } else {

                # hide info from non-admin
                $con_names = $uname;
            }
            $row .= $con_names . $sd . $con_names . "\t";
        }

        if ( !$ap_prod_name ) {
            $ap_prod_name = "-";
        }
        $row .= $ap_prod_name . $sd . $ap_prod_name . "\t";

        # submitter
        if ( !$contact_username && $submit_contact_email ) {
            my $sql4 = "select contact_oid, username from contact "
              . "where lower(email) = ?";
            my $cur4 = $dbh->prepare($sql4);
            $cur4->execute( lc($submit_contact_email) );
            ( $contact, $contact_username ) = $cur4->fetchrow();
            $cur4->finish();
        }
        if ( $isAdmin eq 'Yes' ) {
            my $contact_link = "";
            if ($contact) {
                $contact_link = getContactLink( $contact, $contact_username );
            }
            $row .= $contact_username . $sd . $contact_link . "\t";
        } else {
            $row .= $contact_username . $sd . $contact_username . "\t";
        }

        # JGI genome?
        #	if ( $analysis_project_id && $jgi_ap_id ) {
        #	    $row .= "Yes" . $sd . "Yes" . "\t";
        #	}
        #	elsif ( $proj_id && $is_jgi_genome{$proj_id} ) {
        #	    $row .= "Yes" . $sd . "Yes" . "\t";
        #	}
        #	else {
        #	    $row .= "No" . $sd . "No" . "\t";
        #	}
        if ($its_spid) {
            $is_jgi = 'Yes';
        }

        #	$row .= $is_jgi . $sd . $is_jgi . "\t";

        my $seq_center = "";
        if ($analysis_project_id) {
            $seq_center = $ap_seq_center_h{$analysis_project_id};
        }
        if ( !$seq_center && $proj_id ) {
            $seq_center = $proj_seq_center_h{$proj_id};
        }
        if ( !$seq_center ) {
            $seq_center = "-";
        }
        $row .= $seq_center . $sd . $seq_center . "\t";

        if (    $isAdmin eq 'Yes'
             && $show_jat_option eq 'Yes' )
        {
            $row .= $jat_key_id . $sd . $jat_key_id . "\t";
        }

        # duplicate genomes?
        if (    $database eq 'IMG ER'
             && $isAdmin eq 'Yes'
             && $duplicate_check_option eq 'Yes' )
        {
            my %dup_taxons;
            if ( $analysis_project_id && !$gold_stamp_id ) {
                $gold_stamp_id = $sp_gold_id_ref_h{$analysis_project_id};
            }

            if ( $gold_stamp_id && $duplicate_gold_h{$gold_stamp_id} ) {
                my @arr = split( /\t/, $duplicate_gold_h{$gold_stamp_id} );
                for my $s2 (@arr) {
                    if ( $s2 != $taxon ) {
                        $dup_taxons{$s2} = 1;
                    }
                }
            }
            if ( $proj_id && $duplicate_proj_h{$proj_id} ) {
                my @arr = split( /\t/, $duplicate_proj_h{$proj_id} );
                for my $s2 (@arr) {
                    if ( $s2 != $taxon ) {
                        $dup_taxons{$s2} = 1;
                    }
                }
            }
            my $dup_tax_str = "";
            for my $k3 ( keys %dup_taxons ) {
                if ($dup_tax_str) {
                    $dup_tax_str .= ", " . $k3;
                } else {
                    $dup_tax_str = $k3;
                }

                if ( $replace_taxon_oid
                     && ( $replace_taxon_oid eq $k3 ) )
                {
                    $dup_tax_str .= " (replacing)";
                }
            }

            if ( !$dup_tax_str ) {
                $dup_tax_str = "-";
            }
            $row .= $dup_tax_str . $sd . $dup_tax_str . "\t";
        }

        # scope of work
        if ( $isAdmin eq 'Yes' ) {
            my $cul_type = "";
            if ($culture_type) {
                $cul_type = $culture_type;
            } elsif ($uncultured_type) {
                $cul_type = $uncultured_type;
            }
            $row .= $cul_type . $sd . $cul_type . "\t";

            if ( !$scope_of_work ) {
                $scope_of_work = "-";
            }
            $row .= $scope_of_work . $sd . $scope_of_work . "\t";
        }

        # date and status
        $row .= $sub_date . $sd . $sub_date . "\t";
        $row .= $status . $sd . $status . "\t";

        # approval status
        $row .= $app_status . $sd . $app_status . "\t";

        $row .= $gene_calling_flag . $sd . $gene_calling_flag . "\t";

        #        $row .= $prod_flag . $sd . $prod_flag . "\t";

        # predicted gene count
        if ( $predicted_gene_count{$sub_id} ) {
            $row .=
                $predicted_gene_count{$sub_id}
              . $sd
              . $predicted_gene_count{$sub_id} . "\t";
        } else {
            $row .= " " . $sd . " " . "\t";
        }

        #	$row .= $loaded_into_img . $sd . $loaded_into_img . "\t";

        #	if ( $database eq 'IMG/M ER' ) {
        #	    $row .= $in_file . $sd . $in_file . "\t";
        #	}

        $it->addRow($row);
    }

    if ( $cnt > 0 ) {
        $it->printOuterTable(1);

        print "<input type='button' name='selectAll' value='Select All' "
          . "onClick='selectAllCheckBoxes(1)' class='smbutton' />\n";
        print nbsp(1);
        print "<input type='button' name='clearAll' value='Clear All' "
          . "onClick='selectAllCheckBoxes(0)' class='smbutton' />\n";
        print "<br/>\n";
    } else {
        print "<h5>No IMG submissions.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    return $cnt;
}

sub strUniqueValue {
    my ($str) = @_;

    my %h;
    my @vals = split( /\,/, $str );

    for my $v1 (@vals) {
        $h{$v1} = $v1;
    }

    my @keys = sort( keys %h );
    my $new  = "";
    for my $k (@keys) {
        if ($new) {
            $new .= ", " . $k;
        } else {
            $new = $k;
        }
    }

    return $new;
}

#########################################################################
# ExportList
##########################################################################
sub ExportList {
    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);
    my $database    = param('database');
    my $cond        = param1('submit_cond');

    # print Excel Header
    my $fileName = "submit_export$$.xls";
    print "Content-type: application/vnd.ms-excel\n";
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n";

    if ( $isAdmin ne 'Yes' ) {
        exit 0;
    }

    my $long_disp       = 0;
    my $sub_disp_option = getSessionParam('sub_disp:img_contacts');
    if ( $sub_disp_option eq 'Yes' ) {
        $long_disp = 1;
    }

    # max display
    my $max_display = getSessionParam('sub_filter:max_display');
    if ( blankStr($max_display) ) {
        $max_display = $default_max_sub_row;
    }

    my $dbh = WebFunctions::Connect_IMG;

    my $sql = qq{
	select s.submission_id, s.project_info, p.display_name,
	    p.gold_stamp_id, s.sample_oid,
	    s.database, s.img_taxon_oid, s.is_img_public,
	    p.contact_name, s.contact, c.username,
	    to_char(s.submission_date, 'yyyy-mm-dd'),
	    v.cv_term, s.approval_status,
	    s.img_dev_flag, s.img_product_flag, s.seq_status,
	    s.gene_calling_flag, s.subtitle,
            s.loaded_into_img, s.in_file
	    from submission s, submission_statuscv v, project_info p,
		contact c
	    where s.project_info = p.project_oid (+)
	    and s.contact = c.contact_oid
	    and s.status = v.term_oid (+)
	};

    if ( $contact_oid && $isAdmin ne 'Yes' ) {
        $sql .=
" and (s.contact = $contact_oid or s.submission_id in (select s2.submission_id from submission_img_contacts s2 where s2.img_contacts = $contact_oid)) ";
    }

    if ( $isAdmin eq 'Yes' && $database eq 'IMG/M ER' ) {
        $sql .= " and s.database in ('IMG/M ER', 'IMG/M HMP')";
    } elsif ( length($database) > 0 ) {
        $sql .= " and s.database = '$database'";
    }

    if ( length($cond) > 0 ) {
        $sql .= " and " . $cond;
    }
    my $cond2 = "";

    #    if ( ! $ignore_filter ) {
    $cond2 = submissionFilterCondition();

    #    }

    if ( !blankStr($cond2) ) {

#	print "<p><font color='red'>Filter is on. Some submissions may be hidden. Reset the filter to see more submissions.</font></p>\n";

        $sql .= " and " . $cond2;
    }

    $sql .= " order by s.submission_id desc";

webLog("750-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my @all_attrs = (
                      'submission_id', 'project_info',
                      'display_name',  'seq_status',
                      'gold_stamp_id', 'img_taxon_oid'
    );
    for my $attr1 (@all_attrs) {
        my $disp_name = getAttrDispName($attr1);
        print $disp_name . "\t";
    }

    if ($long_disp) {
        print "IMG Contact Name(s)\t";
    }

    print "Submitter\t";

    @all_attrs = (
                   'submission_date', 'status',
                   'approval_status', 'gene_calling_flag',
                   'img_product_flag'
    );
    for my $attr1 (@all_attrs) {
        my $disp_name = getAttrDispName($attr1);
        print $disp_name . "\t";
    }
    print "Load into IMG\t";
    print "Load into MER-FS\t";
    print "\n";

    my %contact_h;
    if ($long_disp) {
        my $dbh2 = WebFunctions::Connect_IMG();
        my $sql2 = qq{
	    select sic.submission_id, c.contact_oid, c.username
		from contact c, submission_img_contacts sic
		where sic.img_contacts = c.contact_oid
		order by 1, 2
	    };
	    
webLog("800-- $sql2\n");	    
	    
        my $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();
        my $cnt2 = 0;

        for ( ; ; ) {
            my ( $sub_id, $cid, $cname ) = $cur2->fetchrow_array();
            if ( !$sub_id || !$cid ) {
                last;
            }

            if ( $isAdmin eq 'Yes' ) {
                $cname = getContactLink( $cid, $cname );
            }

            $cnt2++;

            if ( $contact_h{$sub_id} ) {
                $contact_h{$sub_id} .= ", " . $cname;
            } else {
                $contact_h{$sub_id} = $cname;
            }
        }
        $cur2->finish();
        $dbh2->disconnect();
    }

    my $cnt = 0;

    for ( ; ; ) {
        my (
             $sub_id,        $proj_id,          $proj_name,
             $gold_stamp_id, $sample_oid,       $db,
             $taxon,         $is_img_public,    $contact_name,
             $contact,       $contact_username, $sub_date,
             $status,        $app_status,       $img_dev_flag,
             $prod_flag,     $seq_status,       $gene_calling_flag,
             $subtitle,      $loaded_into_img,  $in_file
        ) = $cur->fetchrow_array();
        if ( !$sub_id ) {
            last;
        }

        $cnt++;

        my $row = "";

        print $sub_id . "\t";

        if ($sample_oid) {
            my $samp_id_disp = sprintf( "%05d", $sample_oid );
            print "$proj_id (Sample: $samp_id_disp)\t";
        } else {
            print $proj_id . "\t";
        }

        # project name
        if ( !blankStr($subtitle) ) {
            $proj_name .= " (" . $subtitle . ")";
        }

        print $proj_name . "\t";

        # seq status
        print $seq_status . "\t";

        # GOLD ID
        print $gold_stamp_id . "\t";

        # IMG taxon oid
        print $taxon . "\t";

        # IMG contacts
        if ($long_disp) {
            my $canUpdate = getCanUpdateSubmission($contact_oid);
            my $con_names = "";
            if ( $canUpdate || $contact == $contact_oid ) {
                $con_names = $contact_h{$sub_id};
            } else {

                # hide info from non-admin
                $con_names = "-";
            }
            print $con_names . "\t";
        }

        # submitter
        print $contact_username . "\t";

        # date and status
        print $sub_date . "\t";
        print $status . "\t";

        # approval status
        print $app_status . "\t";

        print $gene_calling_flag . "\t";
        print $prod_flag . "\t";
        print $loaded_into_img . "\t";
        print $in_file . "\t";

        print "\n";
    }

    $cur->finish();
    $dbh->disconnect();

    exit 0;
}

#############################################################################
# ShowStatus - show submission status
#############################################################################
sub ShowStatus {
    my ($submission_id) = @_;

    print start_form(
                      -name   => 'userProfile',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin   = getIsAdmin($contact_oid);
    my $canUpdate = getCanUpdateSubmission($contact_oid);

    if ( blankStr($submission_id) ) {
        $submission_id = param1('submission_id');
    }

    if ( blankStr($submission_id) ) {
        printError("No submission ID has been selected.");
        print end_form();
        return;
    }
    print hiddenVar( 'submission_id', $submission_id );

    #    if ( $isAdmin eq 'Yes' ) {
    if ($canUpdate) {
        EditStatus($submission_id);
    } else {
        DisplayStatus($submission_id);
    }

    print end_form();
}

#############################################################################
# DisplayStatus - display submission status for a non-admin user
#############################################################################
sub DisplayStatus {
    my ($submission_id) = @_;

    if ( blankStr($submission_id) ) {
        printError("No submission ID has been selected.");
        return;
    }

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    print start_form(
                      -name   => 'displayStatus',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    print "<h2>Submission Status for $submission_id :</h2>\n";
    print hiddenVar( 'submission_id', $submission_id );

    my $project_oid = 0;

    my $def_subm = def_Submission();
    my @attrs    = @{ $def_subm->{attrs} };

    my %db_val = SelectSubmission($submission_id);

    # samples?
    my $dbh2 = Connect_IMG();
    my $sql2 =
        "select e.sample_oid, e.sample_display_name "
      . "from submission_samples s, env_sample e "
      . "where s.submission_id = $submission_id "
      . "and s.sample_oid = e.sample_oid";
      
webLog("810-- $sql2\n");      
      
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute();
    my %sample_h;
    for ( ; ; ) {
        my ( $sam_id, $sample_name ) = $cur2->fetchrow();
        last if !$sam_id;

        $sample_h{$sam_id} = $sample_name;
    }
    $cur2->finish();
    $dbh2->disconnect();

    my @sample_ids = ();
    if ( scalar( keys %sample_h ) > 0 ) {
        @sample_ids = sort ( keys %sample_h );
        my $sample_list = join( ", ", @sample_ids );
        $db_val{'sample_oid'} = $sample_list;
    }

    print "<p>\n";
    print "<table class='img' border='1'>\n";

    my @tabs = (
                 'Submission Information',
                 'Submit annotated file',
                 'Submit sequence file',
                 'Functional annotation options',
                 'Advanced options',
                 'Database Info'
    );
    my $database = $db_val{'database'};
    my $section  = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';

        #	push @tabs, ( 'Additional Metagenome Files' );
    }

    for my $tab (@tabs) {
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>"
          . "<font color='darkblue'>"
          . $tab
          . "</font></th>\n";
        print "  <td class='img'   align='left' bgcolor='lightblue'>"
          . "</td>\n";
        print "</tr>\n";

        for my $k (@attrs) {
            if ( $k->{tab} ne $tab ) {
                next;
            }

            my $attr_val  = "";
            my $attr_name = $k->{name};

            if ( $attr_name eq 'img_dev_flag' ) {
                if ( $isAdmin eq 'Yes' ) {

                    # only admin can see this
                } else {
                    next;
                }
            }

            if ( $attr_name eq 'remove_duplicate_flag' ) {
                if ( $isAdmin eq 'Yes' ) {

                    # only admin can see this
                } else {
                    next;
                }
            }

            if ( $db_val{$attr_name} ) {
                $attr_val = $db_val{$attr_name};
            }

            if ( !$attr_val || blankStr($attr_val) ) {
                next;
            }

            my $disp_name = $k->{display_name};

            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>$disp_name</th>\n";

            if ( $attr_name eq 'project_info' ) {

                # project
                print "  <td class='img'   align='left'>"
                  . getProjectLink($attr_val)
                  . "</td>\n";
            } elsif ( $attr_name eq 'prev_submission_id' ) {

                # previous submission ID
                my $prev_submit_link =
                  getSubmissionLink( $attr_val, $database );
                print "  <td class='img'   align='left'>"
                  . $prev_submit_link
                  . " </td>\n";
            } elsif (    $attr_name eq 'stats_file'
                      || $attr_name eq 'error_file' )
            {
                # stats_file or error_file
                print "  <td class='img'   align='left'>";
                if ( !blankStr($attr_val) ) {
                    print "<a href='"
                      . $main_cgi
                      . "?section=$section&page=displayFile"
                      . "&submission_id=$submission_id"
                      . "&file_type=$attr_name' >"
                      . escapeHTML($attr_val) . "</a>";
                }
                print "</td>\n";
            } elsif ( $attr_name eq 'status' ) {
                my $display_status = "";
                if ( length($attr_val) > 0 && isInt($attr_val) ) {
                    $display_status = db_getValue(
"select cv_term from submission_statuscv where term_oid = $attr_val"
                    );
                }
                print "  <td class='img'   align='left'>"
                  . $attr_val . " - "
                  . escapeHTML($display_status)
                  . "</td>\n";
            } elsif (    $attr_val =~ /^http\:\/\// || $attr_val =~ /^https\:\/\// )
            {
                # bug fix https url - ken
                
                # url
                print "  <td class='img'   align='left'>"
                  . alink( $attr_val, 'URL', 'target', 1 )
                  . "</td>\n";
            } else {

                # regular attribute value
                print "  <td class='img'   align='left'>"
                  . escapeHTML($attr_val)
                  . "</td>\n";
            }

            print "</tr>\n";

            if ( $attr_name eq 'contact' ) {

                # show IMG contacts here
                my @contact_oids = db_getValues(
"select img_contacts from submission_img_contacts where submission_id = $submission_id"
                );
                my $con_names = "";
                for my $cid (@contact_oids) {
                    if ( $isAdmin ne 'Yes' && $cid != $contact_oid ) {
                        next;
                    }
                    my $cname = db_getContact($cid);
                    if ( length($con_names) == 0 ) {
                        $con_names = $cname;
                    } else {
                        $con_names .= ", " . $cname;
                    }
                }

                print "<tr class='img' >\n";
                print
"  <th class='subhead' align='right'>IMG Contact Name(s)</th>\n";
                print "  <td class='img'   align='left'>"
                  . escapeHTML($con_names)
                  . "</td>\n";
                print "</tr>\n";
            }
        }
    }

    # show READS files, if any
    my $dbh = Connect_IMG();
    my $sql =
        "select submission_id, reads_file "
      . "from submission_reads_file "
      . "where submission_id = "
      . $submission_id;

webLog("820-- $sql\n");      
      
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( my $j2 = 0 ; $j2 < 5 ; $j2++ ) {
        my ( $id2, $name2 ) = $cur->fetchrow_array();
        last if !$id2;

        if ( $j2 == 0 ) {

            # first value
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>"
              . "READS Files"
              . "</th>\n";
            print "  <td class='img'   align='left'>";
        } else {

            # print row separator
            print "<br/>\n";
        }

        print escapeHTML($name2);
    }
    $cur->finish();
    $dbh->disconnect();

    print "</table>\n";

    print
"<input type='submit' name='_section_$section:showStats' value='Show Statistics' class='meddefbutton' />";
    print "<p>\n";

    DisplaySubmissionHistory($submission_id);

    #    if ( $project_oid ) {
    #	DisplayMProjectInfo($project_oid);
    #    }

    print "<p>\n";
    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:showMPage" value="OK" class="smbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:showERPage" value="OK" class="smbutton" />';
    }

    printHomeLink();

    print end_form();
}

#############################################################################
# EditStatus - display submission status for admin user
#############################################################################
sub EditStatus {
    my ($submission_id) = @_;

    if ( blankStr($submission_id) ) {
        printError("No submission ID has been selected.");
        return;
    }

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    print start_form(
                      -name   => 'editStatus',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    print "<h2>Submission Status for $submission_id:</h2>\n";
    print hidden( 'submission_id', $submission_id );

    saveSubmissionPageInfo();

    my $project_oid = 0;

    my $def_subm = def_Submission();
    my @attrs    = @{ $def_subm->{attrs} };

    my %db_val   = SelectSubmission($submission_id);
    my $database = $db_val{'database'};

    # samples?
    my $dbh2 = Connect_IMG();
    my $sql =
        "select e.sample_oid, e.sample_display_name "
      . "from submission_samples s, env_sample e "
      . "where s.submission_id = $submission_id "
      . "and s.sample_oid = e.sample_oid";
      
webLog("830-- $sql\n");      
      
    my $cur = $dbh2->prepare($sql);
    $cur->execute();
    my %sample_h;
    for ( ; ; ) {
        my ( $sam_id, $sample_name ) = $cur->fetchrow();
        last if !$sam_id;

        $sample_h{$sam_id} = $sample_name;
    }
    $cur->finish();
    $dbh2->disconnect();

    my @sample_ids = ();
    if ( scalar( keys %sample_h ) > 0 ) {
        @sample_ids = sort ( keys %sample_h );
        my $sample_list = join( ", ", @sample_ids );
        $db_val{'sample_oid'} = $sample_list;
    }

    if ( $database eq 'IMG ER' ) {
        my $new_text = qq{
	    IMG sequencing status and the topology of the replicons 
		can be related to the Finishing Level according
		to Chain et al. (Science  9 October 2009:Vol. 326 no. 5950 pp. 236-237) 
		using the following table. 
		
		<table class='img'>
		<th class='img'>Finishing level per Chain et al.</th>
		<th class='img'>IMG Seq status</th>
		<th class='img'>Topology</th>
		<tr class='img'><td class='img'>Standard Draft (SD)</td>
		<td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		<tr><td class='img'>High Quality Draft (HQD)</td>
		<td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		<tr><td class='img'>Improved High Quality Draft (IHQD)</td>
		<td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		<tr><td class='img'>Annotation Directed Improvement (ADI)</td>
		<td class='img'>Permanent Draft</td><td class='img'>linear</td></tr>
		<tr><td class='img'>Non-contiguous Finished (NF)</td>
		<td class='img'>Permanent Draft</td><td class='img'>circular</td></tr>
		<tr><td class='img'>Finished (F)</td>
		<td class='img'>Finished</td><td class='img'>circular</td></tr>
		</table>

		If you have reasons to believe that this correspondence
		does not apply to this project (e.g., you have a project that is 
			Finished but the replicons are linear),
		    use the corresponding options to set Sequencing status and Topology
		    at your own risk.
		};

        printHint3($new_text);
    }

    print "<p>\n";
    print "<table class='img' border='1'>\n";

    my @tabs = (
                 'Submission Information',
                 'Submit annotated file',
                 'Submit sequence file',
                 'Functional annotation options',
                 'Advanced options',
                 'Database Info'
    );

    #    if ( $database eq 'IMG/M ER' ) {
    #	push @tabs, ( 'Additional Metagenome Files' );
    #    }

    for my $tab (@tabs) {
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>"
          . "<font color='darkblue'>"
          . $tab
          . "</font></th>\n";
        print "  <td class='img'   align='left' bgcolor='lightblue'>"
          . "</td>\n";
        print "</tr>\n";

        for my $k (@attrs) {
            if ( $k->{tab} ne $tab ) {
                next;
            }

            my $attr_val  = "";
            my $attr_name = $k->{name};
            if ( $db_val{$attr_name} ) {
                $attr_val = $db_val{$attr_name};
            }

            my $disp_name = $k->{display_name};
            my $data_type = $k->{data_type};
            my $len       = $k->{length};

            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>$disp_name";
            if ( $attr_name eq 'species_code' ) {
                print "<br/>(* required if gene calling is needed)";
            } elsif ( $k->{is_required} ) {
                print " (*) ";
            }
            print "</th>\n";

            print "  <td class='img'   align='left'>\n";

            if ( $k->{can_edit} || $attr_name eq 'status' ) {
                if ( $data_type =~ /\|/ ) {

                    # selection
                    my @selects = split( /\|/, $data_type );
                    print "<select name='$attr_name' class='img' size='1'>\n";
                    for my $s0 (@selects) {
                        print "    <option value='" . escapeHTML($s0) . "'";
                        if ( $s0 eq $attr_val ) {
                            print " selected ";
                        }
                        print ">$s0</option>\n";
                    }
                    print "</select>\n";
                } elsif ( $data_type eq 'cv2' ) {
                    print "<select name='$attr_name' class='img' size='1'>\n";
                    if ( !$k->{is_required} ) {
                        print "   <option value=''> </option>\n";
                    }

                    my $sql2 = $k->{cv_query};
                    my $dbh2 = Connect_IMG();
                    
webLog("840-- $sql2\n");                    
                    
                    my $cur2 = $dbh2->prepare($sql2);
                    $cur2->execute();

                    for ( my $j2 = 0 ; $j2 <= 10000 ; $j2++ ) {
                        my ( $id2, $name2 ) = $cur2->fetchrow_array();
                        if ( !$id2 ) {
                            last;
                        }

                        print "    <option value='" . escapeHTML($id2) . "'";
                        if ( !blankStr($attr_val) && $attr_val eq $id2 ) {
                            print " selected ";
                        }
                        print ">$id2 - $name2</option>\n";
                    }
                    print "</select>\n";
                    $cur2->finish();
                    $dbh2->disconnect();
                } elsif ( $data_type eq 'cv' && $k->{cv_query} ) {

                    # cv
                    my @cv_vals = db_getValues( $k->{cv_query} );

                    #		    if ( $attr_name eq 'gene_calling_flag' &&
                    #			 $isAdmin eq 'Yes' ) {
                    #			push @cv_vals, ( 'Glimmer', 'RNA gene calling' );
                    #		    }

                    print "<select name='$attr_name' class='img' size='1'>\n";
                    if ( !$k->{is_required} ) {
                        print "   <option value=''> </option>\n";
                    }
                    for my $s0 (@cv_vals) {
                        print "    <option value='" . escapeHTML($s0) . "'";
                        if ( $s0 eq $attr_val ) {
                            print " selected ";
                        }
                        print ">$s0</option>\n";
                    }
                    print "</select>\n";
                } elsif ( $data_type eq 'list' ) {

                    # selection
                    my @selects = split( /\|/, $k->{list_values} );
                    print "<select name='$attr_name' class='img' size='1'>\n";
                    if ( !$k->{is_required} ) {
                        print "   <option value=''> </option>\n";
                    }
                    for my $s0 (@selects) {
                        print "    <option value='" . escapeHTML($s0) . "'";
                        if ( !blankStr($attr_val) && $attr_val eq $s0 ) {
                            print " selected ";
                        }
                        print ">$s0</option>\n";
                    }
                    print "</select>\n";
                } else {
                    my $size = 80;
                    if ( $len && $size > $len ) {
                        $size = $len;
                    }

                    print "<input type='text' name='$attr_name' value='";
                    if ( $attr_val && !blankStr($attr_val) ) {
                        print escapeHTML($attr_val);
                    }
                    print "' size='$size' maxLength='$len'/>";
                }
            } else {
                print escapeHTML($attr_val);
            }
            print "</td>\n";

            print "</tr>\n";

            if ( $attr_name eq 'contact' ) {

                # show IMG contacts here
                my @contact_oids = db_getValues(
"select img_contacts from submission_img_contacts where submission_id = $submission_id"
                );
                my $con_names = "";
                for my $cid (@contact_oids) {
                    if ( $isAdmin ne 'Yes' && $cid != $contact_oid ) {
                        next;
                    }
                    my $cname = db_getContact($cid);
                    if ( length($con_names) == 0 ) {
                        $con_names = $cname;
                    } else {
                        $con_names .= ", " . $cname;
                    }
                }

                print "<tr class='img' >\n";
                print
"  <th class='subhead' align='right'>IMG Contact Name(s)</th>\n";
                print "  <td class='img'   align='left'>"
                  . escapeHTML($con_names)
                  . "</td>\n";
                print "</tr>\n";
            }
        }

        if ( $tab eq 'Advanced options' ) {

            # show READS Files for editing
            if ( $database eq 'IMG/M ER' ) {
                EditReadsFiles($submission_id);
            }
        }
    }    # end for tab

    print "</table>\n";

    my $section = 'ERSubmission';
    my $page2   = 'showERPage';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
        $page2   = 'showMPage';
    }

    print
"<input type='submit' name='_section_$section:showStats' value='Show Statistics' class='meddefbutton' />";
    print "<p>\n";

    DisplaySubmissionHistory($submission_id);

    #    if ( $project_oid ) {
    #	DisplayMProjectInfo($project_oid);
    #    }

    # send email?
    print "<p>\n";
    print "<input type='checkbox' name='email_contact' />";
    print nbsp(1);
    print "Send email to inform contact(s)</p>\n";

    print "<p>\n";

    print
"<input type='submit' name='_section_$section:updateStatus' value='Update Submission Info' class='meddefbutton' />";
    print "&nbsp; \n";
    print
"<input type='submit' name='_section_$section:updateContact' value='Update IMG Contact(s)' class='medbutton' />";
    print "&nbsp; \n";
    print
"<input type='submit' name='_section_$section:$page2' value='Cancel' class='smbutton' />";

    printHomeLink();

    print end_form();
}

##########################################################################
# EditReadsFiles
##########################################################################
sub EditReadsFiles {
    my ($submission_id) = @_;

    my $def_aux = def_Class('submission_reads_file');
    if ( !$def_aux ) {
        return;
    }

    my $j = 0;

    if ($submission_id) {

        # there is a submission id
        my $id_attr  = $def_aux->{id};
        my $sql      = "select $id_attr";
        my $order_by = " order by $id_attr";

        my @aux_attrs = @{ $def_aux->{attrs} };
        my $attr1;
        for my $attr (@aux_attrs) {
            if ( $attr->{name} eq $id_attr ) {
                next;
            }

            $attr1 = $attr;
            $sql      .= ", " . $attr->{name};
            $order_by .= ", " . $attr->{name};
        }

        $sql .=
            " from submission_reads_file where "
          . $id_attr . " = "
          . $submission_id
          . $order_by;
        my $dbh = Connect_IMG();
        
webLog("850-- $sql\n");        
        
        my $cur = $dbh->prepare($sql);
        $cur->execute();

        for ( $j = 0 ; $j < $def_aux->{new_rows} ; $j++ ) {
            my ( $id_val, @vals ) = $cur->fetchrow_array();
            last if !$id_val;

            if ( $j == 0 ) {

                # first value
                print "<tr class='img' >\n";
                print "  <th class='subhead' align='right'>";

                #		if ( $def_aux->{tooltip} ) {
                #		    printTooltip($def_aux->{display_name},
                #				 $def_aux->{tooltip});
                #		}
                #		else {
                print escapeHTML( $def_aux->{display_name} );

                #		}

                print "<br/><br/> (*** Note: READ Files are used by Ernest.)";
                print "</th>\n";
                print "  <td class='img'   align='left'>";
            } else {

                # print row separator
                print "<br/>\n";
            }

            my $val = "";
            if ( scalar(@vals) > 0 ) {
                $val = $vals[0];
                my $fld_name = 'reads_file_' . $j;
                print "<input type='text' name='$fld_name' value='";
                if ( $val && !blankStr($val) ) {
                    print escapeHTML($val);
                }
                print "' size='60' maxLength='255'/>";
            }
        }    # end for j

        $cur->finish();
        $dbh->disconnect();
    }    # end if submisison id

    # extra new rows
    while ( $j < $def_aux->{new_rows} ) {
        if ( $j == 0 ) {

            # first value
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>";

            #	    if ( $def_aux->{tooltip} ) {
            #		printTooltip($def_aux->{display_name},
            #			     $def_aux->{tooltip});
            #	    }
            #	    else {
            print escapeHTML( $def_aux->{display_name} );

            #	    }

            print "<br/><br/> (*** Note: READ Files are used by Ernest.)";
            print "</th>\n";
            print "  <td class='img'   align='left'>";
        } else {

            # print row separator
            print "<br/>\n";
        }

        my $fld_name = 'reads_file_' . $j;
        print "<input type='text' name='$fld_name' value='";
        print "' size='60' maxLength='255'/>";

        $j++;
    }    # end while j

    if ( $j > 0 ) {
        print "</td></tr>\n";
    }

}

#############################################################################
# DisplayMProjectInfo - display metagenome project info
#############################################################################
sub DisplayMProjectInfo {
    my ($project_oid) = @_;

    if ( blankStr($project_oid) ) {
        return;
    }

    print hr();
    print "<h2>Metagenome Project $project_oid:</h2>\n";

    my %attrs = MicrobiomeProjAttrs();
    my @keys  = keys %attrs;

    my $sql = "select project_oid";
    for my $k ( sort @keys ) {
        my ( $order, $attr_name ) = split( / /, $k );
        $sql .= ", $attr_name";
    }
    $sql .= " from microbiome_project where project_oid = $project_oid";

    my $dbh = WebFunctions::Connect_IMG;
    
webLog("860-- $sql\n");    
    
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my @db_vals = $cur->fetchrow_array();
    $cur->finish();

    if ( scalar(@db_vals) == 0 ) {
        $dbh->disconnect();
        return;
    } elsif ( !$db_vals[0] ) {
        $dbh->disconnect();
        return;
    }

    print "<p>\n";
    print "<table class='img' border='1'>\n";

    # project oid
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>Project ID</th>\n";
    print "  <td class='img'   align='left'>"
      . escapeHTML($project_oid)
      . "</td>\n";
    print "</tr>\n";

    # project info
    my $j = 1;
    for my $k ( sort @keys ) {
        my $attr_val = "";
        my ( $order, $attr_name ) = split( / /, $k );
        if ( $j < scalar(@db_vals) ) {
            $attr_val = $db_vals[$j];
        }
        if ( !$attr_val || blankStr($attr_val) ) {
            next;
        }

        my ( $disp_name, $data_type, $len, $edit ) = split( /\t/, $attrs{$k} );

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>$disp_name</th>\n";
        print "  <td class='img'   align='left'>"
          . escapeHTML($attr_val)
          . "</td>\n";
        print "</tr>\n";
        $j++;
    }
    print "</table>\n";

    # show samples, if any
    $sql = "select sample_oid, sample_display_name from env_sample "
      . "where microbiome_project = $project_oid order by sample_oid";
    print "<p>\n";

    my $cnt = 0;

webLog("870-- $sql\n");    
    
    $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ( $sample_oid, $sample_name ) = $cur->fetchrow_array();
        last if !$sample_oid;

        if ( $cnt == 0 ) {
            print "<h3>Samples in this project</h3>\n";
            print "<p>\n";
        }
        $cnt++;

        print nbsp(1);
        print "Sample $sample_oid - ";
        print nbsp(1);
        print escapeHTML($sample_name);
        print "<br>\n";
    }
    $cur->finish();
    $dbh->disconnect();

    if ( $cnt == 0 ) {
        print "<h3>No samples are in this project.</h3>\n";
    }
}

####################################################################
# select Submission data given an ID
####################################################################
sub SelectSubmission {
    my ($submission_id) = @_;

    my %db_val;

    my $dbh = Connect_IMG();

    my $db_id = $submission_id;

    my $def_subm = def_Submission();
    my @attrs    = @{ $def_subm->{attrs} };

    my $sql = "";
    for my $k (@attrs) {
        my $attr_name = $k->{name};

        $attr_name = 's.' . $attr_name;

        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } else {
            $sql .= ", " . $attr_name;
        }
    }

    ## for v5.2
    $sql .= ", s.gff_file, s.protein_faa_file";
    $sql .= " from submission s where s.submission_id = $db_id";

##    print "<p>SQL: $sql</p>\n";

webLog("870-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my @flds = $cur->fetchrow_array();

    # save result
    my $j = 0;
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        if ( scalar(@flds) < $j ) {
            last;
        }

        if (    $attr_name eq 'contact'
             || $attr_name eq 'approved_by'
             || $attr_name eq 'modified_by' )
        {
            $db_val{$attr_name} = db_getContact( $flds[$j] );
        } else {
            $db_val{$attr_name} = $flds[$j];
        }
        $j++;
    }

    ## for v5.2
    $db_val{'gff_file'} = $flds[$j];
    $j++;
    $db_val{'protein_faa_file'} = $flds[$j];

    # finish
    $cur->finish();
    $dbh->disconnect();

    return %db_val;
}

#############################################################################
# dbUpdateStatus - update submission status
#############################################################################
sub dbUpdateStatus {

    my $msg = "";

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        $msg = "Unknown username / password";
        return $msg;
    }

    my $isAdmin   = getIsAdmin($contact_oid);
    my $canUpdate = getCanUpdateSubmission($contact_oid);

    #    if ( $isAdmin ne 'Yes' ) {
    if ( !$canUpdate ) {
        $msg = "You do not have the privilege to update submission status.";
        return $msg;
    }

    my $submission_id = param1('submission_id');
    if ( blankStr($submission_id) ) {
        $msg = "No submission ID has been selected.";
        return $msg;
    }

    my $new_status = param1('status');

    # check input
    my $def_subm = def_Submission();
    my @attrs    = @{ $def_subm->{attrs} };
    for my $k (@attrs) {
        if (    $new_status == 5
             || $new_status == 8
             || $new_status == 9
             || $new_status == 10
             || $new_status == 14
             || $new_status == 16
             || $new_status == 90 )
        {
            # do not check
        }

        if ( !$k->{can_edit} ) {
            next;
        }

        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};
        my $v         = param1($attr_name);
        if ( $k->{is_required} && blankStr($v) ) {
            $msg = $k->{display_name} . " cannot be null.";
            return $msg;
        }

        if ( blankStr($v) ) {
            next;
        }

        if ( $data_type eq 'int' ) {
            if ( !isInt($v) ) {
                $msg = $k->{display_name} . " must be an integer.";
                return $msg;
            }
        } elsif ( $data_type eq 'number' ) {
            if ( !isNumber($v) ) {
                $msg = $k->{display_name} . " must be a number.";
                return $msg;
            }
        } elsif ( $data_type eq 'file' ) {
            if ( $v =~ /^http\:\/\// || $v =~ /^https\:\/\// || $v =~ /^ftp\:\/\// ) {
# bug fix https url - ken
                # no need to check http or ftp
            } else {

                # check file
                my $new_name = validPathName($v);

                # check whether file exists
                # skip checking file exist for now, because UI cannot access to
                # some NERSC file directories
                #		if ( -e $v ) {
                #		    # file exists
                #		}
                #		else {
                #		    $msg = $k->{display_name} . " does not exist.";
                #		    return $msg;
                #		}

             # disable file name checking here
             #		if ( $new_name ne $v ) {
             #		    $msg = $k->{display_name} . " contains illegal characters.";
             #		    return $msg;
             #		}
            }
        }
    }

    # check species code?
    if (    $new_status == 5
         || $new_status == 8
         || $new_status == 9
         || $new_status == 10
         || $new_status == 14
         || $new_status == 16
         || $new_status == 90 )
    {
        # no need to check species code
    } elsif ( param('species_code') ) {

        # has code
        if ( checkLocusTag( param1('species_code') ) ) {

            # correct locus tag prefix
        } else {
            print
"<h4>Error: The locus_tag prefix must be 3-12 alphanumeric characters and the first character may not be a digit. Do not put '_' at the end of prefix.</h4>\n";
            print end_form();
            return;
        }
    }

    #    else {
    #	# check gene calling
    #	my $gc = param1('gene_calling_flag');
    #	if ( $gc ne 'No' && $gc ne 'Unknown' && $gc ne 'No gene calling' ) {
##	    $msg = "Unique species code / locus_tag prefix cannot be null when gene calling is needed.";
    #	    return $msg;
    #	}
    #    }

    # get previous approval status
    my $apv_status = db_getValue(
"select approval_status from submission where submission_id = $submission_id"
    );

    # database update
    my $sql = "update submission set mod_date = sysdate";
    $sql .= ", modified_by = $contact_oid";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};

        if ( $attr_name eq 'approval_status' ) {
            my $val = param1($attr_name);
            if ( blankStr($val) ) {

                # there is no status
                if ( !blankStr($apv_status) ) {
                    $sql .= ", $attr_name = null, approved_by = $contact_oid"
                      . ", approval_date = sysdate";
                }
            } else {

                # there is a status
                if ( blankStr($apv_status) || $apv_status ne $val ) {
                    $val =~ s/'/''/g;    # replace ' with ''
                    $sql .=
                        ", $attr_name = '"
                      . $val
                      . "', approved_by = $contact_oid"
                      . ", approval_date = sysdate";
                }
            }
        } elsif ( $k->{can_edit} || $attr_name eq 'status' ) {
            my $val = param1($attr_name);
            if ( blankStr($val) ) {
                $sql .= ", $attr_name = null";
            } elsif ( $data_type eq 'int' || $data_type eq 'number' ) {
                $sql .= ", $attr_name = $val";
            } else {
                $val =~ s/'/''/g;    # replace ' with ''
                $sql .= ", $attr_name = '" . $val . "'";
            }
        }
    }

    $sql .= " where submission_id = $submission_id";

    my @sqlList = ($sql);

    $sql = "delete from submission_reads_file where submission_id = "
      . $submission_id;
    push @sqlList, ($sql);

    for ( my $k = 0 ; $k < 5 ; $k++ ) {
        my $name0 = "reads_file_" . $k;
        if ( param1($name0) ) {
            my $n = param1($name0);
            $n =~ s/'/''/g;    # replace ' with ''
            $sql =
                "insert into submission_reads_file ("
              . "submission_id, reads_file) values ("
              . $submission_id . ", '"
              . $n . "')";
            push @sqlList, ($sql);
        }
    }

    EmailContact();

    db_sqlTrans( \@sqlList );

    return $msg;
}

##########################################################################
# DisplaySubmission
##########################################################################
sub DisplaySubmission {
    my ($submission_id) = @_;

    print start_form(
                      -name   => 'displaySubmission',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        printError("Unknown username / password");
        return;
    }

    if ( !$submission_id || !isInt($submission_id) ) {
        return;
    }

    # check whether user can view this project
    my $isAdmin   = getIsAdmin($contact_oid);
    my $canUpdate = getCanUpdateSubmission($contact_oid);

    #    if ( $isAdmin eq 'No' ) {
    if ( !$canUpdate ) {

        # contact must be the owner of this submission
        my $project_contact = db_getValue(
"select contact from submission where submission_id = $submission_id"
        );
        if ( $project_contact == $contact_oid ) {

            # fine
        } else {

            # check submission_img_contacts
            my $cnt0 = db_getValue(
"select count(*) from submission_img_contacts where submission_id = $submission_id and img_contacts = $contact_oid"
            );
            if ( $cnt0 == 0 ) {

                # check PI email
                my $email = db_getValue(
                    "select email from contact where contact_oid = $contact_oid"
                );
                my $pi_email = db_getValue(
"select p.contact_email from project_info p, submission s where s.submission_id = $submission_id and s.project_info = p.project_oid"
                );
                if ( lc($email) eq lc($pi_email) ) {

                    # fine -- PI
                } else {
                    printError("You cannot view this submission.");
                    return;
                }
            }
        }
    }

    print "<h2>Submission $submission_id</h2>\n";
    print hidden( 'submission_id', $submission_id );

    # get Submission definition
    my $def_submit = def_Submission();
    my @attrs      = @{ $def_submit->{attrs} };

    # get data from database
    my %db_val   = SelectSubmission($submission_id);
    my $database = $db_val{'database'};
    my $section  = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }


    my %sample_h;



    my @sample_ids = ();
    if ( scalar( keys %sample_h ) > 0 ) {
        @sample_ids = sort ( keys %sample_h );
        my $sample_list = join( ", ", @sample_ids );
        $db_val{'sample_oid'} = $sample_list;
    }

    print "<p>\n";
    print "<table class='img' border='1'>\n";

    my @tabs = (
                 'Submission Information',
                 'Submit annotated file',
                 'Submit sequence file',
                 'Functional annotation options',
                 'Advanced options',
                 'Database Info'
    );

    #    if ( $database eq 'IMG/M ER' ) {
    #	push @tabs, ( 'Additional Metagenome Files' );
    #    }

    for my $tab (@tabs) {
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>"
          . "<font color='darkblue'>"
          . $tab
          . "</font></th>\n";
        print "  <td class='img'   align='left' bgcolor='lightblue'>"
          . "</td>\n";
        print "</tr>\n";

        if ( $tab eq 'Submit annotated file' ) {
            ## for V5.2
            my $gff_file = $db_val{'gff_file'};
            if ($gff_file) {
                print "<tr>\n";
                print "  <td class='img'   align='left'><b>GFF File</b></td>\n";
                print "  <td class='img'   align='left'>$gff_file</td>\n";
                print "</tr>\n";
            }
            my $protein_faa_file = $db_val{'protein_faa_file'};
            if ($protein_faa_file) {
                print "<tr>\n";
                print
"  <td class='img'   align='left'><b>Protein faa File</b></td>\n";
                print
                  "  <td class='img'   align='left'>$protein_faa_file</td>\n";
                print "</tr>\n";
            }
        }

        for my $k (@attrs) {
            if ( $k->{tab} ne $tab ) {
                next;
            }

            my $attr_val  = "";
            my $attr_name = $k->{name};

            ## we no longer show old Submission Project or Sample
            if (    $attr_name eq 'project_info'
                 || $attr_name eq 'sample_oid' )
            {
                next;
            }

            if ( $attr_name eq 'img_dev_flag' ) {
                if ( $isAdmin eq 'Yes' ) {

                    # only admin can see this flag
                } else {
                    next;
                }
            }

            if ( $attr_name eq 'remove_duplicate_flag' ) {
                if ( $isAdmin eq 'Yes' ) {

                    # only admin can see this flag
                } else {
                    next;
                }
            }

            if ( $db_val{$attr_name} ) {
                $attr_val = $db_val{$attr_name};
            }

            if ( !$attr_val || blankStr($attr_val) ) {
                next;
            }

            my $disp_name = $k->{display_name};
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>";

            #	    if ( $k->{tooltip} ) {
            #		printTooltip($disp_name, $k->{tooltip});
            #	    }
            #	    else {
            print escapeHTML($disp_name);

            #	    }
            print "</th>\n";

            if ( $attr_name eq 'project_info' ) {

                # project
                print "  <td class='img'   align='left'>"
                  . getProjectLink($attr_val)
                  . "</td>\n";
            } elsif ( $attr_name eq 'prev_submission_id' ) {

                # previous submission ID
                my $prev_submit_link =
                  getSubmissionLink( $attr_val, $database );
                print "  <td class='img'   align='left'>"
                  . $prev_submit_link
                  . " </td>\n";
            } elsif ( $attr_name eq 'analysis_project_id' ) {
                my $ap_link =
                    "<a href='"
                  . $main_cgi
                  . "?section=ProjectInfo&page=analysisProject"
                  . "&analysis_project_id=$attr_val' >"
                  . $attr_val . "</a>";
                print "  <td class='img'   align='left'>" . $ap_link
                  . " </td>\n";
            } elsif (    $attr_name eq 'stats_file'
                      || $attr_name eq 'error_file' )
            {
                # stats_file or error_file
                print "  <td class='img'   align='left'>";
                if ( !blankStr($attr_val) ) {
                    print "<a href='"
                      . $main_cgi
                      . "?section=$section&page=displayFile"
                      . "&submission_id=$submission_id"
                      . "&file_type=$attr_name' >"
                      . escapeHTML($attr_val) . "</a>";
                }
                print "</td>\n";
            } elsif ( $attr_name eq 'status' ) {
                my $display_status = "";
                if ( length($attr_val) > 0 && isInt($attr_val) ) {
                    $display_status = db_getValue(
"select cv_term from submission_statuscv where term_oid = $attr_val"
                    );
                }
                print "  <td class='img'   align='left'>"
                  . $attr_val . " - "
                  . escapeHTML($display_status)
                  . "</td>\n";
            } elsif (    $attr_val =~ /^http\:\/\// || $attr_val =~ /^https\:\/\// )
            {
                # bug fix https url - ken
                
                # url
                print "  <td class='img'   align='left'>"
                  . alink( $attr_val, 'URL', 'target', 1 )
                  . "</td>\n";
            } else {

                # regular attribute value
                print "  <td class='img'   align='left'>"
                  . escapeHTML($attr_val)
                  . "</td>\n";
            }
            print "</tr>\n";

            if ( $attr_name eq 'contact' ) {

                # show IMG contacts here
                my @contact_oids = db_getValues(
"select img_contacts from submission_img_contacts where submission_id = $submission_id"
                );
                my $con_names = "";
                for my $cid (@contact_oids) {

                    #		    if ( $isAdmin ne 'Yes' && $cid != $contact_oid ) {
                    if ( !$canUpdate && $cid != $contact_oid ) {
                        next;
                    }
                    my $cname = db_getContact($cid);
                    if ( length($con_names) == 0 ) {
                        $con_names = $cname;
                    } else {
                        $con_names .= ", " . $cname;
                    }
                }

                print "<tr class='img' >\n";
                print
"  <th class='subhead' align='right'>IMG Contact Name(s)</th>\n";
                print "  <td class='img'   align='left'>"
                  . escapeHTML($con_names)
                  . "</td>\n";
                print "</tr>\n";
            }
        }

     #        if ( $tab eq 'Links' ) {
     #            DisplayProjectSetAttr($project_oid, 'project_info_data_links',
     #                                  ', ', "<br/>");
     #        }
    }    # end for tab

    for my $tname ('submission_reads_file') {
        DisplaySetAttr( $submission_id, $tname, ', ', ', ' );
    }

    # samples?
    if ( scalar(@sample_ids) > 0 ) {

        # show sample
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . "Samples" . "</th>\n";
        print "  <td class='img'   align='left'>";

        for my $key (@sample_ids) {
            my $sample_link = getSampleLink($key);
            print $sample_link . " - "
              . escapeHTML( $sample_h{$key} )
              . "<br/>\n";
        }

        print "  </td>\n";
        print "</tr>\n";
    }

    print "</table>\n";

    if ( $isAdmin eq 'Yes' ) {
        DisplaySubmissionDataFiles($submission_id);
    }

    print
"<input type='submit' name='_section_$section:showStats' value='Show Statistics' class='meddefbutton' />";
    print "<p>\n";

    DisplaySubmissionHistory($submission_id);

    DisplayAccessPermission($submission_id);

    printHomeLink();

    print end_form();
}

##########################################################################
# DisplaySubmissionDataFiles
##########################################################################
sub DisplaySubmissionDataFiles {
    my ($submission_id) = @_;

    my $cnt = db_getValue(
"select count(*) from submission_data_files where submission_id = $submission_id"
    );

    if ( !$cnt ) {
        return;
    }

    print "<h4>Submission Data Files</h4>\n";

    print "<table class='img' border='1'>\n";
    print "<th class='img'>File Type</th>\n";
    print "<th class='img'>File Name</th>\n";

    my $dbh = Connect_IMG();

    # get previous submission ID if any
    my $sql = qq{
	select h.submission_id, h.file_type, h.file_name
	    from submission_data_files h
            where h.submission_id = ?
	};

webLog("900-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute($submission_id);
    for ( ; ; ) {
        my ( $s_id, $file_type, $file_name ) = $cur->fetchrow_array();
        last if !$s_id;

        print "<tr class='img'>\n";
        print "  <td class='img'   align='left'>$file_type</td>\n";
        print "  <td class='img'   align='left'>$file_name</td>\n";
        print "</tr>\n";
    }
    $cur->finish();
    $dbh->disconnect();

    print "</table>\n";
}

##########################################################################
# DisplaySubmissionHistory
##########################################################################
sub DisplaySubmissionHistory {
    my ($submission_id) = @_;

    if ( !$submission_id || !isInt($submission_id) ) {
        return;
    }

    my $cnt = db_getValue(
"select count(*) from submission_history where submission_id = $submission_id"
    );

    if ( !$cnt ) {
        return;
    }

    my $batch_cnt = db_getValue(
"select b.no_submissions from submission s, batch b where s.submission_id = $submission_id and s.batch_id = b.batch_id"
    );

    print "<h4>History Tracking</h4>\n";

    print "<table class='img' border='1'>\n";
    print "<th class='img'>Submission ID</th>\n";
    print "<th class='img'>Mod Date</th>\n";
    print "<th class='img'>Status</th>\n";
    print "<th class='img'>Comments</th>\n";
    print "<th class='img'>Modified By</th>\n";
    print "<th class='img'>Elapsed Time</th>\n";

    my $dbh = Connect_IMG();

    # get previous submission ID if any
    my $sql =
"select prev_submission_id from submission where submission_id = $submission_id";
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($prev_submission_id) = $cur->fetchrow();
    $cur->finish();
    if ( !$prev_submission_id ) {
        $prev_submission_id = 0;
    }

    # get next submission ID if any
    $sql =
"select submission_id from submission where prev_submission_id = $submission_id";

webLog("910-- $sql\n");

    $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($next_submission_id) = $cur->fetchrow();
    $cur->finish();
    if ( !$next_submission_id ) {
        $next_submission_id = 0;
    }

    $sql = qq{
	select h.submission_id, h.status, s.cv_term, 
	h.comments, h.username, h.mod_date,
	to_char(h.mod_date, 'DD-MON-YYYY HH24:MI:SS'),
	floor(h.mod_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))
	    from submission_history h, submission_statuscv s
	    where h.submission_id in ( $prev_submission_id, $submission_id, $next_submission_id )
	    and h.status = s.term_oid
	    order by 6
	};

webLog("920-- $sql\n");

    $cur = $dbh->prepare($sql);
    $cur->execute();
    my $prev_date = "";
    my $prev_diff = 0;
    for ( ; ; ) {
        my (
             $s_id,     $status,   $status_val,   $comments,
             $username, $mod_date, $mod_date_str, $day_diff
        ) = $cur->fetchrow_array();
        last if !$s_id;

        if (
             $batch_cnt
             && (    $status == 8
                  || $status == 9
                  || $status == 10
                  || $status == 15
                  || $status == 26 )
          )
        {
            $comments = "Batch of $batch_cnt submissions.";
        }

        if ( $s_id != $submission_id ) {
            print "<tr class='img' bgcolor='lightgray'>\n";
        } else {
            print "<tr class='img'>\n";
        }
        print "  <td class='img'   align='left'>$s_id</td>\n";
        print "  <td class='img'   align='left'>$mod_date_str</td>\n";
        print "  <td class='img'   align='left'>$status - "
          . escapeHTML($status_val)
          . "</td>\n";
        print "  <td class='img'   align='left'>"
          . escapeHTML($comments)
          . "</td>\n";
        print "  <td class='img'   align='left'>$username</td>\n";

        my $diff = CalculateDateInterval( $prev_date, $mod_date_str,
                                          $day_diff - $prev_diff );
        print "  <td class='img'   align='left'>$diff</td>\n";

        print "</tr>\n";

        $prev_date = $mod_date_str;
        $prev_diff = $day_diff;
    }
    $cur->finish();
    $dbh->disconnect();

    print "</table>\n";
}

##########################################################################
# DisplayAccessPermission
##########################################################################
sub DisplayAccessPermission {
    my ($submission_id) = @_;

    if ( !$submission_id || !isInt($submission_id) ) {
        return;
    }

    my $img_oid = db_getValue(
"select img_taxon_oid from submission where submission_id = $submission_id"
    );

    if ( !$img_oid ) {
        return;
    }

    my $dbh2 = Connect_IMG_MI();
    my $sql2 = "select is_public from taxon where taxon_oid = ?";
    
    
webLog("920-- $sql2\n");
    
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($img_oid);
    my ($is_public) = $cur2->fetchrow();
    $cur2->finish();

    print "<h4>Access Permission</h4>\n";

    if ( $is_public eq 'Yes' ) {
        print "<p>The genome is public in IMG.<br/>\n";
        $dbh2->disconnect();
        return;
    }

    my %user_h;
    my $c_count = 0;
    $sql2 =
        "select c.contact_oid, c.name "
      . "from contact c, contact_taxon_permissions ct "
      . "where ct.taxon_permissions = ? "
      . "and ct.contact_oid = c.contact_oid ";

webLog("921-- $sql2\n");
    $cur2 = $dbh2->prepare($sql2);
    $cur2->execute($img_oid);
    for ( ; ; ) {
        my ( $c_oid, $c_name ) = $cur2->fetchrow();
        last if !$c_oid;

        $user_h{$c_oid} = $c_name;
        $c_count++;
    }
    $cur2->finish();
    $dbh2->disconnect;

    if ( $c_count == 1 ) {
        print "<p>The following user has access permission:\n";
    } elsif ( $c_count > 1 ) {
        print "<p>The following users have access permission:\n";
    }

    print "<ul>\n";
    for my $c_oid ( keys %user_h ) {
        print "<li>" . $user_h{$c_oid} . "</li>\n";
    }
    print "</ul>\n";
}

##########################################################################
# CalculateDateInterval
##########################################################################
sub CalculateDateInterval {
    my ( $datetime1, $datetime2, $day_diff ) = @_;

    if ( blankStr($datetime1) || blankStr($datetime2) ) {
        return "";
    }

    my ( $date1, $time1 ) = split( / /, $datetime1 );

    my ( $day1,  $mon1, $year1 ) = getDayDetail($date1);
    my ( $hour1, $min1, $sec1 )  = getTimeDetail($time1);

    my ( $date2, $time2 ) = split( / /, $datetime2 );
    my ( $day2,  $mon2, $year2 ) = getDayDetail($date2);
    my ( $hour2, $min2, $sec2 )  = getTimeDetail($time2);

    my $dd = $day_diff;

    # calculate interval
    if ( $sec2 < $sec1 ) {
        $sec2 += 60;
        $min2--;
    }
    if ( $min2 < $min1 ) {
        $min2 += 60;
        $hour2--;
    }
    if ( $hour2 < $hour1 ) {
        $hour2 += 24;
        $dd--;
    }

    $dd = sprintf( "%02d", $dd );
    my $hh = $hour2 - $hour1;
    $hh = sprintf( "%02d", $hh );
    my $mm = $min2 - $min1;
    $mm = sprintf( "%02d", $mm );
    my $ss = $sec2 - $sec1;
    $ss = sprintf( "%02d", $ss );

    my $str = "$dd d, $hh h, $mm m, $ss s";
    return $str;
}

sub getDayDetail {
    my ($date1) = @_;

    if ( blankStr($date1) ) {
        return ( 0, 0, 0 );
    }

    my ( $day1, $mon1, $year1 ) = split( /\-/, $date1 );

    my %months = (
                   JAN => 1,
                   FEB => 2,
                   MAR => 3,
                   APR => 4,
                   MAY => 5,
                   JUN => 6,
                   JUL => 7,
                   AUG => 8,
                   SEP => 9,
                   OCT => 10,
                   NOV => 11,
                   DEC => 12,
    );

    $mon1 = $months{$mon1};

    return ( $day1, $mon1, $year1 );
}

sub getTimeDetail {
    my ($time1) = @_;

    if ( blankStr($time1) ) {
        return ( 0, 0, 0 );
    }

    my ( $hour1, $min1, $sec1 ) = split( /\:/, $time1 );
    return ( $hour1, $min1, $sec1 );
}

##########################################################################
# DisplayFile
##########################################################################
sub DisplayFile {
    my ( $submission_id, $file_type ) = @_;

    print start_form(
                      -name   => 'displayFile',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        printError("Unknown username / password");
        return;
    }

    if ( !$submission_id ) {
        return;
    }

    # check whether user can view this project
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'No' ) {

        # contact must be the owner of this submission
        my $project_contact = db_getValue(
"select contact from submission where submission_id = $submission_id"
        );
        if ( $project_contact == $contact_oid ) {

            # fine
        } else {

            # check submission_img_contacts
            my $cnt0 = db_getValue(
"select count(*) from submission_img_contacts where submission_id = $submission_id and img_contacts = $contact_oid"
            );
            if ( $cnt0 == 0 ) {
                printError("You cannot view this submission.");
                return;
            }
        }
    }

    my $file_display_type = 'Error Report';
    if ( $file_type eq 'stats_file' ) {
        $file_display_type = 'Statistics Report';
    } else {
        $file_type = 'error_file';
    }

    print "<h2>$file_display_type for Submission $submission_id</h2>\n";

    my $line_no = 0;
    my $is_html = 0;

    my $file_name =
      db_getValue(   "select "
                   . $file_type
                   . " from submission "
                   . "where submission_id = $submission_id" );
    if ( blankStr($file_name) ) {
        print "<h4>No error file for this submission.</h4>\n";
    } elsif ( -e $file_name ) {

        # display file
        print "<h4>File Name: " . escapeHTML($file_name) . "</h4>\n";

        open( FILE, $file_name );
        my $line;
        while ( $line = <FILE> ) {
            chomp($line);

            if ( $line_no == 0 ) {
                if ( $line =~ /html/ ) {
                    $is_html = 1;
                }
            }

            if ($is_html) {
                print $line . "\n";
            } else {
                print escapeHTML($line) . "<br/>\n";
            }

            $line_no++;
        }
        close(FILE);
    } else {
        print "<h4>Error file does not exist.</h4>\n";
    }

    #    printHomeLink();

    print end_form();
}

##########################################################################
# EmailContact
##########################################################################
sub EmailContact {

    # need to send email?
    my $email_contact = param1('email_contact');
    if ( !$email_contact ) {
        return;
    }

    my $email_from = db_getContactEmail(1000);
    if ( !isEmail($email_from) ) {
        return;
    }
    $email_from = "IMG_PIPELINE <" . $email_from . ">";

    # check submission
    my $submission_id = param1('submission_id');
    if ( !$submission_id ) {
        print "<p>No submission ID!</p>\n";
        print end_form();
        return;
    }

    # submission contact
    my $email_to = "";
    my $sub_cont = db_getValue(
         "select contact from submission where submission_id = $submission_id");
    my $sub_cont_email = db_getContactEmail($sub_cont);
    if ( isEmail($sub_cont_email) ) {
        $email_to = $sub_cont_email;
    }

# submission modified_by
#    my $sub_mod = db_getValue("select modified_by from submission where submission_id = $submission_id");
    my $sub_mod = getContactOid();

    if ( $sub_mod != $sub_cont ) {
        my $sub_mod_email = db_getContactEmail($sub_mod);
        if ( isEmail($sub_mod_email) ) {
            if ( length($email_to) == 0 ) {
                $email_to = $sub_mod_email;
            } else {
                $email_to .= ", " . $sub_mod_email;
            }
        }
    }

    # IMG contacts
    my @img_contacts = db_getValues(
"select s2.img_contacts from submission_img_contacts s2 where s2.submission_id = $submission_id"
    );

    my $project_oid = db_getValue(
"select project_info from submission where submission_id = $submission_id"
    );

    my $db = param1('database');
    $db =~ s/%20/ /g;
    my $dbh      = Connect_IMG_Contact();
    my $jgi_proj = isJgiProject( $dbh, $project_oid );
    if ( $jgi_proj && $db eq 'IMG ER' ) {

        # add Lynne and Sam if they are not there
        for my $id2 ( 333, 100440 ) {
            if ( !inIntArray( $id2, @img_contacts ) ) {
                push @img_contacts, ($id2);
            }
        }
    }

    for my $proj_cont (@img_contacts) {
        if ( $proj_cont != $sub_cont && $proj_cont != $sub_mod ) {
            my $proj_cont_email = db_getContactEmail($proj_cont);
            if ( isEmail($proj_cont_email) ) {
                if ( length($email_to) == 0 ) {
                    $email_to = $proj_cont_email;
                } else {
                    $email_to .= ", " . $proj_cont_email;
                }
            }
        }
    }

    if ( $jgi_proj && $db eq 'IMG ER' ) {

        # add Lin Peters, who does not have an IMG account
        $email_to .= ", lgpeters\@lbl.gov";
    }

    # send email to IMG_PIPELINE for product name assignment
    my $new_status_code = param1('status');
    if ( $new_status_code && $new_status_code == 9 ) {
        $email_to .= ", " . $email_from;
    }

    # subject
    my $email_subj = "Your IMG Submission $submission_id";

    # contents
    my $sql = qq{
	select s.submission_id, s.project_info, p.display_name,
	    s.sample_oid, s.submission_date, v.cv_term, s.error_msg
	    from submission s, submission_statuscv v, project_info p
	    where s.submission_id = $submission_id
	    and s.project_info = p.project_oid
	    and s.status = v.term_oid (+)
	};

webLog("930-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my (
         $sub_id,   $proj_oid,   $proj_name, $sam_oid,
         $sub_date, $sub_status, $error_msg
    ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    my $email_data = "IMG Submission ID: $sub_id\n";

    my $img_db_url = "";
    if ( $db eq 'IMG/M ER' ) {
        $img_db_url = getImgMIUrl();
    } else {
        $img_db_url = getImgERUrl();
    }

    $email_data .= "IMG Database: $db ($img_db_url)\n";

    $email_data .= "Project Name: $proj_name\n";
    if ($sam_oid) {
        my $sam_name = db_getValue(
"select sample_display_name from env_sample where sample_oid = $sam_oid"
        );
        $email_data .= "Sample $sam_oid: $sam_name\n";
    }

    $email_data .= "Submission Date: $sub_date\n";

    $email_data .= "Old Submission Status: $sub_status\n";
    if ($new_status_code) {
        my $new_status = db_getValue(
"select cv_term from submission_statuscv where term_oid = $new_status_code"
        );
        $email_data .= "New Submission Status: $new_status\n";
    }

    # error message?
    if ( !blankStr($error_msg) ) {
        $email_data .= "Error Message: $error_msg\n";
    }

    sendEmail( $email_from, $email_to, $email_subj, $email_data );
}

############################################################################
# UpdateContact
############################################################################
sub UpdateContact {
    my ($submission_id) = @_;

    print start_form(
                      -name   => 'updateContact',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    if ( !$submission_id ) {
        printError("No submission has been selected.");
        return;
    }

    if ( !isInt($submission_id) ) {
        printError("Incorrect submission ID.");
        return;
    }

    print "<h1>Submission (ID: $submission_id)</h1>\n";
    print hidden( 'submission_id', $submission_id );

    saveSubmissionPageInfo();

    # get Submission definition
    my $def_submit = def_Submission();
    my @attrs      = @{ $def_submit->{attrs} };

    # get data from database
    my %db_val = SelectSubmission($submission_id);

    print "<p>\n";
    print "<table class='img' border='1'>\n";

    for my $k (@attrs) {
        my $attr_val  = "";
        my $attr_name = $k->{name};

        if (    $attr_name ne 'project_oid'
             && $attr_name ne 'status'
             && $attr_name ne 'contact'
             && $attr_name ne 'is_img_public'
             && $attr_name ne 'database' )
        {
            next;
        }

        if ( $db_val{$attr_name} ) {
            $attr_val = $db_val{$attr_name};
        }

        if ( !$attr_val || blankStr($attr_val) ) {
            next;
        }

        my $disp_name = $k->{display_name};
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . $disp_name . "</th>\n";

        if ( $attr_name eq 'project_info' ) {

            # project
            print "  <td class='img'   align='left'>"
              . getProjectLink($attr_val)
              . "</td>\n";
        } elsif ( $attr_name eq 'status' ) {
            my $display_status = "";
            if ( length($attr_val) > 0 && isInt($attr_val) ) {
                $display_status = db_getValue(
"select cv_term from submission_statuscv where term_oid = $attr_val"
                );
            }
            print "  <td class='img'   align='left'>"
              . $attr_val . " - "
              . escapeHTML($display_status)
              . "</td>\n";
        } else {

            # regular attribute value
            print "  <td class='img'   align='left'>"
              . escapeHTML($attr_val)
              . "</td>\n";
        }
        print "</tr>\n";
    }

    my $database = $db_val{'database'};
    if ( !$database ) {
        print "</table>\n";
        printError("No IMG database information is provided.");
        return;
    }
    print hidden( 'database', $db_val{'database'} );

    print "</table>\n";

    # get existing IMG contacts
    my @contact_list = db_getValues(
"select img_contacts from submission_img_contacts where submission_id = $submission_id"
    );

    print "<h3>Select IMG contact(s) for this submission:</h3>\n";
    my $dbh = Connect_IMG_Contact();
    my $sql =
      "select contact_oid, username, name from contact order by username";

webLog("931-- $sql\n");      
      
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( my $j = 0 ; $j <= 10000 ; $j++ ) {
        my ( $c_oid, $username, $name ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( !$username || $username =~ /MISSING/ ) {
            next;
        }

        print nbsp(3);
        print "<input type='checkbox' name='sub_img_contact' value='$c_oid'";
        if ( inIntArray( $c_oid, @contact_list ) ) {
            print " checked ";
        }
        print ">" . escapeHTML($username) . " (" . escapeHTML($name) . ")\n";
        print "<br/>\n";
    }

    $cur->finish();

    $dbh->disconnect();

    print "<p>\n";
    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:dbUpdateIMGContact" value="Update Contact(s)" class="medbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:dbUpdateIMGContact" value="Update Contact(s)" class="medbutton" />';
    }

    print "<p>\n";
    printHomeLink();

    print end_form();
}

#############################################################################
# dbUpdateIMGContact
#############################################################################
sub dbUpdateIMGContact {

    my $msg = "";

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        $msg = "Unknown username / password";
        return $msg;
    }

    my $isAdmin   = getIsAdmin($contact_oid);
    my $canUpdate = getCanUpdateSubmission($contact_oid);

    #    if ( $isAdmin ne 'Yes' ) {
    if ( !$canUpdate ) {
        $msg = "You do not have the privilege to update permission.";
        return $msg;
    }

    my $submission_id = param1('submission_id');
    if ( blankStr($submission_id) || !isInt($submission_id) ) {
        $msg = "No Submission ID for update.";
        return $msg;
    }

    my @contact_list = param1('sub_img_contact');
    my @sqlList      = ();

    my $sql = "delete from submission_img_contacts "
      . "where submission_id = $submission_id";
    push @sqlList, ($sql);

    for my $c_oid (@contact_list) {
        $sql =
            "insert into submission_img_contacts ("
          . "submission_id, img_contacts) values ("
          . $submission_id . ", "
          . $c_oid . ")";
        push @sqlList, ($sql);
    }

    #    for $sql ( @sqlList ) {
    #	$msg .= "SQL: $sql; ";
    #    }

    if ($msg) {
        return $msg;
    }

    # update database
    my $dbh = Connect_IMG();
    $dbh->{AutoCommit} = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for $sql (@sqlList) {
            $last_sql++;

            my $cur = $dbh->prepare($sql)
              || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
            $cur->execute()
              || dienice("execSql: cannot execute: $DBI::errstr\n");
        }
    };

    if ($@) {
        $dbh->rollback();
        $dbh->disconnect();
        return $last_sql;
    }

    $dbh->commit();
    $dbh->disconnect();

    return "";
}

##########################################################################
# CancelSubmission
##########################################################################
sub CancelSubmission {

    print start_form(
                      -name   => 'cancelSubmission',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        printError("Unknown username / password");
        return;
    }

    my @submission_ids = param('submission_id');

    if ( scalar(@submission_ids) == 0 ) {
        printError("No submission has been selected.");
        print end_form();
        return;
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    # check whether user can cancel this project
    my $isAdmin = getIsAdmin($contact_oid);
    for my $submission_id (@submission_ids) {
        if ( !isInt($submission_id) ) {
            next;
        }

        if ( $isAdmin eq 'No' ) {

            # contact must be the owner of this submission
            my $project_contact = db_getValue(
"select contact from submission where submission_id = $submission_id"
            );
            if ( $project_contact == $contact_oid ) {

                # fine
            } else {
                printError("You cannot cancel this submission.");
                return;
            }
        }

        print "<h2>Submission $submission_id</h2>\n";

        my $status = db_getValue(
"select c.cv_term from submission s, submission_statuscv c where s.submission_id = $submission_id and s.status = c.term_oid"
        );
        if (    $status =~ /^Finished/
             || $status =~ /^Cancel/ )
        {
            my $msg =
              "You cannot cancel this submission (submission status: $status).";
            printError($msg);
            return;
        } elsif (    $status eq 'Data loading in progress.'
                  || $status =~ /^Data loading/ )
        {
            my $msg =
              "You cannot cancel this submission (submission status: $status).";
            printError($msg);
            return;
        }

        my $sql =
"update submission set status = 90, modified_by = $contact_oid, mod_date = sysdate where submission_id = $submission_id";
        my @sqlList = ();
        push @sqlList, ($sql);
        db_sqlTrans( \@sqlList );

        print "<p>This submission is cancelled.</p>\n";
    }

    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:showMPage" value="OK" class="smbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:showERPage" value="OK" class="smbutton" />';
    }

    print "<p>\n";
    printHomeLink();

    print end_form();
}

############################################################################
# showSetIMGPermission - for special IMG admin to set taxon permission
#                        in IMG database
############################################################################
sub ShowSetIMGPermission {
    my ($database) = @_;

    my $section = 'ERSubmission';

    print "<h3>Set IMG Permission for Loaded Genomes</h3>\n";
    print "<p>Please select a loaded submission: to set taxon permission in ";
    if ( $database eq 'IMG/M ER' ) {
        print "IMG/M ER database.</p>\n";
        $section = 'MSubmission';
    } else {
        print "IMG ER database.</p>\n";
    }

    my $dbh = WebFunctions::Connect_IMG;

    my $cond2 = submissionFilterCondition();
    if ( !blankStr($cond2) ) {
        $cond2 = " and " . $cond2;
    }

    my $sql = qq{ 
        select s.submission_id, p.display_name
	    from submission s, project_info p
	    where s.status in (8, 9, 10)
	    and s.project_info = p.project_oid
	    and s.database = '$database'
	    $cond2
            order by s.submission_id, p.display_name
        };

webLog("500-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();

    print "<h4>Select a finished submission:</h4>";
    print "&nbsp; &nbsp; &nbsp;\n";
    print "<select name='sub_id_permission' class='img' size='1'>\n";

    print "    <option value=''></option>\n";
    for ( my $j = 0 ; $j <= 10000 ; $j++ ) {
        my ( $submit_id, $proj_name ) = $cur->fetchrow_array();
        if ( !$submit_id ) {
            last;
        }

        print
          "    <option value='$submit_id'>$submit_id - $proj_name</option>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "</select>\n";
    print "<p/>\n";

    print
"<input type='submit' name='_section_$section:setIMGPermission' value='Set IMG Permission' class='medbutton' />";
}

############################################################################
# SetIMGPermission - set IMG permission for a finished submission
#                    (in IMG ER or IMG/M ER)
############################################################################
sub SetIMGPermission {
    my ($submission_id) = @_;

    print start_form(
                      -name   => 'setIMGPermission',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    if ( !$submission_id || !isInt($submission_id) ) {
        printError("No submission has been selected.");
        return;
    }

    print "<h1>Submission (ID: $submission_id)</h1>\n";

    # get Submission definition
    my $def_submit = def_Submission();
    my @attrs      = @{ $def_submit->{attrs} };

    # get data from database
    my %db_val = SelectSubmission($submission_id);

    print "<p>\n";
    print "<table class='img' border='1'>\n";

    for my $k (@attrs) {
        my $attr_val  = "";
        my $attr_name = $k->{name};

        if (    $attr_name ne 'project_oid'
             && $attr_name ne 'status'
             && $attr_name ne 'contact'
             && $attr_name ne 'is_img_public'
             && $attr_name ne 'database'
             && $attr_name ne 'img_taxon_oid' )
        {
            next;
        }

        if ( $db_val{$attr_name} ) {
            $attr_val = $db_val{$attr_name};
        }

        if ( !$attr_val || blankStr($attr_val) ) {
            next;
        }

        my $disp_name = $k->{display_name};
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . $disp_name . "</th>\n";

        if ( $attr_name eq 'project_info' ) {

            # project
            print "  <td class='img'   align='left'>"
              . getProjectLink($attr_val)
              . "</td>\n";
        } elsif ( $attr_name eq 'status' ) {
            my $display_status = "";
            if ( length($attr_val) > 0 && isInt($attr_val) ) {
                $display_status = db_getValue(
"select cv_term from submission_statuscv where term_oid = $attr_val"
                );
            }
            print "  <td class='img'   align='left'>"
              . $attr_val . " - "
              . escapeHTML($display_status)
              . "</td>\n";
        } else {

            # regular attribute value
            print "  <td class='img'   align='left'>"
              . escapeHTML($attr_val)
              . "</td>\n";
        }
        print "</tr>\n";
    }

    # samples?
    my $sample_oid = db_getValue(
        "select sample_oid from submission where submission_id = $submission_id"
    );
    if ($sample_oid) {

        # show sample
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . "Samples" . "</th>\n";
        print "  <td class='img'   align='left'>";

        my $sample_link = getSampleLink($sample_oid);
        print $sample_link . " - ";
        my $s_name = db_getValue(
"select sample_display_name from env_sample where sample_oid = $sample_oid"
        );
        print escapeHTML($s_name);
        print "<br/>\n";

        print "  </td>\n";

        print "</tr>\n";
    }

    # IMG contacts
    my $con_names = "";
    my @contact_oids = db_getValues(
"select img_contacts from submission_img_contacts where submission_id = $submission_id"
    );
    for my $cid (@contact_oids) {
        my $cname = db_getContactName($cid);
        if ( length($con_names) == 0 ) {
            $con_names = $cname;
        } else {
            $con_names .= ", " . $cname;
        }
    }
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>IMG Contact(s) in "
      . "Submission Database</th>\n";
    print "  <td class='img'   align='left'>"
      . escapeHTML($con_names)
      . "</td>\n";
    print "</tr>\n";

    my $database = $db_val{'database'};
    if ( !$database ) {
        print "</table>\n";
        printError("No IMG database information is provided.");
        return;
    }
    print hidden( 'database', $database );
    if ( !$db_val{'img_taxon_oid'} ) {
        print "</table>\n";
        printError("No IMG taxon OID is provided.");
        return;
    }
    print hidden( 'img_taxon_oid', $db_val{'img_taxon_oid'} );

    # get existing contacts in IMG database
    my @contact_list = ();
    my $dbh2;
    if ( $database eq 'IMG ER' ) {

        # connect to ER
        $dbh2 = Connect_IMG_ER();
    } else {
        $dbh2 = Connect_IMG_MI();
    }
    my $sql2 =
        "select contact_oid from contact_taxon_permissions "
      . "where taxon_permissions = "
      . $db_val{'img_taxon_oid'};
      
webLog("940-- $sql2\n");      
      
    my $cur2 = $dbh2->prepare($sql2);
    $cur2->execute();
    for ( my $j = 0 ; $j <= 1000000000 ; $j++ ) {
        my ($cid) = $cur2->fetchrow_array();
        if ( !$cid ) {
            last;
        }
        push @contact_list, ($cid);
    }
    $cur2->finish();

    # get taxon name and permission in IMG/ER or IMG/MI
    $sql2 = "select taxon_name, is_public from taxon where taxon_oid = "
      . $db_val{'img_taxon_oid'};
      
webLog("941-- $sql2\n");      
      
    $cur2 = $dbh2->prepare($sql2);
    $cur2->execute();
    my ( $taxon_name_in_img, $is_public_in_img ) = $cur2->fetchrow_array();
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>Taxon Name in "
      . $db_val{'database'}
      . "</th>\n";
    print "  <td class='img'   align='left'>"
      . escapeHTML($taxon_name_in_img)
      . "</td>\n";
    print "</tr>\n";
    print "<tr class='img' >\n";
    print "  <th class='subhead' align='right'>Is Public in "
      . $db_val{'database'}
      . "</th>\n";
    print "  <td class='img'   align='left'>"
      . escapeHTML($is_public_in_img)
      . "</td>\n";
    print "</tr>\n";

    $cur2->finish();

    print "</table>\n";

    $dbh2->disconnect();

    print "<h3>Grant Taxon Permission to Contact(s):</h3>\n";

    my $dbh = Connect_IMG_Contact();
    my $sql =
      "select contact_oid, username, name from contact order by username";
      
webLog("950-- $sql\n");      
      
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( my $j = 0 ; $j <= 1000000000 ; $j++ ) {
        my ( $c_oid, $username, $name ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( !$username || $username =~ /MISSING/ ) {
            next;
        }

        print nbsp(3);
        print "<input type='checkbox' name='img_contact_oid' value='$c_oid'";
        if ( inIntArray( $c_oid, @contact_list ) ) {
            print " checked ";
        }
        print ">" . escapeHTML($username) . " (" . escapeHTML($name) . ")\n";
        print "<br/>\n";
    }

    $cur->finish();

    $dbh->disconnect();

    print "<p>\n";
    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_MSubmission:dbUpdatePermission" value="Update Permission" class="medbutton" />';
    } else {
        print
'<input type="submit" name="_section_ERSubmission:dbUpdatePermission" value="Update Permission" class="medbutton" />';
    }

    print "<p>\n";
    printHomeLink();

    print end_form();
}

sub DefineNewAnalysis {
    my ( $project_type, $database ) = @_;

    print "<p>Please define a new analysis project for ";
    if ( $project_type eq 'extract' ) {
        print "the extracted genome.\n";
    } else {
        print "the combined assembly.\n";
    }

    print "<table class='img'\n";
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>";
    if ( $project_type eq 'extract' ) {
        print "Extracted Genome Name</td>\n";
    } else {
        print "Analysis Name</td>\n";
    }
    print "<td class='img'>\n";
    print "<input type='text' name='ap_name' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    if (    $project_type eq 'extract'
         || $database eq 'IMG ER' )
    {
        print "<tr class='img'>\n";
        print "<td class='img' align='left' bgcolor='lightblue'>";
        print "NCBI Taxon ID</td>\n";
        print "<td class='img'>\n";
        print "<input type='text' name='ncbi_tax_id' value=''"
          . " size='20' maxLength='80'/>\n";
        print "</td></tr>\n";
    }

    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>Visibility</td>\n";
    print "<td class='img'>\n";
    print "<select name='visibility' class='img' size='1'>\n";
    print "    <option value='private'>private</option>\n";
    print "    <option value='public'>public</option>\n";
    print "</select>\n";
    print "</td></tr>\n";

    my $contact_oid = getContactOid();
    my $name =
      db_getValue("select name from contact where contact_oid = $contact_oid");
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>";
    print "PI Name</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='pi_name' value='" . $name . "'"
      . " size='60' maxLength='100'/>\n";
    print "</td></tr>\n";

    my $email =
      db_getValue("select email from contact where contact_oid = $contact_oid");
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>";
    print "PI Email</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='pi_email' value='" . $email . "'"
      . " size='60' maxLength='100'/>\n";
    print "</td></tr>\n";

    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>";
    print "Analysis Description</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='ap_desc' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
        print "<tr class='img'>\n";
        print "<td class='img' align='left' bgcolor='lightblue'>";
        print "Submitter ID</td>\n";
        print "<td class='img'>\n";
        print "<input type='text' name='submitter_id' value='"
          . $contact_oid . "'"
          . " size='60' maxLength='100'/>\n";
        print "</td></tr>\n";
    }

    print "</table>\n";
}

#############################################################################
# dbUpdatePermission
#############################################################################
sub dbUpdatePermission {

    my $msg = "";

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        $msg = "Unknown username / password";
        return $msg;
    }

    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin ne 'Yes' ) {
        $msg = "You do not have the privilege to update permission.";
        return $msg;
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    my $img_taxon_oid = param1('img_taxon_oid');
    if ( blankStr($database) || blankStr($img_taxon_oid) ) {
        $msg = "No database and/or taxon OID for update.";
        return $msg;
    }

    my @contact_list = param('img_contact_oid');
    if ( scalar(@contact_list) == 0 ) {
        $msg = "No IMG contacts have been selected.";
        return $msg;
    }

    my @sqlList = ();

    my $sql = "delete from contact_taxon_permissions "
      . "where taxon_permissions = $img_taxon_oid";
    push @sqlList, ($sql);

    for my $c_oid (@contact_list) {
        $sql =
            "insert into contact_taxon_permissions ("
          . "contact_oid, taxon_permissions) values ("
          . $c_oid . ", "
          . $img_taxon_oid . ")";
        push @sqlList, ($sql);
    }

    #    for $sql ( @sqlList ) {
    #	$msg .= "SQL: $sql; ";
    #    }

    if ($msg) {
        return $msg;
    }

    # update database
    my $dbh;
    my $update_ext = 1;


    my $last_sql = 0;



    if ($update_ext) {

        # update img_ext too
##	my $dbh2 = Connect_IMG_EXT();
        my $dbh2 = Connect_IMG_ER();
        $dbh2->{AutoCommit} = 0;

        $last_sql = 0;

        # perform database update
        eval {
            for $sql (@sqlList) {
                $last_sql++;

                my $cur2 = $dbh2->prepare($sql)
                  || dienice(
                          "execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");
            }
        };

        if ($@) {
            $dbh2->rollback();
            $dbh2->disconnect();
            return $last_sql;
        }

        $dbh2->commit();
        $dbh2->disconnect();
    }

    return "";
}

###########################################################################
# ShowSearchSubmissionProject
###########################################################################
sub ShowSearchSubmissionProject {
    my ( $database, $p_type ) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $project_type = param1('project_type_option');
    if ( $project_type eq 'combined' ) {
        if ( $database eq 'IMG ER' ) {
            $p_type = 'G';
        } else {
            $p_type = 'B';
        }
    } elsif ( $project_type eq 'extract' ) {
        $p_type = 'M';
    }

    if ( $p_type eq 'M' ) {
        print "<h2>Search Metagenome Project for Your Submission</h2>\n";
    } else {
        print "<h2>Search Project for Your Submission</h2>\n";
    }
    print hiddenVar( 'project_type_option', $project_type );
    print hiddenVar( 'p_type',              $p_type );

    #    print "<h3>Target Database: $database</h3>\n";

    print hiddenVar( 'database', $database );

    print hiddenVar( 'proj_search_option', 'query' );

    my $hint_msg = qq{
	In order to submit your dataset you need to define a project.
	    A project contains only information about the organism or the sample
	    and NOT actual sequence or analysis data for it.
	    If you or somebody else has worked with this dataset in the past, then the
	    project may already exist in the GOLD database.
	    First search whether the project already exists.
	    If it does, select the project and proceed to submit data for that project.
    };
    printHint3($hint_msg);

    if (    $project_type eq 'combined'
         || $project_type eq 'extract' )
    {
##	DefineNewAnalysis($project_type);
        print
"<h4>Error: Please define your analysis project in GOLD and then use the AP ID submission option.</h4>\n";
        print end_form();
        return;
    }

    print
      "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    # genus or species
    if ( $p_type eq 'B' ) {
        print
          "<p>Genus or species is applied to isolate genome projects only. ";
        print "Ecosystem fields are applied to metagenome projects only.\n";
    }

    print "<table class='img' border='1'>\n";

    # target database
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Target Database</td>\n";
    print "<td class='img'>$database</td></tr>\n";

    # project display name
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Project name</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:display_name' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    # proposal name
    print "<tr class='img'>\n";
    print
"<td class='img' align='left' bgcolor='lightblue'>Study/Proposal Name</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:proposal_name' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    # gold stamp ID
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>GOLD ID</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:gold_stamp_id' value=''"
      . " size='40' maxLength='60'/>\n";
    print "</select>\n";
    print "</td></tr>\n";

    # genus and species selection
    if ( $p_type ne 'M' ) {
        print "<tr class='img'>\n";
        print
"<td class='img' align='left' bgcolor='lightblue'>Genus or Species</td>\n";
        print "<td class='img'>\n";
        print "<input type='text' name='search_sp:genus_or_species' value=''"
          . " size='60' maxLength='255'/>\n";
        print "</td></tr>\n";

        print "<tr class='img'>\n";
        print "<td class='img' align='left' bgcolor='lightblue'>Genus</td>\n";
        print "<td class='img'>\n";
        my @cv_vals = db_getValues(
"select unique genus from project_info where domain != 'MICROBIAL' and genus is not null order by 1"
        );
        print "<select name='search_sp:genus' class='img' size='1'>\n";
        print "   <option value=''> </option>\n";

        for my $s0 (@cv_vals) {
            print "    <option value='" . escapeHTML($s0) . "'";
            print ">$s0</option>\n";
        }
        print "</select>\n";
        print "</td></tr>\n";

        print "<tr class='img'>\n";
        print "<td class='img' align='left' bgcolor='lightblue'>Species</td>\n";
        print "<td class='img'>\n";
        @cv_vals = db_getValues(
"select unique species from project_info where domain != 'MICROBIAL' and species is not null order by 1"
        );
        print "<select name='search_sp:species' class='img' size='1'>\n";
        print "   <option value=''> </option>\n";

        for my $s0 (@cv_vals) {
            print "    <option value='" . escapeHTML($s0) . "'";
            print ">$s0</option>\n";
        }
        print "</select>\n";
        print "</td></tr>\n";
    }

    if ( $p_type eq 'M' || $p_type eq 'B' ) {
        print "<tr class='img'>\n";
        print
"<td class='img' align='left' bgcolor='lightblue'>Any Ecosystem field</td>\n";
        print "<td class='img'>\n";
        print "<input type='text' name='search_sp:any_ecosystem_field' value=''"
          . " size='60' maxLength='255'/>\n";
        print "</td></tr>\n";

        my %eco_disp_name;
        $eco_disp_name{'ecosystem'}          = 'Ecosystem';
        $eco_disp_name{'ecosystem_category'} = 'Ecosystem category';
        $eco_disp_name{'ecosystem_type'}     = 'Ecosystem type';
        $eco_disp_name{'ecosystem_subtype'}  = 'Ecosystem subtype';
        $eco_disp_name{'specific_ecosystem'} = 'Specific ecosystem';

        for my $eco_fld (
                          'ecosystem',      'ecosystem_category',
                          'ecosystem_type', 'ecosystem_subtype',
                          'specific_ecosystem'
          )
        {
            print "<tr class='img'>\n";
            print "<td class='img' align='left' bgcolor='lightblue'>"
              . $eco_disp_name{$eco_fld}
              . "</td>\n";
            print "<td class='img'>\n";
            my @cv_vals = db_getValues(
"select unique $eco_fld from project_info where domain = 'MICROBIAL' and $eco_fld is not null order by 1"
            );
            print "<select name='search_sp:$eco_fld' class='img' size='1'>\n";
            print "   <option value=''> </option>\n";
            for my $s0 (@cv_vals) {
                print "    <option value='" . escapeHTML($s0) . "'";
                print ">$s0</option>\n";
            }
            print "</select>\n";
            print "</td></tr>\n";
        }
    }
    print "</table>\n";

    print "<p>\n";
    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_NewSubmission:selectSubProj" value="Search Projects" class="meddefbutton" />';
    } else {
        print
'<input type="submit" name="_section_NewSubmission:selectSubProj" value="Search Projects" class="meddefbutton" />';

#	print nbsp(1);
#	print '<input type="submit" name="_section_ERSubmission:exportProjectList_noHeader" value="Export Project List" class="medbutton" />';
    }

    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";
    $hint_msg = "If you cannot find the project";
    if ( $p_type eq 'M' ) {
        $hint_msg .= " or sample";
    }
    $hint_msg .= ", you need to create a new one in " . getImgGoldLink() . ".";
    printHint3($hint_msg);

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowQuerySearchImgMetagenome
###########################################################################
sub ShowQuerySearchImgMetagenome {
    my ( $database, $p_type ) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $project_type = param1('project_type_option');
    $p_type = 'M';

    print "<h2>Search Source Metagenome in IMG</h2>\n";

    print hiddenVar( 'database',            $database );
    print hiddenVar( 'project_type_option', $project_type );

    my $hint_msg = qq{
          In order to submit an extracted genome you need to provide
          genome name and NCBI Taxon ID. Enter the information, and then
          use the following search option to select a source
          metagenome that has aleady been loaded into IMG/M ER.
          };

    printHint3($hint_msg);

    print hiddenVar( 'p_type', $p_type );

    #    print "<h3>Target Database: $database</h3>\n";

    if (    $project_type eq 'combined'
         || $project_type eq 'extract' )
    {
##	DefineNewAnalysis($project_type);
        print
"<h4>Error: Please define your analysis project in GOLD and then use the AP ID submission option.</h4>\n";
        print end_form();
        return;
    }

    print
      "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    print "<table class='img' border='1'>\n";

# target database
#    print "<tr class='img'>\n";
#    print "<td class='img' align='left' bgcolor='lightblue'>Target Database</td>\n";
#    print "<td class='img'>$database</td></tr>\n";

    # project display name
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>Genome Name</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:taxon_display_name' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    # proposal name
    print "<tr class='img'>\n";
    print
"<td class='img' align='left' bgcolor='lightblue'>Study/Proposal Name</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:proposal_name' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    # gold stamp ID
    print "<tr class='img'>\n";
    print "<td class='img' align='left' bgcolor='lightblue'>GOLD ID</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:gold_id' value=''"
      . " size='40' maxLength='60'/>\n";
    print "</select>\n";
    print "</td></tr>\n";

    print "<tr class='img'>\n";
    print
"<td class='img' align='left' bgcolor='lightblue'>Any Ecosystem field</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_sp:any_ecosystem_field' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    my %eco_disp_name;
    $eco_disp_name{'ecosystem'}          = 'Ecosystem';
    $eco_disp_name{'ecosystem_category'} = 'Ecosystem category';
    $eco_disp_name{'ecosystem_type'}     = 'Ecosystem type';
    $eco_disp_name{'ecosystem_subtype'}  = 'Ecosystem subtype';
    $eco_disp_name{'specific_ecosystem'} = 'Specific ecosystem';

    for my $eco_fld (
                      'ecosystem',      'ecosystem_category',
                      'ecosystem_type', 'ecosystem_subtype',
                      'specific_ecosystem'
      )
    {
        print "<tr class='img'>\n";
        print "<td class='img' align='left' bgcolor='lightblue'>"
          . $eco_disp_name{$eco_fld}
          . "</td>\n";
        print "<td class='img'>\n";
        my @cv_vals = db_getValues(
"select unique $eco_fld from project_info where domain = 'MICROBIAL' and $eco_fld is not null order by 1"
        );
        print "<select name='search_sp:$eco_fld' class='img' size='1'>\n";
        print "   <option value=''> </option>\n";
        for my $s0 (@cv_vals) {
            print "    <option value='" . escapeHTML($s0) . "'";
            print ">$s0</option>\n";
        }
        print "</select>\n";
        print "</td></tr>\n";
    }
    print "</table>\n";

    print "<p>\n";
    print
'<input type="submit" name="_section_MSubmission:selectImgMetagenome" value="Search Metagenomes" class="meddefbutton" />';
    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowKeywordSearchSubmissionProject
###########################################################################
sub ShowKeywordSearchSubmissionProject {
    my ( $database, $p_type ) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );
    my $project_type = param1('project_type_option');

    if ( $project_type eq 'combined' ) {
        if ( $database eq 'IMG ER' ) {
            print "<h2>Search Projects for Combined Assembly</h2>\n";
            $p_type = 'G';
        } else {
            print
              "<h2>Search Projects and Samples for Combined Assembly</h2>\n";
            $p_type = 'B';
        }
        print hiddenVar( 'p_type', $p_type );
    } elsif ( $project_type eq 'extract' ) {
        print "<h2>Search Metagenome Projects for Extraction Source</h2>\n";
        $p_type = 'M';
        print hiddenVar( 'p_type', 'M' );
    } elsif ( $p_type eq 'M' ) {
        print "<h2>Search Metagenome Project for Your Submission</h2>\n";
        print hiddenVar( 'p_type', 'M' );
    } else {
        print "<h2>Search Project for Your Submission</h2>\n";
        print hiddenVar( 'p_type', 'G' );
    }

    #    print "<h3>Target Database: $database</h3>\n";

    print hiddenVar( 'database', $database );

    print hiddenVar( 'proj_search_option',  'keyword' );
    print hiddenVar( 'project_type_option', $project_type );

    my $hint_msg = qq{
	In order to submit your dataset you need to define a project.
	    A project contains only information about the organism or the sample
	    and NOT actual sequence or analysis data for it.
	    If you or somebody else has worked with this dataset in the past, then the
	    project may already exist in the GOLD database.
	    First search whether the project already exists.
	    If it does, select the project and proceed to submit data for that project.
    };
    if ( $project_type eq 'combined' ) {
        $hint_msg = qq{
          In order to submit a combined assembly you need to define an
          analysis project. Enter an analysis project name, and then
          use the following search option to select all
          projects and samples for the combined assembly.
          };
    } elsif ( $project_type eq 'extract' ) {
        $hint_msg = qq{
          In order to submit an extracted genome you need to provide
          genome name and NCBI Taxon ID. Enter the information, and then
          use the following search option to select all
          projects and samples for source of extraction.
          };
    }
    printHint3($hint_msg);

    # analysis project name
    if (    $project_type eq 'combined'
         || $project_type eq 'extract' )
    {
##	DefineNewAnalysis($project_type, $database);
        print
"<h4>Error: Please define your analysis project in GOLD and then use the AP ID submission option.</h4>\n";
        print end_form();
        return;
    }

    print
      "<h5>Text searches are based on case-insensitive substring match.</h5>\n";
    print "<table class='img' border='1'>\n";

    # target database
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Target database</td>\n";
    print "<td class='img'>$database</td></tr>\n";

    # search filter
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Search Filter</td>\n";
    print "<td class='img'>\n";
    print "<select name='search_filter' class='img' size='1'>\n";
    my $s0;

    my @sel_fields =
      ( 'Project name', 'Proposal Name', 'GOLD ID', 'Genus', 'Species' );
    if ( $p_type eq 'M' ) {
        @sel_fields = (
                        'Study Name',
                        'GOLD ID',
                        'Ecosystem',
                        'Ecosystem category',
                        'Ecosystem type',
                        'Ecosystem subtype',
                        'Specific ecosystem'
        );
    } elsif ( $p_type eq 'B' ) {
        @sel_fields = ( 'Project name', 'Study/Proposal Name', 'GOLD ID' );
    }
    for $s0 (@sel_fields) {
        print "    <option value='" . escapeHTML($s0) . "'";
        print ">$s0</option>\n";
    }
    print "</select>\n";
    print "</td></tr>\n";
    print "</td></tr>\n";

    # search keyword
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Search Keyword</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_keyword' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    print "</table>\n";

    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_NewSubmission:selectSubProj" value="Search Projects" class="meddefbutton" />';
    } else {
        print
'<input type="submit" name="_section_NewSubmission:selectSubProj" value="Search Projects" class="meddefbutton" />';

#	print nbsp(1);
#	print '<input type="submit" name="_section_ERSubmission:exportProjectList_noHeader" value="Export Project List" class="medbutton" />';
    }

    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";
    $hint_msg = "If you cannot find the project";
    if ( $p_type eq 'M' ) {
        $hint_msg .= " or sample";
    }
    $hint_msg .= ", you need to create a new one in " . getImgGoldLink() . ".";
    printHint3($hint_msg);

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowKeywordSearchImgMetagenome
###########################################################################
sub ShowKeywordSearchImgMetagenome {
    my ( $database, $p_type ) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );
    my $project_type = param1('project_type_option');

    print "<h2>Search Source Metagenome in IMG</h2>\n";
    $p_type = 'M';
    print hiddenVar( 'p_type',   'M' );
    print hiddenVar( 'database', $database );

    print hiddenVar( 'proj_search_option',  'keyword' );
    print hiddenVar( 'project_type_option', $project_type );

    my $hint_msg = qq{
          In order to submit an extracted genome you need to provide
          genome name and NCBI Taxon ID. Enter the information, and then
          use the following search option to select a source
          metagenome that has aleady been loaded into IMG/M ER.
          };

    printHint3($hint_msg);

    # analysis project name
##    DefineNewAnalysis($project_type, $database);
    print
"<h4>Error: Please define your analysis project in GOLD and then use the AP ID submission option.</h4>\n";
    print end_form();
    return;

    print
      "<h5>Text searches are based on case-insensitive substring match.</h5>\n";
    print "<table class='img' border='1'>\n";

    # search filter
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Search Filter</td>\n";
    print "<td class='img'>\n";
    print "<select name='search_filter' class='img' size='1'>\n";
    my $s0;

    my @sel_fields = (
                       'Study/Proposal Name',
                       'Genome Name',
                       'GOLD ID',
                       'Ecosystem',
                       'Ecosystem category',
                       'Ecosystem type',
                       'Ecosystem subtype',
                       'Specific ecosystem'
    );
    for $s0 (@sel_fields) {
        print "    <option value='" . escapeHTML($s0) . "'";
        print ">$s0</option>\n";
    }
    print "</select>\n";
    print "</td></tr>\n";
    print "</td></tr>\n";

    # search keyword
    print "<tr class='img'>\n";
    print
      "<td class='img' align='left' bgcolor='lightblue'>Search Keyword</td>\n";
    print "<td class='img'>\n";
    print "<input type='text' name='search_keyword' value=''"
      . " size='60' maxLength='255'/>\n";
    print "</td></tr>\n";

    print "</table>\n";

    print
'<input type="submit" name="_section_MSubmission:selectImgMetagenome" value="Search Metagenomes" class="meddefbutton" />';
    print nbsp(1);
    print reset( -name => "Reset", -value => "Reset", -class => "smbutton" );

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###################################################################
# checkUserInput
###################################################################
sub checkUserInput {
    my $project_type = param1('project_type_option');
    my $msg          = "";

    if (    $project_type ne 'combined'
         && $project_type ne 'extract' )
    {
        return "";
    }

    if ( $project_type eq 'combined' ) {
        my $ap_name = param1('ap_name');
        if ( !$ap_name ) {
            $msg = "Analysis name for the combined assembly is not provided.";
        }
    } elsif ( $project_type eq 'extract' ) {
        my $ap_name = param1('ap_name');
        if ( !$ap_name ) {
            $msg = "Name for the extracted genome is not provided.";
            return $msg;
        }

        my $ncbi_tax_id = param1('ncbi_tax_id');
        if ( !$ncbi_tax_id || !isInt($ncbi_tax_id) ) {
            $msg = "NCBI Taxon ID is incorrect.";
        } else {
            my %lineage = getNCBITaxonInfo($ncbi_tax_id);
            my @ranks = (
                          'superkingdom', 'phylum', 'class', 'order',
                          'family',       'genus',  'species'
            );
            for my $s2 (@ranks) {
                my $val2 = $lineage{$s2};
                if ( $val2 =~ /Error/ ) {
                    $msg = "NCBI $s2 is incorrect.";
                    last;
                }
            }
        }
    }

    my $pi_name = param1('pi_name');
    if ( !$pi_name ) {
        $msg = "PI name is not provided.";
    }

    my $pi_email = param1('pi_email');
    if ( !$pi_email ) {
        $msg = "PI email is not provided.";
    }

    my $ap_desc = param1('ap_desc');
    if ( !$ap_desc ) {
        $msg = "Analysis description is not provided.";
    }

    return $msg;
}

###########################################################################
# ShowSearchSubmissionProject_old
###########################################################################
sub ShowSearchSubmissionProject_old {
    my ( $database, $p_type ) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    if ( $p_type eq 'M' ) {
        print "<h2>Search Metagenome Project for Your Submission</h2>\n";
        print hiddenVar( 'p_type', 'M' );
    } else {
        print "<h2>Search Project for Your Submission</h2>\n";
        print hiddenVar( 'p_type', 'G' );
    }
    print "<h3>Target Database: $database</h3>\n";

    print hiddenVar( 'database', $database );

    print "<p>\n";

    #    if ( $database eq 'IMG/M ER' ) {
    #	print "Only metagenome projects can be submitted to IMG/M ER. \n";
    #    }
    print "Go to " . getImgGoldLink() . " if you need to define new projects";
    if ( $p_type eq 'M' ) {
        print " or sample";
    }
    print ".</p>\n";

    print
      "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    # gold stamp ID
    ProjectInfo::printFilterCond( 'GOLD Stamp ID',
                                'search_sp:gold_stamp_id', 'text', '', '', '' );

    # project type
    if ( $p_type eq 'M' ) {

        # Metagenome
        print hiddenVar( 'search_sp:project_type', 'Metagenome' );
        print "<p>Project Type: Metagenome</p>\n";
    } else {

        # Genome or Draft-Genome
        ProjectInfo::printFilterCond(
            'Project Type', 'search_sp:project_type',
            'select',       'list',
            'Genome|Draft-Genome|Metagenome',

#				     "select cv_term from project_typecv where cv_term != 'Metagenome' order by cv_term",
            ''
        );
    }

# project status
# Victor said to remove this
#    ProjectInfo::printFilterCond('Project Status', 'search_sp:project_status',
#                                 'select', 'query',
#                                 'select cv_term from project_statuscv order by cv_term',
#			       '');

    # domain
    if ( $p_type eq 'M' ) {
        print hiddenVar( 'search_sp:domain', 'MICROBIAL' );
        print "<p>Domain: MICROBIAL</p>\n";
    } else {
        my $domain_str = 'ARCHAEAL|BACTERIAL|EUKARYAL|PLASMID|VIRUS';
        ProjectInfo::printFilterCond( 'Domain', 'search_sp:domain',
                                      'select', 'list', $domain_str, '' );
    }

# seq status
#    ProjectInfo::printFilterCond('Sequencing Status', 'search_sp:seq_status',
#                                 'select', 'query',
#                                 'select cv_term from seq_statuscv order by cv_term',
#			       '');

# seq method
#    ProjectInfo::printFilterCond('Sequencing Method', 'search_sp:seq_method',
#                                 'select', 'query',
#                                 'select cv_term from seq_methodcv order by cv_term',
#				 '');

    # availability: Proprietary, Public
    #    ProjectInfo::printFilterCond('Availability', 'search_sp:availability',
    #			       'select', 'list', 'Proprietary|Public', '');

    my $label = "";

# phylogeny
#    $label = 'Phylogeny';
#    if ( $p_type eq 'M' ) {
#	$label = 'Project Category';
#    }
#    ProjectInfo::printFilterCond($label, 'search_sp:phylogeny',
#                                 'select', 'query',
#                                 'select cv_term from phylogenycv order by cv_term',
#				 '');

    # project display name
    ProjectInfo::printFilterCond( 'Project Display Name',
                                 'search_sp:display_name', 'text', '', '', '' );

    # genus
    $label = 'Genus';
    if ( $p_type eq 'M' ) {

        # $label = 'Project Habitat';
        # don't display
    } else {
        ProjectInfo::printFilterCond( $label, 'search_sp:genus',
                                      'text', '', '', '' );
    }

    # species
    $label = 'Species';
    if ( $p_type eq 'M' ) {

        # $label = 'Project Community';
        # don't display
    } else {
        ProjectInfo::printFilterCond( $label, 'search_sp:species',
                                      'text', '', '', '' );
    }

    # common_name
    #    ProjectInfo::printFilterCond('Common Name', 'search_sp:common_name',
    #				 'text', '', '', '');

    # add date
    #    ProjectInfo::printFilterCond('Add Date', 'search_sp:add_date',
    #				 'date', '', '', '');

    # mod date
    #    ProjectInfo::printFilterCond('Last Mod Date', 'search_sp:mod_date',
    #				 'date', '', '', '');

    print "<p>\n";
    if ( $database eq 'IMG/M ER' ) {
        print
'<input type="submit" name="_section_NewSubmission:selectSubProj" value="Search Projects" class="meddefbutton" />';
    } else {
        print
'<input type="submit" name="_section_NewSubmission:selectSubProj" value="Search Projects" class="meddefbutton" />';

#	print nbsp(1);
#	print '<input type="submit" name="_section_ERSubmission:exportProjectList_noHeader" value="Export Project List" class="medbutton" />';
    }

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowSelectSubmissionProject  (use HTML table)
###########################################################################
sub ShowSelectSubmissionProject {

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    print "<h2>Project Search Result</h2>\n";

    printHint3(   "If you cannot find a project for your submission, go to "
                . getImgGoldLink()
                . " to define a new project." );

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    my $proj_search_option = param1('proj_search_option');
    my $cond               = "";
    if ( $proj_search_option eq 'keyword' ) {
        $cond = getKeywordProjectSearchCond( $isAdmin, $contact_oid );
    } else {
        $cond = getQueryProjectSearchCond( $isAdmin, $contact_oid );
    }

    # show results
    my $dbh = Connect_IMG();
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.mod_date
            from project_info p 
        };

    if ( !blankStr($cond) ) {
        $sql .= $cond;
    }
    $sql .= " order by p.project_oid";

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>";
    #    }

    # select project
    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }
    $section = 'NewSubmission';

    my $p_type = param1('p_type');
    if ( $p_type eq 'M' ) {

        # show samples?
        print
"<input type='submit' name='_section_$section:selectSubSample' value='Show Samples' class='meddefbutton' />";
    } else {

#	print "<input type='submit' name='_section_$section:submitProject' value='Select Project' class='meddefbutton' />";
        print
"<input type='submit' name='_section_$section:signAgreement' value='Select Project' class='meddefbutton' />";
    }

    # show search results
    print "<p>\n";
    
webLog("960-- $sql\n");    
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $color1 = '#eeeeee';

    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";
    print "<th class='img' bgcolor='$color1'>ER Project ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Project Display Name</th>\n";
    print "<th class='img' bgcolor='$color1'>GOLD ID</th>\n";
    if ( $p_type eq 'M' ) {
        print "<th class='img' bgcolor='$color1'>Project Category</th>\n";
    } else {
        print "<th class='img' bgcolor='$color1'>Phylogeny</th>\n";
    }
    print "<th class='img' bgcolor='$color1'>Add Date</th>\n";
    print "<th class='img' bgcolor='$color1'>Last Mod Date</th>\n";

    my $cnt2     = 0;
    my $too_many = 0;

    for ( ; ; ) {
        my ( $proj_id, $proj_name, $gold_stamp_id,
             $phylo, $add_date, $mod_date ) = $cur->fetchrow_array();
        if ( !$proj_id ) {
            last;
        }

        $cnt2++;

        if ( $cnt2 % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n";

        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);

        PrintAttribute($proj_name);
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);

        PrintAttribute($phylo);

        PrintAttribute($add_date);
        PrintAttribute($mod_date);
        print "</tr>\n";

        if ( $cnt2 >= 20000 ) {
            $too_many = 1;
            last;
        }
    }
    print "</table>\n";

    print "<p>\n";
    if ($too_many) {
        print
"<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    } else {
        print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p>\n";

    if ( $cnt2 == 0 ) {
        printHomeLink();
        print end_form();
        return;
    }

    if ( $p_type eq 'M' ) {

        # show samples?
        #	$section = 'MSubmission';
        $section = 'NewSubmission';
        print
"<input type='submit' name='_section_$section:selectSubSample' value='Show Samples' class='meddefbutton' />";
    } else {

# select project
#	print "<input type='submit' name='_section_$section:submitProject' value='Select Project' class='meddefbutton' />";
        print
"<input type='submit' name='_section_$section:signAgreement' value='Select Project' class='meddefbutton' />";
    }

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowSelectSubmissionProject_yui  (use Yahoo datatable)
###########################################################################
sub ShowSelectSubmissionProject_yui {

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $project_type = param1('project_type_option');
    print hiddenVar( 'project_type_option', $project_type );
    if ( $project_type eq 'combined' ) {
        my $ap_name = param1('ap_name');
        print "<h4>Analysis Name: $ap_name</h4>\n";
        print hiddenVar( 'ap_name', $ap_name );
    } elsif ( $project_type eq 'extract' ) {
        my $ap_name = param1('ap_name');
        print "<h4>Extracted Genome Name: $ap_name</h4>\n";
        print hiddenVar( 'ap_name', $ap_name );
    }

    my $ncbi_tax_id = param1('ncbi_tax_id');
    if ( isInt($ncbi_tax_id) ) {
        print hiddenVar( 'ncbi_tax_id', $ncbi_tax_id );
        my %lineage = getNCBITaxonInfo($ncbi_tax_id);
        print "<p>NCBI Taxon ID $ncbi_tax_id: \n";
        my @ranks =
          ( 'superkingdom', 'phylum', 'class', 'order', 'family', 'genus' );
        for my $s2 (@ranks) {
            print $lineage{$s2} . '; ';
        }
        if ( $lineage{'species'} ) {
            print $lineage{'species'};
        } else {
            print "sp.";
        }
    }

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    for
      my $tag ( 'visibility', 'pi_name', 'pi_email', 'ap_desc', 'submitter_id' )
    {
        my $val = param($tag);
        if ($val) {
            print hiddenVar( $tag, $val );
        }
    }

    my $isAdmin = getIsAdmin($contact_oid);

    print "<h2>Project Search Result</h2>\n";

    printHint3(   "If you cannot find a project for your submission, go to "
                . getImgGoldLink()
                . " to define a new project." );

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    my $proj_search_option = param1('proj_search_option');
    my $cond               = "";
    if ( $proj_search_option eq 'keyword' ) {
        $cond = getKeywordProjectSearchCond( $isAdmin, $contact_oid );
    } else {
        $cond = getQueryProjectSearchCond( $isAdmin, $contact_oid );
    }

    # show results
    my $dbh = Connect_IMG();
    my $sql = qq{
        select p.project_oid, p.display_name, 
        p.proposal_name, p.gold_stamp_id,
        p.domain, p.phylogeny, p.ecosystem,
        to_char(p.add_date, 'yyyy-mm-dd'),
	to_char(p.mod_date, 'yyyy-mm-dd')
            from project_info p 
        };

    if ( !blankStr($cond) ) {
        $sql .= $cond;
    }

    #    $sql .= " order by p.project_oid";

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>";
    #    }

    # select project
    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }
    $section = 'NewSubmission';

    my $p_type = param1('p_type');

    # show search results
    print "<p>\n";

webLog("970-- $sql\n");    
    
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $it = new InnerTable( 1, "searchProject$$", "searchProject", 0 );
    my $sd = $it->getSdDelim();    # sort delimiter
    $it->addColSpec("Select");
    $it->addColSpec(
                     "ER Project ID",
                     "number asc",
                     "right",
                     "",
                     "ER Project ID"
    );

    if ( $database eq 'IMG ER' ) {
        $it->addColSpec(
                         "Project Name",
                         "char asc",
                         "left",
                         "",
                         "Project Name"
        );
    } else {
        $it->addColSpec( "Study Name", "char asc", "left", "", "Study Name" );
    }
    $it->addColSpec( "Proposal Name", "char asc", "left", "", "Proposal Name" );
    $it->addColSpec( "GOLD ID",       "char asc", "left", "", "GOLD ID" );

    if ( $p_type eq 'M' || $p_type eq 'B' ) {
        $it->addColSpec(
                         "Project Category",
                         "char asc",
                         "left",
                         "",
                         "Project Category"
        );
    } else {
        $it->addColSpec( "Phylogeny", "char asc", "left", "", "Phylogeny" );
    }

    $it->addColSpec( "Add Date",      "char asc", "left", "", "Add Date" );
    $it->addColSpec( "Last Mod Date", "char asc", "left", "", "Last Mod Date" );

    my $cnt2        = 0;
    my $button_type = 'radio';
    if (    $project_type eq 'combined'
         || $project_type eq 'extract' )
    {
        $button_type = 'checkbox';
    }

    for ( ; ; ) {
        my (
             $proj_id,       $proj_name, $proposal_name,
             $gold_stamp_id, $domain,    $phylo,
             $eco,           $add_date,  $mod_date
        ) = $cur->fetchrow_array();
        if ( !$proj_id ) {
            last;
        }

        $cnt2++;

        my $row = "";

        $row .=
            $sd
          . "<input type='$button_type' name='project_oid' "
          . "value='$proj_id'" . "/>\t";

        my $proj_link = getProjectLink($proj_id);
        $row .= $proj_id . $sd . $proj_link . "\t";

        $row .= $proj_name . $sd . $proj_name . "\t";
        $row .= $proposal_name . $sd . $proposal_name . "\t";

        if ($gold_stamp_id) {
            my $gold_link = getGoldLink($gold_stamp_id);
            $row .= $gold_stamp_id . $sd . $gold_link . "\t";
        } else {
            $row .= " " . $sd . " " . "\t";
        }

        if ( $domain eq 'MICROBIAL' ) {
            $row .= $eco . $sd . $eco . "\t";
        } else {
            $row .= $phylo . $sd . $phylo . "\t";
        }

        $row .= $add_date . $sd . $add_date . "\t";
        $row .= $mod_date . $sd . $mod_date . "\t";

        $it->addRow($row);
    }

    print "<p>\n";

    if ( $cnt2 > 0 ) {
        if ( $p_type eq 'G' && $project_type eq 'combined' ) {
            print
"<input type='submit' name='_section_$section:selectSubProposal' value='Select Project(s)' class='meddefbutton' />";
        } elsif ( $p_type eq 'M' || $p_type eq 'B' ) {

            # show samples?
            print
"<input type='submit' name='_section_$section:selectSubSample' value='Show Samples' class='meddefbutton' />";
        } else {

#	    print "<input type='submit' name='_section_$section:submitProject' value='Select Project' class='meddefbutton' />";
            print
"<input type='submit' name='_section_$section:signAgreement' value='Select Project' class='meddefbutton' />";
        }

        $it->printOuterTable(1);
        print
          "<p><font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
        print "<p>\n";
        if ( $p_type eq 'G' && $project_type eq 'combined' ) {
            print
"<input type='submit' name='_section_$section:selectSubProposal' value='Select Project(s)' class='meddefbutton' />";
        } elsif ( $p_type eq 'M' || $p_type eq 'B' ) {

            # show samples?
            #	    $section = 'MSubmission';
            $section = 'NewSubmission';
            print
"<input type='submit' name='_section_$section:selectSubSample' value='Show Samples' class='meddefbutton' />";
        } else {

# select project
#	    print "<input type='submit' name='_section_$section:submitProject' value='Select Project' class='meddefbutton' />";
            print
"<input type='submit' name='_section_$section:signAgreement' value='Select Project' class='meddefbutton' />";
        }
    } else {
        print "<h5>No projects found.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowSelectImgMetagenome_yui  (use Yahoo datatable)
###########################################################################
sub ShowSelectImgMetagenome_yui {

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $project_type = param1('project_type_option');
    print hiddenVar( 'project_type_option', $project_type );

    my $ap_name = param1('ap_name');
    print "<h4>Extracted Genome Name: $ap_name ($project_type)</h4>\n";
    print hiddenVar( 'ap_name', $ap_name );

    my $ncbi_tax_id = param1('ncbi_tax_id');
    if ( isInt($ncbi_tax_id) ) {
        print hiddenVar( 'ncbi_tax_id', $ncbi_tax_id );
        my %lineage = getNCBITaxonInfo($ncbi_tax_id);
        print "<p>NCBI Taxon ID $ncbi_tax_id: \n";
        my @ranks =
          ( 'superkingdom', 'phylum', 'class', 'order', 'family', 'genus' );
        for my $s2 (@ranks) {
            print $lineage{$s2} . '; ';
        }
        if ( $lineage{'species'} ) {
            print $lineage{'species'};
        } else {
            print "sp.";
        }
    }

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    for
      my $tag ( 'visibility', 'pi_name', 'pi_email', 'ap_desc', 'submitter_id' )
    {
        my $val = param($tag);
        if ($val) {
            print hiddenVar( $tag, $val );
        }
    }

    my $isAdmin = getIsAdmin($contact_oid);

    print "<h2>Metagenome Search Result</h2>\n";

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    my $proj_search_option = param1('proj_search_option');
    my $cond               = "";
    if ( $proj_search_option eq 'keyword' ) {
        $cond = getKeywordMetagenomeSearchCond( $isAdmin, $contact_oid );
    } else {
        $cond = getQueryMetagenomeSearchCond( $isAdmin, $contact_oid );
    }

    # show results
    my $sql = qq{
        select t.taxon_oid, t.taxon_display_name,
        t.proposal_name, t.gold_id,
        t.phylum, t.ir_class, t.ir_order, t.family,
        t.genus
        from taxon t
        where t.genome_type = 'metagenome'
        and t.obsolete_flag = 'No'
        };

    if ( !blankStr($cond) ) {
        $sql .= $cond;
    }

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>";
    #    }

    my $dbh = Connect_IMG_MI();

    # select project
    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }

    my $p_type = param1('p_type');

    # show search results
    print "<p>\n";

webLog("975-- $sql\n");    
    
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $it = new InnerTable( 1, "searchMetagenome$$", "searchMetagenome", 0 );
    my $sd = $it->getSdDelim();    # sort delimiter
    $it->addColSpec("Select");
    $it->addColSpec(
                     "IMG Taxon OID",
                     "number asc",
                     "right",
                     "",
                     "IMG Taxon OID"
    );
    $it->addColSpec(
                     "Study/Proposal Name",
                     "char asc",
                     "left",
                     "",
                     "Study/Proposal Name"
    );
    $it->addColSpec( "Genome Name", "char asc", "left", "", "Genome Name" );
    $it->addColSpec( "GOLD ID",     "char asc", "left", "", "GOLD ID" );

    for my $s2 (
                 'Ecosystem',
                 'Ecosystem category',
                 'Ecosystem type',
                 'Ecosystem subtype',
                 'Specific ecosystem'
      )
    {
        $it->addColSpec( $s2, "char asc", "left", "", $s2 );
    }

    my $cnt2        = 0;
    my $button_type = 'radio';

    for ( ; ; ) {
        my (
             $taxon_oid, $taxon_name, $proposal_name,
             $gold_id,   $phylum,     $ir_class,
             $ir_order,  $family,     $genus
        ) = $cur->fetchrow_array();
        if ( !$taxon_oid ) {
            last;
        }

        $cnt2++;

        my $row = "";

        $row .=
            $sd
          . "<input type='$button_type' name='taxon_oid' "
          . "value='$taxon_oid'" . "/>\t";

        $row .= $taxon_oid . $sd . $taxon_oid . "\t";

        $row .= $proposal_name . $sd . $proposal_name . "\t";
        $row .= $taxon_name . $sd . $taxon_name . "\t";

        if ($gold_id) {
            my $gold_link = getGoldLink($gold_id);
            $row .= $gold_id . $sd . $gold_link . "\t";
        } else {
            $row .= " " . $sd . " " . "\t";
        }

        $row .= $phylum . $sd . $phylum . "\t";
        $row .= $ir_class . $sd . $ir_class . "\t";
        $row .= $ir_order . $sd . $ir_order . "\t";
        $row .= $family . $sd . $family . "\t";
        $row .= $genus . $sd . $genus . "\t";

        $it->addRow($row);
    }

    print "<p>\n";

    if ( $cnt2 > 0 ) {
        print
"<input type='submit' name='_section_$section:selectSubProposal' value='Select Metagenome' class='meddefbutton' />";

        $it->printOuterTable(1);

        print
"<input type='submit' name='_section_$section:selectSubProposal' value='Select Metagenome' class='meddefbutton' />";
    } else {
        print "<h5>No metagenomes found.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# getQueryProjectSearchCond
###########################################################################
sub getQueryProjectSearchCond {
    my ( $isAdmin, $contact_oid ) = @_;

    # search condition
    my @all_params = (
        'search_sp:gold_stamp_id',
        'search_sp:project_type',
        'search_sp:project_status',
        'search_sp:proposal_name',

        #                       'search_sp:domain',
        'search_sp:seq_status',
        'search_sp:seq_method',
        'search_sp:availability',
        'search_sp:phylogeny',
        'search_sp:display_name',
        'search_sp:genus',
        'search_sp:species',
        'search_sp:common_name',
        'search_sp:add_date',
        'search_sp:mod_date',
        'search_sp:genus_or_species',
        'search_sp:any_ecosystem_field',
        'search_sp:ecosystem',
        'search_sp:ecosystem_category',
        'search_sp:ecosystem_type',
        'search_sp:ecosystem_subtype',
        'search_sp:specific_ecosystem'
    );

    my $cond = "";
    if ( $isAdmin ne 'Yes' ) {
        $cond =
            " where ( p.contact_oid = $contact_oid"
          . " or p.gold_stamp_id is not null "
          . " or p.project_oid in (select cpp.project_permissions from"
          . " contact_project_permissions cpp where cpp.contact_oid = $contact_oid)) "
          . " and (p.gold_stamp_id is null or p.gold_stamp_id not like 'Gpto%') ";
    } else {
        $cond =
" where (p.gold_stamp_id is null or p.gold_stamp_id not like 'Gpto%') ";
    }

    # condition on domain
    my $domain_cond = param1('search_sp:domain');
    my $p_type      = param1('p_type');
    if ( blankStr($domain_cond) ) {

        # add default condition
        if ( $p_type eq 'B' ) {
            $domain_cond = "p.domain is not null";
        } elsif ( $p_type eq 'M' ) {
            $domain_cond = "p.domain = 'MICROBIAL'";
        } else {
            $domain_cond = "p.domain != 'MICROBIAL'";
        }
    } else {

        # use user-specified condition
        $domain_cond = "p.domain = '" . $domain_cond . "'";
    }

    if ( blankStr($cond) ) {
        $cond = " where " . $domain_cond;
    } else {
        $cond .= " and " . $domain_cond;
    }

    for my $p0 (@all_params) {
        my $cond1 = "";
        if ( param1($p0) ) {
            my @vals = split( /\,/, param1($p0) );
            my ( $tag, $fld_name ) = split( /\:/, $p0 );

            if ( lc($fld_name) eq 'genus_or_species' ) {

                # special genus, species search
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) {
                    $cond1 =
                        "(lower(p.genus) like '%"
                      . lc($s1) . "%'"
                      . " or lower(p.species) like '%"
                      . lc($s1) . "%')";
                }
            } elsif ( lc($fld_name) eq 'any_ecosystem_field' ) {

                # special ecosystem search
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) {
                    $cond1 =
                        "(lower(p.ecosystem) like '%"
                      . lc($s1) . "%'"
                      . " or lower(p.ecosystem_category) like '%"
                      . lc($s1) . "%'"
                      . " or lower(p.ecosystem_type) like '%"
                      . lc($s1) . "%'"
                      . " or lower(p.ecosystem_subtype) like '%"
                      . lc($s1) . "%'"
                      . " or lower(p.specific_ecosystem) like '%"
                      . lc($s1) . "%')";
                }
            } elsif (    lc($fld_name) eq 'display_name'
                      || lc($fld_name) eq 'genus'
                      || lc($fld_name) eq 'species'
                      || lc($fld_name) eq 'common_name'
                      || lc($fld_name) eq 'proposal_name'
                      || lc($fld_name) eq 'gold_stamp_id' )
            {
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) {
                    $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
                }
            } elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) {
                    $cond1 =
                        "p.project_oid in (select pism.project_oid "
                      . "from project_info_seq_method pism "
                      . "where lower(pism.$fld_name) = '"
                      . lc($s1) . "')";
                }
            } elsif (    lc($fld_name) eq 'add_date'
                      || lc($fld_name) eq 'mod_date' )
            {
                my $op_name = $p0 . ":op";
                my $op1     = param1($op_name);
                my $d1      = param1($p0);
                if ( !blankStr($op1) && !blankStr($d1) ) {
                    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
                }
            } else {
                for my $s2 (@vals) {
                    $s2 =~ s/'/''/g;    # replace ' with ''
                    if ( blankStr($cond1) ) {
                        $cond1 = "p.$fld_name in ('" . $s2 . "'";
                    } else {
                        $cond1 .= ", '" . $s2 . "'";
                    }
                }

                if ( !blankStr($cond1) ) {
                    $cond1 .= ")";
                }
            }
        }

        if ( !blankStr($cond1) ) {
            if ( blankStr($cond) ) {
                $cond = " where $cond1";
            } else {
                $cond .= " and " . $cond1;
            }
        }
    }    # end for p0

    return $cond;
}

###########################################################################
# getQueryMetagenomeSearchCond
###########################################################################
sub getQueryMetagenomeSearchCond {
    my ( $isAdmin, $contact_oid ) = @_;

    # search condition
    my @all_params = (
                       'search_sp:gold_id',
                       'search_sp:proposal_name',
                       'search_sp:taxon_display_name',
                       'search_sp:any_ecosystem_field',
                       'search_sp:ecosystem',
                       'search_sp:ecosystem_category',
                       'search_sp:ecosystem_type',
                       'search_sp:ecosystem_subtype',
                       'search_sp:specific_ecosystem'
    );

    my $cond = "";
    if ( $isAdmin ne 'Yes' ) {
        $cond =
            " and ( t.is_public = 'Yes' "
          . " or t.taxon_oid in (select taxon_permissions from"
          . " contact_project_permissions where contact_oid = $contact_oid))";
    }

    for my $p0 (@all_params) {
        my $cond1 = "";
        if ( param1($p0) ) {
            my @vals = split( /\,/, param1($p0) );
            my ( $tag, $fld_name ) = split( /\:/, $p0 );

            if ( lc($fld_name) eq 'any_ecosystem_field' ) {

                # special ecosystem search
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) {
                    $cond1 =
                        "(lower(t.phylum) like '%"
                      . lc($s1) . "%'"
                      . " or lower(t.ir_class) like '%"
                      . lc($s1) . "%'"
                      . " or lower(t.ir_order) like '%"
                      . lc($s1) . "%'"
                      . " or lower(t.family) like '%"
                      . lc($s1) . "%'"
                      . " or lower(t.genus) like '%"
                      . lc($s1) . "%')";
                }
            } elsif (    lc($fld_name) eq 'taxon_display_name'
                      || lc($fld_name) eq 'proposal_name'
                      || lc($fld_name) eq 'gold_id' )
            {
                my $s1 = param1($p0);
                $s1 =~ s/'/''/g;    # replace ' with ''
                if ( length($s1) > 0 ) {
                    $cond1 = "lower(t.$fld_name) like '%" . lc($s1) . "%'";
                }
            } else {
                my $db_fld = $fld_name;
                if ( $fld_name eq 'ecosystem' ) {
                    $db_fld = 'phylum';
                } elsif ( $fld_name eq 'ecosystem_category' ) {
                    $db_fld = 'ir_class';
                } elsif ( $fld_name eq 'ecosystem_type' ) {
                    $db_fld = 'ir_order';
                } elsif ( $fld_name eq 'ecosystem_subtype' ) {
                    $db_fld = 'family';
                } elsif ( $fld_name eq 'specific_ecosystem' ) {
                    $db_fld = 'genus';
                }

                for my $s2 (@vals) {
                    $s2 =~ s/'/''/g;    # replace ' with ''
                    if ( blankStr($cond1) ) {
                        $cond1 = "t.$db_fld in ('" . $s2 . "'";
                    } else {
                        $cond1 .= ", '" . $s2 . "'";
                    }
                }

                if ( !blankStr($cond1) ) {
                    $cond1 .= ")";
                }
            }
        }

        if ($cond1) {
            $cond .= " and " . $cond1;
        }
    }    # end for p0

    return $cond;
}

###########################################################################
# getKeywordProjectSearchCond
###########################################################################
sub getKeywordProjectSearchCond {
    my ( $isAdmin, $contact_oid ) = @_;

    my $cond = "";

    if ( $isAdmin ne 'Yes' ) {
        $cond =
            " where ( p.contact_oid = $contact_oid"
          . " or p.gold_stamp_id is not null "
          . " or p.project_oid in (select cpp.project_permissions from"
          . " contact_project_permissions cpp where cpp.contact_oid = $contact_oid))"
          . " and (p.gold_stamp_id is null or p.gold_stamp_id not like 'Gpto%') ";
    } else {
        $cond =
" where (p.gold_stamp_id is null or p.gold_stamp_id not like 'Gpto%') ";
    }

    # condition on domain
    my $domain_cond = param1('search_sp:domain');
    my $p_type      = param1('p_type');
    if ( blankStr($domain_cond) ) {

        # add default condition
        if ( $p_type eq 'M' ) {
            $domain_cond = "p.domain = 'MICROBIAL'";
        } elsif ( $p_type eq 'G' ) {
            $domain_cond = "p.domain != 'MICROBIAL'";
        } else {
            $domain_cond = "p.domain is not null";
        }
    } else {

        # use user-specified condition
        $domain_cond = "p.domain = '" . $domain_cond . "'";
    }

    if ( blankStr($cond) ) {
        $cond = " where " . $domain_cond;
    } else {
        $cond .= " and " . $domain_cond;
    }

    my $search_filter  = param1('search_filter');
    my $search_keyword = param1('search_keyword');
    $search_keyword =~ s/'/''/g;    # replace ' with ''

    if ( blankStr($search_filter) || blankStr($search_keyword) ) {
        return $cond;
    }

    if ( $search_filter eq 'Project name' ) {
        $cond .=
          " and lower(p.display_name) like '%" . lc($search_keyword) . "%'";
    } elsif ( $search_filter =~ /Study Name/ ) {
        $cond .=
          " and lower(p.display_name) like '%" . lc($search_keyword) . "%'";
    } elsif ( $search_filter =~ /Proposal Name/ ) {
        $cond .=
          " and lower(p.proposal_name) like '%" . lc($search_keyword) . "%'";
    } elsif ( $search_filter eq 'GOLD ID' ) {
        $cond .=
          " and lower(p.gold_stamp_id) like '%" . lc($search_keyword) . "%'";
    } elsif ( $search_filter eq 'Genus' ) {
        $cond .= " and lower(p.genus) like '%" . lc($search_keyword) . "%'";
    } elsif ( $search_filter eq 'Species' ) {
        $cond .= " and lower(p.species) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem' ) {
        $cond .= " and lower(p.ecosystem) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem category' ) {
        $cond .= " and lower(p.ecosystem_category) like '%"
          . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem type' ) {
        $cond .=
          " and lower(p.ecosystem_type) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem subtype' ) {
        $cond .= " and lower(p.ecosystem_subtype) like '%"
          . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'specific ecosystem' ) {
        $cond .= " and lower(p.specific_ecosystem) like '%"
          . lc($search_keyword) . "%'";
    }

    return $cond;
}

###########################################################################
# getKeywordMetagenomeSearchCond
###########################################################################
sub getKeywordMetagenomeSearchCond {
    my ( $isAdmin, $contact_oid ) = @_;

    my $cond = "";

    if ( $isAdmin ne 'Yes' ) {
        $cond =
            " and ( t.is_public = 'Yes' "
          . " or t.taxon_oid in (select taxon_permissions from"
          . " contact_project_permissions where contact_oid = $contact_oid))";
    }

    my $search_filter  = param1('search_filter');
    my $search_keyword = param1('search_keyword');
    $search_keyword =~ s/'/''/g;    # replace ' with ''

    if ( blankStr($search_filter) || blankStr($search_keyword) ) {
        return $cond;
    }

    if ( $search_filter eq 'Genome Name' ) {
        $cond .= " and lower(t.taxon_display_name) like '%"
          . lc($search_keyword) . "%'";
    } elsif (    $search_filter =~ /Study Name/
              || $search_filter =~ /Proposal Name/ )
    {
        $cond .=
          " and lower(t.proposal_name) like '%" . lc($search_keyword) . "%'";
    } elsif ( $search_filter eq 'GOLD ID' ) {
        $cond .= " and lower(t.gold_id) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem' ) {
        $cond .= " and lower(t.phylum) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem category' ) {
        $cond .= " and lower(t.ir_class) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem type' ) {
        $cond .= " and lower(t.ir_order) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'ecosystem subtype' ) {
        $cond .= " and lower(t.family) like '%" . lc($search_keyword) . "%'";
    } elsif ( lc($search_filter) eq 'specific ecosystem' ) {
        $cond .= " and lower(t.genus) like '%" . lc($search_keyword) . "%'";
    }

    return $cond;
}

######################################################################
# printCreateAP
######################################################################
sub printCreateAP {

    my $gold_ap_id = "";
    my $msg        = "";

    my @projects         = param1('project_oid');
    my @isolate_projects = ();
    for my $p2 (@projects) {
        if ($p2) {
            my $domain = db_getValue(
                     "select domain from project_info where project_oid = $p2");
            if ( $domain ne 'MICROBIAL' ) {
                push @isolate_projects, ($p2);
            }
        }
    }

    my @samples = param1('sample_oid');

    my $project_type = param1('project_type_option');
    my $taxon_oid    = param1('taxon_oid');
    if ( $project_type eq 'extract' ) {
        if ( !$taxon_oid ) {
            $msg = "No metagenome has been selected.";
            return ( "", $msg );
        }
    }

    my @ap_lookup;
    my %json_data;

    $json_data{'analysisProjectName'} = param1('ap_name');

    if ( param1('ncbi_tax_id') ) {
        $json_data{'ncbiTaxonId'} = int param1('ncbi_tax_id');
    }

    if (    param1('visibility') eq 'public'
         || param1('visibility') eq 'Yes' )
    {
        $json_data{'visibility'} = "Yes";
    } else {
        $json_data{'visibility'} = "No";
    }
    if ( param1('proposal_name') ) {
        $json_data{'goldProposalName'} = param1('proposal_name');
    }

    if ( param1('pi_name') ) {
        $json_data{'piName'} = param1('pi_name');
    }
    if ( param1('pi_email') ) {
        $json_data{'piEmail'} = param1('pi_email');
    }
    if ( param1('ap_desc') ) {
        $json_data{'comments'} = param1('ap_desc');
    }

    my $submitter_id = param1('submitter_id');
    my $contact_oid  = getContactOid();
    if ( !$submitter_id || !isInt($submitter_id) ) {
        $submitter_id = $contact_oid;
    }
    $json_data{'submitterContactOid'} = int $submitter_id;

    if ($taxon_oid) {
        $json_data{'referenceImgOid'} = int $taxon_oid;
    }

    for my $project_oid (@isolate_projects) {
        if ( !$project_oid ) {
            next;
        }

        push @ap_lookup, ( { 'projectOid' => int $project_oid } );
    }

    for my $sample_oid (@samples) {
        if ( !$sample_oid ) {
            next;
        }

        push @ap_lookup, ( { 'sampleOid' => int $sample_oid } );
    }

    if ( scalar(@ap_lookup) == 0 ) {
        $msg = "No projects or samples have been selected.";
        return ( "", $msg );
    }

    $json_data{'projects'} = \@ap_lookup;

    if ( $project_type eq 'combined' ) {
        if ( scalar(@ap_lookup) > 1 ) {
            $json_data{'goldAnalysisProjectType'} = "combined assembly";
        } else {
            $json_data{'goldAnalysisProjectType'} = "reassembly";
        }
    } else {
        $json_data{'goldAnalysisProjectType'} = "genome from metagenomes";
    }

    my $data = encode_json( \%json_data );

    my $debug_mode = 0;
    if ($debug_mode) {
        $gold_ap_id = "Ga0010876";
        $msg        = "$data";
    } else {
        ( $gold_ap_id, $msg ) = postGoldAnalysisProject($data);
        if ( !$gold_ap_id ) {
            $msg = "Cannot create analysis project (code: $msg).<br/>" . $data;
        }
    }

    return ( $gold_ap_id, $msg );
}

######################################################################
# printSignAgreement
######################################################################
sub printSignAgreement {
    my ($gold_ap_id) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $dbh = Connect_IMG();
    my $sql = qq{
        select type, notes
        from announcement
        where type = 'Agreement'
        };
        
webLog("980-- $sql\n");
        
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ( $type, $notes ) = $cur->fetchrow();
    $cur->finish();
    $dbh->disconnect();

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );
    my $project_oid = param1('project_oid');
    print hiddenVar( 'project_oid', $project_oid );
    my @samples = param('sample_oid');
    for my $s2 (@samples) {
        print hiddenVar( 'sample_oid', $s2 );
    }
    if ($gold_ap_id) {
        print hiddenVar( 'gold_ap_id', $gold_ap_id );
    }
    if ( param1('ap_name') ) {
        print hiddenVar( 'ap_name', param1('ap_name') );
    }

    if ($gold_ap_id) {
        my $contact_oid = getContactOid();
        my $isAdmin     = getIsAdmin($contact_oid);
        if ( $isAdmin eq 'Yes' ) {
            my $s2 = "created";
            if ( param1('select_gold_ap_id') ) {
                $s2 = "selected";
            }
            print "<p><b>GOLD Analysis Project $gold_ap_id ("
              . param1('ap_name')
              . ") $s2.</b><br/>\n";
        }
    }

    my $ap_locus_tag = param1('ap_locus_tag');
    if ($ap_locus_tag) {
        print hiddenVar( 'ap_locus_tag', $ap_locus_tag );
    }

    my $ap_domain = param1('ap_domain');
    if ($ap_domain) {
        print hiddenVar( 'ap_domain', $ap_domain );
    }

    my $special_euk = param1('special_euk');
    if ($special_euk) {
        print hiddenVar( 'special_euk', $special_euk );
        print "<p>(This is a special Eukaryal genome submission.)\n";
    }

    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
        $section = 'MSubmission';
    }
    $section = 'NewSubmission';

    print $notes;
    print "\n";

    print "<p>\n";
    if ( $database eq 'IMG/M ER' ) {
        print
"<input type='submit' name='_section_$section:submitSample' value='I Agree' class='meddefbutton' />";
    } else {

#	print '<input type="submit" name="_section_Submission:submitProject" value="I Agree" class="meddefbutton" />';
        print
"<input type='submit' name='_section_$section:submitProject' value='I Agree' class='meddefbutton' />";
    }

    print end_form();
}

###########################################################################
# checkIsolateGenomeProject
###########################################################################
sub checkIsolateGenomeProject {
    my $msg = "";

    # per Nikos, no checking
    return $msg;

    my $additional_msg = "";

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) ) {
        $msg = "No project has been selected.";
        return $msg;
    }

    my $dbh = Connect_IMG();
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
	p.proj_desc, p.contact_name, p.contact_email,
        p.isolation, p.iso_country, p.seq_country
            from project_info p
	    where p.project_oid = ?
	};

webLog("986-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute($project_oid);

    my (
         $proj_id,   $proj_name,    $gold_stamp_id,
         $proj_desc, $contact_name, $contact_email,
         $isolation, $iso_country,  $seq_country
    ) = $cur->fetchrow_array();
    $cur->finish();

    my @null_flds = ();
    if ( !$proj_id ) {
        $msg = "Project does not exist.";
    } else {

        # display_name
        if ( blankStr($proj_name) ) {
            push @null_flds, ("Project Display Name");
        }

        # project description
        if ( blankStr($proj_desc) ) {
            push @null_flds, ("Project Description");
        }

        # contact name
        if ( blankStr($contact_name) ) {
            push @null_flds, ("Contact Name");
        }

        # contact email
        if ( blankStr($contact_email) ) {
            push @null_flds, ("Contact Email");
        }

        # isolation
        if ( blankStr($isolation) ) {
            push @null_flds, ("Isolation Site");
        }
        if ( blankStr($iso_country) ) {
            push @null_flds, ("Isolation Country");
        }

        # sequencing country
        if ( blankStr($seq_country) ) {
            push @null_flds, ("Sequencing Country");
        }

        # seq center
        my $cnt1 = db_getValue(
"select count(*) from project_info_data_links where project_oid = $project_oid and link_type = 'Seq Center'"
        );
        if ( !$cnt1 ) {
            push @null_flds, ("Sequencing Center");
            $additional_msg =
" Sequencing Center information is specified in the Data Links (URLs) section in the Project Info tab. Select 'Seq Center' in the Link Type, and then select sequencing center name in the Source Name. ";
        }

# sequencing method
# check in sample instead
#	my $cnt2 = db_getValue("select count(*) from project_info_seq_method where project_oid = $project_oid");
#	if ( ! $cnt2 ) {
#	    push @null_flds, ( "Sequencing Method" );
#	}

        if ( scalar(@null_flds) > 0 ) {
            for my $fld (@null_flds) {
                if ( blankStr($msg) ) {
                    $msg =
"You have selected a project that is not completely defined. The following information must be provided: ";
                } else {
                    $msg .= ", ";
                }
                $msg .= $fld;
            }

            $msg .= ". ";
            $msg .= $additional_msg;
            $msg .=
                "<p>Please go to "
              . getImgGoldLink()
              . " to complete the project definition first.";
        }
    }

    if ( !blankStr($msg) ) {
        $dbh->disconnect();
        return $msg;
    }

    $dbh->disconnect();
    return $msg;
}

###########################################################################
# checkMetagenomeProject
###########################################################################
sub checkMetagenomeProject {
    my $msg = "";

    # per Nikos, no checking
    return $msg;

    my $additional_msg = "";

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    my $project_oid = param1('project_oid');
    if ( blankStr($project_oid) ) {
        $msg = "No project has been selected.";
        return $msg;
    }

    my $dbh = Connect_IMG();
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
	p.proj_desc, 
	p.contact_name, p.contact_email
            from project_info p
	    where p.project_oid = ?
	};

webLog("987-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute($project_oid);

    my (
         $proj_id,   $proj_name,    $gold_stamp_id,
         $proj_desc, $contact_name, $contact_email
    ) = $cur->fetchrow_array();
    $cur->finish();

    my @null_flds = ();
    if ( !$proj_id ) {
        $msg = "Project does not exist.";
    } else {

        # display_name
        if ( blankStr($proj_name) ) {
            push @null_flds, ("Project Display Name");
        }

        # project description
        if ( blankStr($proj_desc) ) {
            push @null_flds, ("Project Description");
        }

        # contact name
        if ( blankStr($contact_name) ) {
            push @null_flds, ("Contact Name");
        }

        # contact email
        if ( blankStr($contact_email) ) {
            push @null_flds, ("Contact Email");
        }

        # seq center
        my $cnt1 = db_getValue(
"select count(*) from project_info_data_links where project_oid = $project_oid and link_type = 'Seq Center'"
        );
        if ( !$cnt1 ) {
            push @null_flds, ("Sequencing Center");
            $additional_msg =
" Sequencing Center information is specified in the Data Links (URLs) section in the Project Info tab. Select 'Seq Center' in the Link Type, and then select sequencing center name in the Source Name. ";
        }

# sequencing method
# check in sample instead
#	my $cnt2 = db_getValue("select count(*) from project_info_seq_method where project_oid = $project_oid");
#	if ( ! $cnt2 ) {
#	    push @null_flds, ( "Sequencing Method" );
#	}

        if ( scalar(@null_flds) > 0 ) {
            for my $fld (@null_flds) {
                if ( blankStr($msg) ) {
                    $msg =
"You have selected a project that is not completely defined. The following information must be provided: ";
                } else {
                    $msg .= ", ";
                }
                $msg .= $fld;
            }

            $msg .= ". ";
            $msg .= $additional_msg;
            $msg .=
                "<p>Please go to "
              . getImgGoldLink()
              . " to complete the project definition first.";
        }
    }

    if ( !blankStr($msg) ) {
        $dbh->disconnect();
        return $msg;
    }

    $dbh->disconnect();
    return $msg;
}

###########################################################################
# ShowSelectSubmissionSample   (use HTML table)
###########################################################################
sub ShowSelectSubmissionSample {
    my ($msg) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    my $project_type = param1('project_type_option');
    print hiddenVar( 'project_type_option', $project_type );

    if ( $project_type eq 'combined' && $database eq 'IMG ER' ) {
        print "<h2>Continue Analysis Project Definition<h2>\n";
    } else {
        print "<h2>Select Sample(s) to Submit</h2>\n";
    }

    my $isAdmin = getIsAdmin($contact_oid);

    my $ap_name = param1('ap_name');
    if ($ap_name) {
        print "<p>Project Name: $ap_name\n";
        print hiddenVar( 'ap_name', $ap_name );
    }
    my $ncbi_tax_id = param1('ncbi_tax_id');
    if ($ncbi_tax_id) {
        print "<p>NCBI Taxon ID: $ncbi_tax_id\n";
        print hiddenVar( 'ncbi_tax_id', $ncbi_tax_id );
    }

    for
      my $tag ( 'visibility', 'pi_name', 'pi_email', 'ap_desc', 'submitter_id' )
    {
        my $val = param($tag);
        if ($val) {
            print hiddenVar( $tag, $val );
        }
    }

    my @project_oids = param1('project_oid');
    if ( scalar(@project_oids) == 0 ) {
        print "<h4>Error: No project has been selected.</h4>\n";
        print end_form();
        return;
    } elsif ( scalar(@project_oids) > 1000 ) {
        print "<h4>Error: You have selected too many projects!</h4>\n";
        print end_form();
        return;
    }

    my $p_cnt = 0;
    for my $project_oid (@project_oids) {
        if ($project_oid) {
            $p_cnt++;
            print hiddenVar( 'project_oid', $project_oid );
        }
    }
    if ( !$p_cnt ) {
        print "<h4>Error: No project has been selected.</h4>\n";
        print end_form();
        return;
    }

    my $project_name = "";
    if ( scalar(@project_oids) == 1 ) {
        my $project_oid = $project_oids[0];
        $project_name = db_getValue(
"select display_name from project_info where project_oid = $project_oid"
        );
        print "<h3>Project $project_oid: "
          . escapeHTML($project_name)
          . "</h3>\n";
    } else {
        $project_name =
          "Combination of " . scalar(@project_oids) . " projects</h3>\n";
        print "<h3>$project_name</h3>\n";
    }
    if ( scalar(@project_oids) > 1000 ) {
        print "<p>Too many projects!\n";
        print end_form();
        return;
    }

    # proposal name
    my $dbh          = Connect_IMG();
    my $project_list = join( ", ", @project_oids );

    my @proposal_names;
    my $sql = qq{ 
        select unique (p.proposal_name)
            from project_info p
	    where p.project_oid in ( $project_list )
            and p.proposal_name is not null
        };
        
webLog("988-- $sql\n");        
        
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ($proposal_name) = $cur->fetchrow();
        last if !$proposal_name;
        push @proposal_names, ($proposal_name);
    }
    $cur->finish();
    if ( scalar(@proposal_names) > 0 ) {
        print "<p>Proposal Name: ";
        print nbsp(2);
        print "<select name='proposal_name' class='img' size='1'>\n";
        for my $s0 (@proposal_names) {
            print "    <option value='" . escapeHTML($s0) . "'";
            print ">$s0</option>\n";
        }
        print "</select>\n";
        print "<br/>\n";
    }

    ## any metagenome projects?
    my $m_project_cnt = db_getValue(
"select count(*) from project_info where project_oid in ( $project_list ) and domain = 'MICROBIAL'"
    );
    if ( !$m_project_cnt ) {
        print "<p>Only isolate genome/single cell projects are selected. "
          . "Continue to submit.\n";
        if (    $project_type eq 'combined'
             || $project_type eq 'extract' )
        {
            print
"<p><font color='red'>A new GOLD analysis project will be created after you click 'Submit' button.</font></p>\n";
        }
        print "<br/>\n";
        print
'<input type="submit" name="_section_MSubmission:signAgreement" value="Submit" class="smdefbutton" />';
        print end_form();
        return;
    }

#    print "<p>If you cannot find a sample for your submission, either create a default sample or go to " . getImgGoldLink() . " to define a new sample.</p>\n";
    print "<p>If you cannot find a sample for your submission, go to "
      . getImgGoldLink()
      . " to define a new sample.</p>\n";

    if ( !blankStr($msg) ) {
        print "<h5>$msg</h5>\n";
    }

    # show samples
    $sql = qq{ 
        select s.sample_oid, s.sample_display_name,
        s.sample_site, s.add_date, s.mod_date 
            from env_sample s 
	    where s.project_info in ( $project_list )
            order by s.sample_oid
        };

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>";
    #    }

    print "<p>\n";
    
webLog("989-- $sql\n");    
    
    $cur = $dbh->prepare($sql);
    $cur->execute();

    my $color1 = '#eeeeee';
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";
    print "<th class='img' bgcolor='$color1'>ER Sample ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Sample Display Name</th>\n";
    print "<th class='img' bgcolor='$color1'>Sample Site</th>\n";
    print "<th class='img' bgcolor='$color1'>Add Date</th>\n";
    print "<th class='img' bgcolor='$color1'>Last Mod Date</th>\n";

    my $cnt2     = 0;
    my $too_many = 0;

    for ( ; ; ) {
        my ( $sample_oid, $sample_name, $sample_site, $add_date, $mod_date ) =
          $cur->fetchrow_array();
        if ( !$sample_oid ) {
            last;
        }

        $cnt2++;
        if ( $cnt2 % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        print "<td class='img'>\n";
        print "<input type='checkbox' ";
        print "name='sample_oid' value='$sample_oid'";
        print "/></td>\n";

        my $sample_link = getSampleLink($sample_oid);
        PrintAttribute($sample_link);

        PrintAttribute($sample_name);

        PrintAttribute($sample_site);

        PrintAttribute($add_date);
        PrintAttribute($mod_date);
        print "</tr>\n";

        if ( $cnt2 >= 20000 ) {
            $too_many = 1;
            last;
        }
    }
    print "</table>\n";

    print "<p>\n";
    if ($too_many) {
        print
"<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    } else {
        print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p>\n";

    if ( $cnt2 > 0 ) {

#	print '<input type="submit" name="_section_NewSubmission:submitSample" value="Submit Sample" class="meddefbutton" />';
        if (    $project_type eq 'combined'
             || $project_type eq 'extract' )
        {
            print
"<p><font color='red'>A new GOLD analysis project will be created after you click 'Submit' button.</font></p>\n";
        }

        print
'<input type="submit" name="_section_NewSubmission:signAgreement" value="Submit Sample" class="medbutton" />';
    }

# Natalia wants to remove this option
#    print nbsp(1);
#    print '<input type="submit" name="_section_MSubmission:defaultSample" value="Create Default Sample" class="medbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowSelectSubmissionSample_yui   (use Yahoo datatable)
###########################################################################
sub ShowSelectSubmissionSample_yui {
    my ($msg) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    print "<h2>Select Sample(s) to Submit</h2>\n";

    my @project_oids = param1('project_oid');
    if ( scalar(@project_oids) == 0 ) {
        print "<h4>Error: No project has been selected.</h4>\n";
        print end_form();
        return;
    } elsif ( scalar(@project_oids) > 1000 ) {
        print "<h4>Error: You have selected too many projects!</h4>\n";
        print end_form();
        return;
    }
    for my $project_oid (@project_oids) {
        print hiddenVar( 'project_oid', $project_oid );
    }

    my $project_name = "";
    if ( scalar(@project_oids) == 1 ) {
        my $project_oid = $project_oids[0];
        $project_name = db_getValue(
"select display_name from project_info where project_oid = $project_oid"
        );
        print "<h3>Project $project_oid: "
          . escapeHTML($project_name)
          . "</h3>\n";
    } else {
        $project_name =
          "Combination of " . scalar(@project_oids) . " projects</h3>\n";
        print "<h3>$project_name</h3>\n";
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

#    print "<p>If you cannot find a sample for your submission, either create a default sample or go to " . getImgGoldLink() . " to define a new sample.</p>\n";
    print "<p>If you cannot find a sample for your submission, go to "
      . getImgGoldLink()
      . " to define a new sample.</p>\n";

    if ( !blankStr($msg) ) {
        print "<h5>$msg</h5>\n";
    }

    # show samples
    my $project_list = join( ", ", @project_oids );
    my $dbh          = Connect_IMG();
    my $sql          = qq{ 
        select s.sample_oid, s.sample_display_name,
        s.sample_site, to_char(s.add_date, 'yyyy-mm-dd'),
	to_char(s.mod_date, 'yyyy-mm-dd')
            from env_sample s 
	    where s.project_info in ( $project_list )
            order by s.sample_oid
        };

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>";
    #    }

    print "<p>\n";
    
webLog("990-- $sql\n");    
    
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $it = new InnerTable( 1, "searchSample$$", "searchSample", 0 );
    my $sd = $it->getSdDelim();    # sort delimiter
    $it->addColSpec("Select");
    $it->addColSpec( "ER Sample ID", "number asc", "right", "",
                     "ER Sample ID" );
    $it->addColSpec(
                     "Sample Display Name",
                     "char asc",
                     "left",
                     "",
                     "Sample Display Name"
    );
    $it->addColSpec( "Sample Site",   "char asc", "left", "", "Sample Site" );
    $it->addColSpec( "Add Date",      "char asc", "left", "", "Add Date" );
    $it->addColSpec( "Last Mod Date", "char asc", "left", "", "Last Mod Date" );

    my $cnt2 = 0;

    for ( ; ; ) {
        my ( $sample_oid, $sample_name, $sample_site, $add_date, $mod_date ) =
          $cur->fetchrow_array();
        if ( !$sample_oid ) {
            last;
        }

        $cnt2++;

        my $row = "";

        $row .=
            $sd
          . "<input type='radio' name='sample_oid' "
          . "value='$sample_oid'" . "/>\t";

        my $sample_link = getSampleLink($sample_oid);
        $row .= $sample_oid . $sd . $sample_link . "\t";

        $row .= $sample_name . $sd . $sample_name . "\t";
        $row .= $sample_site . $sd . $sample_site . "\t";
        $row .= $add_date . $sd . $add_date . "\t";
        $row .= $mod_date . $sd . $mod_date . "\t";

        $it->addRow($row);
    }

    if ( $cnt2 > 0 ) {
        $it->printOuterTable(1);
        print
          "<p><font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    } else {
        print "<h5>No samples found.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p>\n";

    if ( $cnt2 > 0 ) {

#	print '<input type="submit" name="_section_MSubmission:submitSample" value="Submit Sample(s)" class="meddefbutton" />';
        print
'<input type="submit" name="_section_NewSubmission:signAgreement" value="Submit Sample(s)" class="medbutton" />';
    }

# Natalia wants to remove this option
#    print nbsp(1);
#    print '<input type="submit" name="_section_MSubmission:defaultSample" value="Create Default Sample" class="medbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# ShowSelectProposalToSubmit
###########################################################################
sub ShowSelectProposalToSubmit {
    my ($msg) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    print "<h2>Create Analysis Project for Submission</h2>\n";

    print "<table class='img'>\n";
    my @project_oids = param1('project_oid');
    my $taxon_oid    = param1('taxon_oid');
    my $project_type = param1('project_type_option');
    print hiddenVar( 'project_type_option', $project_type );
    my $ap_name = param1('ap_name');
    print hiddenVar( 'ap_name', $ap_name );
    print "<tr class='img'>\n";
    print "<td class='img' bgcolor='lightblue'>Analysis Name</td>\n";
    print "<td class='img'>" . $ap_name . "</td></tr>\n";

    my $ncbi_tax_id = param1('ncbi_tax_id');
    if ($ncbi_tax_id) {
        print hiddenVar( 'ncbi_tax_id', $ncbi_tax_id );
        print "<tr class='img'>\n";
        print "<td class='img' bgcolor='lightblue'>NCBI Taxon ID</td>\n";
        print "<td class='img'>" . $ncbi_tax_id . "</td></tr>\n";
    }

    for
      my $tag ( 'visibility', 'pi_name', 'pi_email', 'submitter_id', 'ap_desc' )
    {
        my $val   = param($tag);
        my $label = $tag;
        if ( $tag eq 'visibility' ) {
            $label = 'Visibility';
        } elsif ( $tag eq 'pi_name' ) {
            $label = "PI Name";
        } elsif ( $tag eq 'pi_email' ) {
            $label = "PI Email";
        } elsif ( $tag eq 'ap_desc' ) {
            $label = "Analysis Description";
        } elsif ( $tag eq 'submitter_id' ) {
            $label = "Submitter ID";
        }

        if ($val) {
            print hiddenVar( $tag, $val );

            if ( $tag eq 'submitter_id' && $isAdmin ne 'Yes' ) {

                # skip
                next;
            }

            print "<tr class='img'>\n";
            print "<td class='img' bgcolor='lightblue'>$label</td>\n";
            print "<td class='img'>" . $val . "</td></tr>\n";
        }
    }

    my %proposal_h;

    if ( $project_type eq 'extract' ) {
        if ( !$taxon_oid || !isInt($taxon_oid) ) {
            print "</table>\n";
            print "<h4>Error: No source metagenome has been selected.</h4>\n";
            print end_form();
            return;
        }
        print hiddenVar( 'taxon_oid', $taxon_oid );

        print "<tr class='img'>\n";
        print "<td class='img' bgcolor='lightblue'>Source Metagenome</td>\n";
        print "<td class='img'>" . $taxon_oid . "</td></tr>\n";

        my $dbh = Connect_IMG_MI();
        my $sql = "select submission_id, proposal_name from taxon "
          . "where taxon_oid = $taxon_oid";
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        my ( $submission_id, $proposal_name ) = $cur->fetchrow();
        if ($proposal_name) {
            $proposal_h{$proposal_name} = 1;
        }
        $cur->finish();
        $dbh->disconnect();

        # get all samples
        my @sample_oids;
        if ($submission_id) {
            $dbh = Connect_IMG();
            $sql = qq{
                (select submission_id, sample_oid from submission
                 where submission_id = $submission_id) union
                (select submission_id, sample_oid from submission_samples
                 where submission_id = $submission_id)
            };

webLog("991-- $sql\n");            
            
            $cur = $dbh->prepare($sql);
            $cur->execute();
            for ( ; ; ) {
                my ( $submission_id, $sample_oid ) = $cur->fetchrow();
                last if !$submission_id;

                if ($sample_oid) {
                    push @sample_oids, ($sample_oid);
                    print hiddenVar( 'sample_oid', $sample_oid );
                }
            }
            $cur->finish();

            if ( scalar(@sample_oids) > 0 && scalar(@sample_oids) <= 1000 ) {
                $sql =
                    "select proposal_name from env_sample "
                  . "where sample_oid in ("
                  . join( ", ", @sample_oids )
                  . ") and proposal_name is not null";
                  
webLog("992-- $sql\n");                  
                  
                $cur = $dbh->prepare($sql);
                $cur->execute();
                for ( ; ; ) {
                    my ($proposal_name) = $cur->fetchrow();
                    last if !$proposal_name;

                    $proposal_h{$proposal_name} = 1;
                }
                $cur->finish();
            }
            $dbh->disconnect();
        }
    } elsif ( $project_type eq 'combined' ) {
        if ( scalar(@project_oids) == 0 ) {
            print "</table>\n";
            print "<h4>Error: No project has been selected.</h4>\n";
            print end_form();
            return;
        } elsif ( scalar(@project_oids) > 1000 ) {
            print "</table>\n";
            print "<h4>Error: You have selected too many projects!</h4>\n";
            print end_form();
            return;
        }

        for my $project_oid (@project_oids) {
            print hiddenVar( 'project_oid', $project_oid );
        }

        my $project_name = "";
        if ( scalar(@project_oids) == 1 ) {
            my $project_oid = $project_oids[0];
            $project_name = db_getValue(
"select display_name from project_info where project_oid = $project_oid"
            );

            print "<tr class='img'>\n";
            print "<td class='img' bgcolor='lightblue'>Project</td>\n";
            print "<td class='img'>"
              . $project_oid . ": "
              . $project_name
              . "</td></tr>\n";
        } else {
            $project_name =
              "Combination of " . scalar(@project_oids) . " projects</h3>\n";
            print "<tr class='img'>\n";
            print "<td class='img' bgcolor='lightblue'>Projects</td>\n";
            print "<td class='img'>" . $project_name . "</td></tr>\n";
        }

        # proposal name
        my $dbh          = Connect_IMG();
        my $project_list = join( ", ", @project_oids );

        my $sql = qq{ 
            select unique (p.proposal_name)
            from project_info p
	    where p.project_oid in ( $project_list )
            and p.proposal_name is not null
        };

webLog("993-- $sql\n");        
        
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        for ( ; ; ) {
            my ($proposal_name) = $cur->fetchrow();
            last if !$proposal_name;
            $proposal_h{$proposal_name} = 1;
        }
        $cur->finish();
        $dbh->disconnect();
    }
    print "</table>\n";

    my @proposal_names = ( keys %proposal_h );
    if ( scalar(@proposal_names) > 0 ) {
        print "<p>Proposal Name: ";
        print nbsp(2);
        print "<select name='proposal_name' class='img' size='1'>\n";
        for my $s0 (@proposal_names) {
            print "    <option value='" . escapeHTML($s0) . "'";
            print ">$s0</option>\n";
        }
        print "</select>\n";
        print "<br/>\n";
    } else {
        print "<h5>There is no proposal name.</h5>\n";
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;
    print hiddenVar( 'database', $database );

    print
"<p><font color='red'>A new GOLD analysis project will be created after you click 'Submit' button.</font></p>\n";
    print
'<input type="submit" name="_section_ERSubmission:signAgreement" value="Submit" class="medbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# checkMetagenomeSample
###########################################################################
sub checkMetagenomeSample {
    my $msg = "";

    # per Nikos, no checking
    return $msg;

    my $contact_oid = getContactOid();

    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    my $project_oid = param1('project_oid');
    my @samples     = param('sample_oid');

    if ( blankStr($project_oid) || scalar(@samples) == 0 ) {
        $msg = "No sample has been selected.";
        return $msg;
    }

    my $sample_oid = $samples[0];

    my $dbh = Connect_IMG();
    my $sql = qq{
        select s.sample_oid, s.sample_display_name, s.gold_id,
	s.ecosystem, s.ecosystem_category, s.ecosystem_type,
	s.ecosystem_subtype, s.specific_ecosystem,
	s.sample_isolation, s.date_collected,
        s.geo_location, s.latitude, s.longitude
            from env_sample s
	    where s.sample_oid = ?
	};

    my $select_type = "Sample";
    my $select_id   = $sample_oid;
    if ( scalar(@samples) > 1 ) {
        $select_type = "Project";
        $select_id   = $project_oid;
        $sql         = qq{
            select p.project_oid, p.display_name, p.gold_stamp_id,
	    p.ecosystem, p.ecosystem_category, p.ecosystem_type,
	    p.ecosystem_subtype, p.specific_ecosystem,
	    p.iso_source, p.add_date,
            p.geo_location, p.latitude, p.longitude
                from project_info p
	        where p.project_oid = ?
	    };
    }

webLog("994-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute($select_id);

    my (
         $samp_id,           $samp_name,          $gold_id,
         $ecosystem,         $ecosystem_category, $ecosystem_type,
         $ecosystem_subtype, $specific_ecosystem, $sample_isolation,
         $date_collected,    $geo_location,       $latitude,
         $longitude
    ) = $cur->fetchrow_array();
    $cur->finish();

    my @null_flds = ();
    if ( !$samp_id ) {
        $msg = "$select_type does not exist.";
    } else {

        # display_name
        if ( blankStr($samp_name) ) {
            push @null_flds, ("$select_type Display Name");
        }

        # ecosystem
        if ( blankStr($ecosystem) ) {
            push @null_flds, ("Ecosystem");
        }
        if ( blankStr($ecosystem_category) ) {
            push @null_flds, ("Ecosystem Category");
        }
        if ( blankStr($ecosystem_type) ) {
            push @null_flds, ("Ecosystem Type");
        }
        if ( blankStr($ecosystem_subtype) ) {
            push @null_flds, ("Ecosystem Subtype");
        }
        if ( blankStr($specific_ecosystem) ) {
            push @null_flds, ("Specific Ecosystem");
        }

        # isolation site
        if ( $select_type eq 'Sample'
             && blankStr($sample_isolation) )
        {
            push @null_flds, ("Sample Isolation");
        }

        # iso_year
        if ( $select_type eq 'Sample'
             && blankStr($date_collected) )
        {
            push @null_flds, ("Sample Collection Date");
        }

        # geographic location
        if ( blankStr($geo_location) ) {
            push @null_flds, ("Geographic Location");
        }

        # latitude
        if ( blankStr($latitude) ) {
            push @null_flds, ("Latitude");
        }

        # longitude
        if ( blankStr($longitude) ) {
            push @null_flds, ("Longitude");
        }

# seq center
#	my $cnt1 = db_getValue("select count(*) from project_info_data_links where project_oid = $project_oid and link_type = 'Seq Center'");
#	if ( ! $cnt1 ) {
#	    push @null_flds, ( "Sequencing Center" );
#	}

        # sequencing method
        if ( $select_type eq 'Sample' ) {
            my $cnt2 = db_getValue(
"select count(*) from env_sample_seq_method where sample_oid = $sample_oid"
            );
            if ( !$cnt2 ) {
                push @null_flds, ("Sequencing Method");
            }
        }

        if ( scalar(@null_flds) > 0 ) {
            for my $fld (@null_flds) {
                if ( blankStr($msg) ) {
                    $msg =
                        "You have selected a "
                      . lc($select_type)
                      . " that is not completely defined. The following information must be provided: ";
                } else {
                    $msg .= ", ";
                }
                $msg .= $fld;
            }

            $msg .=
                ". <p>Please go to "
              . getImgGoldLink()
              . " to complete the project and sample definition first.";
        }
    }

    if ( !blankStr($msg) ) {
        $dbh->disconnect();
        return $msg;
    }

    $dbh->disconnect();
    return $msg;
}

############################################################################
# showSetApproval: set approval status for a set of submissions
############################################################################
sub ShowSetApproval {
    my ($database) = @_;

    my $section = 'ERSubmission';

    print "<h3>Set Approval Status for Submissions</h3>\n";
    print "<p>Set apporval status to ";

    print "<select name='approval_status' class='img' size='1'>\n";

    print "    <option value=''></option>\n";
    for my $st1 ( 'approved', 'missing metadata', 'on hold', 'pending review' )
    {
        print "    <option value='$st1'>$st1</option>\n";
    }
    print "</select>\n";

    print "<p>For ";
    if ( $database eq 'IMG/M ER' ) {
        print "IMG/M ER submissions";
        $section = 'MSubmission';
    } elsif ( $database eq 'IMG_ER_RNASEQ' ) {
        print "Isolate RNA Seq submissions";
        $section = 'RnaSeq';
    } elsif ( $database eq 'IMG_MER_RNASEQ' ) {
        print "Metagenome RNA Seq submissions";
        $section = 'MerRnaSeq';
    } elsif ( $database eq 'IMG Methylomics' ) {
        print "Methylomics submissions";
        $section = 'Methylomics';
    } else {
        print "IMG ER submissions";
    }

    print " with ID between ";
    print "<input type='text' name='id_val1' value='' size='6' maxLength='6'/>";
    print " and ";
    print "<input type='text' name='id_val2' value='' size='6' maxLength='6'/>";

    print "<p>\n";
    print
"<input type='submit' name='_section_$section:dbUpdateApproval' value='Update Approval Status' class='medbutton' />";
}

############################################################################
# dbUpdateApproval
############################################################################
sub dbUpdateApproval {

    my $msg = "";

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        $msg = "Unknown username / password";
        return $msg;
    }

    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin ne 'Yes' ) {
        $msg = "You do not have the privilege to update permission.";
        return $msg;
    }

    my $approval_status = param1('approval_status');
    my $id_val1         = param1('id_val1');
    my $id_val2         = param1('id_val2');
    my $database        = param1('database');
    $database =~ s/%20/ /g;

    if ( blankStr($id_val1) ) {
        $msg = "Please specify a starting ID value.";
        return $msg;
    }
    if ( !isInt($id_val1) ) {
        $msg = "ID value '" . escapeHTML($id_val1) . "' is not an integer.";
        return $msg;
    }
    if ( !blankStr($id_val2) && !isInt($id_val2) ) {
        $msg = "ID value '" . escapeHTML($id_val2) . "' is not an integer.";
        return $msg;
    }

    my @sqlList = ();

    my $sql = "update submission set approval_status = ";
    if ( blankStr($approval_status) ) {
        $sql .= "null";
    } else {
        $sql .= "'" . $approval_status . "'";
    }
    $sql .= ", approved_by = $contact_oid, modified_by = $contact_oid, "
      . "mod_date = sysdate, approval_date = sysdate where ";

    if ($database) {
        $sql .= " database = '$database' and ";
    }

    if ( blankStr($id_val2) ) {
        $sql .= "submission_id = $id_val1";
    } else {
        $sql .= "submission_id between $id_val1 and $id_val2";
    }
    push @sqlList, ($sql);

    if ($msg) {
        return $msg;
    }

    # update database
    db_sqlTrans( \@sqlList );

    return "";
}

##########################################################################
# ExportProjectList
##########################################################################
sub ExportProjectList {

    my $my_c_oid = getContactOid();
    if ( !$my_c_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($my_c_oid);

    my $dbh = WebFunctions::Connect_IMG();

    my $sql = "select project_oid, db_name from project_info_data_links "
      . "where link_type = 'Seq Center'";
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $cnt = 0;
    my %seq_centers;
    for ( ; ; ) {
        my ( $project_oid, $seq_center ) = $cur->fetchrow_array();
        if ( !$project_oid ) {
            last;
        }

        $cnt++;

        if ( $seq_centers{$project_oid} ) {
            $seq_centers{$project_oid} .= ", " . $seq_center;
        } else {
            $seq_centers{$project_oid} = $seq_center;
        }
    }

    $cur->finish();

    $sql = "select p.project_oid, p.display_name, p.gold_stamp_id "
      . "from project_info p";
    if ( $isAdmin ne 'Yes' ) {
        $sql .=
            " where ( p.contact_oid = $my_c_oid"
          . " or p.gold_stamp_id is not null "
          . " or p.project_oid in (select cpp.project_permissions from"
          . " contact_project_permissions cpp where cpp.contact_oid = $my_c_oid))";
    }
    $sql .= " order by 1";

webLog("995-- $sql\n");

    $cur = $dbh->prepare($sql);
    $cur->execute();

    # print Excel Header
    my $fileName = "myimg_export$$.xls";
    print "Content-type: application/vnd.ms-excel\n";
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n";

    $cnt = 0;
    for ( ; ; ) {
        my ( $project_oid, $project_name, $gold_id ) = $cur->fetchrow_array();
        if ( !$project_oid ) {
            last;
        }

        $cnt++;

        print "$project_oid\t$project_name\t$gold_id\t"
          . $seq_centers{$project_oid} . "\n";
    }

    $cur->finish();
    $dbh->disconnect();

    exit 0;
}

#########################################################################
# ShowStatisticsInfo
#########################################################################
sub ShowStatisticsInfo {

    #generate a form
    print start_form( -method => 'post', -action => "$section_cgi" );

    print "<h1>Submission Statistics</h1>\n";

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        printError("Unknown username / password");
        return;
    }

    my $submission_id = param1('submission_id');
    if ( !$submission_id || !isInt($submission_id) ) {
        printError("No Submission ID");
        return;
    }

    # check whether user can view this project
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'No' ) {

        # contact must be the owner of this submission
        my $project_contact = db_getValue(
"select contact from submission where submission_id = $submission_id"
        );
        if ( $project_contact == $contact_oid ) {

            # fine
        } else {

            # check submission_img_contacts
            my $cnt0 = db_getValue(
"select count(*) from submission_img_contacts where submission_id = $submission_id and img_contacts = $contact_oid"
            );
            if ( $cnt0 == 0 ) {
                printError("You cannot view this submission.");
                return;
            }
        }
    }

    print "<h4>Submission ID: $submission_id</h4>\n";
    my $has_stats = PrintSubmissionStats($submission_id);
    if ( !$has_stats ) {
        print "<p>No statistics information for this submission.</p>\n";
    }

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# printSubmissionStats
###########################################################################
sub PrintSubmissionStats {
    my ($submission_id) = @_;

    my $has_stats = 0;

    ## remove Plots file per Marcel (3/2/2016)
    my @headers = (
                    "Number of seqs",
                    "Number of bps",
                    "Length shortest seq",
                    "Length longest seq",
                    "Average length",
                    "Median length",
                    "Standard deviation"
    );
##		   "Plots file");

    my @steps = (
                  "SubmittedData",          "Trimming",
                  "FixFasta",               "SizeSeparation",
                  "LowComplexityFiltering", "Dereplication",
                  "Clustering"
    );

    my @proc_headers = (    ## "Processing Step",
                         "Data type",
                         "Number of seqs",
                         "Number of bps",
                         "Length shortest seq",
                         "Length longest seq",
                         "Average length",
                         "Median length",
                         "Standard deviation"
    );
##			 "Plots file" );

    my @gene_call_headers = (    ## "Processing Step",
                              "Feature type",
                              "Prediction method",
                              "Number of predicted features",
                              "Number of seqs",
                              "Number of bps",
                              "Length shortest seq",
                              "Length longest seq",
                              "Average length",
                              "Median length",
                              "Standard deviation"
    );

 # file directories
 #    my $pictureFileDir = "/ifs/data/img_scratch/" . $submission_id . "/run/";
 #    my $pictureFileUrl = "http://" . $env->{ domain_name } . "/img_scratch/" .
 #	$submission_id . "/run/";

    ### start
    my @genesStats = getGeneCallingStats($submission_id);

    my $first = 1;
    my @all_seq_types =
      ( "Assembled", "Unassembled_454", "Unassembled_Illumina" );
    for my $seq_type (@all_seq_types) {
        $first = 1;
        for my $step (@steps) {
            my @data =
              getStepSeqStats( $submission_id, $step . "_" . $seq_type );

            if ( defined( $data[0] ) ) {
                my $step_name = $data[0];
                if ($first) {

                    # print header
                    my $seq_type_display = $seq_type;
                    $seq_type_display =~ s/\_/ /g;
                    print "<h3>$seq_type_display sequences stats</h3>";
                    print "<table class='img' border='1'>\n";
                    print "<tr class='img' >\n";
                    print
                      "  <th class='subhead' align='right' bgcolor='lightblue'>"
                      . "Processing step</th>\n";
                    for my $s1 (@headers) {
                        print
"  <th class='subhead' align='right' bgcolor='lightblue'>"
                          . $s1
                          . "</th>\n";
                    }
                    print "</tr>\n";

                    $first = 0;
                }

                # print data
                print "<tr class='img' >\n";
                print "<td class='img'>$step_name</td>\n";
                for ( my $i = 1 ; $i < ( scalar(@data) - 1 ) ; $i++ ) {
                    print "  <td class='img' align='right'>"
                      . escapeHTML( $data[$i] )
                      . "</td>\n";
                }

                # print file url
                #		print "  <td class='img' align='left'>" .
                #		    alink($pictureFileUrl . $data[-1], $data[-1], 'target').
                #		    "</td>\n";
                print "</tr>\n";
            }
        }    # end for each step

        if ( !$first ) {
            print "</table>\n";
            $has_stats = 1;
        } else {
            ## no such sequence type
            next;
        }

        # Data processed for gene calling
        my @final_fasta = ("FinalFastaStats");
        if ( $seq_type eq "Assembled" ) {
            push @final_fasta, ("assembled_final_fasta");
        } elsif ( $seq_type eq "Unassembled_454" ) {
            push @final_fasta, ("unassembled_454_final_fasta");
        } elsif ( $seq_type eq "Unassembled_Illumina" ) {
            push @final_fasta, ("unassembled_illumina_final_fasta");
        }

        $first = 1;
        for my $step (@final_fasta) {
            my @data = getStepSeqStats( $submission_id, $step );

            # Get gene stats here already
            if ( defined( $data[0] ) ) {
                my $step_name = $data[0];

                if ($first) {
                    print "<h4>Processed sequences stats</h4>";
                    print "<table class='img' border='1'>\n";
                    print "<tr class='img' >\n";
                    for my $s1 (@proc_headers) {
                        print
"  <th class='subhead' align='right' bgcolor='lightblue'>"
                          . $s1
                          . "</th>\n";
                    }
                    print "</tr>\n";
                    $first = 0;
                }

                print "<tr class='img' >\n";
##		print "<td class='img'>$step_name</td>\n";
                print "<td class='img'>$data[0]</td>\n";
                for ( my $i = 1 ; $i < ( scalar(@data) - 1 ) ; $i++ ) {
                    print "<td class='img' align='right'>$data[$i]</td>\n";
                }

                #		print "<td class='img'>" .
                #		    alink($pictureFileUrl . $data[-1], $data[-1], 'target').
                #		    "</td>\n";
                print "</tr>\n";
            }
        }

        foreach my $stats (@genesStats) {
            if (    $stats->[1] =~ /SequencesWithGenes/
                 || $stats->[1] =~ /SequencesWithoutGenes/
                 || $stats->[1] =~ /sequences\_with/ )
            {

                ## check step
                my $step1 = $stats->[0];

                if ( $seq_type eq "Assembled" ) {
                    if ( $step1 =~ /^unassembled\_/ ) {
                        ## different seq type
                        next;
                    }
                } else {

                    # unassembled
                    my $lc_seq_type = lc($seq_type);
                    if (    $step1 =~ /^$lc_seq_type/
                         || $step1 eq 'FinalFastaStats'
                         || $step1 eq 'GeneCalling' )
                    {
                        ## ok
                    } else {
                        next;
                    }
                }

##		print "<td class='img'>" . $step1 . "</td>\n";
                print "<td class='img'>" . $stats->[1] . "</td>\n";
                for ( my $i = 4 ; $i <= 10 ; $i++ ) {
                    print "<td class='img' align='right'>"
                      . $stats->[$i]
                      . "</td>\n";
                }
                print "</tr>\n";
            }
        }
        if ( !$first ) {
            print "</table>\n";
            $has_stats = 1;
        }

        # Gene calling results
        $first = 1;
        foreach my $stats (@genesStats) {
            if (    $stats->[2] =~ /SequencesWithGenes/
                 || $stats->[2] =~ /SequencesWithoutGenes/
                 || $stats->[2] =~ /sequences\_with/ )
            {
                ## skip
                next;
            }

            ## check step
            my $step1 = $stats->[0];
            if ( $seq_type eq "Assembled" ) {
                if ( $step1 =~ /^unassembled\_/ ) {
                    ## different seq type
                    next;
                }
            } else {

                # unassembled
                my $lc_seq_type = lc($seq_type);
                if ( $step1 =~ /^$lc_seq_type/ || $step1 eq 'GeneCalling' ) {
                    ## ok
                } else {
                    next;
                }
            }

            if ($first) {

                # print header
                print "<h4>Predictd genes stats</h4>\n";

                print "<table class='img' border='1'>\n";
                print "<tr class='img' >\n";
                for my $s1 (@gene_call_headers) {
                    print
                      "  <th class='subhead' align='right' bgcolor='lightblue'>"
                      . $s1
                      . "</th>\n";
                }
                print "</tr>\n";
                $first = 0;
            }

            # print data
            print "<tr class='img' >\n";
            my $cnt2 = 0;
            for my $s2 ( @{$stats} ) {
                if ( $cnt2 > 0 ) {
                    print "<td class='img' align='right'>" . $s2 . "</td>\n";
                }
                $cnt2++;
            }
            print "</tr>\n";
        }    # end for each stats

        if ( !$first ) {
            print "</table>\n";
            $has_stats = 1;
        }
    }

    return $has_stats;
}

sub getStepSeqStats {
    my ( $submission_id, $step ) = @_;

    my %new_steps;
    $new_steps{"SubmittedData"}          = "submitted_data";
    $new_steps{"FixFasta"}               = "fix_fasta";
    $new_steps{"LowComplexityFiltering"} = "low_complexity_filtering";

    my $dbh = Connect_IMG();

    my $db_step = $step;
    $db_step =~ s/'/''/g;    # replace ' with ''
    my $cond = " and step = '" . $db_step . "'";

    my ( $s1, @rest ) = split( /\_/, $step );
    my $new_step = "";
    if ( $new_steps{$s1} ) {

        # has new stats
        for my $s2 (@rest) {
            $new_step .= lc($s2) . "_";
        }
        $new_step .= lc( $new_steps{$s1} );
        $new_step =~ s/'/''/g;    # replace ' with ''

        if ( $step eq 'SubmittedData_Assembled' ) {
            $cond =
                " and step in ( '"
              . $db_step . "', '"
              . $new_step
              . "', 'assembled_structural_annotation') ";
        } else {
            $cond = " and step in ( '" . $db_step . "', '" . $new_step . "') ";
        }
    }

    my $sql = qq{
	    select step, total_seq, total_bases, shortest_seq, longest_seq,
   	       avg_seq, median_seq, standard_dev, picture_file
	    from submission_proc_stats
	    where submission_id = $submission_id
            $cond
	    };

    my $contact_oid = getContactOid();
    if ( $contact_oid == 312 ) {
##	print "<p>SQL ($step): $sql\n";
    }
webLog("996-- $sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my @array = $cur->fetchrow_array();

    $cur->finish();
    $dbh->disconnect();

    return @array;
}

sub getGeneCallingStats {
    my ( $submission_id, $exclude_total ) = @_;

    my $exclude_cond = " ";
    if ($exclude_total) {
        $exclude_cond = qq {
            and type_of_feature not like 'SequencesWith%'
            and type_of_feature not like 'sequences_with%'
	};
    }

    my $sql = qq{
	select step, type_of_feature, 
           description, no_pred_features, 
	   total_seq, total_bases, shortest_seq, longest_seq, 
	   avg_seq, median_seq, standard_dev
	    from submission_proc_stats
	    where submission_id = $submission_id
	    and step in ( 'GeneCalling', 
                          'assembled_structural_annotation',
                          'unassembled_illumina_structural_annotation' )
            $exclude_cond
	    order by type_of_feature, description
	};

    my $contact_oid = getContactOid();
    if ( $contact_oid == 312 ) {
##	print "<p>**SQL2: $sql\n";
    }

    my $dbh = Connect_IMG();
    
webLog("997-- $sql\n");    
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my @rowsArray;
    while ( my @row = $cur->fetchrow_array() ) {
        push @rowsArray, [@row];
    }

    $cur->finish();
    $dbh->disconnect();

    return @rowsArray;
}

#############################################################################
# ShowGrantAccessResult
#############################################################################
sub ShowGrantAccessResult {
    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my @submission_ids = param('submission_id');

    if ( scalar(@submission_ids) == 0 ) {
        printError("No submission ID has been selected.");
        print end_form();
        return;
    }
    for my $submission_id (@submission_ids) {
        print hiddenVar( 'submission_id', $submission_id );
    }

    my $img_user_names = param1('img_user_names');
    my @names          = split( /\,/, $img_user_names );
    if ( scalar(@names) == 0 ) {
        printError("Please enter IMG user name(s).");
        print end_form();
        return;
    }

    my $database = param1('database');
    $database =~ s/%20/ /g;

    print "<h1>Grant User Access Permission</h1>";

    my $dbh     = Connect_IMG();
    my $isAdmin = getIsAdmin($contact_oid);
    my $dbh2;
    if ( $database =~ /RNASEQ/ ) {
        if ( $isAdmin ne 'Yes' ) {
            return;
        }
        $dbh2 = Connect_IMG_ER();
    } else {
##	$dbh2 = Connect_IMG_EXT();
        $dbh2 = Connect_IMG_ER();
    }

    ## get contact_oid for all users
    my %contact_h;
    for my $name2 (@names) {
        my $name = strTrim($name2);
        my $sql2 =
            "select contact_oid from contact where lower(username) = '"
          . lc($name)
          . "' or lower(caliban_user_name) = '"
          . lc($name) . "'";
        my $cur2 = $dbh->prepare($sql2);
        $cur2->execute();
        my ($c_id3) = $cur2->fetchrow_array();
        $cur2->finish();
        if ($c_id3) {
            $contact_h{$name} = $c_id3;
        } else {
            print
              "<p><font color='red'>Incorrect IMG user name: $name.</font>\n";
        }
        print "<p>\n";
    }

    my @sqlList = ();
    my $sql     = "";

    for my $submission_id (@submission_ids) {
        ## print "<h3>Grant User Access to Submission $submission_id</h3>";
        print "<h3>Granted access to Submission $submission_id to user(s): "
          . join( ", ", @names ) . "</h3>";
        $sql = qq{
            select submission_id, img_taxon_oid, contact, analysis_project_id
                from submission where submission_id = $submission_id
	};
        if ( $database =~ /RNASEQ/ ) {

           #	    $sql = qq{
           #	    select submission_id, exp_oid, 0
           #	        from rnaseq_experiment where submission_id = $submission_id
           #	    };
            $sql = qq{
	    select submission_id, dataset_oid, 0
	        from rnaseq_dataset where submission_id = $submission_id
	    };
        }
        my $cur;
        
webLog("998-- $sql\n");        
        
        if ( $database =~ /RNASEQ/ ) {
            $cur = $dbh2->prepare($sql);
        } else {
            $cur = $dbh->prepare($sql);
        }
        $cur->execute();
        my ( $s_id2, $img_oid, $c_id2, $gold_ap_id ) = $cur->fetchrow_array();
        $cur->finish();

        if ( !$s_id2 || !$img_oid ) {
            if ( $database =~ /RNASEQ/ ) {
                printError(
                         "This submission is not associated with any dataset.");
            } else {
                ## printError("This submission is not associated with any genome.");
            }
            next;
        }

        if ( $isAdmin eq 'Yes' ) {

            # fine
        } else {

            # check submitter (FIXME)
            if ( !$c_id2 ) {
                printError("Incorrect submission ID.");
                next;
            } elsif ( $c_id2 != $contact_oid ) {
                if ($gold_ap_id) {
                    my $role =
                      WebFunctions::getGoldAPRole( $contact_oid, $gold_ap_id );
                    if (    lc($role) ne 'superuser'
                         && lc($role) ne 'pi'
                         && lc($role) ne 'co-pi'
                         && lc($role) ne 'submitter' )
                    {
                        printError(
"Only owner (submitter) can grant access of this submission." );
                        return;
                    }
                } else {
                    printError(
"Only owner (submitter) can grant access of this submission." );
                    next;
                }
            }
        }

        # get the users that already have permissions
        my %accessList;
        my $sql2 = "select contact_oid from contact_taxon_permissions "
          . "where taxon_permissions = $img_oid";
        if ( $database =~ /RNASEQ/ ) {

           #	    $sql2 = "select contact_oid from contact_rnaexp_permissions " .
           #		"where rnaexp_permissions = $img_oid";
            $sql2 = "select contact_oid from contact_rna_data_permissions "
              . "where dataset_oid = $img_oid";
        }
        
webLog("1000-- $sql2\n");        
        
        my $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();
        for ( ; ; ) {
            my ($c_id4) = $cur2->fetchrow_array();
            last if !$c_id4;
            $accessList{$c_id4} = 1;
        }
        $cur2->finish();

        for my $name (@names) {
            my $name  = strTrim($name);
            my $c_id3 = $contact_h{$name};

            if ($c_id3) {
                if ( $accessList{$c_id3} ) {
                    print "<p>User $name already has access.\n";
                    next;
                }

                my $sql3 = "insert into contact_taxon_permissions "
                  . "(contact_oid, taxon_permissions) values ($c_id3, $img_oid)";
                if ( $database =~ /RNASEQ/ ) {

              #		    $sql3 = "insert into contact_rnaexp_permissions " .
              #			"(contact_oid, rnaexp_permissions) values ($c_id3, $img_oid)";
                    $sql3 = "insert into contact_rna_data_permissions "
                      . "(contact_oid, dataset_oid) values ($c_id3, $img_oid)";
                }
                push @sqlList, ($sql3);

                print "<p>Access granted to user $name.\n";
            }
        }
    }    # end for submission_id
    $dbh->disconnect();
    $dbh2->disconnect();

    # update database
    if ( $database eq 'IMG ER' || $database eq 'IMG_ER_RNASEQ' ) {

        # connect to ER
        $dbh = Connect_IMG_ER();
    } elsif ( $database eq 'IMG/M ER' || $database eq 'IMG_MER_RNASEQ' ) {
        $dbh = Connect_IMG_MI();
    } else {
        return;
    }
    $dbh->{AutoCommit} = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for $sql (@sqlList) {
            $last_sql++;
webLog("1010-- $sql\n");
            my $cur = $dbh->prepare($sql)
              || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
            $cur->execute()
              || dienice("execSql: cannot execute: $DBI::errstr\n");
        }
    };

    if ($@) {
        $dbh->rollback();
        $dbh->disconnect();
        return $last_sql;
    }

    $dbh->commit();
    $dbh->disconnect();

    print "<p><b>Grant access completed.</b>\n";

    print end_form();

    return "";
}

#############################################################################
# ShowReleaseResult
#############################################################################
sub ShowReleaseResult {
    my ($is_public) = @_;

    print start_form(
                      -name   => 'mainForm',
                      -method => 'post',
                      action  => "$section_cgi"
    );

    if ( !$is_public ) {
        ## disable unrelease
        return;
    }

    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my @submission_ids = param('submission_id');

    if ( scalar(@submission_ids) == 0 ) {
        printError("No submission ID has been selected.");
        print end_form();
        return;
    }
    for my $submission_id (@submission_ids) {
        print hiddenVar( 'submission_id', $submission_id );
    }

    if ($is_public) {
        print "<h1>Release Genome(s)</h1>";
    } else {
        print "<h1>Make Genome(s) Private</h1>";
    }

    my $dbh     = Connect_IMG();
    my $dbh2    = Connect_IMG('imgtaxon');
    my $isAdmin = getIsAdmin($contact_oid);

    my @sqlList = ();
    my $sql     = "";

    for my $submission_id (@submission_ids) {
        if ( !isInt($submission_id) ) {
            next;
        }

        if ($is_public) {
            print "<h3>Release Submission $submission_id</h3>";
        } else {
            print "<h3>Unrelease Submission $submission_id</h3>";
        }

        $sql = qq{
	    select submission_id, img_taxon_oid, contact, analysis_project_id
	        from submission where submission_id = $submission_id
	    };
	    
webLog("1020-- $sql\n");	    
	    
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        my ( $s_id2, $img_oid, $c_id2, $gold_ap_id ) = $cur->fetchrow_array();
        $cur->finish();

        if ( !$s_id2 ) {
            printError("Incorrect submission ID.");
            next;
        }

        if ( !$img_oid ) {
            printError("This submission is not associated with any IMG OID.");
            next;
        }

        if ( $isAdmin eq 'Yes' ) {

            # fine
        } else {

            # check submitter
            if ( !$c_id2 ) {
                printError("Incorrect submission ID.");
                next;
            } elsif ( $c_id2 != $contact_oid ) {
                if ($gold_ap_id) {
                    my $role =
                      WebFunctions::getGoldAPRole( $contact_oid, $gold_ap_id );
                    if (    lc($role) ne 'superuser'
                         && lc($role) ne 'pi'
                         && lc($role) ne 'co-pi'
                         && lc($role) ne 'submitter' )
                    {
                        printError(
"Only owner (submitter) can grant access of this submission." );
                        return;
                    }
                } else {
                    printError(
"Only PI, co-PI and submitter can release this submission." );
                    next;
                }
            }
        }

        # check public/private
        my $sql2 =
"select is_public, release_date from taxon where taxon_oid = $img_oid";

webLog("1022-- $sql2\n");

        my $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();
        my ( $taxon_is_public, $release_date ) = $cur2->fetchrow_array();
        $cur2->finish();

        my $comments = param1('release_note');
        if ( blankStr($comments) ) {
            if ( !$contact_oid ) {
                $contact_oid = 0;
            }
            my $sql2 =
              "select name from contact where contact_oid = $contact_oid";

webLog("1023-- $sql2\n");
              
              
            my $cur2 = $dbh->prepare($sql2);
            $cur2->execute();
            my ($c_name) = $cur2->fetchrow_array();
            $cur2->finish();

            if ($is_public) {
                $comments = "released by " . $c_name;
            } else {
                $comments = "reverted to private by " . $c_name;
            }
        }
        $comments =~ s/'/''/g;    # replaced ' by ''

        if ($is_public) {
            if ( $taxon_is_public eq 'Yes' ) {
                print "<p>Genome $img_oid has already been released on"
                  . $release_date . ".\n";
                next;
            }

            my $sql3 =
"update taxon set is_public = 'Yes', release_date = sysdate, mod_date = sysdate, modified_by = $contact_oid, comments = '"
              . $comments
              . "'  where taxon_oid = $img_oid";
            push @sqlList, ($sql3);

            print "<p>Genome is released.\n";
        } else {

            # private
            if ( $taxon_is_public eq 'No' ) {
                print "<p>Genome $img_oid is already private.\n";
                next;
            }

            my $sql3 =
"update taxon set is_public = 'No', release_date = null, mod_date = sysdate, modified_by = $contact_oid, comments = '"
              . $comments, "'  where taxon_oid = $img_oid";
            push @sqlList, ($sql3);

            print "<p>Genome is unreleased.\n";
        }
    }    # end for submission_id
    $dbh->disconnect();
    $dbh2->disconnect();

    # update database
    $dbh2 = Connect_IMG('imgtaxon');
    $dbh2->{AutoCommit} = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for $sql (@sqlList) {
            $last_sql++;

webLog("1030-- $sql\n");


            my $cur = $dbh2->prepare($sql)
              || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
            $cur->execute()
              || dienice("execSql: cannot execute: $DBI::errstr\n");
        }
    };

    if ($@) {
        $dbh2->rollback();
        $dbh2->disconnect();
        return $last_sql;
    }

    $dbh2->commit();
    $dbh2->disconnect();

    if ($is_public) {
        print "<p><b>Release is completed.</b>\n";
    } else {
        print "<p><b>Unrelease is completed.</b>\n";
    }

    print end_form();

    return "";
}

sub getReferenceTaxonOid {
    my ($analysis_project_id) = @_;

    my $taxon_oid = "";

    if ( !$analysis_project_id ) {
        return "";
    }

    my $res = getGoldAnalysisProject($analysis_project_id);
    if ($res) {
        my $decode = decode_json($res);
        if ($decode) {
            $taxon_oid = $decode->{'imgTaxonOid'};
            if ($taxon_oid) {
                return $taxon_oid;
            }

            my $ref_ap_id = $decode->{'referenceGoldId'};
            if ($ref_ap_id) {
                my $res2 = getGoldAnalysisProject($ref_ap_id);
                if ($res2) {
                    my $decode2 = decode_json($res2);
                    if ($decode2) {
                        $taxon_oid = $decode2->{'imgTaxonOid'};
                    }
                }
            }
        }
    }

    return $taxon_oid;
}

########################################################################
# loadSubmissionFilter
########################################################################
sub loadSubmissionFilter {

    # set default filter condition
    my $contact_oid = checkAccess();
    if ( !$contact_oid ) {
        return;
    }

    # filter out cancelled submission (per Nikos' request)
    my $status_str = db_getValuesToString(
"select term_oid from submission_statuscv where term_oid != 90 order by 1"
    );

# setSessionParam('sub_filter:status', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18');
    setSessionParam( 'sub_filter:status', $status_str );

    setSessionParam( 'sub_filter:comp_submission_date', '>=' );
    setSessionParam( 'sub_filter:val_submission_date',  '01-JAN-22' );

    # set saved filter condition
    if ( $workspace_dir && $contact_oid ) {
        my $file_name =
          $workspace_dir . "/" . $contact_oid . "/" . "mySubmissionFilter";
        if ( -e $file_name ) {
            my $fh = newReadFileHandle( $file_name, "mySubmissionFilter" );
            if ($fh) {
                while ( my $cond1 = $fh->getline() ) {
                    chomp($cond1);
                    my ( $tag, $val ) = split( /\t/, $cond1, 2 );
                    setSessionParam( $tag, $val );
                }
                close($fh);
            }
        }
    }
}

sub printKbaseAssemblyMsg() {
    my $kbase_link =
      alink( "https://narrative.kbase.us/narrative/ws.36189.obj.1",
             "KBase Assembly Service" );
    my $new_msg = qq{
        IMG only accepts assembled fasta submissions.
        Users who have unassembled data can use $kbase_link
        before submitting to IMG.
        <br/>
    };

    printHint3($new_msg);
}

1;

