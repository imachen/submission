package GoldMetaProj;

use strict; 
#use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
use lib 'lib'; 
use WebEnv;
use WebFunctions; 
use RelSchema;
use ProjectInfo;
use TabHTML;

my $env = getEnv(); 
my $base_url = $env->{ base_url }; 


my $default_max_row = 50;

my $is_test = 0;


#########################################################################
# dispatch 
#########################################################################
sub dispatch { 
    my ($page) = @_; 
 
    if ( $page eq 'newProject' ) {
	NewGoldMetaProject();
    } 
    elsif ( $page eq 'dbNewGoldProject' ) {
        my $msg = dbNewGoldMetaProject(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else { 
	    my $web_code = 4;
            ShowNextPage($web_code);
        }
    } 
    elsif ( $page eq 'updateProject' ) {
        my $project_oid = param1('project_oid');
 
        if ( blankStr($project_oid) ) {
            printError("No project has been selected."); 
            print end_form(); 
            return; 
        } 
 
        UpdateGoldMetaProject($project_oid); 
    } 
    elsif ( $page eq 'dbUpdateGoldProject' ) { 
        my $msg = dbUpdateGoldMetaProject(); 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
	}
    } 
    elsif ( $page eq 'deleteProject' ) { 
        DeleteGoldProject(); 
    } 
    elsif ( $page eq 'dbDeleteGoldProject' ) {
        my $msg = ProjectInfo::dbDeleteProject(); 
 
        if ( ! blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
          } 
        else { 
	    my $web_code = 4;
            ShowNextPage($web_code);
        } 
    } 
    elsif ( $page eq 'copyProject' ) {
        my $project_oid = param1('project_oid');
        CopyGoldProject($project_oid); 
    } 
    elsif ( $page eq 'showPage' ) {
	ShowPage();
    }
    elsif ( $page eq 'displayProject' ) {
        my $project_oid = param1('project_oid');
        ProjectInfo::DisplayProject($project_oid); 
    }
#    elsif ( $page eq 'displaySample' ) {
#        my $sample_oid = param1('sample_oid');
#        EnvSample::DisplaySample($sample_oid);
#    }
    elsif ( $page eq 'showCategory' ) {
	my $web_code = 4;
	if ( defined param1('web_page_code') ) {
	    $web_code = param1('web_page_code');
	}

	ShowNextPage($web_code);
    }
    elsif ( $page eq 'editSample' ) {
        my $project_oid = param1('project_oid');
 
        if ( blankStr($project_oid) ) {
            printError("No project has been selected."); 
            print end_form(); 
            return; 
        } 

#	EnvSample::ShowProjectSamples($project_oid, 'GoldMetaProj');
	EnvSample::ShowProjectSamples($project_oid);
    }
    elsif ( $page eq 'searchId' ) {
	SearchByID();
    }
    elsif ( $page eq 'searchIdResult' ) {
	ShowSearchIdResultPage();
    }
    elsif ( $page eq 'advSearch' ) {
	AdvancedSearch();
    }
    elsif ( $page eq 'advSearchResult' ) {
	ShowAdvSearchResultPage();
    }
    elsif ( $page eq 'filterProject' ) {
	FilterGoldMetaProject();
    }
    elsif ( $page eq 'applyGoldProjectFilter' ) {
        ApplyGoldMetaProjectFilter(); 

	ShowPage();
    } 
    elsif ( $page eq 'changeContact' ) {
        my $msg = ProjectInfo::dbChangeProjectContact();
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'grantEdit' ) {
        ProjectInfo::GrantEditPrivilege();
      }
    elsif ( $page eq 'dbGrantEditPrivilege' ) {
        my $msg = ProjectInfo::dbGrantEditPrivilege();
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'setGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        ProjectInfo::SetGoldStampId($project_oid); 
    } 
    elsif ( $page eq 'dbSetGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        my $gold_type = param1('gold_type'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = ProjectInfo::dbSetGoldStampId($project_oid, $gold_type,
						$gold_stamp_id); 
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
        } 
    } 
    elsif ( $page eq 'delGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        ProjectInfo::DeleteGoldStampId($project_oid); 
    } 
    elsif ( $page eq 'dbDelGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        my $del_gold_id_option = param1('del_gold_id_option'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = ProjectInfo::dbDelGoldStampId($project_oid,
						$del_gold_id_option, 
						$gold_stamp_id);
 
        if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'mergeGoldProj' ) {
	PrintMergeGoldProjPage();
    }
    elsif ( $page eq 'mergeProjects' ) { 
        my $project_oid = param1('project_oid'); 
        if ( blankStr($project_oid) ) { 
            printError("No project has been selected."); 
            print end_form(); 
            return; 
        } 
 
        my $merged_project = param1('merged_project'); 
        if ( blankStr($merged_project) ) { 
            printError("Please select a project to merge."); 
            print end_form(); 
            return; 
        } 
 
        if ( $project_oid == $merged_project ) { 
            printError("The two projects are the same. Please select a different project to merge."); 
            print end_form(); 
            return; 
        } 
 
        ProjectInfo::MergeProjects($project_oid, $merged_project);
    } 
#    elsif ( $page eq 'dbMergeProjects' ) {
#        my $msg = ProjectInfo::dbMergeProjects(); 
# 
#        if ( ! blankStr($msg) ) { 
#            WebFunctions::showErrorPage($msg);
#          } 
#        else { 
#            ShowNextPage();
#        } 
#    } 
    else {
	ShowPage();
    }
}


#########################################################################
# ShowNextPage
##########################################################################
sub ShowNextPage {
    my ($new_code) = @_;

    my $page = param1('page');

    my $web_code2 = param1('web_code');
 
    if ( $web_code2 && $web_code2 > 4 ) {
        ShowCategoryPage($web_code2);
    } 
    elsif ( $page eq 'showCategory' && length($new_code) > 0 ) {
	ShowCategoryPage($new_code);
    }
    elsif ( $page eq 'showCategory' && defined param1('web_code') ) {
	my $web_code = param1('web_code');
	if ( length($web_code) > 0 ) {
	    ShowCategoryPage($web_code);
	}
	else {
	    ShowPage();
	}
    }
    else {
	ShowPage();
    }

#    if ( $next_page eq 'searchIdResult' ) {
#	ShowPage();
#    }
#    else {
#	ShowCategoryPage();    
#    }
}


#########################################################################
# ShowPage
##########################################################################
sub ShowPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 

    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h2>IMG-GOLD Metagenome Studies (All)</h2>\n";

#    if ( $contact_oid == 312 ) {
#	my @pageParams = ( 'browse:goldproj_page_no', 
#			   'browse:goldproj_orderby',
#			   'browse:project_desc' );
# 
#	for my $p1 ( @pageParams ) {
#	    if ( getSessionParam($p1) ) {
#		print "<p>$p1: " . getSessionParam($p1) . "</p>\n";
#	    } 
#	} 
#    }

    my $cnt = 0; 
    my $select_cnt = 0; 

    print "<h4>Only those studies you have permission to update are selectable.</h4>\n";

    $cnt = allGoldMetaProjectCount($contact_oid);
    $select_cnt = filteredGoldMetaProjectCount($contact_oid);

    print "<p>Selected Project Count: $select_cnt (Total: $cnt)";
    if ( $select_cnt != $cnt ) {
	print nbsp(3);
	print "<font color='red'>(Filter is on. Not all projects are displayed.)</font></p>\n";
    }
    else {
	print "</p>\n";
    }

    # save orderby param
    my $orderby = param1('goldproj_orderby');
    if ( blankStr($orderby) && getSessionParam('browse:goldproj_orderby') ) {
	$orderby = getSessionParam('browse:goldproj_orderby');
    }
    my $desc = param1('project_desc');
    if ( blankStr($desc) && getSession('browse:project_desc') ) {
	$desc = getSessionParam('browse:project_desc');
    }
 
    # max display 
    my $max_display = getSessionParam('meta_proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    # display page numbers 
    my $curr_page = param1('goldproj_page_no'); 
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
	if ( getSessionParam('browse:goldproj_page_no') ) {
	    $curr_page = getSessionParam('browse:goldproj_page_no');
	}

	if ( blankStr($curr_page) || $curr_page <= 0 ) {
	    $curr_page = 1; 
	}
    } 

    # save browse history
    setSessionParam('browse:goldproj_page_no', $curr_page);
    setSessionParam('browse:goldproj_orderby', $orderby);
    setSessionParam('browse:project_desc', $desc);

    my $i = 0; 
    my $page = 1; 
    print "<p>\n"; 
    while ( $i < $select_cnt ) { 
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>$page</b>"; 
        } 
        my $link = "<a href='" . url() . 
            "?section=GoldMetaProj&page=showPage" . 
            "&goldproj_page_no=$page"; 
        if ( ! blankStr($orderby) ) { 
            $link .= "&goldproj_orderby=$orderby"; 
            if ( ! blankStr($desc) ) { 
                $link .= "&project_desc=$desc"; 
            } 
        } 
        $link .= "' >" . $s . "</a>";
        print $link . nbsp(1);
        $i += $max_display; 
        $page++; 
        if ( $page > 1000 ) { 
            last;
        }
    } 
 
    printGoldMetaProjButtons(1);

    if ( $select_cnt > 0 ) {
	listGoldMetaProjects($contact_oid, $curr_page, '', $select_cnt); 

	printGoldMetaProjButtons(1);
    }

    if ( $isAdmin eq 'Yes' ) {
	print hr;

	print "<h2>Search GOLD Project</h2>\n";
	print '<input type="submit" name="_section_GoldMetaProj:searchId" value="Search By GOLD ID" class="medbutton" />'; 
#    print "&nbsp; \n"; 
#    print '<input type="submit" name="_section_GoldProj:advSearch" value="Advanced Search" class="medbutton" />'; 

	printAdditionalProjSection();
    }

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


#########################################################################
# ShowGoldProjects
##########################################################################
sub ShowGoldProjects {
    my %web_page_code = getGoldWebPageCode();

    my $dbh = Connect_IMG();

    my $sql = qq{
	select web_page_code, count(*) from project_info
	    group by web_page_code
	    order by web_page_code
	};

webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $row_cnt = 0;

    print "<p>\n"; 
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Category</th>\n";
    print "<th class='img'>Count</th>\n";

    for (;;) { 
        my ( $code, $count ) = $cur->fetchrow( );
	if ( ! (defined $code) || length($code) == 0 ) {
	    last;
	}

	$row_cnt++;
	if ( $row_cnt > 10 ) {
	    last;      # just in case
	}

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
	if ( $web_page_code{$code+1} ) {
	    print escapeHTML($web_page_code{$code+1});
	}
	else {
	    print escapeHTML($code);
	}
        print "</td>\n"; 

        print "<td class='img'>\n";
        my $link = "<a href='" . url() .
            "?section=GoldMetaProj&page=showCategory" .
	    "&web_code=$code" .
            "&category_page_no=1";
        $link .= "' >" . $count . "</a>";
	print $link;
        print "</td>\n"; 

	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();
}


######################################################################### 
# allGoldMetaProjectCount - count the number of metagenome projects 
########################################################################## 
sub allGoldMetaProjectCount { 
    my ($contact_oid) = @_; 
 
    my $isAdmin = 'No';
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid);
    } 

    my $dbh=WebFunctions::Connect_IMG; 
 
    my $sql = "select count(*) from project_info p" .
	" where p.domain = 'MICROBIAL'"; 

#    if ( $contact_oid ) { 
#	if ( getCanEditImgGold($contact_oid) ) {
#	    $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid)"; 
#	}
#	else {
#	    $sql .= " and p.contact_oid = $contact_oid"; 
#	}
#    } 

    if ( $isAdmin eq 'Yes' ) {
        # super user can view all projects
    }
    else { 
        # only GOLD projects or user's own projects
        $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
    }
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql); 
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish(); 
    $dbh->disconnect();
 
    if ( ! $cnt ) {
        return 0; 
    } 
    return $cnt; 
} 

 
######################################################################### 
# filteredGoldMetaProjectCount - count the number of filtered 
#                                metagenome projects
########################################################################## 
sub filteredGoldMetaProjectCount {
    my ($contact_oid) = @_; 

    my $isAdmin = 'No'; 
    if ( $contact_oid ) {
        $isAdmin = getIsAdmin($contact_oid);
    }
 
    my $dbh=WebFunctions::Connect_IMG;
 
    my $filter_cond = ProjectInfo::projectFilterCondition('meta_');
    my $sql = "select count(*) from project_info p" .
	" where p.domain = 'MICROBIAL'";

    my $only_my_project = getSessionParam('meta_proj_filter:only_my_project'); 
    if ( $only_my_project ) { 
	# there is already a condition on project owner
    }
    elsif ( $isAdmin eq 'Yes' ) {
        # super user can view all projects
    } 
    else {
        # only GOLD projects or user's own projects
        $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
    } 
 
    # filter condition 
    if ( ! blankStr($filter_cond) ) {
        $sql .= " and " . $filter_cond;
    } 
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 


#########################################################################
# getGoldWebPageCode
#
# (Note: need to use term_oid+1 because code starts with 0)
##########################################################################
sub getGoldWebPageCode {
    my %h;

    my $dbh = Connect_IMG();
    my $sql = "select term_oid+1, description from web_page_codecv";
    webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    for (;;) { 
        my ( $id, $val ) = $cur->fetchrow( );
	last if !$id;

	$h{$id} = $val;
    }
    $cur->finish();
    $dbh->disconnect();

    return %h;
}

#########################################################################
# ShowCategoryPage
##########################################################################
sub ShowCategoryPage {
    my ($new_code) = @_;

    my $code = $new_code;
    if ( length($code) == 0 ) {
	$code = param1('web_code');
	if ( length($code) == 0 ) {
	    ShowPage();
	    return;
	}
    }

    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    if ( $code > 4 ) {
	print "<h2>My Metagenome Studies</h2>\n";
    }
    else {
	my $category = db_getValue("select description from web_page_codecv where term_oid=$code");
	print "<h2>IMG-GOLD $category Projects (GOLD Projects Only)</h2>\n";

	print "<h4>Only those studies you have permission to update are selectable.</h4>\n";
    }

    if ( param1('page') ) {
	print hiddenVar('page', param1('page'));
    }
    else {
	print hiddenCar('page', 'showCategory');
    }

    my $cat_sql = "";

    if ( $code > 4 ) {
	$cat_sql = qq{
	    select count(*) from project_info p
		where p.domain = 'MICROBIAL'
		and (p.contact_oid = $contact_oid
		     or p.project_oid in (select cpp.project_permissions
					  from contact_project_permissions cpp
					  where cpp.contact_oid = $contact_oid))
	    };
    }
    else {
	$cat_sql = "select count(*) from project_info where web_page_code = $code";
	if ( $isAdmin ne 'Yes' ) {
	    $cat_sql .= " and gold_stamp_id is not null";
	}
    }

    my $category_count = db_getValue($cat_sql);
    print "<p><font color='blue'>(Count: $category_count)</font></p>\n";

    my $curr_page = param1('category_page_no');
    my $orderby = param1('goldproj_orderby'); 
    my $desc = param1('project_desc'); 
    my $max_display = getSessionParam('meta_proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 

    # save parameters
    print hiddenVar('web_code', $code);
    print hiddenVar('category_page_no', $curr_page);
    if ( ! blankStr($orderby) ) {
	print hiddenVar('goldproj_orderby', $orderby);
    }
    if ( ! blankStr($desc) ) {
	print hiddenVar('project_desc', $desc);
    }

    my $i = 0; 
    my $page = 1; 
#    print "<br/>\n"; 
    while ( $i < $category_count ) { 
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>$page</b>"; 
        } 
        my $link = "<a href='" . url() . 
            "?section=GoldMetaProj&page=showCategory" . 
	    "&web_code=$code" .
            "&category_page_no=$page";

        if ( ! blankStr($orderby) ) { 
            $link .= "&goldproj_orderby=$orderby"; 
            if ( ! blankStr($desc) ) { 
                $link .= "&project_desc=$desc"; 
            } 
        } 

        $link .= "' >" . $s . "</a>"; 
        print $link . nbsp(1); 
        $i += $max_display; 
        $page++; 
        if ( $page > 1000 ) {
	    print " ...\n";
            last; 
        } 
    } 
 
    printGoldMetaProjButtons(0);

    listGoldMetaProjects($contact_oid, $curr_page, $code, $category_count);

    printGoldMetaProjButtons(0);

    printAdditionalProjSection();

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


#########################################################################
# listMetaGoldProjects - list metagenome projects 
# 
# admin does not have condition on contact_oid
##########################################################################
sub listGoldMetaProjects {
    my ($contact_oid, $curr_page, $code, $cnt) = @_; 

#    my $cond = "where p.web_page_code = 0";
    my $cond = "where p.domain = 'MICROBIAL'";

    if ( length($code) == 0 ) {
	my $filter_cond = ProjectInfo::projectFilterCondition('meta_');
	if ( ! blankStr($filter_cond) ) {
	    $cond .= " and " . $filter_cond;
	}
    }
    elsif ( $code > 4 ) {
	$cond .= " and (p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
    }
    else {
	$cond = "where p.web_page_code = $code";
    }

    my $isAdmin = 'No';
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid);
    }
    my $can_edit = getCanEditImgGold($contact_oid);
 
    my $only_my_project = getSessionParam('meta_proj_filter:only_my_project'); 
    if ( $only_my_project || $code > 4 ) {
	# there is already a condition on project owner
    }
    elsif ( $isAdmin eq 'Yes' ) { 
        # all projects
    } 
    else {
	my $perm_cond = "(p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
        if ( blankStr($cond) ) { 
            $cond = "where " . $perm_cond;
        } 
        else { 
            $cond .= " and " . $perm_cond;
        } 
    } 

    my $selected_proj = param1('project_oid');
 
    # max display 
    my $max_display = getSessionParam('meta_proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    my $dbh=WebFunctions::Connect_IMG;
    my $orderby = param1('goldproj_orderby');
    my $desc = param1('project_desc');
    if ( blankStr($orderby) ) {
        $orderby = "p.project_oid $desc";
    }
    else {
        if ( $orderby eq 'project_oid' ) {
            $orderby = "p.project_oid $desc"; 
        } 
        else {
            $orderby = "p." . $orderby . " $desc, p.project_oid";
        } 
    } 
 
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.ncbi_project_id,
	p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
            $cond
            order by $orderby
        }; 
 
    if ( $contact_oid == 312 ) {
        print "<p>SQL: $sql</p>";
    } 
    webLog("$sql\n"); 
#    print "$sql<BR>";
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
#    print "<h5>Click the column name to have the data order by the selected column. Click (Rev) to order by the same column in reverse order.</h5>\n";
    print "<p>\n";
    my $color1 = "#eeeeee";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Row No.</th>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";

    my %names_h = ( 
        'project_oid' => 'ER Study ID',
        'display_name' => 'Study Display Name', 
        'gold_stamp_id' => 'Study GOLD ID',
        'phylogeny' => 'Category',
        'ncbi_project_id' => 'NCBI Project ID',
        'add_date' => 'Add Date',
        'mod_date' => 'Last Mod Date' );
 
    my $orderby_attr = param1('goldproj_orderby'); 

    for my $attr1 ( 'project_oid', 'display_name', 'gold_stamp_id',
		    'phylogeny', 'ncbi_project_id' ) { 
        my $sort_flag = 0; 
        if ( $attr1 eq $orderby_attr ) { 
            if ( $desc eq 'desc' ) {
                $sort_flag = -1; 
            }
            else {
                $sort_flag = 1; 
            } 
        } 
 
        print "<th class='img' bgcolor='$color1'>" .
            getGoldProjOrderByLink($attr1, $names_h{$attr1}, $code, $cnt, $sort_flag) .
            "</th>\n"; 
    } 
 
    print "<th class='img' bgcolor='$color1'>Contact Name</th>\n";
    print "<th class='img' bgcolor='$color1'>IMG Contact</th>\n";

    for my $attr1 ( 'add_date', 'mod_date' ) { 
        my $sort_flag = 0; 
        if ( $attr1 eq $orderby_attr ) { 
            if ( $desc eq 'desc' ) { 
                $sort_flag = -1; 
            } 
            else { 
                $sort_flag = 1; 
            } 
        } 
 
        print "<th class='img' bgcolor='$color1'>" .
            getGoldProjOrderByLink($attr1, $names_h{$attr1}, $code, $cnt, $sort_flag) .
            "</th>\n";
    } 
 
    print "<th class='img' bgcolor='$color1'>Sample Count</th>\n";
    print "<th class='img' bgcolor='$color1'>Last Sample Mod Date</th>\n";

    my $cnt2 = 0; 
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);
    my $cpp_sql = "select count(*) from contact_project_permissions where contact_oid = $contact_oid and project_permissions = ";

    my %sample_mod_date_h;
    my %sample_count_h;

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $ncbi_project_id, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 <= $skip ) {
            next; 
        } 
 
        $disp_cnt++; 
        if ( $disp_cnt > $max_display ) {
            last;
        } 
 
        if ( $disp_cnt % 2 ) { 
            print "<tr class='img'>\n"; 
        } 
        else { 
            print "<tr class='img' bgcolor='#ddeeee'>\n"; 
        } 

	# row no.
	print "<td class='img'>$cnt2</td>\n";

        print "<td class='img'>\n";

        # check edit permission
        if ( $c_oid == $contact_oid || $can_edit ||
	     db_getValue($cpp_sql . $proj_id) ) {
	    print "<input type='radio' "; 
	    print "name='project_oid' value='$proj_id'";
	    if ( $proj_id == $selected_proj ) {
		print " checked ";
	    }
	    print "/>";
	}
        print "</td>\n"; 
 
        my $proj_link = getMProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        if ( $ncbi_project_id ) {
            PrintAttribute(getNcbiProjLink($ncbi_project_id));
        }
        else {
            PrintAttribute($ncbi_project_id);
        }

        PrintAttribute($c_name); 

        if ( $contact_list{$c_oid} ) { 
            my $username = $contact_list{$c_oid}; 
            if ( $isAdmin eq 'Yes' ) { 
                my $link = "<a href='" . url() . 
                    "?section=UserTool&page=showContactInfo" . 
                    "&selected_contact_oid=$c_oid' >"; 
                PrintAttribute($link . $username . "</a>"); 
            } 
            else { 
                PrintAttribute($username); 
            } 
        } 
        else { 
            my $contact_str = db_getContactName($c_oid); 
            $contact_list{$c_oid} = $contact_str; 
            if ( $isAdmin eq 'Yes' ) { 
                my $link = "<a href='" . url() . 
                    "?section=UserTool&page=showContactInfo" . 
                    "&selected_contact_oid=$c_oid' >"; 
                PrintAttribute($link . $contact_str . "</a>"); 
            } 
            else { 
                PrintAttribute($contact_str); 
            } 
        } 
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);

	my $sample_md = $sample_mod_date_h{$proj_id};
	my $sample_cnt = $sample_count_h{$proj_id};
	if ( ! $sample_md ) {
	    if ( ! $proj_id ) {
		$proj_id = 0;
	    }
	    my $sql2 = "select max(add_date), max(mod_date), count(*) from env_sample where project_info = $proj_id";
	    my $cur2=$dbh->prepare($sql2);
	    $cur2->execute(); 
	    my ($d1, $d2, $c1) = $cur2->fetchrow();
	    $cur2->finish();
	    if ( $d2 ) {
		$sample_md = $d2;
	    }
	    elsif ( $d1 ) {
		$sample_md = $d1;
	    }
	    else {
		$sample_md = "-";
	    }
	    $sample_mod_date_h{$proj_id} = $sample_md;
	    $sample_cnt = $c1;
	    $sample_count_h{$proj_id} = $c1;
	}
        PrintAttribute($sample_cnt);
        PrintAttribute($sample_md);

        print "</tr>\n";
    }
    print "</table>\n"; 
 
    $cur->finish(); 
    $dbh->disconnect(); 

    return $cnt2;
}


##########################################################################
# getGoldProjOrderByLink
##########################################################################
sub getGoldProjOrderByLink { 
    my ($attr, $label, $code, $category_count, $sort) = @_; 
 
    my $sort_icon = $base_url . "/images/sort_icon.png"; 
    my $up_icon = $base_url . "/images/up_icon.png"; 
    my $down_icon = $base_url . "/images/down_icon.png"; 
 
    my $sort_title = "click to sort in ascending order"; 
 
    my $link = escapeHTML($label) . 
        "<a href='" . url() . "?section=GoldMetaProj"; 
 
    if ( length($code) > 0 ) { 
        $link .= "&page=showCategory&web_code=$code"; 
    } 
    else { 
        $link .= "&page=showPage"; 
    } 
 
    $link .= "&category_page_no=1&goldproj_orderby=$attr"; 
    my $icon2 = $sort_icon; 
 
    if ( $sort < 0 ) { 
        $link .= "&project_desc=asc"; 
        $icon2 = $up_icon; 
    } 
    elsif ( $sort > 0 ) { 
        $link .= "&project_desc=desc"; 
        $icon2 = $down_icon; 
        $sort_title = "click to sort in descending order"; 
    } 
    else { 
        $link .= "&project_desc=asc"; 
    } 
 
    $link .= "&goldproj_page_no=1' >" . 
        "<img src='" . $icon2 . 
        "' width='11' height='11' border='0'" . 
        " alt='$sort_title' title='$sort_title' />" . "</a>"; 
 
    return $link; 
}


######################################################################### 
# printGoldMetaProjButtons - print New, Update, Delete and Copy buttons 
######################################################################### 
sub printGoldMetaProjButtons {
    my ($with_filter) = @_;

    print "<br/>\n";

    # New, Update, Delete and Copy buttons 
    print '<input type="submit" name="_section_GoldMetaProj:newProject" value="New" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldMetaProj:updateProject" value="Update" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldMetaProj:deleteProject" value="Delete" class="smbutton" />'; 
 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldMetaProj:copyProject" value="Copy" class="smbutton" />'; 
    
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldMetaProj:editSample" value="Edit Samples" class="smbutton" />'; 

    if ( $with_filter ) {
	# project selection filter 
	print "&nbsp; \n"; 
	print '<input type="submit" name="_section_GoldMetaProj:filterProject" value="Filter Studies" class="smbutton" />';
    }
}


######################################################################### 
# NewGoldMetaProject 
######################################################################### 
sub NewGoldMetaProject { 
 
    my $url=url(); 
    print start_form(-name=>'newMetaProject',-method=>'post',action=>"$url"); 

    print hiddenVar('page_name', 'newMetaProject');
    my %db_val; 
    if ( defined param1('web_code') ) {
        $db_val{'web_page_code'} = param1('web_code');
    }

    saveProjectPageInfo();

    # tab view 
    TabHTML::printTabAPILinks("newMetaProject"); 
# No HMP metagenome projects yet
#    my @tabIndex = ( "#tab1", "#tab2", "#tab3"); 
    my @tabIndex = ( "#tab1", "#tab2"); 

    my @tabNames = 
        ( "Metagenome Info", "Study Info (*)");
# No HMP metagenome projects yet
#	  "HMP Metadata" ); 

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin ) {
        push @tabIndex, ( "#tab3" );
        push @tabNames, ( "JGI Info" );
    } 

    TabHTML::printTabDiv( "newMetaProject", \@tabIndex, \@tabNames ); 

    # tab 1 
    print "<div id='tab1'><p>\n"; 
    printMetaProjectTab("Metagenome", "", \%db_val); 
    print "</p></div>\n";

    # tab 2 
    print "<div id='tab2'><p>\n"; 
    printMetaProjectTab("Project", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('meta_project_info_project_relevance', '');
    ProjectInfo::printProjectSetValTab ('meta_project_info_data_links', '');
    print "</p></div>\n";

    # tab 3
    print "<div id='tab3'><p>\n";
    printMetaProjectTab("JGI", "", \%db_val);
    ProjectInfo::printProjectSetValTab ('meta_project_info_jgi_url', '');
    print "</p></div>\n"; 

    # tab 3 
# No HMP metagenome projects yet
#    print "<div id='tab7'><p>\n"; 
#    ProjectInfo::printProjectTab("HMP", "", \%db_val); 
#    print "</p></div>\n";


    TabHTML::printTabDivEnd();
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:dbNewGoldProject" value="Add Project" class="medbutton" />';

    printHomeLink(); 
 
    print end_form();
} 


######################################################################### 
# dbNewGoldMetaProject
######################################################################### 
sub dbNewGoldMetaProject {

    # get user info 
    my $contact_oid = getContactOid(); 
 
    # get Project_Info definition 
    my $def_project = def_meta_Project_Info(); 
    my @attrs = @{$def_project->{attrs}}; 
 
    my $msg = ""; 
 
    my $project_oid = 0; 
    my $gold_stamp_id = param1('gold_stamp_id'); 
    if ( blankStr($gold_stamp_id) ) { 
        # no GOLD id is given 
	$project_oid = ProjectInfo::getNewProjectId(); 
    } 
    else { 
        # use higher range project oid for GOLD entry 
	$project_oid = db_findMaxID('project_info', 'project_oid') + 1; 
    } 
    if ( $project_oid <= 0 ) { 
        # error checking -- this shouldn't happen 
	return "Incorrect project ID value: $project_oid"; 
    } 
 
    my $ins = "insert into project_info (project_oid, contact_oid, add_date, web_page_code";
    my $vals = "values ($project_oid, $contact_oid, sysdate, 4";

    # update domain
    $ins .= ", domain";
    $vals .= ", 'MICROBIAL'";

    # update lineage 
    for my $aname ( 'ncbi_superkingdom', 'ncbi_phylum',
		    'ncbi_class', 'ncbi_order',
		    'ncbi_family', 'ncbi_genus', 'ncbi_species' ) {
	$ins .= ", " . $aname;
	$vals .= ", 'unclassified'";
    } 

    for my $k ( @attrs ) {
	my $attr_name = $k->{name};
	my $disp_name = $k->{display_name};
	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $edit = $k->{can_edit};
 
	if ( ! $edit ) {
	    next;
	} 
 
# get the names from the ecosystem classification ids
#	my %ecosystemMap;
#	my $dbh=WebFunctions::Connect_IMG;
#	my $ecoSQL = qq{select mc_node_id, node from metagenomic_class_nodes};
#	my $sth = $dbh->prepare($ecoSQL) or die "Couldn't prepare statement: " . $dbh->errstr;
#	$sth->execute() or die "Couldn't execute statement: " . $sth->errstr;
	
#	my @results = @{$sth->fetchall_arrayref()};
#	for my $row (@results) {
#	    my ($mc_node_id, $node )  = @$row;
#	    $ecosystemMap{$mc_node_id} = $node;
#	}
#	my $val = '';
#	if ( (($attr_name eq 'ecosystem') || ($attr_name eq 'ecosystem_category') || 
#	      ($attr_name eq 'ecosystem_type') || ($attr_name eq 'ecosystem_subtype') || (
#		  $attr_name eq 'specific_ecosystem')) ) {
#	    $val = $ecosystemMap{param1($attr_name)};
#	} else {
#        $val = param1($attr_name); 
#    }

        my $val = param1($attr_name); 

	if ( ! (defined $val) || length($val) == 0 ) {
	    next; 
	} 
 
	$ins .= ", " . $attr_name;
 
	if ( $data_type eq 'int' ) { 
	    if ( ! blankStr($val) && ! isInt($val) ) {
		$msg = "$disp_name must be an integer.";
		last;
	    } 
 
	    $vals .= ", " . $val; 
	} 
	elsif ( $data_type eq 'number' ) { 
	    if ( ! blankStr($val) && ! isNumber($val) ) {
		$msg = "$disp_name must be a number.";
		last;
	    } 
 
	    $vals .= ", " . $val; 
	} 
	elsif ( $data_type eq 'date' ) { 
	    if ( ! blankStr($val) && ! isDate($val) ) {
		$msg = "$disp_name must be a date (DD-MON-YY).";
		last; 
	    } 
 
	    $vals .= ", '" . $val . "'";
	} 
	else { 
            $val =~ s/'/''/g;   # replace ' with ''
            $vals .= ", '" . $val . "'";
        } 
 
    } 
 
    if ( ! blankStr($msg) ) {
	return $msg;
    } 
 
    my @sqlList = (); 
    my $sql = $ins . ") " . $vals . ")"; 
    push @sqlList, ( $sql ); 
 
    # insert set-valued attribute
    my @setnames = getMetaProjectAuxTables();
    for my $sname ( @setnames ) { 
	$msg = ProjectInfo::dbUpdateProjectSetValues($project_oid, $sname, 
						     0, \@sqlList);
	if ( ! blankStr($msg) ) { 
	    return $msg; 
	} 
    } 
 
    if ( $is_test ) {
	for $sql ( @sqlList ) { 
	    $msg .= " SQL: " . $sql;
	} 
    } 
    else { 
	db_sqlTrans(\@sqlList);
    }
 
    return $msg; 
} 


######################################################################### 
# UpdateGoldMetaProject 
######################################################################### 
sub UpdateGoldMetaProject { 
    my ($project_oid) = @_;

    my %db_val = ProjectInfo::SelectProjectInfo($project_oid);
 
    my $url=url(); 
    print start_form(-name=>'updateProject',-method=>'post',action=>"$url"); 

    print "<h2>Update Metagenome Study $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid);

    saveProjectPageInfo();

    # save parameters
    for my $p ('web_code', 'category_page_no',
	       'goldproj_orderby', 'project_desc') {
	if ( param1($p) ) {
	    print hiddenVar($p, param1($p));
	}
	elsif ( $p eq 'web_code' ) {
	    print hiddenVar('web_code', 0);
	}
    }

    # tab view 
    TabHTML::printTabAPILinks("updateMetaProject"); 
#    my @tabIndex = ( "#tab1", "#tab2", "#tab3"); 
# No hmp metagenome projects yet
    my @tabIndex = ( "#tab1", "#tab2" );
 
    my @tabNames = 
        ( "Metagenome Info", "Study Info (*)" );
# No HMP metagenome projects yet
#          "HMP Metadata"); 

    my $contact_oid = getContactOid();
    my $isAdmin = getIsAdmin($contact_oid); 
    if ( $isAdmin ) {
	push @tabIndex, ( "#tab3" );
	push @tabNames, ( "JGI Info" );
    }

    TabHTML::printTabDiv( "updateMetaProject", \@tabIndex, \@tabNames ); 
 
    # tab 1
    print "<div id='tab1'><p>\n"; 
    printMetaProjectTab("Metagenome", $project_oid, \%db_val); 
    print "</p></div>\n";

    # tab 2 
    print "<div id='tab2'><p>\n"; 
    printMetaProjectTab("Project", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('meta_project_info_project_relevance', $project_oid);
    ProjectInfo::printProjectSetValTab ('meta_project_info_data_links', $project_oid); 
    print "</p></div>\n";

    # tab 3
    print "<div id='tab3'><p>\n"; 
    printMetaProjectTab("JGI", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('meta_project_info_jgi_url', $project_oid);
    print "</p></div>\n";
 
    # tab 3 
# No HMP Metagenome Projects yet
#     print "<div id='tab7'><p>\n"; 
#    ProjectInfo::printProjectTab("HMP", $project_oid, \%db_val); 
#    print "</p></div>\n";

    TabHTML::printTabDivEnd();
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:dbUpdateGoldProject" value="Update Project" class="meddefbutton" />';
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldMetaProj:showCategory" value="Cancel" class="smbutton" />';

    printHomeLink(); 
 
    print end_form();
} 


#############################################################################
# dbUpdateGoldMetaProject 
#############################################################################
sub dbUpdateGoldMetaProject { 
    # get user info 
    my $contact_oid = getContactOid();
 
    # get Project_Info definition
    my $def_project = def_meta_Project_Info();
    my @attrs = @{$def_project->{attrs}};
 
    my $msg = ""; 
 
    # get project_oid 
    my $project_oid = param1('project_oid'); 
    if ( blankStr($project_oid) ) { 
        $msg = "No project has been selected for update."; 
        return $msg; 
    } 
 
    my $sql = "update project_info set modified_by = $contact_oid, " . 
        "mod_date = sysdate"; 

    # update domain
    $sql .= ", domain = 'MICROBIAL'";

    # update lineage 
    for my $aname ( 'ncbi_superkingdom', 'ncbi_phylum',
		    'ncbi_class', 'ncbi_order',
		    'ncbi_family', 'ncbi_genus', 'ncbi_species' ) {
	$sql .= ", " . $aname . " = 'unclassified'";
    }

    for my $k ( @attrs ) { 
        my $attr_name = $k->{name}; 
        my $disp_name = $k->{display_name}; 
        my $data_type = $k->{data_type}; 
        my $len = $k->{length}; 
        my $edit = $k->{can_edit}; 
 
       if ( ! $edit ) { 
            next; 
        } 

	my $val = param1($attr_name);

        if ( ! (defined $val) || length($val) == 0 ) { 
            $sql .= ", $attr_name = null"; 
            next; 
        } 

#	if (($attr_name eq 'ecosystem') || ($attr_name eq 'ecosystem_category') || 
#	    ($attr_name eq 'ecosystem_type') || ($attr_name eq 'ecosystem_subtype') || 
#	    ($attr_name eq 'specific_ecosystem') ) {
#            # get the names from the ecosystem classification ids
#	    my %ecosystemMap;
#	    my $dbh=WebFunctions::Connect_IMG;
#	    my $ecoSQL = qq{select mc_node_id, node from metagenomic_class_nodes};
#	    my $sth = $dbh->prepare($ecoSQL) or die "Couldn't prepare statement: " . $dbh->errstr;
#	    $sth->execute() or die "Couldn't execute statement: " . $sth->errstr;
#	
#	    my @results = @{$sth->fetchall_arrayref()};
#	    for my $row (@results) {
#		my ($mc_node_id, $node )  = @$row;
#		$ecosystemMap{$mc_node_id} = $node;
#	    }
#            $val = $ecosystemMap{$val};
#	}

        if ( $data_type eq 'int' ) { 
            if ( ! blankStr($val) && ! isInt($val) ) { 
                $msg = "$disp_name must be an integer."; 
                last; 
            }
 
            $sql .= ", $attr_name = $val"; 
        } 
        elsif ( $data_type eq 'number' ) { 
            if ( ! blankStr($val) && ! isNumber($val) ) { 
                $msg = "$disp_name must be a number."; 
                last; 
            }
 
            $sql .= ", $attr_name = $val"; 
        } 
        elsif ( $data_type eq 'date' ) { 
            if ( ! blankStr($val) && ! isDate($val) ) {
                $msg = "$disp_name must be a date (DD-MON-YY).";
                last;
            } 
 
            $sql .= ", $attr_name = '" . $val . "'";
        } 
        else { 
            $val =~ s/'/''/g;   # replace ' with ''
            $sql .= ", $attr_name = '" . $val . "'";
        } 
    } 
 
   $sql .= " where project_oid = $project_oid";
 
    if ( ! blankStr($msg) ) {
        return $msg; 
    } 
 
    if ( $is_test && $contact_oid == 312 ) { 
        print "<p>SQL: $sql</p>\n"; 
    } 
 
    my @sqlList = ( );
    push @sqlList, ( $sql ); 
 
    # set valued
    my @setnames = getProjectAuxTables(); 
    for my $sname ( @setnames ) { 
        $msg = ProjectInfo::dbUpdateProjectSetValues($project_oid, $sname, 1, \@sqlList);
        if ( ! blankStr($msg) ) {
            return $msg; 
        } 
    } 
 
    if ( $is_test ) { 
        for $sql ( @sqlList ) { 
            $msg .= " SQL: " . $sql; 
        } 
    } 
    else { 
        db_sqlTrans(\@sqlList); 
    } 
 

    return "$msg";
}


############################################################################
# getPage - Wrapper for page parameter, in case POST instead of GET
#   method is used.  This is an adaptor function.
############################################################################
sub getPage {
   my $page = param1( "page" );
   return $page;
}


########################################################################## 
# printMetaProjectTab - print metagenome project info in tab format 
########################################################################## 
sub printMetaProjectTab { 
    my ($tab, $project_oid, $db_val) = @_; 
 
    my $def_project = def_meta_Project_Info(); 
    my @attrs = @{$def_project->{attrs}}; 
    my $currentPage = getPage();
 
    if ( ! blankStr($project_oid) ) { 
        # get data from database 
    } 
 
    # javascript for tooltips
    print "<script language='JavaScript' type='text/javascript' " .
        "src='" . $base_url . "/wz_tooltip.js'></script>\n";
 
    print "<p>Required fields are marked with (*). Mouse over MIGS ID to see field description.</p>\n";
#    print "<div id=\"loading\">Loading ...</div>";
 
    print "<table class='img' border='1'>\n";
 
    for my $k ( @attrs ) { 
        # only show attributes in this tab 
        if ( ! ($k->{tab}) || 
             $k->{tab} ne $tab ) {
            next; 
        }
 
        # skip non-editable for new 
        if ( blankStr($project_oid) ) {
            if ( $k->{can_edit} ) { 
                # show attribute 
            }
            elsif ( $k->{name} =~ /^ncbi\_/ ) {
                # show attribute
            } 
            else { 
                next; 
            } 
        }
 
        my $attr_name = $k->{name}; 
        my $data_type = $k->{data_type};
        my $len = $k->{length}; 
        my $disp_name = $k->{display_name};
        if ( ! $disp_name ) {
            $disp_name = $attr_name;
        } 
 
        my $size = 80; 
        if ( $len && $size > $len ) {
            $size = $len;
        }
 
        my $attr_val = "";
        # skip lineage info, if any
        if ( $attr_name eq 'ncbi_superkingdom' ||
             $attr_name eq 'ncbi_phylum' ||
             $attr_name eq 'ncbi_class' ||
             $attr_name eq 'ncbi_order' ||
             $attr_name eq 'ncbi_family' ||
             $attr_name eq 'ncbi_genus' ||
             $attr_name eq 'ncbi_species' ) {
	    next;
        } 
        elsif ( $attr_name eq 'web_page_code' ) {
	    next;
        } 
        else { 
            # get previously saved values
            if ( param1($attr_name) ) {
                $attr_val = param1($attr_name);
            } 
            elsif ( defined $db_val->{$attr_name} ) {
                $attr_val = $db_val->{$attr_name};
            } 
        } 

        print "<tr class='img' >\n";

        # MIGS 
	printCellTooltip($k->{migs_id}, $k->{migs_name}, 70);

        # print attribute display name 
        if ( $k->{font_color} ) { 
            print "  <th class='subhead' align='right'>" .
                "<font color='" . $k->{font_color} .
                "'>" . nbsp(3) . $disp_name . "</font></th>\n";
        } 
        elsif ( $k->{url} ) {
            print "  <th class='subhead' align='right'>";
            print alink($k->{url}, $disp_name, 'target', 1);
            if ( $k->{is_required} ) {
                print " (*) "; 
            } 
            print "</th>\n";
        }
        elsif ( $k->{is_required} ) {
            print "  <th class='subhead' align='right'>" .
                escapeHTML($disp_name) . " (*) </th>\n";
        } 
        else { 
            print "  <th class='subhead' align='right'>" .
                escapeHTML($disp_name) . "</th>\n";
        } 
	
#	if ( $attr_name eq 'project_type' ) {
#	    $attr_val = 'Metagenome';
#            PrintAttribute($attr_val); 
#            print hidden($attr_name, $attr_val);
#	}
#        elsif ( $k->{can_edit} ) { 

	if ( $k->{can_edit} ) { 
            # editable
            if ( $data_type eq 'file' ) {
                print "  <td class='img'   align='left'>" .
                    "<input type='file' name='$attr_name' size='$size'" .
                    " />" . "</td>\n"; 
            } 
            elsif ( $data_type eq 'cv' || $data_type eq 'list' ) {
                # controlled vocabularies - select
                my @db_vals = ();
                if ( $data_type eq 'cv' ) {
                    @db_vals = db_getValues($k->{cv_query});
                } 
                elsif ( $data_type eq 'list' ) {
                    @db_vals = split(/\|/, $k->{list_values});
                } 
		
# If newProject page then use javascript chained menu dynamic population
# for metagenomic classification
# else for updating use normal interface
#		if ( ($attr_name eq 'ecosystem') && ($currentPage eq 'newProject')) {
#		    print "  <td class='img' align='left'>\n";
#		    print "<select id='ecosystem' name='ecosystem' style=\"width:180px;\"\">
#                    <option value=\"Make a selection\" selected=yes>Make a selection</option>
#                    <option value=\"1\">Host-associated</option>
#                    <option value=\"364\">Engineered</option>
#                    <option value=\"489\">Environmental</option>
#                    <option value=\"573\">Unclassified</option>";
#		    print "</select></td>";
#		}
#		elsif ( (($attr_name eq 'ecosystem_category') ||
#			($attr_name eq 'ecosystem_type') ||
#			($attr_name eq 'ecosystem_subtype') ||
#					($attr_name eq 'specific_ecosystem') ) && ($currentPage eq 'newProject'))
#		    {
#			print "  <td class='img' align='left'>\n";
#			print "
#<select id='$attr_name' name='$attr_name' style=\"width:180px;\" disabled>
#<option selected=yes>Make a selection</option>";
# 		    }
#		elsif ( $use_eco_cv && (($attr_name eq 'ecosystem') ||
#			($attr_name eq 'ecosystem_category') ||
#			($attr_name eq 'ecosystem_type') ||
#			($attr_name eq 'ecosystem_subtype') ||
#			($attr_name eq 'specific_ecosystem') ) && ($currentPage eq 'updateProject'))
#		    {
## get the names from the ecosystem classification ids
#	my %ecosystemMap;
#	my $dbh=WebFunctions::Connect_IMG;
#	my $ecoSQL = qq{select mc_node_id, node, node_type from metagenomic_class_nodes};
#	my $sth = $dbh->prepare($ecoSQL) or die "Couldn't prepare statement: " . $dbh->errstr;
#	$sth->execute() or die "Couldn't execute statement: " . $sth->errstr;
#	
#	my @results = @{$sth->fetchall_arrayref()};
#	for my $row (@results) {
#	    my ($mc_node_id, $node, $node_type )  = @$row;
#	    $ecosystemMap{$node}{$node_type} = $mc_node_id;
#	}
#		    print "  <td class='img'   align='left'>\n";
#		    print "<select id='$attr_name' name='$attr_name' class='img' size='1'>\n";
#		    if ( ! $k->{is_required} ) {
#			print "   <option value=''> </option>\n";
#		    } 
#		    for my $val ( sort @db_vals ) { 
#			print "   <option value='$ecosystemMap{$val}{$attr_name}'";
#			if ( $val eq $attr_val ) {
#			    print " selected ";
#			} 
#			print ">" . escapeHTML($val) . "</option>\n";
#		    } 
#		    print "</select></td>\n"; 
#		    } else {

		    print "  <td class='img'   align='left'>\n";
		    print "<select name='$attr_name' class='img' size='1'>\n";

                    if ( ! $attr_val && $k->{default_value} ) {
                        $attr_val = $k->{default_value}; 
                    } 

		    if ( ! $k->{is_required} ) {
			print "   <option value=''> </option>\n";
		    } 
		    for my $val ( sort @db_vals ) { 
			print "   <option value='" . escapeHTML($val) . "'";
			if ( $val eq $attr_val ) {
			    print " selected ";
			} 
			print ">" . escapeHTML($val) . "</option>\n";
		    }

		    print "</select></td>\n"; 
#		}     
	    }
	    elsif ( $data_type eq 'cv2' ) {
                print "  <td class='img'   align='left'>\n";
                print "<select name='$attr_name' class='img' size='1'>\n";
                if ( ! $k->{is_required} ) { 
                    print "   <option value=''> </option>\n";
                }
 
                my $sql2 = $k->{cv_query};
                my $dbh2 = Connect_IMG();
		webLog("$sql2\n");                
                my $cur2=$dbh2->prepare($sql2); 
                $cur2->execute(); 

		if ( ! $attr_val && $k->{default_value} ) {
		    $attr_val = $k->{default_value}; 
		} 
 
                for (my $j2 = 0; $j2 <= 10000; $j2++) {
                    my ($id2, $name2) = $cur2->fetchrow_array(); 
                    if ( ! $id2 && !$name2 ) { 
                        last; 
                    } 
 
                    print "    <option value='" . escapeHTML($id2) . "'";
                    if ( length($attr_val) > 0 && $attr_val eq $id2 ) {
                        print " selected "; 
                    }
 
                    print ">$id2 - $name2</option>\n"; 
                } 
                print "</select></td>\n"; 
                $cur2->finish();
                $dbh2->disconnect(); 
            } 
            elsif ( $len >= 2000 && length($attr_val) > 60 ) {
                # use text area instead
                print "  <td class='img'   align='left'>" .
                    "<textarea id='$attr_name' name='$attr_name' " .
                    "rows='5' cols='60' maxLength='$len'>\n"; 
                print $attr_val;
                print "</textarea>\n";
            }
            else { 
                print "  <td class='img'   align='left'>" .
                    "<input type='text' name='$attr_name' value='";
                if ( length($attr_val) > 0 ) { 
                    print escapeHTML($attr_val);
                } 
                print "' size='$size' maxLength='$len'/>" . "</td>\n";
            } 
        } 
        elsif ( $k->{font_color} ) { 
            print "  <td class='img' align='left'>" .
                "<font color='" . $k->{font_color} . "'>"; 
            if ( $attr_val ) {
                print escapeHTML($attr_val);
            } 
            print "</font></td>\n"; 
        } 
        else { 
            # non-editable
            # display only field 
            PrintAttribute($attr_val); 
            print hidden($attr_name, $attr_val);
        } 
 
        print "</tr>\n"; 
    } 
 
    print "</table>\n";
} 
 
 

############################################################################# 
# DeleteGoldProject - delete project 
############################################################################# 
sub DeleteGoldProject { 
    my $url=url(); 
 
    print start_form(-name=>'deleteProject',-method=>'post',action=>"$url"); 
 
    my $project_oid = param1('project_oid'); 
    if ( blankStr($project_oid) ) { 
        printError("No project has been selected."); 
        print end_form(); 
        return; 
    } 
 
    print "<h2>Delete Metagenome Study $project_oid</h2>\n"; 
    print hidden('project_oid', $project_oid); 

    saveProjectPageInfo();

    # save parameters
    for my $p ('web_code', 'category_page_no',
	       'goldproj_orderby', 'project_desc') {
	if ( defined param1($p) ) {
	    print hiddenVar($p, param1($p));
	}
    }

    # check whether there are any FK 
    # check submission 
    my $cnt = db_getValue("select count(*) from submission where project_info = $project_oid"); 
    if ( $cnt > 0 ) { 
        printError("This project is linked to an IMG submission. You cannot delete a submitted project."); 
        print end_form(); 
        return; 
    } 
 
    # show project, if any 
    ProjectInfo::showProjectSample($project_oid); 
 
    print "<p>Please click the Delete button to confirm the deletion, or cancel the deletion.</p>\n"; 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:dbDeleteGoldProject" value="Delete" class="smdefbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldMetaProj:showCategory" value="Cancel" class="smbutton" />'; 
 
    print "<p>\n"; 
    printHomeLink(); 
 
    print end_form(); 
} 


#########################################################################
# CopyGoldProject 
######################################################################### 
sub CopyGoldProject { 
    my ($project_oid) = @_;
 
    # check project oid 
    if ( blankStr($project_oid) ) {
        printError("No project has been selected."); 
        print end_form(); 
        return;
    }
 
    my $gold_stamp_id = db_getValue("select gold_stamp_id from project_info where project_oid = $project_oid");
 
    my $url=url(); 
    print start_form(-name=>'copyProject',-method=>'post',action=>"$url");
 
    print "<h2>Copy Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid); 

    saveProjectPageInfo();
 
    my $web_code = param1('web_code');
    if ( length($web_code) > 0 ) {
	print hiddenVar('web_code', $web_code);
    }

    # get new project oid 
    my $new_proj_oid = 0;
    if ( $gold_stamp_id ) {
        # GOLD project
        # use higher range project oid for GOLD entry
        $new_proj_oid = db_findMaxID('project_info', 'project_oid') + 1;
    } 
    else { 
        $new_proj_oid = ProjectInfo::getNewProjectId();
    }

    if ( $new_proj_oid <= 0 ) {
        # error checking -- this shouldn't happen 
        printError("Incorrect new project ID value: $new_proj_oid"); 
        print end_form(); 
        return;
    } 
 
    print "<h3>New Project ID: $new_proj_oid</h3>\n";
 
#    $new_proj_oid = 2; 
    my @sqlList = ();
 
    my $contact_oid = getContactOid();
    my $ins = "insert into project_info(project_oid, contact_oid, add_date";
    my $sel = "select $new_proj_oid, $contact_oid, sysdate";

    # set domain
    $ins .= ", domain";
    $sel .= ", 'MICROBIAL'";

    # set lineage 
    for my $aname ( 'ncbi_superkingdom', 'ncbi_phylum',
		    'ncbi_class', 'ncbi_order',
		    'ncbi_family', 'ncbi_genus', 'ncbi_species' ) {
	$ins .= ", " . $aname;
	$sel .= ", 'unclassified'";
    } 

    # get Project_Info definition 
    my $def_project = def_meta_Project_Info();
    my @attrs = @{$def_project->{attrs}};
    for my $attr ( @attrs ) { 
        if ( $attr->{name} eq 'pmo_project_id' ||
                $attr->{name} eq 'jgi_dir_number' ||
                $attr->{name} eq 'hmp_id' ||
                $attr->{name} eq 'ncbi_project_id' ) { 
	    # skip pmo_project_id etc.
	    next;
	}
        elsif ( $attr->{name} =~ /^ncbi\_/ ) {
            # ok - NCBI taxon info
        } 
        elsif ( ! $attr->{can_edit} ) { 
            # skip non-editable attribute
            next;
        }
 
        $ins .= ", " . $attr->{name}; 
        $sel .= ", " . $attr->{name};
    } 
 
    my $sql = $ins . ") " . $sel . " from project_info " .
        "where project_oid = " . $project_oid;
    push @sqlList, ( $sql ); 
 
    # set-valued attributes
    my @tables = getProjectAuxTables();
    for my $tname ( @tables ) { 
        my $def_aux = def_Class($tname); 
        if ( ! $def_aux ) { 
            next; 
        } 
 
        my @aux_attrs = @{$def_aux->{attrs}};
        $ins = "insert into $tname (" . $def_aux->{id};
        $sel = "select $new_proj_oid";
        for my $attr2 ( @aux_attrs ) {
            if ( $attr2->{name} eq $def_aux->{id} ) {
                next; 
            } 
 
            $ins .= ", " . $attr2->{name};
            $sel .= ", " . $attr2->{name}; 
        } 
        $sql = $ins . ") " . $sel . " from " . $tname .
            " where " . $def_aux->{id} . " = " . $project_oid;
        push @sqlList, ( $sql );
    } 

#    for $sql ( @sqlList ) {
#	print "<p>SQL: $sql</p>\n";
#    }

    db_sqlTrans(\@sqlList); 
 
    print "<p>New project has been created by copying project " .
        $project_oid . ".</p>\n";
 
    print "<p>Note: Samples of this project (if any) are not copied.</p>\n";
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:showCategory" value="OK" class="smbutton" />'; 
 
    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
} 


############################################################################# 
# SearchByID - search by GOLD Stamp ID
############################################################################# 
sub SearchByID {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h3>Search GOLD Metagenome Studies by GOLD Stamp ID</h3>\n"; 

    print "<p>Enter a GOLD Stamp ID or select from the list.</p>\n";

    print "<h4>Enter a GOLD Stamp ID</h4>\n";
    print "<input type='text' name='gold_stamp_id_1' value='" .
	"' size='10' maxLength='10'/>\n";

    print "<h4>Select a GOLD Stamp ID</h4>\n";
    my @gold_ids = db_getValues("select gold_stamp_id from project_info where domain = 'MICROBIAL' order by gold_stamp_id");
    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
    for my $id0 ( @gold_ids ) {
        print "    <option value='$id0' >$id0</option>\n";
    }
    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:searchIdResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 

}


#########################################################################
# ShowSearchIdResultPage
# 
# admin does not have condition on contact_oid
##########################################################################
sub ShowSearchIdResultPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Stamp ID Result Page</h2>\n"; 

#    print hiddenVar('next_page', 'searchIdResult');

    my $contact_oid = getContactOid();

    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    my $cond = "";
    my $gold_stamp_id_1 = param1('gold_stamp_id_1');
    $gold_stamp_id_1 =~ s/'/''/g;   # replace ' with '', just in case
    my $gold_stamp_id_2 = param1('gold_stamp_id_2');
    $gold_stamp_id_2 =~ s/'/''/g;   # replace ' with '', just in case

    if ( ! blankStr($gold_stamp_id_1) ) {
	$cond = " where lower(p.gold_stamp_id) in ( '" .
	    lc($gold_stamp_id_1) . "'";
	if ( ! blankStr($gold_stamp_id_2) ) {
	    $cond .= ", '" . lc($gold_stamp_id_2) . "'";
	}
	$cond .= " )";
    }
    elsif ( ! blankStr($gold_stamp_id_2) ) {
	$cond = " where lower(p.gold_stamp_id) = '" .
	    lc($gold_stamp_id_2) . "'";
    }
    else {
        printError("Please enter or select a GOLD Stamp ID."); 
        print end_form(); 
        return;
    }

    if ( $isAdmin ne 'Yes' ) {
        $cond = " and p.contact_oid = $contact_oid";
    } 
 
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
            $cond
        }; 
 
    webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>ER Study ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Category</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 > $default_max_row ) {
            last;   # just in case
        } 
 
        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getMProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 

    print "<font color='blue'>(Number of projects found: $cnt2)</font>\n";

    $cur->finish(); 
    $dbh->disconnect(); 

    if ( $cnt2 > 0 ) {
	printGoldMetaProjButtons();

	printAdditionalProjSection();
    }

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}


############################################################################# 
# AdvancedSearch - advanced search
############################################################################# 
sub AdvancedSearch {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Projects by Query Condition</h2>\n"; 

    print "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    # gold stamp ID 
    ProjectInfo::printFilterCond('GOLD Stamp ID', 'gold_filter:gold_stamp_id',
				 'text', '', '', '');

    # ncbi_project_id
    ProjectInfo::printFilterCond('NCBI Project ID',
				 'gold_filter:ncbi_project_id', 
				 'number', '', '', '');

    # ncbi_project_name 
    ProjectInfo::printFilterCond('NCBI Project Name',
				 'gold_filter:ncbi_project_name', 
				 'text', '', '', '');

    # gold web page code 
    ProjectInfo::printFilterCond('GOLD Web Page Code', 'gold_filter:web_page_code',
				 'select', 'query',
				 "select term_oid+1, description " .
				 "from web_page_codecv order by term_oid",
				 '');

    # project type 
    ProjectInfo::printFilterCond('Project Type', 'gold_filter:project_type', 
				 'select', 'query', 
				 'select cv_term from project_typecv order by cv_term', 
				 '');
 
    # project status 
    ProjectInfo::printFilterCond('Project Status', 'gold_filter:project_status', 
				 'select', 'query', 
				 'select cv_term from project_statuscv order by cv_term', 
				 '');
 
    # domain 
    ProjectInfo::printFilterCond('Domain', 'gold_filter:domain',
				 'select', 'list',
				 'ARCHAEAL|BACTERIAL|EUKARYAL|MICROBIAL|PLASMID|VIRUS',
				 '');
 
    # seq status 
    ProjectInfo::printFilterCond('Sequencing Status', 'gold_filter:seq_status',
				 'select', 'query', 
				 'select cv_term from seq_statuscv order by cv_term',
				 '');
 
    # seq method 
    ProjectInfo::printFilterCond('Sequencing Method', 'gold_filter:seq_method',
				 'select', 'query',
				 'select cv_term from seq_methodcv order by cv_term',
				 '');
 
    # availability: Proprietary, Public
    ProjectInfo::printFilterCond('Availability', 'gold_filter:availability', 
				 'select', 'list', 'Proprietary|Public', '');

    # phylogeny 
    ProjectInfo::printFilterCond('Category', 'gold_filter:phylogeny', 
				 'select', 'query', 
				 'select cv_term from phylogenycv order by cv_term', 
				 '');

    # project display name 
    ProjectInfo::printFilterCond('Project Display Name', 'gold_filter:display_name', 
				 'text', '', '', '');
 
    # genus 
    ProjectInfo::printFilterCond('Genus', 'gold_filter:genus', 
				 'text', '', '', '');
 
    # species 
    ProjectInfo::printFilterCond('Species', 'gold_filter:species', 
				 'text', '', '', '');
 
    # common_name 
    ProjectInfo::printFilterCond('Common Name', 'gold_filter:common_name', 
				 'text', '', '', '');

    # add date
    ProjectInfo::printFilterCond('Add Date', 'gold_filter:add_date',
				 'date', '', '', '');
 
    # mod date
    ProjectInfo::printFilterCond('Last Mod Date', 'gold_filter:mod_date',
				 'date', '', '', '');

#    print "<p>GOLD Stamp ID:\n";
#    print nbsp(3);
#    print "<input type='text' name='gold_stamp_id' value='" .
#	"' size='10' maxLength='10'/>\n";

#    print "<h4>Select a GOLD Stamp ID</h4>\n";
#    my @gold_ids = db_getValues("select gold_stamp_id from project_info order by gold_stamp_id");
#    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
#    for my $id0 ( @gold_ids ) {
#        print "    <option value='$id0' >$id0</option>\n";
#    }
#    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:advSearchResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}


######################################################################### 
# goldFilterParams - get all gold project filter parameters 
######################################################################### 
sub goldFilterParams { 
    my @all_params = ( 'gold_filter:project_type', 
                       'gold_filter:project_status', 
                       'gold_filter:domain', 
                       'gold_filter:seq_status', 
                       'gold_filter:seq_method', 
                       'gold_filter:availability', 
                       'gold_filter:phylogeny', 
                       'gold_filter:display_name', 
                       'gold_filter:genus', 
                       'gold_filter:species', 
                       'gold_filter:common_name', 
                       'gold_filter:ncbi_project_id', 
                       'gold_filter:ncbi_project_name', 
                       'gold_filter:gold_stamp_id', 
                       'gold_filter:add_date', 
                       'gold_filter:mod_date', 
                       'gold_filter:web_page_code' ); 
 
    return @all_params; 
} 


######################################################################### 
# goldFilterCondition 
########################################################################## 
sub goldFilterCondition { 
    my $cond = ""; 
 
    # filter condition ??? 
    my @all_params = goldFilterParams(); 
    for my $p0 ( @all_params ) { 
        my $cond1 = ""; 
        my ($tag, $fld_name) = split(/\:/, $p0);
 
        my $op_name = $p0 . ":op";
        my $op1 = "=";
        if ( param1($op_name) ) {
            $op1 = param1($op_name);
        }

        if ( $op1 eq 'is null' || $op1 eq 'is not null' ) {
            $cond1 ="p.$fld_name $op1";
        }
        elsif ( param1($p0) ) { 
            my @vals = split(/\,/, param1($p0)); 
            my ($tag, $fld_name) = split(/\:/, $p0); 
 
            if ( lc($fld_name) eq 'display_name' ||
                 lc($fld_name) eq 'genus' || 
                 lc($fld_name) eq 'species' ||
                 lc($fld_name) eq 'common_name' ||
                 lc($fld_name) eq 'ncbi_project_name' ||
		 lc($fld_name) eq 'gold_stamp_id' ) {
                my $s1 = param1($p0); 
                if ( length($s1) > 0 ) { 
                    $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
                } 
            } 
            elsif ( lc($fld_name) eq 'ncbi_project_id' ) {
                my $s1 = param1($p0); 
                if ( length($s1) > 0 && isInt($s1) ) { 
                    $cond1 = "p.$fld_name = $s1";
                } 
            } 
            elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) { 
                    $cond1 = "p.project_oid in (select pism.project_oid " .
                        "from project_info_seq_method pism " .
                        "where lower(pism.$fld_name) = '" . lc($s1) . "')";
                } 
            }
            elsif ( lc($fld_name) eq 'web_page_code' ) {
                for my $s2 ( @vals ) {
                    my $i = $s2 - 1;
                    if ( $i >= 0 ) { 
                        $cond1 = "p.$fld_name = $i";
                    } 
                } 
            } 
            elsif ( lc($fld_name) eq 'add_date' ||
                    lc($fld_name) eq 'mod_date' ) {
                my $op_name = $p0 . ":op"; 
                my $op1 = param1($op_name);
                my $d1 = param1($p0);
                if ( !blankStr($op1) && !blankStr($d1) ) {
                    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
                } 
            }
            else { 
                for my $s2 ( @vals ) { 
                    $s2 =~ s/'/''/g;   # replace ' with ''
                    if ( blankStr($cond1) ) { 
                        $cond1 = "p.$fld_name in ('" . $s2 . "'";
                    } 
                    else { 
                        $cond1 .= ", '" . $s2 . "'";
                    }
                } 
 
                if ( ! blankStr($cond1) ) {
                    $cond1 .= ")"; 
                } 
            } 
        } 
 
        if ( ! blankStr($cond1) ) { 
            if ( blankStr($cond) ) { 
                $cond = $cond1; 
            } 
            else {
                $cond .= " and " . $cond1;
            } 
        } 
    }  # end for p0 
 
    return $cond; 
} 


##########################################################################
# selectedGoldProjCount
##########################################################################
sub selectedGoldProjCount { 
    my ($contact_oid, $filter_cond) = @_; 
 
    my $dbh = Connect_IMG();
 
    my $sql = "select count(*) from project_info p"; 
    if ( $contact_oid ) { 
        $sql .= " where p.contact_oid = $contact_oid"; 
        if ( ! blankStr($filter_cond) ) { 
            $sql .= " and " . $filter_cond; 
        } 
    } 
    else { 
        if ( ! blankStr($filter_cond) ) { 
            $sql .= " where " . $filter_cond; 
        } 
    } 
    webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 


########################################################################## 
# ShowAdvSearchResultPage
########################################################################## 
sub ShowAdvSearchResultPage{
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Search Result</h2>\n"; 

    my $filter_cond = goldFilterCondition(); 
 
    my $select_cnt = 0;
    if ( $isAdmin eq 'Yes' ) {
        $select_cnt = selectedGoldProjCount( '', $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 
    else { 
        $select_cnt = selectedGoldProjCount($contact_oid, $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 

    if ( $select_cnt == 0 ) {
	print "<p>\n";
	printHomeLink();

	print end_form(); 
	return;
    }

    printGoldMetaProjButtons();

    # show results
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
        }; 

    if ( ! blankStr($filter_cond) ) {
	$sql .= " where " . $filter_cond;
    }

    print "<p>\n";
    webLog("$sql\n");    
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>ER Study ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Category</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getMProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";

        if ( $cnt2 >= 1000 ) {
            last;
        } 
    }
    print "</table>\n"; 

    if ( $cnt2 < $select_cnt ) {
	print "<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    }
    else {
	print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    }

    $cur->finish(); 
    $dbh->disconnect(); 

    printGoldMetaProjButtons();

    printAdditionalProjSection();

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}


########################################################################## 
# printAdditionalProjSection
########################################################################## 
sub printAdditionalProjSection {
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 

    # allow admin to change IMG contact 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
        print "<h3>Change IMG Contact or Grant Edit Privilege for Selected Project</h3>\n"; 
        print "<p>New IMG Contact:\n"; 
        print "<select name='new_contact' class='img' size='1'>\n"; 
 
        my $sql2 = "select contact_oid, username from contact order by username, contact_oid"; 
        my $dbh2 = Connect_IMG_Contact(); 
	webLog("$sql2\n");        
        my $cur2=$dbh2->prepare($sql2); 
        $cur2->execute(); 
 
        for (my $j2 = 0; $j2 <= 10000; $j2++) { 
            my ($id2, $name2) = $cur2->fetchrow_array(); 
            if ( ! $id2 ) { 
                last; 
            } 
 
            print "    <option value='$id2'"; 
            if ( $contact_oid == $id2 ) { 
                print " selected "; 
            } 
            print ">$name2 (OID: $id2)</option>\n"; 
        } 
        print "</select>\n"; 
        $cur2->finish(); 
        $dbh2->disconnect(); 
 
        print nbsp(3);
        print '<input type="submit" name="_section_GoldMetaProj:changeContact" value="Change Contact" class="medbutton" />'; 

        print "<p>\n"; 
        print '<input type="submit" name="_section_GoldMetaProj:grantEdit" value="Grant Edit Privilege" class="medbutton" />'; 
    } 
  
    # allow admin to change GOLD id 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
 
        print "<h3>Assign or Change GOLD Stamp ID for Selected Study</h3>\n";
        print "<p>Note: GCAT ID and HMP ID (for HMP projects only) will be automatically assigned when the selected project gets a GOLD Stamp ID. GCAT ID and HMP ID (if any) will be automatically deleted when the study GOLD Stamp ID is deleted.</p>\n"; 
	print "<br/>\n";
        print '<input type="submit" name="_section_GoldMetaProj:setGoldStampId" value="Assign GOLD Stamp ID" class="medbutton" />';
        print "&nbsp; \n";
        print '<input type="submit" name="_section_GoldMetaProj:delGoldStampId" value="Delete GOLD Stamp ID" class="medbutton" />';
    } 
 
    # allow admin to merge projects
    if ( $isAdmin eq 'Yes' ) {
	print hr;
	print "<h3>Merge Metagenome Studies</h3>\n"; 
	print "<p>Merge the selected project with another metagenome study.</p>\n";
	print "<p>\n";
	print '<input type="submit" name="_section_GoldMetaProj:mergeGoldProj" value="Merge Projects" class="medbutton" />';
    }
}


######################################################################### 
# PrintMergeGoldProjPage
######################################################################### 
sub PrintMergeGoldProjPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Merge Metagenome Studies</h2>\n"; 

    my $project_oid = param1('project_oid');
    if ( ! $project_oid ) {
        printError("Please select a project first."); 
        print end_form(); 
        return;
    }
    print "<h3>Selected Project: $project_oid</h3>\n";
    print hiddenVar('project_oid', $project_oid);

    print "<p>Merge the selected project with the following project:</p>\n"; 
 
    my $sql2 = "select project_oid, display_name from project_info " . 
	"where domain = 'MICROBIAL' " .
        "order by project_oid"; 
    my $dbh2 = Connect_IMG (); 
    webLog("$sql2\n");    
    my $cur2=$dbh2->prepare($sql2); 
    $cur2->execute(); 
 
    print "<select name='merged_project' class='img' size='1'>\n"; 
    for (my $j2 = 0; $j2 <= 100000; $j2++) {
        my ($id2, $name2) = $cur2->fetchrow_array();
        if ( ! $id2 ) { 
            last; 
        } 
 
	if ( $id2 == $project_oid ) {
	    next;
	}

        print "    <option value='$id2'>";
        print "$id2 - $name2</option>\n";
    } 
    $cur2->finish(); 
    $dbh2->disconnect(); 
    print "</select>\n"; 
 
    print "<p>\n";
    print '<input type="submit" name="_section_GoldMetaProj:mergeProjects" value="Merge Projects" class="medbutton" />';

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
} 
 


##########################################################################
# FilterGoldMetaProject_old - set filter on displaying projects
#             so that there won't be too many to display
##########################################################################
sub FilterGoldMetaProject_old { 
    my $url=url(); 
 
    print start_form(-name=>'filterProject',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid); 
 
    print "<h3>Set GOLD Metagenome Study Filter for $uname</h3>\n";

    # only show my projects?
    my $only_my_project = getSessionParam('meta_proj_filter:only_my_project'); 
    print "<p>Only display my metagenome studies? \n";
    print "<input type='checkbox' name='meta_proj_filter:only_my_project' value='1' "; 
    if ( $only_my_project ) { 
        print " checked ";
    } 
    print ">Yes\n"; 
    print "</p>\n"; 

    # max display 
    my $max_display = getSessionParam('meta_proj_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = "$default_max_row"; 
    } 
    print "<p>Maximal Rows of Display:\n"; 
    print nbsp(3); 
    print "<select name='meta_proj_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
                   '300', '500', '800', '1000', '2000',
                   '3000', '4000', '5000', '10000' ) {
        print "    <option value='$cnt0' ";
        if ( $cnt0 eq $max_display ) {
            print " selected ";
        }
        print ">$cnt0</option>\n";
    }
    print "</select>\n";
 
    # gold stamp ID
    printFilterCond('GOLD Stamp ID', 'meta_proj_filter:gold_stamp_id',
                    'select', 'query', 
                    'select distinct gold_stamp_id from project_info order by gold_stamp_id', 
                    getSessionParam('meta_proj_filter:gold_stamp_id'));

    # ncbi_project_id
    printFilterCond('NCBI Project ID', 'meta_proj_filter:ncbi_project_id', 
                    'number', '', '', 
                    getSessionParam('meta_proj_filter:ncbi_project_id')); 

    # ncbi_project_name
    printFilterCond('NCBI Project Name', 'meta_proj_filter:ncbi_project_name', 
                    'text', '', '', 
                    getSessionParam('meta_proj_filter:ncbi_project_name')); 
 
    # gold web page code 
#    printFilterCond('GOLD Web Page Code', 'meta_proj_filter:web_page_code',
#                    'select', 'query', 
#                    'select term_oid+1, description from web_page_codecv order by term_oid', 
#                    getSessionParam('meta_proj_filter:web_page_code'));
 
    # project type
    printFilterCond('Project Type', 'meta_proj_filter:project_type',
                    'select', 'query', 
                    'select cv_term from project_typecv order by cv_term',
                    getSessionParam('meta_proj_filter:project_type'));
 
    # project status
    printFilterCond('Project Status', 'meta_proj_filter:project_status',
                    'select', 'query',
                    'select cv_term from project_statuscv order by cv_term',
                    getSessionParam('meta_proj_filter:project_status'));
 
    # domain 
#    printFilterCond('Domain', 'meta_proj_filter:domain',
#                    'select', 'list', 'ARCHAEAL|BACTERIAL|EUKARYAL|MICROBIAL|PLASMID|VIRUS',
#                    getSessionParam('meta_proj_filter:domain'));
 
    # seq status
    printFilterCond('Sequencing Status', 'meta_proj_filter:seq_status',
                    'select', 'query',
                    'select cv_term from seq_statuscv order by cv_term',
                    getSessionParam('meta_proj_filter:seq_status')); 
 
    # seq method 
    printFilterCond('Sequencing Method', 'meta_proj_filter:seq_method',
                    'select', 'query', 
                    'select cv_term from seq_methodcv order by cv_term',
                    getSessionParam('meta_proj_filter:seq_method')); 
 
    # availability: Proprietary, Public 
    printFilterCond('Availability', 'meta_proj_filter:availability',
                    'select', 'list', 'Proprietary|Public',
                    getSessionParam('meta_proj_filter:availability'));
 
    # phylogeny 
    printFilterCond('Category (Phylogeny)',
		    'meta_proj_filter:phylogeny', 
                    'select', 'query', 
#                    'select cv_term from phylogenycv order by cv_term',
		    "select distinct phylogeny from project_info where domain = 'MICROBIAL' order by phylogeny",
                    getSessionParam('meta_proj_filter:phylogeny'));
 
    # project display name 
    printFilterCond('Project Display Name', 'meta_proj_filter:display_name', 
                    'text', '', '', 
                    getSessionParam('meta_proj_filter:display_name'));
 
    # genus 
    printFilterCond('Habitat (Genus)', 'meta_proj_filter:genus', 
                    'text', '', '', 
                    getSessionParam('meta_proj_filter:genus'));
 
    # species 
    printFilterCond('Community (Species)', 'meta_proj_filter:species',
                    'text', '', '', 
                    getSessionParam('meta_proj_filter:species')); 
    # common_name
    printFilterCond('Common Name', 'meta_proj_filter:common_name', 
                    'text', '', '', 
                    getSessionParam('meta_proj_filter:common_name')); 

    # relevance 
    printFilterCond('Project Relevance', 'meta_proj_filter:project_relevance',
                    'select', 'query', 
                    'select cv_term from relevancecv order by cv_term',
                    getSessionParam('meta_proj_filter:project_relevance')); 
 
    # add date 
    printFilterCond('Add Date', 'meta_proj_filter:add_date',
                    'date', '', '', 
                    getSessionParam('meta_proj_filter:add_date')); 
 
    # mod date 
    printFilterCond('Last Mod Date', 'meta_proj_filter:mod_date', 
                    'date', '', '', 
                    getSessionParam('meta_proj_filter:mod_date')); 
 
    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:applyGoldProjectFilter" value="Apply Study Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print end_form();
} 


##########################################################################
# FilterGoldMetaProject - set filter on displaying projects
#             so that there won't be too many to display
##########################################################################
sub FilterGoldMetaProject { 
    my $url=url(); 
 
    print start_form(-name=>'filterProject',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid); 
    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h3>Set GOLD Metagenome Study Filter for $uname</h3>\n";

    # only show my projects?
    my $only_my_project = getSessionParam('meta_proj_filter:only_my_project'); 
    print "<p>Only display my metagenome studies? \n";
    print "<input type='checkbox' name='meta_proj_filter:only_my_project' value='1' "; 
    if ( $only_my_project ) { 
        print " checked ";
    } 
    print ">Yes\n"; 
    print "</p>\n"; 

    # max display 
    my $max_display = getSessionParam('meta_proj_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = "$default_max_row"; 
    } 
    print "<p>Maximal Rows of Display:\n"; 
    print nbsp(3); 
    print "<select name='meta_proj_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
                   '300', '500', '800', '1000', '2000',
                   '3000', '4000', '5000', '10000' ) {
        print "    <option value='$cnt0' ";
        if ( $cnt0 eq $max_display ) {
            print " selected ";
        }
        print ">$cnt0</option>\n";
    }
    print "</select>\n";
 
    my $def_project = def_meta_Project_Info();
    my @attrs = @{$def_project->{attrs}};
 
    my @tabs =  ( 'Metagenome', 'Project', 'Sequencing',
		  'EnvMeta', 'HostMeta', 'OrganMeta', 'HMP' );
 
    # tab display label 
    my %tab_display = (
                       'Organism' => 'Organism Info',
                       'Metagenome' => 'Metagenome Info',
                       'Project' => 'Study Info',
                       'Sequencing' => 'Sequencing Info', 
                       'EnvMeta' => 'Environment Metadata',
                       'HostMeta' => 'Host Metadata',
                       'OrganMeta' => 'Organism Metadata', 
		       'HMP' => 'HMP Metadata');

    if ( $isAdmin eq 'Yes' ) {
	push @tabs, ( 'JGI' );
	$tab_display{'JGI'} = 'JGI Info';
    }
 
    my @aux_tables = getProjectAuxTables(); 
 
    print "<table class='img' border='1'>\n";
    for my $tab ( @tabs ) {
        my $tab_label = $tab;
        if ( $tab_display{$tab} ) {
            $tab_label = $tab_display{$tab};
        }
 
        print "<tr class='img' >\n"; 
        print "  <th class='subhead' colspan='3' align='right' bgcolor='lightblue'>" . 
	    "<font color='darkblue'>" . $tab_label . "</font></th>\n";
        print "</tr>\n"; 
 
        for my $k ( @attrs ) { 
            if ( $k->{tab} ne $tab ) { 
                next; 
            } 
            elsif ( ! $k->{filter_cond} ) { 
                next; 
            } 
 
            # show filter condition
            my $filter_name = "meta_proj_filter:" . $k->{name};
            my $ui_type = 'text';
            my $select_type = '';
            my $select_method = ''; 
 
            if ( $k->{name} eq 'contact_oid' ) { 
                # special for contact_oid 
		my $perm_cond = " ";
		if ( $isAdmin ne 'Yes' ) {
		    $perm_cond = " where p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid)"; 
		}

                $ui_type = 'select';
                $select_type = 'query';
                $select_method = qq{
                    select c.contact_oid, c.username
                        from contact c 
                        where c.contact_oid in
                        (select p.contact_oid from project_info p $perm_cond)
                        order by 2
                    };
            } 
            elsif ( $k->{data_type} eq 'cv' ) {
                $ui_type = 'select'; 
                $select_type = 'query'; 
                $select_method = $k->{cv_query};
            } 
            elsif ( $k->{data_type} eq 'list' ) {
                $ui_type = 'select'; 
                $select_type = 'list'; 
                $select_method = $k->{list_values};
            } 
            elsif ( $k->{data_type} eq 'int' || 
                    $k->{data_type} eq 'number' ) { 
                $ui_type = 'number'; 
            }
            elsif ( $k->{data_type} eq 'date' ) {
                $ui_type = 'date';
            } 
	    ProjectInfo::printFilterCondRow($k->{display_name}, $filter_name,
                               $ui_type, $select_type, $select_method,
					    getSessionParam($filter_name));
        }  # end for my k 
 
        for my $k2 ( @aux_tables ) {
            my $def_aux = def_Class($k2);
            if ( ! $def_aux ) { 
                next;
            } 
            if ( $def_aux->{tab} ne $tab ) {
                next;
            } 
	    if ($k2 eq 'project_info_cyano_metadata') {
		next;
	    }
            # special condition for data links 
            if ( $k2 eq 'project_info_data_links' ) { 
                for my $typ ( 'Funding', 'Seq Center' ) { 
                    my $filter_name = "meta_proj_filter:" . $k2 . ":" . $typ; 
                    my $filter_disp = "Data Links: " . $typ; 
                    my $filter_sql = qq{ 
                        select distinct db_name 
                            from project_info_data_links 
                            where link_type = '$typ' 
                            and db_name is not null 
                            order by 1 
                        }; 
                    # $filter_sql = "select cv_term from db_namecv"; 
                    ProjectInfo::printFilterCondRow($filter_disp, 
                                                    $filter_name, 
                                                    'select', 'query', 
                                                    $filter_sql, 
                                   getSessionParam($filter_name)); 
                } 
            } 
 
            my @aux_attrs = @{$def_aux->{attrs}};
            for my $k3 ( @aux_attrs ) {
                if ( ! $k3->{filter_cond} ) {
                    next;
                }
 
                my $filter_name = "meta_proj_filter:" . $k3->{name};

		my $ui_type = 'text';
		my $select_type = '';
		my $select_method = '';

		if ( $k3->{data_type} eq 'cv' ) {
		    $ui_type = 'select'; 
		    $select_type = 'query'; 
		    $select_method = $k3->{cv_query};
		} 
		elsif ( $k3->{data_type} eq 'list' ) {
		    $ui_type = 'select'; 
		    $select_type = 'list'; 
		    $select_method = $k3->{list_values};
		} 
		elsif ( $k3->{data_type} eq 'int' || 
			$k3->{data_type} eq 'number' ) { 
		    $ui_type = 'number'; 
		}
		elsif ( $k3->{data_type} eq 'date' ) {
		    $ui_type = 'date';
		} 

		ProjectInfo::printFilterCondRow($k3->{display_name},
                                                $filter_name,
						$ui_type,
						$select_type,
						$select_method,
						getSessionParam($filter_name));
            } 
        }  # end for my k2 
    } # end for my tab 
    print "</table>\n"; 

 
    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:applyGoldProjectFilter" value="Apply Study Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print end_form();
} 


###########################################################################
# printFilterCond - print filter condition 
###########################################################################
sub printFilterCond { 
    my ($title, $param_name, $ui_type, $select_type, $select_method,
        $def_val_str) = @_; 

  ProjectInfo::printFilterCond($title, $param_name, $ui_type, $select_type,
			       $select_method, $def_val_str);
}


########################################################################## 
# ApplyGoldMetaProjectFilter - save filter info into database 
#                              and apply filter 
########################################################################## 
sub ApplyGoldMetaProjectFilter { 
    # only my projects? 
    my $only_my_project = param1('meta_proj_filter:only_my_project'); 
    if ( $only_my_project ) { 
        setSessionParam('meta_proj_filter:only_my_project', $only_my_project); 
    } 
 
    # max display 
    my $max_display = param1('meta_proj_filter:max_display'); 
    if ( ! blankStr($max_display) ) { 
        setSessionParam('meta_proj_filter:max_display', $max_display); 
    } 
 
    # show filter selection 
    my @all_params = ProjectInfo::projectFilterParams(); 
    for my $p0 ( @all_params ) { 
	# add meta to filter
	$p0 = "meta_" . $p0;
        if ( $p0 eq 'meta_proj_filter:add_date' || 
             $p0 eq 'meta_proj_filter:mod_date' ) { 
            # date 
            my $op_name = $p0 . ":op"; 
            my $op1 = param1($op_name); 
            my $d1 = param1($p0); 
            if ( !blankStr($d1) && !blankStr($op1) ) { 
                if ( ! isDate($d1) ) { 
                    print "<p>Incorrect Date (" . escapeHTML($d1) . 
                        ") -- Filter condition is ignored.</p>\n"; 
                } 
                else { 
                    setSessionParam($op_name, $op1); 
                    setSessionParam($p0, $d1); 
                } 
            } 
            else { 
                # clear condition 
                setSessionParam($op_name, ''); 
                setSessionParam($p0, ''); 
            } 

	    next;
        } 

        # op 
        my $op_name = $p0 . ":op"; 
        if ( param1($op_name) ) { 
            setSessionParam($op_name, param1($op_name));
        } 
	else {
	    # clear
	    setSessionParam($op_name, ''); 
	}
 
        # value 
        if ( param($p0) ) {
            # filter
            my @options = param($p0);
 
            my $s0 = "";
            for my $s1 ( @options ) {
                if ( blankStr($s0) ) { 
                    $s0 = $s1;
                } 
                else {
                    $s0 .= "," . $s1;
                }
            } 
            setSessionParam($p0, $s0); 
        } 
        else { 
            my ($tag1, $val1) = split(/\:/, $p0);
            setSessionParam($p0, ''); 
        } 
    }

    # clear browse params
    clearProjectPageInfo();

} 



1;



