###########################################################################
#
#
# $Id: Caliban.pm,v 1.5 2012-03-27 19:00:15 klchu Exp $
#
############################################################################
package Caliban;

use strict;
use CGI qw/:standard/;
use CGI::Session;    # for security - ken
use CGI::Cookie;
use HTML::Template;
use XML::Simple;
#use URI::Escape;
use Scalar::Util 'reftype';
use perl5lib;
use Data::Dumper;
use LWP;
use LWP::UserAgent;
use HTTP::Request;
use HTTP::Request::Common qw( GET POST PUT);
use JSON;
use Digest::SHA qw(sha256);
use MIME::Base64 qw( encode_base64 decode_base64 );
use DBI;

use WebEnv;
use WebFunctions;


use MIME::Base64::URLSafe;
use URL::Encode qw( url_encode url_encode_utf8);
use Mojo::JWT;
use Mojo::UserAgent;
use JSON::WebToken;

$| = 1;

my $env         = getEnv();
my $base_url    = $env->{base_url};
my $base_dir    = $env->{base_dir};
my $main_cgi    = $env->{main_cgi};
my $cgi_url     = $env->{cgi_url};
my $section     = "Caliban";
my $section_cgi = "$main_cgi?section=$section";

my $keycloak = $env->{keycloak};
my $redirUrl = $cgi_url . "/main.cgi?section=CalibanLoginReturn";


# sso Caliban
# cookie name: jgi_return, value: url, domain: jgi.doe.gov
my $sso_url                 = $env->{sso_url};
my $sso_domain              = $env->{sso_domain};
my $sso_cookie_name         = $env->{sso_cookie_name};           # jgi_return cookie name
my $sso_session_cookie_name = $env->{sso_session_cookie_name};
my $sso_api_url             = $env->{sso_api_url};
my $sso_user_info_url       = $env->{sso_user_info_url};
my $verbose                 = $env->{verbose};
my $allow_img_login         = $env->{allow_img_login};

my $user_guide_url = $env->{ user_guide_url }; 

#
# sso login form
#
sub printSsoForm {
    my ($tool) = @_;

    # default is submission site
    if ( ! $tool ) {
	$tool = 'submit';
    }

    if ($keycloak) {
        $sso_url = prepareKeyCloak();
    }    


print qq{
<div id='ssologin' style="margin-top: 60px; margin-left: 20px;">
};

##    print "<p><font color='red'>Due to system configuration problem, we are not able to accept new submissions at this moment. Sorry for the inconvenience.</font>\n";        

#    print "<h5><font color='red'>We are currently processing a very large batch of JGI metagenome submissions. Expect a long turnaround time (especially for non-JGI submissions). Thank you for your patience and sorry for the inconvenience.</font></h5>\n"; 
 
    Submission::printNewGoldMsg();
 
#    Submission::printHint3("IMG no longer accepts unassembled 454 reads submissions starting 1/1/2016.");

    Submission::printKbaseAssemblyMsg();
##    print "<p><font color='red'>Starting 2/11/2019 IMG will only accept fasta submissions.</font>\n";
##    print "<p><font color='red'>Note: We are transitioning to a new computer system. There will be substantial delay in data loading. Please do not send email asking when your submission will be loaded into IMG.</font>\n";

    my $url = "main.cgi?oldLogin=true";

    if ( $tool eq 'submit' ) {
	print "<h2>Welcome to IMG ER and IMG/M ER Submission Site. Please log in.</h2>\n"; 
    }
    elsif ( $tool eq 'gold' ) {
	$url = "gold.cgi?oldLogin=true";
	print "<h2>Welcome to IMG-GOLD. Please log in.</h2>\n"; 
    }

##    print "<font color='red'><b>LBNL/JGI Holiday Shutdown: 12/24/2019 - 1/1/2020. </b></font>\n";

    print "<h3>JGI Single Sign On (SSO)</h3>\n";

    if($keycloak) {
    print qq{
<form>
    <p>JGI Keycloak<br/>

<input class="smdefbutton" 
type="button" 
name="jgi_sso" 
value="JGI Keycloak Sign In" 
title="Sign in with your JGI SSO Account" 
onclick="window.location.href='$sso_url'">

};        
    } else {

    print qq{
<form action="$sso_url/signon/create" id="home" name="home" method="post">
    <p>JGI SSO Email: (or <a href="$sso_url/help#username">JGI SSO User Name</a>): <br/>
    <input id="login" name="login" type="text" size='30'/></p>
    <p>JGI SSO Password: <br/>
    <input id="password" name="password" type="password" size='30' />
<br/><br/>
    <input class="smdefbutton" name="commit" type="submit" value="JGI SSO Sign In" title="Sign in using JGI SSO account"/>
<br/><br/>
<span style="color: #B1B1B1; text-decoration: line-through;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
};

    }

    if ( $allow_img_login ) {
	print qq{
&nbsp;&nbsp; <span style="font-size:14px;"><b>OR</b></span> &nbsp;&nbsp;
<span style="color: #B1B1B1; text-decoration: line-through;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><br/>
<br/>
<span style="font-size:14px;">Sign in with:</span> <br/> 
    <input class="smbutton" type="button" onclick="javascript:window.open('$url', '_self');" value="IMG Account" title="Go back to use old IMG login page"/>
    &nbsp; Go back to use IMG login page.
};
    }

    print "</form>\n";

#    my $url_link = alink( $url, "Sign in with IMG Account" );

    # TODO request form updated to update sso too
    # re-direct account request to submission site
#    print qq{
#    <br/>
#    <p>
#    <a class="formlink" href="$sso_url/password_resets">Forgot your password?</a>
#    &nbsp;&nbsp; | &nbsp;&nbsp;
#    <a class="formlink" href="$sso_url/help">Help</a>
#    &nbsp;&nbsp; |&nbsp;&nbsp; 
#    $url_link
#    </p>

#    <p>If you are not a registered user,
#    <a href="$sso_url/register"> request an account here</a>.
#    </p>
    
#    <p>
#    <b>
#    Requirements for using JGI Single Sign On:
#    </b>
#    <p>
#    <ul>
#    <li>JavaScript enabled.</li>
#    <li>Accept cookies from domain *.jgi.doe.gov and *.jgi-psf.org.</li>
#    <li>IE users you must allow 3rd party cookies and accept both https and http content.</li>
#    </ul>
#    </p>
#    };

    if ( $tool ne 'submit' ) {
	print "</div>";
	return;
    }

    print hr;
    print "<h2>For New Users</h2>\n"; 
    my $img_url = getImgMIUrl();
    print qq{ 
        In order to submit a dataset to IMG ER or IMG/M ER, you need to: 
        <ol> 
            <li>Go to <a href='$img_url' target='view_link'>IMG/M ER</a>
            to fill out an IMG account request and get yourself
            familiar with IMG functionalities first. </li>
            <li>Prepare files for submission (for details see 
           <a href='$user_guide_url' target='view_link'> 
              User Guide</a>).</li> 
            </ol> 
        }; 

    return;

    print qq{ 
        <h5>For IMG ER:</h5>
 
            <ol>
            <li>Find or define in IMG-GOLD the genome project to be associated
            with the submitted dataset (for details see
           <a href='$user_guide_url' target='view_link'> 
              User Guide</a>);</li>
            <li>Submit the genome dataset and provide data processing requirements,
                including gene prediction, EC prediction, product name assignment
                (for details see 
           <a href='$user_guide_url' target='view_link'>
              User Guide</a>);</li>
            <li>Check the status of your submission.</li>
            </ol> 
        };
 
    print qq{
 
        <h5>For IMG/M ER:</h5>
 
        <ol> 
            <li>Find or define in IMG-GOLD the metagenome project and
            sample to be associated with the submitted dataset
            (for details see 
           <a href='$user_guide_url' target='view_link'> 
              User Guide</a>);</li> 
            <li>Submit the metagenome dataset and provide data processing
            requirements, including gene prediction, EC prediction,
            product name assignment (for details see
           <a href='$user_guide_url' target='view_link'> 
              User Guide</a>);</li>
            <li>Check the status of your submission.</li>
            </ol>
        }; 

    print "</div>";
}

#
# once login in sso call this to setup cgi session
sub validateUser {
    my ($dbh) = @_;

    my %cookies = CGI::Cookie->fetch;

    #webLog "here 1 <br/>\n";
    return 0 if ( !exists $cookies{$sso_session_cookie_name} );
    my $id = $cookies{$sso_session_cookie_name}->value;

    #webLog "here 2 <br/>\n";
    return 0 if ( $id eq "" );

    #webLog "here 3 <br/>\n";

    # the cookie has the sub path in id
    # https://signon.jgi-psf.org/api/sessions/e29a0ea6ac80f6b8
    # see cookie jgi_session set by jgi sso
    my $url = $sso_url . $id;

    #webLog("here 4 $url\n");
    my $ua   = new LWP::UserAgent();
    my $req  = GET($url);
    my $res  = $ua->request($req);
    my $code = $res->code;

    #webLog("here 5 $code\n");

    if ( $code eq "200" ) {
        my $content = $res->content;

        my $href    = XMLin($content);
        my @tmp     = split( /\//, $href->{location} );
        my $sid     = $tmp[$#tmp];
        my @tmp     = split( /\//, $href->{user} );
        my $user_id = $tmp[$#tmp];

        #   print header(-type => "text/plain" );
        #   print Dumper $href;
        #   $VAR1 = {
        #          'location' => '/api/sessions/6f3ba675647c9395',
        #          'ip' => '198.129.96.57',
        #          'user' => '/api/users/3701'
        #        };
        #  exit 0;

        #        $content =~ /<location>\/api\/sessions\/(\w+)<\/location>/;
        #        my $sid = $1;
        #        $content =~ /<user>\/api\/users\/(\d+)<\/user>/;
        #        my $user_id = $1;

        my ( $contact_oid, $username, $super_user ) = getContactOid( $dbh, $user_id );
        my ( $ans, $login, $email, $userData_href ) = getUserInfo($user_id);

        if ( $ans == 1 && $contact_oid eq "" ) {
            $login = CGI::unescape($login);
            $email = CGI::unescape($email);

            my $emailExists = emailExist( $dbh, $email );
            if ($emailExists) {

                # update user's old img account with caliban data
                updateUser( $login, $email, $user_id );
            } else {

                # user is an old jgi sso user, data not in img's contact table
                # insertUser( $login, $email, $user_id, $userData_href );

		return 0;
            }
            ( $contact_oid, $username, $super_user ) = getContactOid( $dbh, $user_id );
        }

        return 0 if ( $contact_oid eq "" || $contact_oid eq "0" );

        setSessionParam( "contact_oid",    $contact_oid );
        setSessionParam( "super_user",     $super_user );
        setSessionParam( "username",       $username );
        setSessionParam( "jgi_session_id", $sid );
        return 1;
    }
    return 0;
}

sub emailExist {
    my ( $dbh, $email ) = @_;
    my $sql = qq{
        select contact_oid
        from contact
        where email = ?
        };
    my $cur = execSql( $dbh, $sql, 1, $email );
    my ($id) = $cur->fetchrow();
    if ($id) {
        return 1;
    }
    return 0;
}

sub updateUser {
    my ( $login, $email, $caliban_id ) = @_;
    my $dbh = Connect_IMG_Contact();
    $dbh->{AutoCommit} = 0;
    $dbh->{RaiseError} = 1;

    # update contact table
    my $sql = qq{
        update contact 
        set caliban_id = ? , 
        caliban_user_name = ?
        where email = ?
    };
    my $cur = $dbh->prepare($sql) or webError("cannot preparse statement: $DBI::errstr");
    my $i       = 1;
    $cur->bind_param( $i++, $caliban_id ) or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $login )      or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $email )      or webError("$i-1 cannot bind param: $DBI::errstr\n");

    $cur->execute() or webError("cannot execute: $DBI::errstr\n");
    $dbh->commit;
    $dbh->disconnect();
}

sub insertUser {
    my ( $login, $email, $caliban_id, $userData_href ) = @_;
    my $dbh = Connect_IMG_Contact();
    $dbh->{AutoCommit} = 0;
    $dbh->{RaiseError} = 1;

    #        my %userData = (
    #                         'username'     => $href->{'login'},
    #                         'email'        => $href->{'email'},
    #                         'name'         => $href->{'first_name'} . ' ' . $href->{'last_name'},
    #                         'phone'        => $href->{'phone_number'},
    #                         'organization' => $href->{'institution'},
    #                         'address'      => $href->{'address_1'},
    #                         'state'        => $href->{'state'},
    #                         'country'      => $href->{'country'},
    #                         'city'         => $href->{'city'},
    #                         'title'        => $href->{'prefix'},
    #                         'department'   => $href->{'department'},
    #        );
    my $name         = $userData_href->{'name'};
    my $reftype = reftype $name;
    $name = '' if ($reftype eq 'HASH');
    
    my $phone        = $userData_href->{'phone'};
    $reftype = reftype $phone;
    $phone = '' if ($reftype eq 'HASH');
    
    my $organization = $userData_href->{'organization'};
    $reftype = reftype $organization;
    $organization = '' if ($reftype eq 'HASH');

    my $address      = $userData_href->{'address'};
    $reftype = reftype $address;
    $address = '' if ($reftype eq 'HASH');

    my $state        = $userData_href->{'state'};
    $reftype = reftype $state;
    $state = '' if ($reftype eq 'HASH');

    my $country      = $userData_href->{'country'};
    $reftype = reftype $country;
    $country = '' if ($reftype eq 'HASH');
    
    my $city         = $userData_href->{'city'};
    $reftype = reftype $city;
    $city = '' if ($reftype eq 'HASH');

    my $title        = $userData_href->{'title'};
    $reftype = reftype $title;
    $title = '' if ($reftype eq 'HASH');

    my $department   = $userData_href->{'department'};
    $reftype = reftype $department;
    $department = '' if ($reftype eq 'HASH');

    # get max contact_oid
    my $sql = qq{
        select max(contact_oid)
        from contact
    };
    my $cur = execSql( $dbh, $sql, 1 );
    my ($contact_oid_max) = $cur->fetchrow();
    $contact_oid_max = $contact_oid_max + 1;

    # insert into contact table
    my $sql = qq{
        insert into contact 
        (contact_oid, username, password, email, comments,
         add_date, 
        caliban_id, caliban_user_name, super_user,
        name, phone, organization, address, state, country, city, title, department
        )
        values
        (?, ?, ?, ?, ?, 
         sysdate, 
         ?, ?, 'No',
        ?,?,?, ?,?,?, ?,?,?)
    };
    my $cur = $dbh->prepare($sql) or webError("cannot preparse statement: $DBI::errstr");

    my $comment = 'user created via caliban img submit';
    my $i       = 1;
    $cur->bind_param( $i++, $contact_oid_max ) or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $login )           or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, 'no_password!!' )  or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $email )           or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $comment )         or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $caliban_id )      or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $login )           or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $name )            or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $phone )           or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $organization )    or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $address )         or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $state )           or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $country )         or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $city )            or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $title )           or webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $department )      or webError("$i-1 cannot bind param: $DBI::errstr\n");

    $cur->execute() or webError("cannot execute: $DBI::errstr\n");
    $dbh->commit;
    $dbh->disconnect();

}

#
# get user img contact oid via caliban_id
#
sub getContactOid {
    my ( $dbh, $user_id ) = @_;

    my $sql = qq{
      select contact_oid, username, super_user
      from contact
      where caliban_id = ?
   };

    my $cur = execSql( $dbh, $sql, 1, $user_id );

    my ( $contact_oid, $username, $super_user ) = $cur->fetchrow();
    return ( $contact_oid, $username, $super_user );
}

#
# is a session valid and do a "touch" too
#
sub isValidSession {
    my $sid = getSessionParam("jgi_session_id");

    #webLog("sid === $sid\n");
    return 0 if ( $sid eq "" || $sid eq 0 );

    my $url = $sso_api_url . $sid;

    #webLog("$url \n");

    my $ua = new LWP::UserAgent();

    #my $req = GET($url);
    my $req = PUT($url);            # does a touch
    my $res = $ua->request($req);

    my $code = $res->code;

    #webLog("code: $code\n");

    # 200 - OK
    # 204 - ok but no content
    # 410 - Gone
    if ( $code eq "200" || $code eq "204" ) {
        return 1;
    } else {
        return 0;
    }
}

#  we need to touch the session to keep active
#
# /jgi/tools/bin/curl -X PUT -d "" -D headers.txt https://signon.jgi-psf.org/api/sessions/e8b6ef108302e1e4
# ; cat headers.txt
#
# exec("/jgi/tools/bin/curl -i -X PUT https://signon.jgi-psf.org/api/sessions/$sid");
#sub touch {
#    my($sid) = @_;
#
#    webLog("running: /jgi/tools/bin/curl -i -X PUT $sso_url/api/sessions/$sid \n");
#
#    exec("/jgi/tools/bin/curl -X PUT $sso_url/api/sessions/$sid");
#}

sub logout {
    my ($sso_logout) = @_;

    setSessionParam( "blank_taxon_filter_oid_str", "1" );
    setSessionParam( "contact_oid",                "" );
    #setTaxonSelections("");
    setSessionParam( "jgi_session_id", "" );
    setSessionParam( "oldLogin",       "" );

    # http://blog.unmaskparasites.com/2009/10/28/evolution-of-hidden-iframes/
    
    setSessionParam( "jgi_session_id", "" );
    setSessionParam( "oldLogin",       "" );
    setSessionParam("last_jgisso_ping", 0);    
    
    # clear cgi session
    my $session = getSession();
    $session->delete();
    $session->flush(); 
    
   if ($keycloak) {
        print <<EOF;
    <script language='JavaScript' type='text/javascript'>
    document.cookie ='img_kc=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    document.cookie ='CGISESSID_KC=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    </script>
EOF


        # logout of keycloak
        logoutCallback();
    
    } elsif ($sso_logout) {
        print <<EOF;
    <script language='JavaScript' type='text/javascript'>
    document.cookie='jgi_return=$base_url; domain=$sso_domain; path=/;';  
    //window.open('$sso_url/signon/destroy', '_blank');
    </script>
    
    <iframe height="0" width="0" style="visibility: hidden" src="$sso_url/signon/destroy"></iframe>    
EOF
    }
}

#
# get caliban user info id
#
sub getUserInfo {
    my ($id) = @_;
    my $url = $sso_url .  '/api/users/' . $id;
    if ( $sso_user_info_url ne '' ) {
        $url = $sso_user_info_url . $id;
    }

    my $ua   = new LWP::UserAgent();
    my $req  = GET($url);
    my $res  = $ua->request($req);
    my $code = $res->code;
    if ( $code eq "200" ) {
        my $content = $res->content;

        my $href = XMLin($content);

        my $login = $href->{login};
        my $email = $href->{email};

        my %userData = (
                         'username'     => CGI::unescape( $href->{'login'} ),
                         'email'        => CGI::unescape( $href->{'email'} ),
                         'name'         => CGI::unescape( $href->{'first_name'} ) . ' ' . CGI::unescape( $href->{'last_name'} ),
                         'phone'        => CGI::unescape( $href->{'phone_number'}),
                         'organization' => CGI::unescape( $href->{'institution'} ),
                         'address'      => CGI::unescape( $href->{'address_1'} ),
                         'state'        => CGI::unescape( $href->{'state'} ),
                         'country'      => CGI::unescape( $href->{'country'} ),
                         'city'         => CGI::unescape( $href->{'city'} ),
                         'title'        => CGI::unescape( $href->{'prefix'} ),
                         'department'   => CGI::unescape( $href->{'department'} ),
        );

        return ( 1, $login, $email, \%userData );
    } else {

        # failed
        return ( 0, '', '', '' );
    }
}


sub execSql {
    my ( $dbh, $sql, $verbose, @args ) = @_;
 
    my $nArgs = @args;
    if ( $nArgs > 0 ) {
        my $s;
        for ( my $i = 0 ; $i < $nArgs ; $i++ ) {
            my $a = $args[$i];
            $s .= "arg[$i] '$a'\n";
        }
        
    }
    my $cur = $dbh->prepare($sql)
      or die("execSql: cannot preparse statement: $DBI::errstr\n");
    $cur->execute(@args)
      or die("execSql: cannot execute: $DBI::errstr\n");
    return $cur;
}

# ================== keycloak ====================

#
# get 64 char string
#
sub randomStr64 {
    
    my $code_verifier = getSessionParam('code_verifier');
    if($code_verifier) {
        return $code_verifier;
    }
    
    
    my $r = join '',
      map +( 0 .. 9, 'a' .. 'z', 'A' .. 'Z' )[ rand( 10 + 26 * 2 ) ], 1 .. 64;
    return $r;
}

sub getKeyCloakProp {
    my $file    = "/webfs/keycloak/prop.json";
    my $content = file2Str($file);
    my $href    = decode_json($content);
    return $href;
}

# returns the login url
sub prepareKeyCloak {

    # read the json secret file now
    my $href          = getKeyCloakProp();
    my $scope         = url_encode( $href->{'scope'} );
    my $client_id     = $href->{'client_id'};
    my $client_secret = $href->{'client_secret'};

    my $login_url = $href->{'login_url'};

    my $sid            = getSessionId();
    my $code_verifier  = randomStr64();
    my $code_challenge = urlsafe_b64encode( sha256($code_verifier) );

    setSessionParam( 'code_challenge', $code_challenge );
    setSessionParam( 'code_verifier',  $code_verifier );
    setSessionParam( 'scope',          $scope );
    setSessionParam( 'client_id',      $client_id );
    setSessionParam( 'client_secret',  $client_secret );

    # link to login page
    my $url =
        $login_url
      . "?client_id=$client_id"
      . "&redirect_uri=$redirUrl"
      . "&state=$sid"
      . "&response_type=code"
      . "&scope=$scope"
      . "&code_challenge=$code_challenge"
      . "&code_challenge_method=S256";

    return $url;
}



# return from the keycloak login page

=cmnt
STEP 2

A successful login. 
It will return here because the login url have a redirect param

Ideally, this page will not be shown, it's a debugging step.

Key take aways:
1 - 'redirect_uri' => $redirUrl is the same return url as the original - why?


setup session params like validateUser

=cut

sub verifyKeyCloak {
    my $code          = param('code');
    my $session_state = param('session_state');

    my $href2      = getKeyCloakProp();
    my $token_url = $href2->{'token_url'};
    my $jwks_url  = $href2->{'jwks_url'};

    setSessionParam( 'code',          $code );
    setSessionParam( 'session_state', $session_state );

    my $client_id     = getSessionParam('client_id');
    my $client_secret = getSessionParam('client_secret');
    my $code_verifier = getSessionParam('code_verifier');

    #    debug(qq{
    #      Back from new SSO server
    #      Lets try to verify token
    #      POST url: $token_url
    #      POST Data:
    #     'client_id' => $client_id,
    #     'redirect_uri' => $redirUrl,
    #     'grant_type' => 'authorization_code',
    #     'client_secret' => $client_secret,
    #     'code' => $code,
    #     'code_verifier' => $code_verifier
    #    });

    # token url
    my $jwt_object = getSessionParam('jwt_object') // '';
    my $href;

    if ( !$jwt_object ) {

        my $ua = new LWP::UserAgent();
        $ua->ssl_opts( verify_hostname => 0, SSL_verify_mode => 0x00 );
        my $res = $ua->post(
                             $token_url,
                             {
                               'client_id'     => $client_id,
                               'redirect_uri'  => $redirUrl,
                               'grant_type'    => 'authorization_code',
                               'client_secret' => $client_secret,
                               'code'          => $code,
                               'code_verifier' => $code_verifier
                             }
        );

        my $htmlcode = $res->code;
        my $content  = $res->content;
        $href = decode_json $content;

        #    my $tmp =  Dumper $href;
        #    debug(qq{
        #        Message from get request:
        #        html code = $htmlcode
        #        return json content =
        #        $tmp
        #    });

        if ( $htmlcode > 250 || $content =~ /error/i ) {
            webLog("Error: $htmlcode keycloak getting token \n <br> $content \n");
            
            # no exit
            webErrorHeader("Error: $htmlcode keycloak getting token !<br> $content <br>", 1);
            
            logout();
            exit 0;
        }

        setSessionParam( 'jwt_object', $href );

    } else {
        $href = $jwt_object;
    }

    my $access_token = $href->{'access_token'};
    my $id_token     = $href->{'id_token'};
    my $jwks = Mojo::UserAgent->new->get($jwks_url)->result->json('/keys');
    my $jwt  = Mojo::JWT->new( jwks => $jwks );

    # user info
    my $x = $jwt->decode($id_token);
    my $y = $jwt->decode($access_token);

    my $keycloak_id       = $y->{'sub'};
    my $legacy_caliban_id = $y->{'legacy_caliban_id'};  # this can be null
    my $email             = $y->{'email'};
    my $last_name         = $y->{'family_name'};
    my $first_name        = $y->{'given_name'};
    my $name              = $y->{'name'};               # this is first and last
    my $expired_time      = $y->{'exp'};                # session expired time
    my $login_time        = $y->{'auth_time'};

    my $dbh = Connect_IMG_Contact();
    my (
        $contact_oid, $username, $super_user, $name2,
        $email2,      $jgi_user, $img_editor, $opt_in,
        $caliban_id,  $keycloak_id2

    );
    if ($legacy_caliban_id) {
        (
           $contact_oid, $username, $super_user, $name2,
           $email2,      $jgi_user, $img_editor, $opt_in,
           $caliban_id,  $keycloak_id2
        ) = getContactOidDb( $dbh, $legacy_caliban_id );

        # TODO if no $keycloak_id - it was not mapped
        if ( !$keycloak_id2 ) {

webLog("No keycloak id in img db \n");

                my $gold_dbh = $dbh; #dbGoldLogin();
                $gold_dbh->{AutoCommit} = 0;
                $gold_dbh->{RaiseError} = 1;

            # update row
            my $sql = qq{
                update contact
                set keycloak_id = ? 
                where contact_oid = ?
            };
            
            webLog("$sql\n $contact_oid \n $keycloak_id \n");
            
            my $cur = $gold_dbh->prepare($sql)
              or webErrorHeader("kc cannot preparse statement: $DBI::errstr");
            my $i = 1;
            $cur->bind_param( $i++, $keycloak_id )
              or webErrorHeader("kc $i-1 cannot bind param: $DBI::errstr\n");
            $cur->bind_param( $i++, $contact_oid )
              or webErrorHeader("kc $i-1 cannot bind param: $DBI::errstr\n");

            $cur->execute()
              or webErrorHeader("kc cannot execute: $DBI::errstr\n");
            $gold_dbh->commit;
        }

    } elsif ($keycloak_id) {

        # TODO - user with no caliban id
        # this could a be a new user? - ken
webLog("user has keycloak id\n");

        (
           $contact_oid, $username, $super_user, $name2,
           $email2,      $jgi_user, $img_editor, $opt_in,
           $caliban_id,  $keycloak_id2

        ) = getContactOidKeycloak( $dbh, $keycloak_id );
        
#        if(!$contact_oid) {
#            # this is a NEW user to IMG
#            
#        }
        
        
    } else {

        # TODO - no keycloak id. Is this possible?
        # for now I'm just catching the error and exit 
        # - ken
        webErrorHeader( "Error: no keycloak id" );
        exit -1;
    }

    if ( !$contact_oid ) {
        # TODO - no id in img
        # Is it a new user?
        # I'll have to create the workspace and contact oid?
        
        # TODO create new form to ask for additional info
        # I have to get country and stuff from the new Charon db
        
        #webErrorHeader( "Error: new user???" );


        # new user - ?
        $contact_oid = insertUseKeyCloak($name, $email ,$keycloak_id);
    }


    setSessionParam( "contact_oid",       $contact_oid );
    setSessionParam( "super_user",        $super_user );
    setSessionParam( "username",          $username );
    setSessionParam( "jgi_session_id",    $keycloak_id );
    setSessionParam( "kc_id",    $keycloak_id );
    setSessionParam( "name",              $name );
    setSessionParam( "email",             $email );
    setSessionParam( "caliban_id",        $legacy_caliban_id );
    setSessionParam( "caliban_user_name", 'n/a' );
    setSessionParam( "jgi_user",          $jgi_user );
    setSessionParam( "img_editor",        $img_editor );
    setSessionParam( "opt_in",            $opt_in );

    if ( $img_editor eq "Yes" ) {
        setSessionParam( "editor", 1 );
    } else {
        setSessionParam( "editor", 0 );
    }
}

sub insertUseKeyCloak {
    my ( $name, $email, $keycloak_id ) = @_;
    $email = lc($email);
    my $dbh = WebUtil::dbGoldLogin();
    $dbh->{AutoCommit} = 0;
    $dbh->{RaiseError} = 1;

    # get max contact_oid
    my $sql = qq{
        select max(contact_oid)
        from contact
    };
    my $cur = execSql( $dbh, $sql, $verbose );
    my ($contact_oid_max) = $cur->fetchrow();
    $contact_oid_max = $contact_oid_max + 1;

    # insert into contact table
    $sql = qq{
        insert into contact
        (contact_oid, 
        name, email, comments, 
        add_date, SUPER_USER, 
        KEYCLOAK_ID 
        )
        values
        ($contact_oid_max, 
        ?, ?, 'user created via keycloak img', 
        sysdate, 'No',  
        ?)
    };
    $cur = $dbh->prepare($sql)
      or WebUtil::webError("cannot preparse statement: $DBI::errstr");

    
    my $i       = 1;
    $cur->bind_param( $i++, $name )
      or WebUtil::webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $email )
      or WebUtil::webError("$i-1 cannot bind param: $DBI::errstr\n");
    $cur->bind_param( $i++, $keycloak_id )
      or WebUtil::webError("$i-1 cannot bind param: $DBI::errstr\n");


    $cur->execute() or WebUtil::webError("cannot execute: $DBI::errstr\n");
    $dbh->commit;
    
    return $contact_oid_max;
}


sub debug {
    my ($text) = @_;
    print qq{
      <pre>
DEBUG:
$text
      </pre>
    };

}

#
# TODO logout call back
#
sub logoutCallback {
    my $href       = getKeyCloakProp();
    my $logout_url = $href->{'logout_url'};

    # window.location.href = "http://www.w3schools.com";
    # dynamically create redirect url - ken

    my $url = $logout_url;
    $url .= "?redirect_uri=$base_url";

    print qq{
       <script>
       
       window.location.replace("$url");
       
       </script> 
    };

    #clearSession();
}

#
# prints keyclock logout waiting screen
#
sub printKeycloakLogoutMsg {
    print qq{
            <img src="/images/loading-large.gif" alt="Logging out...">
            <br>
            <div id='message'>
            <p>Please wait...logging out.
            </div>
    };
}

#
# keycloak login
# use jgi return cookie
# to figure which page to go to
# otherwise go to home page
#
sub printKeycloakLoginMsg {
    
    my $urlTag = $env->{ urlTag };
    
    my $value = cookie('jgi_return');
    if ( !$value ) {
        $value = $base_url;
    
    } elsif($value =~ /$urlTag/) {
        # the return cookie and base user is teh same site
        # do nothing
    } else {
        # the return cookie url tag and urlTag is not the same
        # use the base_url
        $value = $base_url;
    }

    print qq{
        <p>
        <img src="/images/loading-large.gif" alt="Login successful redirecting">
        <br>
        Logging in...    
            
       <script>
       window.location.replace("$value");
       
       </script> 
        };
}

#
# the Portal API is settting this cookie
#
sub getKc_session {
    my %cookies = CGI::Cookie->fetch;
    
    if(!$cookies{'kc_session'}) {
        return '';
    }
    
    my $refresh_token = $cookies{'kc_session'}->value;
    return $refresh_token;
}

#
# img kc cookie
#
sub getImg_kc {
    my %cookies = CGI::Cookie->fetch;
    
    if(!$cookies{'img_kc'}) {
        return '';
    }
    
    my $value = $cookies{'img_kc'}->value;
    return $value;
}


#
# redirect the keycloak login  page via javascript
#
sub keycloalRedirect {
    my $url  = prepareKeyCloak();
    
    
    #print $url;
    
    #sleep(10);
    
    # this does not with keycloak
    # window.location.href = "http://mywebsite.com/home.html";

    print <<EOF;
<script>
window.open('$url', '_self');
</script>

EOF
}

sub keyCloakRefresh {
    my $href      = getKeyCloakProp();
    my $token_url = $href->{'token_url'};    
    
    
    my $jwt_object = getSessionParam('jwt_object') // '';
    
    #print "Refresh: JWT object <pre>";
    #print Dumper $href;
    
    my $access_token = $jwt_object->{'access_token'};
    my $id_token = $jwt_object->{'id_token'};  
    my $refresh_token = $jwt_object->{'refresh_token'};  
    
    my $code_challenge = getSessionParam( 'code_challenge' );
    my $code_verifier = getSessionParam( 'code_verifier' );
    my $scope = getSessionParam( 'scope' );
    my $client_id = getSessionParam( 'client_id' );
    my $client_secret = getSessionParam( 'client_secret' );
    
    
    my $ua = new LWP::UserAgent();
    $ua->ssl_opts( verify_hostname => 0, SSL_verify_mode => 0x00 );

    my $res = $ua->post(
                         $token_url,
                         {
                           'client_id'     => $client_id,
                           'grant_type'    => 'refresh_token',
                           'client_secret' => $client_secret,
                           'refresh_token' => $refresh_token
                         }
    ); 
    
    my $htmlcode = $res->code;
    my $content  = $res->content;
    my $href = decode_json $content;

    my $tmp =  Dumper $href;
    print qq{
        <pre>
        KC refresh
        Message from get request:
        html code = $htmlcode
        return json content =
        $tmp
        </pre>
    };  
}

sub getContactOidDb {
    my ( $dbh, $user_id ) = @_;

    my $sql = qq{
      select contact_oid, username, super_user, name, email, jgi_user, img_editor,
      OPT_IN, caliban_id, keycloak_id
      from contact
      where caliban_id = ?
   };

    my $cur = execSql( $dbh, $sql, 1, $user_id );

    my (
         $contact_oid, $username,   $super_user, $name,       $email,
         $jgi_user,    $img_editor, $opt_in,     $caliban_id, $keycloak_id
    ) = $cur->fetchrow();
    return (
             $contact_oid, $username,   $super_user, $name,       $email,
             $jgi_user,    $img_editor, $opt_in,     $caliban_id, $keycloak_id
    );
}

sub getContactOidKeycloak {
    my ( $dbh, $keycloak_id ) = @_;

    my $sql = qq{
      select contact_oid, username, super_user, name, email, jgi_user, img_editor,
      OPT_IN, caliban_id, keycloak_id
      from contact
      where keycloak_id = ?
   };

    my $cur = execSql( $dbh, $sql, $verbose, $keycloak_id );

    my (
         $contact_oid, $username,   $super_user, $name,       $email,
         $jgi_user,    $img_editor, $opt_in,     $caliban_id, $keycloak_id
    ) = $cur->fetchrow();
    return (
             $contact_oid, $username,   $super_user, $name,       $email,
             $jgi_user,    $img_editor, $opt_in,     $caliban_id, $keycloak_id
    );
}

1;
