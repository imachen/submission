package ProjStats;
my $section = "ProjStats";

use strict;
use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';
use FileHandle;
use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;
use ChartUtil;


my $env = getEnv();
my $main_cgi             = $env->{main_cgi};
my $section_cgi          = "$main_cgi?section=$section";
my $base_url = $env->{ base_url };
my $tmp_url = $env->{ tmp_url };
my $tmp_dir = $env->{ tmp_dir };


#########################################################################
# dispatch
#########################################################################
sub dispatch {
    my ($page) = @_;
 
    if ( $page eq 'showSummary' ) {
        ShowStatsMain();
    }
    elsif ( $page eq 'printPieChart' ) {
	my $graph_fld = param1('graph_fld_option');
	my $include_null = param1('include_null');
	PrintPieChart($graph_fld, $include_null);
    }
    elsif ( $page eq 'displayPieChart' ) {
	my $graph_fld = param1('graph_fld_option');
	DisplayPieChartPage($graph_fld);
    }
    elsif ( $page eq 'displayBarChart' ) {
	my $graph_fld = param1('graph_fld_option');
	DisplayBarChartPage($graph_fld);
    }
    else {
        ShowStatsMain();
    }
}


#########################################################################
# ShowStatsMain
##########################################################################
sub ShowStatsMain {
    my $contact_oid = getContactOid();
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi");
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password");
    }

#    print "<p><font color='red'><b>WARNING: This page is currently under construction.</b></font></p>\n";

    if ( $contact_oid != 312 ) {
#	 $contact_oid != 11 &&
#	 $contact_oid != 17 ) {
	print "<p>\n";
	printHomeLink(); 
 
	print end_form();
	return;
    }

    my $genome_type = "Genome";
    my $stats_proj_type = param1('stats_proj_type');
    if ( $stats_proj_type ) {
	$genome_type = "Metagenome";
    }
    print "<h2>$genome_type Statistics</h2>\n";

    DisplayStatAttrList ();

    print qq{
	    <div id="graph-div" style="left: 170px; position: absolute; top: 76px;">
	};

    print "<p>\n";
    print "<p>" . nbsp(3) . "<b>Please select a field.</b></p>\n";

    print qq{
	</div>
	};

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();

    return;

    #### old version
    print "<p>Please select a category:</p>\n";

    print "<select name='graph_fld_option' class='img' size='1'>\n"; 
    print "    <option value=''> </option>\n"; 
    print "    <option value='body_sample_site'>Body Sample Site</option>\n"; 
    print "    <option value='cell_shape'>Cell Shape</option>\n"; 
    print "    <option value='color'>Color</option>\n"; 
    print "    <option value='funding_program'>Funding Program</option>\n"; 
    print "    <option value='habitat_category'>Habitat Category</option>\n"; 
    print "    <option value='motility'>Motility</option>\n"; 
    print "    <option value='ncbi_submit_status'>NCBI Submission Status</option>\n"; 
    print "    <option value='project_status'>Project Status</option>\n"; 
    print "    <option value='project_type'>Project Type</option>\n";
    print "    <option value='salinity'>Salinity</option>\n"; 
    print "    <option value='seq_country'>Sequencing Country</option>\n"; 
    print "    <option value='seq_status'>Sequencing Status</option>\n";
    print "    <option value='sporulation'>Sporulation</option>\n";
    print "    <option value='temp_range'>Temperature Range</option>\n";
    print "    <option value='type_strain'>Type Strain</option>\n";
    print "</select>\n"; 
    print "<p/>\n"; 

    print "<p>\n";
    print "<input type='checkbox' name='include_null' value='1' checked>";
    print nbsp(2);
    print "Include unclassified values?\n";
    print "</p>\n";

    print '<input type="submit" name="_section_ProjStats:printPieChart" value="Show Statistics" class="meddefbutton" />'; 


    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
} 


#########################################################################
# DisplayPieChartPage
##########################################################################
sub DisplayPieChartPage {
    my ( $graph_fld ) = @_;

    my $url=url(); 
 
#    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");

#    print "<p><font color='red'><b>WARNING: This page is currently under construction.</b></font></p>\n";

    my $genome_type = "Genome";
    my $stats_proj_type = param1('stats_proj_type');
    if ( $stats_proj_type ) {
	$genome_type = "Metagenome";
    }
    print "<h2>$genome_type Statistics</h2>\n";

    my $contact_oid = getContactOid();

#    if ( $contact_oid != 312 &&
#	 $contact_oid != 11 &&
#	 $contact_oid != 17 ) {
#	print "<p>\n";
#	printHomeLink(); 
# 
#	print end_form();
#	return;
#    }

    DisplayPieChartContent($graph_fld);
    print "<p>\n";

    # Home 
    print "<p>\n";
    printHomeLink(); 

    print end_form();
}


#########################################################################
# DisplayStatAttrList
#
# display attribute list section for selection
##########################################################################
sub DisplayStatAttrList {

    my @flds = getSingleStatsAttrs ();
    my $def_project = def_Project_Info();

    my $stats_proj_type = param1('stats_proj_type');
    my $new_section = "ProjStats";
    if ( $stats_proj_type ) {
	$new_section = "MetaProjStats";
    }

    print qq{
	<div >
	    <table class="img" width='140'>
	    <th bgcolor='lightblue'>Pie Chart</th>
	};

    for my $fld1 ( @flds ) {
	my $attr = $def_project->findAttr($fld1);
	print "<tr class='img' bgcolor='lightblue'>\n";
	my $url = "gold.cgi?section=" . $new_section .
	    "&page=displayPieChart&graph_fld_option=$fld1";
	if ( $stats_proj_type ) {
	    $url .= "&stats_proj_type=$stats_proj_type";
	}
	print "<td>";
	print "<a href='$url'>" . $attr->{display_name} .
	    "</a></td></tr>\n";
    }

    print "</table>\n";

    my $contact_oid = getContactOid();
    if ( $contact_oid != 312 ) {
	print "</div>\n";
	return;
    }

    print qq{
	</p>
	    <table class="img" width='140'>
	    <th bgcolor='#bbbbbb'>Bar Chart</th>
	};

    my @set_attrs = getSetStatsAttrs();

    for my $fld1 ( @set_attrs ) {
	print "<tr class='img' bgcolor='#bbbbbb'>\n";
	my $url = "gold.cgi?section=" . $new_section . 
	    "&page=displayBarChart&graph_fld_option=$fld1";
	if ( $stats_proj_type ) {
	    $url .= "&stats_proj_type=$stats_proj_type";
	}
	print "<td>";

	my $attr_disp_name = $fld1;
	if ( $fld1 eq 'seq_center' ) {
	    $attr_disp_name = "Sequencing Center";
	}
	else {
	    my $aux_name = getAuxTableName($fld1);
	    if ( ! blankStr($aux_name) ) {
		my $def_aux = def_Class($aux_name); 
		if ( $def_aux ) { 
		    $attr_disp_name = $def_aux->{display_name};
		}
	    }
	}

	print "<a href='$url'>" . $attr_disp_name .
	    "</a></td></tr>\n";
    }

    print "</table>\n";

    print "</div>\n";
}


#########################################################################
# DisplayPieChartContent
##########################################################################
sub DisplayPieChartContent {
    my ( $graph_fld ) = @_;

#    print "<p>\n";
#    print "<input type='checkbox' name='include_null' value='1'";
#    if ( $include_null ) {
#	print " checked ";
#    }
#    print ">";
#    print nbsp(1);
#    print "Include unclassified values?\n";
#    print "</p>\n";

    DisplayStatAttrList ();

    print qq{
	    <div id="graph-div" style="left: 170px; position: absolute; top: 76px;">
	};

    PrintPieChart($graph_fld, 0);

    print qq{
	</div>
	};


}


############################################################################ 
# getSingleStatsAttrs
############################################################################ 
sub getSingleStatsAttrs { 

    my @flds = ( 'body_sample_site', 'cell_shape', 'color',
		 'funding_program', 'habitat_category',
		 'motility', 'ncbi_submit_status', 'oxygen_req',
		 'project_status', 'project_type',
		 'salinity', 'seq_country',
		 'seq_status', 'sporulation',
		 'temp_range', 'type_strain' );

    return @flds;
}


############################################################################ 
# PrintPieChart
############################################################################ 
sub PrintPieChart { 
    my ($graph_fld, $include_null) = @_;

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi");

    if ( blankStr($graph_fld) ) {
	print "<p>" . nbsp(3) . "<b>Please select a field.</b></p>\n";
	return;
    }

    my $def_project = def_Project_Info();
    my $attr = $def_project->findAttr($graph_fld);
    if ( ! $attr ) {
	print "<h3>Incorrect attribute $graph_fld</h3>\n";
	return;
    }

    my $url2 = "gold.cgi?section=ProjSummary&page=showChartCategory";
 
    print "<h1>IMG-GOLD Categories: " . $attr->{display_name} .
	"</h1>\n"; 

    #### PREPARE THE PIECHART ###### 
    
    my $chart = newPieChart();
    $chart->WIDTH(300); 
    $chart->HEIGHT(300); 
    $chart->INCLUDE_LEGEND("no"); 
    $chart->INCLUDE_TOOLTIPS("yes"); 
    $chart->INCLUDE_URLS("yes"); 
    $chart->ITEM_URL($url2); 
    $chart->INCLUDE_SECTION_URLS("yes"); 
    $chart->URL_SECTION_NAME($graph_fld); 

    # file prefix
    my $fileprefix = "";
    my $p_count = 3000;
    my @flds = getSingleStatsAttrs();
    for my $f1 ( @flds ) {
	$p_count++;
	if ( $graph_fld eq $f1 ) {
	    $fileprefix = $p_count;
	    if ( ! $include_null ) {
		$fileprefix .= '0';
	    } 
	    else {
		$fileprefix .= '1';
	    }

	    last;
	}
    }
 
    if ( ! blankStr($fileprefix) ) {
	$chart->FILE_PREFIX( $fileprefix ); 
	$chart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix ); 
    }

#    print "<p>FILE_PREFIX: " . $chart->FILE_PREFIX . "</p>\n";

#    my $contact_oid = getContactOid();
#    if ( $contact_oid == 312 ) {
#	print "<p>FILEPATH_PREFIX: " . $chart->FILEPATH_PREFIX . "</p>\n";
#    }

    my @chartseries; 
    my @chartcategories; 
    my @functioncodes; 
    my @chartdata; 

   ################################# 

    my $stats_proj_type = param1('stats_proj_type');
    my $projcond = getProjCond('p', $stats_proj_type);

    my $dbh = Connect_IMG();

    # get categories
    my $sql = qq{
	select p.$graph_fld, count(*)
	    from project_info p
	    where $projcond
	    and p.$graph_fld is not null
	    group by p.$graph_fld
	};

#    print "<p>SQL: $sql</p>\n";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    print "<table width=800 border=0>\n";
    print "<tr>"; 
    print "<td valign=top>\n";
 
    print "<table class='img'  border='1'>\n";
    print "<th class='img' >" . $attr->{display_name} . "</th>\n";
    print "<th class='img' >Count</th>\n";
    print "<th class='img' >Percentage</th>\n";
    my $count = 0;
    my $total = 0;
    for( ;; ) { 
	my( $function_code, $f_count ) = $cur->fetchrow( );
	last if !$function_code;
	$count++; 

	if ( $count > 1000 ) {
	    last;
	}

	push @chartcategories, "$function_code";
	push @functioncodes, "$function_code";
	push @chartdata, $f_count;

	$total += $f_count;
    } 
    $cur->finish();

    # get unclassified
    my $sql0 = qq{
	select count(*)
	    from project_info p
	    where $projcond
	    and $graph_fld is null
	};
webLog("$sql0\n");
    $cur=$dbh->prepare($sql0);
    $cur->execute();
    my( $f_count0 ) = $cur->fetchrow( );
    $cur->finish();

    if ( $include_null ) {
	push @chartcategories, "Unclassified";
	push @functioncodes, "Unclassified";
	push @chartdata, $f_count0;
	$total += $f_count0;
    }

    push @chartseries, "count"; 
    $chart->SERIES_NAME(\@chartseries); 
    $chart->CATEGORY_NAME(\@chartcategories);
    $chart->URL_SECTION(\@functioncodes);
    my $datastr = join(",", @chartdata);
    my @datas = ($datastr);
    $chart->DATA(\@datas); 

    my $st = -1; 
    my $cmd = "";

#    print "<p>chart exe: " . $env->{ chart_exe } . "</p>\n";

    if ($env->{ chart_exe } ne "") {
	$st = generateChart($chart); 
    } 

    my $idx=0;
    for my $category1 ( @chartcategories ) {
	last if !$category1; 

        my $disp_key = $category1; 
        $disp_key =~ s/\s/\_/g; 
	my $url = "gold.cgi?section=ProjSummary&page=showCategory&cat_field=$graph_fld&cat_code=$disp_key";

	print "<tr class='img' >\n"; 
	print "<td class='img' >\n";
 
	if ($st == 0) {
	    print "<a href='$url'>"; 
	    my $fname = $chart->FILE_PREFIX . "-color-" . $idx .
		".png";
	    print "<img src='$tmp_url/". $fname . "' border=0>"; 
	    print "</a>";
	    print "&nbsp;&nbsp;";
	} 
	print escapeHTML( $category1 );
	print "</td>\n"; 
	print "<td class='img' align='right'>\n";
	if ( $chartdata[$idx] ) {
	    print alink( $url, $chartdata[$idx] );
	}
	else {
	    print "0";
	}

	print "</td>\n"; 

	# percentage
	if ( $total > 0 ) {
	    if ( $chartdata[$idx] ) {
		my $perc = int(($chartdata[$idx] * 100 / $total) + .5);
		print "<td class='img' align='right'>" . $perc . "%</td>\n";
	    }
	    else {
		print "<td class='img' align='right'>0%</td>\n";
	    }
	}
	else {
	    print "<td class='img' align='right'>-</td>\n";
	}

	print "</tr>\n"; 
	$idx++;
    }
 
    print "</table>\n"; 
    print "</td>\n"; 
    print "<td valign=top align=left>\n";
 
   ###########################
    if ($env->{ chart_exe } ne "") {
	if ( $st == 0 ) {
	    print "<script src='$base_url/overlib.js'></script>\n";
	    my $filename = $chart->FILEPATH_PREFIX . ".html";
	    my $fh = newReadFileHandle($filename, "printPieChart",1); 
#	    print "<p>File: $filename</p>\n";
	    if ( $fh ) {
		while (my $s = $fh->getline()) {
		    print $s;
		} 
		close ($fh); 
	    }

	    my $pngname = $chart->FILE_PREFIX . ".png";
	    print "<img src='$tmp_url/" . $pngname . "' BORDER=0 ";
	    print " width=".$chart->WIDTH." HEIGHT=".$chart->HEIGHT;
	    print " USEMAP='#" . $chart->FILE_PREFIX . "'>\n";
	} 
    } 
   ###########################  

    $dbh->disconnect( ); 

    print "<p>\n";
 
    print "</td></tr>\n"; 
    print "</table>\n";

    if ( ! $include_null ) {
	if ( $f_count0 ) {
	    my $url = "gold.cgi?section=ProjSummary&page=showCategory&cat_field=$graph_fld&cat_code=Unclassified";
	    print "<p>Unclassified: <a href='$url'>$f_count0</a></p>\n";
	}
	else {
	    print "<p>Unclassified: $f_count0</p>\n";
	}
    }

    my $proj_total_cnt = $total + $f_count0;
    print "<p>Total: $proj_total_cnt</p>\n";
} 


#########################################################################
# DisplayBarChartPage
##########################################################################
sub DisplayBarChartPage {
    my ( $graph_fld ) = @_;

    my $url=url(); 
 
#    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");

#    print "<p><font color='red'><b>WARNING: This page is currently under construction.</b></font></p>\n";

    my $genome_type = "Genome";
    my $stats_proj_type = param1('stats_proj_type');
    if ( $stats_proj_type ) {
	$genome_type = "Metagenome";
    }
    print "<h2>$genome_type Statistics</h2>\n";

    my $contact_oid = getContactOid();

    if ( $contact_oid != 312 &&
	 $contact_oid != 11 &&
	 $contact_oid != 17 ) {
	print "<p>\n";
	printHomeLink(); 
 
	print end_form();
	return;
    }

    DisplayBarChartContent($graph_fld);
    print "<p>\n";

    # Home 
    print "<p>\n";
    printHomeLink(); 

    print end_form();
}


#########################################################################
# DisplayBarChartContent
##########################################################################
sub DisplayBarChartContent {
    my ( $graph_fld ) = @_;

#    print "<p>\n";
#    print "<input type='checkbox' name='include_null' value='1'";
#    if ( $include_null ) {
#	print " checked ";
#    }
#    print ">";
#    print nbsp(1);
#    print "Include unclassified values?\n";
#    print "</p>\n";

    DisplayStatAttrList ();

    print qq{
	    <div id="graph-div" style="left: 170px; position: absolute; top: 76px;">
	};

    PrintBarChart($graph_fld, 0);

    print qq{
	</div>
	};


}


############################################################################ 
# getSetStatsAttrs
############################################################################ 
sub getSetStatsAttrs { 

    my @flds = ( 'cell_arrangement',
		 'diseases', 'energy_source',
		 'habitat',
		 'metabolism', 'phenotypes',
		 'project_relevance',
		 'seq_center', 'seq_method' );

    return @flds;
}


############################################################################ 
# PrintBarChart
############################################################################ 
sub PrintBarChart { 
    my ($graph_fld, $include_null) = @_;

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi");

    if ( blankStr($graph_fld) ) {
	print "<p>" . nbsp(3) . "<b>Please select a field.</b></p>\n";
	return;
    }

    my $aux_name = getAuxTableName($graph_fld);
    if ( $graph_fld eq 'seq_center' ) {
	$aux_name = 'project_info_data_links';
    }

    if ( blankStr($aux_name) ) {
	print "<h3>No table for $graph_fld</h3>\n";
	return;
    }

    my $def_aux = def_Class($aux_name); 
    if ( ! $def_aux ) { 
	print "<h3>Cannot find $$aux_name</h3>\n";
        return; 
    } 

    my $attr_disp_name = "";
    if ( $graph_fld eq 'seq_center' ) {
	$attr_disp_name = "Sequencing Center";
    }
    else {
	$attr_disp_name = $def_aux->{display_name};
    }

    print "<h1>IMG-GOLD Categories: " . $attr_disp_name . "</h1>\n"; 

    my $url2 = "gold.cgi?section=ProjSummary&page=showChartCategory";

   # PREPARE THE BAR CHART 
    my $chart = newBarChart(); 
    $chart->WIDTH(550); 
    $chart->HEIGHT(300); 
    $chart->DOMAIN_AXIS_LABEL($attr_disp_name);
    $chart->RANGE_AXIS_LABEL("Number of projects"); 
    $chart->INCLUDE_TOOLTIPS("yes"); 
    $chart->INCLUDE_URLS("yes"); 
#	my $url = "gold.cgi?section=ProjSummary&page=showCategory&cat_field=$graph_fld&cat_code=$disp_key";
    $chart->ITEM_URL($main_cgi."?section=ProjSummary&page=showCategory&cat_field=$graph_fld");
    $chart->ROTATE_DOMAIN_AXIS_LABELS("yes"); 
    $chart->COLOR_THEME("ORANGE"); 

    # file prefix
    my $fileprefix = "";
    my $p_count = 4000;
    my @flds = getSetStatsAttrs();
    for my $f1 ( @flds ) {
	$p_count++;
	if ( $graph_fld eq $f1 ) {
	    $fileprefix = $p_count;
	    if ( ! $include_null ) {
		$fileprefix .= '0';
	    } 
	    else {
		$fileprefix .= '1';
	    }

	    last;
	}
    }
 
    if ( ! blankStr($fileprefix) ) {
	$chart->FILE_PREFIX( $fileprefix ); 
	$chart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix ); 
    }

#    print "<p>FILE_PREFIX: " . $chart->FILE_PREFIX . "</p>\n";

#    my $contact_oid = getContactOid();
#    if ( $contact_oid == 312 ) {
#	print "<p>FILEPATH_PREFIX: " . $chart->FILEPATH_PREFIX . "</p>\n";
#    }

    my @chartseries; 
    my @chartcategories; 
    my @functioncodes; 
    my @chartdata; 

   ################################# 

    my $stats_proj_type = param1('stats_proj_type');
    my $projcond = getProjCond('p2', $stats_proj_type);

    my $dbh = Connect_IMG();

    # get total count
    my $sql = "select count(*) from project_info p2 where " . $projcond;
    my $total = db_getValue($sql);

    # get categories
    my $projcond0 = getProjCond('p0', $stats_proj_type);
    if ( $graph_fld eq 'seq_center' ) {
	$sql = qq{
	    select p2.db_name, count(*)
		from project_info_data_links p2
		where p2.project_oid in (select p0.project_oid
					 from project_info p0
					 where $projcond0)
		and p2.link_type = 'Seq Center'
		and p2.db_name is not null
		group by p2.db_name
	    };
    }
    else {
	$sql = qq{
	    select p2.$graph_fld, count(*)
		from $aux_name p2
		where p2.project_oid in (select p0.project_oid
					 from project_info p0
					 where $projcond0)
		group by p2.$graph_fld
	    };
    }
    $sql .= " order by 2 desc, 1";
#    print "<p>SQL: $sql</p>\n";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    print "<table width=800 border=0>\n";
    print "<tr>"; 
    print "<td valign=top>\n";
 
    my $count = 0;
    my $max_disp = 15;
    for( ;; ) { 
	my( $function_code, $f_count ) = $cur->fetchrow( );
	last if !$function_code;
	$count++; 

	if ( $count >= $max_disp ) {
	    last;
	}

	push @chartcategories, "$function_code";
	push @functioncodes, "$function_code";
	push @chartdata, $f_count;
    } 
    $cur->finish();

    # get unclassified
    my $projcond9 = getProjCond('p', $stats_proj_type);
    my $sql0 = qq{
	select count(*)
	    from project_info p
	    where $projcond9
	    and project_oid not in
	};

    if ( $graph_fld eq 'seq_center' ) {
	$sql0 .= qq{
	    (select pidl.project_oid
	     from project_info_data_links pidl
	     where pidl.link_type = 'Seq Center')
	    };
    }
    else {
	$sql0 .= " (select p2.project_oid from $aux_name p2)";
    }

#    print "<p>SQL: $sql0</p>\n";
webLog("$sql0\n");
    $cur=$dbh->prepare($sql0);
    $cur->execute();
    my( $f_count0 ) = $cur->fetchrow( );
    $cur->finish();

    if ( $include_null ) {
	push @chartcategories, "Unclassified";
	push @functioncodes, "Unclassified";
	push @chartdata, $f_count0;
    }

    print "<p>Percentage shows the percentage of projects having this specific field value in " . $attr_disp_name . ".</p>\n";

    if ( $count >= $max_disp ) {
	print "<p><font color='red'>Too many values! Only $max_disp values are displayed.</font></p>\n";
    }

    # display table data
    print "<table class='img'  border='1'>\n";
    print "<th class='img' >" . $attr_disp_name . "</th>\n";
    print "<th class='img' >Count</th>\n";
    print "<th class='img' >Percentage</th>\n";

    my $idx=0;
    for my $category1 ( @chartcategories ) {
	last if !$category1; 

        my $disp_key = $category1; 
        $disp_key =~ s/\s/\_/g; 
	my $url = "gold.cgi?section=ProjSummary&page=showCategory&cat_field=$graph_fld&cat_code=$disp_key";

	print "<tr class='img' >\n"; 
	print "<td class='img' >\n";
 
#	print "<a href='$url'>"; 
#	my $fname = $chart->FILE_PREFIX . "-color-" . $idx .
#	    ".png";
#	print "<img src='$tmp_url/". $fname . "' border=0>"; 
#	print "</a>";
#	print "&nbsp;&nbsp;";

	print escapeHTML( $category1 );
	print "</td>\n"; 
	print "<td class='img' align='right'>\n";
	if ( $chartdata[$idx] ) {
	    print alink( $url, $chartdata[$idx] );
	}
	else {
	    print "0";
	}

	print "</td>\n"; 

	# percentage
	if ( $total > 0 ) {
	    if ( $chartdata[$idx] ) {
		my $perc = int(($chartdata[$idx] * 100 / $total) + .5);
		print "<td class='img' align='right'>" . $perc . "%</td>\n";
	    }
	    else {
		print "<td class='img' align='right'>0%</td>\n";
	    }
	}
	else {
	    print "<td class='img' align='right'>-</td>\n";
	}

	print "</tr>\n"; 
	$idx++;
    }
 
    print "</table>\n"; 
    print "</td>\n"; 
    print "<td valign=top align=left>\n";

    print "</td><td valign=top align=right>\n";

    # display the bar chart
    push @chartseries,$graph_fld;
    $chart->SERIES_NAME(\@chartseries);
    $chart->CATEGORY_NAME(\@chartcategories);
    my $datastr = join(",", @chartdata);
    my @datas = ($datastr);
    $chart->DATA(\@datas);

    print "<td align=right valign=center>\n"; 
    if ($env->{ chart_exe } ne "") { 
	my $st = generateChart($chart);
	if ( $st == 0 ) { 
	    print "<script src='$base_url/overlib.js'></script>\n";
            my $FH = newReadFileHandle($chart->FILEPATH_PREFIX.".html", "printDistribution",1); 
	    while (my $s = $FH->getline()) {
		print $s;
	    } 
	    close ($FH);
	    print "<img src='$tmp_url/".$chart->FILE_PREFIX.".png' BORDER=0 ";
	    print " width=".$chart->WIDTH." HEIGHT=".$chart->HEIGHT;
	    print " USEMAP='#".$chart->FILE_PREFIX."'>\n";
	} 
    } 
    print "</td></tr>\n";
    print "</table>\n"; 
 
   ###########################  

    $dbh->disconnect( ); 

    print "<p>\n";
 
    if ( ! $include_null ) {
	my $url = "gold.cgi?section=ProjSummary&page=showCategory&cat_field=$graph_fld&cat_code=Unclassified";
	print "<p>Unclassified: <a href='$url'>$f_count0</a></p>\n";
    }
    print "<p>Total: $total</p>\n";
} 
 



1;
 
