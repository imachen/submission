package SubmitStats;
my $section = "SubmitStats";
 
use strict; 
use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
 
use lib 'lib'; 
use WebEnv; 
use WebFunctions; 
use RelSchema; 
 
 
my $env = getEnv(); 
my $main_cgi             = $env->{main_cgi};
my $section_cgi          = "$main_cgi?section=$section";
my $cgi_tmp_dir = $env->{ cgi_tmp_dir }; 
my $use_yahoo_table = $env->{ use_yahoo_table };

my $default_max_sub_row = 30;


 
######################################################################### 
# dispatch 
######################################################################### 
sub dispatch { 
    my ($page) = @_; 
 
    my $contact_oid = getContactOid( ); 
    my $isAdmin = 'No'; 
 
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    } 
 
    if ( $page eq 'listSubmissionPage' ) {
	if ( $use_yahoo_table ) {
	    ListSubmissionPage_yui();
	}
	else {
	    ListSubmissionPage();
	}
    } 
    else { 
        ShowPage($isAdmin); 
    } 
} 

 
############################################################################
# ShowPage
############################################################################
sub ShowPage { 
    my ($isAdmin) = @_; 
 
    if ( $isAdmin ne 'Yes' ) {
        ShowUserStatsPage();
    } 
    else {
        ShowAdminStatsPage();
    } 
} 
 
############################################################################
# ShowUserStatsPage 
############################################################################
sub ShowUserStatsPage { 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi");
 
    print "<h1>User Submission Statistics</h1>\n";

    my $database = param1('database');
    $database =~ s/%20/ /g;

    my $new_url = $main_cgi . "?section=SubmitStats&page=showPage"; 
    $database = ShowDataSetSelectionSection("No", $database, $new_url); 

    ShowStatisticsSection($database);

#    ShowStatisticsSection('IMG ER');
#    ShowStatisticsSection('IMG/M ER');
 
    # Home
    print "<p>\n"; 
    printHomeLink(); 
 
    print end_form(); 
} 


############################################################################ 
# ShowAdminStatsPage 
############################################################################ 
sub ShowAdminStatsPage { 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 
 
    print "<h1>IMG Submission Statistics</h1>\n";

    my $database = param1('database');
    $database =~ s/%20/ /g;

    my $new_url = $main_cgi . "?section=SubmitStats&page=showPage"; 
    $database = ShowDataSetSelectionSection("Yes", $database, $new_url); 

    ShowStatisticsSection($database);

#    ShowStatisticsSection('IMG ER');
#    ShowStatisticsSection('IMG/M ER');
 
    # Home 
    print "<p>\n"; 
    printHomeLink(); 
 
    print end_form(); 
} 
 

######################################################################### 
# ShowStatisticsSection
######################################################################### 
sub ShowStatisticsSection { 
    my ($database) = @_; 

    my $contact_oid = getContactOid( ); 
    my $isAdmin = 'No'; 

    my $cond = " ";
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    }

    if ( ! $database || ! $contact_oid ) {
	return;
    }

    if ( $contact_oid && $isAdmin ne 'Yes' ) {
	$cond = " and (s.contact = $contact_oid or s.submission_id in (select s2.submission_id from submission_img_contacts s2 where s2.img_contacts = $contact_oid)) ";
    }

    print "<h2>" . getDataSetDisplayName($database) .
	" Submission Statistics</h2>\n";
    print "<p>\n";

    Submission::printHint3("Older submissions do not have complete information to indicate whether they are JGI or external.");

    my $color1 = '#eeeeee';

    my $dbh=WebFunctions::Connect_IMG;

    # get total counts
    my $sql = qq{
	select v.term_oid, v.cv_term, count(*)
	    from submission s, submission_statuscv v
	    where s.database = '$database'
	    $cond
	    and s.status = v.term_oid
	    group by v.term_oid, v.cv_term
	    order by v.term_oid
	};
webLog("$sql\n");

    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $cnt0 = 0;
    my %cv;
    my %total;
    undef %total;
    my %last;
    undef %last;

    for (;;) { 
        my ( $term_oid, $cv_term, $count ) =
		 $cur->fetchrow_array(); 
        if ( ! $term_oid ) {
            last; 
        } 

	$cnt0++;
	if ( $cnt0 > 100 ) {
	    last;
	}

	$cv{$term_oid} = $cv_term;
	$total{$term_oid} = $count;
    }
    $cur->finish();

    # get JGI vs external
    my %jgi_submit;
    undef %jgi_submit;
    my %external_submit;
    undef %external_submit;
    for my $external ( 'No', 'Yes' ) {
	my $sql = qq{
	    select v.term_oid, v.cv_term, count(*)
	        from submission s, submission_statuscv v, gold_analysis_project a
   	        where s.database = '$database'
	        $cond
	        and s.status = v.term_oid
                and s.analysis_project_id = a.gold_id
                and a.external = ?
	        group by v.term_oid, v.cv_term
	        order by v.term_oid
	    };

	my $cur=$dbh->prepare($sql); 
	$cur->execute($external); 

	my $cnt0 = 0;

	for (;;) { 
	    my ( $term_oid, $cv_term, $count ) =
		$cur->fetchrow_array(); 
	    if ( ! $term_oid ) {
		last; 
	    } 

	    $cnt0++;
	    if ( $cnt0 > 100 ) {
		last;
	    }

	    if ( $external eq 'No' ) {
		$jgi_submit{$term_oid} = $count;
	    }
	    else {
		$external_submit{$term_oid} = $count;
	    }
	}
    }
    $cur->finish();


    # get last loaded submission ID
#    $sql = qq{
#	select max(s.submission_id)
#	    from submission s
#	    where s.database = '$database'
#	    and s.status in (8, 10)
#	};
#    $cur=$dbh->prepare($sql); 
#    $cur->execute(); 
#    my ($max_id) = $cur->fetchrow();
#    $cur->finish();

#    if ( blankStr($max_id) ) {
#	$max_id = 0;
#    }

#    if ( $contact_oid && $isAdmin eq 'Yes' ) {
#	print "<p>Last Loaded Submission ID: $max_id\n";
#    }

    # get counts from last load
#    $sql = qq{
#	select v.term_oid, v.cv_term, count(*)
#	    from submission s, submission_statuscv v
#	    where s.database = '$database'
#	    $cond
#	    and s.submission_id > $max_id
#	    and s.status = v.term_oid
#	    group by v.term_oid, v.cv_term
#	    order by v.term_oid
#	};
#    $cur=$dbh->prepare($sql); 
#    $cur->execute(); 

#    $cnt0 = 0;
#    for (;;) { 
#        my ( $term_oid, $cv_term, $count ) =
#		 $cur->fetchrow_array(); 
#        if ( ! $term_oid ) {
#            last; 
#        } 

#	$cnt0++;
#	if ( $cnt0 > 100 ) {
#	    last;
#	}

#	$last{$term_oid} = $count;
#   }
#   $cur->finish();

    $dbh->disconnect();

    my @keys = (keys %cv);
    if ( scalar(@keys) == 0 ) {
	print "<p>No submissions.\n";
	return;
    }

    my $max_count = 3000;

    # print table header
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Submission Status</th>\n";
    print "<th class='img' bgcolor='$color1'>Total Count</th>\n";
##    print "<th class='img' bgcolor='$color1'>Since Last Upload</th>\n";
    print "<th class='img' bgcolor='$color1'>JGI</th>\n";
    print "<th class='img' bgcolor='$color1'>External</th>\n";

    for my $term_oid (sort @keys) {
	print "<tr class='img'>\n"; 
	print "<td class='img'>" . escapeHTML($cv{$term_oid}) . "</td>\n";

	## total
	if ( ! $total{$term_oid} || $total{$term_oid} > $max_count ) {
	    print "<td class='img' align='right'>" . $total{$term_oid} . "</td>";
	}
	else {
	    print "<td class='img' align='right'>"; 
	    print "<a href='" . $main_cgi .
		"?section=SubmitStats&page=listSubmissionPage" .
		"&database=$database&status_code=$term_oid' >" .
		$total{$term_oid} . "</a></td>"; 
	}

	## jgi
	if ( ! $jgi_submit{$term_oid} || $jgi_submit{$term_oid} > $max_count ) {
	    print "<td class='img' align='right'>" . $jgi_submit{$term_oid} . "</td>";
	}
	else {
	    print "<td class='img' align='right'>"; 
	    print "<a href='" . $main_cgi .
		"?section=SubmitStats&page=listSubmissionPage" .
		"&database=$database&external=No&status_code=$term_oid' >" .
		$jgi_submit{$term_oid} . "</a></td>"; 
	}

	## external
	if ( ! $external_submit{$term_oid} || $external_submit{$term_oid} > $max_count ) {
	    print "<td class='img' align='right'>" . $external_submit{$term_oid} . "</td>";
	}
	else {
	    print "<td class='img' align='right'>"; 
	    print "<a href='" . $main_cgi .
		"?section=SubmitStats&page=listSubmissionPage" .
		"&database=$database&external=Yes&status_code=$term_oid' >" .
		$external_submit{$term_oid} . "</a></td>"; 
	}

#	if ( $last{$term_oid} ) {
#	    print "<td class='img' align='right'>"; 
#	    print "<a href='" . $main_cgi .
#		"?section=SubmitStats&page=listSubmissionPage" .
#		"&database=$database&status_code=$term_oid&lastLoad=$max_id'>" .
#		$last{$term_oid} . "</a></td>"; 
#	}
#	else {
#	    print "<td class='img' align='right'>0</td>\n";
#	}

	print "</tr>\n";
    }

    print "</table>\n";
}


############################################################################
# ListSubmissionPage
############################################################################
sub ListSubmissionPage { 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    my $contact_oid = getContactOid( ); 
    my $isAdmin = 'No'; 

    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    }
 
    my $status_code = param1('status_code');
    my $database = param1('database');
    $database =~ s/%20/ /g;

    my $status = db_getValue("select v.cv_term from submission_statuscv v where v.term_oid=$status_code");

    my $max_id = param1('lastLoad');
    if ( $max_id ) {
	print "<h2>Submission Status: " . escapeHTML($status) .
	    " (since last upload)</h2>\n";
    }
    else {
	print "<h2>Submission Status: " . escapeHTML($status) . "</h2>\n";
    }

    print "<h3>Database: $database</h3>\n";

    print hiddenVar('status_code', $status_code);
    print hiddenVar('database', $database);

    my $cond = "s.status = $status_code";
    if ( $max_id ) {
	$cond .= " and s.submission_id > $max_id";
    }
    my $cnt2 = Submission::submissionCount($contact_oid, $isAdmin, $database, $cond); 
 
    print "<p>Selected Submission Count: $cnt2</p>\n"; 

    # save orderby param 
    my $orderby = param1('submission_orderby'); 
#    if ( blankStr($orderby) && 
#         getSessionParam('browse:submission_orderby') ) { 
#        $orderby = getSessionParam('browse:submission_orderby'); 
#    } 
    my $desc = param1('submission_desc'); 
#    if ( blankStr($desc) && getSession('browse:submission_desc') ) { 
#        $desc = getSessionParam('browse:submission_desc'); 
#    } 
 
    # max display 
    my $max_display = getSessionParam('sub_filter:max_display'); 
    if ( blankStr($max_display) ) { 
        $max_display = $default_max_sub_row; 
    } 
 
    # display page numbers 
    my $curr_page = param1('submission_page_no'); 
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
#        if ( getSessionParam('browse:submission_page_no') ) { 
#            $curr_page = getSessionParam('browse:submission_page_no'); 
#        } 
 
        if ( blankStr($curr_page) || $curr_page <= 0 ) { 
            $curr_page = 1; 
        } 
    } 

    # save browse history 
#    setSessionParam('browse:submission_page_no', $curr_page);
#    setSessionParam('browse:submission_orderby', $orderby);
#    setSessionParam('browse:submission_desc', $desc);
 
    my $i = 0; 
    my $page = 1; 

    print "<p>\n";

    while ( $i < $cnt2 ) { 
        my $s = $page;
        if ( $page == $curr_page ) {
            $s = "<b>\[$page\]</b>"; 
        } 
        my $link = "<a href='" . $main_cgi;
	$link .= "?section=SubmitStats&page=listSubmissionPage&database=$database&status_code=$status_code";
        $link .= "&submission_page_no=$page";
        if ( ! blankStr($orderby) ) { 
            $link .= "&submission_orderby=$orderby"; 
            if ( ! blankStr($desc) ) { 
                $link .= "&submission_desc=$desc";
            }
        } 
        $link .= "' >" . $s . "</a>"; 
        print $link . nbsp(1); 
        $i += $max_display; 
        $page++;
        if ( $page > 1000 ) {
            last; 
        } 
    } 
    print "<p>\n"; 
 
    if ( $cnt2 == 0 ) {
        return;
    } 
 
    my $page_no = param1('submission_page_no');
    Submission::listSubmissions($contact_oid, $isAdmin, $database, $cond, $page_no,
				"section=SubmitStats&page=listSubmissionPage&database=$database&status_code=$status_code",
				1);

    # buttons
    my $section2 = 'ERSubmission'; 
    if ( $database eq 'IMG/M ER' ) { 
        $section2 = 'MSubmission'; 
    } 

    my $canUpdate = Submission::getCanUpdateSubmission($contact_oid); 
    if ( $canUpdate ) { 
        print "<p>(<b>Note:</b> Check/Update Status and Update IMG Contact(s) functions only apply to the first selected submission.)<br/>\n";
        print "<input type='submit' name='_section_$section2:checkStatus' value='Check / Update Status' class='medbutton' />"; 
        print "&nbsp; \n"; 
        print "<input type='submit' name='_section_$section2:updateContact' value='Update IMG Contact(s)' class='medbutton' />"; 
    } 
    else { 
        print "<p>(<b>Note:</b> Check Status only applies to the first selected submission.)<br/>\n"; 
        print "<input type='submit' name='_section_$section2:checkStatus' value='Check Status' class='medbutton' />"; 
    } 
 
    # Home 
    print "<p>\n"; 
    printHomeLink(); 
 
    print end_form(); 
}



############################################################################
# ListSubmissionPage_yui  (Yahoo data table display)
############################################################################
sub ListSubmissionPage_yui { 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi"); 

    my $contact_oid = getContactOid( ); 
    my $isAdmin = 'No'; 

    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid); 
    }
 
    my $status_code = param1('status_code');
    my $database = param1('database');
    $database =~ s/%20/ /g;

    my $status = db_getValue("select v.cv_term from submission_statuscv v where v.term_oid=$status_code");

    my $max_id = param1('lastLoad');
#    if ( $max_id ) {
#	print "<h2>Submission Status: " . escapeHTML($status) . 
#	    " (since last upload)</h2>\n";
 #   }
#    else {
#	print "<h2>Submission Status: " . escapeHTML($status) . "</h2>\n";
#    }

    my $external = param1("external");
    if ( $external eq 'No' ) {
	print "<h2>JGI Submissions</h2>\n";
    }
    elsif ( $external eq 'Yes' ) {
	print "<h2>External Submissions</h2>\n";
    }
    print "<h2>Submission Status: " . escapeHTML($status) . "</h2>\n";

    print "<h3>Database: $database</h3>\n";
    print hiddenVar('status_code', $status_code);
    print hiddenVar('database', $database);

    my $cond = "s.status = $status_code";
    if ( $max_id ) {
	$cond .= " and s.submission_id > $max_id";
    }

    if ( $external ) {
	$cond .= " and s.analysis_project_id in (select a.gold_id from gold_analysis_project a where a.external = '" . $external . "') ";
    }

    my $cnt2 = Submission::submissionCount($contact_oid, $isAdmin, $database, $cond); 
    print "<p>Selected Submission Count: $cnt2</p>\n"; 

    Submission::listSubmissions_yui($contact_oid, $isAdmin, $database, $cond, 1);

    # buttons
    my $section2 = 'ERSubmission'; 
    if ( $database eq 'IMG/M ER' ) { 
        $section2 = 'MSubmission'; 
    } 

    my $canUpdate = Submission::getCanUpdateSubmission($contact_oid); 
    if ( $canUpdate ) { 
        print "<p>(<b>Note:</b> Check/Update Status and Update IMG Contact(s) functions only apply to the first selected submission.)<br/>\n";
        print "<input type='submit' name='_section_$section2:checkStatus' value='Check / Update Status' class='medbutton' />"; 
        print "&nbsp; \n"; 
        print "<input type='submit' name='_section_$section2:updateContact' value='Update IMG Contact(s)' class='medbutton' />"; 
    } 
    else { 
        print "<p>(<b>Note:</b> Check Status only applies to the first selected submission.)<br/>\n"; 
        print "<input type='submit' name='_section_$section2:checkStatus' value='Check Status' class='medbutton' />"; 
    } 
 
    # Home 
    print "<p>\n"; 
    printHomeLink(); 
 
    print end_form(); 
}




1;
