package RequestAcct;
my $section = "RequestAcct";

use strict;
use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';

use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;

my $env = getEnv();
my $main_cgi = $env->{ main_cgi };
my $section_cgi          = "$main_cgi?section=$section";


#########################################################################
# showPage - the starting page
#
#########################################################################
sub ShowPage{

    #generate a form
    print start_form(-method=>'post',
		     -action=>"$main_cgi");

    print "<h1>Request IMG Account</h1>\n";

    print "<p>Please fill in the request form. Required fields are marked with (*)</p>\n";

    # get request account definition
    my $def_req = def_Request_Account();

    my @attrs = @{$def_req->{attrs}};

    print "<table class='img' border='1'>\n";

    for my $k ( @attrs ) { 
	my $attr_name = $k->{name};

	if ( ! $k->{can_edit} ) {
	    next;
	}

	my $data_type = $k->{data_type};
	my $len = $k->{length};
	my $disp_name = $k->{display_name};
	if ( ! $disp_name ) {
	    $disp_name = $attr_name;
	}
 
        my $size = 80; 
        if ( $len && $size > $len ) { 
            $size = $len; 
        } 

	my $attr_val = "";

        print "<tr class='img' >\n"; 
        print "  <th class='subhead' align='right'>";
	print escapeHTML($disp_name);
	if ( $k->{is_required} ) {
	    print " (*)";
	}
	print "</th>\n"; 

	if ( ! $k->{can_edit} ) {
	    # skip non-editable field
	    print "  <td class='img'   align='left'>" .
		escapeHTML($attr_val) . "</td>\n";
	}
	elsif ( $data_type eq 'text' ) {
	    print "  <td class='img'   align='left'>" .
		"<textarea name='$attr_name' cols='$size' rows='10'>" .
		"</textarea>" . "</td>\n"; 
	}
	elsif ( $data_type eq 'cv' && $k->{cv_query} ) {
	    my @vals = db_getValues($k->{cv_query});
	    print "  <td class='img'   align='left'>\n";
	    print "<select name='$attr_name' class='img' size='1'>\n";
	    if ( ! $k->{is_required} ) {
                print "   <option value=''></option>\n";
	    }
	    for my $val ( @vals ) {
                print "   <option value='$val'>" . escapeHTML($val) .
                    "</option>\n";
            } 
	    print "</select></td>\n"; 
	}
	else {
	    print "  <td class='img'   align='left'>" .
		"<input type='text' name='$attr_name' value='" .
		escapeHTML($attr_val) . "' size='$size'" .
		" maxLength='$len'/>" . "</td>\n"; 
	}

        print "</tr>\n";
    } 

    print "</table>\n";

    print "<p>\n";
    print '<input type="submit" name="submitRequest" value="Submit" class="smbutton" />';

    print end_form();
}


#########################################################################
# SubmitRequest
#########################################################################
sub SubmitRequest {

    my $msg = "";

    # get request account definition
    my $req_id = db_findMaxID('request_account', 'request_oid') + 1;
    my $ins = "insert into request_account (request_oid, request_date, status";
    my $vals = "values (" . $req_id . ", sysdate, 'new request'";

    my $def_req = def_Request_Account();
    my @attrs = @{$def_req->{attrs}};
    for my $k ( @attrs ) {
	if ( ! $k->{can_edit} ) {
	    next;
	}

	my $attr_name = $k->{name};
	my $data_type = $k->{data_type};
	my $v = param1($attr_name);
	if ( $k->{is_required} && blankStr($v) ) {
	    $msg = "Please enter a value in " . $k->{display_name} . ".";
	    return $msg;
	}

	if ( ! blankStr($v) ) {
	    $ins .= ", " . $attr_name;
	    if ( $data_type eq 'int' ) {
		if ( ! isInt($v) ) {
		    $msg = $k->{display_name} . " must be an integer.";
		    return $msg;
		}
		$vals .= ", " . $v;
	    }
	    elsif ( $data_type eq 'number' ) {
		if ( ! isNumber($v) ) {
		    $msg = $k->{display_name} . " must be a number.";
		    return $msg;
		}
		$vals .= ", " . $v;
	    }
	    else {
		$v =~ s/'/''/g;   # replace ' with '';
		$vals .= ", '" . $v . "'";
	    }
	}
    }  # end for k

    my $sql = $ins . ") " . $vals . ")";

    my @sqlList = ( );
    push @sqlList, ( $sql );
    db_con_sqlTrans(\@sqlList); 

    return $msg;
}


#########################################################################
# requestCount
#########################################################################
sub requestCount {
    my ($cond) = @_;

    my $dbh=WebFunctions::Connect_IMG_Contact();
    if ( ! $dbh ) {
	return -1;
    }

    my $sql = "select count(*) from request_account";
    if ( ! blankStr($cond) ) {
	$sql .= " where " . $cond;
    } 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
	return 0;
    }

    return $cnt;
}


########################################################################## 
# ShowRequest - show all user request
#
# req_type: all, new
########################################################################## 
sub ShowRequest{ 
    my ($req_type) = @_;

    print start_form(-name=>'showSample',-method=>'post',action=>"$section_cgi"); 
 
    # verify the username and password 
    my $userString = param1('userstr'); 
 
    if ( blankStr($userString) ) { 
        # check login 
        $userString = isValid(); 
        if ( blankStr($userString) ) { 
            dienice("Unknown username / password"); 
        } 
    } 
 
    my ($contact_oid, $uname) = split(/ /, $userString); 
 
#    print hidden('userstr',$userString); 
    saveAppHiddenVar();

    if ( blankStr($req_type) ) {
	$req_type = param1('request_type');
	if ( blankStr($req_type) ) {
	    $req_type = 'all';
	}
    }

    print hidden('request_type', $req_type);

    if ( $req_type eq 'new' ) {
	print "<h1>New User Requests</h1>\n";
    }
    else {
	print "<h1>All User Requests</h1>\n";
    }

    my $cnt = 0; 
    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) { 
	my $cond = "";
	if ( $req_type eq 'new' ) {
	    $cond = "status = 'new request'";
	}
        $cnt = requestCount($cond); 
        print "<p>Count: $cnt</p>\n"; 
        listRequests($cond); 
    }

    # Update and Delete buttons
    print "<p>\n";
    print '<input type="submit" name="updateRequest" value="Update" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="deleteRequest" value="Delete" class="smbutton" />'; 
 
    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
} 


######################################################################### 
# listRequests - list requests
########################################################################## 
sub listRequests { 
    my ($cond) = @_;

    my $selected_request = param1('request_oid'); 
 
    my $dbh=WebFunctions::Connect_IMG_Contact(); 

    my $cond2 = "";
    if ( ! blankStr($cond) ) {
	$cond2 = " where " . $cond;
    }

    my $sql = qq{ 
        select request_oid, name, organization, status, request_date
            from request_account
	    $cond2
            order by request_oid
        };
 
    #print "$sql<br>";
webLog("$sql\n");    
    my $cur=$dbh->prepare($sql); 
    $cur->execute();
 
    print "<p>\n"; 
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>Request ID</th>\n"; 
    print "<th class='img'>Name</th>\n";
    print "<th class='img'>Organization</th>\n";
    print "<th class='img'>Status</th>\n";
    print "<th class='img'>Request Date</th>\n";
 
    my $cnt = 0; 
    for (;;) { 
        my ( $request_oid, $name, $org, $status, $request_date ) = 
            $cur->fetchrow_array(); 
        if ( ! $request_oid ) { 
            last; 
        } 
 
        $cnt++; 
 
        print "<tr class='img'>\n"; 
 
        print "<td class='img'>\n"; 
        print "<input type='radio' "; 
        print "name='request_oid' value='$request_oid'"; 
        if ( $request_oid == $selected_request ) { 
            print " checked "; 
        } 
        print "/></td>\n";
 
        PrintAttribute($request_oid);
        PrintAttribute($name);
        PrintAttribute($org);
        PrintAttribute($status);
        PrintAttribute($request_date);
        print "</tr>\n";
    }
    print "</table>\n";
    print "<p>\n"; 
 
    $cur->finish();
    $dbh->disconnect();
 
    return $cnt; 
}


############################################################################# 
# UpdateRequest - update request
############################################################################# 
sub UpdateRequest { 
 
    print start_form(-name=>'updateRequest',-method=>'post',action=>"$section_cgi"); 
 
    # keep user info 
    my $userString = param1('userstr'); 
#    print hidden('userstr',$userString); 
    saveAppHiddenVar();

    my $request_oid = param1('request_oid'); 
 
    if ( blankStr($request_oid) ) { 
	printError("No request has been selected.");
	print end_form();
	return;
    } 
 
    my $req_type = param1('request_type');
    if ( ! blankStr($req_type) ) {
	print hidden('request_type', $req_type);
    }

    print "<h2>Update Request $request_oid</h2>\n";
    print hidden('request_oid', $request_oid);
 
    my %db_val = SelectRequest($request_oid);
 
    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs = @{$def_req->{attrs}};
 
    print "<p>Required fields are marked with (*).</p>\n";

#    print "<p><font color='red'>Note: A request is considered as a new request if and only if status = 'new request'.</font></p>\n";

    print "<table class='img' border='1'>\n";
    for my $k ( @attrs ) { 
        my $attr_name = $k->{name};
        my $attr_val = ""; 
        if ( $db_val{$attr_name} ) {
            $attr_val = $db_val{$attr_name};
        }
 
        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type}; 
        my $len = $k->{length};
        my $edit = $k->{can_edit};
 
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        if ( $k->{is_required} ) {
            print escapeHTML($disp_name) . " (*) </th>\n";
        } 
        elsif ( $k->{url} ) {
            print alink($k->{url}, $disp_name, 'target', 1);
        } 
        else {
            print escapeHTML($disp_name); 
        }
        print "</th>\n"; 
 
        print "  <td class='img'   align='left'>\n";
 
        if ( ! $edit &&
	     $attr_name ne 'status' &&
	     $attr_name ne 'contact_oid' ) { 
            print escapeHTML($attr_val);
        } 
        elsif ( $data_type eq 'cv' && $k->{cv_query} ) {
            my @selects = db_getValues($k->{cv_query}); 
            print "<select name='$attr_name' class='img' size='1'>\n";
            if ( ! $k->{is_required} ) {
                print "   <option value=''> </option>\n";
            } 
            for my $s0 ( @selects ) {
                print "    <option value='$s0'"; 
		if ( $s0 eq $attr_val ) {
		    print " selected ";
		}
                print ">$s0</option>\n";
            } 
            print "</select>\n";
        }
	elsif ( $data_type eq 'text' ) {
	    print "<textarea name='$attr_name' cols='$len' rows='10'>" .
		$attr_val . "</textarea>";
	}
	else {
            my $size = 80; 
            if ( $len && $size > $len ) {
                $size = $len;
            } 
 
            print "<input type='text' name='$attr_name' value='" .
                escapeHTML($attr_val) . "' size='$size'" .
                " maxLength='$len'/>"; 
        }
        print "</td>\n"; 
        print "</tr>\n";
    } 

    print "</table>\n";
 
    print "<p>\n";
    print '<input type="submit" name="dbUpdateRequest" value="Update" class="smbutton" />'; 
    print "&nbsp; \n";
    if ( $req_type eq 'new' ) {
	print '<input type="submit" name="showNewRequest" value="Cancel" class="smbutton" />';
    }
    else {
	print '<input type="submit" name="showRequest" value="Cancel" class="smbutton" />';
    }
 
    print "<p>\n";

    printHomeLink();
 
    print end_form(); 
} 


#################################################################### 
# select Request_Account data given an ID 
#################################################################### 
sub SelectRequest { 
    my ( $request_oid ) = @_; 
 
    my %db_val; 
 
    my $dbh = Connect_IMG_Contact(); 
 
    my $db_id = $request_oid;
 
    # get Request_Account definition 
    my $def_req = def_Request_Account(); 
    my @attrs = @{$def_req->{attrs}}; 
 
    my $sql = ""; 
    for my $k ( @attrs ) { 
        my $attr_name = $k->{name}; 
 
        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } 
        else { 
            $sql .= ", " . $attr_name;
        } 
    }
 
    $sql .= " from request_account where request_oid = $db_id"; 
 
    # print "<p>SQL: $sql</p>\n"; 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my @flds = $cur->fetchrow_array();
 
    # save result 
    my $j = 0; 
    for my $k ( @attrs ) { 
        my $attr_name = $k->{name}; 
        if ( scalar(@flds) < $j ) { 
            last; 
        } 
        $db_val{$attr_name} = $flds[$j]; 
        $j++; 
    } 
 
    # finish 
    $cur->finish(); 
    $dbh->disconnect(); 

    # modified_by 
    if ( $db_val{'modified_by'} ) { 
        $db_val{'modified_by'} = db_getContact($db_val{'modified_by'}); 
    } 
 
    return %db_val; 
} 


############################################################################# 
# dbUpdateRequest - update request_account info in database 
############################################################################# 
sub dbUpdateRequest { 
    # get user info 
    my $userString = param1('userstr'); 
    my ($contact_oid, $uname) = split(/ /, $userString); 

    # get Request_Account definition 
    my $def_req = def_Request_Account(); 
    my @attrs = @{$def_req->{attrs}}; 
 
    my $msg = ""; 
 
    # get request_oid
    my $request_oid = param1('request_oid'); 
    if ( blankStr($request_oid) ) { 
        $msg = "No request has been selected for update."; 
        return $msg; 
    } 
 
    my $sql = "update request_account set modified_by = $contact_oid, " .
        "mod_date = sysdate";
 
    for my $k ( @attrs ) {
        my $attr_name = $k->{name};
        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type};
        my $len = $k->{length};
        my $edit = $k->{can_edit}; 
 
        if ( ! $edit &&
	     $attr_name ne 'status' &&
	     $attr_name ne 'contact_oid' ) { 
            next; 
        } 
 
        my $val = param1($attr_name);
        if ( blankStr($val) ) {
            $sql .= ", $attr_name = null";
            next;
        }
 
        if ( $data_type eq 'int' ) { 
            if ( ! blankStr($val) && ! isInt($val) ) {
                $msg = "$disp_name must be an integer."; 
                last;
            } 
 
            $sql .= ", $attr_name = $val"; 
        }
        else { 
            $val =~ s/'/''/g;   # replace ' with ''
            $sql .= ", $attr_name = '" . $val . "'";
        }
    } 
 
    $sql .= " where request_oid = $request_oid";
 
    if ( blankStr($msg) ) { 
        my @sqlList = ( $sql ); 
        db_con_sqlTrans(\@sqlList);
    } 
 
    return $msg; 
} 
 


############################################################################# 
# ConfirmDeleteRequest - confirm delete request
############################################################################# 
sub ConfirmDeleteRequest { 
 
    print start_form(-name=>'confirmDeleteRequest',-method=>'post',action=>"$section_cgi"); 

    # keep user info 
    my $userString = param1('userstr'); 
#    print hidden('userstr',$userString); 
    saveAppHiddenVar();

    my $request_oid = param1('request_oid'); 
 
    if ( blankStr($request_oid) ) { 
	printError("No request has been selected.");
	print end_form();
	return;
    } 
 
    my $req_type = param1('request_type');
    if ( ! blankStr($req_type) ) {
	print hidden('request_type', $req_type);
    }

    print "<h2>Confirm Deleting Request $request_oid</h2>\n";
    print hidden('request_oid', $request_oid);
 
    my %db_val = SelectRequest($request_oid);
 
    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs = @{$def_req->{attrs}};
 
    print "<p>Please click 'Delete' to confirm the deletion, or click 'Cancel' to cancel the operation.</p>\n";
 
    print "<table class='img' border='1'>\n";
    for my $k ( @attrs ) { 
        my $attr_name = $k->{name};
        my $attr_val = ""; 
        if ( $db_val{$attr_name} ) {
            $attr_val = $db_val{$attr_name};
        }
 
        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type}; 
        my $len = $k->{length};
        my $edit = $k->{can_edit};
 
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
	print escapeHTML($disp_name); 
        print "</th>\n"; 
 
        print "  <td class='img'   align='left'>\n";
 
	if ( $data_type eq 'text' ) {
	    print "<textarea name='$attr_name' " .
		"cols='$len' rows='10' readOnly >" .
		$attr_val . "</textarea>";
	}
	else {
            print escapeHTML($attr_val);
	}

        print "</td>\n"; 
        print "</tr>\n";
    } 
 
    print "</table>\n";
 
    print "<p>\n";
    print '<input type="submit" name="dbDeleteRequest" value="Delete" class="smbutton" />'; 
    print "&nbsp; \n";
    if ( $req_type eq 'new' ) {
	print '<input type="submit" name="showNewRequest" value="Cancel" class="smbutton" />';
    }
    else {
	print '<input type="submit" name="showRequest" value="Cancel" class="smbutton" />';
    }
 
    print "<p>\n";

    printHomeLink();

    print end_form();
}


############################################################################# 
# dbDeleteRequest - delete request_account info from database 
############################################################################# 
sub dbDeleteRequest { 
    # get user info 
    my $userString = param1('userstr'); 
    my ($contact_oid, $uname) = split(/ /, $userString); 

    # get Request_Account definition 
    my $def_req = def_Request_Account(); 
    my @attrs = @{$def_req->{attrs}}; 
 
    my $msg = ""; 
 
    # get request_oid
    my $request_oid = param1('request_oid'); 
    if ( blankStr($request_oid) ) { 
        $msg = "No request has been selected for update."; 
        return $msg; 
    } 
 
    my $sql = "delete from request_account where request_oid = $request_oid";
    my @sqlList = ( $sql ); 
    db_con_sqlTrans(\@sqlList);
 
    return $msg; 
} 



1;
