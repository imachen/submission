package ProjSummary;
my $section = "ProjSummary";

use strict; 
use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
use lib 'lib'; 
use WebEnv;
use WebFunctions; 
use RelSchema;
use ProjectInfo;
use GoldProj;
use GoldMetaProj;

use POSIX;


my $env = getEnv(); 
my $main_cgi = $env->{ main_cgi };
my $section_cgi          = "$main_cgi?section=$section";
my $base_url = $env->{ base_url }; 



#########################################################################
# dispatch 
#########################################################################
sub dispatch { 
    my ($page) = @_; 
 
    if ( $page eq 'showSummary' ) {
	ShowSummary();
    } 
    elsif ( $page eq 'showCategory' ) {
	my $cat_field = param1('cat_field'); 
	my $cat_code = param1('cat_code'); 
	if ( blankStr($cat_code) ) {
	    $cat_code = param1('category');
	}
	ShowSummary($cat_field, $cat_code);
    } 
    elsif ( $page eq 'showChartCategory' ) {
	my @flds = ( 
#		     'body_sample_site', 
		     'cell_shape', 'color',
		     'funding_program', 'habitat_category',
		     'motility', 'ncbi_submit_status', 'oxygen_req',
		     'project_status', 'project_type',
		     'salinity', 'seq_country',
		     'seq_status', 'sporulation',
		     'temp_range', 'type_strain' );
	for my $cat_field ( @flds ) {
	    if ( param1($cat_field) ) {
		my $cat_code = param1($cat_field); 
		ShowSummary($cat_field, $cat_code);
		last;
	    }
	}
    } 
    elsif ( $page eq 'exportSummary' ) {
	ExportSummary();
    } 
    elsif ( $page eq 'exportCatSummary' ) {
	my $cat_field = param1('cat_field'); 
	my $cat_code = param1('cat_code'); 
	ExportSummary($cat_field, $cat_code);
    } 
    elsif ( $page eq 'filterProject' ) { 
	if ( param1('section') eq 'MetaProjSummary' ) {
	    GoldMetaProj::FilterGoldMetaProject();
	  }
	else {
	    GoldProj::FilterGoldProject(); 
	  }
    } 
    elsif ( $page eq 'applyProjectFilter' ) {
        ApplyProjectFilter(); 
 
        ShowSummary(); 
    } 
    else {
	# ShowPage();
    }
}

##?????
#########################################################################
# ShowSummary
#
# cat_field: category field
# cat_code: category code value
##########################################################################
sub ShowSummary {
    my ($cat_field, $cat_code) = @_;

    print start_form(-name=>'mainForm',-method=>'post',action=>"$section_cgi");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 

    my $section = param1('section');
    if ( blankStr($section) ) {
	$section = 'ProjSummary';
    }
    my $genome_type = "Genome";
    my $metag = 0;
    if ( $section eq 'MetaProjSummary' ) {
	$genome_type = "Metagenome";
	$metag = 1;
    }

    if ( ! blankStr($cat_field) ) {
	print hiddenVar('cat_field', $cat_field);
    }
    if ( ! blankStr($cat_code) ) {
	print hiddenVar('cat_code', $cat_code);
    }

    if ( ! blankStr($cat_field) && ! blankStr($cat_code) ) {
	my $def_project = def_Project_Info(); 
	my $cat_attr = $def_project->findAttr($cat_field);
	my $disp_code = $cat_code;
	$disp_code =~ s/_/ /g;

	print "<h2>Selected IMG-GOLD $genome_type Project Summary</h2>\n"; 
	if ( $cat_attr ) {
	    print "<h3>" . $cat_attr->{display_name} . ": " .
		escapeHTML($disp_code) . "</h3>\n";
	}
	else {
	    print "<h3>Project Category ($cat_field): " .
		escapeHTML($disp_code) . "</h3>\n";
	}
    }
    else {
	print "<h2>IMG-GOLD $genome_type Project Summary</h2>\n"; 
    }

#    print "<p><font color='red'><b>WARNING: This page is currently under construction. Some functions may not work.</b></font></p>\n";

    my $summary_disp_option = param1('summary_disp_option');

    my $ck1 = "selected";
    print "<p>Display option:\n";
    print "&nbsp;";
    print "<select name='summary_disp_option' class='img' size='1'>\n"; 
    print "    <option value='all' ";
    if ( blankStr($summary_disp_option) || $summary_disp_option eq 'all' ) {
	print $ck1;
    }
    print ">All projects</option>\n"; 

    print "    <option value='ncbi_pid_only' ";
    if ( $summary_disp_option eq 'ncbi_pid_only' ) {
	print $ck1;
    }
    print ">Only projects with NCBI Project ID</option>\n";

    print "    <option value='genbank_id_only' ";
    if ( $summary_disp_option eq 'genbank_id_only' ) {
	print $ck1;
    }
    print ">Only projects with Genbank ID</option>\n";

    print "    <option value='refseq_only' ";
    if ( $summary_disp_option eq 'refseq_only' ) {
	print $ck1;
    }
    print ">Only projects in RefSeq</option>\n";

    print "    <option value='refseq_no_gene' ";
    if ( $summary_disp_option eq 'refseq_no_gene' ) {
	print $ck1;
    }
    print ">Only projects in RefSeq with no genes</option>\n";

    print "    <option value='img_oid_only' ";
    if ( $summary_disp_option eq 'img_oid_only' ) {
	print $ck1;
    }
    print ">Only projects in IMG</option>\n";

    print "    <option value='jgi_only' ";
    if ( $summary_disp_option eq 'jgi_only' ) {
	print $ck1;
    }
    print ">Only JGI projects</option>\n";

    print "</select>\n";

    # sort option
    my $summary_sort_option = param1('summary_sort_option');
    print "<p>Sort option:\n";
    print "&nbsp;";
    print "<select name='summary_sort_option' class='img' size='1'>\n"; 
    print "    <option value='genus' ";
    if ( blankStr($summary_sort_option) ||
	 $summary_sort_option eq 'genus' ) {
	print $ck1;
    }
    if ( $genome_type eq "Metagenome" ) {
	print ">Order by Habitat, Community, Location</option>\n"; 
    }
    else {
	print ">Order by Genus, Species, Strain</option>\n"; 
    }

#    print "    <option value='body_site' ";
#    if ( $summary_sort_option eq 'body_site' ) {
#	print $ck1;
#    }
#    print ">Order by Body Sample Site</option>\n";

    print "    <option value='ncbi_proj_id' ";
    if ( $summary_sort_option eq 'ncbi_proj_id' ) {
	print $ck1;
    }
    print ">Order by NCBI Project ID</option>\n";

    print "    <option value='img_oid' ";
    if ( $summary_sort_option eq 'img_oid' ) {
	print $ck1;
    }
    print ">Order by IMG Object ID</option>\n";
    print "</select>\n";

    # layout option
    my $summary_layout_option = param1('summary_layout_option');
    print "<p>Layout option:\n";
    print "&nbsp;";
    print "<select name='summary_layout_option' class='img' size='1'>\n"; 
    print "    <option value='all' ";
    if ( blankStr($summary_layout_option) ||
	 $summary_layout_option eq 'fulltable' ) {
	print $ck1;
    }
    print ">Full Table View</option>\n"; 

    print "    <option value='scrollabletable' ";
    if ( $summary_layout_option eq 'scrollabletable' ) {
	print $ck1;
    }
    print ">Scrollable Table View</option>\n";
    print "</select>\n";

    # max display option
    my $summary_max_disp_option = param1('summary_max_display_option');
    if ( blankStr($summary_max_disp_option) ) {
	$summary_max_disp_option = '100';
    }
    print "<p>Maximum Display Rows:\n";
    print "&nbsp;";
    print "<select name='summary_max_display_option' class='img' size='1'>\n"; 
    for my $maxval ( '100', '200', '500', '1000', '2000', '3000',
		     '4000', '5000', '10000', 'all' ) {
	print "    <option value='$maxval' ";
	if ( $summary_max_disp_option eq $maxval ) {
	    print $ck1;
	}

	print ">$maxval</option>\n"; 
    }
    print "</select>\n";

    # query condition
    my $cond = " where " . getProjCond('p', $metag);

    # filter condition
    if ( ! blankStr($cat_field) && ! blankStr($cat_code) ) {
	# category condition
	my $db_code = $cat_code;
	$db_code =~ s/\_/ /g;

	if ( $cat_field eq 'seq_center' ) {
	    # sequencing center
	    if ( $db_code eq 'Unclassified' ) {
		$cond .= " and p.project_oid not in (select project_oid " .
		    "from project_info_data_links p2 " .
		    " where p2.link_type = 'Seq Center' " .
		    " and p2.db_name is not null)"; 
	    }
	    else {
		$cond .= " and p.project_oid in (select project_oid " .
		    "from project_info_data_links p2 " .
		    " where p2.link_type = 'Seq Center' " .
		    " and p2.db_name = '$db_code')"; 
	    }
	}
	elsif ( getAuxTableName($cat_field) ) {
	    # set valued
	    my $aux_table_name = getAuxTableName($cat_field); 
	    my $aux_def = def_Class($aux_table_name);
	    if ( $aux_def ) { 
		if ( $db_code eq 'Unclassified' ) {
		    $cond .= " and p.project_oid not in (select project_oid " .
			"from " . $aux_table_name .
			" where " . lc($cat_field) .
			" is not null)"; 
		}
		else {
		    $cond .= " and p.project_oid in (select project_oid " .
			"from " . $aux_table_name .
			" where " . lc($cat_field) .
			" = '$db_code')"; 
		}
	    }
	}
	else {
	    if ( $db_code eq 'Unclassified' ) {
		$cond .= " and $cat_field is null";
	    }
	    else {
		$cond .= " and p.$cat_field = '$db_code'";
	    }
	}
    }
    else {
	# filter condition
	my $filter_cond;
	if ( $section eq 'MetaProjSummary' ) {
	    $filter_cond = ProjectInfo::projectFilterCondition('meta_');
	}
	else {
	    $filter_cond = ProjectInfo::projectFilterCondition();
	}
	if ( ! blankStr($filter_cond) ) {
	    print "<h5>Search project condition is on.</h5>\n";
	    $cond .= " and " . $filter_cond;
	}
	else {
	    print "<h5>Search project condition is off.</h5>\n";
	}
    }

    my $orderby = " order by 2, 3, 4, 5, 7";

    if ( $summary_disp_option eq 'ncbi_pid_only' ) {
	$cond .= " and p.ncbi_project_id is not null";
    }
    elsif ( $summary_disp_option eq 'genbank_id_only' ) {
	$cond .= " and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where link_type = 'Data' and lower(pidl.db_name) = 'genbank' and id is not null)";
    }
    elsif ( $summary_disp_option eq 'refseq_only' ) {
	$cond .= " and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where lower(pidl.db_name) = 'refseq')";
    }
    elsif ( $summary_disp_option eq 'refseq_no_gene' ) {
	$cond .= " and p.gene_count = 0 and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where lower(pidl.db_name) = 'refseq')";
    }
    elsif ( $summary_disp_option eq 'img_oid_only' ) {
	$cond .= " and p.img_oid is not null";
    }
    elsif ( $summary_disp_option eq 'jgi_only' ) {
	$cond .= " and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where pidl.link_type = 'Seq Center' and lower(pidl.db_name) like '%joint genome institute%')";
    }

#    if ( $summary_sort_option eq 'body_site' ) {
#	$orderby = " order by 5, 2, 3, 4, 7";
#    }

    if ( $summary_sort_option eq 'ncbi_proj_id' ) {
	$orderby = " order by 7, 2, 3, 4, 5";
    }
    elsif ( $summary_sort_option eq 'img_oid' ) {
	$orderby = " order by 12, 2, 3, 4, 5, 7";
    }

    my $dbh = WebFunctions::Connect_IMG();

#    print "<p>cond: $cond</p>\n";
    
    my %seq_center = getProjSeqCenter($dbh, $cond);
    my %genbank_info = getProjGenbankID($dbh, $cond, 1);

#    my $sql = qq{
#        select p.project_oid, p.genus, p.species, p.strain,
#	p.body_sample_site, 
#	p.contact_name, p.ncbi_project_id,
#	p.seq_status, p.project_status, p.hmp_id, p.gold_stamp_id,
#	p.img_oid, p.gene_count, p.ncbi_taxon_id, p.contact_email,
#	p.funding_program, p.project_type,
#	p.ncbi_archive_id, short_read_archive_id
#            from project_info p 
#            $cond
#            $orderby
#        }; 

    my $sql = qq{
        select p.project_oid, p.genus, p.species, p.strain,
	p.contact_name, p.ncbi_project_id,
	p.seq_status, p.project_status, p.hmp_id, p.gold_stamp_id,
	p.img_oid, p.gene_count, p.ncbi_taxon_id, p.contact_email,
	p.funding_program, p.project_type,
	p.ncbi_archive_id, short_read_archive_id
            from project_info p 
            $cond
            $orderby
        }; 
 
#    if ( $contact_oid == 312 ) { 
#        print "<p>SQL: $sql</p>";
#    }
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 

    print "<p>\n"; 
    if ( blankStr($cat_field) ) {
	print "<input type='submit' name='_section_$section:filterProject' value='Search Projects' class='meddefbutton' />"; 
	print "&nbsp; \n"; 
    }
    print "<input type='submit' name='_section_$section:exportSummary_noHeader' value='Export Summary' class='medbutton' />"; 
    print "&nbsp; \n"; 
    if ( blankStr($cat_field) ) {
	print "<input type='submit' name='_section_$section:showSummary' value='Refresh' class='medbutton' />"; 
    }
    else {
	print "<input type='submit' name='_section_$section:showCategory' value='Refresh' class='medbutton' />"; 
    }

    print "<p>\n"; 

    if ( $summary_layout_option eq 'scrollabletable' ) {
	print qq{
	    <link rel="stylesheet" type="text/css"
		href="$base_url/superTables.css" />

		<div class="scroll-table-head"
		style="height: 540px; width: 900px; overflow: hidden; ">
		<table class='img' id='scroll-table'>
	    };
    }
    else {
	print "<table class='img'>\n";
    }

    print qq{
          <tr>
	  <th class='img'>Count</th>
	  <th class='img'>Project ID</th>
      };

    if ( $genome_type eq 'Metagenome' ) {
	print qq{
	    <th class='img'>Habitat (Genus)</th>
		<th class='img'>Community (Species)</th>
		<th class='img'>Location (Strain)</th>
	    };
    }
    else {
	print qq{
	    <th class='img'>Genus</th>
		<th class='img'>Species</th>
		<th class='img'>Strain</th>
	    };
    }

    print qq{
	  <th class='img'>NCBI Taxon ID</th>
	  };

#	  <th class='img'>Body Sample Site</th>

    print qq{
	  <th class='img'>Sequencing Center</th>
	  <th class='img'>Funding Program</th>
	  <th class='img'>NCBI Project ID</th>
	  <th class='img'>Project Status</th>
	  <th class='img'>Sequencing Status</th>
	  <th class='img'>Contact Name</th>
	  <th class='img'>NCBI Archive ID</th>
	  <th class='img'>Short Read Archive ID</th>
	  <th class='img'>GOLD ID</th>
	  <th class='img'>Genbank ID</th>
	  <th class='img'>Gene Count</th>
      };
#	  <th class='img'>IMG Project ID</th>

    print qq{
          </tr>
      };

    ## set up the table rows (tr, td, etc.) here ...
    my $cnt = 0;
    for (;;) { 
        my ( $proj_id, $genus, $species, $strain,
#             $body_sample_site, 
	     $contact_name, $ncbi_project_id,
	     $seq_status, $proj_status, $hmp_id, $gold_stamp_id,
	     $img_oid, $gene_count, $ncbi_taxon_id, $contact_email,
	     $funding_prog, $project_type,
	     $ncbi_archive_id, $short_read_archive_id) =
                 $cur->fetchrow_array();
        if ( ! $proj_id ) { 
            last; 
        }

	$cnt++;
	if ( isInt($summary_max_disp_option) &&
	     $cnt > $summary_max_disp_option ) {
	    last;
	}
	elsif ( $cnt > 100000 ) {
	    last;
	}

        print "<tr class='img'>\n";
	PrintAttribute($cnt);

	my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link); 

	PrintAttribute($genus);
	PrintAttribute($species);
#	PrintAttribute($strain);
	PrintStrainInfo($strain);

        if ( $ncbi_taxon_id ) { 
            PrintAttribute(getNcbiTaxonLink($ncbi_taxon_id));
        } 
        else { 
            PrintAttribute($ncbi_taxon_id);
        }

#	PrintAttribute($body_sample_site);

	# sequencing center
	if ( $seq_center{$proj_id} ) {
	    PrintAttribute($seq_center{$proj_id});
	}
	else {
	    PrintAttribute(" ");
	}

	PrintAttribute($funding_prog);

        if ( $ncbi_project_id ) { 
            PrintAttribute(getNcbiProjLink($ncbi_project_id));
        } 
        else { 
            PrintAttribute($ncbi_project_id);
        }

#	PrintAttribute($project_type);

	PrintAttribute($proj_status);
	PrintAttribute($seq_status);

	if ( blankStr($contact_email) ) {
	    PrintAttribute($contact_name);
	}
	else {
	    my $c_mail = "<a href=\"mailto:" . $contact_email .
		"\">" . $contact_name . "</a>";
	    PrintAttribute($c_mail);
	}

	PrintAttribute($ncbi_archive_id);

	if ( $short_read_archive_id ) {
	    my $sr_url = "http://www.ncbi.nlm.nih.gov/" .
		"sites/entrez?db=sra&term=" . $short_read_archive_id;
	    my $sr_link = "<a href='" . $sr_url . 
		"' target='view_link'>" . $short_read_archive_id .
		"</a>"; 
	    PrintAttribute($sr_link);
	}
	else {
	    PrintAttribute($short_read_archive_id);
	}

        my $gold_link = getGoldLink($gold_stamp_id); 
        PrintAttribute($gold_link); 

#	my $img_hmp_link = getImgHmpLink($img_oid);
#	if ( $contact_oid == 312 ) {
#	    PrintAttribute($img_hmp_link);
#	}
#	else {
#	    PrintAttribute($img_oid);
#	}

	# genbank ID
	if ( $genbank_info{$proj_id} ) {
	    PrintAttribute($genbank_info{$proj_id});
	}
	else {
	    PrintAttribute(" ");
	}

	# gene count
	PrintAttribute($gene_count);

#	my $proj_link = getProjectLink($proj_id);
#        PrintAttribute($proj_link); 

	print "</tr>\n";
    }

    print "</table>\n";

    if ( $summary_layout_option eq 'scrollabletable' ) {
	print "</div>\n";

	print "<script src='$base_url/superTables.js'></script>\n";
	print qq{
	    <script type="text/javascript">
    //<![CDATA[
	       (function() {
		 var mySt = new superTable("scroll-table", {
               headerRows : 1,
               onStart : function () {
                   this.start = new Date();
               },
               onFinish : function () {
               }
           });
	     })();
	       //]]>
		   </script>
	       }; 
    }

    if ( isInt($summary_max_disp_option) &&
	 $cnt > $summary_max_disp_option ) {
	print "<p><font color='blue'>Too many rows. Only $summary_max_disp_option rows are displayed.</font></p>\n";
    }
    else {
	print "<p><font color='blue'>Count: $cnt</font></p>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p>\n"; 
    if ( blankStr($cat_field) ) {
	print "<input type='submit' name='_section_$section:filterProject' value='Search Projects' class='meddefbutton' />"; 
	print "&nbsp; \n"; 
    }
    print "<input type='submit' name='_section_$section:exportSummary_noHeader' value='Export Summary' class='medbutton' />"; 
    print "&nbsp; \n"; 
    if ( blankStr($cat_field) ) {
	print "<input type='submit' name='_section_$section:showSummary' value='Refresh' class='medbutton' />"; 
    }
    else {
	print "<input type='submit' name='_section_$section:showCategory' value='Refresh' class='medbutton' />"; 
    }

    # Home 
    print "<p>\n";
    printHomeLink();

    print end_form();
}


###########################################################################
# getProjSeqCenter
###########################################################################
sub getProjSeqCenter {
    my ( $dbh, $cond ) = @_; 

    my %seq_center;

    my $sql = qq{ 
	select distinct p.project_oid, pidl.db_name
	    from project_info p, project_info_data_links pidl
	    $cond
	    and p.project_oid = pidl.project_oid
	    and pidl.link_type = 'Seq Center'
	    and pidl.db_name is not null
	    order by 1, 2
	};

#    print "<p>SQL: $sql</p>\n";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $p_id = 0;
    my $s1 = "";

    for (;;) { 
        my ( $proj_id, $seq_c ) = $cur->fetchrow_array();
        if ( ! $proj_id ) { 
            last; 
        }

	if ( $proj_id != $p_id ) {
	    # new one
	    if ( $p_id > 0 && ! blankStr($s1) ) {
		$seq_center{$p_id} = $s1;
	    }

	    # save
	    $p_id = $proj_id;
	    $s1 = $seq_c;
	}
	else {
	    # same one
	    if ( blankStr($s1) ) {
		$s1 = $seq_c;
	    }
	    else {
		$s1 .= ", " . $seq_c;
	    }
	}
    }

    $cur->finish();

    # last one
    if ( $p_id > 0 && ! blankStr($s1) ) {
	$seq_center{$p_id} = $s1;
    }

    return %seq_center;
}


###########################################################################
# getProjGenbankID
###########################################################################
sub getProjGenbankID {
    my ( $dbh, $cond, $include_url ) = @_; 

    my %genbank_info;

    my $sql = qq{ 
	select distinct p.project_oid, pidl.id, pidl.url
	    from project_info p, project_info_data_links pidl
	    $cond
	    and p.project_oid = pidl.project_oid
	    and pidl.link_type = 'Data'
	    and lower(pidl.db_name) = 'genbank'
	    order by 1, 2
	};

#    print "<p>SQL: $sql</p>\n";
webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $p_id = 0;
    my $s1 = "";

    for (;;) { 
        my ( $proj_id, $genbank_id, $genbank_url ) = $cur->fetchrow_array();
        if ( ! $proj_id ) { 
            last; 
        }

	my $genbank_link = "N/A";
	if ( $genbank_id ) {
	    $genbank_link = $genbank_id;
	}
	if ( $include_url && $genbank_url ) {
	    $genbank_link = "<a href='" . $genbank_url .
		"' target='view_link'>" . $genbank_link . "</a>";
	}

	if ( $proj_id != $p_id ) {
	    # new one
	    if ( $p_id > 0 && ! blankStr($s1) ) {
		$genbank_info{$p_id} = $s1;
	    }

	    # save
	    $p_id = $proj_id;
	    $s1 = $genbank_link;
	}
	else {
	    # same one
	    if ( blankStr($s1) == 0 ) {
		$s1 = $genbank_link;
	    }
	    else {
		$s1 .= ", " . $genbank_link;
	    }
	}
    }

    $cur->finish();

    # last one
    if ( $p_id > 0 && ! blankStr($s1) ) {
	$genbank_info{$p_id} = $s1;
    }

    return %genbank_info;
}


###########################################################################
# PrintStrainInfo
###########################################################################
sub PrintStrainInfo { 
    my ( $val ) = @_; 
 
    if ( defined($val) ) { 
	my @vals = split(/\,/, $val);
	my $cnt1 = 0;
	for my $v1 ( @vals ) {
	    if ( blankStr($v1) ) {
		next;
	    }

	    $cnt1++;
	    if ( $cnt1 == 1 ) {
		print "<td class='img' align='left'>";
	    }
	    else {
		print ", ";
	    }

	    $v1 = strTrim($v1);
	    if ( $v1 =~ /^ATCC(\s*)(\d+)/ ) {
		# ATCC
		my $atcc_id = $2;
#		my $atcc_url = "https://www.lgcstandards-atcc.org/" .
#		    "ShoppingCart/tabid/537/ATCCNum/" . $atcc_id .
#		    "/Default.aspx";
		my $atcc_url = "http://www.lgcstandards-atcc.org/" .
		    "LGCAdvancedCatalogueSearch/ProductDescription/" .
		    "tabid/1068/Default.aspx?ATCCNum=" .
		    $atcc_id . "&Template=bacteria";
		my $atcc_link = "<a href='" . $atcc_url . 
		    "' target='view_link'>" . $v1 . "</a>"; 
		print $atcc_link;
	    }
	    elsif ( $v1 =~ /^DSM(\s*)(\d+)/ ) {
		# DSM
		my $dsm_id = $2;
		my $dsm_url = "http://www.dsmz.de/microorganisms/" .
		    "html/strains/strain.dsm" .
		    sprintf("%06d", $dsm_id) . ".html";
		my $dsm_link = "<a href='" . $dsm_url . 
		    "' target='view_link'>" . $v1 . "</a>"; 
		print $dsm_link;
	    }
	    else {
		print $v1;
	    }
	}

	if ( $cnt1 > 0 ) {
	    print "</td>\n";
	}
	else {
	    print "<td class='img' align='left'> </td>\n";
	}
    } 
    else {
        print "<td class='img' align='left'> </td>\n";
    } 
} 


########################################################################## 
# ExportSummary
########################################################################## 
sub ExportSummary {
    my ($cat_field, $cat_code) = @_;

    my $summary_disp_option = param1('summary_disp_option');
    my $summary_sort_option = param1('summary_sort_option');
    my $summary_max_disp_option = param1('summary_max_display_option');

    my $section = param1('section');
    my $metag = 0;
    if ( $section eq 'MetaProjSummary' ) {
	$metag = 1;
    }
    my $cond = " where " . getProjCond('p', $metag);

    if ( ! blankStr($cat_field) && ! blankStr($cat_code) ) {
	# category condition
	my $db_code = $cat_code;
	$db_code =~ s/\_/ /g;
	if ( $db_code eq 'Unclassified' ) {
	    $cond .= " and p.$cat_field is null";
	}
	else {
	    $cond .= " and p.$cat_field = '$db_code'";
	}
    }
    else {
	# filter condition
	my $filter_cond;
	if ( $section eq 'MetaProjSummary' ) {
	    $filter_cond = ProjectInfo::projectFilterCondition('meta_');
	}
	else {
	    $filter_cond = ProjectInfo::projectFilterCondition();
	}

	if ( ! blankStr($filter_cond) ) {
	    $cond .= " and " . $filter_cond;
	}
    }

    my $orderby = " order by 2, 3, 4, 5, 7";

    if ( $summary_disp_option eq 'ncbi_pid_only' ) {
	$cond .= " and p.ncbi_project_id is not null";
    }
    elsif ( $summary_disp_option eq 'genbank_id_only' ) {
	$cond .= " and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where link_type = 'Data' and lower(pidl.db_name) = 'genbank' and id is not null)";
    }
    elsif ( $summary_disp_option eq 'refseq_only' ) {
	$cond .= " and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where lower(pidl.db_name) = 'refseq')";
    }
    elsif ( $summary_disp_option eq 'refseq_no_gene' ) {
	$cond .= " and p.gene_count = 0 and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where lower(pidl.db_name) = 'refseq')";
    }
    elsif ( $summary_disp_option eq 'img_oid_only' ) {
	$cond .= " and p.img_oid is not null";
    }
    elsif ( $summary_disp_option eq 'jgi_only' ) {
	$cond .= " and p.project_oid in (select pidl.project_oid from project_info_data_links pidl where pidl.link_type = 'Seq Center' and lower(pidl.db_name) like '%joint genome institute%')";
    }

#    if ( $summary_sort_option eq 'body_site' ) {
#	$orderby = " order by 5, 2, 3, 4, 7";
#    }

    if ( $summary_sort_option eq 'ncbi_proj_id' ) {
	$orderby = " order by 7, 2, 3, 4, 5";
    }
    elsif ( $summary_sort_option eq 'img_oid' ) {
	$orderby = " order by 12, 2, 3, 4, 5, 7";
    }

    my $dbh = WebFunctions::Connect_IMG();

    my %seq_center = getProjSeqCenter($dbh, $cond);
    my %genbank_info = getProjGenbankID($dbh, $cond, 0);

#    my $sql = qq{
#        select p.project_oid, p.genus, p.species, p.strain,
#	p.body_sample_site, 
#	p.contact_name, p.ncbi_project_id,
#	p.seq_status, p.project_status,
#	p.hmp_id, p.gold_stamp_id, p.img_oid, p.gene_count,
#	p.ncbi_taxon_id, p.funding_program, p.project_type,
#	p.ncbi_archive_id, p.short_read_archive_id
#            from project_info p 
#            $cond
#            order by 2, 3, 4, 5, 7
#        }; 

    my $sql = qq{
        select p.project_oid, p.genus, p.species, p.strain,
	p.contact_name, p.ncbi_project_id,
	p.seq_status, p.project_status,
	p.hmp_id, p.gold_stamp_id, p.img_oid, p.gene_count,
	p.ncbi_taxon_id, p.funding_program, p.project_type,
	p.ncbi_archive_id, p.short_read_archive_id
            from project_info p 
            $cond
            order by 2, 3, 4, 5, 7
        }; 
webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    # print Excel Header 
    my $fileName = "imggold_export$$.xls"; 
    print "Content-type: application/vnd.ms-excel\n"; 
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n"; 

    # header
    if ( $metag ) {
	print "Project ID\tHabitat (Genus)\tCommunity (Species)\tLocation (Strain)\t";
    }
    else {
	print "Project ID\tGenus\tSpecies\tStrain\t";
    }
    print "NCBI Taxon ID\t";
#    print "Body Sample Site\t";
    print "Sequencing Center\tFunding Program\t";
    print "NCBI Project ID\t";
#    print "Project Type\t";
    print "Project Status\tSequencing Status\t";
    print "Contact Name\tNCBI Archive ID\tShort Read Archive ID\t";
    print "GOLD ID\t";
#    print "IMG OID\t";
#    print "Gene Count\tIMG Project ID\n";
    print "Genbank ID\tGene Count\n";

    my $cnt = 0;
    for (;;) { 
        my ( $proj_id, $genus, $species, $strain,
#             $body_sample_site, 
	     $contact_name, $ncbi_project_id,
	     $seq_status, $proj_status,
	     $hmp_id, $gold_id, $img_oid, $gene_count,
	     $ncbi_taxon_id, $funding_prog, $project_type,
	     $ncbi_archive_id, $short_read_archive_id) =
                 $cur->fetchrow_array();
        if ( ! $proj_id ) { 
            last; 
        }

	$cnt++;
	if ( isInt($summary_max_disp_option) &&
	     $cnt > $summary_max_disp_option ) {
	    last;
	}
	elsif ( $cnt > 100000 ) {
	    last;
	}

	print "$proj_id\t$genus\t$species\t$strain\t";
	print "$ncbi_taxon_id\t";
#	print "$body_sample_site\t";

	if ( $seq_center{$proj_id} ) {
	    print $seq_center{$proj_id};
	}
	print "\t";

	print "$funding_prog\t$ncbi_project_id\t";
#	print "$project_type\t";
	print "$proj_status\t$seq_status\t";
	print "$contact_name\t$ncbi_archive_id\t$short_read_archive_id\t";
	print "$gold_id\t";
#	print "$img_oid\t";
#	print "$gene_count\t$proj_id\n";

	# genbank ID
	if ( $genbank_info{$proj_id} ) {
	    print $genbank_info{$proj_id};
	}
	print "\t";

	print "$gene_count\n";
    }

    $cur->finish();
    $dbh->disconnect();

    exit 0;
}



1;

