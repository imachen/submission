package HmpStats;

use strict;
use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';
use FileHandle;
use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;
use ChartUtil;


my $env = getEnv();
my $base_url = $env->{ base_url };
my $tmp_url = $env->{ tmp_url };
my $tmp_dir = $env->{ tmp_dir };


#########################################################################
# dispatch
#########################################################################
sub dispatch {
    my ($page) = @_;
 
    if ( $page eq 'showSummary' ) {
        ShowStatsMain();
    }
    elsif ( $page eq 'printPieChart' ) {
	my $graph_fld = param1('graph_fld_option');
	my $include_null = param1('include_null');
	PrintPieChart($graph_fld, $include_null);
    }
    elsif ( $page eq 'displayPieChart' ) {
	my $graph_fld = param1('graph_fld_option');
	DisplayPieChartPage($graph_fld);
    }
    elsif ( $page eq 'displayBarChart' ) {
	my $graph_fld = param1('graph_fld_option');
	DisplayBarChartPage($graph_fld);
    }
    else {
        ShowStatsMain();
    }
}


#########################################################################
# ShowStatsMain
##########################################################################
sub ShowStatsMain {
    my $contact_oid = getContactOid();
 
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url");
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password");
    }
    elsif ( ! getIsHmpUser($contact_oid) ) {
        dienice("You are not HMP user.");
    } 

    print "<p><font color='red'><b>WARNING: This page is currently under construction.</b></font></p>\n";

#    if ( $contact_oid != 312 &&
#	 $contact_oid != 11 &&
#	 $contact_oid != 17 ) {
#	print "<p>\n";
#	printHomeLink(); 
 
#	print endform();
#	return;
#    }

    print "<h2>HMP Statistics</h2>\n";

    DisplayStatAttrList ();

    print qq{
	    <div id="graph-div" style="left: 170px; position: absolute; top: 40px;">
	};
    print "<p>" . nbsp(3) . "<b>Please select a field.</b></p>\n";

    print qq{
	</div>
	};

    return;

    #### old version
    print "<p>Please select a category:</p>\n";

    print "<select name='graph_fld_option' class='img' size='1'>\n"; 
    print "    <option value=''> </option>\n"; 
    print "    <option value='body_sample_site'>Body Sample Site</option>\n"; 
    print "    <option value='cell_shape'>Cell Shape</option>\n"; 
    print "    <option value='color'>Color</option>\n"; 
    print "    <option value='funding_program'>Funding Program</option>\n"; 
    print "    <option value='habitat_category'>Habitat Category</option>\n"; 
    print "    <option value='motility'>Motility</option>\n"; 
    print "    <option value='ncbi_submit_status'>NCBI Submission Status</option>\n"; 
    print "    <option value='project_status'>Project Status</option>\n"; 
    print "    <option value='project_type'>Project Type</option>\n";
    print "    <option value='salinity'>Salinity</option>\n"; 
    print "    <option value='seq_country'>Sequencing Country</option>\n"; 
    print "    <option value='seq_status'>Sequencing Status</option>\n";
    print "    <option value='sporulation'>Sporulation</option>\n";
    print "    <option value='temp_range'>Temperature Range</option>\n";
    print "</select>\n"; 
    print "<p/>\n"; 

    print "<p>\n";
    print "<input type='checkbox' name='include_null' value='1' checked>";
    print nbsp(2);
    print "Include unclassified values?\n";
    print "</p>\n";

    print '<input type="submit" name="_section_HmpStats:printPieChart" value="Show Statistics" class="meddefbutton" />'; 


    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print endform();
} 


#########################################################################
# DisplayPieChartPage
##########################################################################
sub DisplayPieChartPage {
    my ( $graph_fld ) = @_;

    my $url=url(); 
 
#    print startform(-name=>'mainForm',-method=>'post',action=>"$url");

    print "<p><font color='red'><b>WARNING: This page is currently under construction.</b></font></p>\n";

    print "<h2>HMP Statistics</h2>\n";

    my $contact_oid = getContactOid();

#    if ( $contact_oid != 312 &&
#	 $contact_oid != 11 &&
#	 $contact_oid != 17 ) {
#	print "<p>\n";
#	printHomeLink(); 
# 
#	print endform();
#	return;
#    }

    DisplayPieChartContent($graph_fld);
    print "<p>\n";

    # Home 
    print "<p>\n";
    printHomeLink(); 

    print endform();
}


#########################################################################
# DisplayStatAttrList
#
# display attribute list section for selection
##########################################################################
sub DisplayStatAttrList {

    my @flds = getSingleStatsAttrs ();
    my $def_project = def_Project_Info();

    print qq{
	<div >
	    <table class="img" width='140'>
	    <th bgcolor='lightblue'>Statistics Field</th>
	};

    for my $fld1 ( @flds ) {
	my $attr = $def_project->findAttr($fld1);
	print "<tr class='img' bgcolor='lightblue'>\n";
	my $url = "hmp.cgi?section=HmpStats&page=displayPieChart&graph_fld_option=$fld1";
	print "<td>";
	print "<a href='$url'>" . $attr->{display_name} .
	    "</a></td></tr>\n";
    }

    print "</table>\n";

    my $contact_oid = getContactOid();
    if ( $contact_oid != 312 ) {
	print "</div>\n";
	return;
    }

    print qq{
	</p>
	    <table class="img" width='140'>
	    <th bgcolor='#bbbbbb'>Bar Chart</th>
	};

    my @set_attrs = ( 'seq_center' );
    for my $fld1 ( @set_attrs ) {
	print "<tr class='img' bgcolor='#bbbbbb'>\n";
	my $url = "hmp.cgi?section=HmpStats&page=displayBarChart&graph_fld_option=$fld1";
	print "<td>";
	print "<a href='$url'>" . "Sequencing Center" .
	    "</a></td></tr>\n";
    }

    print "</table>\n";

    print "</div>\n";
}


#########################################################################
# DisplayPieChartContent
##########################################################################
sub DisplayPieChartContent {
    my ( $graph_fld ) = @_;

#    print "<p>\n";
#    print "<input type='checkbox' name='include_null' value='1'";
#    if ( $include_null ) {
#	print " checked ";
#    }
#    print ">";
#    print nbsp(1);
#    print "Include unclassified values?\n";
#    print "</p>\n";

    DisplayStatAttrList ();

    print qq{
	    <div id="graph-div" style="left: 170px; position: absolute; top: 40px;">
	};

    PrintPieChart($graph_fld, 0);

    print qq{
	</div>
	};


}


############################################################################ 
# getSingleStatsAttrs
############################################################################ 
sub getSingleStatsAttrs { 

    my @flds = ( 'body_sample_site', 'cell_shape', 'color',
		 'funding_program', 'habitat_category',
		 'motility', 'ncbi_submit_status', 'oxygen_req',
		 'project_status', 'project_type',
		 'salinity', 'seq_country',
		 'seq_status', 'sporulation',
		 'temp_range' );

    return @flds;
}


############################################################################ 
# PrintPieChart
############################################################################ 
sub PrintPieChart { 
    my ($graph_fld, $include_null) = @_;

    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url");

    if ( blankStr($graph_fld) ) {
	print "<p>" . nbsp(3) . "<b>Please select a field.</b></p>\n";
	return;
    }

    my $def_project = def_Project_Info();
    my $attr = $def_project->findAttr($graph_fld);
    if ( ! $attr ) {
	print "<h3>Incorrect attribute $graph_fld</h3>\n";
	return;
    }

    my $url2 = "hmp.cgi?section=HmpSummary&page=showChartCategory";
 
    print "<h1>HMP Categories: " . $attr->{display_name} .
	"</h1>\n"; 

    #### PREPARE THE PIECHART ###### 
    
    my $chart = newPieChart();
    $chart->WIDTH(300); 
    $chart->HEIGHT(300); 
    $chart->INCLUDE_LEGEND("no"); 
    $chart->INCLUDE_TOOLTIPS("yes"); 
    $chart->INCLUDE_URLS("yes"); 
    $chart->ITEM_URL($url2); 
    $chart->INCLUDE_SECTION_URLS("yes"); 
    $chart->URL_SECTION_NAME($graph_fld); 

    # file prefix
    my $fileprefix = "";
    my $p_count = 1000;
    my @flds = getSingleStatsAttrs();
    for my $f1 ( @flds ) {
	$p_count++;
	if ( $graph_fld eq $f1 ) {
	    $fileprefix = $p_count;
	    if ( ! $include_null ) {
		$fileprefix .= '0';
	    } 
	    else {
		$fileprefix .= '1';
	    }

	    last;
	}
    }
 
    if ( ! blankStr($fileprefix) ) {
	$chart->FILE_PREFIX( $fileprefix ); 
	$chart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix ); 
    }

#    print "<p>FILE_PREFIX: " . $chart->FILE_PREFIX . "</p>\n";

#    my $contact_oid = getContactOid();
#    if ( $contact_oid == 312 ) {
#	print "<p>FILEPATH_PREFIX: " . $chart->FILEPATH_PREFIX . "</p>\n";
#    }

    my @chartseries; 
    my @chartcategories; 
    my @functioncodes; 
    my @chartdata; 

   ################################# 


    my $hmpcond = getHmpCond();

    my $dbh = Connect_IMG();

    # get categories
    my $sql = qq{
	select p.$graph_fld, count(*)
	    from project_info p
	    where $hmpcond
	    and p.$graph_fld is not null
	    group by p.$graph_fld
	};

    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    print "<table width=800 border=0>\n";
    print "<tr>"; 
    print "<td valign=top>\n";
 
    print "<table class='img'  border='1'>\n";
    print "<th class='img' >" . $attr->{display_name} . "</th>\n";
    print "<th class='img' >Count</th>\n";
    print "<th class='img' >Percentage</th>\n";
    my $count = 0;
    my $total = 0;
    for( ;; ) { 
	my( $function_code, $f_count ) = $cur->fetchrow( );
	last if !$function_code;
	$count++; 

	if ( $count > 1000 ) {
	    last;
	}

	push @chartcategories, "$function_code";
	push @functioncodes, "$function_code";
	push @chartdata, $f_count;

	$total += $f_count;
    } 
    $cur->finish();

    # get unclassified
    my $sql0 = qq{
	select count(*)
	    from project_info
	    where project_oid in 
	    (select pipr.project_oid 
	     from project_info_project_relevance pipr 
	     where pipr.project_relevance = 'Human Microbiome Project (HMP)')
	    and $graph_fld is null
	};

    $cur=$dbh->prepare($sql0);
    $cur->execute();
    my( $f_count0 ) = $cur->fetchrow( );
    $cur->finish();

    if ( $include_null ) {
	push @chartcategories, "Unclassified";
	push @functioncodes, "Unclassified";
	push @chartdata, $f_count0;
	$total += $f_count0;
    }

    push @chartseries, "count"; 
    $chart->SERIES_NAME(\@chartseries); 
    $chart->CATEGORY_NAME(\@chartcategories);
    $chart->URL_SECTION(\@functioncodes);
    my $datastr = join(",", @chartdata);
    my @datas = ($datastr);
    $chart->DATA(\@datas); 
 
    my $st = -1; 
    my $cmd = "";

#    print "<p>chart exe: " . $env->{ chart_exe } . "</p>\n";

    if ($env->{ chart_exe } ne "") {
	$st = generateChart($chart); 
    } 

    my $idx=0;
    for my $category1 ( @chartcategories ) {
	last if !$category1; 

        my $disp_key = $category1; 
        $disp_key =~ s/\s/\_/g; 
	my $url = "hmp.cgi?section=HmpSummary&page=showCategory&cat_field=$graph_fld&cat_code=$disp_key";

	print "<tr class='img' >\n"; 
	print "<td class='img' >\n";
 
	if ($st == 0) {
	    print "<a href='$url'>"; 
	    my $fname = $chart->FILE_PREFIX . "-color-" . $idx .
		".png";
	    print "<img src='$tmp_url/". $fname . "' border=0>"; 
	    print "</a>";
	    print "&nbsp;&nbsp;";
	} 
	print escapeHTML( $category1 );
	print "</td>\n"; 
	print "<td class='img' align='right'>\n";
	if ( $chartdata[$idx] ) {
	    print alink( $url, $chartdata[$idx] );
	}
	else {
	    print "0";
	}

	print "</td>\n"; 

	# percentage
	if ( $total > 0 ) {
	    if ( $chartdata[$idx] ) {
		my $perc = int(($chartdata[$idx] * 100 / $total) + .5);
		print "<td class='img' align='right'>" . $perc . "%</td>\n";
	    }
	    else {
		print "<td class='img' align='right'>0%</td>\n";
	    }
	}
	else {
	    print "<td class='img' align='right'>-</td>\n";
	}

	print "</tr>\n"; 
	$idx++;
    }
 
    print "</table>\n"; 
    print "</td>\n"; 
    print "<td valign=top align=left>\n";
 
   ###########################
    if ($env->{ chart_exe } ne "") {
	if ( $st == 0 ) {
	    print "<script src='$base_url/overlib.js'></script>\n";
	    my $filename = $chart->FILEPATH_PREFIX . ".html";
	    my $fh = newReadFileHandle($filename, "printPieChart",1); 
#	    print "<p>File: $filename</p>\n";
	    if ( $fh ) {
		while (my $s = $fh->getline()) {
		    print $s;
		} 
		close ($fh); 
	    }

	    my $pngname = $chart->FILE_PREFIX . ".png";
	    print "<img src='$tmp_url/" . $pngname . "' BORDER=0 ";
	    print " width=".$chart->WIDTH." HEIGHT=".$chart->HEIGHT;
	    print " USEMAP='#" . $chart->FILE_PREFIX . "'>\n";
	} 
    } 
   ###########################  

    $dbh->disconnect( ); 

    print "<p>\n";
 
    print "</td></tr>\n"; 
    print "</table>\n";

    if ( ! $include_null ) {
	if ( $f_count0 ) {
	    my $url = "hmp.cgi?section=HmpSummary&page=showCategory&cat_field=$graph_fld&cat_code=Unclassified";
	    print "<p>Unclassified: <a href='$url'>$f_count0</a></p>\n";
	}
	else {
	    print "<p>Unclassified: $f_count0</p>\n";
	}
    }

} 


#########################################################################
# DisplayBarChartPage
##########################################################################
sub DisplayBarChartPage {
    my ( $graph_fld ) = @_;

    my $url=url(); 
 
#    print startform(-name=>'mainForm',-method=>'post',action=>"$url");

    print "<p><font color='red'><b>WARNING: This page is currently under construction.</b></font></p>\n";

    print "<h2>HMP Statistics</h2>\n";

    my $contact_oid = getContactOid();

    if ( $contact_oid != 312 &&
	 $contact_oid != 11 &&
	 $contact_oid != 17 ) {
	print "<p>\n";
	printHomeLink(); 
 
	print endform();
	return;
    }

    DisplayBarChartContent($graph_fld);
    print "<p>\n";

    # Home 
    print "<p>\n";
    printHomeLink(); 

    print endform();
}


#########################################################################
# DisplayBarChartContent
##########################################################################
sub DisplayBarChartContent {
    my ( $graph_fld ) = @_;

#    print "<p>\n";
#    print "<input type='checkbox' name='include_null' value='1'";
#    if ( $include_null ) {
#	print " checked ";
#    }
#    print ">";
#    print nbsp(1);
#    print "Include unclassified values?\n";
#    print "</p>\n";

    DisplayStatAttrList ();

    print qq{
	    <div id="graph-div" style="left: 170px; position: absolute; top: 40px;">
	};

    PrintBarChart($graph_fld, 0);

    print qq{
	</div>
	};


}


############################################################################ 
# getSetStatsAttrs
############################################################################ 
sub getSetStatsAttrs { 

    my @flds = ( 'seq_center' );

    return @flds;
}


############################################################################ 
# PrintBarChart
############################################################################ 
sub PrintBarChart { 
    my ($graph_fld, $include_null) = @_;

    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url");

    if ( blankStr($graph_fld) ) {
	print "<p>" . nbsp(3) . "<b>Please select a field.</b></p>\n";
	return;
    }

    my $attr_disp_name = "";
    if ( $graph_fld eq 'seq_center' ) {
	$attr_disp_name = "Sequencing Center";
    }
    else {
	my $def_project = def_Project_Info();
	my $attr = $def_project->findAttr($graph_fld);
	if ( ! $attr ) {
	    print "<h3>Incorrect attribute 2 $graph_fld</h3>\n";
	    return;
	}
	$attr_disp_name = $attr->{display_name};
    }

    print "<h1>HMP Categories: " . $attr_disp_name . "</h1>\n"; 

    my $url2 = "hmp.cgi?section=HmpSummary&page=showChartCategory";

   # PREPARE THE BAR CHART 
    my $chart = newBarChart(); 
    $chart->WIDTH(450); 
    $chart->HEIGHT(300); 
    $chart->DOMAIN_AXIS_LABEL($attr_disp_name);
    $chart->RANGE_AXIS_LABEL("Number of projects"); 
    $chart->INCLUDE_TOOLTIPS("yes"); 
    $chart->INCLUDE_URLS("yes"); 
    $chart->ITEM_URL($url."&chart=y"); 
   #$chart->ROTATE_DOMAIN_AXIS_LABELS("yes"); 
    $chart->COLOR_THEME("ORANGE"); 

    # file prefix
    my $fileprefix = "";
    my $p_count = 2000;
    my @flds = getSingleStatsAttrs();
    for my $f1 ( @flds ) {
	$p_count++;
	if ( $graph_fld eq $f1 ) {
	    $fileprefix = $p_count;
	    if ( ! $include_null ) {
		$fileprefix .= '0';
	    } 
	    else {
		$fileprefix .= '1';
	    }

	    last;
	}
    }
 
    if ( ! blankStr($fileprefix) ) {
	$chart->FILE_PREFIX( $fileprefix ); 
	$chart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix ); 
    }

#    print "<p>FILE_PREFIX: " . $chart->FILE_PREFIX . "</p>\n";

#    my $contact_oid = getContactOid();
#    if ( $contact_oid == 312 ) {
#	print "<p>FILEPATH_PREFIX: " . $chart->FILEPATH_PREFIX . "</p>\n";
#    }

    my @chartseries; 
    my @chartcategories; 
    my @functioncodes; 
    my @chartdata; 

   ################################# 

    my $hmpcond = getHmpCond('pidl');

    my $dbh = Connect_IMG();

    # get categories
    my $sql = qq{
	select pidl.db_name, count(*)
	    from project_info_data_links pidl
	    where $hmpcond
	    and pidl.link_type = 'Seq Center'
	    and pidl.db_name is not null
	    group by pidl.db_name
	};

#    print "<p>SQL: $sql</p>\n";

    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    print "<table width=800 border=0>\n";
    print "<tr>"; 
    print "<td valign=top>\n";
 
    my $count = 0;
    my $total = 0;
    for( ;; ) { 
	my( $function_code, $f_count ) = $cur->fetchrow( );
	last if !$function_code;
	$count++; 

	if ( $count > 1000 ) {
	    last;
	}

	push @chartcategories, "$function_code";
	push @functioncodes, "$function_code";
	push @chartdata, $f_count;

	$total += $f_count;
    } 
    $cur->finish();

    # get unclassified
    my $sql0 = qq{
	select count(*)
	    from project_info
	    where project_oid in 
	    (select pipr.project_oid 
	     from project_info_project_relevance pipr 
	     where pipr.project_relevance = 'Human Microbiome Project (HMP)')
	    and project_oid not in
	    (select pidl.project_oid
	     from project_info_data_links pidl
	     where pidl.link_type = 'Seq Center')
	};

#    print "<p>SQL: $sql0</p>\n";

    $cur=$dbh->prepare($sql0);
    $cur->execute();
    my( $f_count0 ) = $cur->fetchrow( );
    $cur->finish();

    if ( $include_null ) {
	push @chartcategories, "Unclassified";
	push @functioncodes, "Unclassified";
	push @chartdata, $f_count0;
	$total += $f_count0;
    }

    # display table data
    print "<table class='img'  border='1'>\n";
    print "<th class='img' >" . $attr_disp_name . "</th>\n";
    print "<th class='img' >Count</th>\n";
    print "<th class='img' >Percentage</th>\n";

    my $idx=0;
    for my $category1 ( @chartcategories ) {
	last if !$category1; 

        my $disp_key = $category1; 
        $disp_key =~ s/\s/\_/g; 
	my $url = "hmp.cgi?section=HmpSummary&page=showCategory&cat_field=$graph_fld&cat_code=$disp_key";

	print "<tr class='img' >\n"; 
	print "<td class='img' >\n";
 
#	print "<a href='$url'>"; 
#	my $fname = $chart->FILE_PREFIX . "-color-" . $idx .
#	    ".png";
#	print "<img src='$tmp_url/". $fname . "' border=0>"; 
#	print "</a>";
#	print "&nbsp;&nbsp;";

	print escapeHTML( $category1 );
	print "</td>\n"; 
	print "<td class='img' align='right'>\n";
	if ( $chartdata[$idx] ) {
	    print alink( $url, $chartdata[$idx] );
	}
	else {
	    print "0";
	}

	print "</td>\n"; 

	# percentage
	if ( $total > 0 ) {
	    if ( $chartdata[$idx] ) {
		my $perc = int(($chartdata[$idx] * 100 / $total) + .5);
		print "<td class='img' align='right'>" . $perc . "%</td>\n";
	    }
	    else {
		print "<td class='img' align='right'>0%</td>\n";
	    }
	}
	else {
	    print "<td class='img' align='right'>-</td>\n";
	}

	print "</tr>\n"; 
	$idx++;
    }
 
    print "</table>\n"; 
    print "</td>\n"; 
    print "<td valign=top align=left>\n";

    print "</td><td valign=top align=right>\n";

    # display the bar chart
    push @chartseries, "num scaffolds"; 
    $chart->SERIES_NAME(\@chartseries);
    $chart->CATEGORY_NAME(\@chartcategories);
    my $datastr = join(",", @chartdata);
    my @datas = ($datastr);
    $chart->DATA(\@datas);
    print "<td align=right valign=center>\n"; 
    if ($env->{ chart_exe } ne "") { 
	my $st = generateChart($chart);
	if ( $st == 0 ) { 
	    print "<script src='$base_url/overlib.js'></script>\n";
            my $FH = newReadFileHandle($chart->FILEPATH_PREFIX.".html", "printDistribution",1); 
	    while (my $s = $FH->getline()) {
		print $s;
	    } 
	    close ($FH);
	    print "<img src='$tmp_url/".$chart->FILE_PREFIX.".png' BORDER=0 ";
	    print " width=".$chart->WIDTH." HEIGHT=".$chart->HEIGHT;
	    print " USEMAP='#".$chart->FILE_PREFIX."'>\n";
	} 
    } 
    print "</td></tr>\n";
    print "</table>\n"; 
 
   ###########################  

    $dbh->disconnect( ); 

    print "<p>\n";
 
    if ( ! $include_null ) {
	my $url = "hmp.cgi?section=HmpSummary&page=showCategory&cat_field=$graph_fld&cat_code=Unclassified";
	print "<p>Unclassified: <a href='$url'>$f_count0</a></p>\n";
    }

} 
 



1;
 
