######################################################################### 
# RelTable.pm - relational table definition 
######################################################################### 
 
package RelTable;

use RelAttr;

my %fields = (
              name => undef,
	      display_name => undef,
              desc => undef, 
	      tooltip => undef,
              id => undef,
	      tab => undef,
	      ref_list => undef,
	      multipart_form => undef,
	      new_rows => undef,
              migs_id => undef,
              migs_name => undef,
              attrs => ( ),
              ); 
 
sub new { 
    my $class = shift;
    my $self = {
        %fields,
    };

    # set default
    $self->{multipart_form} = 0;
    $self->{new_rows} = 1;

    bless $self, $class;
    return $self;
}


############################################################################
# findAttr
############################################################################
sub findAttr {
    my ($self, $aname) = @_;
 
    my $attr; 
 
    for $attr ( @{$self->{attrs}} ) {
        if ( $attr->{name} eq $aname ) {
            return $attr; 
        } 
    } 
 
    return $attr;
} 



1;

