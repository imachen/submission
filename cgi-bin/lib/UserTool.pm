package UserTool;
my $section = "UserTool";

use strict;
use warnings;
use CGI qw(:standard);
use Digest::MD5 qw( md5_base64);
use CGI::Carp 'fatalsToBrowser';

use lib 'lib';
use WebEnv;
use WebFunctions;
use RelSchema;
use InnerTable;
use REST::Client;
use JSON;

my $env                   = getEnv();
my $main_cgi              = $env->{main_cgi};
my $section_cgi           = "$main_cgi?section=$section";
my $base_url              = $env->{base_url};
my $cgi_tmp_dir           = $env->{cgi_tmp_dir};
my $use_yahoo_table       = $env->{use_yahoo_table};
my $max_upload_line_count = 100000;
my $user_guide_url        = $env->{user_guide_url};
my $grant_access_url      = $env->{base_url} . "/doc/GrantAccess.pdf";

$ENV{TMP}     = "/local/scratch";
$ENV{TEMP}    = "/local/scratch";
$ENV{TEMPDIR} = "/local/scratch";
$ENV{TMPDIR}  = "/local/scratch";

$CGITempFile::TMPDIRECTORY = $TempFile::TMPDIRECTORY = '/local/scratch';

#########################################################################
# dispatch
#########################################################################
sub dispatch {
    my ($page) = @_;

    my $contact_oid = getContactOid();
    my $isAdmin     = 'No';

    if ($contact_oid) {
        $isAdmin = getIsAdmin($contact_oid);
    }

    if ( $page eq 'showRequest' ) {
        ShowRequest('all');
    } elsif ( $page eq 'showNewRequest' ) {
        ShowRequest('new');
    } elsif ( $page eq 'updateRequest' ) {
        UpdateRequest();
    } elsif ( $page eq 'redirectToEDU' ) {
        my $msg = RedirectToEDU();
        if ($msg) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowRequest('new');
        }
    } elsif ( $page eq 'requestDenied' ) {
        my $msg = RequestDenied();
        if ($msg) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowRequest('new');
        }
    } elsif ( $page eq 'viewImgContacts' ) {
        if ($use_yahoo_table) {
            ViewIMGContacts_yui();
        } else {
            ViewIMGContacts();
        }
    } elsif ( $page eq 'viewUserDist' ) {
        ViewUserDistribution();
    } elsif ( $page eq 'viewUserOrg' ) {
        my $country = param1('country');
        ViewUserOrganization($country);
    } elsif ( $page eq 'viewOrgUserList' ) {
        my $country = param1('country');
        my $org     = param1('org');
        ViewOrgUserList( $country, $org );
    } elsif ( $page eq 'showContactInfo' ) {
        ShowContactInfo();
    } elsif ( $page eq 'updateContactInfo' ) {
        UpdateContactInfo();
    } elsif ( $page eq 'deleteContact' ) {
        DeleteContact();
    } elsif ( $page eq 'dbUpdateImgContactInfo' ) {
        my $msg = dbUpdateImgContactInfo();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            if ($use_yahoo_table) {
                ViewIMGContacts_yui();
            } else {
                ViewIMGContacts();
            }
        }
    } elsif ( $page eq 'dbDeleteImgContact' ) {
        my $msg = dbDeleteImgContact();

        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            if ($use_yahoo_table) {
                ViewIMGContacts_yui();
            } else {
                ViewIMGContacts();
            }
        }
    } elsif ( $page eq 'resetContactPwd' ) {
        ResetContactPassword();
    } elsif ( $page eq 'dbUpdateRequest' ) {
        my $req_type = param1('request_type');
        my $msg      = dbUpdateRequest();
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            if ($req_type) {
                ShowRequest($req_type);
            } else {
                ShowPage($isAdmin);
            }
        }
    } elsif ( $page eq 'dbAddContact' ) {
        my $msg      = dbAddContact();
        my $req_type = param1('request_type');
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            if ($req_type) {
                ShowRequest($req_type);
            } else {
                ShowPage($isAdmin);
            }
        }
    } elsif ( $page eq 'dbAddReqContact' ) {
        my $msg      = dbAddReqContact();
        my $req_type = param1('request_type');
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            if ($req_type) {
                ShowRequest($req_type);
            } else {
                ShowPage($isAdmin);
            }
        }
    } elsif ( $page eq 'deleteRequest' ) {
        ConfirmDeleteRequest();
    } elsif ( $page eq 'dbDeleteRequest' ) {
        my $msg = dbDeleteRequest();
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage($isAdmin);
        }
    } elsif ( $page eq 'resetPassword' ) {
        ResetPassword();
    } elsif ( $page eq 'dbBulkAccountApproval' ) {
        dbBulkAccountApproval();
    } elsif ( $page eq 'ControlVocab' ) {
        EditCV($isAdmin);
    } elsif ( $page eq 'newCv' ) {
        my $msg = AddUpdateCv( 0, $isAdmin );
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {

            #            ShowPage($isAdmin);
            EditCV($isAdmin);
        }
    } elsif ( $page eq 'updateCv' ) {
        my $msg = AddUpdateCv( 1, $isAdmin );
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {

            #            ShowPage($isAdmin);
            EditCV($isAdmin);
        }
    } elsif ( $page eq 'deleteCv' ) {
        ConfirmDeleteCv($isAdmin);
    } elsif ( $page eq 'dbDelCv' ) {
        my $msg = dbDeleteCV($isAdmin);
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {

            #            ShowPage($isAdmin);
            EditCV($isAdmin);
        }
    } elsif ( $page eq 'editAnnouncement' ) {
        EditAnnouncement('Home Page');
    } elsif ( $page eq 'editAgreement' ) {
        EditAnnouncement('Agreement');
    } elsif ( $page eq 'dbUpdateAnnounce' ) {
        my $msg = dbUpdateAnnounce();
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage($isAdmin);
        }
    } elsif ( $page eq 'selectToExport' ) {
        ShowExportSelection();
    } elsif ( $page eq 'listExportProj' ) {
        ListExportProjects();
    } elsif ( $page eq 'selectImportFile' ) {
        PrintImportFileForm();
    } elsif ( $page eq 'validateImportFile' ) {
        ValidateImportFileForm();
    } elsif ( $page eq 'dbFileUpload' ) {
        dbFileUpload();
    } elsif ( $page eq 'genUserReport' ) {
        my $report_type = param1('report_type');

        if ( $report_type eq 'project' ) {
            GenerateContactProjectReport();
        } else {
            GenerateContactReport();
        }
    } elsif ( $page eq 'genProjReport' ) {
        my $msg = genProjectReport();
        if ( !blankStr($msg) ) {
            print header( -type => "text/html" );
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage($isAdmin);
        }
    } elsif ( $page eq 'showFunctions' ) {
        ShowFunctions();
    } elsif ( $page eq 'syncDatabase' ) {
        SyncDatabase();
    } elsif (    $page eq 'dbReleaseGenome'
              || $page eq 'dbUnreleaseGenome'
              || $page eq 'dbObsoleteGenome'
              || $page eq 'dbRecoverGenome'
              || $page eq 'dbSetHighQuality'
              || $page eq 'dbGrantAccess' )
    {
        my $msg = dbUpdateGenomes($page);
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage($isAdmin);
        }
    } elsif ( $page eq 'checkDisconnectSubmission' ) {
        CheckDisconnectSubmission();
    } elsif ( $page eq 'dbDisconnectSubmission' ) {
        my $msg = dbDisconnectSubmission();
        if ( !blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
        } else {
            ShowPage($isAdmin);
        }
    } elsif ( $page eq 'faqMain' ) {
        ShowFaqMainPage();
    } elsif ( $page eq 'testFunctions' ) {
        TestFunctions();
    } else {
        ShowPage($isAdmin);
    }
}

############################################################################
# ShowPage
############################################################################
sub ShowPage {
    my ($isAdmin) = @_;

    if ( $isAdmin eq 'Yes' ) {
        ShowAdminToolPage();
    } else {
        my $contact_oid = getContactOid();
        my $submitAdmin = isSubmissionAdmin($contact_oid);

        if ( $submitAdmin eq 'Yes' ) {
            ShowAdminToolPage();
        } else {
            ShowUserToolPage();
        }
    }
}

############################################################################
# ShowUserToolPage
############################################################################
sub ShowUserToolPage {

    print start_form( -name => 'showUserTool', -method => 'post', action => "$section_cgi" );

    ShowCVSection('No');

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

############################################################################
# ShowAdminToolPage
############################################################################
sub ShowAdminToolPage {

    print start_form( -name => 'showAdminTool', -method => 'post', action => "$section_cgi" );

    print "<p>\n";
    Submission::ShowQuickSearchSection();

    print "<h2>User Account Management</h2>\n";

    my $request_cnt = requestCount("");
    my $new_cnt     = requestCount("status = 'new request'");
    print "<p>Total Request Count: $request_cnt ";
    print nbsp(1);
    print " (New Request Count: $new_cnt)</p>\n";

    print
      '<input type="submit" name="_section_UserTool:showRequest" value="View / Update User Requests" class="medbutton" />';
    print nbsp(1);
    print
      '<input type="submit" name="_section_UserTool:showNewRequest" value="View / Update New Requests" class="medbutton" />';
    print nbsp(1);
    print '<input type="submit" name="_section_UserTool:viewImgContacts" value="View IMG Contacts" class="smbutton" />';

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    #    if ( $contact_oid == 312 ) {
    if ( $isAdmin eq 'Yes' ) {
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:viewUserDist" value="User Distribution" class="smbutton" />';
    }

    #    my $submitAdmin = isSubmissionAdmin($contact_oid);
    my $url = url();
    if ( $url =~ /gold\.cgi/ ) {

        # CV
        print hr;
        ShowCVSection('Yes');

        # skip announcement
        # export to gold
        print hr;
        print "<h2>Export to GOLD</h2>\n";

        print "<p>Daily export to GOLD database: " . getGoldDbUrl() . "</p>\n";
        print "<p>Export Log: " . getGoldLogUrl() . "</p>\n";

        # file export and import
        ShowFileExportImport();
    } elsif ( $isAdmin eq 'Yes' ) {

        # Edit annoucement
        my $dbh = WebFunctions::Connect_IMG;

        my $sql = qq{
        select c.username, a.mod_date,
	to_char(a.mod_date, 'DD-MON-YYYY HH24:MI:SS')
            from announcement a, contact c
	    where a.modified_by = c.contact_oid
	    and a.type = ?
            order by 2 desc
        };
        webLog("$sql\n");
        my $cur = $dbh->prepare($sql);
        $cur->execute('Home Page');
        my ( $cname, $d1, $d2 ) = $cur->fetchrow();
        $cur->finish();
        $cur->execute('Agreement');
        my ( $c2name, $d3, $d4 ) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();

        print hr;
        print "<h2>Edit Announcement or Agreement</h2>\n";
        print "<p>Edit the message shown on the Home Page. " . "Last modified by " . escapeHTML($cname) . " ($d2).</p>\n";
        print
          '<input type="submit" name="_section_UserTool:editAnnouncement" value="Edit Announcement" class="medbutton" />';

        print "<p>Edit the message shown on the Agreement Page. "
          . "Last modified by "
          . escapeHTML($c2name)
          . " ($d4).</p>\n";
        print '<input type="submit" name="_section_UserTool:editAgreement" value="Edit Agreement" class="medbutton" />';
    }

    # generate user report
    print hr;
    print "<h2>Generate User Report</h2>\n";
    print "Report Type: ";
    print "&nbsp; \n";
    print "<select name='report_type' class='img' size='1'>\n";
    print "    <option value='contact'>IMG Contacts</option>\n";
    print "    <option value='project'>IMG Contacts and Projects</option>\n";
    print "</select>\n";
    print "&nbsp; &nbsp; &nbsp;\n";
    print
'<input type="submit" name="_section_UserTool:genUserReport_noHeader" value="Generate User Report" class="medbutton" />';

    # generate project report
    print hr;
    print "<h2>Generate Project Report</h2>\n";
    print "<p>Data Type: ";
    print "&nbsp; \n";
    print "<input type='radio' name='proj_db_option' " . "value='IMG ER' checked /> Isolate and Single Cell Genomes \n";
    print nbsp(1);
    print "<input type='radio' name='proj_db_option' " . "value='IMG/M ER' /> Metagenomes\n";

    print nbsp(1);
    print "<input type='radio' name='proj_db_option' " . "value='IMG_MER_RNASEQ' /> Metatranscriptome <br/>\n";

    print "<p>\n";
    print "<p>Sequenced at: ";
    print "&nbsp; \n";
    print "<input type='radio' name='proj_seq_center' " . "value='all' checked /> All \n";
    print nbsp(1);
    print "<input type='radio' name='proj_seq_center' " . "value='jgi_only' /> JGI only \n";
    print nbsp(1);
    print "<input type='radio' name='proj_seq_center' " . "value='non_jgi' /> Non-JGI only <br/>\n";

    print "<p>\n";
    print "<select name='proj_attr_option' class='img' size='1'>\n";
    print "    <option value='submission_date'>Submission Date</option>\n";
    print "    <option value='loading_date'>Database Loading Date</option>\n";
    print "    <option value='release_date'>Release Date</option>\n";
    print "</select>\n";

    print nbsp(1);
    print "<select name='proj_op' class='img' size='1'>\n";
    print "   <option value='0'> </option>\n";
    for my $k ( '>', '>=', '<', '<=', 'between' ) {
        my $k2 = escapeHTML($k);
        print "   <option value='$k2'>$k2</option>\n";
    }
    print "</select>\n";
    print nbsp(1);
    print "<input type='text' name='proj_date_1' size='20' maxLength='20'/>\n";
    print " and ";
    print "<input type='text' name='proj_date_2' size='20' maxLength='20'/>\n";

    print "<br/>\n";
    print "<h5>(Use 'DD-MON-YY' for Date format - e.g., 01-MAR-11)</h5>\n";

    print "<p>\n";
    print
'<input type="submit" name="_section_UserTool:genProjReport_noHeader" value="Generate Project Report" class="medbutton" />';

    if ( $isAdmin ne 'Yes' ) {

        # Home
        print "<p>\n";
        printHomeLink();

        print end_form();
        return;
    }

    # disconnect submissions
    if (    $contact_oid != 312
         && $contact_oid != 11
         && $contact_oid != 17
         && $contact_oid != 19
         && $contact_oid != 3038
         && $contact_oid != 101232
         && $contact_oid != 102319
         && $contact_oid != 100284
         && $contact_oid != 101565
         && $contact_oid != 102151 )
    {
        # Home
        print "<p>\n";
        printHomeLink();

        print end_form();
        return;
    }

    print hr;
    print "<h2>Disconnect/Reconnect Submission from Project or Sample</h2>\n";
    print "<p>Please enter a project/sample ER ID or GOLD ID. ";
    print "Leave New ID value blank for disconnection.\n";
    print "<select name='disconnect_id_type' class='img' size='1'>\n";
    print "    <option value='project'>Project ID/GOLD ID</option>\n";
    print "    <option value='sample'>Sample ID/GOLD ID</option>\n";
    print "</select>\n";
    print "&nbsp; &nbsp;\n";
    print "<input type='text' name='disconnect_id_value' value='' " . "' size='20" . " maxLength='40'/>";
    print "&nbsp; &nbsp;\n";
    print "with New ID: ";
    print "<input type='text' name='reconnect_id_value' value='' " . "' size='20" . " maxLength='40'/>";
    print "<p>\n";

    print
'<input type="submit" name="_section_UserTool:checkDisconnectSubmission" value="Disconnect / Reconnect" class="meddefbutton" />';

    if (    $contact_oid != 312
         && $contact_oid != 11
         && $contact_oid != 17
         && $contact_oid != 19
         && $contact_oid != 3038
         && $contact_oid != 101232
         && $contact_oid != 102319
         && $contact_oid != 100284
         && $contact_oid != 101565
         && $contact_oid != 102151 )
    {
        # Home
        print "<p>\n";
        printHomeLink();

        print end_form();
        return;
    }

    # update genomes
    print hr;
    print "<h2>Release/Unrelease/Delete/Recover/Review Genomes</h2>\n";
    print "<p>List IMG Taxon OIDs below; use space or comma to separate IDs.\n";
    print "<textarea id='genome_ids' name='genome_ids' " . "rows='16' cols='80' maxLength='4000'></textarea>\n";
    print "<p>Comments: \n";
    print "<input type='text' name='adm_comments' value='' " . "' size='80" . " maxLength='255'/>";

    print "<p>\n";

    print '<input type="submit" name="_section_UserTool:dbReleaseGenome" value="Release" class="smdefbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_UserTool:dbUnreleaseGenome" value="Make Private" class="smdefbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_UserTool:dbObsoleteGenome" value="Obsolete" class="smdefbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_UserTool:dbRecoverGenome" value="Recover" class="smdefbutton" />';

#    print "<p>High Quality Flag:\n";
#    print "<select name='h_q_flag' class='img' size='1'>\n";
#    print "   <option value='0'> </option>\n";
#    for my $k2 ( 'Yes', 'No', 'Unknown' ) {
#        print "   <option value='$k2'>$k2</option>\n";
#    }
#    print "</select>\n";
#    print "&nbsp; \n";
#    print "&nbsp; \n";
#    print '<input type="submit" name="_section_UserTool:dbSetHighQuality" value="Update High Quality Flag" class="meddefbutton" />';

    print "<p><b>Grant Access To</b> (JGI SSO separated by commas): \n";
    print "<input type='text' name='grant_to' value='' " . "' size='80" . " maxLength='1024'/>";

    print "<p>\n";

    print '<input type="submit" name="_section_UserTool:dbGrantAccess" value="Grant Access" class="smdefbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
    return;

    print hr;
    print "<h2>Sync Database Content</h2>\n";
    print "Operation Type: ";
    print "&nbsp; \n";
    print "<select name='sync_op_type' class='img' size='1'>\n";
    print "    <option value='proposal'>Refresh Proposal Name</option>\n";
    print "    <option value='ecosystem'>Refresh Metagenome Name and EcoSystem</option>\n";
    print "    <option value='seq_center'>Refresh Sequencing Center</option>\n";
    print "    <option value='gold_id'>Refresh GOLD ID based on NCBI PID</option>\n";
    print "    <option value='submit_id'>Refresh GOLD ID based on Submission ID</option>\n";
    print "    <option value='img_oid'>Refresh IMG OID and Stats in IMG-GOLD</option>\n";
    print "</select>\n";

    my $dbs_ref = $env->{db_connect};
    print "<p>Database: ";
    print nbsp(1);
    print "<input type='radio' name='sync_db_option' " . "value='all' checked /> All Databases (excluding pg)\n";

    #    print nbsp(1);
    #    print "<input type='radio' name='sync_db_option' " .
    #	"value='imgw' /> IMG/W " .
    #	"(" . $dbs_ref->{'imgw'}->{user} . ")\n";
    print nbsp(1);
    print "<input type='radio' name='sync_db_option' "
      . "value='imger' /> IMG 4.0 " . "("
      . $dbs_ref->{'imger'}->{user}
      . ") <br/>\n";

    #    print nbsp(9);
    #    print "<input type='radio' name='sync_db_option' " .
    #	"value='imgmer' /> IMG/M ER " .
    #	"(" . $dbs_ref->{'imgmer'}->{user} . ")\n";
    #    print nbsp(1);
    #    print "<input type='radio' name='sync_db_option' " .
    #	"value='imgi' /> IMG I " .
    #	"(" . $dbs_ref->{'imgi'}->{user} . ")\n";
    print nbsp(1);
    print "<input type='radio' name='sync_db_option' "
      . "value='imgpg' /> IMG Developer DB " . "("
      . $dbs_ref->{'imgpg'}->{user} . ") "
      . "<br/>\n";
    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:syncDatabase" value="Update Database(s)" class="medbutton" />';

    #    my $contact_oid = getContactOid( );
    #    if ( $contact_oid == 312 ) {
    #	print hr;
    #	print "<h2>Test Email</h2>\n";
    #	print "<p>Submission ID: ";
    #	print "&nbsp; \n";
    #	print "<input type='text' name='test_submit_id' " .
    #	    "size='20' maxLength='20' />\n";
    #	print "</p>\n";
    #	print "<p>\n";
    #	print '<input type="submit" name="_section_UserTool:testFunctions" value="Test" class="medbutton" />';
    #    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

#########################################################################
# ShowCVSection: edit CV
#########################################################################
sub ShowCVSection {
    my ($isAdmin) = @_;

    print "<h2>Controlled Vocabulary Metadata</h2>\n";

    print "Select a controlled vocabulary class to edit: ";
    print "&nbsp; &nbsp; &nbsp;\n";
    print "<select name='cv_option' class='img' size='1'>\n";
    print "    <option value='bei_statuscv'>BEI Status CV</option>\n";

    if ( $isAdmin eq 'Yes' ) {
        print "    <option value='bin_methodcv'>Binning Method CV</option>\n";
    }

    print "    <option value='body_productcv'>Body Product CV</option>\n";
    print "    <option value='body_sitecv'>Body Sample Site CV</option>\n";
    print "    <option value='body_subsitecv'>Body Sample SubSite CV</option>\n";
    print "    <option value='cell_arrcv'>Cell Arrangement CV</option>\n";
    print "    <option value='cell_shapecv'>Cell Shape CV</option>\n";
    print "    <option value='collaboratorcv'>Collaborator CV</option>\n";
    print "    <option value='colorcv'>Color CV</option>\n";
    print "    <option value='countrycv'>Country CV</option>\n";
    print "    <option value='diseasecv'>Disease CV</option>\n";
    print "    <option value='energy_sourcecv'>Energy Source CV</option>\n";
    print "    <option value='funding_programcv'>Funding Program CV</option>\n";
    print "    <option value='habitatcv'>Habitat CV</option>\n";
    print "    <option value='cvecosystem'>Ecosystem CV</option>\n";
    print "    <option value='cvecosystem_category'>Ecosystem Category CV</option>\n";
    print "    <option value='cvecosystem_type'>Ecosystem Type CV</option>\n";
    print "    <option value='cvecosystem_subtype'>Ecosystem Subtype CV</option>\n";
    print "    <option value='cvspecific_ecosystem'>Specific Ecosystem CV</option>\n";
    print "    <option value='habitat_categorycv'>Habitat Category CV</option>\n";
    print "    <option value='hmp_project_statuscv'>HMP Project Status CV</option>\n";

    if ( $isAdmin eq 'Yes' ) {
        print "    <option value='jgi_funding_progcv'>JGI Funding Program CV</option>\n";
        print "    <option value='jgi_final_deliverablecv'>JGI ITS Final Deliverable CV</option>\n";
    }

    print "    <option value='metabolismcv'>Metabolism CV</option>\n";
    print "    <option value='motilitycv'>Motility CV</option>\n";

    if ( $isAdmin eq 'Yes' ) {
        print "    <option value='ncbi_submit_statuscv'>NCBI Submission Status CV</option>\n";
    }

    print "    <option value='oxygencv'>Oxygen Requirement CV</option>\n";
    print "    <option value='phenotypecv'>Phenotype CV</option>\n";

    if ( $isAdmin eq 'Yes' ) {
        print "    <option value='phylogenycv'>Phylogeny CV</option>\n";
    }

    print "    <option value='project_goalcv'>Project Goal CV</option>\n";
    print "    <option value='project_statuscv'>Project Status CV</option>\n";
    print "    <option value='project_typecv'>Project Type CV</option>\n";
    print "    <option value='publication_journalcv'>Publication Journal CV</option>\n";
    print "    <option value='relevancecv'>Relevance CV</option>\n";
    print "    <option value='salinitycv'>Salinity CV</option>\n";
    if ( $isAdmin eq 'Yes' ) {
        print "    <option value='sci_progcv'>Scientific Program CV</option>\n";
    }
    print "    <option value='seq_centercv'>Sequencing Center CV</option>\n";
    print "    <option value='seq_methodcv'>Sequencing Method CV</option>\n";
    print "    <option value='seq_qualitycv'>Sequencing Quality CV</option>\n";
    print "    <option value='seq_statuscv'>Sequencing Status CV</option>\n";
    print "    <option value='sporulationcv'>Sporulation CV</option>\n";
    print "    <option value='db_namecv'>Source Name CV</option>\n";
    print "    <option value='temp_rangecv'>Temperature Range CV</option>\n";
    print "    <option value='unitcv'>Unit CV</option>\n";
    print "</select>\n";
    print "<p/>\n";

    print
      '<input type="submit" name="_section_UserTool:ControlVocab" value="Edit Controlled Vocabularies" class="medbutton" />';
}

##########################################################################
# EditCV - edit CV
##########################################################################
sub EditCV {
    my ($isAdmin) = @_;

    print start_form( -name => 'editCV', -method => 'post', action => "$section_cgi" );

    print "<h2>Edit Controlled Vocabulary Metadata</h2>\n";

    my $cv_option = param1('cv_option');
    if ( blankStr($cv_option) ) {
        printError("No Controlled Vocabulary Class has been selected.");
        print end_form();
        return;
    }
    print hidden( 'cv_option', $cv_option );

    my $def_class = def_Class($cv_option);
    if ( !$def_class ) {
        printError("Incorrect class: $cv_option");
        print end_form();
        return;
    }

    my $class_disp_name = $def_class->{display_name};
    if ( blankStr($class_disp_name) ) {
        $class_disp_name = $cv_option;
    }
    print "<h3>Class: $class_disp_name </h3>\n";
    print hidden( 'class_display_name', $class_disp_name );

    my @attrs = @{ $def_class->{attrs} };
    my $sql   = "";

    # print table header
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Select</th>\n";
    for my $attr (@attrs) {
        if ( $attr->{can_edit} ) {
            print "<th class='img'>" . escapeHTML( $attr->{display_name} ) . "</th>\n";
        }

        if ( blankStr($sql) ) {
            $sql = "select " . $attr->{name};
        } else {
            $sql .= ", " . $attr->{name};
        }
    }
    $sql .= " from " . $def_class->{name} . " order by 1";

    my $dbh = Connect_IMG();
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( my $i2 = 0 ; $i2 <= 100000 ; $i2++ ) {
        my @flds = $cur->fetchrow_array();
        if ( scalar(@flds) == 0 ) {
            last;
        }
        my $oid = $flds[0];
        if ( !$oid ) {
            last;
        }

        # select
        print "<tr class='img'>\n";
        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='oid' value='" . escapeHTML($oid) . "' />\n";
        print "</td>\n";

        my $j = 0;
        for my $k (@attrs) {
            if ( $j >= scalar(@attrs) ) {
                last;
            }

            my $val = $flds[$j];
            if ( $k->{can_edit} ) {
                PrintAttribute($val);
            }

            $j++;
        }

        print "</tr>\n";
    }
    $cur->finish();
    $dbh->disconnect();

    print "</table>\n";

    for my $attr (@attrs) {
        if ( $attr->{can_edit} ) {
            print "<p>\n";
            print "<p>New " . $attr->{display_name} . ":\n";
            my $new_name = "new:" . $attr->{name};
            my $size     = 60;
            my $len      = $attr->{length};
            if ( $size > $len ) {
                $size = $len;
            }
            print "<input type='text' name='$new_name' " . "size='$size' maxLength='$len' />\n";
            print "</p>\n";
        }
    }

    showEditCvButtons($isAdmin);

    printHomeLink();

    print end_form();
}

###################################################################
#  showEditCvButtons
###################################################################
sub showEditCvButtons {
    my ($isAdmin) = @_;

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:newCv" value="New" class="smbutton" />';

    # only admin can update or delete
    if ( $isAdmin eq 'Yes' ) {
        print "&nbsp; \n";
        print '<input type="submit" name="_section_UserTool:updateCv" value="Update" class="smbutton" />';
        print "&nbsp; \n";
        print '<input type="submit" name="_section_UserTool:deleteCv" value="Delete" class="smbutton" />';
    }

    print "&nbsp; \n";
    print '<input type="submit" name="_section_UserTool:showPage" value="Cancel" class="smbutton" />';
    print "<p>\n";
}

###########################################################################
# AddUpdateCv
###########################################################################
sub AddUpdateCv {
    my ( $upd, $isAdmin ) = @_;

    my $msg = "";

    print start_form( -name => 'addUpdateCv', -method => 'post', action => "$section_cgi" );

    # CV option is CV class name
    my $cv_option = param1('cv_option');
    if ( blankStr($cv_option) ) {
        $msg = "No Controlled Vocabulary Class has been selected.";
        return $msg;
    }
    print hidden( 'cv_option', $cv_option );

    my $def_class = def_Class($cv_option);
    my @attrs     = @{ $def_class->{attrs} };

    my $upd_cv_only = 0;

    # check value
    my $new_cv = "";
    for my $attr (@attrs) {
        my $new_name = "new:" . $attr->{name};
        if ( $attr->{copy_from} ) {
            $new_name = "new:" . $attr->{copy_from};
        }

        # check required
        $new_cv = param1($new_name);
        if ( $attr->{is_required} ) {
            if ( blankStr($new_cv) ) {
                $msg = "Please enter a value in " . $attr->{display_name} . ".";
                return $msg;
            }
        }

        # check id (cannot be duplicate)
        if ( $attr->{name} eq $def_class->{id} ) {
            if (    $upd
                 && lc($cv_option) eq 'habitatcv'
                 && $new_cv eq param1('oid') )
            {
                # fine. user can update envo_id
                $upd_cv_only = 1;
            } else {
                my $db_id = $new_cv;
                $db_id =~ s/'/''/g;    # replace ' with ''
                my $sql2 =
                    "select "
                  . $def_class->{id}
                  . " from "
                  . $def_class->{name}
                  . " where "
                  . $def_class->{id} . " = '"
                  . $db_id . "'";
                my $ret_id = db_getValue($sql2);
                if ( !blankStr($ret_id) ) {
                    $msg = "Value '$new_cv' already exists.";
                    return $msg;
                }
            }
        }
    }

    my $sql     = "";
    my @sqlList = ();
    my $oid     = param1('oid');

    if ($upd) {
        if ( blankStr($oid) ) {
            $msg = "No term has been selected.";
            return $msg;
        }
    }

    if ($upd_cv_only) {

        # special code for update envo_id only in habitatcv
        my $new1 = param1('new:cv_term');
        my $new2 = param1('new:envo_id');
        if ( $new1 eq $oid ) {
            $sql = "update habitatcv set envo_id = ";
            if ( blankStr($new2) ) {
                $sql .= "null";
            } else {
                $new2 =~ s/'/''/g;    # replace ' with ''
                $sql .= "'" . $new2 . "'";
            }
            $sql .= " where cv_term = '";
            $new1 =~ s/'/''/g;        # replace ' with '';
            $sql .= $new1 . "'";
            push @sqlList, ($sql);
            db_sqlTrans( \@sqlList );
            return;
        }
    }

    # add new value
    my $ins    = "";
    my $vals   = "";
    my $new_id = "";
    for my $attr (@attrs) {
        if ( blankStr($ins) ) {
            $ins  = "insert into $cv_option (";
            $vals = "values (";
        } else {
            $ins  .= ", ";
            $vals .= ", ";
        }
        $ins .= $attr->{name};
        my $new_name = "new:" . $attr->{name};
        if ( $attr->{copy_from} ) {
            $new_name = "new:" . $attr->{copy_from};
        }

        $new_cv = param1($new_name);
        if ( $attr->{name} eq $def_class->{id} ) {
            $new_id = $new_cv;
        }

        if (    $attr->{data_type} eq 'int'
             || $attr->{data_type} eq 'number' )
        {
            $vals .= $new_cv;
        } else {
            $new_cv =~ s/'/''/g;    # replace ' with ''
            $vals .= "'" . $new_cv . "'";
        }

        $sql = $ins . ") " . $vals . ")";
    }

    push @sqlList, ($sql);

    if ($upd) {
        my $db_oid = $oid;
        $db_oid =~ s/'/''/g;        # replace ' with ''

        my $db_nid = $new_id;
        $db_nid =~ s/'/''/g;        # replace ' with ''

        # replace all FK references
        my @refs = split( /\,/, $def_class->{ref_list} );
        for my $r2 (@refs) {
            my ( $ref_table, $ref_key ) = split( /\|/, $r2 );

            $sql = "update $ref_table set $ref_key = '" . $db_nid . "' " . "where $ref_key = '" . $db_oid . "'";
            push @sqlList, ($sql);
        }

        # delete old value
        $sql = "delete from " . $def_class->{name} . " where " . $def_class->{id} . " = '" . $db_oid . "'";
        push @sqlList, ($sql);
    }

    db_sqlTrans( \@sqlList );

    return $msg;
}

###########################################################################
# ConfirmDeleteCv
###########################################################################
sub ConfirmDeleteCv {
    my ($isAdmin) = @_;

    print start_form( -name => 'confirmDeleteCv', -method => 'post', action => "$section_cgi" );

    if ( $isAdmin ne 'Yes' ) {
        printError("You do not have privilege to delete term.");
        print end_form();
        return;
    }

    # CV option is CV class name
    my $cv_option = param1('cv_option');
    if ( blankStr($cv_option) ) {
        printError("No Controlled Vocabulary Class has been selected.");
        print end_form();
        return;
    }
    print hidden( 'cv_option', $cv_option );

    my $def_class = def_Class($cv_option);
    my @attrs     = @{ $def_class->{attrs} };

    my $oid = param1('oid');
    if ( blankStr($oid) ) {
        printError("No term has been selected.");
        print end_form();
        return;
    }
    print hidden( 'oid', $oid );

    # check whether this cv value has been referenced
    my $is_used = 0;
    my @refs    = split( /\,/, $def_class->{ref_list} );
    for my $r2 (@refs) {
        my ( $ref_table, $ref_key ) = split( /\|/, $r2 );
        my $ref_class = def_Class($ref_table);
        if ( !$ref_class ) {
            printError("Cannot find $ref_table");
            print end_form();
            return;
        }
        my @ref_attrs    = @{ $ref_class->{attrs} };
        my $ref_id_disp  = $ref_class->{id};
        my $ref_key_disp = $ref_key;
        for my $a2 (@ref_attrs) {
            if ( $a2->{name} eq $ref_class->{id} ) {
                $ref_id_disp = $a2->{display_name};
            }
            if ( $a2->{name} eq $ref_key ) {
                $ref_key_disp = $a2->{display_name};
            }
        }

        my $db_id = $oid;
        $db_id =~ s/'/''/g;    # replace ' with ''
        my $sql =
            "select "
          . $ref_class->{id} . ", "
          . $ref_key
          . " from "
          . $ref_table
          . " where "
          . $ref_key . " = '"
          . $db_id . "'";
        my $dbh = Connect_IMG();
        webLog("$sql\n");
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        my $i2 = 0;

        for ( $i2 = 0 ; $i2 <= 100000 ; $i2++ ) {
            my ( $id2, $val2 ) = $cur->fetchrow_array();
            last if !$id2;

            if ( $i2 == 0 ) {
                print "<h3>The selected vocabulary has been referenced:</h3>\n";
                $is_used = 1;

                # print table header
                print "<table class='img' border='1'>\n";
                print "<th class='img'>" . escapeHTML($ref_id_disp) . "</th>\n";
                print "<th class='img'>" . escapeHTML($ref_key_disp) . "</th>\n";
            }

            # print a row
            print "<tr class='img'>\n";
            print "<td class='img'>" . escapeHTML($id2) . "</td>\n";
            print "<td class='img'>" . escapeHTML($val2) . "</td>\n";
            print "</tr>\n";
        }

        print hidden( 'ref_count', $i2 );

        if ( $i2 > 0 ) {
            print "</table>\n";
        }

        $cur->finish();
        $dbh->disconnect();
    }

    if ( $is_used == 0 ) {
        print "<h3>The selected vocabulary has not been referenced.</h3>\n";
    } else {
        print "<h3>Please select a replacement option:</h3>\n";
        print "<input type='radio' name='replace_option' " . "value='no' /> Set reference field to null. <br/>\n";
        print "<input type='radio' name='replace_option' " . "value='yes' /> Replace reference field to: <br/>\n";

        # display replacement values
        print "<p>\n";
        print "<select name='replace_cv' class='img' size='1'>\n";
        my $sql2    = "select " . $def_class->{id} . " from " . $def_class->{name};
        my @db_vals = db_getValues($sql2);
        for my $val ( sort @db_vals ) {
            if ( $val eq $oid ) {
                next;
            }

            print "   <option value='" . escapeHTML($val) . "'";
            print ">" . escapeHTML($val) . "</option>\n";
        }
        print "</select></td>\n";
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbDelCv" value="Delete" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_UserTool:ControlVocab" value="Cancel" class="smbutton" />';

    print end_form();
}

##########################################################################
# dbDeleteCV
###########################################################################
sub dbDeleteCV {
    my ($isAdmin) = @_;

    my $msg = "";

    if ( $isAdmin ne 'Yes' ) {
        $msg = "You do not have privilege to delete term.";
        return $msg;
    }

    # CV option is CV class name
    my $cv_option = param1('cv_option');
    if ( blankStr($cv_option) ) {
        $msg = "No Controlled Vocabulary Class has been selected.";
        return $msg;
    }
    print hidden( 'cv_option', $cv_option );

    my $oid = param1('oid');
    if ( blankStr($oid) ) {
        $msg = "No term has been selected.";
        return $msg;
    }
    my $db_id = $oid;
    $db_id =~ s/'/''/g;    # replace ' with ''

    my $def_class = def_Class($cv_option);

    my @sqlList   = ();
    my $sql       = "";
    my $ref_count = param1('ref_count');
    if (    !blankStr($ref_count)
         && $ref_count > 0
         && $def_class->{ref_list} )
    {
        # replacement
        my $replace_option = param1('replace_option');
        if ( blankStr($replace_option) ) {
            $msg = "No replacement option has been selected.";
            return $msg;
        }

        my @refs = split( /\,/, $def_class->{ref_list} );
        for my $r2 (@refs) {
            my ( $ref_table, $ref_key ) = split( /\|/, $r2 );
            if ( $replace_option eq 'no' ) {
                if (    lc($ref_table) eq 'project_info'
                     || lc($ref_table) eq 'env_sample' )
                {
                    $sql = "update $ref_table set $ref_key = null " . "where $ref_key = '" . $db_id . "'";
                } else {
                    $sql = "delete from $ref_table where $ref_key = '" . $db_id . "'";
                }
                push @sqlList, ($sql);
            } else {
                my $replace_cv = param1('replace_cv');
                if ( blankStr($replace_cv) ) {
                    $msg = "No replacement value has been selected.";
                    return $msg;
                }

                my $db_new = $replace_cv;
                $db_new =~ s/'/''/g;    # replace ' with ''
                $sql = "update $ref_table set $ref_key = '" . $db_new . "' where $ref_key = '" . $db_id . "'";
                push @sqlList, ($sql);
            }
        }
    }

    # delete
    $sql = "delete from " . $def_class->{name} . " where " . $def_class->{id} . " = '" . $db_id . "'";
    push @sqlList, ($sql);

    db_sqlTrans( \@sqlList );

    return $msg;
}

#########################################################################
# showPage_old - the starting page
# (old)
#########################################################################
sub ShowPage_old {

    #generate a form

    print start_form( -method => 'post',
                      -action => "$section_cgi" );

    print "<h1>Request IMG Account</h1>\n";

    print "<p>Please fill in the request form. Required fields are marked with (*)</p>\n";

    # get request account definition
    my $def_req = def_Request_Account();

    my @attrs = @{ $def_req->{attrs} };

    print "<table class='img' border='1'>\n";

    for my $k (@attrs) {
        my $attr_name = $k->{name};

        if ( !$k->{can_edit} ) {
            next;
        }

        my $data_type = $k->{data_type};
        my $len       = $k->{length};
        my $disp_name = $k->{display_name};
        if ( !$disp_name ) {
            $disp_name = $attr_name;
        }

        my $size = 80;
        if ( $len && $size > $len ) {
            $size = $len;
        }

        my $attr_val = "";

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        print escapeHTML($disp_name);
        if ( $k->{is_required} ) {
            print " (*)";
        }
        print "</th>\n";

        if ( !$k->{can_edit} ) {

            # skip non-editable field
            print "  <td class='img'   align='left'>" . escapeHTML($attr_val) . "</td>\n";
        } elsif ( $data_type eq 'text' ) {
            print "  <td class='img'   align='left'>"
              . "<textarea name='$attr_name' cols='$size' rows='10'>"
              . "</textarea>"
              . "</td>\n";
        } elsif ( $data_type eq 'cv' && $k->{cv_query} ) {
            my @vals = db_getValues( $k->{cv_query} );
            print "  <td class='img'   align='left'>\n";
            print "<select name='$attr_name' class='img' size='1'>\n";
            if ( !$k->{is_required} ) {
                print "   <option value=''></option>\n";
            }
            for my $val (@vals) {
                print "   <option value='" . escapeHTML($val) . "'>" . escapeHTML($val) . "</option>\n";
            }
            print "</select></td>\n";
        } else {
            print "  <td class='img'   align='left'>"
              . "<input type='text' name='$attr_name' value='"
              . escapeHTML($attr_val)
              . "' size='$size'"
              . " maxLength='$len'/>"
              . "</td>\n";
        }

        print "</tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    print '<input type="submit" name="submitRequest" value="Submit" class="smbutton" />';

    print end_form();
}

#########################################################################
# SubmitRequest
#########################################################################
sub SubmitRequest {

    my $msg = "";

    # get request account definition
    my $req_id = db_findMaxID( 'request_account', 'request_oid' ) + 1;
    my $ins    = "insert into request_account (request_oid, request_date, status";
    my $vals   = "values (" . $req_id . ", sysdate, 'new request'";

    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };
    for my $k (@attrs) {
        if ( !$k->{can_edit} ) {
            next;
        }

        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};
        my $v         = param1($attr_name);
        if ( $k->{is_required} && blankStr($v) ) {
            $msg = "Please enter a value in " . $k->{display_name} . ".";
            return $msg;
        }

        if ( !blankStr($v) ) {
            $ins .= ", " . $attr_name;
            if ( $data_type eq 'int' ) {
                if ( !isInt($v) ) {
                    $msg = $k->{display_name} . " must be an integer.";
                    return $msg;
                }
                $vals .= ", " . $v;
            } elsif ( $data_type eq 'number' ) {
                if ( !isNumber($v) ) {
                    $msg = $k->{display_name} . " must be a number.";
                    return $msg;
                }
                $vals .= ", " . $v;
            } else {
                $v =~ s/'/''/g;    # replace ' with '';
                $vals .= ", '" . $v . "'";
            }
        }
    }    # end for k

    my $sql = $ins . ") " . $vals . ")";

    my @sqlList = ();
    push @sqlList, ($sql);
    db_con_sqlTrans( \@sqlList );

    return $msg;
}

#########################################################################
# requestCount
#########################################################################
sub requestCount {
    my ( $cond, $use_filter ) = @_;

    my $dbh = WebFunctions::Connect_IMG_Contact();
    if ( !$dbh ) {
        return -1;
    }

    if ($use_filter) {
        my $req_filter  = param1('request_filter');
        my $filter_cond = "";
        if ( blankStr($req_filter) ) {
            $req_filter = 'Last 1 Month';
        }
        if ( $req_filter =~ /Last (\d+)/ ) {
            my $d2 = $1;
            $filter_cond = "months_between(sysdate, request_date) < $d2";
        }

        if ( !blankStr($filter_cond) ) {
            if ( blankStr($cond) ) {
                $cond = $filter_cond;
            } else {
                $cond .= " and " . $filter_cond;
            }
        }
    }

    my $sql = "select count(*) from request_account";
    if ( !blankStr($cond) ) {
        $sql .= " where " . $cond;
    }
    webLog("$sql\n");

    my $cur = $dbh->prepare($sql) or die("Error: 1 $DBI::errstr\n");
    $cur->execute() or die("Error: 2 $DBI::errstr\n");
    my ($cnt) = $cur->fetchrow_array() or die("Error: 3 $DBI::errstr\n");
    $cur->finish();
    $dbh->disconnect();

    if ( !$cnt ) {
        return 0;
    }

    return $cnt;
}

##########################################################################
# ShowRequest - show all user request
#
# req_type: all, new
##########################################################################
sub ShowRequest {
    my ($req_type) = @_;

    print start_form( -name => 'showRequest', -method => 'post', action => "$section_cgi" );

    #    my $contact_oid = getContactOid();

    #    saveAppHiddenVar();

    if ( blankStr($req_type) ) {
        $req_type = param1('request_type');
        if ( blankStr($req_type) ) {
            $req_type = 'all';
        }
    }

    print hidden( 'request_type', $req_type );

    my $req_filter = param1('request_filter');

    my $with_filter = 0;
    if ( $req_type eq 'new' ) {
        print "<h1>New User Requests</h1>\n";
    } else {
        print "<h1>All User Requests</h1>\n";

        $with_filter = 1;
        print "<p>Request Filter: \n";
        print nbsp(1);
        print "<select name='request_filter' class='img' size='1'>\n";
        if ( blankStr($req_filter) ) {
            $req_filter = 'Last 1 Month';
        }

        for my $v2 ( 'Last 1 Month', 'Last 2 Months', 'Last 3 Months', 'Last 6 Months', 'Last 12 Months', 'All' ) {
            print "   <option value='$v2'";
            if ( $v2 eq $req_filter ) {
                print " selected ";
            }
            print ">$v2</option>\n";
        }
        print "</select>\n";
        print nbsp(3);
        print '<input type="submit" name="_section_UserTool:showRequest" value="Refresh" class="smbutton" />';
    }

    my $cnt  = 0;
    my $cond = "";
    if ( $req_type eq 'new' ) {
        $cond = "status = 'new request'";
    }
    $cnt = requestCount( $cond, $with_filter );
    print "<p>Count: $cnt</p>\n";

    if ( $cnt == 0 ) {

        # Home
        print "<p>\n";
        printHomeLink();

        print end_form();
        return;
    }

    if ($use_yahoo_table) {
        listRequests_yui( $cond, $with_filter, $req_type );
    } else {
        listRequests( $cond, $with_filter );
    }

    # Update and Delete buttons
    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:updateRequest" value="Update" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_UserTool:deleteRequest" value="Delete" class="smbutton" />';

    if ( $req_type eq 'new' ) {
        print "&nbsp; \n";
        print '<input type="submit" name="_section_UserTool:redirectToEDU" value="Redirect to EDU" class="medbutton" />';
        print "&nbsp; \n";
        print '<input type="submit" name="_section_UserTool:requestDenied" value="Request Denied" class="medbutton" />';
        print "<p>\n";
        print '<input type="submit" name="_section_UserTool:dbAddReqContact" value="Create Account" class="medbutton" />';
        print nbsp(3);
        print "With initial password (default is used if no initial password): ";
        print "<input type='text' name='init_pwd' value=''" . "size='30' maxLength='30'/>";
        print nbsp(3);
        print "JGI User?";
        print nbsp(1);
        print "<select name='jgi_user' class='img' size='1'>\n";
        print "<option value='No' selected>No</option>\n";
        print "<option value='Yes'>Yes</option>\n";
        print "</select>\n";
    } else {
        print "<p>\n";
        print '<input type="submit" name="_section_UserTool:resetPassword" value="Reset Password" class="medbutton" />';
        print nbsp(3);
        print "With new password (*): ";
        print "<input type='text' name='new_pwd' value=''" . "size='30' maxLength='30'/>";
    }

    ## bulk process
    my $allow_bulk_approve = 0;
    if ( $req_type eq 'new' && $allow_bulk_approve ) {
        print "<h2>Bulk Approval of New Requests</h2>\n";
        print "<p>Approve all <u>new</u> requests ";
        print " with ID between ";
        print "<input type='text' name='id_val1' value='' size='6' maxLength='6'/>
";
        print " and ";
        print "<input type='text' name='id_val2' value='' size='6' maxLength='6'/>";
        print "<p>\n";
        print
"<input type='submit' name='_section_UserTool:dbBulkAccountApproval' value='Approve Requests' class='medbutton' />";
    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

#########################################################################
# listRequests_yui - list requests (use Yahoo datatable)
##########################################################################
sub listRequests_yui {
    my ( $cond, $use_filter, $req_type ) = @_;

    my $selected_request = param1('request_oid');

    my $dbh = WebFunctions::Connect_IMG_Contact();

    if ($use_filter) {
        my $req_filter  = param1('request_filter');
        my $filter_cond = "";
        if ( blankStr($req_filter) ) {
            $req_filter = 'Last 1 Month';
        }
        if ( $req_filter =~ /Last (\d+)/ ) {
            my $d2 = $1;
            $filter_cond = "months_between(sysdate, request_date) < $d2";
        }

        if ( !blankStr($filter_cond) ) {
            if ( blankStr($cond) ) {
                $cond = $filter_cond;
            } else {
                $cond .= " and " . $filter_cond;
            }
        }
    }

    my $cond2 = " where r.assign_contact_oid = c.contact_oid (+)";
    if ( !blankStr($cond) ) {
        $cond2 .= " and " . $cond;
    }

    my $sql = qq{ 
        select r.request_oid, r.name, r.organization, 
        r.country, r.comments, r.status, 
        to_char(r.request_date, 'yyyy-mm-dd'), 
	r.assign_contact_oid, c.username 
            from request_account r, contact c 
            $cond2 
            order by request_oid 
        };

    #print "$sql<br>";
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();                          # sort delimiter
    $it->addColSpec("Select");
    $it->addColSpec( "Request ID", "number asc", "right", "", "Request ID" );

    $it->addColSpec( "Name",         "char asc", "left", "", "Name" );
    $it->addColSpec( "Organization", "char asc", "left", "", "Organization" );
    $it->addColSpec( "Country",      "char asc", "left", "", "Country" );
    $it->addColSpec( "Reason",       "char asc", "left", "", "Reason" );
    if ( $req_type ne 'new' ) {
        $it->addColSpec( "Status", "char asc", "left", "", "Status" );
    }
    $it->addColSpec( "Request Date", "char asc", "left", "", "Request Date" );

    if ( $req_type ne 'new' ) {
        $it->addColSpec( "IMG User Name", "char asc", "left", "", "IMG User Name" );
    }

    my $cnt = 0;
    for ( ; ; ) {
        my ( $request_oid, $name, $org, $country, $comments, $status, $request_date, $c_oid, $c_name ) =
          $cur->fetchrow_array();
        if ( !$request_oid ) {
            last;
        }

        $country = convertCountry($country);

        $cnt++;
        my $row = "";

        $row .= $sd . "<input type='radio' name='request_oid' " . "value='$request_oid'";
        if ( $request_oid == $selected_request ) {
            $row .= " checked ";
        }
        $row .= "/>\t";

        $row .= $request_oid . $sd . $request_oid . "\t";

        $row .= $name . $sd . $name . "\t";
        $row .= $org . $sd . $org . "\t";
        $row .= $country . $sd . $country . "\t";
        $row .= $comments . $sd . $comments . "\t";

        if ( $req_type ne 'new' ) {
            $row .= $status . $sd . $status . "\t";
        }
        $row .= $request_date . $sd . $request_date . "\t";

        if ( $req_type ne 'new' ) {
            if ( !$c_name ) {
                $c_name = " ";
            }
            $row .= $c_name . $sd . $c_name . "\t";
        }

        $it->addRow($row);
    }

    print "<p>\n";

    if ( $cnt > 0 ) {
        $it->printOuterTable(1);
    } else {
        print "<h5>No requests.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    return $cnt;
}

#########################################################################
# listRequests - list requests (HTML table display)
##########################################################################
sub listRequests {
    my ($cond) = @_;

    my $selected_request = param1('request_oid');

    my $dbh = WebFunctions::Connect_IMG_Contact();

    my $cond2 = " where r.assign_contact_oid = c.contact_oid (+)";
    if ( !blankStr($cond) ) {
        $cond2 .= " and " . $cond;
    }

    my $sql = qq{ 
        select r.request_oid, r.name, r.organization, r.status, 
        r.request_date, r.assign_contact_oid, c.username 
            from request_account r, contact c 
            $cond2 
            order by request_oid 
        };

    #print "$sql<br>";
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $color1 = '#eeeeee';
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Selection</th>\n";
    print "<th class='img' bgcolor='$color1'>Request ID</th>\n";
    print "<th class='img' bgcolor='$color1'>Name</th>\n";
    print "<th class='img' bgcolor='$color1'>Organization</th>\n";
    print "<th class='img' bgcolor='$color1'>Status</th>\n";
    print "<th class='img' bgcolor='$color1'>Request Date</th>\n";
    print "<th class='img' bgcolor='$color1'>IMG User Name</th>\n";

    my $cnt = 0;
    for ( ; ; ) {
        my ( $request_oid, $name, $org, $status, $request_date, $c_oid, $c_name ) = $cur->fetchrow_array();
        if ( !$request_oid ) {
            last;
        }

        $cnt++;

        if ( $cnt % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='request_oid' value='$request_oid'";
        if ( $request_oid == $selected_request ) {
            print " checked ";
        }
        print "/></td>\n";

        PrintAttribute($request_oid);
        PrintAttribute($name);
        PrintAttribute($org);
        PrintAttribute($status);
        PrintAttribute($request_date);

        # IMG Contact user name
        #	if ( $c_oid ) {
        #	    my $c_name = db_getContactName($c_oid);
        PrintAttribute($c_name);

        #	}
        #	else {
        #	    print "<td class='img'></td>\n";
        #	}

        print "</tr>\n";
    }
    print "</table>\n";
    print "<p>\n";

    $cur->finish();
    $dbh->disconnect();

    return $cnt;
}

#############################################################################
# UpdateRequest - update request
#############################################################################
sub UpdateRequest {
    print start_form( -name => 'updateRequest', -method => 'post', action => "$section_cgi" );

    # keep user info
    #    my $userString = param1('userstr');
    #    print hidden('userstr',$userString);
    #    saveAppHiddenVar();

    my $request_oid = param1('request_oid');

    if ( blankStr($request_oid) ) {
        printError("No request has been selected.");
        print end_form();
        return;
    }

    my $req_type = param1('request_type');
    if ( !blankStr($req_type) ) {
        print hidden( 'request_type', $req_type );
    }

    print "<h2>Update Request $request_oid</h2>\n";
    print hidden( 'request_oid', $request_oid );

    my %db_val = SelectRequest($request_oid);

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    print "<p>Required fields are marked with (*).</p>\n";

#    print "<p><font color='red'>Note: A request is considered as a new request if and only if status = 'new request'.</font></p>\n";

    print "<table class='img' border='1'>\n";
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $attr_val  = "";
        if ( $db_val{$attr_name} ) {
            $attr_val = $db_val{$attr_name};
        }
        if ( $attr_name eq 'country' ) {
            $attr_val = convertCountry($attr_val);
        }

        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type};
        my $len       = $k->{length};
        my $edit      = $k->{can_edit};

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        if ( $k->{is_required} ) {
            print escapeHTML($disp_name) . " (*) </th>\n";
        } elsif ( $k->{url} ) {
            print alink( $k->{url}, $disp_name, 'target', 1 );
        } else {
            print escapeHTML($disp_name);
        }
        print "</th>\n";

        print "  <td class='img'   align='left'>\n";

        if (   !$edit
             && $attr_name ne 'status'
             && $attr_name ne 'assign_contact_oid'
             && $attr_name ne 'notes' )
        {
            print escapeHTML($attr_val);
        } elsif ( $data_type eq 'cv' && $k->{cv_query} ) {
            my @selects = db_getValues( $k->{cv_query} );
            print "<select name='$attr_name' class='img' size='1'>\n";
            if ( !$k->{is_required} ) {
                print "   <option value=''> </option>\n";
            }
            for my $s0 (@selects) {
                print "    <option value='" . escapeHTML($s0) . "'";
                if ( $s0 eq $attr_val ) {
                    print " selected ";
                }
                print ">$s0</option>\n";
            }
            print "</select>\n";
        } elsif ( $data_type eq 'text' ) {
            print "<textarea name='$attr_name' cols='$len' rows='10'>" . $attr_val . "</textarea>";
        } else {
            my $size = 80;
            if ( $len && $size > $len ) {
                $size = $len;
            }

            print "<input type='text' name='$attr_name' value='"
              . escapeHTML($attr_val)
              . "' size='$size'"
              . " maxLength='$len'/>";
        }
        print "</td>\n";
        print "</tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbUpdateRequest" value="Update Record" class="meddefbutton" />';
    print "&nbsp; \n";
    if ( $req_type eq 'new' ) {
        print '<input type="submit" name="_section_UserTool:showNewRequest" value="Cancel" class="smbutton" />';
    } else {
        print '<input type="submit" name="_section_UserTool:showRequest" value="Cancel" class="smbutton" />';
    }

    print "<p>\n";

    print '<input type="submit" name="_section_UserTool:dbAddContact" value="Create Account" class="medbutton" />';
    print nbsp(3);
    print "With initial password (default is used if no initial password): ";
    print "<input type='text' name='init_pwd' value=''" . "size='30' maxLength='30'/>";
    print nbsp(3);
    print "JGI User?";
    print nbsp(1);
    print "<select name='jgi_user' class='img' size='1'>\n";
    print "<option value='No' selected>No</option>\n";
    print "<option value='Yes'>Yes</option>\n";
    print "</select>\n";

    print "<p>\n";
    printHomeLink();

    print end_form();
}

####################################################################
# select Request_Account data given an ID
####################################################################
sub SelectRequest {
    my ($request_oid) = @_;

    my %db_val;

    my $dbh = Connect_IMG_Contact();

    my $db_id = $request_oid;

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    my $sql = "";
    for my $k (@attrs) {
        my $attr_name = $k->{name};

        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } else {
            $sql .= ", " . $attr_name;
        }
    }

    $sql .= " from request_account where request_oid = $db_id";

    # print "<p>SQL: $sql</p>\n";
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my @flds = $cur->fetchrow_array();

    # save result
    my $j = 0;
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        if ( scalar(@flds) < $j ) {
            last;
        }
        $db_val{$attr_name} = $flds[$j];
        $j++;
    }

    # finish
    $cur->finish();
    $dbh->disconnect();

    # modified_by
    if ( $db_val{'modified_by'} ) {
        $db_val{'modified_by'} = db_getContact( $db_val{'modified_by'} );
    }

    return %db_val;
}

#############################################################################
# dbUpdateRequest - update request_account info in database
#############################################################################
sub dbUpdateRequest {

    # get user info
    my $contact_oid = getContactOid();

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    my $msg = "";

    # get request_oid
    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        $msg = "No request has been selected for update.";
        return $msg;
    }

    my $email_redirect = 0;
    my $email_denied   = 0;
    my $email          = "";
    my $uname          = "";

    my $sql = "update request_account set modified_by = $contact_oid, " . "mod_date = sysdate";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type};
        my $len       = $k->{length};
        my $edit      = $k->{can_edit};

        if (   !$edit
             && $attr_name ne 'status'
             && $attr_name ne 'notes'
             && $attr_name ne 'assign_contact_oid' )
        {
            next;
        }

        my $val = param1($attr_name);

        if (    $attr_name eq 'status'
             && $val eq 'redirect to IMG EDU' )
        {
            $email_redirect = 1;
        } elsif (    $attr_name eq 'status'
                  && $val eq 'rejected' )
        {
            $email_denied = 1;
        } elsif ( $attr_name eq 'email' && $val ) {
            $email = $val;
        } elsif ( $attr_name eq 'name' && $val ) {
            $uname = $val;
        }

        if ( blankStr($val) ) {
            if ( $k->{is_required} ) {
                $msg = "Attribute '$disp_name' must have a value.";
                return $msg;
            }

            $sql .= ", $attr_name = null";
            next;
        }

        if ( $data_type eq 'int' ) {
            if ( !blankStr($val) && !isInt($val) ) {
                $msg = "Attribute '$disp_name' must be an integer.";
                last;
            }

            $sql .= ", $attr_name = $val";
        } else {
            $val =~ s/'/''/g;    # replace ' with ''
            $sql .= ", $attr_name = '" . $val . "'";
        }
    }

    $sql .= " where request_oid = $request_oid";

    if ( blankStr($msg) ) {
        my @sqlList = ($sql);
        db_con_sqlTrans( \@sqlList );

        if ( $email_redirect && $email ) {
            if ( !$uname ) {
                $uname = "User";
            }
            EmailRedirectEDU( $email, $uname );
        } elsif ( $email_denied && $email ) {
            if ( !$uname ) {
                $uname = "User";
            }
            EmailRequestDenied( $email, $uname );
        }
    }

    return $msg;
}

#############################################################################
# RedirectToEDU
#############################################################################
sub RedirectToEDU {

    # get user info
    my $contact_oid = getContactOid();

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    my $msg = "";

    # get request_oid
    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        $msg = "No request has been selected for update.";
        return $msg;
    }

    my $dbh = Connect_IMG_Contact();
    my $sql = "select name, email from request_account where request_oid = ? ";
    my $cur = $dbh->prepare($sql);
    $cur->execute($request_oid);
    my ( $uname, $email ) = $cur->fetchrow();
    $cur->finish();
    $dbh->disconnect();

    $sql =
        "update request_account set modified_by = $contact_oid, "
      . "mod_date = sysdate, status = 'redirect to IMG EDU' "
      . "where request_oid = $request_oid ";

    my @sqlList = ($sql);
    db_con_sqlTrans( \@sqlList );

    if ($email) {
        if ( !$uname ) {
            $uname = "User";
        }
        EmailRedirectEDU( $email, $uname );
    }

    return $msg;
}

#############################################################################
# RequestDenied
#############################################################################
sub RequestDenied {

    # get user info
    my $contact_oid = getContactOid();

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    my $msg = "";

    # get request_oid
    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        $msg = "No request has been selected for update.";
        return $msg;
    }

    my $dbh = Connect_IMG_Contact();
    my $sql = "select name, email from request_account where request_oid = ? ";
    my $cur = $dbh->prepare($sql);
    $cur->execute($request_oid);
    my ( $uname, $email ) = $cur->fetchrow();
    $cur->finish();
    $dbh->disconnect();

    $sql =
        "update request_account set modified_by = $contact_oid, "
      . "mod_date = sysdate, status = 'rejected' "
      . "where request_oid = $request_oid ";

    my @sqlList = ($sql);
    db_con_sqlTrans( \@sqlList );

    if ($email) {
        if ( !$uname ) {
            $uname = "User";
        }
        EmailRequestDenied( $email, $uname );
    }

    return $msg;
}

#############################################################################
# dbAddContact - add a new contact into IMG/ER database
#############################################################################
sub dbAddContact {

    # get user info
    my $contact_oid = getContactOid();

    my $username = param1('username');
    $username =~ s/ /\./g;

    my $msg = "";

    # get request_oid
    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        $msg = "No request has been selected for update.";
        return $msg;
    }

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    # check request info
    my $sql = "update request_account set modified_by = $contact_oid, " . "mod_date = sysdate";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type};
        my $len       = $k->{length};
        my $edit      = $k->{can_edit};

        if (   !$edit
             && $attr_name ne 'status' )
        {
            next;
        }

        my $val = param1($attr_name);
        if ( blankStr($val) ) {
            if ( $k->{is_required} ) {
                $msg = "Attribute '$disp_name' must have a value.";
                return $msg;
            }

            $sql .= ", $attr_name = null";
            next;
        }

        if ( $data_type eq 'int' ) {
            if ( !blankStr($val) && !isInt($val) ) {
                $msg = "Attribute '$disp_name' must be an integer.";
                last;
            }

            $sql .= ", $attr_name = $val";
        } else {
            if ( $attr_name eq 'username' ) {
                if ( length($attr_name) < 8 || length($attr_name) > 30 ) {
                    $msg = "Preferred login name must be between 8 and 30 characters.";
                    last;
                }
            }
            $val =~ s/'/''/g;    # replace ' with ''
            $sql .= ", $attr_name = '" . $val . "'";
        }
    }

    $sql .= " where request_oid = $request_oid";

    if ($msg) {
        return $msg;
    }

    my $str         = $sql;
    my @sqlList_req = ();
    push @sqlList_req, ($sql);

    # check init password
    my $init_pwd = param1('init_pwd');
    if ( blankStr($init_pwd) ) {

        # use reverse user name
        $init_pwd = "";
        my $uname = $username;
        my $len   = length($uname);
        if ( $len > 0 ) {
            for ( my $k = $len - 1 ; $k >= 0 ; $k-- ) {
                $init_pwd .= substr( $uname, $k, 1 );
            }
            $init_pwd .= "123";
        }

        if ( blankStr($init_pwd) ) {
            $msg = "Please assign an initial password.";
            return $msg;
        }
    }

    my $dbh = Connect_IMG_Contact();

    # check assign_contact_oid
    my $assign_contact_oid = param1('assign_contact_oid');
    if ( blankStr($assign_contact_oid) ) {

        # use next contact_oid
        my $sql = "select max(contact_oid) from contact";
        webLog("$sql\n");
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        my ($val) = $cur->fetchrow();
        if ($val) {
            $assign_contact_oid = $val + 1;
        } else {
            $assign_contact_oid = 1;
        }
        $cur->finish();
    } else {

        # assign contact id must be an integer
        if ( !isInt($assign_contact_oid) ) {
            $msg = "Attribute 'Assign Contact OID' must be an integer.";
            $dbh->disconnect();
            return $msg;
        }
    }

    # check whether contact_oid exist
    $sql = "select contact_oid from contact where contact_oid = $assign_contact_oid";
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($val) = $cur->fetchrow();
    $cur->finish();
    if ($val) {
        $msg = "Contact OID $assign_contact_oid has been taken.";
        $dbh->disconnect();
        return $msg;
    }

    my $db_uname = $username;
    $db_uname =~ s/'/''/g;    # replace ' with '', just in case
    my $lc_db_uname = lc($db_uname);
    $sql = "select contact_oid from contact where lower(username) = '$lc_db_uname'";
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    ($val) = $cur->fetchrow();
    $cur->finish();

    if ($val) {
        $msg = "User Name '$username' has been taken.";
        $dbh->disconnect();
        return $msg;
    }

    $dbh->disconnect();

    $sql =
        "insert into contact (contact_oid, username, password, "
      . "super_user, "
      . "name, title, department, email, "
      . "phone, organization, "
      . "address, city, state, country, comments, jgi_user, "
      . "caliban_id, caliban_user_name, "
      . "add_date) "
      . "values (";

    # contact_oid
    $sql .= $assign_contact_oid;

    # username
    $sql .= ", '" . $db_uname . "'";

    # password (encode)
    my $md5pw = md5_base64($init_pwd);
    $md5pw =~ s/'/''/g;    # replace ' with '', just in case
    $sql .= ", '" . $md5pw . "'";

    # super_user
    $sql .= ", 'No'";

    # rest of attributes
    my $email_to          = "";
    my $caliban_user_name = "";
    for my $attr2 (
                    'name',    'title', 'department', 'email',   'phone', 'organization',
                    'address', 'city',  'state',      'country', 'notes', 'jgi_user'
      )
    {
        my $a_val = param1($attr2);

        if ( $attr2 eq 'country' ) {
            $a_val = convertCountry($a_val);
        }

        if ( blankStr($a_val) ) {
            $sql .= ", null";
        } else {
            if ( $attr2 eq 'email' ) {
                $email_to          = $a_val;
                $caliban_user_name = $a_val;
            }

            $a_val =~ s/'/''/g;    # replace ' with '', just in case
            $sql .= ", '" . $a_val . "'";
        }
    }

    # get caliban ID
    my $caliban_id = '';
    if ($caliban_user_name) {
        $caliban_id = getCalibanId($caliban_user_name);

        if ( !$caliban_id && $caliban_user_name =~ /lbl/ ) {
            my ( $email2, $org ) = split( /\@/, $caliban_user_name );
            $caliban_id = getCalibanId($email2);
            if ($caliban_id) {
                $caliban_user_name = $email2;
            }
        }
    }
    if ( $caliban_id && isInt($caliban_id) ) {
        $sql .= ", " . $caliban_id . ", '" . $caliban_user_name . "'";
    } else {
        $sql .= ", null, null";
    }

    # add_date
    $sql .= ", sysdate)";
    my @sqlList_con = ();
    push @sqlList_con, ($sql);

    $sql = "update request_account " . "set assign_contact_oid = $assign_contact_oid, " . "status = 'account created'";
    if ( param1('notes') ) {
        my $notes = param1('notes');
        $notes =~ s/'/''/g;    # replace ' with ''
        $sql .= ", notes = '" . $notes . "'";
    }
    $sql .= " where request_oid = $request_oid";

    push @sqlList_req, ($sql);

    if ( blankStr($email_to) ) {
        $msg = "Cannot send email to user because email address is missing.";
        return $msg;
    } else {
        EmailAccountCreated( $email_to, $username );
    }

    if ( blankStr($msg) ) {
        db_con_sqlTrans( \@sqlList_req );
        db_con_sqlTrans( \@sqlList_con );
    }

    return $msg;
}

#############################################################################
# dbAddReqContact - add a new contact from request
#############################################################################
sub dbAddReqContact {

    # get user info
    my $contact_oid = getContactOid();

    my $msg = "";

    # get request_oid
    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        $msg = "No request has been selected for update.";
        return $msg;
    }

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    # check request info
    my $dbh = Connect_IMG_Contact();
    my $sql = "";
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        if ($sql) {
            $sql .= ", " . $attr_name;
        } else {
            $sql = "select " . $attr_name;
        }
    }
    $sql .= " from request_account where request_oid = ?";
    my $cur = $dbh->prepare($sql);
    $cur->execute($request_oid);
    my (@vals) = $cur->fetchrow();
    $cur->finish();

    my %fld_val;
    my $i = 0;
    for my $k (@attrs) {
        if ( $i >= scalar(@attrs) ) {
            last;
        }
        my $attr_name = $k->{name};
        $fld_val{$attr_name} = $vals[$i];
        $i++;
    }

    $sql = "update request_account set modified_by = $contact_oid, " . "mod_date = sysdate";

    my $caliban_user_name = '';
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type};
        my $len       = $k->{length};
        my $edit      = $k->{can_edit};

        if (   !$edit
             && $attr_name ne 'status' )
        {
            next;
        }

        my $val = $fld_val{$attr_name};
        if ( $attr_name eq 'email' ) {
            $caliban_user_name = $val;
        }

        if ( blankStr($val) ) {
            if ( $k->{is_required} ) {
                $msg = "Attribute '$disp_name' must have a value.";
                return $msg;
            }

            $sql .= ", $attr_name = null";
            next;
        }

        if ( $data_type eq 'int' ) {
            if ( !blankStr($val) && !isInt($val) ) {
                $msg = "Attribute '$disp_name' must be an integer.";
                last;
            }

            $sql .= ", $attr_name = $val";
        } else {
            $val =~ s/'/''/g;    # replace ' with ''
            $sql .= ", $attr_name = '" . $val . "'";
        }
    }

    $sql .= " where request_oid = $request_oid";

    if ($msg) {
        return $msg;
    }

    my $str         = $sql;
    my @sqlList_req = ();
    push @sqlList_req, ($sql);

    # check init password
    my $init_pwd = param1('init_pwd');
    if ( blankStr($init_pwd) ) {

        # use reverse user name
        $init_pwd = "";
        my $uname = $fld_val{'username'};
        $uname =~ s/ /\./g;
        my $len = length($uname);
        if ( $len > 0 ) {
            for ( my $k = $len - 1 ; $k >= 0 ; $k-- ) {
                $init_pwd .= substr( $uname, $k, 1 );
            }
            $init_pwd .= "123";
        }

        if ( blankStr($init_pwd) ) {
            $msg = "Please assign an initial password.";
            return $msg;
        }
    }

    # check assign_contact_oid
    my $assign_contact_oid = $fld_val{'assign_contact_oid'};
    if ( blankStr($assign_contact_oid) ) {

        # use next contact_oid
        my $sql = "select max(contact_oid) from contact";
        webLog("$sql\n");
        my $cur = $dbh->prepare($sql);
        $cur->execute();
        my ($val) = $cur->fetchrow();
        if ($val) {
            $assign_contact_oid = $val + 1;
        } else {
            $assign_contact_oid = 1;
        }
        $cur->finish();
    } else {

        # assign contact id must be an integer
        if ( !isInt($assign_contact_oid) ) {
            $msg = "Attribute 'Assign Contact OID' must be an integer.";
            $dbh->disconnect();
            return $msg;
        }
    }

    # check whether contact_oid exist
    $sql = "select contact_oid from contact where contact_oid = $assign_contact_oid";
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($val) = $cur->fetchrow();
    $cur->finish();
    if ($val) {
        $msg = "Contact OID $assign_contact_oid has been taken.";
        $dbh->disconnect();
        return $msg;
    }

    # check whether username exist
    my $username = $fld_val{'username'};
    $username =~ s/ /\./g;

    my $db_uname = $username;
    $db_uname =~ s/'/''/g;    # replace ' with '', just in case
    my $lc_db_uname = lc($db_uname);
    $sql = "select contact_oid from contact where lower(username) = '$lc_db_uname'";
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    ($val) = $cur->fetchrow();
    $cur->finish();

    if ($val) {
        $msg = "User Name '$username' has been taken.";
        $dbh->disconnect();
        return $msg;
    }

    # get country
    $sql = "select r.country from request_account r where r.request_oid = ? ";
    $cur = $dbh->prepare($sql);
    $cur->execute($request_oid);
    my ($country) = $cur->fetchrow();
    $cur->finish();
    $country = convertCountry($country);
    $dbh->disconnect();

    $sql =
        "insert into contact (contact_oid, username, password, "
      . "super_user, "
      . "name, title, department, email, phone, organization, "
      . "address, city, state, country, comments, jgi_user, add_date) "
      . "select ";

    # contact_oid
    $sql .= $assign_contact_oid;

    # username
    $username =~ s/'/''/g;    # replace ' with '', just in case
    $sql .= ", '" . $db_uname . "'";

    # password (encode)
    my $md5pw = md5_base64($init_pwd);
    $md5pw =~ s/'/''/g;       # replace ' with '', just in case
    $sql .= ", '" . $md5pw . "'";

    # jgi user?
    my $jgi_user = param1('jgi_user');
    if ( !$jgi_user ) {
        $jgi_user = 'No';
    }

    $country =~ s/'/''/g;     # replace ' with ''

    $sql .=
        ", 'No', r.name, r.title, r.department, r.email, "
      . "r.phone, r.organization, r.address, r.city, r.state, " . "'"
      . $country
      . "', r.notes, '"
      . $jgi_user
      . "', sysdate "
      . "from request_account r where r.request_oid = $request_oid ";

    # add_date
    my @sqlList_con = ();
    push @sqlList_con, ($sql);

    if ($caliban_user_name) {
        my $caliban_id = getCalibanId($caliban_user_name);

        if ( !$caliban_id && $caliban_user_name =~ /lbl/ ) {
            my ( $email2, $org ) = split( /\@/, $caliban_user_name );
            $caliban_id = getCalibanId($email2);
            if ($caliban_id) {
                $caliban_user_name = $email2;
            }
        }

        if ( $caliban_id && isInt($caliban_id) ) {
            my $sql2 =
                "update contact set caliban_id = $caliban_id, "
              . "caliban_user_name = '"
              . $caliban_user_name . "' "
              . "where contact_oid = $assign_contact_oid";
            push @sqlList_con, ($sql2);
        }
    }

    $sql = "update request_account " . "set assign_contact_oid = $assign_contact_oid, " . "status = 'account created'";
    if ( $fld_val{'notes'} ) {
        my $notes = $fld_val{'notes'};
        $notes =~ s/'/''/g;    # replace ' with ''
        $sql .= ", notes = '" . $notes . "'";
    }
    $sql .= " where request_oid = $request_oid";

    push @sqlList_req, ($sql);

    my $email_to = $fld_val{'email'};
    if ( blankStr($email_to) ) {
        $msg = "Cannot send email to user because email address is missing.";
        return $msg;
    } else {
        EmailAccountCreated( $email_to, $username );
    }

    if ( blankStr($msg) ) {
        db_con_sqlTrans( \@sqlList_req );
        db_con_sqlTrans( \@sqlList_con );
    }

    return $msg;
}

##########################################################################
# EmailAccountCreated
##########################################################################
sub EmailAccountCreated {
    my ( $email_to, $uname ) = @_;

    if ( blankStr($email_to) ) {
        return;
    }

    my $sender_name  = "Amy Chen";
    my $sender_email = "imachen\@lbl.gov";

    my $contact_oid = getContactOid();
    if ($contact_oid) {
        my $dbh = Connect_IMG_Contact();
        my $sql = "select name, email from contact where contact_oid = ? ";
        my $cur = $dbh->prepare($sql);
        $cur->execute($contact_oid);

        my ( $name2, $email2 ) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();

        if ( $name2 && $email2 ) {
            $sender_name  = $name2;
            $sender_email = $email2;
        }
    }

    my $email_from = $sender_name . " <" . $sender_email . ">";

    # subject
    my $email_subj = "Your IMG account request has been approved";

    my $email_data = qq{
	Dear colleague,

	An account has been created for you in IMG.
        Please use your JGI SSO account to log into
        IMG. Contact us if you encounter any problems.

        After you log into IMG, you can update your
        contact information in the MyIMG tab.

        Please contact Amy Chen (IMAChen\@lbl.gov) if you
        encounter any login issues.

	Best regards,
        $sender_name
      };

    sendEmail( $email_from, $email_to, $email_subj, $email_data );
}

##########################################################################
# EmailRedirectEDU
##########################################################################
sub EmailRedirectEDU {
    my ( $email_to, $uname ) = @_;

    if ( blankStr($email_to) ) {
        return;
    }

    my $sender_name  = "Amy Chen";
    my $sender_email = "imachen\@lbl.gov";

    my $contact_oid = getContactOid();
    if ($contact_oid) {
        my $dbh = Connect_IMG_Contact();
        my $sql = "select name, email from contact where contact_oid = ? ";
        my $cur = $dbh->prepare($sql);
        $cur->execute($contact_oid);

        my ( $name2, $email2 ) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();

        if ( $name2 && $email2 ) {
            $sender_name  = $name2;
            $sender_email = $email2;
        }
    }

    my $email_from = $sender_name . " <" . $sender_email . ">";

    # subject
    my $email_subj = "Your IMG account request";

    my $email_data = qq{
	Dear colleague,

        Please use IMG EDU available at http://img.jgi.doe.gov/edu/ 
        for your education and/or teaching needs. 
        Access to this version of IMG does not require login 
        and password.

	Best regards,
        $sender_name
      };

    sendEmail( $email_from, $email_to, $email_subj, $email_data );
}

##########################################################################
# EmailRequestDenied
##########################################################################
sub EmailRequestDenied {
    my ( $email_to, $uname ) = @_;

    if ( blankStr($email_to) ) {
        return;
    }

    my $sender_name  = "Amy Chen";
    my $sender_email = "imachen\@lbl.gov";

    my $contact_oid = getContactOid();
    if ($contact_oid) {
        my $dbh = Connect_IMG_Contact();
        my $sql = "select name, email from contact where contact_oid = ? ";
        my $cur = $dbh->prepare($sql);
        $cur->execute($contact_oid);

        my ( $name2, $email2 ) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();

        if ( $name2 && $email2 ) {
            $sender_name  = $name2;
            $sender_email = $email2;
        }
    }

    my $email_from = $sender_name . " <" . $sender_email . ">";

    # subject
    my $email_subj = "Your IMG account request";

    my $email_data = qq{
	Dear colleague,

        Your request for an IMG account was not approved 
        because you have not filled the mandatory fields 
        without using abbreviations.  Please resubmit your 
        request and remember to provide a clear and detailed 
        reason in English for requesting an account.

	Best regards,
        $sender_name
      };

    sendEmail( $email_from, $email_to, $email_subj, $email_data );
}

#############################################################################
# ResetPassword
#############################################################################
sub ResetPassword {
    print start_form( -name => 'resetPassword', -method => 'post', action => "$section_cgi" );

    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        printError("No request has been selected.");
        print end_form();
        return;
    }

    my $new_pwd = param1('new_pwd');
    if ( blankStr($new_pwd) ) {
        printError("No new password has been entered.");
        print end_form();
        return;
    }

    # get request info
    my $dbh = WebFunctions::Connect_IMG_Contact();
    if ( !$dbh ) {
        return -1;
    }

    my $sql = "select request_oid, status, assign_contact_oid " . "from request_account where request_oid = $request_oid";
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ( $r_id, $r_status, $c_oid ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( !$r_id ) {
        printError("This is not a correct user request.");
        print end_form();
        return;
    }

    if ( !$c_oid ) {
        printError("No IMG Contact is created for this request.");
        print end_form();
        return;
    }

    if ( $r_status ne "account created" ) {
        printError("No IMG Contact is created for this request.");
        print end_form();
        return;
    }

    # password (encode)
    my $md5pw = md5_base64($new_pwd);
    $md5pw =~ s/'/''/g;    # replace ' with '', just in case
    $sql = "update Contact set password = '" . $md5pw . "' where contact_oid = $c_oid";

    my @sqlList_con = ($sql);
    db_con_sqlTrans( \@sqlList_con );

    print "<h4>Password for Contact $c_oid has been reset.</h4>\n";

    print '<input type="submit" name="_section_UserTool:showRequest" value="OK" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}

#############################################################################
# ConfirmDeleteRequest - confirm delete request
#############################################################################
sub ConfirmDeleteRequest {

    print start_form( -name => 'confirmDeleteRequest', -method => 'post', action => "$section_cgi" );

    # get request oid
    my $request_oid = param1('request_oid');

    if ( blankStr($request_oid) ) {
        printError("No request has been selected.");
        print end_form();
        return;
    }

    my $req_type = param1('request_type');
    if ( !blankStr($req_type) ) {
        print hidden( 'request_type', $req_type );
    }

    print "<h2>Confirm Deleting Request $request_oid</h2>\n";
    print hidden( 'request_oid', $request_oid );

    my %db_val = SelectRequest($request_oid);

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    print "<p>Please click 'Delete' to confirm the deletion, or click 'Cancel' to cancel the operation.</p>\n";

    print "<table class='img' border='1'>\n";
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $attr_val  = "";
        if ( $db_val{$attr_name} ) {
            $attr_val = $db_val{$attr_name};
        }

        my $disp_name = $k->{display_name};
        my $data_type = $k->{data_type};
        my $len       = $k->{length};
        my $edit      = $k->{can_edit};

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        print escapeHTML($disp_name);
        print "</th>\n";

        print "  <td class='img'   align='left'>\n";

        if ( $data_type eq 'text' ) {
            print "<textarea name='$attr_name' " . "cols='$len' rows='10' readOnly >" . $attr_val . "</textarea>";
        } else {
            print escapeHTML($attr_val);
        }

        print "</td>\n";
        print "</tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbDeleteRequest" value="Delete" class="smdefbutton" />';
    print "&nbsp; \n";
    if ( $req_type eq 'new' ) {
        print '<input type="submit" name="_section_UserTool:showNewRequest" value="Cancel" class="smbutton" />';
    } else {
        print '<input type="submit" name="_section_UserTool:showRequest" value="Cancel" class="smbutton" />';
    }

    print "<p>\n";

    printHomeLink();

    print end_form();
}

#############################################################################
# dbDeleteRequest - delete request_account info from database
#############################################################################
sub dbDeleteRequest {
    my $msg = "";

    # get request_oid
    my $request_oid = param1('request_oid');
    if ( blankStr($request_oid) ) {
        $msg = "No request has been selected for update.";
        return $msg;
    }

    my $sql     = "delete from request_account where request_oid = $request_oid";
    my @sqlList = ($sql);
    db_con_sqlTrans( \@sqlList );

    return $msg;
}

#############################################################################
# dbBulkAccountApproval
#############################################################################
sub dbBulkAccountApproval {

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<h1>Process Bulk Account Requests</h1>\n";

    # get user info
    my $contact_oid = getContactOid();

    my $msg = "";

    my @success = ();
    my @fail    = ();

    ## check ID ranges
    my $id_val1 = param('id_val1');
    if ( !$id_val1 || !isInt($id_val1) ) {
        WebFunctions::showErrorPage("Incorrect lower range: $id_val1");
        return;
    }

    my $id_val2 = param('id_val2');
    if ( !$id_val2 || !isInt($id_val2) ) {
        WebFunctions::showErrorPage("Incorrect upper range: $id_val2");
        return;
    }

    if ( $id_val1 > $id_val2 ) {
        WebFunctions::showErrorPage("Incorrect range: $id_val1 to $id_val2");
        return;
    }

    # get Request_Account definition
    my $def_req = def_Request_Account();
    my @attrs   = @{ $def_req->{attrs} };

    # check request info
    my @request_oids = ();
    my $dbh          = Connect_IMG_Contact();
    my $sql =
      "select request_oid from request_account where status = 'new request' and request_oid between ? and ? order by 1 ";
    my $cur = $dbh->prepare($sql);
    $cur->execute( $id_val1, $id_val2 );
    for ( ; ; ) {
        my ($r_oid) = $cur->fetchrow();
        last if !$r_oid;
        push @request_oids, ($r_oid);
    }
    $cur->finish();
    if ( scalar(@request_oids) == 0 ) {
        WebFunctions::showErrorPage("No new requests with ID between $id_val1 and $id_val2");
        return;
    }

    $sql = "select max(contact_oid) from contact";
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my ($assign_contact_oid) = $cur->fetchrow();
    $cur->finish();
    if ( !$assign_contact_oid ) {
        WebFunctions::showErrorPage("Error in assigning contact oid.");
        return;
    }

    my %username_h;
    $sql = "select contact_oid, username from contact where username is not null";
    $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ( $c_id, $c_name ) = $cur->fetchrow();
        last if !$c_id;
        $c_name = lc($c_name);
        $username_h{$c_name} = $c_id;
    }
    $cur->finish();

    my $sql9 = "";
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        if ($sql9) {
            $sql9 .= ", " . $attr_name;
        } else {
            $sql9 = "select " . $attr_name;
        }
    }
    $sql9 .= " from request_account where request_oid = ?";
    my $cur9 = $dbh->prepare($sql9);

    my @sqlList_req = ();
    my @sqlList_con = ();

    my $request_oid = 0;
    for $request_oid (@request_oids) {
        $cur9->execute($request_oid);
        my (@vals) = $cur9->fetchrow();
        $cur9->finish();

        my $failed = 0;

        my %fld_val;
        my $i = 0;
        for my $k (@attrs) {
            if ( $i >= scalar(@attrs) ) {
                last;
            }
            my $attr_name = $k->{name};
            $fld_val{$attr_name} = $vals[$i];
            $i++;
        }

        # check whether username exist
        my $username = $fld_val{'username'};
        $username =~ s/ /\./g;
        $username = lc($username);
        if ( $username_h{$username} ) {
            $msg .= "(Request $request_oid) User Name '$username' has been taken.";
            push @fail, ($request_oid);
            $failed = 1;
            next;
        }
        $username_h{$username} = $username;

        my $email_to = $fld_val{'email'};
        if ( blankStr($email_to) ) {
            $msg .= "(Request $request_oid) Email address is missing.";
            push @fail, ($request_oid);
            $failed = 1;
            next;
        }

        my $sql = "update request_account set modified_by = $contact_oid, " . "mod_date = sysdate";

        for my $k (@attrs) {
            my $attr_name = $k->{name};
            my $disp_name = $k->{display_name};
            my $data_type = $k->{data_type};
            my $len       = $k->{length};
            my $edit      = $k->{can_edit};

            if (   !$edit
                 && $attr_name ne 'status' )
            {
                next;
            }

            my $val = $fld_val{$attr_name};
            if ( blankStr($val) ) {
                if ( $k->{is_required} ) {
                    $msg .= "(Request $request_oid) Attribute '$disp_name' must have a value.\n";
                    push @fail, ($request_oid);
                    $failed = 1;
                    last;
                }

                $sql .= ", $attr_name = null";
                next;
            }

            if ( $data_type eq 'int' ) {
                if ( !blankStr($val) && !isInt($val) ) {
                    $msg .= "(Request $request_oid) Attribute '$disp_name' must be an integer.";
                    push @fail, ($request_oid);
                    $failed = 1;
                    last;
                }

                $sql .= ", $attr_name = $val";
            } else {
                $val =~ s/'/''/g;    # replace ' with ''
                $sql .= ", $attr_name = '" . $val . "'";
            }
        }

        $sql .= " where request_oid = $request_oid";

        if ($failed) {
            next;
        }

        my $str = $sql;
        print "<p>SQL: $str\n";

        # check init password
        my $init_pwd = param1('init_pwd');
        if ( blankStr($init_pwd) ) {

            # use reverse user name
            $init_pwd = "";
            my $uname = $fld_val{'username'};
            $uname =~ s/ /\./g;
            my $len = length($uname);
            if ( $len > 0 ) {
                for ( my $k = $len - 1 ; $k >= 0 ; $k-- ) {
                    $init_pwd .= substr( $uname, $k, 1 );
                }
                $init_pwd .= "123";
            }

            if ( blankStr($init_pwd) ) {
                WebFunctions::showErrorPage("Error in creating initial password.");
                return;
            }
        }

        # check assign_contact_oid
        $assign_contact_oid += 1;

        # get country
        my $country = $fld_val{'country'};
        $country = convertCountry($country);

        $sql =
            "insert into contact (contact_oid, username, password, "
          . "super_user, "
          . "name, title, department, email, phone, organization, "
          . "address, city, state, country, comments, jgi_user, add_date) "
          . "select ";

        # contact_oid
        $sql .= $assign_contact_oid;

        # username
        $username = $fld_val{'username'};
        my $db_uname = $username;
        $db_uname =~ s/'/''/g;    # replace ' with '', just in case
        $sql .= ", '" . $db_uname . "'";

        # password (encode)
        my $md5pw = md5_base64($init_pwd);
        $md5pw =~ s/'/''/g;       # replace ' with '', just in case
        $sql .= ", '" . $md5pw . "'";

        my $jgi_user = 'No';
        $country =~ s/'/''/g;     # replace ' with ''

        $sql .=
            ", 'No', r.name, r.title, r.department, r.email, "
          . "r.phone, r.organization, r.address, r.city, r.state, " . "'"
          . $country
          . "', r.notes, '"
          . $jgi_user
          . "', sysdate "
          . "from request_account r where r.request_oid = $request_oid ";

        # add_date
        push @sqlList_con, ($sql);

        $sql =
            "update request_account "
          . "set assign_contact_oid = $assign_contact_oid, "
          . "status = 'account created', modified_by = $contact_oid, "
          . "mod_date = sysdate";
        if ( $fld_val{'notes'} ) {
            my $notes = $fld_val{'notes'};
            $notes =~ s/'/''/g;    # replace ' with ''
            $sql .= ", notes = '" . $notes . "'";
        }
        $sql .= " where request_oid = $request_oid";

        push @sqlList_req, ($sql);

        $email_to = $fld_val{'email'};
        EmailAccountCreated( $email_to, $username );

        push @success, ($request_oid);
    }
    $dbh->disconnect();

    print "<h3>Statistics:</h3>\n";
    if ( scalar(@success) > 0 ) {
        print "<p>Success: " . join( ", ", @success ) . "\n";
    }
    if ( scalar(@fail) > 0 ) {
        print "<p>Fail: " . join( ", ", @fail ) . "\n";
    }

    my @msg_list = split( /\n/, $msg );
    if ( scalar(@msg_list) > 0 ) {
        print "<h3>Error Messages:</h3>\n";
        for my $msg1 (@msg_list) {
            print "<p>$msg1\n";
        }
    }

    if ( scalar(@success) > 0 ) {
        db_con_sqlTrans( \@sqlList_req );
        db_con_sqlTrans( \@sqlList_con );
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:showNewRequest" value="OK" class="smbutton" />';
    printHomeLink();

    print end_form();
}

#############################################################################
# ShowFunctions - show additional admin functions
#############################################################################
sub ShowFunctions {

    print start_form( -name => 'showFunctions', -method => 'post', action => "$section_cgi" );

    print "<p>\n";

    print "<h1>Test</h1>\n";

    print '<input type="submit" name="_section_UserTool:testFunctions" value="Admin Functions" class="medbutton" />';

    printHomeLink();

    print end_form();
}

#############################################################################
# EditAnnouncement
#############################################################################
sub EditAnnouncement {
    my ($type) = @_;

    my $text2 = "Announcement";
    if ( !$type ) {
        $type = 'Home Page';
    }
    if ( $type eq 'Agreement' ) {
        $text2 = $type;
    }

    print start_form( -name => 'editAnnouncement', -method => 'post', action => "$section_cgi" );

    print "<p>\n";

    print "<h1>Edit $text2</h1>\n";

    print hidden( 'type', $type );

    my $dbh = WebFunctions::Connect_IMG;
    my $sql = qq{
        select c.username, a.mod_date,
	to_char(a.mod_date, 'DD-MON-YYYY HH24:MI:SS')
            from announcement a, contact c
	    where a.modified_by = c.contact_oid
	    and a.type = ?
            order by 2 desc
        };
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute($type);
    my ( $cname, $d1, $d2 ) = $cur->fetchrow();
    $cur->finish();
    $dbh->disconnect();

    print "<p>Last modified by " . escapeHTML($cname) . " (" . $d2 . ").</p>\n";

    my $notes = db_getValue( "select notes from announcement where type = '" . $type . "'" );

    print "<p>Please enter text in HTML format.</p>\n";

    print "<textarea id='announce' name='announce' " . "rows='30' cols='80' maxLength='4000'>$notes</textarea>\n";

    print "<p>\n";

    print '<input type="submit" name="_section_UserTool:dbUpdateAnnounce" value="Update Text" class="meddefbutton" />';

    printHomeLink();

    print end_form();
}

#############################################################################
# dbUpdateAnnounce
#############################################################################
sub dbUpdateAnnounce {

    # get user info
    my $contact_oid = getContactOid();
    my $type        = param1('type');
    if ( !$type ) {
        $type = 'Home Page';
    }

    my $msg = "";

    my $notes = param1('announce');
    $notes =~ s/'/''/g;    # replace ' with ''
    if ( length($notes) > 4000 ) {
        $notes = substr( $notes, 0, 4000 );
    }
    my $sql =
        "update announcement set notes = '"
      . $notes
      . "', modified_by = $contact_oid, mod_date = sysdate "
      . "where type = '"
      . $type . "'";
    my @sqlList = ($sql);
    db_sqlTrans( \@sqlList );

    return $msg;
}

##########################################################################
# ViewIMGContacts (HTML table display)
##########################################################################
sub ViewIMGContacts {

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    my $selected_contact_oid = param1('selected_contact_oid');
    my $sort_by              = param1('sort_by');
    if ( blankStr($sort_by) ) {
        $sort_by = 'username';
    }

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql = qq{ 
        select distinct c1.img_group, c2.username, c2.name
            from contact c1, contact c2
	    where c1.img_group = c2.contact_oid
	    order by 1
        };
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my %img_groups;

    for ( ; ; ) {
        my ( $group_oid, $c_username, $c_name ) = $cur->fetchrow_array();
        if ( !$group_oid ) {
            last;
        }

        $img_groups{$group_oid} = $c_username;

        #	print "<p>Group $group_oid: " . escapeHTML($c_username) .
        #	    "; PI: " . escapeHTML($c_name) . "</p>\n";
    }
    $cur->finish();

    print "<h1>All IMG Contacts</h1>\n";

    #    print "<h2>IMG Groups</h2>\n";

    print "<p>IMG Group\n";
    my $img_group = param1('img_group');
    print "<select name='img_group' class='img' size='1' >\n";
    print "<option value=''></option>\n";
    for my $k ( sort ( keys %img_groups ) ) {
        print "<option value='$k'";
        if ( $img_group && $k eq $img_group ) {
            print " selected ";
        }
        print ">" . $img_groups{$k} . " (ID: $k)" . "</option>\n";
    }
    print "</select>\n";
    print nbsp(1);
    print '<input type="submit" name="_section_UserTool:viewImgContacts" value="Show Group Members" class="meddefbutton" />';
    print "<br/>\n";

    # contact filter
    my %names_h = (
                    'username'     => 'IMG User ID',
                    'name'         => 'Name',
                    'organization' => 'Organization',
                    'country'      => 'Country',
                    'email'        => 'Email'
    );
    print "<select name='contact_filter_name' class='img' size='1' >\n";
    my $selected_contact_filter_name = param('contact_filter_name');
    if ( !$selected_contact_filter_name
         && getSessionParam('selected_contact_filter_name') )
    {
        $selected_contact_filter_name = getSessionParam('selected_contact_filter_name');
    }
    print "<option value=''></option>\n";
    for my $k2 ( keys %names_h ) {
        print "<option value='$k2'";
        if ( $k2 eq $selected_contact_filter_name ) {
            print " selected ";
        }
        print ">" . $names_h{$k2} . "</option>\n";
    }
    print "</select>\n";
    print nbsp(1);
    print "match";
    print nbsp(1);
    my $selected_contact_filter_cond = param('contact_filter_cond');
    if ( !$selected_contact_filter_cond
         && getSessionParam('selected_contact_filter_cond') )
    {
        $selected_contact_filter_cond = getSessionParam('selected_contact_filter_cond');
    }

    print "<input type='text' name='contact_filter_cond' value='"
      . $selected_contact_filter_cond . "'"
      . "size='40' maxLength='40'/>";
    print nbsp(1);
    print '<input type="submit" name="_section_UserTool:viewImgContacts" value="Filter Contacts" class="smdefbutton" />';
    print "<br/>\n";

    if ($selected_contact_filter_name) {
        setSessionParam( 'selected_contact_filter_name', $selected_contact_filter_name );
    } else {
        setSessionParam( 'selected_contact_filter_name', "" );
    }

    if ($selected_contact_filter_cond) {
        setSessionParam( 'selected_contact_filter_cond', $selected_contact_filter_cond );
    } else {
        setSessionParam( 'selected_contact_filter_cond', "" );
    }

    #    print "<h2>IMG Contacts</h2>\n";

    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    #    if ( $contact_oid == 312 ) {
    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    print "<p>\n";

    my $where = " ";
    if ($img_group) {
        $where = " where img_group = $img_group";
    } else {
        my $sf1 = getSessionParam('selected_contact_filter_name');
        my $sf2 = getSessionParam('selected_contact_filter_cond');
        if ( $sf1 && $sf2 ) {
            my $db_val = lc($sf2);
            $db_val =~ s/'/''/g;    # replace ' by ''
            $where = " where lower($sf1) like '%" . $db_val . "%'";
        }
    }

    $sql = qq{ 
        select contact_oid, username, img_group, name, 
	organization, country, email, add_date
            from contact
	    $where
            order by $sort_by
        };

    #    if ( $contact_oid == 312 ) {
    #	print "<p>SQL: $sql</p>\n";
    #    }
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    my $link = "<a href='" . $main_cgi . "?section=UserTool&page=viewImgContacts&sort_by=";

    my $color1 = '#eeeeee';
    print "<p>\n";
    print "<div id='contact_div'>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Select</th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "username'>IMG Contact OID</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "username'>IMG User ID</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "img_group'>IMG Group</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "name'>Name</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "organization'>Organization</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "country'>Country</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "email'>Email</a></th>\n";
    print "<th class='img' bgcolor='$color1'>" . $link . "add_date'>Account Created</a></th>\n";
    print "<th class='img' bgcolor='$color1'>Most Recent Activity Date</th>\n";

    my %user_act = getMostRecentActDate();

    my $cnt = 0;
    for ( ; ; ) {
        my ( $c_oid, $username, $img_group, $name, $org, $country, $email, $add_date ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $username =~ /^MISSING/ ) {
            next;
        }

        $country = convertCountry($country);

        $cnt++;

        if ( $cnt % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='selected_contact_oid' value='$c_oid'";
        if ( $c_oid == $selected_contact_oid ) {
            print " checked ";
        }
        print "/></td>\n";

        PrintAttribute($c_oid);

        my $link = "<a href='" . $main_cgi . "?section=UserTool&page=showContactInfo" . "&selected_contact_oid=$c_oid' >";

        PrintAttribute( $link . $username . "</a>" );

        # IMG group
        if ($img_group) {
            my $grp = $img_groups{$img_group};
            PrintAttribute( $img_group . ": " . $grp );
        } else {
            PrintAttribute($img_group);
        }

        PrintAttribute($name);
        PrintAttribute($org);
        PrintAttribute($country);
        PrintAttribute($email);
        PrintAttribute($add_date);

        if ( $user_act{$c_oid} ) {
            PrintAttribute( $link . $user_act{$c_oid} . "</a>" );
        } else {
            PrintAttribute("-");
        }

        print "</tr>\n";
    }
    print "</table>\n";
    print "</div>\n";

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    if ( canUpdateContact($contact_oid) ) {

        # allow resetting password
        print "<p>\n";
        print
'<input type="submit" name="_section_UserTool:resetContactPwd" value="Reset Contact Password" class="medbutton" />';
        print nbsp(3);
        print "With new password (*): ";
        print "<input type='text' name='new_pwd' value=''" . "size='30' maxLength='30'/>";
    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# ViewIMGContacts_yui   (use Yahoo datatable)
##########################################################################
sub ViewIMGContacts_yui {

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    my $selected_contact_oid = param1('selected_contact_oid');
    my $sort_by              = param1('sort_by');
    if ( blankStr($sort_by) ) {
        $sort_by = 'username';
    }

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql = qq{ 
        select distinct c1.img_group, c2.username, c2.name
            from contact c1, contact c2
	    where c1.img_group = c2.contact_oid
	    order by 1
        };
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my %img_groups;

    for ( ; ; ) {
        my ( $group_oid, $c_username, $c_name ) = $cur->fetchrow_array();
        if ( !$group_oid ) {
            last;
        }

        $img_groups{$group_oid} = $c_username;

        #	print "<p>Group $group_oid: " . escapeHTML($c_username) .
        #	    "; PI: " . escapeHTML($c_name) . "</p>\n";
    }
    $cur->finish();

    print "<h1>All IMG Contacts</h1>\n";

    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();                          # sort delimiter
    $it->addColSpec("Select");
    $it->addColSpec( "IMG Contact OID", "number asc", "right", "", "IMG Contact OID" );
    $it->addColSpec( "IMG User ID",     "char asc",   "right", "", "IMG User ID" );

    $it->addColSpec( "IMG Group",                 "char asc", "left", "", "IMG Group" );
    $it->addColSpec( "Name",                      "char asc", "left", "", "Name" );
    $it->addColSpec( "Organization",              "char asc", "left", "", "Organization" );
    $it->addColSpec( "Country",                   "char asc", "left", "", "Country" );
    $it->addColSpec( "Email",                     "char asc", "left", "", "Email" );
    $it->addColSpec( "Account Created",           "char asc", "left", "", "Account Created" );
    $it->addColSpec( "Most Recent Activity Date", "char asc", "left", "", "Most Recent Activity Date" );
    $it->addColSpec( "JGI User Name(s)",          "char asc", "left", "", "JGI User Name(s)" );

    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    print "<p>\n";

    $sql = qq{ 
        select contact_oid, username, img_group, name, 
	organization, country, email, to_char(add_date, 'yyyy-mm-dd'),
	caliban_user_name
            from contact
            order by $sort_by
        };

    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    my $link = "<a href='" . $main_cgi . "?section=UserTool&page=viewImgContacts&sort_by=";

    my %user_act = getMostRecentActDate();

    my $cnt = 0;
    for ( ; ; ) {
        my ( $c_oid, $username, $img_group, $name, $org, $country, $email, $add_date, $jgi_user_name ) =
          $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $username =~ /^MISSING/ ) {
            next;
        }

        $country = convertCountry($country);

        $cnt++;

        my $row = "";

        $row .= $sd . "<input type='radio' name='selected_contact_oid' " . "value='$c_oid'";
        if ( $c_oid == $selected_contact_oid ) {
            $row .= " checked ";
        }
        $row .= "/>\t";

        $row .= $c_oid . $sd . $c_oid . "\t";

        my $link = "<a href='" . $main_cgi . "?section=UserTool&page=showContactInfo" . "&selected_contact_oid=$c_oid' >";

        $row .= $username . $sd . $link . $username . "</a>" . "\t";
        if ($img_group) {
            my $grp = $img_group . ":" . $img_groups{$img_group};
            $row .= $grp . $sd . $grp . "\t";
        } else {
            $row .= " " . $sd . " " . "\t";
        }
        $row .= $name . $sd . $name . "\t";
        $row .= $org . $sd . $org . "\t";
        $row .= $country . $sd . $country . "\t";
        $row .= $email . $sd . $email . "\t";
        $row .= $add_date . $sd . $add_date . "\t";

        if ( $user_act{$c_oid} ) {
            $row .= $user_act{$c_oid} . $sd . $link . $user_act{$c_oid} . "</a>" . "\t";
        } else {
            $row .= "-" . $sd . "-" . "\t";
        }

        $row .= $jgi_user_name . $sd . $jgi_user_name . "\t";

        $it->addRow($row);
    }

    if ( $cnt > 0 ) {
        $it->printOuterTable(1);
    } else {
        print "<h5>No IMG contacts.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    if ( canUpdateContact($contact_oid) ) {

        # allow resetting password
        print "<p>\n";
        print
'<input type="submit" name="_section_UserTool:resetContactPwd" value="Reset Contact Password" class="medbutton" />';
        print nbsp(3);
        print "With new password (*): ";
        print "<input type='text' name='new_pwd' value=''" . "size='30' maxLength='30'/>";
    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

#############################################################################
# ResetContactPassword
#############################################################################
sub ResetContactPassword {

    print start_form( -name => 'resetContactPassword', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();
    if ( !canUpdateContact($contact_oid) ) {
        return;
    }

    my $selected_oid = param1('selected_contact_oid');
    if ( blankStr($selected_oid) ) {
        printError("No contact has been selected.");
        print end_form();
        return;
    }

    my $new_pwd = param1('new_pwd');
    if ( blankStr($new_pwd) ) {
        printError("No new password has been entered.");
        print end_form();
        return;
    }

    # password (encode)
    my $md5pw = md5_base64($new_pwd);
    $md5pw =~ s/'/''/g;    # replace ' with '', just in case
    my $sql = "update Contact set password = '" . $md5pw . "' where contact_oid = $selected_oid";

    my @sqlList_con = ($sql);
    db_con_sqlTrans( \@sqlList_con );

    print "<h4>Password for Contact $selected_oid has been reset.</h4>\n";

    print '<input type="submit" name="_section_UserTool:viewImgContacts" value="OK" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# getMostRecentActDate
##########################################################################
sub getMostRecentActDate {
    my ($get_all) = @_;

    my %user_act;

    my $dbh = WebFunctions::Connect_IMG();

    my $sql = qq{
	select s.contact, max(to_char(s.submission_date, 'yyyy-mm-dd'))
	    from submission s
	    group by s.contact
	};
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $c_oid, $date1 ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        $user_act{$c_oid} = $date1 . " (submission)";
    }    # end for loop
    $cur->finish();

    $dbh->disconnect();

    # IMG ER
    my $database = 'IMG ER';
    $dbh = Connect_IMG_ER();

    $sql = qq{
	select c.contact_oid, max(to_char(t.add_date, 'yyyy-mm-dd'))
	    from taxon t, contact_taxon_permissions c
	    where c.taxon_permissions = t.taxon_oid
	    group by c.contact_oid
	    order by 1
	};
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $c_oid, $date2 ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $user_act{$c_oid} ) {
            if ($get_all) {
                if ( $user_act{$c_oid} ) {
                    $user_act{$c_oid} .= "|";
                }
                $user_act{$c_oid} .= $date2 . " (ER)";
            } else {
                if ( compareDate2( $user_act{$c_oid}, $date2 ) < 0 ) {
                    $user_act{$c_oid} = $date2 . " (ER)";
                } else {

                    # keep the existing one
                }
            }
        } else {
            $user_act{$c_oid} = $date2 . " (ER)";
        }
    }
    $cur->finish();

    $sql = qq{
	select ann.modified_by, max(to_char(ann.mod_date, 'yyyy-mm-dd'))
	    from gene_myimg_functions ann
	    group by ann.modified_by
	};
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $c_oid, $date3 ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ($get_all) {
            if ( $user_act{$c_oid} ) {
                $user_act{$c_oid} .= "|";
            }
            $user_act{$c_oid} .= $date3 . " (ER MyIMG)";
        } else {
            if ( $user_act{$c_oid} ) {
                if ( compareDate2( $user_act{$c_oid}, $date3 ) < 0 ) {
                    $user_act{$c_oid} = $date3 . " (ER MyIMG)";
                }
            } else {
                $user_act{$c_oid} = $date3 . " (ER MyIMG)";
            }
        }
    }
    $cur->finish();

    $dbh->disconnect();

    # IMG/M ER
    $database = 'IMG/M ER';
    $dbh      = Connect_IMG_MI();

    $sql = qq{
	select c.contact_oid, max(to_char(t.add_date, 'yyyy-mm-dd'))
	    from taxon t, contact_taxon_permissions c
	    where c.taxon_permissions = t.taxon_oid
	    group by c.contact_oid
	    order by 1
	};
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $c_oid, $date2 ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ($get_all) {
            if ( $user_act{$c_oid} ) {
                $user_act{$c_oid} .= "|";
            }
            $user_act{$c_oid} .= $date2 . " (MER)";
        } else {
            if ( $user_act{$c_oid} ) {
                if ( compareDate2( $user_act{$c_oid}, $date2 ) < 0 ) {
                    $user_act{$c_oid} = $date2 . " (MER)";
                }
            } else {
                $user_act{$c_oid} = $date2 . " (MER)";
            }
        }
    }
    $cur->finish();

    $sql = qq{
	select ann.modified_by, max(to_char(ann.mod_date, 'yyyy-mm-dd'))
	    from gene_myimg_functions ann
	    group by ann.modified_by
	};
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $c_oid, $date3 ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ($get_all) {
            if ( $user_act{$c_oid} ) {
                $user_act{$c_oid} .= "|";
            }
            $user_act{$c_oid} .= "|" . $date3 . " (MER MyIMG)";
        } else {
            if ( $user_act{$c_oid} ) {
                if ( compareDate2( $user_act{$c_oid}, $date3 ) < 0 ) {
                    $user_act{$c_oid} = $date3 . " (MER MyIMG)";
                }
            } else {
                $user_act{$c_oid} = $date3 . " (MER MyIMG)";
            }
        }
    }
    $cur->finish();

    $dbh->disconnect();

    return %user_act;
}

#############################################################################
# compareDate2 -- compare date1 and date2 (yyyy-mm-dd)
#
# -1: date1 is earlier than date2
# 0: date1 and date2 are the same
# 1: date1 is later than date2
#############################################################################
sub compareDate2 {
    my ( $date1, $date2 ) = @_;

    # check for null
    if ( blankStr($date1) ) {

        # no date1
        if ( blankStr($date2) ) {
            return 0;
        } else {
            return -1;
        }
    } else {
        if ( blankStr($date2) ) {
            return 1;
        }
    }

    # compare two not-null dates
    my ( $yr1, $mon1, $day1 ) = split( /\-/, $date1 );
    my ( $yr2, $mon2, $day2 ) = split( /\-/, $date2 );

    # compare year
    if ( $yr1 < $yr2 ) {
        return -1;
    } elsif ( $yr1 > $yr2 ) {
        return 1;
    }

    # compare month
    if ( $mon1 < $mon2 ) {
        return -1;
    } elsif ( $mon1 > $mon2 ) {
        return 1;
    }

    # compare day
    if ( $day1 < $day2 ) {
        return -1;
    } elsif ( $day1 > $day2 ) {
        return 1;
    }

    return 0;
}

##########################################################################
# ShowContactInfo
##########################################################################
sub ShowContactInfo {

    print start_form( -name => 'showContactInfo', -method => 'post', action => "$section_cgi" );

    #    my $contact_oid = getContactOid();

    my $selected_contact_oid = param1('selected_contact_oid');
    if ( !$selected_contact_oid ) {
        printError("No IMG contact has been selected.");
        print end_form();
        return;
    }

    print "<h2>IMG Contact Information (OID: $selected_contact_oid)</h2>\n";
    print hidden( 'selected_contact_oid', $selected_contact_oid );

    my $def_con = def_Contact();
    my @attrs   = @{ $def_con->{attrs} };
    my %db_vals = SelectContact($selected_contact_oid);

    # show contact info
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    my $tab = "";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $attr_val  = "";
        if ( $db_vals{$attr_name} ) {
            $attr_val = $db_vals{$attr_name};
        }

        # skip null
        if ( blankStr($attr_val) ) {
            next;
        }

        # show tab?
        if ( $tab ne $k->{tab} ) {
            $tab = $k->{tab};
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right' bgcolor='lightblue'>"
              . "<font color='darkblue'>"
              . $tab
              . "</font></th>\n";
            print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n";
            print "</tr>\n";
        }

        if ( $k->{name} eq 'img_group' ) {
            my $g_name = db_getContactName($attr_val);
            $attr_val .= " - " . $g_name;
        }

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . $k->{display_name} . "</th>\n";
        print "  <td class='img'   align='left'>" . escapeHTML($attr_val) . "</td></tr>\n";
    }

    ## JGI account?
    my $dbh = WebFunctions::Connect_IMG();
    my $sql = qq{
	select cja.jgi_user_id, cja.jgi_user_name
	    from contact_jgi_accounts cja
	    where cja.contact_oid = $selected_contact_oid
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ( $jgi_user_id, $jgi_user_name ) = $cur->fetchrow_array();
    $cur->finish();

    if ( $jgi_user_id || $jgi_user_name ) {
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>"
          . "<font color='darkblue'>"
          . "JGI Account Info"
          . "</font></th>\n";
        print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n";
        print "</tr>\n";
        if ($jgi_user_id) {
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>" . "JGI User ID" . "</th>\n";
            print "  <td class='img'   align='left'>" . escapeHTML($jgi_user_id) . "</td></tr>\n";
        }
        if ($jgi_user_name) {
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>" . "JGI User Name" . "</th>\n";
            print "  <td class='img'   align='left'>" . escapeHTML($jgi_user_name) . "</td></tr>\n";
        }
    }

    print "</table>\n";

    ListContactActivities( $selected_contact_oid, 'IMG ER' );
    ListContactActivities( $selected_contact_oid, 'IMG/M ER' );

    ListContactOmicsActivities($selected_contact_oid);

    ## ListContactProjectActivities($selected_contact_oid);

    print "<p>\n";

    #    print '<input type="submit" name="_section_UserTool:viewImgContacts" value="OK" class="smbutton" />';

    printHomeLink();

    print end_form();
}

##########################################################################
# UpdateContactInfo
##########################################################################
sub UpdateContactInfo {

    print start_form( -name => 'updateContactInfo', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();

    my $selected_contact_oid = param1('selected_contact_oid');
    if ( !$selected_contact_oid ) {
        printError("No IMG contact has been selected.");
        print end_form();
        return;
    }

    print "<h2>Update IMG Contact Information (OID: $selected_contact_oid)</h2>\n";
    print hidden( 'selected_contact_oid', $selected_contact_oid );

    my $def_con = def_Contact();
    my @attrs   = @{ $def_con->{attrs} };
    my %db_vals = SelectContact($selected_contact_oid);

    # show contact info
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    my $tab = "";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $attr_val  = "";
        if ( $db_vals{$attr_name} ) {
            $attr_val = $db_vals{$attr_name};
        }

        # show tab?
        if ( $tab ne $k->{tab} ) {
            $tab = $k->{tab};
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right' bgcolor='lightblue'>"
              . "<font color='darkblue'>"
              . $tab
              . "</font></th>\n";
            print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n";
            print "</tr>\n";
        }

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . $k->{display_name} . "</th>\n";
        print "  <td class='img'   align='left'>";

        my $data_type = $k->{data_type};
        my $len       = $k->{length};

        if ( !$k->{can_edit} ) {

            # display only
            print escapeHTML($attr_val);
        } elsif ( $data_type eq 'cv' || $data_type eq 'list' ) {

            # selection
            my @selects = ();

            if ( $data_type eq 'cv' ) {
                @selects = db_getValues( $k->{cv_query} );
            } elsif ( $data_type eq 'list' ) {
                @selects = split( /\|/, $k->{list_values} );
            }
            print "<select name='$attr_name' class='img' size='1'>\n";

            if ( !$k->{is_required} ) {
                print "   <option value=''> </option>\n";
            }
            for my $s0 (@selects) {
                print "    <option value='" . escapeHTML($s0) . "'";
                if ( $s0 eq $attr_val ) {
                    print " selected ";
                }
                print ">$s0</option>\n";
            }
            print "</select>\n";
        } elsif ( $data_type eq 'cv2' ) {
            print "<select name='$attr_name' class='img' size='1'>\n";
            if ( !$k->{is_required} ) {
                print "   <option value=''> </option>\n";
            }

            my $sql2 = $k->{cv_query};
            my $dbh2 = Connect_IMG_Contact();
            webLog("$sql2\n");
            my $cur2 = $dbh2->prepare($sql2);
            $cur2->execute();

            for ( my $j2 = 0 ; $j2 <= 100000 ; $j2++ ) {
                my ( $id2, $name2 ) = $cur2->fetchrow_array();
                if ( !$id2 && !$name2 ) {
                    last;
                }

                print "    <option value='" . escapeHTML($id2) . "'";
                if ( length($attr_val) > 0 && $attr_val eq $id2 ) {
                    print " selected ";
                }

                print ">$name2 (ID: $id2)</option>\n";
            }
            print "</select>\n";
            $cur2->finish();
            $dbh2->disconnect();
        } else {
            my $size = 80;
            if ( $len && $size > $len ) {
                $size = $len;
            }

            print "<input type='text' name='$attr_name' value='";
            if ( $attr_val && !blankStr($attr_val) ) {
                print escapeHTML($attr_val);
            }
            print "' size='$size' maxLength='$len'/>";
        }

        print "</td></tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbUpdateImgContactInfo" value="Update" class="smdefbutton" />';

    printHomeLink();

    print end_form();
}

####################################################################
# select Contact data given an ID
####################################################################
sub SelectContact {
    my ($c_oid) = @_;

    my %db_val;

    my $dbh = Connect_IMG_Contact();

    my $db_id = $c_oid;

    my $def_con = def_Contact();
    my @attrs   = @{ $def_con->{attrs} };

    my $sql = "";
    for my $k (@attrs) {
        my $attr_name = $k->{name};

        $attr_name = 'c.' . $attr_name;

        if ( blankStr($sql) ) {
            $sql = "select " . $attr_name;
        } else {
            $sql .= ", " . $attr_name;
        }
    }

    $sql .= " from contact c where c.contact_oid = $db_id";

    # print "<p>SQL: $sql</p>\n";
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my @flds = $cur->fetchrow_array();

    # save result
    my $j = 0;
    for my $k (@attrs) {
        my $attr_name = $k->{name};
        if ( scalar(@flds) < $j ) {
            last;
        }

        $db_val{$attr_name} = $flds[$j];
        $j++;
    }

    # finish
    $cur->finish();
    $dbh->disconnect();

    return %db_val;
}

##########################################################################
# dbUpdateImgContactInfo -- update Contact information in IMG/ER
##########################################################################
sub dbUpdateImgContactInfo {
    my $contact_oid = getContactOid();
    if ( !canUpdateContact($contact_oid) ) {
        return;
    }

    my $msg                  = "";
    my $selected_contact_oid = param1('selected_contact_oid');
    if ( !$selected_contact_oid ) {
        $msg = "No IMG contact has been selected.";
        return $msg;
    }

    # check input
    my $def_con = def_Contact();
    my @attrs   = @{ $def_con->{attrs} };
    for my $k (@attrs) {
        if ( !$k->{can_edit} ) {
            next;
        }

        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};
        my $v         = param1($attr_name);
        if ( $k->{is_required} && blankStr($v) ) {
            $msg = $k->{display_name} . " cannot be null.";
            return $msg;
        }

        if ( blankStr($v) ) {
            next;
        }

        if ( $data_type eq 'int' ) {
            if ( !isInt($v) ) {
                $msg = $k->{display_name} . " must be an integer.";
                return $msg;
            }
        }
    }

    # database update
    my %db_vals = SelectContact($selected_contact_oid);
    my $sql     = "";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $data_type = $k->{data_type};

        if ( !$k->{can_edit} ) {
            next;
        }

        my $val = param1($attr_name);
        if ( $db_vals{$attr_name} ) {

            # old value is not null
            if ( $val eq $db_vals{$attr_name} ) {
                next;
            }
        } else {

            # old value is null
            if ( blankStr($val) ) {
                next;
            }
        }

        # update
        if ( length($sql) == 0 ) {
            $sql = "update contact set $attr_name = ";
        } else {
            $sql .= ", $attr_name = ";
        }

        if ( blankStr($val) ) {
            $sql .= "null";
        } elsif ( $data_type eq 'int' || $attr_name eq 'img_group' ) {
            $sql .= $val;
        } else {
            $val =~ s/'/''/g;    # replace ' with ''
            $sql .= "'" . $val . "'";
        }
    }

    if ( length($sql) == 0 ) {

        # no need to update
        return $msg;
    }

    $sql .= " where contact_oid = $selected_contact_oid";

    my @sqlList = ($sql);

    db_con_sqlTrans( \@sqlList );

    return $msg;

}

##########################################################################
# DeleteContact
##########################################################################
sub DeleteContact {

    print start_form( -name => 'deleteContact', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();

    my $selected_contact_oid = param1('selected_contact_oid');
    if ( !$selected_contact_oid ) {
        printError("No IMG contact has been selected.");
        print end_form();
        return;
    }

    print "<h2>Delete IMG Contact (OID: $selected_contact_oid)</h2>\n";
    print hidden( 'selected_contact_oid', $selected_contact_oid );

    print "<h5>Are you sure that you want to delete this IMG contact?</h5>\n";

    my $def_con = def_Contact();
    my @attrs   = @{ $def_con->{attrs} };
    my %db_vals = SelectContact($selected_contact_oid);

    # show contact info
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    my $tab = "";

    for my $k (@attrs) {
        my $attr_name = $k->{name};
        my $attr_val  = "";
        if ( $db_vals{$attr_name} ) {
            $attr_val = $db_vals{$attr_name};
        }

        # skip null
        if ( blankStr($attr_val) ) {
            next;
        }

        # show tab?
        if ( $tab ne $k->{tab} ) {
            $tab = $k->{tab};
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right' bgcolor='lightblue'>"
              . "<font color='darkblue'>"
              . $tab
              . "</font></th>\n";
            print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n";
            print "</tr>\n";
        }

        if ( $k->{name} eq 'img_group' ) {
            my $g_name = db_getContactName($attr_val);
            $attr_val .= " - " . $g_name;
        }

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . $k->{display_name} . "</th>\n";
        print "  <td class='img'   align='left'>" . escapeHTML($attr_val) . "</td></tr>\n";
    }

    ## JGI account?
    my $dbh = WebFunctions::Connect_IMG();
    my $sql = qq{
	select cja.jgi_user_id, cja.jgi_user_name
	    from contact_jgi_accounts cja
	    where cja.contact_oid = $selected_contact_oid
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ( $jgi_user_id, $jgi_user_name ) = $cur->fetchrow_array();
    $cur->finish();

    if ( $jgi_user_id || $jgi_user_name ) {
        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right' bgcolor='lightblue'>"
          . "<font color='darkblue'>"
          . "JGI Account Info"
          . "</font></th>\n";
        print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n";
        print "</tr>\n";
        if ($jgi_user_id) {
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>" . "JGI User ID" . "</th>\n";
            print "  <td class='img'   align='left'>" . escapeHTML($jgi_user_id) . "</td></tr>\n";
        }
        if ($jgi_user_name) {
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>" . "JGI User Name" . "</th>\n";
            print "  <td class='img'   align='left'>" . escapeHTML($jgi_user_name) . "</td></tr>\n";
        }
    }

    print "</table>\n";

    my $er_act  = ListContactActivities( $selected_contact_oid, 'IMG ER' );
    my $mer_act = ListContactActivities( $selected_contact_oid, 'IMG/M ER' );

    my $proj_act = ListContactProjectActivities($selected_contact_oid);

    print "<p>\n";
    if ( $er_act || $mer_act || $proj_act ) {
        print "<h5>You cannot delete this IMG contact because there are IMG activities associated with this contact.</h5>\n";
    } else {
        print "<p><input type='checkbox' name='cancelled_user' value='1'>";
        print "Add User Email to Cancelled User List?<br/>\n";

        print '<input type="submit" name="_section_UserTool:dbDeleteImgContact" value="Delete" class="smdefbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:viewImgContacts" value="Cancel" class="smbutton" />';
    }

    print "<p>\n";

    printHomeLink();

    print end_form();
}

##########################################################################
# dbDeleteImgContact -- delete Contact information
##########################################################################
sub dbDeleteImgContact {
    my $contact_oid = getContactOid();
    if ( !canUpdateContact($contact_oid) ) {
        return;
    }

    my $msg                  = "";
    my $selected_contact_oid = param1('selected_contact_oid');
    if ( !$selected_contact_oid ) {
        $msg = "No IMG contact has been selected.";
        return $msg;
    }

    my $sql     = "delete from submission_img_contacts where img_contacts = $selected_contact_oid";
    my @sqlList = ($sql);
    $sql = "delete from contact_project_permissions where contact_oid = $selected_contact_oid";
    push @sqlList, ($sql);
    $sql = "delete from contact_img_groups where contact_oid = $selected_contact_oid";
    push @sqlList, ($sql);
    $sql = "delete from contact where contact_oid = $selected_contact_oid";
    push @sqlList, ($sql);

    my $cancelled_user = param1('cancelled_user');

    if ($cancelled_user) {
        my $dbh      = WebFunctions::Connect_IMG();
        my $user_sql = "select username, email from contact where contact_oid = ?";
        my $cur      = $dbh->prepare($user_sql);
        $cur->execute($selected_contact_oid);
        my ( $username, $email ) = $cur->fetchrow();
        $cur->finish();
        $dbh->disconnect();

        $username =~ s/'/''/g;    # replace ' with ''
        $email    =~ s/'/''/g;    # replace ' with ''

        $sql =
            "insert into cancelled_user (username, email, modified_by, mod_date) "
          . "values ('"
          . $username . "', '"
          . $email
          . "', $contact_oid, sysdate)";
        push @sqlList, ($sql);
    }

    #    for $sql ( @sqlList ) {
    #	$msg .= " SQL: $sql;";
    #    }
    db_con_sqlTrans( \@sqlList );

    return $msg;

}

##########################################################################
# ListContactActivities
##########################################################################
sub ListContactActivities {
    my ( $selected_contact_oid, $database ) = @_;

    print "<h3>Contact Activities in $database</h3>\n";
    my $has_act = 0;

    my $dbh = WebFunctions::Connect_IMG();
    print "<h4>Genome Submissions</h4>\n";

    my $sql = qq{
	select s.submission_id, s.project_info, 
        s.analysis_project_id, p.display_name, a.analysis_project_name,
	s.submission_date, v.cv_term, s.img_taxon_oid
            from submission s, submission_statuscv v, 
                 project_info p, gold_analysis_project a
            where s.database = '$database'
	    and s.contact = $selected_contact_oid
	    and s.project_info = p.project_oid (+)
            and s.status = v.term_oid (+) 
            and s.analysis_project_id = a.gold_id (+)
	    order by 1
	};

    my $cnt = 0;
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $submit_id, $project_oid, $ap_id, $project_name, $ap_name, $submit_date, $status, $img_taxon_oid ) =
          $cur->fetchrow_array();
        if ( !$submit_id ) {
            last;
        }

        $cnt++;
        if ( $cnt == 1 ) {
            print "<table class='img' border='1'>\n";
            print "<th class='img'>Submission ID</th>\n";
            print "<th class='img'>Project OID</th>\n";
            print "<th class='img'>Analysis Project ID</th>\n";
            print "<th class='img'>Project Name</th>\n";
            print "<th class='img'>Submission Date</th>\n";
            print "<th class='img'>Submission Status</th>\n";
            print "<th class='img'>IMG Taxon OID</th>\n";
            print "<th class='img'>IMG Contacts</th>\n";
        }

        # show data
        print "<tr class='img'>\n";
        PrintAttribute($submit_id);
        PrintAttribute($project_oid);

        my $link =
            "<a href='"
          . $main_cgi
          . "?section=ProjectInfo&page=analysisProject"
          . "&analysis_project_id=$ap_id' >"
          . $ap_id . "</a>";
        PrintAttribute($link);

        if ($ap_name) {
            $project_name = $ap_name;
        }
        PrintAttribute($project_name);
        PrintAttribute($submit_date);
        PrintAttribute($status);
        PrintAttribute($img_taxon_oid);

        # IMG contacts
        my $dbh2 = WebFunctions::Connect_IMG();
        my $sql2 = qq{ 
	    select c.contact_oid, c.username 
		from contact c, submission_img_contacts sic 
		where sic.submission_id = $submit_id
		and sic.img_contacts = c.contact_oid 
	    };
        webLog("$sql2\n");
        my $cur2 = $dbh2->prepare($sql2);
        $cur2->execute();
        my $cnt2      = 0;
        my $con_names = "";

        for ( ; ; ) {
            my ( $cid, $cname ) = $cur2->fetchrow_array();
            if ( !$cid ) {
                last;
            }

            # $cname = getContactLink($cid, $cname);
            $cnt2++;
            if ( $cnt2 > 10000 ) {
                last;
            }

            if ( length($con_names) == 0 ) {
                $con_names = $cname;
            } else {
                $con_names .= ", " . $cname;
            }
        }
        $cur2->finish();
        $dbh2->disconnect();
        PrintAttribute($con_names);

        print "</tr>\n";
    }    # end for loop
    $cur->finish();

    if ( $cnt > 0 ) {
        print "</table>\n";
        $has_act = 1;
    } else {
        print "<p>No genome submissions to this database.</p>\n";
    }
    $dbh->disconnect();

    if ( $database eq 'IMG ER' ) {
        $dbh = Connect_IMG_ER();
    } elsif ( $database eq 'IMG/M ER' ) {
        $dbh = Connect_IMG_MI();
    } else {
        return;
    }

    my $genome_type_cond = "";
    if ( $database eq 'IMG ER' ) {
        print "<h4>Private Isolate Genomes</h4>\n";
        $genome_type_cond = " and t.genome_type = 'isolate' ";
    } else {
        print "<h4>Private metagenomes</h4>\n";
        $genome_type_cond = " and t.genome_type = 'metagenome' ";
    }

    $sql = qq{
	select distinct t.taxon_oid, t.taxon_display_name, 
               t.add_date, t.is_public, t.obsolete_flag,
               t.analysis_project_id
	    from taxon t, contact_taxon_permissions c
	    where c.contact_oid = $selected_contact_oid
	    and c.taxon_permissions = t.taxon_oid
            $genome_type_cond
	    order by t.taxon_oid
	};
    $cnt = 0;
    webLog("$sql\n");

    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $taxon_oid, $taxon_name, $add_date, $is_public, $obsolete_flag, $analysis_project_id ) = $cur->fetchrow_array();
        if ( !$taxon_oid ) {
            last;
        }

        $cnt++;
        if ( $cnt == 1 ) {
            print "<table class='img' border='1'>\n";
            print "<th class='img'>Taxon OID</th>\n";
            print "<th class='img'>Taxon Name</th>\n";
            print "<th class='img'>GOLD AP</th>\n";
            print "<th class='img'>Add Date</th>\n";
            print "<th class='img'>Is Public?</th>\n";
            print "<th class='img'>Obsolete?</th>\n";
        }

        # show data
        print "<tr class='img'>\n";
        PrintAttribute($taxon_oid);
        PrintAttribute($taxon_name);

        if ($analysis_project_id) {
            my $link =
                "<a href='"
              . $main_cgi
              . "?section=ProjectInfo&page=analysisProject"
              . "&analysis_project_id=$analysis_project_id' >"
              . $analysis_project_id . "</a>";
            PrintAttribute($link);
        } else {
            PrintAttribute("-");
        }

        PrintAttribute($add_date);
        if ( lc($is_public) eq 'yes' ) {
            PrintAttribute("Public");
        } else {
            PrintAttribute("Private");
        }
        PrintAttribute($obsolete_flag);
        print "</tr>\n";
    }
    $cur->finish();

    if ( $cnt > 0 ) {
        print "</table>\n";
        $has_act = 1;
    } else {
        print "<p>No private genomes in this database.</p>\n";
    }

    print "<h4>MyIMG Annotations</h4>\n";

    $sql = qq{
	select t.taxon_oid, t.taxon_name, max(ann.mod_date)
	    from taxon t, gene g, gene_myimg_functions ann
	    where ann.modified_by = $selected_contact_oid
	    and ann.gene_oid = g.gene_oid
	    and g.taxon = t.taxon_oid
            $genome_type_cond
	    group by t.taxon_oid, t.taxon_name
	    order by t.taxon_oid
	};
    $cnt = 0;
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $taxon_oid, $taxon_name, $mod_date ) = $cur->fetchrow_array();
        if ( !$taxon_oid ) {
            last;
        }

        $cnt++;
        if ( $cnt == 1 ) {
            print "<table class='img' border='1'>\n";
            print "<th class='img'>Taxon OID</th>\n";
            print "<th class='img'>Taxon Name</th>\n";
            print "<th class='img'>Last Mod Date</th>\n";
        }

        # show data
        print "<tr class='img'>\n";
        PrintAttribute($taxon_oid);
        PrintAttribute($taxon_name);
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    $cur->finish();

    if ( $cnt > 0 ) {
        print "</table>\n";
        $has_act = 1;
    } else {
        print "<p>No MyIMG annotations in this database.</p>\n";
    }

    $dbh->disconnect();

    return $has_act;
}

##########################################################################
# ListContactOmicsActivities
##########################################################################
sub ListContactOmicsActivities {
    my ($selected_contact_oid) = @_;

    print "<h3>Contact Activities in OMICS</h3>\n";
    my $has_act = 0;

    my $dbh = WebFunctions::Connect_IMG_MI();
    print "<h4>Private OMICS Dataset</h4>\n";

    my $sql = qq{
	select distinct r.dataset_oid, r.gold_id, r.analysis_project_id,
            r.add_date, r.is_public, r.obsolete_flag,
            ap.analysis_project_name
	    from rnaseq_dataset r, contact_rna_data_permissions c,
                 gold_analysis_project ap
	    where c.contact_oid = $selected_contact_oid
	    and c.dataset_oid = r.dataset_oid
            and r.analysis_project_id = ap.gold_id (+)
	    order by r.dataset_oid
	};
    my $cnt = 0;
    webLog("$sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $dataset_oid, $gold_sp_id, $analysis_project_id, $add_date, $is_public, $obsolete_flag, $ap_name ) =
          $cur->fetchrow_array();
        if ( !$dataset_oid ) {
            last;
        }

        $cnt++;
        if ( $cnt == 1 ) {
            print "<table class='img' border='1'>\n";
            print "<th class='img'>Dataset OID</th>\n";
            print "<th class='img'>GOLD SP</th>\n";
            print "<th class='img'>GOLD AP</th>\n";
            print "<th class='img'>Analysis Project Name</th>\n";
            print "<th class='img'>Add Date</th>\n";
            print "<th class='img'>Is Public?</th>\n";
            print "<th class='img'>Obsolete?</th>\n";
        }

        # show data
        print "<tr class='img'>\n";
        PrintAttribute($dataset_oid);

        if ($gold_sp_id) {
            my $link =
                "<a href='"
              . $main_cgi
              . "?section=ProjectInfo&page=sequencingProject"
              . "&sequencing_project_id=$gold_sp_id' >"
              . $gold_sp_id . "</a>";
            PrintAttribute($link);
        } else {
            PrintAttribute("-");
        }

        if ($analysis_project_id) {
            my $link =
                "<a href='"
              . $main_cgi
              . "?section=ProjectInfo&page=analysisProject"
              . "&analysis_project_id=$analysis_project_id' >"
              . $analysis_project_id . "</a>";
            PrintAttribute($link);
            PrintAttribute($ap_name);
        } else {
            PrintAttribute("-");
            PrintAttribute("-");
        }

        PrintAttribute($add_date);
        if ( lc($is_public) eq 'yes' ) {
            PrintAttribute("Public");
        } else {
            PrintAttribute("Private");
        }
        PrintAttribute($obsolete_flag);
        print "</tr>\n";
    }
    $cur->finish();

    if ( $cnt > 0 ) {
        print "</table>\n";
        $has_act = 1;
    } else {
        print "<p>No private OMICS datasets in IMG.</p>\n";
    }

    $dbh->disconnect();

    return $has_act;
}

##########################################################################
# ListContactProjectActivities
#
# IMG-GOLD projects by this contact
##########################################################################
sub ListContactProjectActivities {
    my ($selected_contact_oid) = @_;

    my $has_act = 0;

    print "<h3>IMG-GOLD Projects owned by Contact</h3>\n";

    my $dbh = WebFunctions::Connect_IMG();
    print "<h4>IMG-GOLD Projects</h4>\n";

    my $sql = qq{
	select p.project_oid, p.display_name, p.gold_stamp_id,
	p.domain, p.ncbi_project_id, p.add_date, p.mod_date
            from project_info p
            where p.contact_oid = $selected_contact_oid
	    order by 1
	};

    my $cnt = 0;
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $project_oid, $project_name, $gold_stamp_id, $domain, $ncbi_project_id, $add_date, $mod_date ) =
          $cur->fetchrow_array();
        if ( !$project_oid ) {
            last;
        }

        $cnt++;
        if ( $cnt == 1 ) {
            print "<table class='img' border='1'>\n";
            print "<th class='img'>Project OID</th>\n";
            print "<th class='img'>Project Name</th>\n";
            print "<th class='img'>GOLD ID</th>\n";
            print "<th class='img'>Domain</th>\n";
            print "<th class='img'>NCBI Project ID</th>\n";
            print "<th class='img'>Add Date</th>\n";
            print "<th class='img'>Last Mod Date</th>\n";
        }

        # show data
        print "<tr class='img'>\n";

        my $proj_link = getProjectLink($project_oid);
        PrintAttribute($proj_link);

        PrintAttribute($project_name);

        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);

        PrintAttribute($domain);

        if ($ncbi_project_id) {
            PrintAttribute( getNcbiProjLink($ncbi_project_id) );
        } else {
            PrintAttribute($ncbi_project_id);
        }

        PrintAttribute($add_date);
        PrintAttribute($mod_date);

        print "</tr>\n";
    }    # end for loop
    $cur->finish();

    if ( $cnt > 0 ) {
        print "</table>\n";
        $has_act = 1;
    } else {
        print "<p>No IMG-GOLD projects owned by this contact.</p>\n";
    }
    $dbh->disconnect();

    return $has_act;
}

#############################################################################
# TestFunctions - test
#############################################################################
sub TestFunctions {

    #    open (OUT, '>', "/home/img2/imachen/submission/amy_test/test.txt") or return;
    #    print OUT "test\n";
    #    close OUT;

    my $test_submit_id = param1('test_submit_id');
    print "<h2>Submission ID: $test_submit_id</h2>\n";
    if ( !$test_submit_id || !isInt($test_submit_id) ) {
        print "<p>Incorrect Submission ID: $test_submit_id\n";
        return;
    }

    my $project_oid = db_getValue("select project_info from submission where submission_id = $test_submit_id");
    use Submission;
    if ( $project_oid && Submission::isJgiProject($project_oid) ) {
        print "<p>JGI Project\n";
    } else {
        print "<p>Non-JGI Project\n";
        return;
    }

    my $database = db_getValue("select database from submission where submission_id = $test_submit_id");

    my $dbh = WebFunctions::Connect_IMG();
    my $sql = qq{
	select p.display_name, p.project_type, p.funding_program,
	p.contact_name, p.contact_email
            from project_info p
            where p.project_oid = $project_oid
	};

    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my ( $proj_name, $proj_type, $funding_program, $pi_name, $pi_email ) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( $database eq 'IMG ER' || lc($proj_type) =~ /single cell/ ) {

        # ER or single cell
    } else {
        return;
    }

    my $email_from = "Tanja Woyke <TWoyke\@lbl.gov>";

    my $email_to       = "";
    my $sub_cont       = db_getValue("select contact from submission where submission_id = $test_submit_id");
    my $sub_cont_email = db_getContactEmail($sub_cont);
    if ( isEmail($sub_cont_email) ) {
        $email_to = $sub_cont_email;
    }

    # IMG contacts
    my @img_contacts =
      db_getValues("select s2.img_contacts from submission_img_contacts s2 where s2.submission_id = $test_submit_id");

    # add Lynne, Sam and Nikos if they are not there
    for my $id2 ( 11, 333, 100440 ) {
        if ( !inIntArray( $id2, @img_contacts ) ) {
            push @img_contacts, ($id2);
        }
    }

    for my $proj_cont (@img_contacts) {
        my $proj_cont_email = db_getContactEmail($proj_cont);
        if ( isEmail($proj_cont_email) ) {
            if ( length($email_to) == 0 ) {
                $email_to = $proj_cont_email;
            } else {
                $email_to .= ", " . $proj_cont_email;
            }
        }
    }

    # add Lin Peters, who does not have an IMG account
    $email_to .= ", lgpeters\@lbl.gov";

    # add PI
    if ( isEmail($pi_email) ) {
        $email_to = $pi_email . ", " . $email_to;
    }
    if ($pi_name) {
        if ( $pi_name =~ /^Dr/ ) {

            # already have title
        } else {
            $pi_name = "Dr. " . $pi_name;
        }
    } else {
        $pi_name = "IMG User";
    }

    # subject
    my $email_subj = "Your IMG Submission $test_submit_id";

    print "<p>From: $email_from\n";
    print "<p>To: $email_to\n";
    print "<p>Subject: $email_subj\n";

    my $img_url = "https://img.jgi.doe.gov/er";
    if ( $database eq 'IMG/M ER' ) {
        $img_url = "https://img.jgi.doe.gov/mer";
    }

    my $embargo_policy = "";

    my $today = db_getValue("select to_char(sysdate, 'mm/dd/yy') from dual");
    if ( lc($proj_type) =~ /single cell/ ) {
        my $end_date = db_getValue("select to_char(sysdate+90, 'mm/dd/yy') from dual");
        $embargo_policy =
"The annotation is now available to you exclusively for a period of three (3) months (90 days) from $today (\"review period\").";
    } elsif (    $funding_program =~ /DOE\-GLBRC/
              || $funding_program =~ /DOE\-JBEI/
              || $funding_program =~ /DOE\-BESC/ )
    {
        my $end_date = db_getValue("select sysdate+180 from dual");
        $embargo_policy =
"The annotation is now available to you exclusively for a period of six (6) months (180 days) from $today to $end_date (\"embargo period\").";
    }

    my $msg = qq{
Dear $pi_name,

The $proj_name genome
has been annotated using the JGI annotation pipeline [1],
and is now available in the $database comparative analysis system [2]
($img_url).

$database provides numerous capabilities to you for genome analysis. To highlight a few, you may:

* Access your genome in $database using your login/password provided when you registered to $database.
* Analyze and revise the functional annotation of your genome using $database capabilities.
* Download your annotated genome sequence via $database download page.
* Revise your genome annotation in $database prior to submission to Genbank.

The annotated genome will be made available to the public through submission to Genbank.
Please review, curate and confirm the genus/species/strain information of your project
as well as all associated metadata at the GOLD database (http://img.jgi.doe.gov/gold).
You can login into the GOLD database with the same account you have for the $database.
For any questions on how to curate the metadata of your genome please contact
Dino Liolios (KLiolios\@lbl.gov).

In addition, please send to Sam Pitluck (S_Pitluck\@lbl.gov) the list of authors
to be included in the Genbank record.

$embargo_policy

Please let us know if you have any questions.

Regards,

Tanja Woyke (US DOE JGI Microbial Genome Program Director)
Lynne Goodwin (US DOE JGI Microbial Genome Program Project Manager)

[1] http://www.ncbi.nlm.nih.gov/pubmed/21304638
[2] http://www.ncbi.nlm.nih.gov/pubmed/19561336
};

    print "<pre>$msg</pre>\n";
}

#########################################################################
# canUpdateContact
#########################################################################
sub canUpdateContact {
    my ($contact_oid) = @_;

    if (    $contact_oid == 312
         || $contact_oid == 11
         || $contact_oid == 19
         || $contact_oid == 3038
         || $contact_oid == 17 )
    {
        return 1;
    }

    return 0;
}

#########################################################################
# ShowFileExportImport
#########################################################################
sub ShowFileExportImport {
    my $contact_oid = getContactOid();
    my $isAdmin     = 'No';

    if ($contact_oid) {
        $isAdmin = getIsAdmin($contact_oid);
    }
    if ( $isAdmin ne 'Yes' ) {
        return;
    }

    print hr;

    print "<h2>File Export and Import</h2>\n";
    print "<p><font color='red'>Under Construction</font></p>\n";

    print "<p>Select IMG-GOLD projects to export to Excel files.</p>\n";

    print '<input type="submit" name="_section_UserTool:selectToExport" value="Export Projects" class="medbutton" />';

    if ( $contact_oid != 312 ) {
        return;
    }

    print "<p>Import changes to existing IMG-GOLD projects from tab-delimited files.</p>\n";

    print '<input type="submit" name="_section_UserTool:selectImportFile" value="Import Changes" class="medbutton" />';

}

##########################################################################
# ShowExportSelection
##########################################################################
sub ShowExportSelection {

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<h2>Select IMG-GOLD Projects to Export</h2>\n";

    # print java script
    print qq{ 
        <script> 
            function selectAllCheckBoxes( x ) { 
                var f = document.mainForm;
                for( var i = 0; i < f.length; i++ ) {
                    var e = f.elements[ i ];
                    if( e.type == "checkbox" ) { 
                        e.checked = ( x == 0 ? false : true );
                    }
                } 
            } 
        </script>
        };

    # buttons
    print "<p>\n";
    print
'<input type="submit" name="_section_UserTool:listExportProj_noHeader" value="Export Projects" class="meddefbutton" />';
    print nbsp(1);
    print "<input type='button' name='selectAll' value='Select All' "
      . "onClick='selectAllCheckBoxes(1)' class='smbutton' />\n";
    print nbsp(1);
    print "<input type='button' name='clearAll' value='Clear All' "
      . "onClick='selectAllCheckBoxes(0)' class='smbutton' />\n";

    print "<h4>Please specify project selection condition:</h4>\n";

    my $def_project = def_Project_Info();
    my @attrs       = @{ $def_project->{attrs} };

    my @tabs = ( 'Organism', 'Project', 'Sequencing', 'EnvMeta', 'HostMeta', 'OrganMeta', 'HMP' );

    # tab display label
    my %tab_display = (
                        'Organism'   => 'Organism Info',
                        'Metagenome' => 'Metagenome Info',
                        'Project'    => 'Project Info',
                        'Sequencing' => 'Sequencing Info',
                        'EnvMeta'    => 'Environment Metadata',
                        'HostMeta'   => 'Host Metadata',
                        'OrganMeta'  => 'Organism Metadata',
                        'HMP'        => 'HMP Metadata'
    );

    my @aux_tables = getProjectAuxTables();

    print "<table class='img' border='1'>\n";

    print "<tr class='img' >\n";
    print "  <th class='subhead' bgcolor='lightblue'>" . "<font color='darkblue'>Export?</font></th>\n";
    print "  <th class='subhead' bgcolor='lightblue'>" . "<font color='darkblue'>Name</font></th>\n";
    print "  <th class='subhead' colspan='2' bgcolor='lightblue'>" . "<font color='darkblue'>Export Condition</font></th>\n";
    print "</tr>\n";

    for my $tab (@tabs) {
        my $tab_label = $tab;
        if ( $tab_display{$tab} ) {
            $tab_label = $tab_display{$tab};
        }

        print "<tr class='img' >\n";
        print "  <th class='subhead' colspan='4' align='right' bgcolor='lightblue'>"
          . "<font color='darkblue'>"
          . $tab_label
          . "</font></th>\n";
        print "</tr>\n";

        for my $k (@attrs) {
            if ( $k->{tab} ne $tab ) {
                next;
            }

            # skip non-editable, non-selectable
            if ( !$k->{can_edit} && !$k->{filter_cond} ) {
                next;
            }

            my $attr_name = $k->{name};
            print "<tr class='img'>\n";
            print "<td class='img'>\n";

            #	    if ( $k->{can_edit} ) {
            print "<input type='checkbox' name='export_attr' " . "value='$attr_name'>";

            #	    }
            print "</td>\n";

            if ( !$k->{filter_cond} ) {
                print "<td class='img'>" . escapeHTML( $k->{display_name} ) . "</td>\n";
                print "</tr>\n";
                next;
            }

            # show filter condition
            my $filter_name   = "export_cond:" . $k->{name};
            my $ui_type       = 'text';
            my $select_type   = '';
            my $select_method = '';

            if ( $k->{data_type} eq 'cv' ) {
                $ui_type       = 'select';
                $select_type   = 'query';
                $select_method = $k->{cv_query};
            } elsif ( $k->{data_type} eq 'list' ) {
                $ui_type       = 'select';
                $select_type   = 'list';
                $select_method = $k->{list_values};
            } elsif (    $k->{data_type} eq 'int'
                      || $k->{data_type} eq 'number' )
            {
                $ui_type = 'number';
            } elsif ( $k->{data_type} eq 'date' ) {
                $ui_type = 'date';
            }
            printExportCondRow( $k->{display_name}, $filter_name, $ui_type, $select_type, $select_method,
                                getSessionParam($filter_name) );
            print "</tr>\n";
        }    # end for my k

        for my $k2 (@aux_tables) {
            my $def_aux = def_Class($k2);
            if ( !$def_aux ) {
                next;
            }
            if ( $def_aux->{tab} ne $tab ) {
                next;
            }

            my @aux_attrs = @{ $def_aux->{attrs} };

            my $set_attr_name = $def_aux->{name};
            print "<td class='img'>\n";
            print "<input type='checkbox' name='export_set_attr' " . "value='$set_attr_name'>" . "</td>\n";
            my $has_cond = 0;

            for my $k3 (@aux_attrs) {
                if ( !$k3->{filter_cond} ) {
                    next;
                }

                $has_cond = 1;
                my $filter_name = "export_cond:" . $k3->{name};
                printExportCondRow( $k3->{display_name}, $filter_name, 'select', 'query', $k3->{cv_query},
                                    getSessionParam($filter_name) );
            }

            if ( !$has_cond ) {
                print "<td class='img'>" . escapeHTML( $def_aux->{display_name} ) . "</td>\n";
            }
            print "</tr>\n";
        }    # end for my k2
    }    # end for my tab
    print "</table>\n";

    print "<p>\n";
    print
      '<input type="submit" name="_section_UserTool:listExportProj_noHeader" value="Export Projects" class="medbutton" />';
    print nbsp(1);
    print "<input type='button' name='selectAll' value='Select All' "
      . "onClick='selectAllCheckBoxes(1)' class='smbutton' />\n";
    print nbsp(1);
    print "<input type='button' name='clearAll' value='Clear All' "
      . "onClick='selectAllCheckBoxes(0)' class='smbutton' />\n";

    print "<p>\n";
    printHomeLink();

    print end_form();

    return;

    # NCBI project ID
    ProjectInfo::printFilterCond( 'NCBI Project ID', 'export_cond:ncbi_project_id', 'number', '', '', '' );

    # NCBI project name
    ProjectInfo::printFilterCond( 'NCBI Project Name', 'export_cond:ncbi_project_name', 'text', '', '', '' );

    # project type
    ProjectInfo::printFilterCond( 'Project Type', 'export_cond:project_type', 'select', 'query',
                                  "select cv_term from project_typecv order by cv_term", '' );

    # project status
    ProjectInfo::printFilterCond( 'Project Status',
              'export_cond:project_status', 'select', 'query', 'select cv_term from project_statuscv order by cv_term', '' );

    # domain
    my $domain_str = 'ARCHAEAL|BACTERIAL|EUKARYAL|MICROBIAL|PLASMID|VIRUS';
    ProjectInfo::printFilterCond( 'Domain', 'export_cond:domain', 'select', 'list', $domain_str, '' );

    # sequencing status
    ProjectInfo::printFilterCond( 'Sequencing Status',
                      'export_cond:seq_status', 'select', 'query', 'select cv_term from seq_statuscv order by cv_term', '' );

    # availability
    ProjectInfo::printFilterCond( 'Availability', 'export_cond:availability', 'select', 'list', 'Proprietary|Public', '' );

    # phylogeny
    ProjectInfo::printFilterCond( 'Phylogeny', 'export_cond:phylogeny', 'select', 'query',
                                  'select cv_term from phylogenycv order by cv_term', '' );

    # project display name
    ProjectInfo::printFilterCond( 'Project Display Name', 'export_cond:display_name', 'text', '', '', '' );

    # genus
    ProjectInfo::printFilterCond( 'Genus', 'export_cond:genus', 'text', '', '', '' );

    # species
    ProjectInfo::printFilterCond( 'Species', 'export_cond:species', 'text', '', '', '' );

    # common_name
    ProjectInfo::printFilterCond( 'Common Name', 'export_cond:common_name', 'text', '', '', '' );

    # relevance
    ProjectInfo::printFilterCond( 'Project Relevance',
                                  'export_cond:project_relevance',
                                  'select', 'query', 'select cv_term from relevancecv order by cv_term', '' );

    # add date
    ProjectInfo::printFilterCond( 'Add Date', 'export_cond:add_date', 'date', '', '', '' );

    # mod date
    ProjectInfo::printFilterCond( 'Last Mod Date', 'export_cond:mod_date', 'date', '', '', '' );

    print
"<h4>Please select attributes to be included in the export. Note that Project OID is always included in the output.</h4>\n";

    #    my $def_project = def_Project_Info();
    #    my @attrs = @{$def_project->{attrs}};

    my $ck = ' ';
    for my $tab ( 'Organism', 'Project', 'Links', 'Metadata' ) {
        print "<h3>$tab</h3>\n";

        for my $k (@attrs) {

            # only show attributes of this tab
            if ( $k->{tab} ne $tab ) {
                next;
            }

            # skip non-editable
            if ( !$k->{can_edit} ) {
                next;
            }

            # show attribute for selection
            my $attr_name = $k->{name};
            my $disp_name = $k->{display_name};

            print "<input type='checkbox' name='export_attr' "
              . "value='$attr_name' $ck>"
              . escapeHTML($disp_name)
              . "<br/>\n";
        }    # end for k

        # set-valued fields
        if ( $tab eq 'Organism' ) {

            # no set-valued
        } elsif ( $tab eq 'Project' ) {

            # seq_method
            my $def_s = def_project_info_seq_method();
            if ($def_s) {
                my $attr_name = $def_s->{name};
                my $disp_name = $def_s->{display_name};
                print "<input type='checkbox' name='export_set_attr' "
                  . "value='$attr_name' $ck>"
                  . escapeHTML($disp_name)
                  . "<br/>\n";
            }
        } elsif ( $tab eq 'Links' ) {

            # data links
            my $def_s = def_project_info_data_links();
            if ($def_s) {
                my $attr_name = $def_s->{name};
                my $disp_name = $def_s->{display_name};
                print "<input type='checkbox' name='export_set_attr' "
                  . "value='$attr_name' $ck>"
                  . escapeHTML($disp_name)
                  . "<br/>\n";
            }
        } elsif ( $tab eq 'Metadata' ) {
            for my $aux_name (
                               'project_info_cell_arrangement', 'project_info_diseases',
                               'project_info_habitat',          'project_info_metabolism',
                               'project_info_phenotypes',       'project_info_project_relevance',
                               'project_info_energy_source'
              )
            {
                my $def_s = def_Class($aux_name);
                if ($def_s) {
                    my $attr_name = $def_s->{name};
                    my $disp_name = $def_s->{display_name};
                    print "<input type='checkbox' name='export_set_attr' "
                      . "value='$attr_name' $ck>"
                      . escapeHTML($disp_name)
                      . "<br/>\n";
                }
            }
        }
    }    # end for tab

    print "<p>\n";
    print
      '<input type="submit" name="_section_UserTool:listExportProj_noHeader" value="Export Projects" class="medbutton" />';
    print nbsp(1);
    print "<input type='button' name='selectAll' value='Select All' "
      . "onClick='selectAllCheckBoxes(1)' class='smbutton' />\n";
    print nbsp(1);
    print "<input type='button' name='clearAll' value='Clear All' "
      . "onClick='selectAllCheckBoxes(0)' class='smbutton' />\n";

    print "<p>\n";
    printHomeLink();

    print end_form();
}

###########################################################################
# printExportCondRow - print export condition in a table row
###########################################################################
sub printExportCondRow {
    my ( $title, $param_name, $ui_type, $select_type, $select_method, $def_val_str ) = @_;

    my ( $tag, $attr_name ) = split( /\:/, $param_name );

    print "<td class='img'>" . escapeHTML($title) . "</td>\n";

    my @vals     = ();
    my @def_vals = ();
    if ( !blankStr($def_val_str) ) {
        @def_vals = split( /\,/, $def_val_str );
    }

    if ( $select_type eq 'query' ) {
        my $dbh = Connect_IMG();
        webLog("$select_method\n");
        my $cur = $dbh->prepare($select_method);
        $cur->execute();

        for ( my $j = 0 ; $j <= 100000 ; $j++ ) {
            my ( $v2, $cv_term ) = $cur->fetchrow_array();
            if ( !$v2 && !$cv_term ) {
                last;
            }

            my $v3 = $v2 . "|" . $cv_term;
            push @vals, ($v3);
        }
        $cur->finish();
        $dbh->disconnect();
    } elsif ( $select_type eq 'list' ) {
        @vals = split( /\|/, $select_method );
    }

    if ( $ui_type eq 'text' ) {

        # print "<td class='img'>match</td>\n";
        # print nbsp(3);
        my $op_name = $param_name . ":op";
        my $comp1   = getSessionParam($op_name);
        print "<td class='img'>";
        print "<select name='$op_name' class='img' size='1'>\n";
        print "   <option value='0'> </option>\n";
        for my $k ( 'match', 'is null', 'is not null' ) {
            my $k2 = escapeHTML($k);
            print "   <option value='$k2'";
            if ( !blankStr($comp1) && $k eq $comp1 ) {
                print " selected ";
            }
            print ">$k2</option>\n";
        }
        print "</select></td>\n";

        my $val2 = $def_val_str;
        print "<td class='img'>";
        print "<input type='text' name='$param_name' value='$val2'" . " size='60' maxLength='255'/>" . "</td>\n";
    } elsif ( $ui_type eq 'number' ) {

        # print nbsp(3);
        my $op_name = $param_name . ":op";
        my $comp1   = getSessionParam($op_name);
        print "<td class='img'>";
        print "<select name='$op_name' class='img' size='1'>\n";
        print "   <option value='0'> </option>\n";
        for my $k ( '=', '!=', '>', '>=', '<', '<=', 'is null', 'is not null' ) {
            my $k2 = escapeHTML($k);
            print "   <option value='$k2'";
            if ( !blankStr($comp1) && $k eq $comp1 ) {
                print " selected ";
            }
            print ">$k2</option>\n";
        }
        print "</select></td>\n";

        # print nbsp( 1 );

        my $val2 = $def_val_str;
        print "<td class='img'>";
        print "<input type='text' name='$param_name' value='$val2'" . " size='60' maxLength='80'/>" . "</td>\n";
    } elsif ( $ui_type eq 'date' ) {
        print nbsp(3);
        my $op_name = $param_name . ":op";
        my $comp1   = getSessionParam($op_name);
        print "<td class='img'>";
        print "<select name='$op_name' class='img' size='1'>\n";
        print "   <option value='0'> </option>\n";
        for my $k ( '=', '!=', '>', '>=', '<', '<=' ) {
            my $k2 = escapeHTML($k);
            print "   <option value='$k2'";
            if ( !blankStr($comp1) && $k eq $comp1 ) {
                print " selected ";
            }
            print ">$k2</option>\n";
        }
        print "</select></td>\n";
        print nbsp(1);

        my $val2 = $def_val_str;
        print "<td class='img'>";
        print "<input type='text' name='$param_name' value='$val2'" . " size='30' maxLength='30'/>\n";
        print nbsp(3);
        print "<b>(Use 'DD-MON-YY' for Date format -- e.g., 01-MAR-11)</b></td>\n";
    } elsif ( $ui_type eq 'checkbox' ) {
        print "<td class='img'>contain</td>\n";

        for my $s2 (@vals) {
            my ( $id2, $val2 ) = split( /\|/, $s2 );
            if ( blankStr($val2) ) {
                $val2 = $id2;
            }

            print "<br/>\n";
            print nbsp(3);
            print "<td class='img'>";
            print "<input type='checkbox' name='$param_name' value='$id2'";
            if ( scalar(@def_vals) == 0
                 || inArray( $id2, @def_vals ) )
            {
                print " checked ";
            }
            print ">" . escapeHTML($val2) . "\n";
            print "<td class='img'>";
        }
    } elsif ( $ui_type eq 'select' ) {
        print "<td class='img'>contain</td>\n";

        # print "<br/>\n";
        print nbsp(3);
        print "<td class='img'>";
        print "<select name='$param_name' class='img' size='1'>\n";
        print "    <option value='' > </option>\n";
        for my $s2 (@vals) {
            my ( $id2, $val2 ) = split( /\|/, $s2 );
            if ( blankStr($val2) ) {
                $val2 = $id2;
            }

            print "    <option value='" . escapeHTML($id2) . "'";
            if ( scalar(@def_vals) > 0
                 && inArray( $id2, @def_vals ) )
            {
                print " selected ";
            }
            print ">$val2</option>\n";
        }
        print "</select>\n";
        print "</td>\n";
    }

    #    print "</tr>\n";
}

##########################################################################
# ListExportProjects
##########################################################################
sub ListExportProjects {

    my $my_c_oid = getContactOid();

    my $sql              = "select p.project_oid";
    my @export_attrs     = param1('export_attr');
    my @export_set_attrs = param1('export_set_attr');
    for my $attr (@export_attrs) {
        if ( blankStr($attr) ) {
            next;
        }

        $sql .= ", p.$attr";
    }
    $sql .= " from project_info p";

    my $cond = "";

    #    my @cond_params = ( 'export_cond:project_type',
    #			'export_cond:project_status',
    #			'export_cond:domain',
    #			'export_cond:seq_status',
    #			'export_cond:seq_method',
    #			'export_cond:availability',
    #			'export_cond:phylogeny',
    #			'export_cond:display_name',
    #			'export_cond:genus',
    #			'export_cond:species',
    #			'export_cond:common_name',
    #			'export_cond:ncbi_project_id',
    #			'export_cond:ncbi_project_name',
    #			'export_cond:project_relevance',
    #			'export_cond:add_date',
    #			'export_cond:mod_date' );

    my @cond_params = getProjectAttrs( 'filter_cond', 'export_cond' );

    for my $p0 (@cond_params) {
        my $cond1 = "";
        my ( $tag, $fld_name ) = split( /\:/, $p0 );

        my $op_name = $p0 . ":op";
        my $op1     = "=";
        if ( param1($op_name) ) {
            $op1 = param1($op_name);
        }

        if ( $op1 eq 'is null' || $op1 eq 'is not null' ) {
            $cond1 = "p.$fld_name $op1";
        } elsif ( $op1 eq 'match' && defined param1($p0) ) {
            my $s1 = param1($p0);
            $s1 =~ s/'/''/g;    # replace ' with ''
            if ( length($s1) > 0 ) {
                $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
            }
        } elsif ( defined param1($p0) ) {
            my @vals = split( /\,/, param1($p0) );

            if (    lc($fld_name) eq 'display_name'
                 || lc($fld_name) eq 'genus'
                 || lc($fld_name) eq 'species'
                 || lc($fld_name) eq 'common_name'
                 || lc($fld_name) eq 'ncbi_project_name' )
            {
                my $s1 = param1($p0);
                $s1 =~ s/'/''/g;    # replace ' with ''
                if ( length($s1) > 0 ) {
                    $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
                }
            } elsif ( getAuxTableName($fld_name) ) {
                my $aux_table_name = getAuxTableName($fld_name);
                my $aux_def        = def_Class($aux_table_name);
                if ($aux_def) {
                    my $s1 = param1($p0);
                    if ( length($s1) > 0 ) {
                        $cond1 =
                            "p.project_oid in (select project_oid " . "from "
                          . $aux_table_name
                          . " where "
                          . lc($fld_name)
                          . " = '$s1')";
                    }
                }
            } elsif ( lc($fld_name) eq 'ncbi_project_id' ) {
                my $s1 = param1($p0);
                $s1 =~ s/'/''/g;    # replace ' with ''
                if ( length($s1) > 0 && isInt($s1) ) {
                    $cond1 = "p.$fld_name $op1 $s1";
                }
            } elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) {
                    $s1 =~ s/'/''/g;    # replace ' with ''
                    $cond1 =
                        "p.project_oid in (select pism.project_oid "
                      . "from project_info_seq_method pism "
                      . "where lower(pism.$fld_name) = '"
                      . lc($s1) . "')";
                }
            } elsif (    lc($fld_name) eq 'add_date'
                      || lc($fld_name) eq 'mod_date' )
            {
                my $op_name = $p0 . ":op";
                my $op1     = param1($op_name);
                my $d1      = param1($p0);
                if (    !blankStr($op1)
                     && !blankStr($d1)
                     && isDate($d1) )
                {
                    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
                }
            } else {
                for my $s2 (@vals) {
                    $s2 =~ s/'/''/g;    # replace ' with ''
                    if ( blankStr($cond1) ) {
                        $cond1 = "p.$fld_name in ('" . $s2 . "'";
                    } else {
                        $cond1 .= ", '" . $s2 . "'";
                    }
                }

                if ( !blankStr($cond1) ) {
                    $cond1 .= ")";
                }
            }
        }

        if ( !blankStr($cond1) ) {
            if ( blankStr($cond) ) {
                $cond = $cond1;
            } else {
                $cond .= " and " . $cond1;
            }
        }
    }

    if ( !blankStr($cond) ) {
        $sql .= " where " . $cond;
    }
    $sql .= " order by 1";

    # get all set attr values first
    my %set_val_h;
    for my $sattr (@export_set_attrs) {
        my %attr_val_h;
        getProjSetValues( $sattr, '|', ',', \%attr_val_h );
        $set_val_h{$sattr} = \%attr_val_h;
    }

    my $dbh = WebFunctions::Connect_IMG;
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    # print Excel Header
    my $fileName = "proj_export$$.xls";
    print "Content-type: application/vnd.ms-excel\n";
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n";

    #    print "SQL: $sql\n";

    # print header line
    my %headers;
    my $def_project = def_Project_Info();
    my @attrs       = @{ $def_project->{attrs} };
    for my $attr (@attrs) {
        my $name = $attr->{name};
        $headers{$name} = $attr->{display_name};
    }

    print $headers{'project_oid'};
    for my $attr_name (@export_attrs) {
        if ( $headers{$attr_name} ) {
            print "\t" . $headers{$attr_name};
        } else {
            print "\t" . $attr_name;
        }
    }

    # set-valued attr
    for my $sattr (@export_set_attrs) {
        my $def_s = def_Class($sattr);
        if ($def_s) {
            print "\t" . $def_s->{display_name};
        } else {
            print "\t" . $sattr;
        }
    }    # end for my sattr

    print "\n";

    my $cnt = 0;
    for ( ; ; ) {
        my ( $project_oid, @attr_vals ) = $cur->fetchrow_array();
        if ( !$project_oid ) {
            last;
        }

        $cnt++;
        if ( $cnt > 1000000 ) {

            #	if ( $cnt > 10 ) {
            last;
        }

        print $project_oid;
        for my $val (@attr_vals) {
            print "\t$val";
        }

        # set valued?
        for my $sattr (@export_set_attrs) {
            my $attr_val_href = $set_val_h{$sattr};
            my $sattr_val     = "";
            if ( $attr_val_href && $attr_val_href->{$project_oid} ) {
                $sattr_val = $attr_val_href->{$project_oid};
            }
            print "\t$sattr_val";
        }    # end for my sattr
        print "\n";
    }    # end for
    $cur->finish();
    $dbh->disconnect();

    exit 0;
}

sub getProjSetValues {
    my ( $aux_name, $t1, $t2, $values_href ) = @_;

    if ( blankStr($t1) ) {
        $t1 = "|";
    }
    if ( blankStr($t2) ) {
        $t2 = ",";
    }

    my $def_aux = RelSchema::def_Class($aux_name);
    if ( !$def_aux ) {
        return;
    }

    my $id_attr  = $def_aux->{id};
    my $sql      = "select $id_attr";
    my $order_by = " order by $id_attr";

    my @aux_attrs = @{ $def_aux->{attrs} };
    for my $attr (@aux_attrs) {
        if ( $attr->{name} eq $id_attr ) {
            next;
        }

        $sql      .= ", " . $attr->{name};
        $order_by .= ", " . $attr->{name};
    }

    $sql .= " from " . $aux_name;
    my $dbh = Connect_IMG();
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my $j        = 0;
    my $prev_id  = "";
    my $attr_val = "";
    for ( ; ; ) {
        my ( $id_val, @vals ) = $cur->fetchrow_array();
        last if !$id_val;

        if ( $id_val ne $prev_id ) {

            # new project
            if ( $prev_id && $attr_val ) {
                $values_href->{$prev_id} = $attr_val;
            }

            $attr_val = "";
            $j        = 0;
            $prev_id  = $id_val;
        }

        if ( $j == 0 ) {

            # first value
        } else {

            # print row separator
            $attr_val .= $t2;
        }

        for ( my $k = 0 ; $k < scalar(@vals) ; $k++ ) {
            my $val = $vals[$k];
            $attr_val .= $val;

            if ( $k < scalar(@vals) - 1 ) {
                $attr_val .= $t1;
            }
        }
    }

    # last one
    if ( $prev_id && $attr_val ) {
        $values_href->{$prev_id} = $attr_val;
    }

    $cur->finish();
    $dbh->disconnect();
}

##########################################################################
# PrintImportFileForm
##########################################################################
sub PrintImportFileForm {

    # need a different ENCTYPE for file upload
    print start_form(
                      -name    => "mainForm",
                      -enctype => "multipart/form-data",
                      -action  => "$section_cgi"
    );

    #		      -action => "$url" );

    print "<h2>Import Changes to IMG-GOLD Projects from File</h2>\n";

    ## Set parameters.
    print "<p>The input file must satisfy the following requirements:</p>\n";
    print "<ul>\n";
    print "<li>Plain tab-delimited text file</li>\n";
    print
"<li>The first line is the header line, and the rest lines contain data changes. Set-valued fields allow multiple values separated using ','. Each value in Data Links (URLs) contains data with format 'link-type|source-name|id|url'.</li>\n";
    print "<li>Header line includes only the following field names:\n";
    print "    (fields with (*) do not allow null values; fields with (+) are set-valued)\n";
    print "   <ul>\n";
    print "   <li>ER Submission Project OID (required)</li>\n";

    # ????
    my $def_project = def_Project_Info();
    my @attrs       = @{ $def_project->{attrs} };

    for my $tab ( 'Organism', 'Project', 'Links', 'Metadata' ) {
        print "<li>$tab attributes: ";
        my $is_first = 1;

        for my $k (@attrs) {

            # only show attributes of this tab
            if ( $k->{tab} ne $tab ) {
                next;
            }

            # skip non-editable
            if ( !$k->{can_edit} ) {
                next;
            }

            # show attribute
            my $disp_name = $k->{display_name};

            if ($is_first) {
                $is_first = 0;
                print $disp_name;
            } else {
                print ", " . $disp_name;
            }

            if ( $k->{is_required} ) {
                print " (*)";
            }
        }    # end for k

        # set-valued fields
        if ( $tab eq 'Organism' ) {

            # no set-valued
        } elsif ( $tab eq 'Project' ) {

            # seq_method
            my $def_s = def_project_info_seq_method();
            if ($def_s) {
                my $disp_name = $def_s->{display_name};
                print ", " . $disp_name . " (+)";
            }
        } elsif ( $tab eq 'Links' ) {

            # data links
            my $def_s = def_project_info_data_links();
            if ($def_s) {
                my $disp_name = $def_s->{display_name};
                print ", " . $disp_name . " (+)";
            }
        } elsif ( $tab eq 'Metadata' ) {
            for my $aux_name (
                               'project_info_cell_arrangement', 'project_info_diseases',
                               'project_info_habitat',          'project_info_metabolism',
                               'project_info_phenotypes',       'project_info_project_relevance',
                               'project_info_energy_source'
              )
            {
                my $def_s = def_Class($aux_name);
                if ($def_s) {
                    my $disp_name = $def_s->{display_name};
                    print ", " . $disp_name . " (+)";
                }
            }
        }
        print "</li>\n";
    }    # end for tab

    print "   </ul>\n";
    print "</li>\n";
    print "</ul>\n";

    print "File Name: ";
    print nbsp(1);
    print "<input type='file' id='fileselect' name='fileselect' size='100' />\n";

    print "<p>\n";

    #    print "Mode: ";
    #    print nbsp( 1 );
    #    print "<input type='radio' name='upload_ar' value='add' checked />Add\n";
    #    print nbsp( 1 );
    #    print "<input type='radio' name='upload_ar' value='replace' />Replace\n";
    #    print "</p>\n";

    # set buttons
    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:validateImportFile" value="Open" class="smdefbutton" />';
    print nbsp(1);
    print '<input type="submit" name="_section_UserTool:index" value="Cancel" class="smbutton" />';

    print "</p>\n";

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# ValidateImportFileForm
##########################################################################
sub ValidateImportFileForm {

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<h2>Import File Validation Result</h2>\n";

    my $filename = param("fileselect");

    if ( blankStr($filename) ) {
        printError("No file name is provided. Please select a file.");
        return;
    }

    # tmp file name for file upload
    my $sessionId       = getSessionId();
    my $tmp_upload_file = $cgi_tmp_dir . "/upload." . $sessionId . ".txt";

    ## Set parameters.
    print hiddenVar( "tmpUploadFile", $tmp_upload_file );

    #    print hiddenVar( "uploadMode", $replace);

    # save the uploaded file to a tmp file, because we need to parse the file
    # more than once
    if ( !open( FILE, '>', $tmp_upload_file ) ) {
        printError("Cannot open tmp file $tmp_upload_file.");
        return;
    }

    my @fld_names = ();
    my $line_no   = 0;
    my $line;
    while ( $line = <$filename> ) {

        # we don't want to process large files
        if ( $line_no <= $max_upload_line_count ) {
            print FILE $line;
        }

        if ( $line_no == 0 ) {

            # get header
            chomp($line);
            @fld_names = split( /\t/, $line );
        }

        $line_no++;
    }
    close(FILE);

    # set-valued attr
    my %aux_def;
    my @aux_names = getProjectAuxTables();
    for my $aname (@aux_names) {
        my $a_def     = def_Class($aname);
        my $disp_name = $a_def->{display_name};
        $aux_def{$disp_name} = $a_def;
    }

    # single-valued attr
    my %fld_def;
    my $def_project     = def_Project_Info();
    my @attrs           = @{ $def_project->{attrs} };
    my $hasError        = 0;
    my $project_oid_fld = 0;

    for my $name0 (@fld_names) {
        $name0 = strTrim($name0);
        my $found = 0;
        if ( $aux_def{$name0} ) {

            # set-valued
            $found = 1;
            next;
        }

        for my $attr (@attrs) {
            my $disp_name = $attr->{display_name};

            if ( $disp_name eq $name0 ) {

                # found
                if ( $attr->{name} eq 'project_oid' ) {
                    $project_oid_fld = 1;
                } elsif ( !$attr->{can_edit} ) {
                    printError( "Attribute '" . $name0 . "' cannot be updated." );
                    $hasError = 1;
                }

                $fld_def{$name0} = $attr;
                $found = 1;
                last;
            }
        }    # end for attr

        if ( !$found ) {
            printError( "Unknown field name: '" . escapeHTML($name0) . "'" );
            $hasError = 1;
        }
    }    # end for name0

    if ( !$project_oid_fld ) {
        printError("There is no 'ER Submission Project OID' field in the upload file.");
        $hasError = 1;
    }

    if ($hasError) {
        return;
    }

    print "<p><font color='red'>Any data lines that contain errors will not be uploaded.</font></p>\n";

    ## buttons
    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbFileUpload" value="Upload" class="smdefbutton" />';
    print nbsp(1);
    print '<input type="submit" name="_section_UserTool:index" value="Cancel" class="smbutton" />';

    print "</p>\n";

    print "<table class='img'>\n";
    print "<th class='img'>Line No.</th>\n";
    for my $name0 (@fld_names) {
        print "<th class='img'>$name0</th>\n";
    }
    print "<th class='img'>Message</th>\n";

    # now read from tmp file
    if ( !open( FILE, $tmp_upload_file ) ) {
        printError("Cannot open tmp file $tmp_upload_file.");
        return;
    }

    my $dbh = WebFunctions::Connect_IMG();

    $line_no = 0;
    my $upload_cnt = 0;
    while ( $line = <FILE> ) {
        chomp($line);
        my @vals = split( /\t/, $line );
        $line_no++;

        # we don't want to process large files
        if ( $line_no > $max_upload_line_count ) {
            next;
        }

        if ( $line_no == 1 ) {

            # header
            next;
        }

        my $msg = "";

        print "<tr class='img'>\n";

        print "<td class='img'>" . $line_no . "</td>\n";

        my $project_oid         = 0;
        my $domain              = "";
        my $ncbi_taxon_id       = 0;
        my $check_ncbi_taxon_id = 0;

        my $j = 0;
        for my $fld (@fld_names) {
            $fld = strTrim($fld);
            my $val = '';
            if ( $j < scalar(@vals) ) {
                $val = removeQuote( $vals[$j] );
            }

            # check field value
            my $msg1        = "";
            my $db_fld_name = "";
            if ( $aux_def{$fld} ) {

                # set valued
                $msg1 = checkSetFieldValue( $aux_def{$fld}, $val );
            } else {

                # single valued
                my $attr_def = $fld_def{$fld};
                $db_fld_name = $attr_def->{name};

                # NCBI taxon id
                if ( $db_fld_name eq 'ncbi_taxon_id'
                     && blankStr($val) )
                {
                    # defer checking
                    $check_ncbi_taxon_id = 1;
                } else {
                    $msg1 = checkFieldValue( $attr_def, $val );
                }
            }

            # display attribute value
            if ( $db_fld_name eq 'project_oid' ) {
                $project_oid = $val;
                if ( blankStr($msg1) ) {

                    # no error
                    my $proj_link = getProjectLink($project_oid);
                    PrintAttribute($proj_link);

                    # get domain in database
                    if ( blankStr($domain) ) {
                        $domain = db_getValue("select domain from project_info where project_oid = $project_oid");
                    }
                } else {
                    PrintCell( $val, 'red' );
                }
            } else {
                if ( $db_fld_name eq 'domain' ) {
                    $domain = $val;
                } elsif ( $db_fld_name eq 'ncbi_taxon_id' ) {
                    $ncbi_taxon_id = $val;
                }

                if ( blankStr($msg1) ) {
                    PrintCell($val);
                } else {
                    PrintCell( $val, 'red' );
                }
            }

            # error message?
            if ( !blankStr($msg1) ) {
                if ( blankStr($msg) ) {
                    $msg = $msg1;
                } else {
                    $msg .= " " . $msg1;
                }
            }

            $j++;
        }    # end for fld

        # check ncbi_taxon_id?
        if ( $check_ncbi_taxon_id && $domain ne 'MICROBIAL' ) {
            if ( !$ncbi_taxon_id ) {
                if ($msg) {
                    $msg .= " NCBI Taxon ID cannot be null for genome project.";
                } else {
                    $msg = "NCBI Taxon ID cannot be null for genome project.";
                }
            }
        }

        # error message?
        if ($msg) {
            print "<td class='img'><font color='red'>" . escapeHTML($msg) . "</font></td>\n";
        } else {
            $upload_cnt++;
            print "<td class='img'></td>\n";
        }

        print "</tr>\n";
    }

    print "</table>\n";
    print "<br/>\n";

    $dbh->disconnect();

    close(FILE);

    if ( $line_no >= $max_upload_line_count ) {
        print "<p>File is too large. Only $max_upload_line_count lines were processed.</p>\n";
    }

    print "<p>Total projects to be updated: $upload_cnt</p>\n";
    print hiddenVar( 'upload_cnt', $upload_cnt );

    ## buttons
    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbFileUpload" value="Upload" class="smdefbutton" />';
    print nbsp(1);
    print '<input type="submit" name="_section_UserTool:index" value="Cancel" class="smbutton" />';

    print "</p>\n";

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# checkFieldValue
#
# attr_def: attribute definition
# val: attribute value
##########################################################################
sub checkFieldValue {
    my ( $attr_def, $val ) = @_;

    my $msg1         = "";
    my $attr_name    = $attr_def->{name};
    my $display_name = $attr_def->{display_name};

    # check attribute

    # required
    if ( $attr_def->{is_required} ) {
        if ( blankStr($val) ) {
            $msg1 = $display_name . " must have a value.";
            return $msg1;
        }
    }
    if ( blankStr($val) ) {
        return $msg1;
    }

    # special check for MICROBIAL
    if ( $attr_name eq 'domain' && $val eq 'MICROBIAL' ) {
        return $msg1;
    }

    # check data type
    if ( $attr_def->{data_type} eq 'int' ) {
        if ( !isInt($val) ) {
            $msg1 = $display_name . " ($val) must be an integer.";
            return $msg1;
        }
    } elsif ( $attr_def->{data_type} eq 'number' ) {
        if ( !isNumber($val) ) {
            $msg1 = $display_name . " ($val) must be a number.";
            return $msg1;
        }
    } elsif ( $attr_def->{data_type} eq 'char' ) {
        if ( length($val) > $attr_def->{length} ) {
            $msg1 = $display_name . " ($val) cannot exceed " . $attr_def->{length} . " characters.";
            return $msg1;
        }
    } elsif ( $attr_def->{data_type} eq 'list' ) {
        my @cv_vals = split( /\|/, $attr_def->{list_values} );
        if ( !inArray( $val, @cv_vals ) ) {
            $msg1 = "Incorrect controlled value for $display_name: " . $val . ".";
            return $msg1;
        }
    } elsif ( $attr_def->{data_type} eq 'cv' ) {
        my $cv_query = $attr_def->{cv_query};
        if ( $cv_query =~ /select(\s+)(\w+)(\s+)from(\s+)(\w+)/ ) {
            my $cv_fld   = $2;
            my $cv_table = $5;
            my $val2     = $val;
            $val2 =~ s/'/''/g;    # replace ' with ''
            my $cnt1 = db_getValue("select count(*) from $cv_table where $cv_fld = '$val2'");
            if ( !$cnt1 ) {
                $msg1 = "Incorrect controlled value for $display_name: " . $val . ".";
                return $msg1;
            }
        } else {
            $msg1 = "Cannot find CV class for $display_name";
        }
    }

    if ( $attr_name eq 'project_oid' ) {
        my $cnt = db_getValue("select count(*) from project_info where project_oid = $val");
        if ( !$cnt ) {
            $msg1 = "Project $val is not in the database.";
            return $msg1;
        }
    }

    return $msg1;
}

##########################################################################
# checkSetFieldValue
#
# aux_def: set-valued attribute definition
# aux_val: attribute value
##########################################################################
sub checkSetFieldValue {
    my ( $aux_def, $aux_val ) = @_;

    if ( !$aux_def ) {
        return "No field definition";
    }
    my $id_fld = $aux_def->{id};

    my $msg1      = "";
    my $disp_name = $aux_def->{display_name};
    my @attrs     = @{ $aux_def->{attrs} };

    my @vals = split( /\,/, $aux_val );

    # check attribute values
    for my $t_val (@vals) {
        $t_val = strTrim($t_val);

        my @v0 = split( /\|/, $t_val );
        my $i  = 0;
        for my $attr_def (@attrs) {
            if ( !$attr_def ) {
                return "No field definition";
            }

            if ( $attr_def->{name} eq $id_fld ) {

                # skip
                next;
            }

            my $val = '';
            if ( scalar(@v0) > $i ) {
                $val = strTrim( $v0[$i] );
            }

            # check value
            # check data type
            if ( $attr_def->{data_type} eq 'int' ) {
                if ( !isInt($val) ) {
                    $msg1 = $attr_def->{display_name} . " ($val) must be an integer.";
                    return $msg1;
                }
            } elsif ( $attr_def->{data_type} eq 'number' ) {
                if ( !isNumber($val) ) {
                    $msg1 = $attr_def->{display_name} . " ($val) must be a number.";
                    return $msg1;
                }
            } elsif ( $attr_def->{data_type} eq 'char' ) {
                if ( length($val) > $attr_def->{length} ) {
                    $msg1 = $attr_def->{display_name} . " ($val) cannot exceed " . $attr_def->{length} . " characters.";
                    return $msg1;
                }
            } elsif ( $attr_def->{data_type} eq 'cv' ) {
                my $cv_query = $attr_def->{cv_query};
                if ( $cv_query =~ /select(\s+)(\w+)(\s+)from(\s+)(\w+)/ ) {
                    my $cv_fld   = $2;
                    my $cv_table = $5;
                    my $val2     = $val;
                    $val2 =~ s/'/''/g;    # replace ' with ''
                    my $cnt1 = db_getValue("select count(*) from $cv_table where $cv_fld = '$val2'");
                    if ( !$cnt1 ) {
                        $msg1 = "Incorrect controlled value for " . $disp_name . ": " . $val . ".";
                        return $msg1;
                    }
                } else {
                    $msg1 = "Cannot find CV class for $disp_name";
                }
            }
        }    # end for my attr_def
    }    # end for t_val

    return $msg1;
}

##########################################################################
# dbFileUpload
##########################################################################
sub dbFileUpload {

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<h2>Update IMG-GOLD Projects Using File Upload</h2>\n";

    my $filename = param1('tmpUploadFile');

    if ( blankStr($filename) ) {
        printError("No file exists.");
        return;
    }
    print "<h3>Temporary File Name: $filename</h3>\n";

    my $my_c_oid = getContactOid();
    if ( !$my_c_oid ) {
        printerror("Login expired.");
        return;
    }

    my $upload_cnt = param1('upload_cnt');
    if ( !$upload_cnt ) {
        printError("Upload file does not contain any correct data lines.");
        print "<p>\n";
        printHomeLink();

        print end_form();
        return;
    }

    # perform upload
    # now read from tmp file
    if ( !open( FILE, $filename ) ) {
        printError("Cannot open tmp file $filename.");
        return;
    }

    my $dbh = WebFunctions::Connect_IMG();

    # set-valued attr
    my %aux_def;
    my @aux_names = getProjectAuxTables();
    for my $aname (@aux_names) {
        my $a_def     = def_Class($aname);
        my $disp_name = $a_def->{display_name};
        $aux_def{$disp_name} = $a_def;
    }

    # single-valued attr
    my %fld_def;
    my $def_project = def_Project_Info();
    my @attrs       = @{ $def_project->{attrs} };

    my @fld_names  = ();
    my $line_no    = 0;
    my $line       = "";
    my $update_cnt = 0;
    my $hasError   = 0;
    my @sqlList    = ();

    while ( $line = <FILE> ) {
        chomp($line);
        my @vals = split( /\t/, $line );

        $line_no++;

        if ( $line_no == 1 ) {

            # get header
            chomp($line);
            @fld_names = split( /\t/, $line );

            for my $attr0 (@attrs) {
                my $aname0 = $attr0->{display_name};
                if ( inArray( $aname0, @fld_names ) ) {
                    $fld_def{$aname0} = $attr0;
                }
            }

            next;
        }

        if ( $line_no > 1000000 ) {
            last;
        }

        if ( ( $line_no % 100 ) == 0 ) {
            print "<p>$line_no lines processed ...</p>\n";
        }

        # we don't want to process large files
        if ( $line_no > $max_upload_line_count ) {
            next;
        }

        $hasError = 0;
        my $sql     = "";
        my @delList = ();
        my @insList = ();
        my @valList = ();

        my $project_oid         = 0;
        my $domain              = "";
        my $ncbi_taxon_id       = 0;
        my $check_ncbi_taxon_id = 0;

        my $j = 0;
        for my $fld (@fld_names) {
            $fld = strTrim($fld);
            my $val = '';
            if ( $j < scalar(@vals) ) {
                $val = removeQuote( $vals[$j] );
            }

            # print "<p>$fld: $val</p>\n";

            # check field value
            my $msg1        = "";
            my $db_fld_name = "";
            if ( $aux_def{$fld} ) {

                # set valued
                my $a_def = $aux_def{$fld};
                $msg1 = checkSetFieldValue( $a_def, $val );

                if ( blankStr($msg1) ) {
                    my $sql2 = "delete from " . $a_def->{name} . " where " . $a_def->{id} . " = ";
                    push @delList, ($sql2);

                    my @s_attrs = @{ $a_def->{attrs} };
                    my @vals    = split( /\,/, $val );
                    for my $t_val (@vals) {
                        $t_val = strTrim($t_val);

                        my @v0   = split( /\|/, $t_val );
                        my $i    = 0;
                        my $ins3 = "insert into $a_def->{name} (" . $a_def->{id};
                        my $val3 = "";

                        for my $s_attr_def (@s_attrs) {
                            if ( !$s_attr_def ) {
                                next;
                            }

                            if ( $s_attr_def->{name} eq $a_def->{id} ) {

                                # skip
                                next;
                            }

                            my $val = '';
                            if ( scalar(@v0) > $i ) {
                                $val = strTrim( $v0[$i] );
                            }

                            # check value
                            # check data type
                            $ins3 .= ", " . $s_attr_def->{name};
                            if ( blankStr($val) ) {
                                $val3 .= ", null";
                            } elsif (    $s_attr_def->{data_type} eq 'int'
                                      || $s_attr_def->{data_type} eq 'number' )
                            {
                                $val3 .= ", " . $val;
                            } else {
                                $val =~ s/'/''/g;    # replace ' with ''
                                $val3 .= ", '" . $val . "'";
                            }

                            # next
                            $i++;
                        }    # end for s_attr_def

                        push @insList, ($ins3);
                        push @valList, ($val3);
                    }    # end for t_val
                }
            } else {

                # single valued
                my $attr_def = $fld_def{$fld};
                $db_fld_name = $attr_def->{name};

                if ( $db_fld_name eq 'domain' ) {
                    $domain = $val;
                } elsif ( $db_fld_name eq 'ncbi_taxon_id' ) {
                    $ncbi_taxon_id = $val;
                }

                # NCBI taxon id
                if ( $db_fld_name eq 'ncbi_taxon_id'
                     && blankStr($val) )
                {
                    # defer checking
                    $check_ncbi_taxon_id = 1;
                } else {
                    $msg1 = checkFieldValue( $attr_def, $val );
                }

                if ( $db_fld_name eq 'project_oid' ) {
                    $project_oid = $val;

                    # print "<p>project oid is: $project_oid</p>\n";
                } elsif ( blankStr($msg1) ) {

                    # update
                    if ( blankStr($sql) ) {
                        $sql = "update project_info set $db_fld_name = ";
                    } else {
                        $sql .= ", $db_fld_name = ";
                    }

                    if ( blankStr($val) ) {
                        $sql .= "null";
                    } elsif (    $attr_def->{data_type} eq 'int'
                              || $attr_def->{data_type} eq 'number' )
                    {
                        $sql .= $val;
                    } else {
                        $val =~ s/'/''/g;    # replace ' with '';
                        $sql .= "'$val'";
                    }
                }
            }

            # error message
            if ( !blankStr($msg1) ) {
                $hasError = 1;
                last;
            }

            $j++;
        }    # end for fld

        # check ncbi_taxon_id?
        if ( $check_ncbi_taxon_id && $domain ne 'MICROBIAL' ) {
            if ( !$ncbi_taxon_id ) {
                $hasError = 1;
            }
        }

        # error message?
        if ($hasError) {

            # no update
            print "<p>Error: line $line_no, project oid: $project_oid</p>\n";
        } else {
            $update_cnt++;

            $sql .= ", modified_by = $my_c_oid, mod_date = sysdate";
            $sql .= " where project_oid = $project_oid";
            push @sqlList, ($sql);

            # delList
            for my $sql4 (@delList) {
                $sql4 .= $project_oid;
                push @sqlList, ($sql4);
            }

            # insList
            for ( my $j = 0 ; $j < scalar(@insList) ; $j++ ) {
                my $sql5 = $insList[$j] . ", modified_by, add_date) " . " values (" . $project_oid;
                if ( $j < scalar(@valList) ) {
                    $sql5 .= $valList[$j] . ", $my_c_oid, sysdate)";
                    push @sqlList, ($sql5);
                }
            }
        }
    }

    $dbh->disconnect();

    close(FILE);

    if ( $line_no >= $max_upload_line_count ) {
        print "<p>File is too large. Only $max_upload_line_count lines were processed.</p>\n";
    }

    #    for my $sql2 ( @sqlList ) {
    #	print "<p>SQL: $sql2;</p>\n";
    #    }

    db_sqlTrans( \@sqlList );

    print "<p>$update_cnt projects updated in database ($upload_cnt updates expected).</p>\n";

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# GenerateContactReport
##########################################################################
sub GenerateContactReport {

    my $sql = qq{
        select contact_oid, username, name,
        organization, country, state, email, add_date,
        caliban_id, caliban_user_name
            from contact 
	    order by 3
        };

    my $dbh = WebFunctions::Connect_IMG;
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    # print Excel Header
    my $fileName = "contact_export$$.xls";
    print "Content-type: application/vnd.ms-excel\n";
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n";

    #    print "SQL: $sql\n";

    # print header line
    print "Name\tOrganization\tCountry\tState\tEmail\tAccount Created\t"
      . "Caliban ID\tCaliban Name\t"
      . "Most Recent Activity Date\n";

    my $cnt      = 0;
    my %user_act = getMostRecentActDate(1);

    for ( ; ; ) {
        my ( $c_oid, $username, $name, $org, $country, $state, $email, $add_date, $caliban_id, $caliban_name ) =
          $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $username =~ /^MISSING/ ) {
            next;
        }

        if (    $username =~ /^IMG\_/
             || $username =~ /^img\_/
             || $username =~ /^IMGSG\_/ )
        {
            next;
        }

        $country = convertCountry($country);

        $cnt++;

        print "$name\t";
        print "$org\t";
        print "$country\t";
        print "$state\t";
        print "$email\t";
        print "$add_date\t";
        print "$caliban_id\t";
        print "$caliban_name\t";

        # most recent activity
        print $user_act{$c_oid};
        print "\n";
    }

    $cur->finish();
    $dbh->disconnect();

    exit 0;
}

##########################################################################
# GenerateContactProjectReport
##########################################################################
sub GenerateContactProjectReport {

    # IMG ER
    my $dbh = Connect_IMG_ER();
    my $sql = qq{
        select distinct c.contact_oid, t.taxon_oid, t.taxon_display_name
            from taxon t, contact_taxon_permissions c
            where c.taxon_permissions = t.taxon_oid
	    order by 1
        };
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    my %projs;
    for ( ; ; ) {
        my ( $c_oid, $taxon_oid, $taxon_name ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        my $str = "$taxon_name (ER OID: $taxon_oid)";
        if ( $projs{$c_oid} ) {
            $projs{$c_oid} .= "\n" . $str;
        } else {
            $projs{$c_oid} = $str;
        }
    }
    $cur->finish();
    $dbh->disconnect();

    # IMG MER
    $dbh = Connect_IMG_ER();
    $sql = qq{
        select distinct c.contact_oid, t.taxon_oid, t.taxon_display_name
            from taxon t, contact_taxon_permissions c
            where c.taxon_permissions = t.taxon_oid
	    order by 1
        };
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $c_oid, $taxon_oid, $taxon_name ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        my $str = "$taxon_name (MER OID: $taxon_oid)";
        if ( $projs{$c_oid} ) {
            $projs{$c_oid} .= "\n" . $str;
        } else {
            $projs{$c_oid} = $str;
        }
    }
    $cur->finish();
    $dbh->disconnect();

    # get contacts
    $dbh = Connect_IMG();
    $sql = qq{
        select contact_oid, username, name,
        organization, country
            from contact 
	    order by 3
        };
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();

    # print Excel Header
    my $fileName = "contact_export$$.xls";
    print "Content-type: application/vnd.ms-excel\n";
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n";

    #    print "SQL: $sql\n";

    # print header line
    print "Name\tOrganization\tCountry\tProjects\n";

    my $cnt = 0;
    for ( ; ; ) {
        my ( $c_oid, $username, $name, $org, $country ) = $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $username =~ /^MISSING/ ) {
            next;
        }

        if (    $username =~ /^IMG\_/
             || $username =~ /^img\_/
             || $username =~ /^IMGSG\_/
             || $username eq 'GEBA' )
        {
            next;
        }

        $country = convertCountry($country);

        if ( $projs{$c_oid} ) {

            # has projects
            my @projects = split( /\n/, $projs{$c_oid} );
            for my $p ( sort @projects ) {
                print "$name\t";
                print "$org\t";
                print "$country\t";
                print "$p\n";
            }
        }
    }

    $cur->finish();
    $dbh->disconnect();

    exit 0;
}

##########################################################################
# genProjectReport
##########################################################################
sub genProjectReport {

    my $contact_oid = getContactOid();
    my $submitAdmin = 'No';

    if ($contact_oid) {
        $submitAdmin = isSubmissionAdmin($contact_oid);
    }
    if ( $submitAdmin ne 'Yes' ) {
        return;
    }

    my $proj_attr = param("proj_attr_option");
    my $attr_op   = param("proj_op");
    my $date1     = param("proj_date_1");
    my $date2     = param("proj_date_2");
    if ($date1) {
        if ( !isDate($date1) ) {
            my $msg = "Incorrect date format -- should be DD-MON-YY (e.g., 01-MAR-11)";
            return $msg;
        }
    }
    if ( $date2 && $attr_op eq 'between' ) {
        if ( !isDate($date2) ) {
            my $msg = "Incorrect date format -- should be DD-MON-YY (e.g., 01-MAR-11)";
            return $msg;
        }
    }

    my $db_option       = param("proj_db_option");
    my $proj_seq_center = param("proj_seq_center");

    if (    $db_option eq 'IMG_MER_RNASEQ'
         && $proj_seq_center eq 'non_jgi' )
    {
        my $msg = "There are no Non-JGI Meta RNA Seq submissions.";
        return $msg;
    }

    my $submit_cond = "";
    if ( $proj_attr eq 'submission_date' && $attr_op && $date1 ) {
        if ( $attr_op eq 'between' ) {
            if ($date2) {
                $submit_cond = " and s.$proj_attr between '$date1' and '$date2' ";
            } else {
                $submit_cond = " and s.$proj_attr >= '$date1' ";
            }
        } else {
            $submit_cond = " and s.$proj_attr $attr_op '" . $date1 . "' ";
        }
    }

    my $dbh2;
    if ( $db_option eq 'IMG ER' ) {

        # IMG ER
        $dbh2 = Connect_IMG_ER();
    } elsif ( $db_option eq 'IMG/M ER' || $db_option eq 'IMG_MER_RNASEQ' ) {

        # IMG/M ER
        $dbh2 = Connect_IMG_MI();
    } else {
        my $msg = "Please select a database.";
        return $msg;
    }

    my $dbh = Connect_IMG();

    # print Excel Header
    my $fileName = "submission_export$$.xls";
    print "Content-type: application/vnd.ms-excel\n";
    print "Content-Disposition: inline;filename=$fileName\n";
    print "\n";

    # print header line
    print "Submission ID\tER Submission Project Id\t" . "Domain\tProject Name\t";

    my %predicted_gene_count;
    if ( $contact_oid == 312 ) {
        print "Predicted Gene Count\t";

        my $sql2 = qq{
        select submission_id, sum(no_pred_features)
            from submission_proc_stats
            where step = 'GeneCalling'
            and type_of_feature not like 'SequencesWith%'
            group by submission_id
        };

        my $cur2 = $dbh->prepare($sql2);
        $cur2->execute();
        for ( ; ; ) {
            my ( $sub_id, $cnt2 ) = $cur2->fetchrow_array();
            last if !$sub_id;
            $predicted_gene_count{$sub_id} = $cnt2;
        }
        $cur2->finish();
    }

    ## analysis project
    my %ap_submit_h;
    my $sql1 =
      "select s.analysis_project_id from submission s " . "where s.analysis_project_id is not null " . "and s.database = ? ";
    my $cur1 = $dbh->prepare($sql1);
    $cur1->execute($db_option);
    for ( ; ; ) {
        my ($ap_id1) = $cur1->fetchrow();
        last if !$ap_id1;
        $ap_submit_h{$ap_id1} = 1;
    }
    $cur1->finish();

    my %ap_project_h;
    my %ap_sample_h;
    my %ap_name_h;
    my %ap_domain_h;
    my %ap_its_id_h;
    my %ap_spid_h;
    my %ap_s_prog_h;
    my %ap_f_prog_h;
    my %ap_f_year_h;
    my %ap_bioclass_h;
    my %ap_culture_type_h;
    my %ap_prod_name_h;
    my %ap_proj_type_h;

    my $sql2 =
        "select ap.gold_id, lookup.project_oid, lookup.sample_oid, "
      . "ap.analysis_project_name, ap.domain, "
      . "ap.its_analysis_project_id, "
      . "ap.analysis_product_name, "
      . "p.culture_type, p.uncultured_type, "
      . "p.scientific_program, p.jgi_funding_program, "
      . "p.jgi_funding_year, p.gpts_bioclassification_name, "
      . "p.its_spid, es.its_spid, ap.gold_analysis_project_type "
      . "from gold_analysis_project ap, "
      . "gold_analysis_project_lookup2 lookup, "
      . "submission s, project_info p, env_sample es "
      . "where ap.gold_id is not null "
      . "and ap.gold_id = lookup.gold_id "
      . "and ap.gold_id = s.analysis_project_id "
      . "and lookup.project_oid = p.project_oid (+) "
      . "and lookup.sample_oid = es.sample_oid (+) ";

    ## faster not to join with submission table
    $sql2 =
        "select ap.gold_id, lookup.project_oid, lookup.sample_oid, "
      . "ap.analysis_project_name, ap.domain, "
      . "ap.its_analysis_project_id, "
      . "ap.analysis_product_name, "
      . "p.culture_type, p.uncultured_type, "
      . "p.scientific_program, p.jgi_funding_program, "
      . "p.jgi_funding_year, p.gpts_bioclassification_name, "
      . "lookup.its_spid, ap.gold_analysis_project_type "
      . "from gold_analysis_project ap, "
      . "gold_analysis_project_lookup2 lookup, "
      . "project_info p, env_sample es "
      . "where ap.gold_id is not null "
      . "and ap.gold_id = lookup.gold_id "
      . "and lookup.project_oid = p.project_oid (+) "
      . "and lookup.sample_oid = es.sample_oid (+) ";

    my $cur2 = $dbh->prepare($sql2);
    $cur2->execute();
    for ( ; ; ) {
        my (
             $ap_id, $p_id,   $s_id,   $ap_name, $ap_domain, $ap_its_id, $ap_prod_name, $type1,
             $type2, $s_prog, $f_prog, $f_year,  $bioclass,  $spid1,     $ap_proj_type
        ) = $cur2->fetchrow_array();
        last if !$ap_id;

        if ( !$ap_submit_h{$ap_id} ) {
            next;
        }

        if ($p_id) {
            $ap_project_h{$ap_id} = $p_id;
        }
        if ($s_id) {
            $ap_sample_h{$ap_id} = $s_id;
            if ( !$ap_domain ) {
                $ap_domain = 'MICROBIAL';
            }
        }
        $ap_name_h{$ap_id}      = $ap_name;
        $ap_domain_h{$ap_id}    = $ap_domain;
        $ap_its_id_h{$ap_id}    = $ap_its_id;
        $ap_prod_name_h{$ap_id} = $ap_prod_name;

        if ($type1) {
            $ap_culture_type_h{$ap_id} = $type1;
        } elsif ($type2) {
            $ap_culture_type_h{$ap_id} = $type2;
        }

        $ap_s_prog_h{$ap_id}   = $s_prog;
        $ap_f_prog_h{$ap_id}   = $f_prog;
        $ap_f_year_h{$ap_id}   = $f_year;
        $ap_bioclass_h{$ap_id} = $bioclass;
        if ($spid1) {
            if ( $ap_spid_h{$ap_id} ) {
                $ap_spid_h{$ap_id} .= ", " . $spid1;
            } else {
                $ap_spid_h{$ap_id} = $spid1;
            }
        }

        $ap_proj_type_h{$ap_id} = $ap_proj_type;
    }
    $cur2->finish();

    ## additional JGI info
    my %jgi_prog_h;
    $sql2 =
        "select s.submission_id, p.scientific_program, "
      . "p.jgi_funding_program, "
      . "p.jgi_funding_year, p.gpts_bioclassification_name, "
      . "p.its_spid "
      . "from project_info p, submission s "
      . "where p.project_oid = s.project_info "
      . "and s.database = '$db_option' "
      . $submit_cond;
    if ( $db_option eq 'IMG/M ER' || $db_option eq 'IMG_MER_RNASEQ' ) {
        $sql2 =
            "select s.submission_id, e.scientific_program, "
          . "e.jgi_funding_program, "
          . "e.jgi_funding_year, p.gpts_bioclassification_name, "
          . "e.its_spid "
          . "from project_info p, submission s, env_sample e "
          . "where p.project_oid = s.project_info "
          . "and s.sample_oid = e.sample_oid "
          . "and s.database = '$db_option' "
          . $submit_cond;
    }
    $cur2 = $dbh->prepare($sql2);
    $cur2->execute();
    for ( ; ; ) {
        my ( $sub_id2, $scientific_program, $funding_program, $funding_year, $bioclass, $its_spid ) =
          $cur2->fetchrow_array();
        last if !$sub_id2;

        $jgi_prog_h{$sub_id2} = "$scientific_program\t$funding_program" . "\t$funding_year\t$bioclass\t$its_spid";
    }
    $cur2->finish();

    ## project manager and proposal ids
    my %pm_name_h;
    my %sp_its_prop_h;
    my %sp_gpts_prop_h;

    $sql2 =
        "select distinct ap.gold_id, sp.pm_name, "
      . "sp.its_proposal_id, sp.gpts_proposal_id "
      . "from gold_analysis_project ap, "
      . "gold_analysis_project_lookup2 lookup, "
      . "gold_sequencing_project sp "
      . "where ap.gold_id is not null "
      . "and ap.gold_id = lookup.gold_id "
      . "and lookup.sp_gold_id = sp.gold_id ";
    $cur2 = $dbh->prepare($sql2);
    $cur2->execute();

    for ( ; ; ) {
        my ( $gold_ap_id, $pm_name, $its_prop_id, $gpts_prop_id ) = $cur2->fetchrow_array();
        if ( !$gold_ap_id ) {
            last;
        }

        if ($pm_name) {
            if ( $pm_name_h{$gold_ap_id} ) {
                $pm_name_h{$gold_ap_id} .= ",$pm_name";
            } else {
                $pm_name_h{$gold_ap_id} = $pm_name;
            }
        }

        if ($its_prop_id) {
            $sp_its_prop_h{$gold_ap_id} = $its_prop_id;
        }
        if ($gpts_prop_id) {
            $sp_gpts_prop_h{$gold_ap_id} = $gpts_prop_id;
        }
    }
    $cur2->finish();

    ## sequencing_center
    my %seq_center_h;
    $sql2 =
        "select lookup.gold_id, sc.name "
      . "from gold_analysis_project_lookup2 lookup, "
      . "gold_sp_seq_center sc "
      . "where lookup.sp_gold_id = sc.gold_id "
      . "and sc.name is not null ";
    $cur2 = $dbh->prepare($sql2);
    $cur2->execute();
    for ( ; ; ) {
        my ( $gold_ap_id, $sc_name ) = $cur2->fetchrow_array();
        if ( !$gold_ap_id ) {
            last;
        }

        if ($sc_name) {
            if ( $seq_center_h{$gold_ap_id} ) {
                $seq_center_h{$gold_ap_id} .= ",$sc_name";
            } else {
                $seq_center_h{$gold_ap_id} = $sc_name;
            }
        }
    }
    $cur2->finish();

    ## new submission status for gene model QA submissions
    my %new_submission_status_h;
    $sql2 =
        "select s.submission_id, s.prev_submission_id, cv.cv_term "
      . "from submission s, submission_statuscv cv "
      . "where s.prev_submission_id is not null "
      . "and s.status = cv.term_oid ";
    $cur2 = $dbh->prepare($sql2);
    $cur2->execute();
    for ( ; ; ) {
        my ( $new_id, $prev_id, $new_status ) = $cur2->fetchrow_array();
        last if !$new_id;
        $new_submission_status_h{$prev_id} = "resubmitted as $new_id with new status '" . $new_status . "'";
    }
    $cur2->finish();

    #    print "Sequencing Quality\tSeq Status\tGOLD ID\t";
    print "Project Seq Center\t" . "PMO Project ID\t" .

      #	"JGI Directory Number\t" .
      "ITS AP ID\tITS SP ID\t" . "ITS Proposal ID\tGPTS Proposal ID\t" . "Project Manager\t" .

      #	"JGI Scientific Program\tJGI Funding Program\t" .
      #	"JGI Funding Year\tBioclassification Name\t" .
      "Culture Type\t" .

      #	"Scope of Work\t" .
      "Analysis Product Name\t"
      . "Submitter\tSubmission Status\tIMG OID\t"
      . "Submission Date\t"
      . "Gene Calling Start Date\tTime between Submission and Gene Calling Start\t"
      . "Gene Calling End Date\tTime between Submission and Gene Calling End\t"
      . "Validation End Date\tTime between Submission and Validation End\t"
      . "Annotation Available Date\tTime between Submission and Annotation Available for Downloading\t"
      . "Loading Date\tTime between Submission and Loading Start\t"
      . "IMG Loading Time\t"
      . "Loaded into IMG Date\tTime between Submission and Loaded into IMG\t"
      . "Public or Private\tGPTS Embargo Start Date\t"
      . "GPTS Embargo Days\tRelease Date\n";

    my $sql = qq{ 
	select s.submission_id, p.project_oid, s.is_jgi,
	p.domain, p.display_name, p.seq_quality, s.seq_status,
        p.gold_stamp_id, s.analysis_project_id,
	p.pmo_project_id, p.jgi_dir_number,
        p.culture_type, p.uncultured_type, p.scope_of_work,
        p.gpts_embargo_start_date, p.gpts_embargo_days,
        c.username, v.cv_term, s.img_taxon_oid,
	to_char(s.submission_date, 'DD-MON-YYYY HH24:MI:SS'),
	floor(s.submission_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS')) 
	    from submission s, project_info p, submission_statuscv v, contact c
	    where s.project_info = p.project_oid (+)
	    and s.database = '$db_option'
	    and s.contact = c.contact_oid (+)
	    and s.status != 90
	    and s.status = v.term_oid
	    $submit_cond
	    order by 1 
	};

##    print "$sql\n";

    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my (
             $submission_id,       $project_oid,       $is_jgi,                  $domain,
             $project_name,        $seq_quality,       $seq_status,              $gold_id,
             $analysis_project_id, $pmo_project_id,    $jgi_dir_number,          $culture_type,
             $uncultured_type,     $scope_of_work,     $gpts_embargo_start_date, $gpts_embargo_days,
             $username,            $submission_status, $img_oid,                 $submit_date,
             $submit_date_diff
        ) = $cur->fetchrow();
        last if ( !$submission_id );

        if ( $proj_seq_center eq 'jgi_only' ) {
            if ( $is_jgi eq 'Yes' ) {

                # JGI project
            } else {

                # non-JGI project
                next;
            }
        } elsif ( $proj_seq_center eq 'non_jgi' ) {
            if ( $is_jgi eq 'Yes' ) {

                # JGI project
                next;
            } else {

                # non-JGI project
            }
        }

        my $seq_center = "";
        if ($analysis_project_id) {
            $gold_id = $analysis_project_id;
            if ( $ap_project_h{$analysis_project_id} ) {
                $project_oid = $ap_project_h{$analysis_project_id};
            }
            if ( $ap_domain_h{$analysis_project_id} ) {
                $domain = $ap_domain_h{$analysis_project_id};
            }
            if ( $ap_name_h{$analysis_project_id} ) {
                $project_name = $ap_name_h{$analysis_project_id};
            }
            if ( $seq_center_h{$analysis_project_id} ) {
                $seq_center = $seq_center_h{$analysis_project_id};
            }
        }

        #	my $seq_center = getProjSeqCenter($dbh, $project_oid);
        #	if ( ! $seq_center && $analysis_project_id ) {
        #	    my $sample_oid = $ap_sample_h{$analysis_project_id};
        #	    $seq_center = getSampleSeqCenter($dbh, $sample_oid);
        #	}

        #	if ( $proj_seq_center eq 'jgi_only' ) {
        #	    if ( $seq_center =~ /JGI/ ||
        #		 $seq_center =~ /Joint Genome Institute/ ) {
        #		# JGI project
        #	    }
        #	    else {
        #		# non-JGI project
        #		next;
        #	    }
        #	}
        #	elsif ( $proj_seq_center eq 'non_jgi' ) {
        #	    if ( $seq_center =~ /JGI/ ||
        #		 $seq_center =~ /Joint Genome Institute/ ) {
        #		# JGI project
        #		next;
        #	    }
        #	    else {
        #		# non-JGI project
        #	    }
        #	}

        # preparing gene calling
        my $sql3 = qq{
	    select submission_id, mod_date,
	    to_char(mod_date, 'DD-MON-YYYY HH24:MI:SS'), 
	    floor(mod_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))
		from submission_history 
		where submission_id = $submission_id
		and status = 20
                order by 1, 2 desc
		};
        my $cur3 = $dbh->prepare($sql3) || die "Cannot prepare $sql3\n";
        $cur3->execute() || die "Cannot execute $sql3\n";
        my ( $sid3, $gc1_date, $gc1_date_str, $gc1_date_diff ) = $cur3->fetchrow();
        $cur3->finish();

        # finishing gene calling
        $sql3 = qq{
	    select submission_id, mod_date,
	    to_char(mod_date, 'DD-MON-YYYY HH24:MI:SS'), 
	    floor(mod_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))
		from submission_history 
		where submission_id = $submission_id
		and status in (3, 4, 18)
		order by 1, 2 desc
		};
        $cur3 = $dbh->prepare($sql3) || die "Cannot prepare $sql3\n";
        $cur3->execute() || die "Cannot execute $sql3\n";
        my ( $sid4, $gc2_date, $gc2_date_str, $gc2_date_diff ) = $cur3->fetchrow();
        $cur3->finish();

        # validation
        $sql3 = qq{
	    select submission_id, mod_date,
	    to_char(mod_date, 'DD-MON-YYYY HH24:MI:SS'), 
	    floor(mod_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))
		from submission_history 
		where submission_id = $submission_id
		and status in (5, 6, 7)
		order by 1, 2 desc
		};
        $cur3 = $dbh->prepare($sql3) || die "Cannot prepare $sql3\n";
        $cur3->execute() || die "Cannot execute $sql3\n";
        my ( $sid5, $vc1_date, $vc1_date_str, $vc1_date_diff ) = $cur3->fetchrow();
        $cur3->finish();

        # annotation available for downloading
        $sql3 = qq{
	    select submission_id, mod_date,
	    to_char(mod_date, 'DD-MON-YYYY HH24:MI:SS'), 
	    floor(mod_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))
		from submission_history 
		where submission_id = $submission_id
		and status in (26)
		order by 1, 2 desc
		};
        $cur3 = $dbh->prepare($sql3) || die "Cannot prepare $sql3\n";
        $cur3->execute() || die "Cannot execute $sql3\n";
        my ( $sid6, $ann1_date, $ann1_date_str, $ann1_date_diff ) = $cur3->fetchrow();
        $cur3->finish();

        # finished
        $sql3 = qq{
	    select submission_id, mod_date,
	    to_char(mod_date, 'DD-MON-YYYY HH24:MI:SS'), 
	    floor(mod_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS'))
		from submission_history 
		where submission_id = $submission_id
		and status in (8, 10)
		order by 1, 2 desc
		};
        $cur3 = $dbh->prepare($sql3) || die "Cannot prepare $sql3\n";
        $cur3->execute() || die "Cannot execute $sql3\n";
        my ( $sid7, $fin1_date, $fin1_date_str, $fin1_date_diff ) = $cur3->fetchrow();
        $cur3->finish();

        # loading into IMG
        my $load_date        = "";
        my $load_date_str    = "";
        my $load_date_diff   = "";
        my $public_flag      = "";
        my $release_date     = "";
        my $release_date_str = "";
        if (    $db_option eq 'IMG_MER_RNASEQ'
             && $submission_id
             && isInt($submission_id) )
        {
            my $sql2 = qq{
		select add_date, 
		to_char(add_date, 'DD-MON-YYYY HH24:MI:SS'),
		floor(add_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS')), 
		is_public, '', ''
		    from rnaseq_dataset where submission_id = $submission_id
		};
            my $cur2 = $dbh2->prepare($sql2) || die "Cannot prepare $sql2\n";
            $cur2->execute() || die "Cannot execute $sql2\n";
            ( $load_date, $load_date_str, $load_date_diff, $public_flag, $release_date, $release_date_str ) =
              $cur2->fetchrow();
            if ( $public_flag eq 'Yes' ) {
                $public_flag = 'Public';
            } else {
                $public_flag = 'Private';
            }
            $cur2->finish();
        } elsif ( $img_oid && isInt($img_oid) ) {
            my $sql2 = qq{
		select add_date, 
		to_char(add_date, 'DD-MON-YYYY HH24:MI:SS'), 
		floor(add_date - to_date('01-JAN-2000 00:00:00', 'DD-MON-YYYY HH24:MI:SS')), 
		is_public, release_date,
		to_char(release_date, 'DD-MON-YYYY HH24:MI:SS')
		    from taxon where taxon_oid = $img_oid
		};
            my $cur2 = $dbh2->prepare($sql2) || die "Cannot prepare $sql2\n";
            $cur2->execute() || die "Cannot execute $sql2\n";
            ( $load_date, $load_date_str, $load_date_diff, $public_flag, $release_date, $release_date_str ) =
              $cur2->fetchrow();
            if ( $public_flag eq 'Yes' ) {
                $public_flag = 'Public';
            } else {
                $public_flag = 'Private';
            }
            $cur2->finish();
        }

        my $to_output = 1;
        if ( $proj_attr eq 'loading_date' && $attr_op && $date1 ) {
            if ($load_date) {
                my $comp = compareDate( $load_date, $date1 );

                if ( $attr_op eq '>' && $comp <= 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq '>=' && $comp < 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq '<' && $comp >= 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq '<=' && $comp > 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq 'between' ) {
                    if ( $comp < 0 ) {
                        $to_output = 0;
                    }
                    if ($date2) {
                        if ( compareDate( $load_date, $date2 ) > 0 ) {
                            $to_output = 0;
                        }
                    }
                }
            } else {

                # not loaded
                $to_output = 0;
            }
        }

        if ( $proj_attr eq 'release_date' && $attr_op && $date1 ) {
            if ($release_date) {
                my $comp = compareDate( $release_date, $date1 );

                if ( $attr_op eq '>' && $comp <= 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq '>=' && $comp < 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq '<' && $comp >= 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq '<=' && $comp > 0 ) {
                    $to_output = 0;
                } elsif ( $attr_op eq 'between' ) {
                    if ( $comp < 0 ) {
                        $to_output = 0;
                    }
                    if ($date2) {
                        if ( compareDate( $release_date, $date2 ) > 0 ) {
                            $to_output = 0;
                        }
                    }
                }
            } else {

                # not released
                $to_output = 0;
            }
        }

        if ($to_output) {
            my $diff = "";
            if ( $submit_date && $submit_date_diff && $load_date && $load_date_diff ) {
                require Submission;
                $diff =
                  Submission::CalculateDateInterval( $submit_date, $load_date_str, $load_date_diff - $submit_date_diff );
            }

            my $diff_gc1 = "";
            if ( $submit_date && $submit_date_diff && $gc1_date && $gc1_date_diff ) {
                require Submission;
                $diff_gc1 =
                  Submission::CalculateDateInterval( $submit_date, $gc1_date_str, $gc1_date_diff - $submit_date_diff );
            }

            my $diff_gc2 = "";
            if ( $submit_date && $submit_date_diff && $gc2_date && $gc2_date_diff ) {
                require Submission;
                $diff_gc2 =
                  Submission::CalculateDateInterval( $submit_date, $gc2_date_str, $gc2_date_diff - $submit_date_diff );
            }

            my $diff_vc1 = "";
            if ( $submit_date && $submit_date_diff && $vc1_date && $vc1_date_diff ) {
                require Submission;
                $diff_vc1 =
                  Submission::CalculateDateInterval( $submit_date, $vc1_date_str, $vc1_date_diff - $submit_date_diff );
            }

            my $diff_vc2 = "";
            if ( $load_date && $load_date_diff && $vc1_date && $vc1_date_diff ) {
                require Submission;
                $diff_vc2 =
                  Submission::CalculateDateInterval( $vc1_date_str, $load_date_str, $load_date_diff - $vc1_date_diff );
            }

            my $diff_ann1 = "";
            if ( $submit_date && $submit_date_diff && $ann1_date && $ann1_date_diff ) {
                require Submission;
                $diff_ann1 =
                  Submission::CalculateDateInterval( $submit_date, $ann1_date_str, $ann1_date_diff - $submit_date_diff );
            }

            my $diff_ann2 = "";
            if ( $load_date && $load_date_diff && $ann1_date && $ann1_date_diff ) {
                require Submission;
                $diff_ann2 =
                  Submission::CalculateDateInterval( $ann1_date_str, $load_date_str, $load_date_diff - $ann1_date_diff );
            }

            my $diff_fin1 = "";
            if ( $submit_date && $submit_date_diff && $fin1_date && $fin1_date_diff ) {
                require Submission;
                $diff_fin1 =
                  Submission::CalculateDateInterval( $submit_date, $fin1_date_str, $fin1_date_diff - $submit_date_diff );
            }

            my $diff_fin2 = "";
            if ( $load_date && $load_date_diff && $fin1_date && $fin1_date_diff ) {
                require Submission;
                $diff_fin2 =
                  Submission::CalculateDateInterval( $fin1_date_str, $load_date_str, $load_date_diff - $fin1_date_diff );
            }

            print "$submission_id\t$project_oid\t" . "$domain\t$project_name\t";

            if ( $contact_oid == 312 ) {
                print $predicted_gene_count{$submission_id} . "\t";
            }

            if ( $new_submission_status_h{$submission_id} ) {
                $submission_status .= " => " . $new_submission_status_h{$submission_id};
            }

            my $its_ana_id       = "";
            my $ap_prod_name     = "";
            my $its_proposal_id  = "";
            my $gpts_proposal_id = "";
            my ( $scientific_program, $funding_program, $funding_year, $bioclass, $its_spid ) =
              split( /\t/, $jgi_prog_h{$submission_id} );
            if ($analysis_project_id) {
                $its_ana_id         = $ap_its_id_h{$analysis_project_id};
                $its_spid           = $ap_spid_h{$analysis_project_id};
                $ap_prod_name       = $ap_prod_name_h{$analysis_project_id};
                $scientific_program = $ap_s_prog_h{$analysis_project_id};
                $funding_program    = $ap_f_prog_h{$analysis_project_id};
                $funding_year       = $ap_f_year_h{$analysis_project_id};
                $bioclass           = $ap_bioclass_h{$analysis_project_id};

                $its_proposal_id  = $sp_its_prop_h{$analysis_project_id};
                $gpts_proposal_id = $sp_gpts_prop_h{$analysis_project_id};
            }

            if ($uncultured_type) {
                if ($culture_type) {
                    $culture_type .= " (" . $uncultured_type . ")";
                } else {
                    $culture_type = $uncultured_type;
                }
            }

            if (    $analysis_project_id
                 && $ap_culture_type_h{$analysis_project_id} )
            {
                $culture_type = $ap_culture_type_h{$analysis_project_id};
            }

            if ( !$culture_type && $ap_proj_type_h{$analysis_project_id} ) {
                $culture_type = $ap_proj_type_h{$analysis_project_id};
            }

            my $pm_name = $pm_name_h{$analysis_project_id};

            #	    print "$seq_quality\t$seq_status\t$gold_id\t";
            print "$seq_center\t$pmo_project_id\t" .

              #		"$jgi_dir_number\t" .
              "$its_ana_id\t$its_spid\t" . "$its_proposal_id\t$gpts_proposal_id\t" . "$pm_name\t" .

              #		"$scientific_program\t$funding_program\t" .
              #		"$funding_year\t$bioclass\t" .
              "$culture_type\t" .

              #		"$scope_of_work\t" .
              "$ap_prod_name\t"
              . "$username\t$submission_status\t$img_oid\t"
              . "$submit_date\t"
              . "$gc1_date_str\t$diff_gc1\t"
              . "$gc2_date_str\t$diff_gc2\t"
              . "$vc1_date_str\t$diff_vc1\t"
              . "$ann1_date_str\t$diff_ann1\t"
              . "$load_date_str\t$diff\t"
              . "$diff_vc2\t"
              . "$fin1_date_str\t$diff_fin1\t"
              . "$public_flag\t$gpts_embargo_start_date\t"
              . "$gpts_embargo_days\t$release_date_str\n";
        }
    }    # end for loop
    $cur->finish();

    $dbh->disconnect();
    $dbh2->disconnect();
    exit 0;
}

##########################################################################
# getProjSeqCenter: get project sequencing center
##########################################################################
sub getProjSeqCenter {
    my ( $dbh, $project_oid ) = @_;

    my $seq_center = "";
    if ( !$project_oid || !isInt($project_oid) ) {
        return $seq_center;
    }

    my $sql = qq{ 
	select p.project_oid, pidl.db_name
	    from project_info p, project_info_data_links pidl
	    where p.project_oid = $project_oid
	    and p.project_oid = pidl.project_oid
	    order by 1, 2
	};

    my $cur = $dbh->prepare($sql);
    $cur->execute();

    for ( ; ; ) {
        my ( $project_oid, $s ) = $cur->fetchrow();
        last if !$project_oid;

        if ($seq_center) {
            $seq_center .= ", " . $s;
        } else {
            $seq_center = $s;
        }
    }
    $cur->finish();

    return $seq_center;
}

##########################################################################
# getSampleSeqCenter: get sample sequencing center
##########################################################################
sub getSampleSeqCenter {
    my ( $dbh, $sample_oid ) = @_;

    if ( !$sample_oid || !isInt($sample_oid) ) {
        return "";
    }

    my $sql = "select s.seq_center_name " . "from env_sample s where s.sample_oid = ?";
    my $cur = $dbh->prepare($sql);
    $cur->execute($sample_oid);

    my ($seq_center) = $cur->fetchrow();
    $cur->finish();

    return $seq_center;
}

##########################################################################
# SyncDatabase
##########################################################################
sub SyncDatabase {
    my $sync_op_type   = param('sync_op_type');
    my $sync_db_option = param('sync_db_option');

    #    print "<p>$sync_op_type\n";
    #    print "<p>$sync_db_option\n";

    if ( $sync_op_type eq 'proposal' ) {
        SyncProposalName($sync_db_option);
    } elsif ( $sync_op_type eq 'ecosystem' ) {
        SyncMetagenomeEcosystem($sync_db_option);
    } elsif ( $sync_op_type eq 'seq_center' ) {
        SyncSeqCenter($sync_db_option);
    } elsif ( $sync_op_type eq 'gold_id' ) {
        SyncGoldId($sync_db_option);
    } elsif ( $sync_op_type eq 'submit_id' ) {
        SyncGoldIdUsingSubmitId($sync_db_option);
    } elsif ( $sync_op_type eq 'img_oid' ) {
        SyncImgOidStats($sync_db_option);
    }
}

##########################################################################
# SyncProposalName
##########################################################################
sub SyncProposalName {
    my ($db_option) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<p>Preparing SQL ...\n";

    # connect to IMG-GOLD
    my $cnt     = 0;
    my @sqlList = ();
    my $dbh     = Connect_IMG('imgsg');

    # get project oid - submission id
    my $sql = qq{
	select s.submission_id, s.project_info
	    from submission s
	    where s.submission_id is not null
	    and s.project_info is not null
	    order by 1
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my %submit_proj;
    for ( ; ; ) {
        my ( $submission_id, $project_oid ) = $cur->fetchrow();
        last if !$submission_id;

        if ( $submit_proj{$project_oid} ) {
            $submit_proj{$project_oid} .= ", " . $submission_id;
        } else {
            $submit_proj{$project_oid} = $submission_id;
        }
    }
    $cur->finish();

    # get project oid - proposal name
    $sql = qq{
	select p.project_oid, p.proposal_name, p.gold_stamp_id, p.ncbi_project_id
	    from project_info p
	    where proposal_name is not null
	    order by 1
	};
    $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ( $project_oid, $proposal_name, $gold_id, $ncbi_project_id ) = $cur->fetchrow();
        last if !$project_oid;

        $proposal_name =~ s/'/''/g;    # replace ' by '' if any
        my $sql2 = "update taxon set proposal_name = '" . $proposal_name . "' where ";
        if ($gold_id) {
            my $sql3 = $sql2 . "gold_id = '$gold_id'";
            if ( $submit_proj{$project_oid} ) {
                my @submit_ids = split( /\,/, $submit_proj{$project_oid} );
                if ( scalar(@submit_ids) <= 1000 ) {
                    $sql3 .= " or submission_id in (" . $submit_proj{$project_oid} . ") ";
                } else {
                    my $cnt2    = 0;
                    my $id_list = "";
                    for my $id2 (@submit_ids) {
                        if ($id_list) {
                            $id_list .= "," . $id2;
                        } else {
                            $id_list = $id2;
                        }
                        $cnt2++;
                        if ( $cnt2 >= 1000 ) {
                            $sql3 .= " or submission_id in (" . $id_list . ") ";
                            $cnt2    = 0;
                            $id_list = "";
                        }
                    }
                }
            }

            push @sqlList, ($sql3);
        } elsif ( $submit_proj{$project_oid} ) {
            my @submit_ids = split( /\,/, $submit_proj{$project_oid} );
            my $sql3       = "";
            if ( scalar(@submit_ids) <= 1000 ) {
                $sql3 = $sql2 . " submission_id in (" . $submit_proj{$project_oid} . ") ";
            } else {
                my $cnt2    = 0;
                my $id_list = "";
                for my $id2 (@submit_ids) {
                    if ($id_list) {
                        $id_list .= "," . $id2;
                    } else {
                        $id_list = $id2;
                    }
                    $cnt2++;
                    if ( $cnt2 >= 1000 ) {
                        if ($sql3) {
                            $sql3 .= " or submission_id in (" . $id_list . ") ";
                        } else {
                            $sql3 = $sql2 . " submission_id in (" . $id_list . ") ";
                        }
                        $cnt2    = 0;
                        $id_list = "";
                    }
                }

                push @sqlList, ($sql3);
            }
        }

        if ($ncbi_project_id) {
            my $sql4 =
                $sql2
              . " gold_id is null and submission_id is null "
              . "and (ncbi_project_id = $ncbi_project_id "
              . "or refseq_project_id = $ncbi_project_id "
              . "or gbk_project_id = $ncbi_project_id)";
            push @sqlList, ($sql4);
        }

        $cnt++;
    }
    $cur->finish();

    $dbh->disconnect();

    my @dbs = ($db_option);
    if ( $db_option eq 'all' ) {

        #	@dbs = ( 'imgtaxon', 'imgw', 'imger', 'imgmer', 'imgi' );
        @dbs = ( 'imgtaxon', 'imger' );
    }

    $cnt = 0;
    for my $db (@dbs) {
        print "<p>Updating $db ...\n";

        my $dbh2 = Connect_IMG($db);
        $dbh2->{AutoCommit} = 0;

        my $last_sql = 0;

        # perform database update
        eval {
            for my $sql2 (@sqlList) {
                $last_sql++;
                my $cur2 = $dbh2->prepare($sql2)
                  || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");

                $cnt++;
                if ( ( $cnt % 1000 ) == 0 ) {
                    print "<p>$cnt updates ...\n";
                }
            }
        };

        print "<p>$cnt updates ...\n";

        if ($@) {
            $dbh2->rollback();
        } else {
            $dbh2->commit();
        }
        $dbh2->disconnect();
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:UserTool" value="OK" class="smbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# SyncMetagenomeEcosystem
##########################################################################
sub SyncMetagenomeEcosystem {
    my ($db_option) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    if (    $db_option eq 'imgw'
         || $db_option eq 'imger'
         || $db_option eq 'imgi' )
    {
        print "<h4>This option is for metagenomes only.</h4>\n";
        print end_form();
        return;
    }

    print "<p>Preparing SQL ...\n";

    # connect to IMG-GOLD
    my $cnt     = 0;
    my @sqlList = ();
    my $dbh     = Connect_IMG('imgsg');

    # get sample oid - submission id
    my $sql = qq{
	select s.submission_id, s.sample_oid
	    from submission s
	    where s.submission_id is not null
	    and s.sample_oid is not null
	    order by 1
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my %submit_sample;
    for ( ; ; ) {
        my ( $submission_id, $sample_oid ) = $cur->fetchrow();
        last if !$submission_id;

        if ( $submit_sample{$sample_oid} ) {
            $submit_sample{$sample_oid} .= ", " . $submission_id;
        } else {
            $submit_sample{$sample_oid} = $submission_id;
        }
    }
    $cur->finish();

    # sample name and gold id
    my %sample_golds;
    my %sample_names;
    $sql = "select sample_oid, sample_display_name, gold_id from env_sample where gold_id is not null";
    $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ( $sample_oid, $sample_name, $sample_gold_id ) = $cur->fetchrow();
        last if !$sample_oid;

        $sample_golds{$sample_oid} = $sample_gold_id;
        $sample_names{$sample_oid} = $sample_name;
    }
    $cur->finish();

    # get project oid - taxon hierarchy
    $sql = qq{ 
	select p.project_oid, s.submission_id, s.sample_oid, s.subtitle, 
	p.img_oid, p.ncbi_project_id, p.domain, p.ecosystem, 
	p.ecosystem_category, p.ecosystem_type, 
	p.ecosystem_subtype, p.specific_ecosystem, p.display_name, p.gold_stamp_id 
	    from project_info p, submission s 
	    where p.project_oid = s.project_info (+) 
	    order by 1 
	};

    $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my (
             $project_oid,     $submission_id, $sample_oid, $subtitle, $img_oid,
             $ncbi_project_id, $domain,        $phylum,     $class,    $order,
             $family,          $genus,         $species,    $gold_id
        ) = $cur->fetchrow();
        last if !$project_oid;

        if ( !$submission_id && !$gold_id && !$img_oid ) {
            next;
        }

        $domain =~ s/'/''/g;    # replace ' with ''
        if ( $domain eq 'MICROBIAL' ) {
            $domain = '*Microbiome';
        }

        #	elsif ( $domain eq 'ARCHAEAL' ) {
        #	    $domain = 'Archaea';
        #	}
        #	elsif ( $domain eq 'BACTERIAL' ) {
        #	    $domain = 'Bacteria';
        #	}
        #	elsif ( $domain eq 'EUKARYAL' ) {
        #	    $domain = 'Eukaryota';
        #	}
        #	elsif ( $domain eq 'VIRUS' ) {
        #	    $domain = 'Viruses';
        #	}
        else {
            next;
        }

        my $cnt2 = 0;
        for my $v ( $phylum, $class, $order, $family, $genus ) {
            if ( !blankStr($v) && $v ne 'unclassified' ) {
                $cnt2++;
            }
        }
        if ( $cnt2 == 0 ) {

            # no info
            next;
        }

        $phylum  = strTrim($phylum);
        $class   = strTrim($class);
        $order   = strTrim($order);
        $family  = strTrim($family);
        $genus   = strTrim($genus);
        $species = strTrim($species);
        $gold_id = strTrim($gold_id);

        $phylum  =~ s/'/''/g;    # replace ' with ''
        $class   =~ s/'/''/g;    # replace ' with ''
        $order   =~ s/'/''/g;    # replace ' with ''
        $family  =~ s/'/''/g;    # replace ' with ''
        $genus   =~ s/'/''/g;    # replace ' with ''
        $species =~ s/'/''/g;    # replace ' with ''

        my $new_name       = "";
        my $sample_gold_id = "";
        if ($sample_oid) {

            # sample gold id
            if ( $sample_golds{$sample_oid} ) {
                $sample_gold_id = $sample_golds{$sample_oid};
                $sample_gold_id =~ s/'/''/g;    # replace ' with ''
            }

            # sample name
            if ( $sample_names{$sample_oid} ) {
                $new_name = $sample_names{$sample_oid};
                if ($subtitle) {
                    $new_name .= " (" . $subtitle . ")";
                }
                $new_name =~ s/'/''/g;          # replace ' with ''
            }
        }

        if ( blankStr($phylum) ) {
            $phylum = 'unclassified';
        }
        if ( blankStr($class) ) {
            $class = 'unclassified';
        }
        if ( blankStr($order) ) {
            $order = 'unclassified';
        }
        if ( blankStr($family) ) {
            $family = 'unclassified';
        }
        if ( blankStr($genus) ) {
            $genus = 'unclassified';
        }

        my $sql2 =
            "update taxon set phylum = '$phylum', "
          . "ir_class = '$class', "
          . "ir_order = '$order', "
          . "family = '$family', "
          . "genus = '$genus', "
          . "species = '$species'";

        if ($sample_gold_id) {
            $sql2 .= ", sample_gold_id = '$sample_gold_id'";
        }
        if ($new_name) {
            $sql2 .= ", taxon_display_name = '$new_name'";
        }

        $sql2 .= " where ";

        if ($submission_id) {
            my $sql3 = $sql2 . " submission_id = $submission_id";
            push @sqlList, ($sql3);
        } elsif ($gold_id) {
            my $sql3 = $sql2 . "(gold_id = '$gold_id' and submission_id is null)";
            if ( $submit_sample{$sample_oid} ) {
                my @submit_ids = split( /\,/, $submit_sample{$sample_oid} );
                if ( scalar(@submit_ids) <= 1000 ) {
                    $sql3 .= " or submission_id in (" . $submit_sample{$sample_oid} . ") ";
                } else {
                    my $cnt2    = 0;
                    my $id_list = "";
                    for my $id2 (@submit_ids) {
                        if ($id_list) {
                            $id_list .= "," . $id2;
                        } else {
                            $id_list = $id2;
                        }
                        $cnt2++;
                        if ( $cnt2 >= 1000 ) {
                            $sql3 .= " or submission_id in (" . $id_list . ") ";
                            $cnt2    = 0;
                            $id_list = "";
                        }
                    }
                }
            }

            push @sqlList, ($sql3);
        } elsif ( $submit_sample{$sample_oid} ) {
            my @submit_ids = split( /\,/, $submit_sample{$sample_oid} );
            my $sql3       = "";
            if ( scalar(@submit_ids) <= 1000 ) {
                $sql3 = $sql2 . " submission_id in (" . $submit_sample{$sample_oid} . ") ";
            } else {
                my $cnt2    = 0;
                my $id_list = "";
                for my $id2 (@submit_ids) {
                    if ($id_list) {
                        $id_list .= "," . $id2;
                    } else {
                        $id_list = $id2;
                    }
                    $cnt2++;
                    if ( $cnt2 >= 1000 ) {
                        if ($sql3) {
                            $sql3 .= " or submission_id in (" . $id_list . ") ";
                        } else {
                            $sql3 = $sql2 . " submission_id in (" . $id_list . ") ";
                        }
                        $cnt2    = 0;
                        $id_list = "";
                    }
                }

                push @sqlList, ($sql3);
            }
        }

        if ($ncbi_project_id) {
            my $sql4 =
                $sql2
              . " gold_id is null and submission_id is null "
              . "and (ncbi_project_id = $ncbi_project_id "
              . "or refseq_project_id = $ncbi_project_id "
              . "or gbk_project_id = $ncbi_project_id)";
            push @sqlList, ($sql4);
        }

        if ( $img_oid && !$gold_id && !$ncbi_project_id ) {
            my $sql5 = $sql2 . " taxon_oid = $img_oid";
            push @sqlList, ($sql5);
        }

        $cnt++;
    }
    $cur->finish();

    $dbh->disconnect();

    #    for my $sql6 ( @sqlList ) {
    #	print "<p>SQL: $sql6\n";
    #    }
    #    return;

    my @dbs = ($db_option);
    if ( $db_option eq 'all' ) {
        @dbs = ('imgtaxon');
    }

    $cnt = 0;
    for my $db (@dbs) {
        print "<p>Updating $db ...\n";

        my $dbh2 = Connect_IMG($db);
        $dbh2->{AutoCommit} = 0;

        my $last_sql = 0;

        # perform database update
        eval {
            for my $sql2 (@sqlList) {
                $last_sql++;
                my $cur2 = $dbh2->prepare($sql2)
                  || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");

                $cnt++;
                if ( ( $cnt % 1000 ) == 0 ) {
                    print "<p>$cnt updates ...\n";
                }
            }
        };

        print "<p>$cnt updates ...\n";

        if ($@) {
            $dbh2->rollback();
        } else {
            $dbh2->commit();
        }
        $dbh2->disconnect();
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:UserTool" value="OK" class="smbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# SyncSeqCenter
##########################################################################
sub SyncSeqCenter {
    my ($db_option) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<p>Preparing SQL ...\n";

    # connect to IMG-GOLD
    my $cnt     = 0;
    my @sqlList = ();
    my $dbh     = Connect_IMG('imgsg');

    # get project oid - submission id
    my $sql = qq{
	select s.submission_id, s.project_info
	    from submission s
	    where s.submission_id is not null
	    and s.project_info is not null
	    order by 1
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my %submit_proj;
    for ( ; ; ) {
        my ( $submission_id, $project_oid ) = $cur->fetchrow();
        last if !$submission_id;

        if ( $submit_proj{$project_oid} ) {
            $submit_proj{$project_oid} .= ", " . $submission_id;
        } else {
            $submit_proj{$project_oid} = $submission_id;
        }
    }
    $cur->finish();

    # get project oid - seq_center
    $sql = qq{ 
	select pidl.project_oid, pidl.db_name
	    from project_info_data_links pidl
	    where pidl.link_type = 'Seq Center' 
	    and pidl.db_name is not null
	    order by 1, 2
	};

    $cur = $dbh->prepare($sql);
    $cur->execute();
    my %seq_center;
    for ( ; ; ) {
        my ( $project_oid, $center_name ) = $cur->fetchrow();
        last if !$project_oid;

        if ( $seq_center{$project_oid} ) {
            my $new_s = $seq_center{$project_oid} . ", " . $center_name;
            $seq_center{$project_oid} = $new_s;
        } else {
            $seq_center{$project_oid} = $center_name;
        }
    }
    $cur->finish();

    $sql = qq{ 
	select p.project_oid, p.gold_stamp_id, p.ncbi_project_id
        from project_info p
        order by 1
    };

    $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ( $project_oid, $gold_id, $ncbi_project_id ) = $cur->fetchrow();
        last if !$project_oid;

        if ( !$seq_center{$project_oid} ) {

            # no sequencing center
            next;
        }

        my $c_name = $seq_center{$project_oid};
        if ( length($c_name) > 500 ) {
            $c_name = substr( $c_name, 0, 500 );
        }
        $c_name =~ s/'/''/g;    # replace ' by '' if any
        my $sql2 = "update taxon set seq_center = '" . $c_name . "' where ";
        if ($gold_id) {
            my $sql3 = $sql2 . "gold_id = '$gold_id'";
            if ( $submit_proj{$project_oid} ) {
                my @submit_ids = split( /\,/, $submit_proj{$project_oid} );
                if ( scalar(@submit_ids) <= 1000 ) {
                    $sql3 .= " or submission_id in (" . $submit_proj{$project_oid} . ") ";
                } else {
                    my $cnt2    = 0;
                    my $id_list = "";
                    for my $id2 (@submit_ids) {
                        if ($id_list) {
                            $id_list .= "," . $id2;
                        } else {
                            $id_list = $id2;
                        }
                        $cnt2++;
                        if ( $cnt2 >= 1000 ) {
                            $sql3 .= " or submission_id in (" . $id_list . ") ";
                            $cnt2    = 0;
                            $id_list = "";
                        }
                    }
                }
            }

            push @sqlList, ($sql3);
        } elsif ( $submit_proj{$project_oid} ) {
            my @submit_ids = split( /\,/, $submit_proj{$project_oid} );
            my $sql3       = "";
            if ( scalar(@submit_ids) <= 1000 ) {
                $sql3 = $sql2 . " submission_id in (" . $submit_proj{$project_oid} . ") ";
            } else {
                my $cnt2    = 0;
                my $id_list = "";
                for my $id2 (@submit_ids) {
                    if ($id_list) {
                        $id_list .= "," . $id2;
                    } else {
                        $id_list = $id2;
                    }
                    $cnt2++;
                    if ( $cnt2 >= 1000 ) {
                        if ($sql3) {
                            $sql3 .= " or submission_id in (" . $id_list . ") ";
                        } else {
                            $sql3 = $sql2 . " submission_id in (" . $id_list . ") ";
                        }
                        $cnt2    = 0;
                        $id_list = "";
                    }
                }
            }

            push @sqlList, ($sql3);
        }

        if ($ncbi_project_id) {
            my $sql4 =
                $sql2
              . " gold_id is null and submission_id is null "
              . "and (ncbi_project_id = $ncbi_project_id "
              . "or refseq_project_id = $ncbi_project_id "
              . "or gbk_project_id = $ncbi_project_id)";
            push @sqlList, ($sql4);
        }

        $cnt++;
    }
    $cur->finish();

    $dbh->disconnect();

    #    for my $sql5 ( @sqlList ) {
    #	print "<p>*** SQL: $sql5\n";
    #    }
    #    return;

    my @dbs = ($db_option);
    if ( $db_option eq 'all' ) {

        #	@dbs = ( 'imgtaxon', 'imgw', 'imger', 'imgmer', 'imgi' );
        @dbs = ( 'imgtaxon', 'imger' );
    }

    $cnt = 0;
    for my $db (@dbs) {
        print "<p>Updating $db ...\n";

        my $dbh2 = Connect_IMG($db);
        $dbh2->{AutoCommit} = 0;

        my $last_sql = 0;

        # perform database update
        eval {
            for my $sql2 (@sqlList) {
                $last_sql++;
                my $cur2 = $dbh2->prepare($sql2)
                  || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");

                $cnt++;
                if ( ( $cnt % 1000 ) == 0 ) {
                    print "<p>$cnt updates ...\n";
                }
            }
        };

        print "<p>$cnt updates ...\n";

        if ($@) {
            $dbh2->rollback();
        } else {
            $dbh2->commit();
        }
        $dbh2->disconnect();
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:UserTool" value="OK" class="smbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# SyncGoldId
##########################################################################
sub SyncGoldId {
    my ($db_option) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<p>Preparing SQL ...\n";

    # connect to IMG-GOLD
    my $cnt     = 0;
    my @sqlList = ();
    my $dbh     = Connect_IMG('imgsg');

    # get ncbi pid - gold id
    my $sql = qq{
	select p.ncbi_project_id, p.gold_stamp_id
	    from project_info p
	    where p.ncbi_project_id is not null
	    and p.gold_stamp_id is not null
	    order by 1
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my %pid_goldid;
    for ( ; ; ) {
        my ( $pid, $gold_id ) = $cur->fetchrow();
        last if !$pid;

        $pid_goldid{$pid} = $gold_id;
    }
    $cur->finish();

    $dbh->disconnect();

    for my $k ( keys %pid_goldid ) {
        my $gold_id = $pid_goldid{$k};
        $gold_id =~ s/'/''/g;    # replace ' with ''
        my $sql2 =
"update taxon set gold_id = '$gold_id' where ncbi_project_id = $k or refseq_project_id = $k or gbk_project_id = $k";
        push @sqlList, ($sql2);
    }

    my @dbs = ($db_option);
    if ( $db_option eq 'all' ) {

        #	@dbs = ( 'imgtaxon', 'imgw', 'imger', 'imgmer', 'imgi' );
        @dbs = ( 'imgtaxon', 'imger' );
    }

    $cnt = 0;
    for my $db (@dbs) {
        print "<p>Updating $db ...\n";

        my $dbh2 = Connect_IMG($db);
        $dbh2->{AutoCommit} = 0;

        my $last_sql = 0;

        # perform database update
        eval {
            for my $sql2 (@sqlList) {
                $last_sql++;
                my $cur2 = $dbh2->prepare($sql2)
                  || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");

                $cnt++;
                if ( ( $cnt % 1000 ) == 0 ) {
                    print "<p>$cnt updates ...\n";
                }
            }
        };

        print "<p>$cnt updates ...\n";

        if ($@) {
            $dbh2->rollback();
        } else {
            $dbh2->commit();
        }
        $dbh2->disconnect();
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:UserTool" value="OK" class="smbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# SyncGoldIdUsingSubmitId
##########################################################################
sub SyncGoldIdUsingSubmitId {
    my ($db_option) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    print "<p>Preparing SQL ...\n";

    # connect to IMG-GOLD
    my $cnt     = 0;
    my @sqlList = ();
    my $dbh     = Connect_IMG('imgsg');

    # get ncbi pid - gold id
    my $sql = qq{
	select s.submission_id, p.gold_stamp_id, e.gold_id
	    from submission s, project_info p, env_sample e
	    where s.project_info = p.project_oid
	    and p.gold_stamp_id is not null
	    and s.sample_oid = e.sample_oid (+)
	    order by 1
	};
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    for ( ; ; ) {
        my ( $submit_id, $gold_id, $sample_gold_id ) = $cur->fetchrow();
        last if !$submit_id;

        if ( !$gold_id ) {
            next;
        }

        $gold_id =~ s/'/''/g;    # replace ' with ''
        my $sql2 = "update taxon set gold_id = '$gold_id'";
        if ($sample_gold_id) {
            $sample_gold_id =~ s/'/''/g;    # replace ' with ''
            $sql2 .= ", sample_gold_id = '$sample_gold_id'";
        }
        $sql2 .= " where submission_id = $submit_id";
        push @sqlList, ($sql2);
    }
    $cur->finish();

    $dbh->disconnect();

    #    for my $sql5 ( @sqlList ) {
    #	print "<p>*** SQL: $sql5\n";
    #    }
    #    return;

    my @dbs = ($db_option);
    if ( $db_option eq 'all' ) {

        #	@dbs = ( 'imgtaxon', 'imgw', 'imger', 'imgmer', 'imgi' );
        @dbs = ( 'imgtaxon', 'imger' );
    }

    $cnt = 0;
    for my $db (@dbs) {
        print "<p>Updating $db ...\n";

        my $dbh2 = Connect_IMG($db);
        $dbh2->{AutoCommit} = 0;

        my $last_sql = 0;

        # perform database update
        eval {
            for my $sql2 (@sqlList) {
                $last_sql++;
                my $cur2 = $dbh2->prepare($sql2)
                  || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");

                $cnt++;
                if ( ( $cnt % 1000 ) == 0 ) {
                    print "<p>$cnt updates ...\n";
                }
            }
        };

        print "<p>$cnt updates ...\n";

        if ($@) {
            $dbh2->rollback();
        } else {
            $dbh2->commit();
        }
        $dbh2->disconnect();
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:UserTool" value="OK" class="smbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# SyncImgOidStats
##########################################################################
sub SyncImgOidStats {
    my ($db_option) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    my @dbs = ($db_option);
    if ( $db_option eq 'all' ) {

        #	@dbs = ( 'imgtaxon', 'imgw', 'imger', 'imgmer', 'imgi' );
        @dbs = ( 'imgtaxon', 'imger' );
    }

    my $cnt = 0;
    for my $db (@dbs) {
        print "<p>Refreshing from $db ...\n";

        my $cnt     = 0;
        my @sqlList = ();
        my $dbh     = Connect_IMG($db);

        my $sql = qq{ 
	    select t.taxon_oid, t.gold_id, t.sample_gold_id, t.submission_id,
	    ts.total_bases, ts.total_gene_count, ts.gc_percent 
		from taxon t, taxon_stats ts
		where t.taxon_oid = ts.taxon_oid 
		and t.obsolete_flag = 'No'
		and t.is_public = 'Yes' 
	    };

        my $cur = $dbh->prepare($sql);
        $cur->execute();
        for ( ; ; ) {
            my ( $taxon_oid, $gold_id, $sample_gold_id, $submission_id, $total_bases, $total_gene_count, $gc_perc ) =
              $cur->fetchrow();
            last if !$taxon_oid;

            if ( !$total_bases ) {
                $total_bases = '0';
            }
            if ( !$total_gene_count ) {
                $total_gene_count = '0';
            }
            if ( !$gc_perc ) {
                $gc_perc = '0';
            } else {
                $gc_perc *= 100.0;
            }

            if ($sample_gold_id) {
                my $sql2 =
                    "update env_sample set img_oid = $taxon_oid, "
                  . "est_size = $total_bases, "
                  . "gene_count = $total_gene_count, "
                  . "gc_perc = $gc_perc where gold_id = '$sample_gold_id'";
                push @sqlList, ($sql2);
            } elsif ( $gold_id =~ /Gi/ || $gold_id =~ /Gc/ ) {
                my $sql2 =
                    "update project_info set img_oid = $taxon_oid, "
                  . "est_size = $total_bases, "
                  . "gene_count = $total_gene_count, "
                  . "gc_perc = $gc_perc where gold_stamp_id = '$gold_id'";
                push @sqlList, ($sql2);
            }
        }
        $cur->finish();

        #	for my $sql5 ( @sqlList ) {
        #	    print "<p>*** SQL: $sql5\n";
        #	}
        #	next;

        my $dbh2 = Connect_IMG('imgsg');
        $dbh2->{AutoCommit} = 0;

        my $last_sql = 0;

        # perform database update
        eval {
            for my $sql2 (@sqlList) {
                $last_sql++;
                my $cur2 = $dbh2->prepare($sql2)
                  || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
                $cur2->execute()
                  || dienice("execSql: cannot execute: $DBI::errstr\n");

                $cnt++;
                if ( ( $cnt % 1000 ) == 0 ) {
                    print "<p>$cnt updates ...\n";
                }
            }
        };

        print "<p>$cnt updates ...\n";

        if ($@) {
            $dbh2->rollback();
        } else {
            $dbh2->commit();
        }
        $dbh2->disconnect();
    }    # end for db

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:UserTool" value="OK" class="smbutton" />';

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# ViewUserDistribution
##########################################################################
sub ViewUserDistribution {
    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    if ( $isAdmin ne 'Yes' ) {
        return;
    }

    print "<h1>IMG User Distribution</h1>\n";

    #    GoogleMapUser2();

    if ($use_yahoo_table) {
        ViewUserCountry_yui();
    } else {
        ViewUserCountry();
    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

###############################################################################
sub GoogleMapUser {
    my $dbh = WebFunctions::Connect_IMG_Contact();

    #my $gmapkey = getGoogleMapsKey();

    my @recs;
    my $sql =
"select c.contact_oid, c.username, c.country, t.longitude, t.latitude from contact c, countrycv t where c.country = t.cv_term and t.longitude is not null and t.latitude is not null order by 5, 4, 3, 2";
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my $cnt = 0;
    for ( ; ; ) {
        my ( $contact_oid, $username, $country, $longitude, $latitude ) = $cur->fetchrow_array();
        if ( !$contact_oid ) {
            last;
        }

        if ( $username =~ /MISSING/ ) {
            next;
        }

        $country = convertCountry($country);

        $latitude  = strTrim($latitude);
        $longitude = strTrim($longitude);
        push( @recs, "$contact_oid\t$username\t$country\t$latitude\t$longitude\t0" );
    }
    $cur->finish();
    $dbh->disconnect();

    my $recs_aref = \@recs;

    my $tmp = $#$recs_aref + 1;
    print qq{
        <p>
        $tmp users. <br/>
        Some users may not shown via Google Maps because of bad location coordinates. 
        See skip count above. <br/>
        Map pins represent location counts. Some pins may have multiple users.
        };

    my $url = "$main_cgi?section=UserTool&page=showContactInfo&selected_contact_oid=";

    print <<EOF;
<link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="$base_url/googlemap.js"></script>
<script type="text/javascript" src="$base_url/markerclusterer.js"></script>

    <div id="map_canvas" style="width: 1000px; height: 700px; position: relative;"></div>            
            
    <script type="text/javascript">
        var map = createMap(2, 0, 0);    
EOF

    my $count      = 0;
    my $skip_users = 0;
    my $last_lat   = "";
    my $last_lon   = "";
    my $last_name  = "";

    #my $last_geo = "";
    my $info     = "";
    my $cnt_info = 0;
    foreach my $line (@$recs_aref) {
        my ( $contact_oid, $name, $geo_location, $latitude, $longitude, $altitude ) =
          split( /\t/, $line );

        # webLog ("$count, $contact_oid\n");

        my $tmp_geo_location = escHtml($geo_location);
        my $tmp_altitude     = escHtml($altitude);

        # add geo location check too ? maybe not
        if ( ( $last_lat ne $latitude ) || ( $last_lon ne $longitude ) ) {

            #            if ( $info ne "" ) {
            if ($cnt_info) {

                # clean lat and long remove " ' , etc
                my $clat  = $last_lat;    # convertLatLong($last_lat);
                my $clong = $last_lon;    # convertLatLong($last_lon);
                print qq{
                    var contentString = "$last_name: $cnt_info </div>";
                    addMarker(map, $clat, $clong, '$last_name', contentString);
                };
                $count++;
            }

            # new point
            $info     = "";
            $cnt_info = 0;

            # clean lat and long remove " ' , etc
            my $clat  = $latitude;     # convertLatLong($latitude);
            my $clong = $longitude;    # convertLatLong($longitude);

            # some data is a space not a null
            if ( $clat eq "" || $clong eq "" ) {
                $skip_users++;
                next;
            }

            $info = "<h1>$tmp_geo_location</h1> <div><p>$latitude, $longitude<br/>$tmp_altitude";
            $info .= "<br/><a href='$url$contact_oid'>$name</a>";

            $cnt_info++;

        } else {
            $info .= "<br/><a href='$url$contact_oid'>$name</a>";
            $cnt_info++;
        }
        $last_lat = $latitude;
        $last_lon = $longitude;

        # $last_name = CGI::escape($name);
        $last_name = escapeHTML($geo_location);
    }

    # last recrod
    #    if ( $#$recs_aref > -1 && $info ne "" ) {
    if ( $#$recs_aref > -1 && $cnt_info ) {
        my $clat  = $last_lat;    # convertLatLong($last_lat);
        my $clong = $last_lon;    # convertLatLong($last_lon);
        print qq{
                    var contentString = "$last_name: $cnt_info";
                    addMarker(map, $clat, $clong, '$last_name', contentString);
                };
        $count++;
    }

    print qq{ 
        cluster(map); 
        </script>  
    };

    print "<p>$count Locations, $skip_users skipped.\n";
}

sub convertLatLong {
    my ($s) = @_;

    return $s;
}

sub GoogleMapUser2 {
    my $dbh = WebFunctions::Connect_IMG_Contact();

    #my $gmapkey = getGoogleMapsKey();

    print "<h2>Map 2</h2>\n";

    my @recs;
    my $sql =
"select c.contact_oid, c.username, c.country, t.longitude, t.latitude from contact c, countrycv t where c.country = t.cv_term and t.longitude is not null and t.latitude is not null order by 5, 4, 3, 2";
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    my $cnt = 0;
    for ( ; ; ) {
        my ( $contact_oid, $username, $country, $longitude, $latitude ) = $cur->fetchrow_array();
        if ( !$contact_oid ) {
            last;
        }

        if ( $username =~ /MISSING/ ) {
            next;
        }

        $country = convertCountry($country);

        $latitude  = strTrim($latitude);
        $longitude = strTrim($longitude);
        push( @recs, "$contact_oid\t$username\t$country\t$latitude\t$longitude\t0" );
    }
    $cur->finish();
    $dbh->disconnect();

    my $gmapkey = getGoogleMapsKey();

    my $recs_aref = \@recs;

    my $tmp = $#$recs_aref + 1;
    print qq{
        <p>
        $tmp users. <br/>
        Some users may not shown via Google Maps because of bad location coordinates. 
        See skip count above. <br/>
        Map pins represent location counts. Some pins may have multiple users.
        };

    my $url = "$main_cgi?section=UserTool&page=showContactInfo&selected_contact_oid=";

    print <<EOF;
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=$gmapkey"
            type="text/javascript"></script>

    <div id="map" style="width: 1000px; height: 700px"></div>            
            
    <script type="text/javascript">
    
    //<![CDATA[
      if (GBrowserIsCompatible()) {
            var map = new GMap2(document.getElementById("map"));
            map.addControl(new GLargeMapControl());
            map.addControl(new GMapTypeControl());
            map.addControl(new GScaleControl());

            var gpoint = new GLatLng(0,10);
            map.setCenter(gpoint, 2);
EOF

    my $count    = 0;
    my $last_lat = "";
    my $last_lon = "";

    my $last_geo = "";
    my $info     = "";
    my $cnt_info = 0;
    foreach my $line (@$recs_aref) {
        my ( $taxon_oid, $name, $geo_location, $latitude, $longitude, $altitude ) =
          split( /\t/, $line );

        my $tmp_geo_location = escHtml($geo_location);
        my $tmp_altitude     = escHtml($altitude);

        # TODO add geo location check too ? maybe not
        if ( $last_lat ne $latitude && $last_lon ne $longitude ) {

            #            if ( $info ne "" ) {
            if ($cnt_info) {
                print qq{
                GEvent.addListener(gmarker$count, "click", function() {
                gmarker$count.openInfoWindowHtml("$last_geo: $cnt_info");
                });            
                };
                $count++;
            }

            # new point
            $info     = "";
            $cnt_info = 0;

            # TODO clean lat and long remove " ' , etc
            my $clat  = convertLatLong($latitude);
            my $clong = convertLatLong($longitude);

            # some data is a space not a null
            next if ( $clat eq "" || $clong eq "" );

            #            print qq{
            #                var baseIcon = new GIcon(G_DEFAULT_ICON);
            #                baseIcon.shadow = "http://www.google.com/mapfiles/markerA.png";
            #                baseIcon.iconSize = new GSize(20, 34);
            #                baseIcon.shadowSize = new GSize(37, 34);
            #                baseIcon.iconAnchor = new GPoint(9, 34);
            #                baseIcon.infoWindowAnchor = new GPoint(9, 2);
            #
            #                var letteredIcon = new GIcon(baseICon);
            #                letteredIcon.image = "http://www.google.com/mapfiles/markerA.png";
            #                var markerOptions = { icon:baseIcon };
            #                var gmarker$count = new GMarker(new GLatLng($clat,$clong));
            #            };

            print qq{
                var baseIcon = new GIcon(G_DEFAULT_ICON);
                baseIcon.image = "http://www.google.com/mapfiles/markerA.png";
                var markerOptions = { icon:baseIcon };
                var gmarker$count = new GMarker(new GLatLng($clat,$clong), markerOptions);
            };

            print "map.addOverlay(gmarker$count);\n";

            $info = "$tmp_geo_location<br/>$latitude, $longitude<br/>$tmp_altitude";
            $info .= "<br/><a href='$url$taxon_oid'>$name</a>";
            $cnt_info++;

        } else {
            $info .= "<br/><a href='$url$taxon_oid'>$name</a>";
            $cnt_info++;
        }
        $last_lat = $latitude;
        $last_lon = $longitude;
        $last_geo = $geo_location;
    }

    #    if ( $#$recs_aref > -1 && $info ne "" ) {
    if ( $#$recs_aref > -1 && $cnt_info ) {
        print qq{
                GEvent.addListener(gmarker$count, "click", function() {
                gmarker$count.openInfoWindowHtml("$last_geo: $cnt_info");
                });            
        };
        $count++;
    }

    print <<EOF;
      }
    //]]>
    </script> 
EOF

    printStatusLine( "$count Locations", 2 );
}

############################################################################
# get the proper google maps key based on server name
############################################################################
sub getGoogleMapsKey {
    my $servername = $ENV{SERVER_NAME};
    my $gkeys      = $env->{google_map_keys};
    foreach my $key ( keys %$gkeys ) {
        if ( $servername =~ m/$key$/i ) {
            return $gkeys->{$key};
        }

    }
    return "";
}

##########################################################################
# ViewUserCountry (HTML table display)
##########################################################################
sub ViewUserCountry {

    my $color1 = '#eeeeee';
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Country</th>\n";
    print "<th class='img' bgcolor='$color1'>User Count</th>\n";

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql =
"select t.cv_term, count(*) from contact c, countrycv t where c.country = t.cv_term and c.username not like '%MISSING%' group by t.cv_term order by 1";
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my $cnt = 0;

    for ( ; ; ) {
        my ( $country, $count ) = $cur->fetchrow_array();
        if ( !$country ) {
            last;
        }

        $cnt++;
        if ( $cnt > 10000 ) {
            last;
        }

        if ( $cnt % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        PrintAttribute($country);

        my $url2 = "$main_cgi?section=UserTool&page=viewUserOrg&country=" . $country;
        if ($count) {
            PrintAttribute( alink( $url2, $count ) );
        } else {
            PrintAttribute($count);
        }
        print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

}

##########################################################################
# ViewUserCountry_yui (Yahoo data table display)
##########################################################################
sub ViewUserCountry_yui {

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql =
"select t.cv_term, count(*) from contact c, countrycv t where c.country = t.cv_term and c.username not like '%MISSING%' group by t.cv_term order by 1";
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my $cnt = 0;

    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();                          # sort delimiter
    $it->addColSpec( "Country",    "char asc",   "right", "", "Country" );
    $it->addColSpec( "User Count", "number asc", "right", "", "User Count" );

    for ( ; ; ) {
        my ( $country, $count ) = $cur->fetchrow_array();
        if ( !$country ) {
            last;
        }

        $cnt++;
        if ( $cnt > 10000 ) {
            last;
        }

        my $row  = $country . $sd . $country . "\t";
        my $url2 = "$main_cgi?section=UserTool&page=viewUserOrg&country=" . $country;
        if ($count) {
            $row .= $count . $sd . alink( $url2, $count ) . "\t";
        } else {
            $row .= $count . $sd . $count . "\t";
        }

        $it->addRow($row);
    }
    print "</table>\n";

    if ( $cnt > 0 ) {
        $it->printOuterTable(1);
    } else {
        print "<h5>No IMG users.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

}

#############################################################################
# dbUpdateGenomes
#############################################################################
sub dbUpdateGenomes {
    my ($type) = @_;

    # get user info
    my $contact_oid = getContactOid();
    if ( !$contact_oid ) {
        dienice("Unknown username / password");
    }

    my $isAdmin = getIsAdmin($contact_oid);

    if ( $isAdmin ne 'Yes' ) {
        return "You do not have the privilege for this operation.";
    }

    my $genome_ids = param1('genome_ids');
    $genome_ids =~ s/\s/\,/g;

    my @genomes = split( /\,/, $genome_ids );

    my $cnt         = 0;
    my $genome_list = "";
    for my $gid (@genomes) {
        if ( $cnt >= 1000 ) {
            return "You can only enter 1,000 IDs at one time.";
        }

        if ( !$gid ) {
            next;
        }
        if ( !isInt($gid) ) {
            return "Incorrect Taxon OID $gid";
        }

        $cnt++;
        if ($genome_list) {
            $genome_list .= ", " . $gid;
        } else {
            $genome_list = $gid;
        }
    }

    my $contact_name = getContactUserName();
    $contact_name =~ s/'/''/g;    # replace ' with '', if any
    my $adm_comments = param1('adm_comments');
    $adm_comments =~ s/'/''/g;    # replace ' with '', if any

    my $sql = "";
    if ( $type eq 'dbReleaseGenome' ) {
        if ( !$adm_comments ) {
            $adm_comments = "released by $contact_name";
        }
        $sql =
            "update taxon set is_public = 'Yes', release_date = sysdate, "
          . "modified_by = "
          . $contact_oid . ", "
          . "mod_date = sysdate, comments = '"
          . $adm_comments . "' "
          . "where taxon_oid in ("
          . $genome_list . ") "
          . "and is_public != 'Yes'";
    } elsif ( $type eq 'dbUnreleaseGenome' ) {
        if ( !$adm_comments ) {
            $adm_comments = "unreleased by $contact_name";
        }
        $sql =
            "update taxon set is_public = 'No', release_date = null, "
          . "modified_by = "
          . $contact_oid . ", "
          . "mod_date = sysdate, comments = '"
          . $adm_comments . "' "
          . "where taxon_oid in ("
          . $genome_list . ") "
          . "and is_public = 'Yes'";
    } elsif ( $type eq 'dbObsoleteGenome' ) {
        if ( !$adm_comments ) {
            $adm_comments = "marked obsolete by $contact_name";
        }
        $sql =
            "update taxon set obsolete_flag = 'Yes', "
          . "modified_by = "
          . $contact_oid . ", "
          . "mod_date = sysdate, comments = '"
          . $adm_comments . "' "
          . "where taxon_oid in ("
          . $genome_list . ") "
          . "and obsolete_flag = 'No'";
    } elsif ( $type eq 'dbRecoverGenome' ) {
        if ( !$adm_comments ) {
            $adm_comments = "recovered by $contact_name";
        }
        $sql =
            "update taxon set obsolete_flag = 'No', "
          . "modified_by = "
          . $contact_oid . ", "
          . "mod_date = sysdate, comments = '"
          . $adm_comments . "' "
          . "where taxon_oid in ("
          . $genome_list . ") "
          . "and obsolete_flag = 'Yes'";
    } elsif ( $type eq 'dbSetHighQuality' ) {
        my $h_q_flag = param1('h_q_flag');
        if ( !$adm_comments ) {
            if ($h_q_flag) {
                $adm_comments = "Set High Quality Flag to $h_q_flag by $contact_name";
            } else {
                $adm_comments = "Clear High Quality Flag by $contact_name";
            }
        }
        if ($h_q_flag) {
            $sql =
                "update taxon set high_quality_flag = '$h_q_flag', "
              . "modified_by = "
              . $contact_oid . ", "
              . "mod_date = sysdate, comments = '"
              . $adm_comments . "' "
              . "where taxon_oid in ("
              . $genome_list . ") ";
        } else {
            $sql =
                "update taxon set high_quality_flag = null, "
              . "modified_by = "
              . $contact_oid . ", "
              . "mod_date = sysdate, comments = '"
              . $adm_comments . "' "
              . "where taxon_oid in ("
              . $genome_list . ") "
              . "and high_quality_flag is not null";
        }
    }

    my @sqlList = ();
    if ($sql) {
        push @sqlList, ($sql);
    }

    if ( $type eq 'dbGrantAccess' ) {
        my $user_list  = param1('grant_to');
        my @user_names = split( /\,/, $user_list );
        if ( scalar(@user_names) == 0 ) {
            return "No user names are specified.";
        }

        my @user_ids = ();
        my $dbh      = dbLogin();
        my $u_sql    = "select contact_oid from contact where caliban_user_name = ?";
        my $u_cur    = $dbh->prepare($u_sql);

        for my $name (@user_names) {
            $name = strTrim($name);
            $u_cur->execute( lc($name) );
            my ($user_id) = $u_cur->fetchrow();
            $u_cur->finish();
            if ( !$user_id ) {
                $dbh->disconnect();
                return "Cannot find user $name";
            }
            push @user_ids, ($user_id);
        }
        $dbh->disconnect();

        my @genomes = split( /\,/, $genome_list );
        for my $taxon_oid (@genomes) {
            for my $user_id (@user_ids) {
                my $sql = "update contact_taxon_permissions (contact_oid, taxon_permissions) values ($user_id, $taxon_oid)";
                push @sqlList, ($sql);
            }
        }
    }

    # update database
    my $dbh2 = Connect_IMG('imgtaxon');
    $dbh2->{AutoCommit} = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for $sql (@sqlList) {
            $last_sql++;

            my $cur = $dbh2->prepare($sql)
              || dienice("execSql: cannot preparse statement: $DBI::errstr\n");
            $cur->execute()
              || dienice("execSql: cannot execute: $DBI::errstr\n");
        }
    };

    if ($@) {
        $dbh2->rollback();
        $dbh2->disconnect();
        return $last_sql;
    }

    $dbh2->commit();
    $dbh2->disconnect();

    return "";
}

###################################################################
# convertCountry: convery Caliban country to GOLD country
###################################################################
sub convertCountry {
    my ($country) = @_;

    if ( $country =~ /Taiwan/ ) {
        return "Taiwan";
    } elsif ( $country =~ /Korea/ ) {
        return "Korea";
    } elsif ( $country =~ /Iran/ ) {
        return "Iran";
    } elsif ( $country =~ /Russia/ ) {
        return "Russia";
    } elsif ( $country eq 'Viet Nam' ) {
        return "Vietnam";
    } elsif ( $country eq 'United States' || $country eq 'US' ) {
        return "USA";
    }

    return $country;
}

############################################################################
# ShowFaqMainPage
############################################################################
sub ShowFaqMainPage {

    print qq{
        FAQ has move to <a href='/docs/submission/index.html'> here </a>
        
        <script>
         window.location.replace("/docs/submission/index.html");
        </script>
         
    };

    return;

    print start_form( -name => 'showFaqMain', -method => 'post', action => "$section_cgi" );

    print "<h1>Frequently Asked Questions</h1>\n";

    print qq{
          <p><b>Q1:</b> Is there a User Guide?<br/>
          <b>A1:</b> <a href='$user_guide_url' target='view_link'>User Guide</a>.
          };

    print qq{
          <p><b>Q2:</b> When will my submission be loaded into IMG?<br/>
          <b>A2:</b> Isolate genomes are loaded about every two weeks.
          Metagenomes are processed constantly. JGI submissions have
          priority over non-JGI submissions. When there is a large
          batch of JGI metagenome submissions, external submissions
          may be delayed for a couple months.
          <p>Please note that only approved submissions will be processed.
          If your submission has approval status 'pending review,'
          please also check the GOLD Review Status of your GOLD Analysis
          Project. GOLD Review Status 'In Review' means that the project
          has not been approved by the GOLD Group.
          If your submission has approval status 'missing metadata,'
          please check <b>Metadata Requirements</b> section at the Home page.
          <u>We will not answer individual email re. when a submission
          will be loaded.</u>
          };

    print qq{
          <p><b>Q3:</b> How do I grant access to my colleagues?<br/>
          <b>A3:</b> <a href='$grant_access_url' target='view_link'>Grant Access Guide</a>.
         };

    print qq{
          <p><b>Q4:</b> How do I request my genomes to be released to the public?<br/>
          <b>A4:</b> PIs, co-PIs and submitters can now release their own 
          genomes in the <b>Submitted Datasets</b> page. 
          First select the corresponding submissions, and then scroll
          down to find the <b><i>Release/Unrelease Genomes</i></b>
          section to release. There will be some 
          delay before the released genomes are available for public 
          downloading from the JGI Genome Portal. 
          Please contact Amy Chen (imachen\@lbl.gov) if a genome has 
          been released by mistake.

          <p><font color='red'><b>Note:</b> All JGI sequenced genomes are
          controlled by JGI ITS. Please contact JGI regarding all
          genome releasing issues. Changes in IMG will be overridden by ITS.
          </font>
          };

    print qq{
          <p><b>Q5:</b> My submission has status "Errors in validation. 
          Process aborted." What should I do now?<br/>
          <b>A5:</b> Go to the submission detail page 
          (by clicking on corresponding Submission ID) 
          to find out more detailed information.
          Check <b>Error File</b> hyperlink for the validation log.
          You can resubmit your data file after you fix all the errors.
          };

    print qq{
          <p><b>Q6:</b> I encountered the following error when
          submitting to IMG: 
          "Error: Please specify unique species code / locus_tag prefix."
          What should I do now?<br/>
          <b>A6:</b> When you request gene calling for your submission,
          you need to specify a species_code/locus tag prefix. 
          It doesn't need to be a "real" NCBI locus tag prefix.
          For example, you can enter "TEST" in the species code field 
          (called "Locus tag prefix" in the <b>Submit sequence file</b> tab) 
          in the submission form. 
          Your genes will have "locus tag" values TEST_1, TEST_2, etc. 
          If you enter "ABC" in the locus tag prefix field, 
          then your genes will be called ABC_1, ABC_2, etc.
          <u>However, if you plan to submit your genome to NCBI in the future,
          it's best for you to request a locus tag prefix
          from NCBI first.</u>

          <p><font color='red'>As of 11/14/2014, the management team has
          decided that locus tag prefix from GOLD Analysis Project
          will be used, and users can no longer specify locus tag prefix
          in the submission form.</font><br/>
          };

    print qq{
          <p><b>Q7:</b> I have a better assembly version of my genome/metagenome. How do I request to have the genome/metagenome in IMG to be replaced by the new version?<br/>
          <b>A7:</b> Please request a new GOLD Analysis Project (AP) ID to re-submit the new sequence. In the submission form, there is a field for you to indicate which IMG genome OID to be replaced.
          };

    print qq{
          <p><b>Q8:</b> IMG has reassigned scaffold IDs to my metagenome. Where can I find the mapping between the original scaffold IDs and the IMG assigned IDs?<br/>
          <b>A8:</b> From the metagenome detail page in IMG, there is a 'Download Data' button near the right upper corner. Click the button to go to the JGI Genome Portal to find IMG download files. There is a file with name ...<b>names_map</b>, which contains the scaffold ID mapping information.
          };

    print qq{
          <p><b>Q9:</b> How can I change genome names and/or other metadata?<br/>
          <b>A9:</b> Please go to GOLD to update. Changes in GOLD will be automatically propagated to IMG in a day or so.
          };

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# CheckDisconnectSubmission
##########################################################################
sub CheckDisconnectSubmission {

    print start_form( -name => 'disconnectSubmission', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();

    my $disconnect_id_type  = param1('disconnect_id_type');
    my $disconnect_id_value = param1('disconnect_id_value');
    my $reconnect_id_value  = param1('reconnect_id_value');
    if ( !$disconnect_id_type || !$disconnect_id_value ) {
        printError("Please enter a project or sample ID.");
        print end_form();
        return;
    }

    my $conn_type = "Disconnect";

    my $disconnect_oid = $disconnect_id_value;
    my $reconnect_oid  = $reconnect_id_value;
    my $dbh            = Connect_IMG('imgsg');
    my $sql            = "";
    my $cur;
    if ( !isInt($disconnect_id_value) ) {
        if ( $disconnect_id_type eq 'project' ) {
            $sql = "select project_oid from project_info " . "where gold_stamp_id = ?";
        } else {
            $sql = "select sample_oid from env_sample " . "where gold_id = ?";
        }
        $cur = $dbh->prepare($sql);
        $cur->execute($disconnect_id_value);
        ($disconnect_oid) = $cur->fetchrow();
        $cur->finish();

        if ( !$disconnect_oid ) {
            $dbh->disconnect();
            printError("Cannot find GOLD ID $disconnect_id_value");
            print end_form();
            return;
        }
    }

    if ( $reconnect_id_value && !isInt($reconnect_id_value) ) {
        if ( $disconnect_id_type eq 'project' ) {
            $sql = "select project_oid from project_info " . "where gold_stamp_id = ?";
        } else {
            $sql = "select sample_oid from env_sample " . "where gold_id = ?";
        }
        $cur = $dbh->prepare($sql);
        $cur->execute($reconnect_id_value);
        ($reconnect_oid) = $cur->fetchrow();
        $cur->finish();

        if ( !$disconnect_oid ) {
            $dbh->disconnect();
            printError("Cannot find GOLD ID $reconnect_id_value");
            print end_form();
            return;
        }
    }

    if ($reconnect_oid) {
        $conn_type = "Reconnect";
        print "<h2>Reconnect Submission</h2>\n";
        print "<p>$disconnect_id_type: $disconnect_oid with new ID $reconnect_oid\n";
    } else {
        print "<h2>Disconnect Submission</h2>\n";
        print "<p>$disconnect_id_type: $disconnect_oid\n";
    }

    print hidden( 'disconnect_id_type',  $disconnect_id_type );
    print hidden( 'disconnect_id_value', $disconnect_id_value );
    print hidden( 'reconnect_id_value',  $reconnect_id_value );
    print hidden( 'disconnect_oid',      $disconnect_oid );
    print hidden( 'reconnect_oid',       $reconnect_oid );

    $sql = "";
    if ( $disconnect_id_type eq 'project' ) {
        $sql = "select submission_id, img_taxon_oid " . "from submission where project_info = ?";
        $cur = $dbh->prepare($sql);
        $cur->execute($disconnect_oid);
    } else {
        $sql =
            "select submission_id, img_taxon_oid "
          . "from submission where sample_oid = ? or "
          . "submission_oid in (select submission_id "
          . "from submission_samples where sample_oid = ?)";
        $cur = $dbh->prepare($sql);
        $cur->execute( $disconnect_oid, $disconnect_oid );
    }

    print "<h5>Are you sure that you want to " . lc($conn_type) . " the following submission(s)?</h5>\n";

    # show contact info
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<tr class='img' >\n";
    print "  <td class='img'   align='left' bgcolor='lightblue'>" . "Submission ID</td>\n";
    print "  <td class='img'   align='left' bgcolor='lightblue'>" . "IMG Taxon OID</td>\n";
    print "  <td class='img'   align='left' bgcolor='lightblue'>" . "Obsolete?</td>\n";
    print "</tr>\n";

    my $dbh2 = Connect_IMG('imgtaxon');
    my $sql2 = "select obsolete_flag from taxon where taxon_oid = ?";
    my $cur2 = $dbh2->prepare($sql2);

    my @submissions = ();
    my @taxons;
    for ( ; ; ) {
        my ( $submission_id, $img_taxon_oid ) = $cur->fetchrow();
        last if !$submission_id;

        push @submissions, ($submission_id);

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>" . $submission_id . "</th>\n";
        print "  <th class='subhead' align='right'>" . $img_taxon_oid . "</th>\n";

        if ($img_taxon_oid) {
            $cur2->execute($img_taxon_oid);
            my ($obsolete_flag) = $cur2->fetchrow();
            $cur2->finish();
            print "  <td class='img'   align='left'>" . $obsolete_flag . "</td></tr>\n";
            if ( $obsolete_flag ne 'Yes' ) {
                push @taxons, ($img_taxon_oid);
            }
        } else {
            print "  <td class='img'   align='left'>" . "</td></tr>\n";
        }
    }
    $cur->finish();

    print "</table>\n";

    $dbh->disconnect();
    $dbh2->disconnect();

    if ( $conn_type eq 'Disconnect' && scalar(@taxons) > 0 ) {
        print "<h5>You cannot disconnect submission(s) because of the "
          . "following IMG OID(s): "
          . join( ", ", @taxons )
          . "</h5>\n";
        print end_form();
        return;
    }

    for my $s2 (@submissions) {
        print hidden( 'disconnect_submission', $s2 );
    }

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:dbDisconnectSubmission" value="'
      . $conn_type
      . '" class="smdefbutton" />';

    print "<p>\n";

    printHomeLink();

    print end_form();
}

##########################################################################
# dbDisconnectSubmission
##########################################################################
sub dbDisconnectSubmission {
    my $contact_oid = getContactOid();
    if ( !canUpdateContact($contact_oid) ) {
        return;
    }

    my $msg                = "";
    my $disconnect_id_type = param1('disconnect_id_type');
    my $disconnect_oid     = param1('disconnect_oid');
    my $reconnect_oid      = param1('reconnect_oid');
    if ( !$disconnect_id_type ) {
        $msg = "No submissions have been selected.";
        return $msg;
    }
    if ( !$reconnect_oid ) {
        $reconnect_oid = "null";
    }

    my @submissions = param('disconnect_submission');
    my @sqlList     = ();
    for my $s2 (@submissions) {
        if ( !$s2 || !isInt($s2) ) {
            next;
        }

        if ( $disconnect_id_type eq 'project' ) {
            my $sql = "update submission set project_info = $reconnect_oid " . "where submission_id = $s2";
            push @sqlList, ($sql);
        } else {
            my $sql = "update submission set sample_oid = $reconnect_oid " . "where submission_id = $s2";
            push @sqlList, ($sql);
            if ( $reconnect_oid && isInt($reconnect_oid) ) {
                $sql =
                    "update submission_samples "
                  . "set sample_oid = $reconnect_oid "
                  . "where submission_id = $s2 "
                  . "and sample_oid = $disconnect_oid";
            } else {
                $sql = "delete from submission_samples " . "where submission_id = $s2 " . "and sample_oid = $disconnect_oid";
            }
            push @sqlList, ($sql);
        }
    }

    for my $sql (@sqlList) {
        $msg .= " SQL: $sql;";
    }
    db_con_sqlTrans( \@sqlList );

    return $msg;
}

##########################################################################
# ViewUserOrganization
##########################################################################
sub ViewUserOrganization {
    my ($country) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    if ( $isAdmin ne 'Yes' ) {
        return;
    }

    print "<h1>IMG User Distribution</h1>\n";
    print "<h2>Country: $country</h2>\n";

    print "<p><font color='red'>Organization names are not curated and may have typos.</font>\n";

    if ($use_yahoo_table) {
        ViewUserOrg_yui($country);
    } else {
        ViewUserOrg($country);
    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# ViewUserOrg (HTML table display)
##########################################################################
sub ViewUserOrg {
    my ($country) = @_;

    my $color1 = '#eeeeee';
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Organization</th>\n";
    print "<th class='img' bgcolor='$color1'>User Count</th>\n";

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql =
        "select lower(c.organization), count(*) from contact c "
      . "where c.country = ? and c.username not like '%MISSING%' "
      . "group by lower(c.organization) order by 1";
    $cur = $dbh->prepare($sql);
    $cur->execute($country);
    my $cnt = 0;

    for ( ; ; ) {
        my ( $org, $count ) = $cur->fetchrow_array();
        if ( !$org ) {
            last;
        }

        $cnt++;
        if ( $cnt > 10000 ) {
            last;
        }

        if ( $cnt % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        PrintAttribute($org);

        my $url2 = "$main_cgi?section=UserTool&page=viewOrgUserList&country=" . $country . "&org=" . $org;
        if ($count) {
            PrintAttribute( alink( $url2, $count ) );
        } else {
            PrintAttribute($count);
        }

        #        PrintAttribute($count);
        print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

}

##########################################################################
# ViewUserOrg_yui (Yahoo data table display)
##########################################################################
sub ViewUserOrg_yui {
    my ($country) = @_;

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql =
        "select lower(c.organization), count(*) from contact c "
      . "where c.country = ? and c.username not like '%MISSING%' "
      . "group by lower(c.organization) order by 1";
    $cur = $dbh->prepare($sql);
    $cur->execute($country);
    my $cnt = 0;

    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();                          # sort delimiter
    $it->addColSpec( "Organization", "char asc",   "right", "", "Organization" );
    $it->addColSpec( "User Count",   "number asc", "right", "", "User Count" );

    for ( ; ; ) {
        my ( $org, $count ) = $cur->fetchrow_array();
        if ( !$org ) {
            last;
        }

        $cnt++;
        if ( $cnt > 10000 ) {
            last;
        }

        my $row = $org . $sd . $org . "\t";

        my $url2 = "$main_cgi?section=UserTool&page=viewOrgUserList&country=" . $country . "&org=" . $org;
        if ($count) {
            $row .= $count . $sd . alink( $url2, $count ) . "\t";
        } else {
            $row .= $count . $sd . $count . "\t";
        }

        #        $row .= $count . $sd . $count . "\t";

        $it->addRow($row);
    }
    print "</table>\n";

    if ( $cnt > 0 ) {
        $it->printOuterTable(1);
    } else {
        print "<h5>No IMG users.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

}

##########################################################################
# ViewOrgUserList
##########################################################################
sub ViewOrgUserList {
    my ( $country, $org ) = @_;

    print start_form( -name => 'mainForm', -method => 'post', action => "$section_cgi" );

    my $contact_oid = getContactOid();
    my $isAdmin     = getIsAdmin($contact_oid);

    if ( $isAdmin ne 'Yes' ) {
        return;
    }

    print "<h1>IMG User Distribution</h1>\n";
    print "<h2>Country: $country</h2>\n";
    print "<h3>Organization: $org</h3>\n";

    print "<p><font color='red'>Organization names are not curated and may have typos.</font>\n";

    if ($use_yahoo_table) {
        ViewOrgUList_yui( $country, $org );
    } else {
        ViewOrgUList( $country, $org );
    }

    # Home
    print "<p>\n";
    printHomeLink();

    print end_form();
}

##########################################################################
# ViewOrgUList
##########################################################################
sub ViewOrgUList {
    my ( $country, $org ) = @_;

    if ( !$country || !$org ) {
        return;
    }

    my $contact_oid          = getContactOid();
    my $selected_contact_oid = param1('selected_contact_oid');

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql = qq{ 
        select distinct c1.img_group, c2.username, c2.name
            from contact c1, contact c2
	    where c1.img_group = c2.contact_oid
	    order by 1
        };
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my %img_groups;

    for ( ; ; ) {
        my ( $group_oid, $c_username, $c_name ) = $cur->fetchrow_array();
        if ( !$group_oid ) {
            last;
        }

        $img_groups{$group_oid} = $c_username;

        #	print "<p>Group $group_oid: " . escapeHTML($c_username) .
        #	    "; PI: " . escapeHTML($c_name) . "</p>\n";
    }
    $cur->finish();

    print "<p>\n";
    my $color1 = '#eeeeee';
    print "<table class='img' border='1'>\n";
    print "<th class='img' bgcolor='$color1'>Select</th>\n";
    print "<th class='img' bgcolor='$color1'>IMG Contact OID</th>\n";
    print "<th class='img' bgcolor='$color1'>IMG User ID</th>\n";
    print "<th class='img' bgcolor='$color1'>IMG Group</th>\n";
    print "<th class='img' bgcolor='$color1'>Name</th>\n";
    print "<th class='img' bgcolor='$color1'>Organization</th>\n";
    print "<th class='img' bgcolor='$color1'>Country</th>\n";
    print "<th class='img' bgcolor='$color1'>Email</th>\n";
    print "<th class='img' bgcolor='$color1'>Account Created</th>\n";
    print "<th class='img' bgcolor='$color1'>Most Recent Activity Date</th>\n";
    print "<th class='img' bgcolor='$color1'>JGI User Name(s)</th>\n";

    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    print "<p>\n";

    $sql = qq{ 
        select contact_oid, username, img_group, name, 
	organization, country, email, to_char(add_date, 'yyyy-mm-dd'),
	caliban_user_name
            from contact
            where country = ? and lower(organization) = ?
        };

    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute( $country, $org );

    my $link = "<a href='" . $main_cgi . "?section=UserTool&page=viewImgContacts&sort_by=";

    my %user_act = getMostRecentActDate();

    my $cnt = 0;
    for ( ; ; ) {
        my ( $c_oid, $username, $img_group, $name, $org, $country, $email, $add_date, $jgi_user_name ) =
          $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $username =~ /^MISSING/ ) {
            next;
        }

        $country = convertCountry($country);

        $cnt++;
        if ( $cnt % 2 ) {
            print "<tr class='img'>\n";
        } else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        }

        print "<td class='img'>\n";
        print "<input type='radio' ";
        print "name='selected_contact_oid' value='$c_oid'";
        if ( $c_oid == $selected_contact_oid ) {
            print " checked ";
        }
        print "/></td>\n";

        PrintAttribute($c_oid);

        my $link = "<a href='" . $main_cgi . "?section=UserTool&page=showContactInfo" . "&selected_contact_oid=$c_oid' >";
        PrintAttribute( alink( $link, $username ) );

        if ($img_group) {
            my $grp = $img_group . ":" . $img_groups{$img_group};
            PrintAttribute($grp);
        } else {
            PrintAttribute("-");
        }
        PrintAttribute($name);
        PrintAttribute($org);
        PrintAttribute($country);
        PrintAttribute($email);
        PrintAttribute($add_date);

        if ( $user_act{$c_oid} ) {
            PrintAttribute( alink( $link, $user_act{$c_oid} ) );
        } else {
            PrintAttribute("-");
        }

        PrintAttribute($jgi_user_name);
        print "</tr>\n";
    }
    print "</table>\n";

    if ( !$cnt ) {
        print "<h5>No IMG contacts.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    if ( canUpdateContact($contact_oid) ) {

        # allow resetting password
        print "<p>\n";
        print
'<input type="submit" name="_section_UserTool:resetContactPwd" value="Reset Contact Password" class="medbutton" />';
        print nbsp(3);
        print "With new password (*): ";
        print "<input type='text' name='new_pwd' value=''" . "size='30' maxLength='30'/>";
    }
}

##########################################################################
# ViewOrgUList_yui (Yahoo data table display)
##########################################################################
sub ViewOrgUList_yui {
    my ( $country, $org ) = @_;

    if ( !$country || !$org ) {
        return;
    }

    my $contact_oid          = getContactOid();
    my $selected_contact_oid = param1('selected_contact_oid');

    my $dbh = WebFunctions::Connect_IMG_Contact();
    my $sql;
    my $cur;
    $sql = qq{ 
        select distinct c1.img_group, c2.username, c2.name
            from contact c1, contact c2
	    where c1.img_group = c2.contact_oid
	    order by 1
        };
    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute();
    my %img_groups;

    for ( ; ; ) {
        my ( $group_oid, $c_username, $c_name ) = $cur->fetchrow_array();
        if ( !$group_oid ) {
            last;
        }

        $img_groups{$group_oid} = $c_username;

        #	print "<p>Group $group_oid: " . escapeHTML($c_username) .
        #	    "; PI: " . escapeHTML($c_name) . "</p>\n";
    }
    $cur->finish();

    print "<p>\n";

    my $it = new InnerTable( 1, "Test$$", "Test", 0 );
    my $sd = $it->getSdDelim();                          # sort delimiter
    $it->addColSpec("Select");
    $it->addColSpec( "IMG Contact OID", "number asc", "right", "", "IMG Contact OID" );
    $it->addColSpec( "IMG User ID",     "char asc",   "right", "", "IMG User ID" );

    $it->addColSpec( "IMG Group",                 "char asc", "left", "", "IMG Group" );
    $it->addColSpec( "Name",                      "char asc", "left", "", "Name" );
    $it->addColSpec( "Organization",              "char asc", "left", "", "Organization" );
    $it->addColSpec( "Country",                   "char asc", "left", "", "Country" );
    $it->addColSpec( "Email",                     "char asc", "left", "", "Email" );
    $it->addColSpec( "Account Created",           "char asc", "left", "", "Account Created" );
    $it->addColSpec( "Most Recent Activity Date", "char asc", "left", "", "Most Recent Activity Date" );
    $it->addColSpec( "JGI User Name(s)",          "char asc", "left", "", "JGI User Name(s)" );

    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    print "<p>\n";

    $sql = qq{ 
        select contact_oid, username, img_group, name, 
	organization, country, email, to_char(add_date, 'yyyy-mm-dd'),
	caliban_user_name
            from contact
            where country = ? and lower(organization) = ?
        };

    webLog("$sql\n");
    $cur = $dbh->prepare($sql);
    $cur->execute( $country, $org );

    my $link = "<a href='" . $main_cgi . "?section=UserTool&page=viewImgContacts&sort_by=";

    my %user_act = getMostRecentActDate();

    my $cnt = 0;
    for ( ; ; ) {
        my ( $c_oid, $username, $img_group, $name, $org, $country, $email, $add_date, $jgi_user_name ) =
          $cur->fetchrow_array();
        if ( !$c_oid ) {
            last;
        }

        if ( $username =~ /^MISSING/ ) {
            next;
        }

        $country = convertCountry($country);

        $cnt++;

        my $row = "";

        $row .= $sd . "<input type='radio' name='selected_contact_oid' " . "value='$c_oid'";
        if ( $c_oid == $selected_contact_oid ) {
            $row .= " checked ";
        }
        $row .= "/>\t";

        $row .= $c_oid . $sd . $c_oid . "\t";

        my $link = "<a href='" . $main_cgi . "?section=UserTool&page=showContactInfo" . "&selected_contact_oid=$c_oid' >";

        $row .= $username . $sd . $link . $username . "</a>" . "\t";
        if ($img_group) {
            my $grp = $img_group . ":" . $img_groups{$img_group};
            $row .= $grp . $sd . $grp . "\t";
        } else {
            $row .= " " . $sd . " " . "\t";
        }
        $row .= $name . $sd . $name . "\t";
        $row .= $org . $sd . $org . "\t";
        $row .= $country . $sd . $country . "\t";
        $row .= $email . $sd . $email . "\t";
        $row .= $add_date . $sd . $add_date . "\t";

        if ( $user_act{$c_oid} ) {
            $row .= $user_act{$c_oid} . $sd . $link . $user_act{$c_oid} . "</a>" . "\t";
        } else {
            $row .= "-" . $sd . "-" . "\t";
        }

        $row .= $jgi_user_name . $sd . $jgi_user_name . "\t";

        $it->addRow($row);
    }

    if ( $cnt > 0 ) {
        $it->printOuterTable(1);
    } else {
        print "<h5>No IMG contacts.</h5>\n";
    }

    $cur->finish();
    $dbh->disconnect();

    print "<p><font color='blue'>$cnt items loaded.</font>\n";

    print "<p>\n";
    print '<input type="submit" name="_section_UserTool:showContactInfo" value="Show Contact Info" class="meddefbutton" />';

    if ( canUpdateContact($contact_oid) ) {
        print nbsp(1);
        print
          '<input type="submit" name="_section_UserTool:updateContactInfo" value="Update Contact Info" class="medbutton" />';
        print nbsp(1);
        print '<input type="submit" name="_section_UserTool:deleteContact" value="Delete Contact" class="medbutton" />';
    }

    if ( canUpdateContact($contact_oid) ) {

        # allow resetting password
        print "<p>\n";
        print
'<input type="submit" name="_section_UserTool:resetContactPwd" value="Reset Contact Password" class="medbutton" />';
        print nbsp(3);
        print "With new password (*): ";
        print "<input type='text' name='new_pwd' value=''" . "size='30' maxLength='30'/>";
    }
}

########################################################################
# getCalibanId
########################################################################
sub getCalibanId {
    my ($email) = @_;

    my $client = REST::Client->new();

##    my $url = 'https://signon.jgi-psf.org/api/users?login=' . $email;
    my $url = 'https://signon.jgi.doe.gov/api/users?login=' . $email;
    $client->GET($url);

    my $response_code = $client->responseCode();
    my $response      = $client->responseContent();
    my @lines         = split( /\n/, $response );
    for my $line (@lines) {
        if ( $line =~ /\<id\>(\d+)/ ) {
            my $c_id = $1;
            return $c_id;
        }
    }

    return 0;
}

1;
