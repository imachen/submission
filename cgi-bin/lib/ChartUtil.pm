############################################################################
#   Charting utility functions
#
#   copied /from img_dev/v2/webUI/webui.cgi directory
############################################################################
package ChartUtil;
require Exporter;
@ISA = qw( Exporter );
@EXPORT = qw(
    newBarChart
    newPieChart
    newLineChart
    generateChart
);

use strict;
use Class::Struct;
use Time::HiRes qw (gettimeofday);

use WebEnv;
use WebFunctions;

# Force flush
$| = 1;

my $env = getEnv( );
my $tmp_url = $env->{ tmp_url };
my $tmp_dir = $env->{ tmp_dir };

# 
# Bar Chart
#
struct BarChart => {
  TYPE                  => '$',
  TITLE                 => '$',
  SUBTITLE              => '$',
  FILE_PREFIX           => '$',
  FILEPATH_PREFIX       => '$',
  WIDTH                 => '$',
  HEIGHT                => '$',
  DOMAIN_AXIS_LABEL     => '$',
  RANGE_AXIS_LABEL      => '$',
  PLOT_ORIENTATION      => '$',
  INCLUDE_LEGEND        => '$',
  INCLUDE_TOOLTIPS      => '$',
  INCLUDE_URLS          => '$',
  INCLUDE_SECTION_URLS  => '$', 
  URL_SECTION_NAME      => '$', 
  URL_SECTION           => '$', 
  SERIES_NAME           => '$',
  CATEGORY_NAME         => '$',
  DATA                  => '$',
  DATA_TOTALS           => '$',
  ITEM_URL              => '$',
  CHART_BG_COLOR        => '$',
  COLOR_THEME           => '$',
  ROTATE_DOMAIN_AXIS_LABELS => '$',
};

# 
# Pie Chart
#
struct PieChart => {
  TYPE                  => '$',
  TITLE                 => '$',
  SUBTITLE              => '$',
  FILE_PREFIX           => '$',
  FILEPATH_PREFIX       => '$',
  WIDTH                 => '$',
  HEIGHT                => '$',
  INCLUDE_LEGEND        => '$',
  INCLUDE_TOOLTIPS      => '$',
  INCLUDE_URLS          => '$',
  INCLUDE_SECTION_URLS  => '$',
  IMAGEMAP_ONCLICK      => '$',
  IMAGEMAP_HREF_ONCLICK => '$',
  URL_SECTION_NAME      => '$',
  URL_SECTION           => '$',
  SERIES_NAME           => '$',
  CATEGORY_NAME         => '$',
  DATA                  => '$',
  ITEM_URL              => '$',
  CHART_BG_COLOR        => '$',
  COLOR_THEME           => '$',
};

# 
# Line Chart
# 
struct LineChart => {
  TYPE                  => '$', 
  TITLE                 => '$', 
  SUBTITLE              => '$', 
  FILE_PREFIX           => '$', 
  FILEPATH_PREFIX       => '$', 
  WIDTH                 => '$', 
  HEIGHT                => '$', 
  INCLUDE_LEGEND        => '$', 
  INCLUDE_TOOLTIPS      => '$', 
  INCLUDE_URLS          => '$', 
  INCLUDE_SECTION_URLS  => '$', 
  IMAGEMAP_ONCLICK      => '$', 
  IMAGEMAP_HREF_ONCLICK => '$', 
  SERIES_NAME           => '$', 
  CATEGORY_NAME         => '$', 
  DATA                  => '$', 
  CHART_BG_COLOR        => '$', 
  COLOR_THEME           => '$', 
}; 

##############################################################################
# return reference to new Bar Chart object
##############################################################################
sub newBarChart {
    my $barchart = BarChart->new();
    $barchart->TYPE( "BAR" );
    my $fileprefix = getUniqueFilePrefix( "barchart" );
    $barchart->FILE_PREFIX( $fileprefix );
    $barchart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix );
    return $barchart;
}

##############################################################################
# return reference to new Pie Chart object
##############################################################################
sub newPieChart {
    my $piechart = PieChart->new();
    $piechart->TYPE( "PIE" );
    my $fileprefix = getUniqueFilePrefix( "piechart" );
    $piechart->FILE_PREFIX( $fileprefix );
    $piechart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix );
    return $piechart;
}

##############################################################################
# return reference to new Line Chart object
##############################################################################
sub newLineChart { 
    my $linechart = LineChart->new();
    $linechart->TYPE( "LINE" );
    my $fileprefix = getUniqueFilePrefix( "linechart" );
    $linechart->FILE_PREFIX( $fileprefix );
    $linechart->FILEPATH_PREFIX( $tmp_dir."/".$fileprefix );
    return $linechart; 
} 

##############################################################################
# generate the chart -- you should be able to pass in any Chart class struct,
# as long as the "TYPE" param is defined
##############################################################################
sub generateChart {
  my ($chart) = @_;

  my $st = -1;
  $st = _genBarChart($chart) if ("BAR" eq $chart->TYPE);  
  $st = _genPieChart($chart) if ("PIE" eq $chart->TYPE);  
  $st = _genLineChart($chart) if ("LINE" eq $chart->TYPE);  

  return $st;
}

##############################################################################
# generate the bar chart
##############################################################################
sub _genBarChart {
    my ($chart) = @_;

    # write out input file
    my $filepath = $chart->FILEPATH_PREFIX.".in";
    my $FH = newWriteFileHandle($filepath);
    print $FH "TYPE="               .$chart->TYPE."\n";
    print $FH "TITLE="              .$chart->TITLE."\n";
    print $FH "SUBTITLE="           .$chart->SUBTITLE."\n";
    print $FH "FILEPATH_PREFIX="    .$chart->FILEPATH_PREFIX."\n";
    print $FH "WIDTH="              .$chart->WIDTH."\n";
    print $FH "HEIGHT="             .$chart->HEIGHT."\n";
    print $FH "DOMAIN_AXIS_LABEL="  .$chart->DOMAIN_AXIS_LABEL."\n";
    print $FH "RANGE_AXIS_LABEL="   .$chart->RANGE_AXIS_LABEL."\n";
    print $FH "PLOT_ORIENTATION="   .$chart->PLOT_ORIENTATION."\n";
    print $FH "INCLUDE_LEGEND="     .$chart->INCLUDE_LEGEND."\n";
    print $FH "INCLUDE_TOOLTIPS="   .$chart->INCLUDE_TOOLTIPS."\n";
    print $FH "INCLUDE_URLS="       .$chart->INCLUDE_URLS."\n";
    print $FH "URL_SECTION_NAME="     .$chart->URL_SECTION_NAME."\n";
    print $FH "INCLUDE_SECTION_URLS=" .$chart->INCLUDE_SECTION_URLS."\n";
    print $FH "ITEM_URL="           .$chart->ITEM_URL."\n";
    print $FH "CHART_BG_COLOR="     .$chart->CHART_BG_COLOR."\n";
    print $FH "COLOR_THEME="        .$chart->COLOR_THEME."\n";
    print $FH "ROTATE_DOMAIN_AXIS_LABELS=".$chart->ROTATE_DOMAIN_AXIS_LABELS."\n";
 
    my $series_names = $chart->SERIES_NAME;
    foreach my $series_name (@$series_names) {
        print $FH "SERIES_NAME=".$series_name."\n";
    }
    my $category_names = $chart->CATEGORY_NAME;
    foreach my $category_name (@$category_names) {
        print $FH "CATEGORY_NAME=".$category_name."\n";
    }
    my $url_sections = $chart->URL_SECTION; 
    foreach my $url_section (@$url_sections) { 
        print $FH "URL_SECTION=".$url_section."\n"; 
    } 
    my $datas = $chart->DATA;
    foreach my $data (@$datas) {
        print $FH "DATA=".$data."\n";
    }
    my $totals = $chart->DATA_TOTALS; 
    foreach my $total (@$totals) {
        print $FH "DATA_TOTALS=".$total."\n";
    } 
    close ($FH);
    
    # run the charting tool
    my $cmd = $env->{ chart_exe }." ".$filepath;
    my $st = runCmdNoExit($cmd);
    return $st;
}

##############################################################################
# generate the pie chart
##############################################################################
sub _genPieChart {
    my ($chart) = @_;

    # write out input file
    my $filepath = $chart->FILEPATH_PREFIX.".in";
    my $FH = newWriteFileHandle($filepath);
    print $FH "TYPE="                  .$chart->TYPE."\n";
    print $FH "TITLE="                 .$chart->TITLE."\n";
    print $FH "SUBTITLE="              .$chart->SUBTITLE."\n";
    print $FH "FILEPATH_PREFIX="       .$chart->FILEPATH_PREFIX."\n";
    print $FH "WIDTH="                 .$chart->WIDTH."\n";
    print $FH "HEIGHT="                .$chart->HEIGHT."\n";
    print $FH "INCLUDE_LEGEND="        .$chart->INCLUDE_LEGEND."\n";
    print $FH "INCLUDE_TOOLTIPS="      .$chart->INCLUDE_TOOLTIPS."\n";
    print $FH "INCLUDE_URLS="          .$chart->INCLUDE_URLS."\n";
    print $FH "URL_SECTION_NAME="      .$chart->URL_SECTION_NAME."\n";
    print $FH "INCLUDE_SECTION_URLS="  .$chart->INCLUDE_SECTION_URLS."\n";
    print $FH "IMAGEMAP_ONCLICK="      .$chart->IMAGEMAP_ONCLICK."\n";
    print $FH "IMAGEMAP_HREF_ONCLICK=" .$chart->IMAGEMAP_HREF_ONCLICK."\n";
    print $FH "ITEM_URL="              .$chart->ITEM_URL."\n";
    print $FH "CHART_BG_COLOR="        .$chart->CHART_BG_COLOR."\n";
    print $FH "COLOR_THEME="           .$chart->COLOR_THEME."\n";
 
    my $series_names = $chart->SERIES_NAME;
    foreach my $series_name (@$series_names) {
        print $FH "SERIES_NAME=".$series_name."\n";
    }
    my $category_names = $chart->CATEGORY_NAME; 
    foreach my $category_name (@$category_names) { 
        print $FH "CATEGORY_NAME=".$category_name."\n"; 
    } 
    my $url_sections = $chart->URL_SECTION;
    foreach my $url_section (@$url_sections) {
        print $FH "URL_SECTION=".$url_section."\n";
    }
    my $datas = $chart->DATA;
    foreach my $data (@$datas) {
        print $FH "DATA=".$data."\n";
    }
    close ($FH);
    
    # run the charting tool
    my $cmd = $env->{ chart_exe } . " " . $filepath;
    my $st = runCmdNoExit($cmd);
    return $st;
}

############################################################################## 
# generate the pie chart 
############################################################################## 
sub _genLineChart { 
    my ($chart) = @_; 
 
    # write out input file 
    my $filepath = $chart->FILEPATH_PREFIX.".in"; 
    my $FH = newWriteFileHandle($filepath); 
    print $FH "TYPE="                  .$chart->TYPE."\n"; 
    print $FH "TITLE="                 .$chart->TITLE."\n"; 
    print $FH "SUBTITLE="              .$chart->SUBTITLE."\n"; 
    print $FH "FILEPATH_PREFIX="       .$chart->FILEPATH_PREFIX."\n"; 
    print $FH "WIDTH="                 .$chart->WIDTH."\n"; 
    print $FH "HEIGHT="                .$chart->HEIGHT."\n"; 
    print $FH "INCLUDE_LEGEND="        .$chart->INCLUDE_LEGEND."\n"; 
    print $FH "INCLUDE_TOOLTIPS="      .$chart->INCLUDE_TOOLTIPS."\n"; 
    print $FH "INCLUDE_URLS="          .$chart->INCLUDE_URLS."\n"; 
    print $FH "IMAGEMAP_ONCLICK="      .$chart->IMAGEMAP_ONCLICK."\n"; 
    print $FH "IMAGEMAP_HREF_ONCLICK=" .$chart->IMAGEMAP_HREF_ONCLICK."\n"; 
    print $FH "CHART_BG_COLOR="        .$chart->CHART_BG_COLOR."\n"; 
    print $FH "COLOR_THEME="           .$chart->COLOR_THEME."\n"; 
 
    my $category_names = $chart->CATEGORY_NAME;
    foreach my $category_name (@$category_names) {
        print $FH "CATEGORY_NAME=".$category_name."\n";
    } 
    my $datas = $chart->DATA;
    foreach my $data (@$datas) {
        print $FH "DATA=".$data."\n";
    }
    close ($FH);
 
    # run the charting tool
    my $cmd = $env->{ chart_exe }." ".$filepath;
    my $st = runCmdNoExit($cmd);
    return $st;
}
 
##############################################################################
# return unique file prefix
##############################################################################
sub getUniqueFilePrefix {
    my ($id) = @_;

    return "123456";

    my ($a,$b,$c,$d,$e,$f,$g,$h,$i) = localtime(time);
    my $rand = $h.$g.$f.$e.$d.$c.$b.$a;
    my ($now, $micro) = gettimeofday;

    my $fileprefix = $id."-".$rand."-".$micro;
    return $fileprefix;
}


#############################################################################
# runCmd - Run external command line tool.  Exit on failure, non-zero
#   exit status.
#############################################################################
sub runCmd { 
    my ($cmd) = @_; 

    my $st = wsystem ($cmd);
    if ($st != 0) {
	print "<p>runCmd: execution error status $st\n";
    } 
} 
 
#############################################################################
# runCmdNoExit - Run external command line, but do not exit on failure.
#############################################################################
sub runCmdNoExit {
    my ($cmd) = @_;

    my $st = wsystem ($cmd);
    return $st; 
} 


############################################################################
# wsystem - system() for web. Use only first token as executable.
############################################################################
sub wsystem { 
    my( $cmd ) = @_; 
    $cmd =~ s/\s+/ /g; 
    my @args = split( / /, $cmd );
    my $ex = shift( @args );
    checkPath( $ex );
    my $envPath = $ENV{ PATH };
    $ENV{ PATH } = "";
    my $st = system( $ex, @args );
    $ENV{ PATH } = $envPath;
    return $st; 
} 




1;
