#########################################################################
# RelAttr.pm - relational attribute definition
#########################################################################

package RelAttr;

my $fields = (
              name => undef, 
	      display_name => undef,
              data_type => undef, 
	      length => undef,
	      default_size => undef,
              desc => undef, 
	      font_color => undef,
              is_required => undef, 
	      can_edit => undef,
	      tooltip => undef,
	      copy_from => undef,
	      cv_query => undef,
	      list_values => undef,
	      filter_cond => undef,
	      url => undef,
	      header => undef,
	      tab => undef,
	      migs_id => undef,
	      migs_name => undef,
	      vcs => ( ),
              ); 
 
sub new { 
    my $class = shift; 
    my $self = { 
        %fields, 
    }; 

    # set default
    $self->{default_size} = 80;
    $self->{is_required} = 0;
    $self->{can_edit} = 1;
    $self->{length} = 80;
    $self->{display_name} = 'None';
    $self->{tab} = 'None';
    $self->{filter_cond} = 0;

    bless $self, $class; 
    return $self; 
} 


1;

