package GoldSample;

use strict; 
#use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
use POSIX qw(ceil floor);
use lib 'lib'; 
use WebFunctions; 
use RelSchema;
use ProjectInfo;
use SampleInfo;
use TabHTML;


my $default_max_row = 50;

my $is_test = 0;


#########################################################################
# dispatch 
#########################################################################
sub dispatch { 
    my ($page) = @_; 
    if ( $page eq 'showPage' ) {
	ShowPage();
    }
    elsif ( $page eq 'displaySample' ) {
        my $sample_oid = param1('sample_oid');
        GoldSample::DisplaySample($sample_oid); 
    }    
    elsif ( $page eq 'delGoldStampId' ) { 
        my $sample_oid = param1('sample_oid'); 
        SampleInfo::DeleteGoldStampId($sample_oid); 
    } 
    elsif ( $page eq 'setGoldStampId' ) { 
        my $sample_oid = param1('sample_oid'); 
        SampleInfo::SetGoldStampId($sample_oid); 
    } 
    elsif ( $page eq 'dbSetGoldStampId' ) { 
        my $sample_oid = param1('sample_oid'); 
        my $msg = SampleInfo::dbSetGoldStampId($sample_oid); 
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
        } 
    } 
    elsif ( $page eq 'searchId' ) {
	SearchByID();
    }
    elsif ( $page eq 'searchIdResult' ) {
	ShowSearchIdResultPage();
    }
    elsif ( $page eq 'delGoldStampId' ) { 
        my $sample_oid = param1('sample_oid'); 
        DeleteGoldStampId($sample_oid); 
    } 
    elsif ( $page eq 'dbDelGoldStampId' ) { 
        my $sample_oid = param1('sample_oid'); 
        my $del_gold_id_option = param1('del_gold_id_option'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = SampleInfo::dbDelGoldStampId($sample_oid,
						$del_gold_id_option, 
						$gold_stamp_id);
 
        if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        }
    }
    elsif ( $page eq 'changeContact' ) {
        my $msg = EnvSample::dbChangeSampleContact();
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'grantEdit' ) {
        SampleInfo::GrantEditPrivilege();
      }
    elsif ( $page eq 'dbGrantEditPrivilege' ) {
        my $msg = SampleInfo::dbGrantEditPrivilege();
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'mergeGoldSample' ) {
	PrintMergeGoldSamplePage();
    }
    elsif ( $page eq 'mergeSamples' ) { 
        my $sample_oid = param1('sample_oid'); 
        if ( blankStr($sample_oid) ) { 
            printError("No sample has been selected."); 
            print end_form(); 
            return; 
        } 
 
        my $merged_sample = param1('merged_sample'); 
        if ( blankStr($merged_sample) ) { 
            printError("Please select a sample to merge."); 
            print end_form(); 
            return; 
        } 
 
        if ( $sample_oid == $merged_sample ) { 
            printError("The two samples are the same. Please select a different sample to merge."); 
            print end_form(); 
            return; 
        } 
 
        GoldSample::MergeSamples($sample_oid, $merged_sample);
    } 
    elsif ( $page eq 'dbMergeSamples' ) {
        my $msg = SampleInfo::dbMergeSamples(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    }     
    elsif ( $page eq 'advSearch' ) {
	AdvancedSearch();
    }
    elsif ( $page eq 'advSearchResult' ) {
	ShowAdvSearchResultPage();
    }
    elsif ( $page eq 'filterSample' ) {
	FilterGoldSample();
    }
    elsif ( $page eq 'applyGoldSampleFilter' ) {
        ApplyGoldSampleFilter(); 
	ShowPage();
    } 
    else {	
	ShowPage();
    } 
}

#########################################################################
# ShowPage
##########################################################################
sub ShowPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 

    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h2>IMG-GOLD Samples (All)</h2>\n";

#    if ( $contact_oid == 312 ) {
#	my @pageParams = ( 'browse:goldproj_page_no', 
#			   'browse:goldproj_orderby',
#			   'browse:sample_desc' );
# 
#	for my $p1 ( @pageParams ) {
#	    if ( getSessionParam($p1) ) {
#		print "<p>$p1: " . getSessionParam($p1) . "</p>\n";
#	    } 
#	} 
#    }

    my $cnt = 0; 
    my $select_cnt = 0; 

    print "<h4>Only those samples you have permission to update are selectable. </h4>\n";
    print "<p>\n";

    $cnt = allGoldSampleCount($contact_oid);
    $select_cnt = filteredGoldSampleCount($contact_oid);

    print "<p>Selected Sample Count: $select_cnt (Total: $cnt)";
    if ( $select_cnt != $cnt ) {
	print nbsp(3);
	print "<font color='red'>(Filter is on. Not all samples are displayed.)</font></p>\n";
    }
    else {
	print "</p>\n";
    }

    # save orderby param
    my $orderby = param1('sample_orderby');
    if ( blankStr($orderby) && getSessionParam('browse:sample_orderby') ) {
	$orderby = getSessionParam('browse:sample_orderby');
    }
    my $desc = param1('sample_desc');
    if ( blankStr($desc) && getSession('browse:sample_desc') ) {
	$desc = getSessionParam('browse:sample_desc');
    }
 
    # max display 
    my $max_display = getSessionParam('sample_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    # display page numbers 
    my $curr_page = param1('goldsample_page_no'); 
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
	if ( getSessionParam('browse:goldsample_page_no') ) {
	    $curr_page = getSessionParam('browse:goldsample_page_no');
	}
    }
    if ( blankStr($curr_page) || $curr_page <= 0 ) {
	$curr_page = 1; 
    } 

    # last page
    my $last = ceil($select_cnt / $max_display); 
 
    # reset page number if too large
    if ( $curr_page > $last ) { 
        $curr_page = $last; 
    } 

    # start and end pages
    my $cnt0 = 20; 
    my $start = $cnt0 * floor(($curr_page-1) / $cnt0) + 1; 
    my $end = $start + $cnt0 - 1; 
    if ( $end > $last ) { 
        $end = $last; 
    } 

    # save browse history
    setSessionParam('browse:goldsample_page_no', $curr_page);
    setSessionParam('browse:sample_orderby', $orderby);
    setSessionParam('browse:sample_desc', $desc);

    if ( $select_cnt == 0 ) { 
        print "<h5>No samples to be displayed.</h5>\n"; 

	printGoldSampleButtons(1);

	if ( $isAdmin eq 'Yes' ) {
	    print hr;

	    print "<h2>Search GOLD Sample</h2>\n";
	    print '<input type="submit" name="_section_GoldSample:searchId" value="Search By GOLD ID" class="medbutton" />'; 

	    printAdditionalSampleSection();
	}

        return; 
    } 

    my $link0 = "<a href='" . url() . "?section=GoldSample&page=showPage";

    print "<p>\n";

    # show first?
    if ( $start > 1 ) { 
        my $link = $link0 . "&goldsample_page_no=1";
        if ( ! blankStr($orderby) ) {
            $link .= "&sample_orderby=$orderby";
            if ( ! blankStr($desc) ) { 
                $link .= "&sample_desc=$desc";
            }
        } 
        $link .= "' >" . "First" . "</a>";
        print $link . nbsp(1);
    } 

    # show previous
    if ( $start > $cnt0 ) {
        my $prev = $start - $cnt0; 
        my $link = $link0 . "&goldsample_page_no=$prev";
        if ( ! blankStr($orderby) ) {
            $link .= "&sample_orderby=$orderby";
            if ( ! blankStr($desc) ) {
                $link .= "&sample_desc=$desc";
            } 
        } 
        $link .= "' >" . "Prev" . "</a>"; 
        print $link . nbsp(1); 
    }

    my $i = 0; 
    my $page = 1; 
    for ($page = $start; $page <= $end && $page <= $last; $page++) {
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>[$page]</b>"; 
        } 
        my $link = $link0 . "&goldsample_page_no=$page"; 
        if ( ! blankStr($orderby) ) { 
            $link .= "&sample_orderby=$orderby"; 
            if ( ! blankStr($desc) ) { 
                $link .= "&sample_desc=$desc"; 
            } 
        } 
        $link .= "' >" . $s . "</a>";
        print $link . nbsp(1);
        $i += $max_display; 
        if ( $page > 1000 ) { 
            last;
        }
    } 

    # show next
    $page = $start + $cnt0; 
    if ( $page <= $last ) { 
        my $link = $link0 . "&goldsample_page_no=$page";
        if ( ! blankStr($orderby) ) {
            $link .= "&sample_orderby=$orderby";
            if ( ! blankStr($desc) ) {
                $link .= "&sample_desc=$desc";
            } 
        } 
        $link .= "' >" . "Next" . "</a>"; 
        print $link . nbsp(1); 
    } 

    # show last
    if ( $last > $end ) { 
        my $link = $link0 . "&goldsample_page_no=$last";
        if ( ! blankStr($orderby) ) {
            $link .= "&sample_orderby=$orderby"; 
            if ( ! blankStr($desc) ) {
                $link .= "&sample_desc=$desc";
            } 
        }
        $link .= "' >" . "Last" . "</a>"; 
        print $link . nbsp(1); 
    } 
    print "<p>\n";

    printGoldSampleButtons(1);

    if ( $select_cnt > 0 ) {
	listGoldSamples($contact_oid, $curr_page, '', $select_cnt); 
	printGoldSampleButtons(1);
    }

    if ( $isAdmin eq 'Yes' ) {
	print hr;

	print "<h2>Search GOLD Sample</h2>\n";
	print '<input type="submit" name="_section_GoldSample:searchId" value="Search By GOLD ID" class="medbutton" />'; 
#    print "&nbsp; \n"; 
#    print '<input type="submit" name="_section_GoldProj:advSearch" value="Advanced Search" class="medbutton" />'; 

	printAdditionalSampleSection();
    }

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}

######################################################################### 
# allGoldMetaSampleCount - count the number of metagenome samples 
########################################################################## 
sub allGoldSampleCount { 
    my ($contact_oid) = @_; 
 
    my $isAdmin = 'No';
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid);
    } 

    my $dbh=WebFunctions::Connect_IMG; 
 
    my $sql = "select count(*) from env_sample es where es.sample_oid is not null "; 

#    if ( $contact_oid ) { 
#	if ( getCanEditImgGold($contact_oid) ) {
#	    $sql .= " and (p.gold_stamp_id is not null or p.contact_oid = $contact_oid)"; 
#	}
#	else {
#	    $sql .= " and p.contact_oid = $contact_oid"; 
#	}
#    } 

    if ( $isAdmin eq 'Yes' ) {
        # super user can view all samples
    }
    else { 
        # only GOLD samples or user's own samples
        $sql .= " and (es.gold_id is not null or es.project_info in (select p.project_oid from project_info p where p.contact_oid = $contact_oid) or es.sample_oid in (select cpp.sample_permissions from contact_sample_permissions cpp where cpp.contact_oid = $contact_oid))";
    }
webLog("$sql\n"); 

    my $cur=$dbh->prepare($sql); 
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish(); 
    $dbh->disconnect();
 
    if ( ! $cnt ) {
        return 0; 
    } 
    return $cnt; 
} 

 ######################################################################### 
# filteredGoldMetaSampleCount - count the number of filtered 
#                                metagenome samples
########################################################################## 
sub filteredGoldSampleCount {
    my ($contact_oid) = @_; 

    my $isAdmin = 'No'; 
    if ( $contact_oid ) {
        $isAdmin = getIsAdmin($contact_oid);
    }
 
    my $dbh=WebFunctions::Connect_IMG;
 
    my $filter_cond = SampleInfo::sampleFilterCondition('');

    my $sql = "select count(*) from env_sample es where es.sample_oid is not null ";

    my $only_my_gold_sample = getSessionParam('gold_sample_filter:only_my_sample'); 
    my $only_my_sample = getSessionParam('sample_filter:only_my_sample'); 
    if ( $only_my_sample || $only_my_gold_sample ) { 
	# only my samples
	$sql .= " and es.contact = $contact_oid";
    }
    elsif ( $isAdmin eq 'Yes' ) {
        # super user can view all samples
    } 
    else {
        # only GOLD samples or user's own samples
        $sql .= " and (es.gold_id is not null or es.project_info in (select p.project_oid from project_info p where p.contact_oid = $contact_oid) or es.sample_oid in (select cpp.sample_permissions from contact_sample_permissions cpp where cpp.contact_oid = $contact_oid))";
    } 
 
    # filter condition 
    if ( ! blankStr($filter_cond) ) {
	if ($sql =~ /where/) {
	    $sql .= " and " . $filter_cond;
	} else {
	    $sql .= " where " . $filter_cond;
	}
    }
    webLog("$sql\n");
#    print "<p>SQL2: $sql";
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 

######################################################################### 
# printGoldMetaProjButtons - print New, Update, Delete and Copy buttons 
######################################################################### 
sub printGoldSampleButtons {
    my ($with_filter) = @_;

    print "<br/>\n";

    # New, Update, Delete and Copy buttons 
    print '<input type="submit" name="_section_EnvSample:newSample" value="New" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_EnvSample:updateSample" value="Update" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_EnvSample:deleteSample" value="Delete" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_EnvSample:moveSample" value="Move" class="smbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_EnvSample:copySample" value="Copy" class="smbutton" />'; 
    

    if ( $with_filter ) {
	# sample selection filter 
	print "&nbsp; \n"; 
	print '<input type="submit" name="_section_GoldSample:filterSample" value="Filter Samples" class="smbutton" /><br>';
    }
}

#########################################################################
# listGoldSamples - list metagenome samples 
# 
# admin does not have condition on contact_oid
##########################################################################
sub listGoldSamples {
    my ($contact_oid, $curr_page, $code, $cnt) = @_; 

    my $cond = "where es.sample_oid is not null";
    if ( length($code) == 0 ) {
	my $filter_cond = SampleInfo::sampleFilterCondition('');
	if ( ! blankStr($filter_cond) ) {
	    $cond .= " and " . $filter_cond;
	}
    }
    elsif ( $code > 4 ) {
	$cond .= " and (es.contact = $contact_oid or es.sample_oid in (select csp.sample_oid from contact_sample_permissions csp where csp.contact_oid = $contact_oid))";
    }
    else {
	$cond = "where es.web_page_code = $code";
    }

    my $isAdmin = 'No';
    if ( $contact_oid ) { 
        $isAdmin = getIsAdmin($contact_oid);
    }
    my $can_edit = getCanEditImgGold($contact_oid);
 
    my $only_my_gold_sample = getSessionParam('gold_sample_filter:only_my_sample'); 
    my $only_my_sample = getSessionParam('sample_filter:only_my_sample'); 
    if ( $only_my_gold_sample || $only_my_sample || $code > 4 ) {
	# only my samples
	my $perm_cond = " es.contact = $contact_oid ";
        if ( blankStr($cond) ) { 
            $cond = "where " . $perm_cond;
        } 
        else { 
            $cond .= " and " . $perm_cond;
        } 
    }
    elsif ( $isAdmin eq 'Yes' ) { 
        # all samples
    } 
    else {
        my $perm_cond = " (es.gold_id is not null or es.project_info in (select p.project_oid from project_info p where p.contact_oid = $contact_oid) or es.sample_oid in (select cpp.sample_permissions from contact_sample_permissions cpp where cpp.contact_oid = $contact_oid))";

        if ( blankStr($cond) ) { 
            $cond = "where " . $perm_cond;
        } 
        else { 
            $cond .= " and " . $perm_cond;
        } 
    } 

    my $selected_proj = param1('sample_oid');
 
    # max display 
    my $max_display = getSessionParam('sample_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    my $dbh=WebFunctions::Connect_IMG;
    my $orderby = param1('sample_orderby');
    my $desc = param1('sample_desc');
    if ( blankStr($orderby) ) {
        $orderby = "es.sample_oid $desc";
    }
    else {
        if ( $orderby eq 'sample_oid' ) {
            $orderby = "es.sample_oid $desc"; 
        } 
        elsif ( $orderby eq 'project_gold_id' ) {
            $orderby = "pi.gold_stamp_id $desc"; 
        } 
        elsif ( $orderby eq 'jgi_proposal_id' ) {
            $orderby = "pi.jgi_proposal_id $desc"; 
        } 
        else {
            $orderby = "es." . $orderby . " $desc, es.sample_oid";
        } 
    } 

    if ( $cond ) {
	$cond .= " and es.project_info = pi.project_oid (+)";
    }
    else {
	$cond .= " where es.project_info = pi.project_oid (+)";
    }
    my $sql = qq{
        select es.sample_oid, es.sample_display_name, es.gold_id, pi.gold_stamp_id,
        es.project_info, es.ncbi_project_id, 
        es.pmo_project_id, es.jgi_dir_number, pi.jgi_proposal_id,
        es.contact, es.add_date, es.mod_date
            from project_info pi, env_sample es 
            $cond
            order by $orderby
        }; 
 
    webLog("$sql\n"); 
#    print "SQL3: $sql<br/>";
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<h5>Click the column name to have the data order by the selected colum\
n. Click (Rev) to order by the same column in reverse order.</h5>\n";
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Row No.</th>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>" . 
        getGoldSampleOrderByLink('sample_oid', 'ER Sample ID', $code, $cnt, 0) .
        "<br/>" . 
        getGoldSampleOrderByLink('sample_oid', '(Rev)', $code, $cnt, 1) .
        "</th>\n";
    print "<th class='img'>" . 
        getGoldSampleOrderByLink('sample_display_name', 'Sample Name', $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('sample_display_name', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 
    print "<th class='img'>" .
        getGoldSampleOrderByLink('gold_id', 'GOLD Sample ID', $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('gold_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 
 
    print "<th class='img'>" .
        getGoldSampleOrderByLink('project_gold_id', 'Study GOLD ID', $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('project_gold_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>" .
        getGoldSampleOrderByLink('project_info', 'ER Study ID', $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('project_info', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>" .
        getGoldSampleOrderByLink('ncbi_project_id', 'NCBI Project ID',
			       $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('ncbi_project_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>" .
        getGoldSampleOrderByLink('pmo_project_id', 'PMO Project ID',
			       $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('pmo_project_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 
 
    print "<th class='img'>" .
        getGoldSampleOrderByLink('jgi_dir_number', 'JGI Directory Number',
			       $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('jgi_dir_number', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>" .
        getGoldSampleOrderByLink('jgi_proposal_id', 'JGI Proposal ID',
			       $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('jgi_proposal_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>" . 
        getGoldSampleOrderByLink('add_date', 'Add Date', $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('add_date', '(Rev)', $code, $cnt, 1) .
        "</th>\n";
    print "<th class='img'>" . 
        getGoldSampleOrderByLink('mod_date', 'Last Mod Date', $code, $cnt, 0) .
        "<br/>" .
        getGoldSampleOrderByLink('mod_date', '(Rev)', $code, $cnt, 1).
        "</th>\n";
 
    my $cnt2 = 0; 
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);
    my $cpp_sql = "select count(*) from contact_project_permissions where contact_oid = $contact_oid and project_permissions = ";

    for (;;) { 
        my ( $sample_id, $proj_name, $gold_stamp_id, 
             $project_gold_id, $project_info,$ncbi_project_id, 
	     $pmo_project_id, $jgi_dir_number, $jgi_proposal_id,
	     $c_oid , $add_date, $mod_date) = 
		 $cur->fetchrow_array(); 
        if ( ! $sample_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 <= $skip ) {
            next; 
        } 
 
        $disp_cnt++; 
        if ( $disp_cnt > $max_display ) {
            last;
        } 

        if ( $disp_cnt % 2 ) {
            print "<tr class='img'>\n";
        } 
        else {
            print "<tr class='img' bgcolor='#ddeeee'>\n";
        } 

	# row no.
	print "<td class='img'>$cnt2</td>\n";

        print "<td class='img'>\n";

        # check edit permission
        if ( $c_oid == $contact_oid || $can_edit ||
	     db_getValue($cpp_sql . $sample_id) ) {
	    print "<input type='radio' "; 
	    print "name='sample_oid' value='$sample_id'";
	    if ( $sample_id == $selected_proj ) {
		print " checked ";
	    }
	    print "/>";
	}
        print "</td>\n"; 
 
        my $sample_link = getSampleLink($sample_id);
        PrintAttribute($sample_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);

#        PrintAttribute($phylo);
	my $project_gold_link =  getGoldLink($project_gold_id);
        PrintAttribute($project_gold_link); 

        my $proj_link = getProjectLink($project_info);
        PrintAttribute($proj_link);

        if ( $ncbi_project_id ) {
            PrintAttribute(getNcbiProjLink($ncbi_project_id));
        }
        else {
            PrintAttribute($ncbi_project_id);
        }

        PrintAttribute($pmo_project_id);
        PrintAttribute($jgi_dir_number);
        PrintAttribute($jgi_proposal_id);

        if ( $contact_list{$c_oid} ) { 
            my $username = $contact_list{$c_oid}; 
            if ( $isAdmin eq 'Yes' ) { 
                my $link = "<a href='" . url() . 
                    "?section=UserTool&page=showContactInfo" . 
                    "&selected_contact_oid=$c_oid' >"; 
                PrintAttribute($link . $username . "</a>"); 
            } 
            else { 
                PrintAttribute($username); 
            } 
        } 
        else { 
            my $contact_str = db_getContactName($c_oid); 
            $contact_list{$c_oid} = $contact_str; 
            if ( $isAdmin eq 'Yes' ) { 
                my $link = "<a href='" . url() . 
                    "?section=UserTool&page=showContactInfo" . 
                    "&selected_contact_oid=$c_oid' >"; 
                PrintAttribute($link . $contact_str . "</a>"); 
            } 
            else { 
                PrintAttribute($contact_str); 
            } 
        } 
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 
 
    $cur->finish(); 
    $dbh->disconnect(); 

    return $cnt2;
}


##########################################################################
# getGoldSampleOrderByLink
##########################################################################
sub getGoldSampleOrderByLink {
    my ($attr, $label, $code, $category_count, $desc) = @_;
 
    my $link = "<a href='" . url() . "?section=GoldSample";

    if ( length($code) > 0 ) {
	$link .= "&page=showCategory&web_code=$code";
    }
    else {
	$link .= "&page=showPage";
    }

    $link .= "&category_page_no=1&sample_orderby=$attr";
    if ( $desc ) { 
        $link .= "&sample_desc=desc";
    }
    else {
        $link .= "&sample_desc=asc";
    }
    $link .= "&goldsample_page_no=1' >" . escapeHTML($label) . "</a>";
 
    return $link; 
} 

########################################################################## 
# printAdditionalSampleSection
########################################################################## 
sub printAdditionalSampleSection {
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 

    # allow admin to change IMG contact 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
        print "<h3>Change IMG Contact or Grant Edit Privilege for Selected Sample</h3>\n"; 
        print "<p>New IMG Contact:\n"; 
        print "<select name='new_contact' class='img' size='1'>\n"; 
 
        my $sql2 = "select contact_oid, username from contact order by username, contact_oid"; 
        my $dbh2 = Connect_IMG_Contact(); 
	webLog("$sql2\n");        
        my $cur2=$dbh2->prepare($sql2); 
        $cur2->execute(); 
 
        for (my $j2 = 0; $j2 <= 10000; $j2++) { 
            my ($id2, $name2) = $cur2->fetchrow_array(); 
            if ( ! $id2 ) { 
                last; 
            } 
 
            print "    <option value='$id2'"; 
            if ( $contact_oid == $id2 ) { 
                print " selected "; 
            } 
            print ">$name2 (OID: $id2)</option>\n"; 
        } 
        print "</select>\n"; 
        $cur2->finish(); 
        $dbh2->disconnect(); 
 
        print nbsp(3);
        print '<input type="submit" name="_section_GoldSample:changeContact" value="Change Contact" class="medbutton" />'; 

        print "<p>\n"; 
        print '<input type="submit" name="_section_GoldSample:grantEdit" value="Grant Edit Privilege" class="medbutton" />'; 
    } 
  
    # allow admin to change GOLD id 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
 
        print "<h3>Assign or Change GOLD Stamp ID for Selected Sample</h3>\n";
	print "<br/>\n";
        print '<input type="submit" name="_section_GoldSample:setGoldStampId" value="Assign GOLD Stamp ID" class="medbutton" />';
        print "&nbsp; \n";
        print '<input type="submit" name="_section_GoldSample:delGoldStampId" value="Delete GOLD Stamp ID" class="medbutton" />';
    } 
 
    # allow admin to merge samples
    if ( $isAdmin eq 'Yes' ) {
	print hr;
	print "<h3>Merge Metagenome Samples</h3>\n"; 
	print "<p>Merge the selected sample with another metagenome sample.</p>\n";
	print "<p>\n";
	print '<input type="submit" name="_section_GoldSample:mergeGoldSample" value="Merge Samples" class="medbutton" />';
    }
}

#########################################################################
# sampleFilterCondition
#
# cond_type = meta_ for metagenome
##########################################################################
sub sampleFilterCondition {
    my ($cond_type) = @_;
    my $cond = "";
    my @all_params = sampleFilterParams($cond_type);
    for my $p0 ( @all_params ) {
#	if ( ! blankStr($cond_type) ) {
#	    $p0 = $cond_type . $p0;
#	}
	my $cond1 = "";
	my ($tag, $fld_name, $typ1) = split(/\:/, $p0);
#	print "$tag, $fld_name, $typ1 $p0<br>";
        my $op_name = $p0 . ":op"; 
        my $op1 = "="; 
        if ( getSessionParam($op_name) ) { 
            $op1 = getSessionParam($op_name);
        } 
        if ( $op1 eq 'is null' || $op1 eq 'is not null' ) { 
            $cond1 ="p.$fld_name $op1"; 
        } 
        elsif ( $op1 eq 'match' && defined getSessionParam($p0) ) {
	    my $s1 = getSessionParam($p0);
	    $s1 =~ s/'/''/g;    # replace ' with ''
	    if ( length($s1) > 0 ) {
		$cond1 = "lower(es.$fld_name) like '%" . lc($s1) . "%'";
	    }
	}
	elsif ( defined getSessionParam($p0) ) {
	    my @vals = split(/\,/, getSessionParam($p0));
	    if ( lc($fld_name) eq 'only_my_sample' ) {
		if ( getSessionParam($p0) ) {
		    my $contact_oid = getContactOid();
		    $cond1 = "(p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))";
		}
	    }
	    elsif ( lc($fld_name) eq 'project_info_data_links' ) {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 ) {
		    $cond1 = "p.project_oid in (select project_oid " .
			"from project_info_data_links" .
			" where link_type = '$typ1' " .
			" and db_name = '$s1')";
		}
	    }
	    elsif ( lc($fld_name) eq 'project_info_body_sites' ) {
		if ($p0 eq 'proj_filter:project_info_body_sites:Sample Body Site') {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 ) {
		    $cond1 = "p.project_oid in (select project_oid " .
			"from project_info_body_sites" .
			" where sample_body_site = '$s1' )";
#			" and db_name = '$s1')";
		}
	    } elsif ($p0 eq 'proj_filter:project_info_body_sites:Sample Body Subsite') {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 ) {
		    $cond1 = "p.project_oid in (select project_oid " .
			"from project_info_body_sites" .
			" where sample_body_subsite = '$s1' )";
		}
	    } else {}
	    }
	    elsif ( $typ1 eq 'Cyano' ) {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 ) {
		    $cond1 = "p.project_oid in (select project_oid " .
			"from project_info_cyano_metadata" .
			" where $fld_name  like '%" . $s1 . "%')";
		}
	    }
	    elsif ( lc($fld_name) eq 'display_name' ||
		 lc($fld_name) eq 'genus' ||
		 lc($fld_name) eq 'species' ||
		 lc($fld_name) eq 'common_name' ||
		 lc($fld_name) eq 'ncbi_project_name' ) {
		my $s1 = getSessionParam($p0);
		$s1 =~ s/'/''/g;    # replace ' with ''
		if ( length($s1) > 0 ) {
		    $cond1 = "lower(es.$fld_name) like '%" . lc($s1) . "%'";
		}
	    }
	    elsif ( getAuxTableName($fld_name) ) {
		my $aux_table_name = getAuxTableName($fld_name);
		my $aux_def = def_Class($aux_table_name);
		if ( $aux_def ) {
		    my $s1 = getSessionParam($p0);
		    if ( length($s1) > 0 ) {
			$cond1 = "p.project_oid in (select project_oid " .
			    "from " . $aux_table_name .
			    " where " . lc($fld_name) .
			    " = '$s1')";
		    }
		}
	    }
	    elsif ( lc($fld_name) eq 'ncbi_project_id' ) {
		my $s1 = getSessionParam($p0);
		if ( length($s1) > 0 && isInt($s1) ) {
		    $cond1 = "p.$fld_name $op1 $s1";
		}
	    }
            elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = getSessionParam($p0);
                if ( length($s1) > 0 ) { 
                    $cond1 = "p.project_oid in (select pism.project_oid " .
                        "from project_info_seq_method pism " .
                        "where lower(pism.$fld_name) = '" . lc($s1) . "')";
                } 
            }
            elsif ( lc($fld_name) eq 'web_page_code' ) {
		for my $s2 ( @vals ) {
		    my $i = $s2 - 1;
		    if ( $i >= 0 ) {
			$cond1 = "p.$fld_name = $i";
		    }
		}
	    }
            elsif ( lc($fld_name) eq 'add_date' ||
		    lc($fld_name) eq 'mod_date' ) {
		my $op_name = $p0 . ":op";
		my $op1 = getSessionParam($op_name);
                my $d1 = getSessionParam($p0);
		if ( !blankStr($op1) && !blankStr($d1) ) {
		    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
		}
	    }
	    else {
		for my $s2 ( @vals ) {
		    $s2 =~ s/'/''/g;   # replace ' with ''
		    if ( blankStr($cond1) ) {
			$cond1 = "p.$fld_name in ('" . $s2 . "'";
		    }
		    else {
			$cond1 .= ", '" . $s2 . "'";
		    }
		}

		if ( ! blankStr($cond1) ) {
		    $cond1 .= ")";
		}
	    }
	}

	if ( ! blankStr($cond1) ) {
	    if ( blankStr($cond) ) {
		$cond = $cond1;
	    }
	    else {
		$cond .= " and " . $cond1;
	    }
	}
    }  # end for p0

    return $cond;
}

############################################################################# 
# SearchByID - search by GOLD Stamp ID
############################################################################# 
sub SearchByID {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h3>Search GOLD Metagenome Samples by GOLD Stamp ID</h3>\n"; 

    print "<p>Enter a GOLD Stamp ID or select from the list.</p>\n";

    print "<h4>Enter a GOLD Stamp ID</h4>\n";
    print "<input type='text' name='gold_stamp_id_1' value='" .
	"' size='10' maxLength='10'/>\n";

    print "<h4>Select a GOLD Stamp ID</h4>\n";
    my @gold_ids = db_getValues("select gold_id from env_sample order by gold_id");
    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
    for my $id0 ( @gold_ids ) {
        print "    <option value='$id0' >$id0</option>\n";
    }
    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldSample:searchIdResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 

}


#########################################################################
# ShowSearchIdResultPage
# 
# admin does not have condition on contact_oid
##########################################################################
sub ShowSearchIdResultPage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Stamp ID Result Page</h2>\n"; 

#    print hiddenVar('next_page', 'searchIdResult');

    my $contact_oid = getContactOid();

    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    my $cond = "";
    my $gold_stamp_id_1 = param1('gold_stamp_id_1');
    $gold_stamp_id_1 =~ s/'/''/g;   # replace ' with '', just in case
    my $gold_stamp_id_2 = param1('gold_stamp_id_2');
    $gold_stamp_id_2 =~ s/'/''/g;   # replace ' with '', just in case

    if ( ! blankStr($gold_stamp_id_1) ) {
	$cond = " where lower(s.gold_id) in ( '" .
	    lc($gold_stamp_id_1) . "'";
	if ( ! blankStr($gold_stamp_id_2) ) {
	    $cond .= ", '" . lc($gold_stamp_id_2) . "'";
	}
	$cond .= " )";
    }
    elsif ( ! blankStr($gold_stamp_id_2) ) {
	$cond = " where lower(s.gold_id) = '" .
	    lc($gold_stamp_id_2) . "'";
    }
    else {
        printError("Please enter or select a GOLD Stamp ID."); 
        print end_form(); 
        return;
    }

    if ( $isAdmin ne 'Yes' ) {
        $cond = " and p.contact_oid = $contact_oid";
    } 
 
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select s.sample_oid, s.sample_display_name, p.gold_stamp_id, s.gold_id,
        s.add_date, p.contact_name, s.mod_date, p.contact_oid
            from project_info p inner join env_sample s  on p.project_oid=s.project_info
            $cond
        }; 
 
    webLog("$sql\n"); 
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>Sample OID</th>\n";
    print "<th class='img'>Sample Display Name</th>\n";
    print "<th class='img'>Parent Project GOLD ID</th>\n";
    print "<th class='img'>Sample GOLD ID</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $sample_id, $sample_name, $project_gold_id, 
             $sample_gold_id, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $sample_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 > $default_max_row ) {
            last;   # just in case
        } 
 
        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='sample_oid' value='$sample_id' />";
        print "</td>\n"; 
 
        my $sample_link = getMProjectLink($sample_id);
        PrintAttribute($sample_link);
 
        PrintAttribute($sample_name); 
        my $gold_link = getGoldLink($project_gold_id);
        PrintAttribute($gold_link);
 
        my $sample_gold_link = getGoldLink($sample_gold_id);
        PrintAttribute($sample_gold_link);
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 

    print "<font color='blue'>(Number of samples found: $cnt2)</font>\n";

    $cur->finish(); 
    $dbh->disconnect(); 

    if ( $cnt2 > 0 ) {
	printGoldSampleButtons();

	printAdditionalSampleSection();
    }

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}

##########################################################################
# FilterGoldMetaSample_old - set filter on displaying samples
#             so that there won't be too many to display
##########################################################################
sub FilterGoldMetaSample_old { 
    my $url=url(); 
 
    print start_form(-name=>'filterSample',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid); 
 
    print "<h3>Set GOLD Sample Filter for $uname</h3>\n";

    # only show my samples?
    my $only_my_sample = getSessionParam('sample_filter:only_my_sample'); 
    print "<p>Only display my samples? \n";
    print "<input type='checkbox' name='sample_filter:only_my_sample' value='1' "; 
    if ( $only_my_sample ) { 
        print " checked ";
    } 
    print ">Yes\n"; 
    print "</p>\n"; 

    # max display 
    my $max_display = getSessionParam('sample_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = "$default_max_row"; 
    } 
    print "<p>Maximal Rows of Display:\n"; 
    print nbsp(3); 
    print "<select name='sample_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
                   '300', '500', '800', '1000', '2000',
                   '3000', '4000', '5000', '10000' ) {
        print "    <option value='$cnt0' ";
        if ( $cnt0 eq $max_display ) {
            print " selected ";
        }
        print ">$cnt0</option>\n";
    }
    print "</select>\n";
 
    # gold stamp ID
    printFilterCond('GOLD Stamp ID', 'sample_filter:gold_stamp_id',
                    'select', 'query', 
                    'select distinct gold_stamp_id from sample_info order by gold_stamp_id', 
                    getSessionParam('sample_filter:gold_stamp_id'));

    # ncbi_project_id
    printFilterCond('NCBI Project ID', 'sample_filter:ncbi_project_id', 
                    'number', '', '', 
                    getSessionParam('sample_filter:ncbi_project_id')); 

    # ncbi_project_name
    printFilterCond('NCBI Project Name', 'sample_filter:ncbi_project_name', 
                    'text', '', '', 
                    getSessionParam('sample_filter:ncbi_project_name')); 
 
    # gold web page code 
#    printFilterCond('GOLD Web Page Code', 'sample_filter:web_page_code',
#                    'select', 'query', 
#                    'select term_oid+1, description from web_page_codecv order by term_oid', 
#                    getSessionParam('sample_filter:web_page_code'));
 
    # sample type
    printFilterCond('Sample Type', 'sample_filter:sample_type',
                    'select', 'query', 
                    'select cv_term from sample_typecv order by cv_term',
                    getSessionParam('sample_filter:sample_type'));
 
    # sample status
    printFilterCond('Sample Status', 'sample_filter:sample_status',
                    'select', 'query',
                    'select cv_term from sample_statuscv order by cv_term',
                    getSessionParam('sample_filter:sample_status'));
 
    # domain 
#    printFilterCond('Domain', 'sample_filter:domain',
#                    'select', 'list', 'ARCHAEAL|BACTERIAL|EUKARYAL|MICROBIAL|PLASMID|VIRUS',
#                    getSessionParam('sample_filter:domain'));
 
    # seq status
    printFilterCond('Sequencing Status', 'sample_filter:seq_status',
                    'select', 'query',
                    'select cv_term from seq_statuscv order by cv_term',
                    getSessionParam('sample_filter:seq_status')); 
 
    # seq method 
    printFilterCond('Sequencing Method', 'sample_filter:seq_method',
                    'select', 'query', 
                    'select cv_term from seq_methodcv order by cv_term',
                    getSessionParam('sample_filter:seq_method')); 
 
    # availability: Proprietary, Public 
    printFilterCond('Availability', 'sample_filter:availability',
                    'select', 'list', 'Proprietary|Public',
                    getSessionParam('sample_filter:availability'));
 
    # phylogeny 
    printFilterCond('Category (Phylogeny)',
		    'sample_filter:phylogeny', 
                    'select', 'query', 
#                    'select cv_term from phylogenycv order by cv_term',
		    "select distinct phylogeny from sample_info where domain = 'MICROBIAL' order by phylogeny",
                    getSessionParam('sample_filter:phylogeny'));
 
    # sample display name 
    printFilterCond('Sample Display Name', 'sample_filter:sample_display_name', 
                    'text', '', '', 
                    getSessionParam('sample_filter:sample_display_name'));
 
    # genus 
    printFilterCond('Habitat (Genus)', 'sample_filter:genus', 
                    'text', '', '', 
                    getSessionParam('sample_filter:genus'));
 
    # species 
    printFilterCond('Community (Species)', 'sample_filter:species',
                    'text', '', '', 
                    getSessionParam('sample_filter:species')); 
    # common_name
    printFilterCond('Common Name', 'sample_filter:common_name', 
                    'text', '', '', 
                    getSessionParam('sample_filter:common_name')); 

    # relevance 
    printFilterCond('Sample Relevance', 'sample_filter:sample_relevance',
                    'select', 'query', 
                    'select cv_term from relevancecv order by cv_term',
                    getSessionParam('sample_filter:sample_relevance')); 
 
    # add date 
    printFilterCond('Add Date', 'sample_filter:add_date',
                    'date', '', '', 
                    getSessionParam('sample_filter:add_date')); 
 
    # mod date 
    printFilterCond('Last Mod Date', 'sample_filter:mod_date', 
                    'date', '', '', 
                    getSessionParam('sample_filter:mod_date')); 
 
    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldSample:applyGoldSampleFilter" value="Apply Sample Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print end_form();
} 


##########################################################################
# FilterGoldSample - set filter on displaying samples
#             so that there won't be too many to display
##########################################################################
sub FilterGoldSample { 
    my $url=url(); 
 
    print start_form(-name=>'filterSample',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid); 
    my $isAdmin = getIsAdmin($contact_oid); 

    print "<h3>Set GOLD Sample Filter for $uname</h3>\n";

    # only show my samples?
    my $only_my_sample = getSessionParam('sample_filter:only_my_sample'); 
    print "<p>Only display my samples? \n";
    print "<input type='checkbox' name='sample_filter:only_my_sample' value='1' "; 
    if ( $only_my_sample ) { 
        print " checked ";
    } 
    print ">Yes\n"; 
    print "</p>\n"; 

    # max display 
    my $max_display = getSessionParam('sample_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = "$default_max_row"; 
    } 
    print "<p>Maximal Rows of Display:\n"; 
    print nbsp(3); 
    print "<select name='sample_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
                   '300', '500', '800', '1000', '2000',
                   '3000', '4000', '5000', '10000' ) {
        print "    <option value='$cnt0' ";
        if ( $cnt0 eq $max_display ) {
            print " selected ";
        }
        print ">$cnt0</option>\n";
    }
    print "</select>\n";
 
    my $def_sample = def_Env_Sample();
    my @attrs = @{$def_sample->{attrs}};
 
    my @tabs =  ( 'Sample Information', 'Sequencing Information',
		  'Environmental Metadata', 'Host Metadata' );
 
    # tab display label 
    my %tab_display = (
                       'Sample Information' => 'Sample Info',
                       'Sequencing Information' => 'Sequencing Info', 
                       'Environmental Metadata' => 'Environment Metadata',
                       'Host Metadata' => 'Host Metadata',
		       );

    if ( $isAdmin eq 'Yes' ) {
	push @tabs, ( 'JGI' );
	$tab_display{'JGI'} = 'JGI Info';
    }

    my @aux_tables = getSampleAuxTables(); 
 
    print "<table class='img' border='1'>\n";
    for my $tab ( @tabs ) {
        my $tab_label = $tab;
        if ( $tab_display{$tab} ) {
            $tab_label = $tab_display{$tab};
        }
 
        print "<tr class='img' >\n"; 
        print "  <th class='subhead' colspan='3' align='right' bgcolor='lightblue'>" . 
	    "<font color='darkblue'>" . $tab_label . "</font></th>\n";
        print "</tr>\n"; 
 
        for my $k ( @attrs ) { 
            if ( $k->{tab} ne $tab ) { 
                next; 
            } 
            elsif ( ! $k->{filter_cond} ) { 
                next; 
            } 
            # show filter condition
            my $filter_name = "sample_filter:" . $k->{name};
            my $ui_type = 'text';
            my $select_type = '';
            my $select_method = ''; 
 
            if ( $k->{name} eq 'contact_oid' ) { 
                # special for contact_oid 
		my $perm_cond = " ";
		if ( $isAdmin ne 'Yes' ) {
		    $perm_cond = " where p.gold_stamp_id is not null or p.contact_oid = $contact_oid or p.sample_oid in (select cpp.sample_permissions from contact_sample_permissions cpp where cpp.contact_oid = $contact_oid)"; 
		}

                $ui_type = 'select';
                $select_type = 'query';
                $select_method = qq{
                    select c.contact_oid, c.username
                        from contact c 
                        where c.contact_oid in
                        (select p.contact_oid from sample_info p $perm_cond)
                        order by 2
                    };
            } 
            elsif ( $k->{data_type} eq 'cv' ) {
                $ui_type = 'select'; 
                $select_type = 'query'; 
                $select_method = $k->{cv_query};
            } 
            elsif ( $k->{data_type} eq 'list' ) {
                $ui_type = 'select'; 
                $select_type = 'list'; 
                $select_method = $k->{list_values};
            } 
            elsif ( $k->{data_type} eq 'int' || 
                    $k->{data_type} eq 'number' ) { 
                $ui_type = 'number'; 
            }
            elsif ( $k->{data_type} eq 'date' ) {
                $ui_type = 'date';
            } 
	    SampleInfo::printFilterCondRow($k->{display_name}, $filter_name,
                               $ui_type, $select_type, $select_method,
					    getSessionParam($filter_name));
        }  # end for my k 

        for my $k2 ( @aux_tables ) {
            my $def_aux = def_Class($k2);
            if ( ! $def_aux ) { 
                next;
            } 
            if ( $def_aux->{tab} ne $tab ) {
                next;
            } 
	    if ($k2 eq 'sample_info_cyano_metadata') {
		next;
	    }
            # special condition for data links 
            if ( $k2 eq 'sample_info_data_links' ) { 
                for my $typ ( 'Funding', 'Seq Center' ) { 
                    my $filter_name = "sample_filter:" . $k2 . ":" . $typ; 
                    my $filter_disp = "Data Links: " . $typ; 
                    my $filter_sql = qq{ 
                        select distinct db_name 
                            from sample_info_data_links 
                            where link_type = '$typ' 
                            and db_name is not null 
                            order by 1 
                        }; 
                    # $filter_sql = "select cv_term from db_namecv"; 
                    SampleInfo::printFilterCondRow($filter_disp, 
                                                    $filter_name, 
                                                    'select', 'query', 
                                                    $filter_sql, 
                                   getSessionParam($filter_name)); 
                } 
            } 
 
            my @aux_attrs = @{$def_aux->{attrs}};
            for my $k3 ( @aux_attrs ) {
                if ( ! $k3->{filter_cond} ) {
                    next;
                }
 
                my $filter_name = "sample_filter:" . $k3->{name};

		my $ui_type = 'text';
		my $select_type = '';
		my $select_method = '';

		if ( $k3->{data_type} eq 'cv' ) {
		    $ui_type = 'select'; 
		    $select_type = 'query'; 
		    $select_method = $k3->{cv_query};
		} 
		elsif ( $k3->{data_type} eq 'list' ) {
		    $ui_type = 'select'; 
		    $select_type = 'list'; 
		    $select_method = $k3->{list_values};
		} 
		elsif ( $k3->{data_type} eq 'int' || 
			$k3->{data_type} eq 'number' ) { 
		    $ui_type = 'number'; 
		}
		elsif ( $k3->{data_type} eq 'date' ) {
		    $ui_type = 'date';
		} 

		SampleInfo::printFilterCondRow($k3->{display_name},
                                                $filter_name,
						$ui_type,
						$select_type,
						$select_method,
						getSessionParam($filter_name));
            } 
        }  # end for my k2 
    } # end for my tab 
    print "</table>\n"; 

 
    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldSample:applyGoldSampleFilter" value="Apply Sample Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print end_form();
} 


###########################################################################
# printFilterCond - print filter condition 
###########################################################################
sub printFilterCond { 
    my ($title, $param_name, $ui_type, $select_type, $select_method,
        $def_val_str) = @_; 

  SampleInfo::printFilterCond($title, $param_name, $ui_type, $select_type,
			       $select_method, $def_val_str);
}


########################################################################## 
# ApplyGoldSampleFilter - save filter info into database 
#                              and apply filter 
########################################################################## 
sub ApplyGoldSampleFilter { 
    # only my samples? 
    my $only_my_sample = param1('sample_filter:only_my_sample'); 
    if ( $only_my_sample ) { 
        setSessionParam('sample_filter:only_my_sample', $only_my_sample); 
    } 
    else {
        setSessionParam('sample_filter:only_my_sample', 0); 
    }

    # max display 
    my $max_display = param1('sample_filter:max_display'); 
    if ( ! blankStr($max_display) ) { 
        setSessionParam('sample_filter:max_display', $max_display); 
    } 
 
    # show filter selection 
    my @all_params = SampleInfo::sampleFilterParams(); 
    for my $p0 ( @all_params ) { 
	# add meta to filter
	if ( $p0 eq 'sample_filter:only_my_sample' ) {
	    next;
	}

#	$p0 = "sample_filter" . $p0;
        if ( $p0 eq 'sample_filter:add_date' || 
             $p0 eq 'sample_filter:mod_date' ) { 
            # date 
            my $op_name = $p0 . ":op"; 
            my $op1 = param1($op_name); 
            my $d1 = param1($p0); 
            if ( !blankStr($d1) && !blankStr($op1) ) { 
                if ( ! isDate($d1) ) { 
                    print "<p>Incorrect Date (" . escapeHTML($d1) . 
                        ") -- Filter condition is ignored.</p>\n"; 
                } 
                else { 
                    setSessionParam($op_name, $op1); 
                    setSessionParam($p0, $d1); 
                } 
            } 
            else { 
                # clear condition 
                setSessionParam($op_name, ''); 
                setSessionParam($p0, ''); 
            } 

	    next;
        } 

        # op 
        my $op_name = $p0 . ":op"; 
        if ( param1($op_name) ) { 
            setSessionParam($op_name, param1($op_name));
        } 
	else {
	    # clear
	    setSessionParam($op_name, ''); 
	}
 
        # value 
        if ( param($p0) ) {
            # filter
            my @options = param($p0);
 
            my $s0 = "";
            for my $s1 ( @options ) {
                if ( blankStr($s0) ) { 
                    $s0 = $s1;
                } 
                else {
                    $s0 .= "," . $s1;
                }
            } 
            setSessionParam($p0, $s0); 
        } 
        else { 
            my ($tag1, $val1) = split(/\:/, $p0);
            setSessionParam($p0, ''); 
        } 
    }

    # clear browse params
#    clearSamplePageInfo();

} 

#########################################################################
# ShowNextPage
##########################################################################
sub ShowNextPage {
    my ($new_code) = @_;

    my $page = param1('page');

    my $web_code2 = param1('web_code');
 
    if ( $web_code2 && $web_code2 > 4 ) {
        ShowCategoryPage($web_code2);
    } 
    elsif ( $page eq 'showCategory' && length($new_code) > 0 ) {
	ShowCategoryPage($new_code);
    }
    elsif ( $page eq 'showCategory' && defined param1('web_code') ) {
	my $web_code = param1('web_code');
	if ( length($web_code) > 0 ) {
	    ShowCategoryPage($web_code);
	}
	else {
	    ShowPage();
	}
    }
    else {
	ShowPage();
    }

#    if ( $next_page eq 'searchIdResult' ) {
#	ShowPage();
#    }
#    else {
#	ShowCategoryPage();    
#    }
}

######################################################################### 
# PrintMergeGoldSamplePage
######################################################################### 
sub PrintMergeGoldSamplePage {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Merge Metagenome Samples</h2>\n"; 

    my $sample_oid = param1('sample_oid');
    if ( ! $sample_oid ) {
        printError("Please select a sample first."); 
        print end_form(); 
        return;
    }
    print "<h3>Selected Sample: $sample_oid</h3>\n";
    print hiddenVar('sample_oid', $sample_oid);

    print "<p>Merge the selected sample with the following sample:</p>\n"; 
 
    my $sql2 = "select sample_oid, sample_display_name from env_sample " . 
        "order by sample_oid"; 
    my $dbh2 = Connect_IMG (); 
    webLog("$sql2\n");    
    my $cur2=$dbh2->prepare($sql2); 
    $cur2->execute(); 
 
    print "<select name='merged_sample' class='img' size='1'>\n"; 
    for (my $j2 = 0; $j2 <= 100000; $j2++) {
        my ($id2, $name2) = $cur2->fetchrow_array();
        if ( ! $id2 ) { 
            last; 
        } 
 
	if ( $id2 == $sample_oid ) {
	    next;
	}

        print "    <option value='$id2'>";
        print "$id2 - $name2</option>\n";
    } 
    $cur2->finish(); 
    $dbh2->disconnect(); 
    print "</select>\n"; 
 
    print "<p>\n";
    print '<input type="submit" name="_section_GoldSample:mergeSamples" value="Merge Samples" class="medbutton" />';

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
} 
 
######################################################################### 
# MergeSamples
######################################################################### 
sub MergeSamples {
    my ($sample_oid, $merged_sample) = @_;

    # check sample oid
    if ( blankStr($sample_oid) || blankStr($merged_sample) ) {
	printError("No sample has been selected.");
	print end_form();
	return;
    }

    my %proj_val = SelectSampleInfo($sample_oid);
    my %m_proj_val = SelectSampleInfo($merged_sample);

    my $url=url(); 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 

    if ( $proj_val{'sample_type'} eq 'Metagenome' ) {
	print "<h2>Merge Metagenome Samples $sample_oid and $merged_sample</h2>\n"; 
    }
    else {
	print "<h2>Merge Samples $sample_oid and $merged_sample</h2>\n"; 
    }
    print hiddenVar('sample_oid', $sample_oid); 
    print hiddenVar('merged_sample', $merged_sample);

    # get Env_Sample definition
    my $def_sample;
    if ( $proj_val{'sample_type'} eq 'Metagenome' ) {
	$def_sample = def_meta_Env_Sample();
    }
    else {
	$def_sample = def_Env_Sample();
    }

    my @attrs = @{$def_sample->{attrs}};

    # add javascript function
    print "\n<script language=\"javascript\" type=\"text/javascript\">\n\n";
    print "function setSelectMerge( x ) {\n";
    print "   var f = document.mainForm;\n";
    print "   for ( var i = 0; i < f.length; i++ ) {\n";
    print "         var e = f.elements[ i ];\n";
    print "         if ( e.name.match( /^select:/ ) ) {\n";
    print "              if ( x == 0 ) {\n";
    print "                 if ( e.value == 'union' ) {\n";
    print "                      e.checked = true;\n";
    print "                    }\n";
    print "                 }\n";

    print "              if ( e.value == x && ! e.disabled ) {\n";
    print "                   e.checked = true;\n";
    print "                 }\n";

    print "            }\n";
    print "         }\n";
    print "   }\n";
    print "\n</script>\n\n";

    print "<p>\n"; 
    print "<table class='img' border='1'>\n"; 
    print "<th class='img'>Field Name</th>\n";
    print "<th class='img'>";
    print "<input type='button' value='Select $sample_oid' " .
	"Class='tinybutton'\n";
    print "  onClick='setSelectMerge ($sample_oid)' />\n";
    print "</th>\n";
    print "<th class='img'>Sample $sample_oid</th>\n";

    print "<th class='img'>";
    print "<input type='button' value='Select $merged_sample' " .
	"Class='tinybutton'\n";
    print "  onClick='setSelectMerge ($merged_sample)' />\n";
    print "</th>\n";
    print "<th class='img'>Sample $merged_sample</th>\n";

    print "<th class='img'>\n";
    print "<input type='button' value='Union Both' Class='tinybutton'\n";
    print "  onClick='setSelectMerge (0)' />\n";
    print "</th>\n";

    my @tabs =  ( 'Sample', 'Organism', 'Links', 'Metadata' );
    if ( $proj_val{'sample_type'} eq 'Metagenome' ) {
	@tabs =  ( 'Sample', 'Metagenome', 'Links', 'Metadata' );
    }
    for my $tab ( @tabs ) {
        print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	    "<font color='darkblue'>" . $tab . "</font></th>\n"; 
        # print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n"; 
        print "</tr>\n"; 

	for my $k ( @attrs ) { 
	    if ( $k->{tab} ne $tab ) {
		next;
	    }

	    my $attr_name = $k->{name}; 
	    if ( $attr_name eq 'modified_by' ||
		 $attr_name eq 'mod_date' ) {
		# these attribute values are system controlled
		next;
	    }

	    my $attr_val = ""; 
	    my $m_attr_val = "";
 
	    if ( defined $proj_val{$attr_name} ) {
		$attr_val = $proj_val{$attr_name};
	    }
	    if ( defined $m_proj_val{$attr_name} ) { 
		$m_attr_val = $m_proj_val{$attr_name}; 
	    } 

	    if ( blankStr($attr_val) && blankStr($m_attr_val) ) {
		next; 
	    } 
 
	    my $disp_name = $k->{display_name}; 
 
	    print "<tr class='img' >\n"; 
	    if ( $k->{font_color} ) {
		print "  <th class='subhead' align='right'>" .
		    "<font color='" . $k->{font_color} .
		    "'>" . nbsp(5) . $disp_name . "</font></th>\n";
	    } 
	    else {
		print "  <th class='subhead' align='right'>" . $disp_name;
	    }
	    print "</th>\n"; 

	    # display select button
	    print "<td class='img'>\n";
	    if ( $attr_name eq 'ncbi_superkingdom' || 
		 $attr_name eq 'ncbi_phylum' ||
		 $attr_name eq 'ncbi_class' || 
		 $attr_name eq 'ncbi_order' ||
		 $attr_name eq 'ncbi_family' ||
		 $attr_name eq 'ncbi_genus' ||
		 $attr_name eq 'ncbi_species' ) {
		# not allowing selection
	    }
	    else {
		print "<input type='radio' name='select:$attr_name' " .
		    "value='$sample_oid' checked />\n";
	    }
	    print "</td>\n";

	    # display attribute value
	    if ( $attr_name eq 'gold_stamp_id' ) {
		# gold
		if ( ! blankStr($attr_val) ) {
		    print "  <td class='img'   align='left'>" . 
			getGoldLink($attr_val) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}

		print "<td class='img'>\n";
		print "<input type='radio' name='select:$attr_name' value='$merged_sample' />\n";
		print "</td>\n";
		if ( ! blankStr($m_attr_val) ) {
		    print "  <td class='img'   align='left'>" . 
			getGoldLink($m_attr_val) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}
	    }
	    elsif ( $attr_val =~ /^http\:\/\// ||
		$attr_val =~ /^https\:\/\// ||
		$attr_val =~ /^ftp\:\/\// ) {
		# url or ftp
		my $url_type = "URL";
		if ( $attr_val =~ /^ftp\:\/\// ) {
		    $url_type = "FTP";
		}

		if ( ! blankStr($attr_val) ) {
		    print "  <td class='img'   align='left'>" .
			alink($attr_val, $url_type, 'target', 1) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}

		print "<td class='img'>\n";
		print "<input type='radio' name='select:$attr_name' value='$merged_sample' />\n";
		print "</td>\n";
		if ( ! blankStr($m_attr_val) ) {
		    print "  <td class='img'   align='left'>" .
			alink($m_attr_val, 'URL', 'target', 1) . "</td>\n";
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}
	    }
	    else {
		# regular attribute value
		if ( ! blankStr($attr_val) ) {
		    if ( $attr_name eq 'ncbi_project_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getNcbiProjLink($attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'ncbi_taxon_id' ) {
			# NCBI taxon 
			print "  <td class='img'   align='left'>" .
			    getNcbiTaxonLink($attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'homd_id' ) {
			# HOMD ID
			print "  <td class='img'   align='left'>" . 
			    getHomdLink($attr_val) . "</td>\n";
		    } 
		    elsif ( $attr_name eq 'greengenes_id' ) {
			# Greengenes
			print "  <td class='img'   align='left'>" .
			    getGreengenesLink($attr_val) . "</td>\n";
		    }
		    else {
			print "  <td class='img'   align='left'>" . 
			    escapeHTML($attr_val) . "</td>\n"; 
		    }
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}

		# display select button
		print "<td class='img'>\n";
		if ( $attr_name eq 'ncbi_superkingdom' || 
		     $attr_name eq 'ncbi_phylum' ||
		     $attr_name eq 'ncbi_class' || 
		     $attr_name eq 'ncbi_order' ||
		     $attr_name eq 'ncbi_family' ||
		     $attr_name eq 'ncbi_genus' ||
		     $attr_name eq 'ncbi_species' ) {
		    # not allowing selection
		}
		else {
		    print "<input type='radio' name='select:$attr_name' " .
			"value='$merged_sample' />\n";
		}
		print "</td>\n";
		if ( ! blankStr($m_attr_val) ) {
		    if ( $attr_name eq 'ncbi_project_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getNcbiProjLink($m_attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'ncbi_taxon_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getNcbiTaxonLink($m_attr_val) . "</td>\n";
		    }
		    elsif ( $attr_name eq 'homd_id' ) {
			# HOMD ID
			print "  <td class='img'   align='left'>" . 
			    getHomdLink($m_attr_val) . "</td>\n";
		    } 
		    elsif ( $attr_name eq 'greengenes_id' ) {
			# NCBI project 
			print "  <td class='img'   align='left'>" .
			    getGreengenesLink($m_attr_val) . "</td>\n";
		    }
		    else {
			print "  <td class='img'   align='left'>" . 
			    escapeHTML($m_attr_val) . "</td>\n"; 
		    }
		}
		else {
		    print "  <td class='img'   align='left'></td>\n";
		}
	    }
	    print "<td class='img'>\n";
	    # print "<input type='radio' name='select:$attr_name' value='union' />\n";

	    print "</td>\n";

	    print "</tr>\n"; 
	}

	if ( $tab eq 'Links' ) {
	    print "<tr class='img' >\n"; 
	    print "  <th class='subhead' align='right'>" . 
		"Data Links" . "</th>\n"; 

	    my $tname = 'env_sample_data_links';
	    print "<td class='img'>\n";
	    print "<input type='radio' name='select:$tname' value='$sample_oid' checked />\n";
	    print "</td>\n";
	    DisplaySampleSetAttr($sample_oid, 'env_sample_data_links',
				  ', ', "<br/>", 1);
	    print "<td class='img'>\n";
	    print "<input type='radio' name='select:$tname' value='$merged_sample' />\n";
	    print "</td>\n";
	    DisplaySampleSetAttr($merged_sample, 'env_sample_data_links',
				  ', ', "<br/>", 1);
	    print "<td class='img'>\n";
	    print "<input type='radio' name='select:$tname' value='union' />\n";
	    print "</td>\n";
	    print "</tr>\n";
	}
    }  # end for tab

    my @aux_tables = getSampleAuxTables ();
    for my $tname ( @aux_tables ) {
	if ( $tname eq 'env_sample_data_links' ) {
	    next;
	}

	my $def_aux = def_Class($tname);
	if ( ! $def_aux ) {
	    next;
	}

	if ($tname eq 'env_sample_cyano_metadata') {
	    next;	
	}

	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right'>" . 
	    $def_aux->{display_name} . "</th>\n"; 

	print "<td class='img'>\n";
	print "<input type='radio' name='select:$tname' value='$sample_oid' checked />\n";
	print "</td>\n";
	DisplaySampleSetAttr($sample_oid, $tname, ', ', ', ', 1);

	print "<td class='img'>\n";
	print "<input type='radio' name='select:$tname' value='$merged_sample' />\n";
	print "</td>\n";
	DisplaySampleSetAttr($merged_sample, $tname, ', ', ', ', 1);

	print "<td class='img'>\n";
	print "<input type='radio' name='select:$tname' value='union' />\n";
	print "</td>\n";
	print "</tr>\n";
    }

# cyano metadata
	print "  <th class='subhead' align='right' bgcolor='lightblue'>" .
	    "<font color='darkblue'>" . "Cyano Metadata" . "</font></th>\n"; 
        # print "  <td class='img'   align='left' bgcolor='lightblue'>" . "</td>\n"; 
        print "</tr>\n"; 

    my $tname = 'env_sample_cyano_metadata';
    my $def_aux = def_Class($tname);
    @attrs = @{$def_aux->{attrs}};
    for my $k ( @attrs ) { 
	my $attr_name = $k->{name}; 
	if ( $attr_name eq 'modified_by' ||
	     $attr_name eq 'mod_date' ) {
	    # these attribute values are system controlled
	    next;
	}
	
	if ( $attr_name eq 'sample_oid' ) {
	    # already printed from env_sample table
	    next;
	}
	
	my $attr_val = ""; 
	my $m_attr_val = "";
	
	if ( defined $proj_val{$attr_name} ) {
	    $attr_val = $proj_val{$attr_name};
	}
	if ( defined $m_proj_val{$attr_name} ) { 
	    $m_attr_val = $m_proj_val{$attr_name}; 
	} 
	
	if ( blankStr($attr_val) && blankStr($m_attr_val) ) {
	    next; 
	} 
	
	my $disp_name = $k->{display_name}; 
	
	print "<tr class='img' >\n"; 
	if ( $k->{font_color} ) {
	    print "  <th class='subhead' align='right'>" .
		"<font color='" . $k->{font_color} .
		"'>" . nbsp(5) . $disp_name . "</font></th>\n";
	} 
	else {
	    print "  <th class='subhead' align='right'>" . $disp_name;
	}
	print "</th>\n"; 
	
	# display select button
	print "<td class='img'>\n";
	print "<input type='radio' name='select:$attr_name' " .
	    "value='$sample_oid' checked />\n";
	print "</td>\n";
	
	# display attribute value
	if ( ! blankStr($attr_val) ) {
	    print "  <td class='img'   align='left'>" . 
		escapeHTML($attr_val) . "</td>\n"; 
	    
	}
	else {
	    print "  <td class='img'   align='left'></td>\n";
	}
	
	# display select button
	print "<td class='img'>\n";
	print "<input type='radio' name='select:$attr_name' " .
	    "value='$merged_sample' />\n";
	print "</td>\n";
	if ( ! blankStr($m_attr_val) ) {
	    print "  <td class='img'   align='left'>" . 
		escapeHTML($m_attr_val) . "</td>\n"; 
	}
	else {
	    print "  <td class='img'   align='left'></td>\n";
	}
    
    print "<td class='img'>\n";
    # print "<input type='radio' name='select:$attr_name' value='union' />\n";
    
    print "</td>\n";
    
    print "</tr>\n"; 

    }    
    # samples?
    my $sample_cnt = db_getValue("select count(*) from env_sample where env_sample = $sample_oid");
    my $m_sample_cnt = db_getValue("select count(*) from env_sample where env_sample = $merged_sample");
    if ( $sample_cnt || $m_sample_cnt ) {
	# show samples
	print "<tr class='img' >\n"; 
	print "  <th class='subhead' align='right'>" .
	    "Samples" . "</th>\n"; 

	print "<td class='img'>\n";
	print "<input type='radio' name='select:Samples' value='$sample_oid'  checked />\n";
	print "</td>\n";

	print "  <td class='img'   align='left'>";

	if ( $sample_cnt ) {
	    my $sql2 = "select sample_oid, sample_display_name " .
		"from env_sample where env_sample = $sample_oid";
	    my $dbh2 = Connect_IMG();
webLog("$sql2\n");	    
	    my $cur2 = $dbh2->prepare($sql2);
	    $cur2->execute(); 
 
	    for (my $j = 0;$j <= 100000; $j++) { 
		my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
		last if !$s_oid;

		my $sample_link = getSampleLink($s_oid);
		print $sample_link . " - ";
		print escapeHTML($s_name);
		print "<br/>\n";
	    }
	    $cur2->finish();
	    $dbh2->disconnect();
	}
	print "  </td>\n";

	print "<td class='img'>\n";
	print "<input type='radio' name='select:Samples' value='$merged_sample' />\n";
	print "</td>\n";

	print "  <td class='img'   align='left'>";

	if ( $m_sample_cnt ) {
	    my $sql2 = "select sample_oid, sample_display_name " .
		"from env_sample where env_sample = $merged_sample";
	    my $dbh2 = Connect_IMG();
webLog("$sql2\n");	    
	    my $cur2 = $dbh2->prepare($sql2);
	    $cur2->execute(); 
 
	    for (my $j = 0;$j <= 100000; $j++) { 
		my ( $s_oid, $s_name ) = $cur2->fetchrow_array(); 
		last if !$s_oid;

		my $sample_link = getSampleLink($s_oid);
		print $sample_link . " - ";
		print escapeHTML($s_name);
		print "<br/>\n";
	    }
	    $cur2->finish();
	    $dbh2->disconnect();
	}
	print "  </td>\n";

	print "<td class='img'>\n";
	print "<input type='radio' name='select:Samples' value='union' />\n";
	print "</td>\n";

	print "</tr>\n";
    }
	
    print "</table>\n"; 

    print "<p>\n";
    print '<input type="submit" name="_section_SampleInfo:dbMergeSamples" value="Merge" class="smbutton" />';
    print "&nbsp; \n";
    print '<input type="submit" name="_section_SampleInfo:showSamples" value="Cancel" class="smbutton" />';

    print "<p>\n";
    printHomeLink();

    print end_form();
}


1;
 __END__

    elsif ( $page eq 'showCategory' ) {
	my $web_code = 4;
	if ( defined param1('web_page_code') ) {
	    $web_code = param1('web_page_code');
	}

	ShowNextPage($web_code);
    }
    elsif ( $page eq 'editSample' ) {
        my $project_oid = param1('project_oid');
 
        if ( blankStr($project_oid) ) {
            printError("No project has been selected."); 
            print end_form(); 
            return; 
        } 

#	EnvSample::ShowProjectSamples($project_oid, 'GoldMetaProj');
	EnvSample::ShowProjectSamples($project_oid);



#########################################################################
# ShowGoldProjects
##########################################################################
sub ShowGoldProjects {
    my %web_page_code = getGoldWebPageCode();

    my $dbh = Connect_IMG();

    my $sql = qq{
	select web_page_code, count(*) from project_info
	    group by web_page_code
	    order by web_page_code
	};

webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 

    my $row_cnt = 0;

    print "<p>\n"; 
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Category</th>\n";
    print "<th class='img'>Count</th>\n";

    for (;;) { 
        my ( $code, $count ) = $cur->fetchrow( );
	if ( ! (defined $code) || length($code) == 0 ) {
	    last;
	}

	$row_cnt++;
	if ( $row_cnt > 10 ) {
	    last;      # just in case
	}

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
	if ( $web_page_code{$code+1} ) {
	    print escapeHTML($web_page_code{$code+1});
	}
	else {
	    print escapeHTML($code);
	}
        print "</td>\n"; 

        print "<td class='img'>\n";
        my $link = "<a href='" . url() .
            "?section=GoldMetaProj&page=showCategory" .
	    "&web_code=$code" .
            "&category_page_no=1";
        $link .= "' >" . $count . "</a>";
	print $link;
        print "</td>\n"; 

	print "</tr>\n";
    }
    print "</table>\n";

    $cur->finish();
    $dbh->disconnect();
}


#########################################################################
# getGoldWebPageCode
#
# (Note: need to use term_oid+1 because code starts with 0)
##########################################################################
sub getGoldWebPageCode {
    my %h;

    my $dbh = Connect_IMG();
    my $sql = "select term_oid+1, description from web_page_codecv";
    webLog("$sql\n");
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    for (;;) { 
        my ( $id, $val ) = $cur->fetchrow( );
	last if !$id;

	$h{$id} = $val;
    }
    $cur->finish();
    $dbh->disconnect();

    return %h;
}

#########################################################################
# ShowCategoryPage
##########################################################################
sub ShowCategoryPage {
    my ($new_code) = @_;

    my $code = $new_code;
    if ( length($code) == 0 ) {
	$code = param1('web_code');
	if ( length($code) == 0 ) {
	    ShowPage();
	    return;
	}
    }

    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    if ( $code > 4 ) {
	print "<h2>My Metagenome Projects</h2>\n";
    }
    else {
	my $category = db_getValue("select description from web_page_codecv where term_oid=$code");
	print "<h2>IMG-GOLD $category Projects (GOLD Projects Only)</h2>\n";

	print "<h4>Only those projects you have permission to update are selectable.</h4>\n";
    }

    if ( param1('page') ) {
	print hiddenVar('page', param1('page'));
    }
    else {
	print hiddenCar('page', 'showCategory');
    }

    my $cat_sql = "";

    if ( $code > 4 ) {
	$cat_sql = qq{
	    select count(*) from project_info p
		where p.domain = 'MICROBIAL'
		and (p.contact_oid = $contact_oid
		     or p.project_oid in (select cpp.project_permissions
					  from contact_project_permissions cpp
					  where cpp.contact_oid = $contact_oid))
	    };
    }
    else {
	$cat_sql = "select count(*) from project_info where web_page_code = $code";
	if ( $isAdmin ne 'Yes' ) {
	    $cat_sql .= " and gold_stamp_id is not null";
	}
    }

    my $category_count = db_getValue($cat_sql);
    print "<p><font color='blue'>(Count: $category_count)</font></p>\n";

    my $curr_page = param1('category_page_no');
    my $orderby = param1('sample_orderby'); 
    my $desc = param1('project_desc'); 
    my $max_display = getSessionParam('sample_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 

    # save parameters
    print hiddenVar('web_code', $code);
    print hiddenVar('category_page_no', $curr_page);
    if ( ! blankStr($orderby) ) {
	print hiddenVar('sample_orderby', $orderby);
    }
    if ( ! blankStr($desc) ) {
	print hiddenVar('project_desc', $desc);
    }

    my $i = 0; 
    my $page = 1; 
#    print "<br/>\n"; 
    while ( $i < $category_count ) { 
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>$page</b>"; 
        } 
        my $link = "<a href='" . url() . 
            "?section=GoldMetaProj&page=showCategory" . 
	    "&web_code=$code" .
            "&category_page_no=$page";

        if ( ! blankStr($orderby) ) { 
            $link .= "&sample_orderby=$orderby"; 
            if ( ! blankStr($desc) ) { 
                $link .= "&project_desc=$desc"; 
            } 
        } 

        $link .= "' >" . $s . "</a>"; 
        print $link . nbsp(1); 
        $i += $max_display; 
        $page++; 
        if ( $page > 1000 ) {
	    print " ...\n";
            last; 
        } 
    } 
 
    printGoldMetaProjButtons(0);
    listGoldMetaProjects($contact_oid, $curr_page, $code, $category_count);

    printGoldMetaProjButtons(0);

    printAdditionalProjSection();

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print end_form();
}


############################################################################
# getPage - Wrapper for page parameter, in case POST instead of GET
#   method is used.  This is an adaptor function.
############################################################################
sub getPage {
   my $page = param1( "page" );
   return $page;
}


############################################################################# 
# AdvancedSearch - advanced search
############################################################################# 
sub AdvancedSearch {
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Projects by Query Condition</h2>\n"; 

    print "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    # gold stamp ID 
    ProjectInfo::printFilterCond('GOLD Stamp ID', 'gold_filter:gold_stamp_id',
				 'text', '', '', '');

    # ncbi_project_id
    ProjectInfo::printFilterCond('NCBI Project ID',
				 'gold_filter:ncbi_project_id', 
				 'number', '', '', '');

    # ncbi_project_name 
    ProjectInfo::printFilterCond('NCBI Project Name',
				 'gold_filter:ncbi_project_name', 
				 'text', '', '', '');

    # gold web page code 
    ProjectInfo::printFilterCond('GOLD Web Page Code', 'gold_filter:web_page_code',
				 'select', 'query',
				 "select term_oid+1, description " .
				 "from web_page_codecv order by term_oid",
				 '');

    # project type 
    ProjectInfo::printFilterCond('Project Type', 'gold_filter:project_type', 
				 'select', 'query', 
				 'select cv_term from project_typecv order by cv_term', 
				 '');
 
    # project status 
    ProjectInfo::printFilterCond('Project Status', 'gold_filter:project_status', 
				 'select', 'query', 
				 'select cv_term from project_statuscv order by cv_term', 
				 '');
 
    # domain 
    ProjectInfo::printFilterCond('Domain', 'gold_filter:domain',
				 'select', 'list',
				 'ARCHAEAL|BACTERIAL|EUKARYAL|MICROBIAL|PLASMID|VIRUS',
				 '');
 
    # seq status 
    ProjectInfo::printFilterCond('Sequencing Status', 'gold_filter:seq_status',
				 'select', 'query', 
				 'select cv_term from seq_statuscv order by cv_term',
				 '');
 
    # seq method 
    ProjectInfo::printFilterCond('Sequencing Method', 'gold_filter:seq_method',
				 'select', 'query',
				 'select cv_term from seq_methodcv order by cv_term',
				 '');
 
    # availability: Proprietary, Public
    ProjectInfo::printFilterCond('Availability', 'gold_filter:availability', 
				 'select', 'list', 'Proprietary|Public', '');

    # phylogeny 
    ProjectInfo::printFilterCond('Category', 'gold_filter:phylogeny', 
				 'select', 'query', 
				 'select cv_term from phylogenycv order by cv_term', 
				 '');

    # project display name 
    ProjectInfo::printFilterCond('Project Display Name', 'gold_filter:display_name', 
				 'text', '', '', '');
 
    # genus 
    ProjectInfo::printFilterCond('Genus', 'gold_filter:genus', 
				 'text', '', '', '');
 
    # species 
    ProjectInfo::printFilterCond('Species', 'gold_filter:species', 
				 'text', '', '', '');
 
    # common_name 
    ProjectInfo::printFilterCond('Common Name', 'gold_filter:common_name', 
				 'text', '', '', '');

    # add date
    ProjectInfo::printFilterCond('Add Date', 'gold_filter:add_date',
				 'date', '', '', '');
 
    # mod date
    ProjectInfo::printFilterCond('Last Mod Date', 'gold_filter:mod_date',
				 'date', '', '', '');

#    print "<p>GOLD Stamp ID:\n";
#    print nbsp(3);
#    print "<input type='text' name='gold_stamp_id' value='" .
#	"' size='10' maxLength='10'/>\n";

#    print "<h4>Select a GOLD Stamp ID</h4>\n";
#    my @gold_ids = db_getValues("select gold_stamp_id from project_info order by gold_stamp_id");
#    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
#    for my $id0 ( @gold_ids ) {
#        print "    <option value='$id0' >$id0</option>\n";
#    }
#    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldMetaProj:advSearchResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}





########################################################################## 
# ShowAdvSearchResultPage
########################################################################## 
sub ShowAdvSearchResultPage{
    my $url=url(); 
 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Search Result</h2>\n"; 

    my $filter_cond = goldFilterCondition(); 
 
    my $select_cnt = 0;
    if ( $isAdmin eq 'Yes' ) {
        $select_cnt = selectedGoldProjCount( '', $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 
    else { 
        $select_cnt = selectedGoldProjCount($contact_oid, $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 

    if ( $select_cnt == 0 ) {
	print "<p>\n";
	printHomeLink();

	print end_form(); 
	return;
    }

    printGoldMetaProjButtons();

    # show results
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
        }; 

    if ( ! blankStr($filter_cond) ) {
	$sql .= " where " . $filter_cond;
    }

    print "<p>\n";
    webLog("$sql\n");    
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>ER Project ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Category</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getMProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";

        if ( $cnt2 >= 1000 ) {
            last;
        } 
    }
    print "</table>\n"; 

    if ( $cnt2 < $select_cnt ) {
	print "<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    }
    else {
	print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    }

    $cur->finish(); 
    $dbh->disconnect(); 

    printGoldMetaProjButtons();

    printAdditionalProjSection();

    print "<p>\n";
    printHomeLink();
 
    print end_form(); 
}




1;



