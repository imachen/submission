#########################################################################
# RelSchema.pm - relational schema definition
#########################################################################
package RelSchema;

require Exporter;
@ISA = qw{ Exporter };
@EXPORT = qw{
	getProjectAuxTables
	getMetaProjectAuxTables
	getSampleAuxTables
	getAuxTableName
	getProjectAttrs
	def_Class
	def_Project_Info
	def_project_info_body_products
	def_project_info_body_sites
	def_project_info_cell_arrangement
	def_project_info_diseases
	def_project_info_habitat
	def_project_info_metabolism
	def_project_info_phenotypes
	def_project_info_project_relevance
	def_project_info_seq_centers
	def_project_info_seq_method
	def_project_info_funding_agencies
	def_project_info_data_links
	def_project_info_nprods_metadata
	def_project_info_publications
	def_project_info_energy_source
	def_project_info_cyano_metadata
        def_project_info_jgi_url
	def_meta_Project_Info
	def_meta_project_info_cell_arrangement
	def_meta_project_info_diseases
	def_meta_project_info_habitat
	def_meta_project_info_metabolism
	def_meta_project_info_phenotypes
	def_meta_project_info_project_relevance
	def_meta_project_info_seq_centers
	def_meta_project_info_seq_method
	def_meta_project_info_funding_agencies
	def_meta_project_info_data_links
	def_meta_project_info_publications
	def_meta_project_info_energy_source
        def_meta_project_jgi_url
	def_Submission
	def_submission_reads_file
	def_Env_Sample
	def_env_sample_diseases
	def_env_sample_habitat_type
	def_env_sample_metabolism
	def_env_sample_phenotypes
	def_env_sample_energy_source
	def_env_sample_misc_meta_data
	def_env_sample_seq_method
        def_env_sample_jgi_url
	def_Request_Account
	def_Cell_arrCv
	def_Cell_shapeCv
	def_ColorCv
	def_Cultured_typeCv
	def_Bin_MethodCv
	def_Body_SiteCv
	def_Body_SubSiteCv
	def_Body_ProductCv
	def_CountryCv
	def_DB_NameCv
	def_DiseaseCv
	def_Energy_sourceCv
	def_Funding_programCv
	def_HabitatCv
	def_Habitat_CategoryCv
	def_JGI_fundind_progCv
	def_JGI_final_deliverableCv
	def_EcosystemCv
	def_Ecosystem_CategoryCv
	def_Ecosystem_TypeCv
	def_Ecosystem_SubtypeCv
	def_Specific_EcosystemCv
	def_NCBI_Submit_statusCv
	def_MetabolismCv
	def_MotilityCv
	def_Organism_typeCv
	def_OxygenCv
	def_PhenotypeCv
	def_PhylogenyCv
	def_Project_statusCv
	def_Project_typeCv
	def_RelevanceCv
	def_SalinityCv
        def_Sci_progCv
	def_Seq_methodCv
	def_Seq_statusCv
	def_HMP_Project_statusCv
	def_BEI_statusCv
	def_Publication_journalCv
	def_Project_goalCv
	def_Seq_qualityCv
	def_SporulationCv
	def_Temp_rangeCv
	def_Uncultured_typeCv
	def_UnitCv
	def_Contact
        def_Analysis_Project
        def_Sequencing_Project
        def_ImgGold_Sequencing_Project
    };

use strict;
use warnings;
use CGI qw(:standard);
use RelTable;





###########################################################################
# getProjectAuxTables - get all auxiliary tables for Project_Info
###########################################################################
sub getProjectAuxTables {
    my @tables = ( 
		   'project_info_body_products',
		   'project_info_body_sites',
		   'project_info_cell_arrangement',
		   'project_info_diseases',
		   'project_info_habitat',
		   'project_info_metabolism',
		   'project_info_phenotypes',
		   'project_info_project_relevance',
		   'project_info_seq_method',
##		   'project_info_seq_centers',
##		   'project_info_funding_agencies',
		   'project_info_data_links',
##		   'project_info_publications',
		   'project_info_energy_source',
		   'project_info_cyano_metadata' );

    return @tables;
}


###########################################################################
# getMetaProjectAuxTables - get all auxiliary tables for 
#                           metagenome project
###########################################################################
sub getMetaProjectAuxTables {
    my @tables = ( 
		   'meta_project_info_cell_arrangement',
		   'meta_project_info_diseases',
		   'meta_project_info_habitat',
		   'meta_project_info_metabolism',
		   'meta_project_info_phenotypes',
		   'meta_project_info_project_relevance',
		   'meta_project_info_seq_method',
##		   'meta_project_info_seq_centers',
##		   'meta_project_info_funding_agencies',
		   'meta_project_info_data_links' );
##		   'meta_project_info_publications',
#		   'meta_project_info_energy_source' );

    return @tables;
}


###########################################################################
# getSampleAuxTables - get all auxiliary tables for Env_Sample
###########################################################################
sub getSampleAuxTables {
    my @tables = ( 'env_sample_diseases',
		   'env_sample_habitat_type',
		   'env_sample_metabolism',
		   'env_sample_phenotypes',
		   'env_sample_energy_source',
		   'env_sample_misc_meta_data',
		   'env_sample_seq_method' );

    return @tables;
}


###########################################################################
# getAuxTableName - get auxiliary table name for Project_Info
#                   set-valued attribute
###########################################################################
sub getAuxTableName {
    my ( $aname ) = @_;

    $aname = lc($aname);
    if ( $aname eq 'body_product' ) {
	return 'project_info_body_products';
    }
    elsif ( $aname eq 'sample_body_site' ) {
	return 'project_info_body_sites';
    }
    elsif ( $aname eq 'sample_body_subsite' ) {
	return 'project_info_body_sites';
    }
    elsif ( $aname eq 'cell_arrangement' ) {
	return 'project_info_cell_arrangement';
    }
    elsif ( $aname eq 'link_type' ) {
	return 'project_info_data_links';
    }
    elsif ( $aname eq 'db_name' ) {
	return 'project_info_data_links';
    }
    elsif ( $aname eq 'diseases' ) {
	return 'project_info_diseases';
    }
    elsif ( $aname eq 'energy_source' ) {
	return 'project_info_energy_source';
    }
    elsif ( $aname eq 'cyano_metadata' ) {
	return 'project_info_cyano_metadata';
    }
    elsif ( $aname eq 'habitat' ) {
	return 'project_info_habitat';
    }
    elsif ( $aname eq 'metabolism' ) {
	return 'project_info_metabolism';
    }
    elsif ( $aname eq 'nprods_metadata' ) {
	return 'project_info_nprods_metadata';
    }
    elsif ( $aname eq 'phenotypes' ) {
	return 'project_info_phenotypes';
    }
    elsif ( $aname eq 'project_relevance' ) {
	return 'project_info_project_relevance';
    }
    elsif ( $aname eq 'seq_method' ) {
	return 'project_info_seq_method';
    }

    return 0;
}


###########################################################################
# getProjectAttrs
###########################################################################
sub getProjectAttrs {
    my ($prop, $prefix) = @_;

    my @all_params = ();
    my $def_project = def_Project_Info(); 
    my @attrs = @{$def_project->{attrs}}; 
    for my $attr1 ( @attrs ) {
        if ( $attr1->{$prop} ) {
            my $name2 = $attr1->{name};
	    if ( defined $prefix && length($prefix) > 0 ) {
		$name2 = $prefix . ':' . $attr1->{name};
	    }
            push @all_params, ( $name2 ); 
        } 
    } 
 
    my @tables = getProjectAuxTables();
    for my $table ( @tables ) {
        my $def_aux = def_Class($table); 
        if ( ! $def_aux ) { 
            next; 
        } 
 
        my @attrs = @{$def_aux->{attrs}}; 
        for my $attr ( @attrs ) { 
            if ( $attr->{$prop} ) { 
                my $name2 = $attr->{name};
		if ( defined $prefix && length($prefix) > 0 ) {
		    $name2 = $prefix . ':' . $attr->{name};
		}

                push @all_params, ( $name2 );
            } 
        } 
    } 
 
    return @all_params; 
} 



###########################################################################
# def_class - return a class definition based on class_name
###########################################################################
sub def_Class {
    my ($class_name) = @_;
    my $class_def;

    if ( lc($class_name) eq 'request_account' ) {
	$class_def = def_Request_Account();
    }
    elsif ( lc($class_name) eq 'contact' ) {
	$class_def = def_Contact();
    }
    elsif ( lc($class_name) eq 'submission' ) {
	$class_def = def_Submission();
    }
    elsif ( lc($class_name) eq 'submission_reads_file' ) {
	$class_def = def_submission_reads_file();
    }
    elsif ( lc($class_name) eq 'env_sample' ) {
	$class_def = def_Env_Sample();
    }
    elsif ( lc($class_name) eq 'env_sample_energy_source' ) {
	$class_def = def_env_sample_energy_source();
    }
    elsif ( lc($class_name) eq 'env_sample_diseases' ) {
	$class_def = def_env_sample_diseases();
    }
    elsif ( lc($class_name) eq 'env_sample_habitat_type' ) {
	$class_def = def_env_sample_habitat_type();
    }
    elsif ( lc($class_name) eq 'env_sample_metabolism' ) {
	$class_def = def_env_sample_metabolism();
    }
    elsif ( lc($class_name) eq 'env_sample_phenotypes' ) {
	$class_def = def_env_sample_phenotypes();
    }
    elsif ( lc($class_name) eq 'env_sample_misc_meta_data' ) {
	$class_def = def_env_sample_misc_meta_data();
    }
    elsif ( lc($class_name) eq 'env_sample_seq_method' ) {
	$class_def = def_env_sample_seq_method();
    }
    elsif ( lc($class_name) eq 'env_sample_jgi_url' ) {
	$class_def = def_env_sample_jgi_url();
    }
    elsif ( lc($class_name) eq 'cell_arrcv' ) {
	$class_def = def_Cell_arrCv();
    }
    elsif ( lc($class_name) eq 'cell_shapecv' ) {
	$class_def = def_Cell_shapeCv();
    }
    elsif ( lc($class_name) eq 'colorcv' ) {
	$class_def = def_ColorCv();
    }
    elsif ( lc($class_name) eq 'bin_methodcv' ) {
	$class_def = def_Bin_MethodCv();
    }
    elsif ( lc($class_name) eq 'body_sitecv' ) {
	$class_def = def_Body_SiteCv();
    }
    elsif ( lc($class_name) eq 'body_subsitecv' ) {
	$class_def = def_Body_SubSiteCv();
    }
    elsif ( lc($class_name) eq 'body_productcv' ) {
	$class_def = def_Body_ProductCv();
    }
    elsif ( lc($class_name) eq 'countrycv' ) {
	$class_def = def_CountryCv();
    }
    elsif ( lc($class_name) eq 'collaboratorcv' ) {
	$class_def = def_CollaboratorCv();
    }
    elsif ( lc($class_name) eq 'db_namecv' ) {
	$class_def = def_DB_NameCv();
    }
    elsif ( lc($class_name) eq 'diseasecv' ) {
	$class_def = def_DiseaseCv();
    }
    elsif ( lc($class_name) eq 'energy_sourcecv' ) {
	$class_def = def_Energy_sourceCv();
    }
    elsif ( lc($class_name) eq 'funding_programcv' ) {
	$class_def = def_Funding_programCv();
    }
    elsif ( lc($class_name) eq 'habitatcv' ) {
	$class_def = def_HabitatCv();
    }
    elsif ( lc($class_name) eq 'habitat_categorycv' ) {
	$class_def = def_Habitat_CategoryCv();
    }
    elsif ( lc($class_name) eq 'jgi_funding_progcv' ) {
	$class_def = def_JGI_funding_progCv();
    }
    elsif ( lc($class_name) eq 'jgi_final_deliverablecv' ) {
	$class_def = def_JGI_final_deliverableCv();
    }
    elsif ( lc($class_name) eq 'cvecosystem' ) {
	$class_def = def_EcosystemCv();
    }
    elsif ( lc($class_name) eq 'cvecosystem_category' ) {
	$class_def = def_Ecosystem_CategoryCv();
    }
    elsif ( lc($class_name) eq 'cvecosystem_type' ) {
	$class_def = def_Ecosystem_TypeCv();
    }
    elsif ( lc($class_name) eq 'cvecosystem_subtype' ) {
	$class_def = def_Ecosystem_SubtypeCv();
    }
    elsif ( lc($class_name) eq 'cvspecific_ecosystem' ) {
	$class_def = def_Specific_EcosystemCv();
    }
    elsif ( lc($class_name) eq 'hmp_project_statuscv' ) {
	$class_def = def_HMP_Project_StatusCv();
    }
    elsif ( lc($class_name) eq 'metabolismcv' ) {
	$class_def = def_MetabolismCv();
    }
    elsif ( lc($class_name) eq 'motilitycv' ) {
	$class_def = def_MotilityCv();
    }
    elsif ( lc($class_name) eq 'ncbi_submit_statuscv' ) {
	$class_def = def_NCBI_Submit_StatusCv();
    }
    elsif ( lc($class_name) eq 'organism_typecv' ) {
	$class_def = def_Organism_typeCv();
    }
    elsif ( lc($class_name) eq 'oxygencv' ) {
	$class_def = def_OxygenCv();
    }
    elsif ( lc($class_name) eq 'phenotypecv' ) {
	$class_def = def_PhenotypeCv();
    }
    elsif ( lc($class_name) eq 'phylogenycv' ) {
	$class_def = def_PhylogenyCv();
    }
    elsif ( lc($class_name) eq 'project_info' ) {
	$class_def = def_Project_Info();
    }
    elsif ( lc($class_name) eq 'project_info_body_products' ) {
	$class_def = def_project_info_body_products();
    }
    elsif ( lc($class_name) eq 'project_info_body_sites' ) {
	$class_def = def_project_info_body_sites();
    }
    elsif ( lc($class_name) eq 'project_info_cell_arrangement' ) {
	$class_def = def_project_info_cell_arrangement();
    }
    elsif ( lc($class_name) eq 'project_info_diseases' ) {
	$class_def = def_project_info_diseases();
    }
    elsif ( lc($class_name) eq 'project_info_habitat' ) {
	$class_def = def_project_info_habitat();
    }
    elsif ( lc($class_name) eq 'project_info_metabolism' ) {
	$class_def = def_project_info_metabolism();
    }
    elsif ( lc($class_name) eq 'project_info_nprods_metadata' ) {
	$class_def = def_project_info_nprods_metadata();
    }
    elsif ( lc($class_name) eq 'project_info_phenotypes' ) {
	$class_def = def_project_info_phenotypes();
    }
    elsif ( lc($class_name) eq 'project_info_project_relevance' ) {
	$class_def = def_project_info_project_relevance();
    }
    elsif ( lc($class_name) eq 'project_info_seq_centers' ) {
	$class_def = def_project_info_seq_centers();
    }
    elsif ( lc($class_name) eq 'project_info_seq_method' ) {
	$class_def = def_project_info_seq_method();
    }
    elsif ( lc($class_name) eq 'project_info_collaborators' ) {
	$class_def = def_project_info_collaborators();
    }
    elsif ( lc($class_name) eq 'project_info_funding_agencies' ) {
	$class_def = def_project_info_funding_agencies();
    }
    elsif ( lc($class_name) eq 'project_info_data_links' ) {
	$class_def = def_project_info_data_links();
    }
    elsif ( lc($class_name) eq 'project_info_jgi_url' ) {
	$class_def = def_project_info_jgi_url();
    }
    elsif ( lc($class_name) eq 'project_info_publications' ) {
	$class_def = def_project_info_publications();
    }
    elsif ( lc($class_name) eq 'project_info_energy_source' ) {
	$class_def = def_project_info_energy_source();
    }
    elsif ( lc($class_name) eq 'project_info_cyano_metadata' ) {
	$class_def = def_project_info_cyano_metadata();
    }
    elsif ( lc($class_name) eq 'meta_project_info' ) {
	$class_def = def_meta_Project_Info();
    }
    elsif ( lc($class_name) eq 'meta_project_info_cell_arrangement' ) {
	$class_def = def_meta_project_info_cell_arrangement();
    }
    elsif ( lc($class_name) eq 'meta_project_info_diseases' ) {
	$class_def = def_meta_project_info_diseases();
    }
    elsif ( lc($class_name) eq 'meta_project_info_habitat' ) {
	$class_def = def_meta_project_info_habitat();
    }
    elsif ( lc($class_name) eq 'meta_project_info_metabolism' ) {
	$class_def = def_meta_project_info_metabolism();
    }
    elsif ( lc($class_name) eq 'meta_project_info_phenotypes' ) {
	$class_def = def_meta_project_info_phenotypes();
    }
    elsif ( lc($class_name) eq 'meta_project_info_project_relevance' ) {
	$class_def = def_meta_project_info_project_relevance();
    }
    elsif ( lc($class_name) eq 'meta_project_info_seq_centers' ) {
	$class_def = def_meta_project_info_seq_centers();
    }
    elsif ( lc($class_name) eq 'meta_project_info_seq_method' ) {
	$class_def = def_meta_project_info_seq_method();
    }
    elsif ( lc($class_name) eq 'meta_project_info_funding_agencies' ) {
	$class_def = def_meta_project_info_funding_agencies();
    }
    elsif ( lc($class_name) eq 'meta_project_info_data_links' ) {
	$class_def = def_meta_project_info_data_links();
    }
    elsif ( lc($class_name) eq 'meta_project_info_jgi_url' ) {
	$class_def = def_meta_project_info_jgi_url();
    }
    elsif ( lc($class_name) eq 'meta_project_info_publications' ) {
	$class_def = def_meta_project_info_publications();
    }
    elsif ( lc($class_name) eq 'meta_project_info_energy_source' ) {
	$class_def = def_meta_project_info_energy_source();
    }
    elsif ( lc($class_name) eq 'project_statuscv' ) {
	$class_def = def_Project_statusCv();
    }
    elsif ( lc($class_name) eq 'project_typecv' ) {
	$class_def = def_Project_typeCv();
    }
    elsif ( lc($class_name) eq 'relevancecv' ) {
	$class_def = def_RelevanceCv();
    }
    elsif ( lc($class_name) eq 'salinitycv' ) {
	$class_def = def_SalinityCv();
    }
    elsif ( lc($class_name) eq 'sci_progcv' ) {
	$class_def = def_Sci_progCv();
    }
    elsif ( lc($class_name) eq 'seq_methodcv' ) {
	$class_def = def_Seq_methodCv();
    }
    elsif ( lc($class_name) eq 'seq_centercv' ) {
	$class_def = def_Seq_centerCv();
    }
    elsif ( lc($class_name) eq 'seq_qualitycv' ) {
	$class_def = def_Seq_qualityCv();
    }
    elsif ( lc($class_name) eq 'seq_statuscv' ) {
	$class_def = def_Seq_statusCv();
    }
    elsif ( lc($class_name) eq 'sporulationcv' ) {
	$class_def = def_SporulationCv();
    }
    elsif ( lc($class_name) eq 'temp_rangecv' ) {
	$class_def = def_Temp_rangeCv();
    }
    elsif ( lc($class_name) eq 'unitcv' ) {
	$class_def = def_UnitCv();
    }
    elsif ( lc($class_name) eq 'project_goalcv' ) {
	$class_def = def_Project_goalCv();
    }
    elsif ( lc($class_name) eq 'bei_statuscv' ) {
	$class_def = def_BEI_statusCv();
    }
    elsif ( lc($class_name) eq 'publication_journalcv' ) {
	$class_def = def_Publication_journalCv();
    }

    return $class_def;
}


#########################################################################
# def_Request_Account
#########################################################################
sub def_Request_Account {
    my $table = new RelTable( );
    $table->{name} = 'request_account';
    $table->{display_name} = 'Request IMG Account';
    $table->{id} = 'request_oid';
    $table->{multipart_form} = 0;

    # request_oid int not null
    my $attr = new RelAttr( );
    $attr->{name} = 'request_oid';
    $attr->{display_name} = 'Request ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # name
    $attr = new RelAttr( );
    $attr->{name} = 'name';
    $attr->{display_name} = 'Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # title
    $attr = new RelAttr( );
    $attr->{name} = 'title';
    $attr->{display_name} = 'Title';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # department
    $attr = new RelAttr( );
    $attr->{name} = 'department';
    $attr->{display_name} = 'Department';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # email
    $attr = new RelAttr( );
    $attr->{name} = 'email';
    $attr->{display_name} = 'Email';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # phone
    $attr = new RelAttr( );
    $attr->{name} = 'phone';
    $attr->{display_name} = 'Phone Number(s)';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # organization
    $attr = new RelAttr( );
    $attr->{name} = 'organization';
    $attr->{display_name} = 'Organization';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # address
    $attr = new RelAttr( );
    $attr->{name} = 'address';
    $attr->{display_name} = 'Address';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # city
    $attr = new RelAttr( );
    $attr->{name} = 'city';
    $attr->{display_name} = 'City';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # state
    $attr = new RelAttr( );
    $attr->{name} = 'state';
    $attr->{display_name} = 'State';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # country
    $attr = new RelAttr( );
    $attr->{name} = 'country';
    $attr->{display_name} = 'Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # username
    $attr = new RelAttr( );
    $attr->{name} = 'username';
    $attr->{display_name} = 'Preferred User Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 20;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # group_name
    $attr = new RelAttr( );
    $attr->{name} = 'group_name';
    $attr->{display_name} = 'Group (if known)';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # status
    $attr = new RelAttr( );
    $attr->{name} = 'status';
    $attr->{display_name} = 'Request Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from request_statuscv";
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # notes
    $attr = new RelAttr( );
    $attr->{name} = 'notes';
    $attr->{display_name} = 'Notes (for admin use)';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Reason(s) for Request';
    $attr->{data_type} = 'text';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    # assign_contact_oid int,
    $attr = new RelAttr( );
    $attr->{name} = 'assign_contact_oid';
    $attr->{display_name} = 'Assign Contact OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # request_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'request_date';
    $attr->{display_name} = 'Request Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # mod_date date
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Date Last Modified';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#########################################################################
# def_Submission
#########################################################################
sub def_Submission {
    my $table = new RelTable( );
    $table->{name} = 'submission';
    $table->{display_name} = 'IMG Submission';
    $table->{id} = 'submission_id';
    $table->{multipart_form} = 1;

    # submission_id varchar2(255) not null
    my $attr = new RelAttr( );
    $attr->{name} = 'submission_id';
    $attr->{display_name} = 'ER Submission ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # project_info int
    $attr = new RelAttr( );
    $attr->{name} = 'project_info';
    $attr->{display_name} = 'ER Submission Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_oid int
    $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'ER Submission Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # project_info varchar2(20)
    $attr = new RelAttr( );
    $attr->{name} = 'analysis_project_id';
    $attr->{display_name} = 'Analysis Project ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # remove subtitle (4/26/2018)
    # subtitle varchar2(80),
#    $attr = new RelAttr( );
#    $attr->{name} = 'subtitle';
#    $attr->{display_name} = 'Subtitle';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 80;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{tooltip} = "Free text. This will be added to the project name (e.g. Draft1, Second assembly, Mitochondrion, Chloroplast etc.) to help identify the submission.";
#    $attr->{tab} = 'Submission Information';
#    push @{$table->{attrs}}, ( $attr );

    # database
    $attr = new RelAttr( );
    $attr->{name} = 'database';
    $attr->{display_name} = 'Target ER System';
#    $attr->{data_type} = 'cv2';
    $attr->{data_type} = 'char';
#    $attr->{cv_query} = "select cv_term, description from imgcv";
    $attr->{length} = 50;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "The IMG system that these data will appear. IMG ER contains isolate genomes while IMG MER metagenomic samples.";
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # jgi_project_id int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'jgi_project_id';
#    $attr->{display_name} = 'JGI Project ID';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{tooltip} = "The JGI project ID for the submitted isolate genome or metagenomic sample (e.g.4005034). This ID is required for projects sequenced at the JGI and can be obtained by your project manager.";
#    $attr->{tab} = 'Submission Information';
#    push @{$table->{attrs}}, ( $attr );

    # is_jgi
    $attr = new RelAttr( );
    $attr->{name} = 'is_jgi';
    $attr->{display_name} = 'Is JGI submission?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # jat_key_id
    $attr = new RelAttr( );
    $attr->{name} = 'jat_key_id';
    $attr->{display_name} = 'JAT Key ID?';
    $attr->{data_type} = 'text';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # is_img_public
    $attr = new RelAttr( );
    $attr->{name} = 'is_img_public';
    $attr->{display_name} = 'Is public in IMG database?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select Yes if the genome/sample you submit will be visible by anyone logged in IMG. If you select No, this genome/sample will be visible only by the submitter and other users that the submitter specifies.";
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # seq_status
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Draft|Finished|Permanent Draft";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "The sequencing status of the project. Select Finished if this is an isolate genome that has been finished and the gaps have been closed. A Draft is a project that some segments are missing or are in the wrong order or are oriented incorrectly, work is being performed on this project to improve its quality. A Permanent Draft is a draft project that is done and no work is being performed to improve it.";
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # replace_taxon_oid int,
    $attr = new RelAttr( );
    $attr->{name} = 'replace_taxon_oid';
    $attr->{display_name} = 'Replacing Taxon OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Provide the taxon_oid (a numeric identifier) that corresponds to an existing version of the same genome/sample in IMG ER/IMG MER, which will be replaced by the current submission. IMG will try to map existing manual annotation and curation from the old version to the new version of the dataset.";
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # genbank file name
    $attr = new RelAttr( );
    $attr->{name} = 'genbank_file';
    $attr->{display_name} = 'Genbank or EMBL annotation file';
    $attr->{data_type} = 'file';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tooltip_file} = 'genbank_file.html';
    $attr->{tooltip} = "Use Browse to locate the file with the data for the submission in your local hard drive. Alternatively, provide a URL (ftp or http); e.g., <u><i>ftp://ftp.myserver.com/myproject.gbk</i></u>. Please make sure the file is accessible online. A Genbank or EMBL format file contain the nucleotide sequence of a contig/chromosome with additional information of the location and annotation of predicted features on them (e.g. CDS, RNA genes etc). Click here (http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html) to see a sample Genbank file. Click here (http://www.ebi.ac.uk/embl/Documentation/User_manual/usrman.html) for a  sample EMBL file.";
    $attr->{tab} = 'Submit annotated file';
    push @{$table->{attrs}}, ( $attr );

    # assembled sequences
    $attr = new RelAttr( );
    $attr->{name} = 'assembled_seq_file';
    $attr->{display_name} = 'Assembled sequences';
    $attr->{data_type} = 'file';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tooltip_file} = 'assembled_seq_file.html';
    $attr->{tooltip} = "Provide a fasta (http://www.ncbi.nlm.nih.gov/BLAST/fasta.shtml)  file with the nucleotide sequences of the assembled contigs.";
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # assembler used
#    $attr = new RelAttr( );
#    $attr->{name} = 'assembler_used';
#    $attr->{display_name} = 'Assembler used';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{indent} = 1;
#    $attr->{tab} = 'Submit sequence file';
#    push @{$table->{attrs}}, ( $attr );

    # reads_for_assembly
#    $attr = new RelAttr( );
#    $attr->{name} = 'reads_for_assembly';
#    $attr->{display_name} = 'Reads used in assembly';
#    $attr->{data_type} = 'file';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{indent} = 1;
#    $attr->{tooltip} = "Provide a fasta file with the nucleotide sequences of the reads used in the assembly (i.e., to produce the sequences submitted in the previous field) OR a SAM file with the mapping of the reads to the assembled contigs.  Alternatively, provide a URL (ftp or http) e.g. <u><i>ftp://ftp.myserver.com/myproject.fq</i></u>. These reads will be used for tools in the IMG system such as the SNP identifier.";
#    $attr->{tab} = 'Submit sequence file';
#    push @{$table->{attrs}}, ( $attr );

    # unassembled 454
    ## We no longer support unassembled 454 per Torben (1/4/2016)
#    $attr = new RelAttr( );
#    $attr->{name} = 'unassembled_454_file';
#    $attr->{display_name} = 'Unassembled 454 sequences';
#    $attr->{data_type} = 'file';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tooltip_file} = 'unassembled_454_file.html';
#    $attr->{tooltip} = "Provide a fasta file with the nucleotide sequences of the reads, sequenced using any 454 technology (GS20, FLX, Titanium etc)  that were not included in the assembled contigs.";
#    $attr->{tab} = 'Submit sequence file';
#    push @{$table->{attrs}}, ( $attr );

    # unassembled 454 quality trimmed?
#    $attr = new RelAttr( );
#    $attr->{name} = 'unassembled_454_quality_trim';
#    $attr->{display_name} = 'Unassembled 454 Quality trimmed?';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from yesnoonlycv";
#    $attr->{length} = 10;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{indent} = 1;
#    $attr->{tab} = 'Submit sequence file';
#    push @{$table->{attrs}}, ( $attr );

    # trimming method
#    $attr = new RelAttr( );
#    $attr->{name} = 'unassembled_454_trim_method';
#    $attr->{display_name} = 'Trimming method';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{indent} = 1;
#    $attr->{tab} = 'Submit sequence file';
#    push @{$table->{attrs}}, ( $attr );

    # unassembled Illumina
    $attr = new RelAttr( );
    $attr->{name} = 'unassembled_illumina_file';
    $attr->{display_name} = 'Unassembled Illumina sequences';
    $attr->{data_type} = 'file';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tooltip_file} = 'unassembled_illumina_file.html';
    $attr->{tooltip} = "Provide a fasta file with the nucleotide sequences of the reads, sequenced using any Illumina technology that were not included in the assembled contigs.";
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # unassembled illumina quality trimmed?
    $attr = new RelAttr( );
    $attr->{name} = 'unassembled_illum_quality_trim';
    $attr->{display_name} = 'Unassembled Illumina Quality trimmed?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{indent} = 1;
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # trimming method
    $attr = new RelAttr( );
    $attr->{name} = 'unassembled_illum_trim_method';
    $attr->{display_name} = 'Trimming method';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{indent} = 1;
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # unassembled pacbio
    $attr = new RelAttr( );
    $attr->{name} = 'unassembled_pacbio_file';
    $attr->{display_name} = 'Unassembled PacBio sequences';
    $attr->{data_type} = 'file';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # scaffold_count int not null,
#    $attr = new RelAttr( );
#    $attr->{name} = 'scaffold_count';
#    $attr->{display_name} = 'Number of Scaffolds';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 1;
#    $attr->{can_edit} = 1;
#    $attr->{tab} = 'Submission';
#    push @{$table->{attrs}}, ( $attr );

    # status
    $attr = new RelAttr( );
    $attr->{name} = 'status';
    $attr->{display_name} = 'Submission Status';
    $attr->{data_type} = 'cv2';
    $attr->{cv_query} = "select term_oid, cv_term from submission_statuscv order by term_oid";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # img_dev_flag
    $attr = new RelAttr( );
    $attr->{name} = 'img_dev_flag';
    $attr->{display_name} = 'Submitted to internal development database only?';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "No|Yes";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select Yes if you want to submit data to the internal development database.";
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # img_product_flag
    # Nikos requested to always have this flag set to Yes (7/10/2014)
    $attr = new RelAttr( );
    $attr->{name} = 'img_product_flag';
    $attr->{display_name} = 'Product Name';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv where cv_term = 'Yes'";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip_file} = 'img_product_flag.html';
    $attr->{tooltip} = "Select Yes if you want IMG to compute the product name of the genes. If you have submitted an annotated file (genbank/embl) that includes product name in the annotation of genes and you select Yes in this option the submitted unknown or hypothetical product names will be overriden.";
    $attr->{tab} = 'Functional annotation options';
    push @{$table->{attrs}}, ( $attr );

    # img_ec_flag
    $attr = new RelAttr( );
    $attr->{name} = 'img_ec_flag';
    $attr->{display_name} = 'E.C. number';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select Yes if you want IMG to predict the E.C. number of the proteins in the submitted sequence. If you have submitted a genbank file that includes EC numbers in the annotation of genes and you select Yes in this option the submitted information will be overridden.";
    $attr->{tab} = 'Functional annotation options';
    push @{$table->{attrs}}, ( $attr );

    # remove_duplicate_flag
    $attr = new RelAttr( );
    $attr->{name} = 'remove_duplicate_flag';
    $attr->{display_name} = 'Remove technical duplicates?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip_file} = "remove_duplicate_flag.html";
    $attr->{tooltip} = "Select yes if you want IMG to remove technical duplicates for reads produced by any 454 technology. For more information click here (http://www.ncbi.nlm.nih.gov/pubmed/19587772).";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # cluster_seq
    $attr = new RelAttr( );
    $attr->{name} = 'cluster_seq';
    $attr->{display_name} = 'Cluster sequences?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select yes if you want IMG to cluster identical sequences. This removes the computationa redundancy and allows faster processing. Statistical tools that rely on copy number of genes take into account this clustering.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # quality_based_trim
    $attr = new RelAttr( );
    $attr->{name} = 'quality_based_trim';
    $attr->{display_name} = 'Quality trimming';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select Yes if you want IMG to trimm the submitted nucleotide sequences of reads. The method for trimming is available here (URL to an IMG page).";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # seqeuence coverage file
    $attr = new RelAttr( );
    $attr->{name} = 'seq_coverage_file';
    $attr->{display_name} = 'Sequence coverage file for assembled sequences';
    $attr->{data_type} = 'file';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tooltip} = "Provide a file with the coverage of each assembled sequence. The file should be <i><u>tab delimited</u></i> with the following two columns: 1. contig name (ID), and 2. the coverage for this contig (Avg_fold). We assume uniform coverage for contigs. <b>Headers are required.</b> <p>Example: <p> ID      Avg_fold <br/>scaffold00001   43.5512 <br/>scaffold00002   43.0642";
    ## move to a different tab per Marcel (2/17/16)
##    $attr->{tab} = 'Advanced options';
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # gene_calling_flag
    $attr = new RelAttr( );
    $attr->{name} = 'gene_calling_flag';
    $attr->{display_name} = 'Gene calling method';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from genecallingcv order by cv_order";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select the gene calling pipeline you want to use. Isolate gene calling pipelie is described here(URL to a web page within the IMG space). Use this option if you are submitting contigs longer than 1KB to IMG ER (i.e. isolate genome), or to IMG MER (in cases of reference isolate genomes or contigs predicted to belong to the same organism). Metagenome gene caling is described here (URL to a web page within the IMG space). Use this option if you submit metagenomic samples, either assembled or unassembled sequences or combinations of both.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # binning_file varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'binning_file';
    $attr->{display_name} = 'Binning File';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
    $attr->{data_type} = 'file';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Provide a file with binning information for the submitted sequences. The file should be tab delimited with the following three columns: 1. contig name 2. bin name (e.g. name of a phylogenetic group or any label) 3. confidence score (optional, can be left blank).";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # bin_method varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'bin_method';
    $attr->{display_name} = 'Binning Method';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from bin_methodcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Binning method for a metagenome";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # species_code: varchar2(100)
    $attr = new RelAttr( );
    $attr->{name} = 'species_code';
    $attr->{display_name} = 'Locus tag prefix';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Provide the locus tag prefix for the submitted sequence. The locus tag prefix is a small text that used to name each gene and is provided by Genbank when a project is registered. For example, in Clostridium thermocellum genes have locus tags: Cthe_0001, Cthe_0002, Cthe_0003. In this example the locus tag prefix is: Cthe_. If you submit a draft genome it is recommended to add the text DRAFT in the locus tag prefix (in the previous example it would be: CtheDRAFT_).";
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # mol_topology varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'mol_topology';
    $attr->{display_name} = 'Topology';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "circular|linear";
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select circular if all the sequences in the submitted files correspond to circular replicons. In any other case leave this field as linear.";
    $attr->{tab} = 'Submit sequence file';
    push @{$table->{attrs}}, ( $attr );

    # genbank_id varchar2(128),
#    $attr = new RelAttr( );
#    $attr->{name} = 'genbank_id';
#    $attr->{display_name} = 'Genbank ID';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 128;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{tooltip} = "Genbank Accession ID (if any)";
#    $attr->{tab} = 'Submission Information';
#    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tooltip} = "Free text. Write comments about this submission. (e.g. First draft assembly of genome).";
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # stats_file varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'stats_file';
    $attr->{display_name} = 'Submission Statistics File';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # error_file varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'error_file';
    $attr->{display_name} = 'Error File';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # error_msg varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'error_msg';
    $attr->{display_name} = 'Error Message';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # admin_notes varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'admin_notes';
    $attr->{display_name} = "Administrator's Notes";
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # img_taxon_oid
    $attr = new RelAttr( );
    $attr->{name} = 'img_taxon_oid';
    $attr->{display_name} = 'IMG Taxon OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Submission Information';
    push @{$table->{attrs}}, ( $attr );

    # contact int
    $attr = new RelAttr( );
    $attr->{name} = 'contact';
    $attr->{display_name} = 'Submitter';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # approval_status varchar(80)
    $attr = new RelAttr( );
    $attr->{name} = 'approval_status';
    $attr->{display_name} = 'Approval Status';
    $attr->{data_type} = 'list';
    $attr->{list_values} = 'approved|missing metadata|on hold|pending review';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # approved_by int
    $attr = new RelAttr( );
    $attr->{name} = 'approved_by';
    $attr->{display_name} = 'Approved/Updated By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # approval_date
    $attr = new RelAttr( );
    $attr->{name} = 'approval_date';
    $attr->{display_name} = 'Approval Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # loaded into img?
    $attr = new RelAttr( );
    $attr->{name} = 'loaded_into_img';
    $attr->{display_name} = "Loaded into IMG?";
    $attr->{data_type} = 'list';
    $attr->{list_values} = 'Yes|No';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # in file?
    $attr = new RelAttr( );
    $attr->{name} = 'in_file';
    $attr->{display_name} = "Loaded into File System?";
    $attr->{data_type} = 'list';
    $attr->{list_values} = 'Yes|No';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # submission_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'submission_date';
    $attr->{display_name} = 'Submission Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Last Modify Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Last Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    # contigs_file varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'contigs_file';
    $attr->{display_name} = 'Contigs File';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "A fasta file with the nucleotide sequences of the contigs. Use this field when you submit metagenomic samples.";
    $attr->{tab} = 'Additional Metagenome Files';
    push @{$table->{attrs}}, ( $attr );

    # singlets_file varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'singlets_file';
    $attr->{display_name} = 'Singlets File';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "A fasta file with the nucleotide sequences of the unassembled. Use this field when you submit metagenomic samples.";
    $attr->{tab} = 'Additional Metagenome Files';
    push @{$table->{attrs}}, ( $attr );

    # additional fields for Portal publishing
    # all_reads varchar2(512)
    $attr = new RelAttr( );
    $attr->{name} = 'all_reads';
    $attr->{display_name} = 'File with all the reads (JGI Project only)';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "File with all the reads used in this project (including reads that were assembled ans singletons. It is advised that this file has been processed to remove reads from contamination). This is for JGI Project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # assembly_qd_report varchar2(512)
    $attr = new RelAttr( );
    $attr->{name} = 'assembly_qd_report';
    $attr->{display_name} = 'Technical report of assembly QD (JGI Project only)';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Technical report of assembly QD. This is for JGI project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # assembly_qc_report varchar2(512)
    $attr = new RelAttr( );
    $attr->{name} = 'assembly_qc_report';
    $attr->{display_name} = 'Summary report of assembly (JGI Project only)';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Summary report of assembly. This is for JGI project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # info_file varchar2(512)
    $attr = new RelAttr( );
    $attr->{name} = 'info_file';
    $attr->{display_name} = 'File with additional information (JGI Project only)';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "File with additional information (e.g. size, topology) of the contigs/scaffolds. This is for JGI project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # agp_file varchar2(512)
    $attr = new RelAttr( );
    $attr->{name} = 'agp_file';
    $attr->{display_name} = 'AGP File (JGI Project only)';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "File in agp format that describes the orientation of contigs on scaffolds. This is for JGI project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # add_to_portal
    $attr = new RelAttr( );
    $attr->{name} = 'add_to_portal';
    $attr->{display_name} = 'Publish Assembly Data in JGI Portal? (JGI Project only)';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tooltip} = "Select Yes if you want assembly data to be published in JGI Portal for downloading. This option is for JGI project only";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # portal_url varchar2(512)
    $attr = new RelAttr( );
    $attr->{name} = 'portal_url';
    $attr->{display_name} = 'JGI Portal URL for Assembly Data (JGI Project only)';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tooltip} = "JGI Portal URL for assembly data. This is for JGI project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # portal_release_date
    $attr = new RelAttr( );
    $attr->{name} = 'portal_release_date';
    $attr->{display_name} = 'Date When Assembly Data Published in JGI Portal(JGI Project only)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tooltip} = "Date when assembly data files are published in JGI Portal. This is for JGI project only.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # passed_quality_chk
    $attr = new RelAttr( );
    $attr->{name} = 'passed_quality_chk';
    $attr->{display_name} = 'Passed quality checking?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnoonlycv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tooltip} = "Select Yes if the submission passed quality checking.";
    $attr->{tab} = 'Advanced options';
    push @{$table->{attrs}}, ( $attr );

    # prev_submission_id int
    $attr = new RelAttr( );
    $attr->{name} = 'prev_submission_id';
    $attr->{display_name} = 'Previous Submission ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Database Info';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_submission_reads_file
###########################################################################
sub def_submission_reads_file {
    my $table = new RelTable( );
    $table->{name} = 'submission_reads_file';
    $table->{display_name} = 'READS Files';
    $table->{id} = 'submission_id';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;
    $table->{tooltip} = "A fasta file with the sequences of the sequence reads produced for this project. This information in conjunction with contigs and singlets sequences is necessary for sequence comparison tools such as SNP VISTA to work.";

    # submission_id int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'submission_id';
    $attr->{display_name} = 'Submission ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'reads_file';
    $attr->{display_name} = 'READS File';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    $attr->{tooltip} = "A fasta file with the sequences of the sequence reads produced for this project. This information in conjunction with contigs and singlets sequences is necessary for sequence comparison tools such as SNP VISTA to work.";
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#########################################################################
# def_Env_Sample
#########################################################################
sub def_Env_Sample {

    my $table = new RelTable( );
    $table->{name} = 'env_sample';
    $table->{display_name} = 'Environmental Sample';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'ER Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # proposal_name 
    $attr = new RelAttr( );
    $attr->{name} = 'proposal_name';
    $attr->{display_name} = 'Sample Proposal Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # gold_id varchar2(50),
    $attr = new RelAttr( );
    $attr->{name} = 'gold_id';
    $attr->{display_name} = 'GOLD Sample ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # legacy_gold_id varchar2(50),
    $attr = new RelAttr( );
    $attr->{name} = 'legacy_gold_id';
    $attr->{display_name} = 'Legacy GOLD Sample ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # project_info int,
    $attr = new RelAttr( );
    $attr->{name} = 'project_info';
    $attr->{display_name} = 'ER Study ID';
    $attr->{data_type} = 'int';
#    $attr->{data_type} = 'cv2';
    $attr->{cv_query} = "select project_oid, display_name from project_info order by project_oid";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_display_name varchar2(512),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_display_name';
    $attr->{display_name} = 'Sample Display Name';
    $attr->{tooltip} = "Please apply the GOLD conventions for sample naming.";
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    $attr->{hint} = '';
    push @{$table->{attrs}}, ( $attr );

    # submitters_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'submitters_name';
    $attr->{display_name} = 'Submitter\'s Name';
    $attr->{tooltip} = "Sample Name.";
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    $attr->{hint} = '';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_taxon_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_taxon_id';
    $attr->{display_name} = 'NCBI Taxonomy ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{header} = 'Metagenome Info';
    $attr->{tab} = 'Sample Information';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_id';
    $attr->{display_name} = 'NCBI Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{header} = 'Metagenome Info';
    $attr->{tab} = 'Sample Information';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/sites/entrez?db=genomeprj';
    $attr->{migs_id} = 'MIGS 1.1 ';
    $attr->{filter_cond} = 1;
    $attr->{migs_name} = 'PID'; 
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_name varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_name';
    $attr->{display_name} = 'NCBI Project Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_archive_id int, 
    $attr = new RelAttr( ); 
    $attr->{name} = 'ncbi_archive_id'; 
    $attr->{display_name} = 'NCBI Trace DB (Archive) ID';
    $attr->{data_type} = 'int'; 
    $attr->{length} = 40; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sample Information';
    $attr->{migs_id} = 'MIGS 1.2 ';
    $attr->{migs_name} = 'TRACE ARCHIVE'; 
    push @{$table->{attrs}}, ( $attr ); 

    # ncbi_archive_id int, 
    $attr = new RelAttr( ); 
    $attr->{name} = 'sra_id'; 
    $attr->{display_name} = 'NCBI SRA ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sample Information';
#    $attr->{migs_id} = 'MIGS 1.2 ';
#    $attr->{migs_name} = 'TRACE ARCHIVE'; 
    push @{$table->{attrs}}, ( $attr ); 

    # img_oid int,
    $attr = new RelAttr( );
    $attr->{name} = 'img_oid';
    $attr->{display_name} = 'IMG Object ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem';
    $attr->{display_name} = 'Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_category varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_category';
    $attr->{display_name} = 'Ecosystem Category';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_category from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_category order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_type';
    $attr->{display_name} = 'Ecosystem Type';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_type from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_type order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_subtype varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_subtype';
    $attr->{display_name} = 'Ecosystem Subtype';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_subtype from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_subtype order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # specific_ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'specific_ecosystem';
    $attr->{display_name} = 'Specific Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct specific_ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvspecific_ecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_description varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_description';
    $attr->{display_name} = 'Sample Description';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_site varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_site';
    $attr->{display_name} = 'Sampling Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{header} = 'Sample Collection Info';
    $attr->{tab} = 'Sample Information';
    $attr->{migs_id} = 'MIGS 13 ';
    $attr->{migs_name} = 'ISOLATION SOURCE';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # date_collected varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'date_collected';
    $attr->{display_name} = 'Sample Collection Date';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    $attr->{migs_id} = 'MIGS 5 '; 
    $attr->{migs_name} = 'COLLECTION DATE';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # sampling_strategy varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'sampling_strategy';
    $attr->{display_name} = 'Sampling Strategy';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # replicate_num int
    $attr = new RelAttr( );
    $attr->{name} = 'replicate_num';
    $attr->{display_name} = 'Replicate Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 4;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_volume varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_volume';
    $attr->{display_name} = 'Sample Volume';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # est_biomass varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'est_biomass';
    $attr->{display_name} = 'Sample Biomass';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # iso_source varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_source';
    $attr->{display_name} = 'Sample Source';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # est_diversity varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'est_diversity';
    $attr->{display_name} = 'Sample Diversity';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # iso_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_comments';
    $attr->{display_name} = 'Sample Isolation Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_link varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_link';
    $attr->{display_name} = 'Sample Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # iso_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_country';
    $attr->{display_name} = 'Sample Isolation Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Sample Information';
    $attr->{migs_id} = 'MIGS 5 '; 
    $attr->{migs_name} = 'COUNTRY'; 
    push @{$table->{attrs}}, ( $attr );

    # iso_pubmed_id int
    $attr = new RelAttr( );
    $attr->{name} = 'iso_pubmed_id';
    $attr->{display_name} = 'Sample Isolation Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    $attr->{migs_id} = 'MIGS 12 <font color=red>  </font>';
    $attr->{migs_name} = 'REFS FOR BIOMATERIAL';
    push @{$table->{attrs}}, ( $attr );

    # geo_location varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'geo_location';
    $attr->{display_name} = 'Geographic Location';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # latitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'latitude';
    $attr->{display_name} = 'Latitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{migs_id} = 'MIGS 4.1 ';
    $attr->{migs_name} = 'LATITUDE';
    $attr->{hint} = 'Please enter ONLY decimal coordinates.'; 
    push @{$table->{attrs}}, ( $attr );

    # longitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longitude';
    $attr->{display_name} = 'Longitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{migs_id} = 'MIGS 4.2 '; 
    $attr->{migs_name} = 'LONGITUDE'; 
    $attr->{hint} = 'Please enter ONLY decimal coordinates.'; 
    push @{$table->{attrs}}, ( $attr );

    # coordinate_type
    $attr = new RelAttr( );
    $attr->{name} = 'coordinate_type';
    $attr->{display_name} = 'Lat/Long Information';
    $attr->{data_type} = 'cv';
    $attr->{length} = 32;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from cvdata_validity_type";
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # altitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'altitude';
    $attr->{display_name} = 'Altitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{migs_id} = 'MIGS 4.3'; 
    $attr->{migs_name} = 'ALTITUDE'; 
    push @{$table->{attrs}}, ( $attr );

    # depth varchar2(255), 
    $attr = new RelAttr( );
    $attr->{name} = 'depth'; 
    $attr->{display_name} = 'Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0; 
    $attr->{can_edit} = 1; 
    $attr->{tab} = 'Environmental Metadata'; 
    $attr->{migs_id} = 'MIGS 4.4'; 
    $attr->{migs_name} = 'DEPTH'; 
    push @{$table->{attrs}}, ( $attr );
 
    # seq_center_name number,
     $attr = new RelAttr( );
     $attr->{name} = 'seq_center_name';
     $attr->{display_name} = 'Sequencing Center';
     $attr->{cv_query} = "select distinct name from luseq_center order by name";
     $attr->{data_type} = 'cv';
     $attr->{length} = 255;
     $attr->{filter_cond} = 1;
     $attr->{is_required} = 0;
     $attr->{tab} = 'Sequencing Information';
     push @{$table->{attrs}}, ( $attr );

    # seq_center_url number,
     $attr = new RelAttr( );
     $attr->{name} = 'seq_center_url';
     $attr->{display_name} = 'Sequencing Center URL';
     $attr->{data_type} = 'char';
     $attr->{length} = 512;
     $attr->{is_required} = 0;
     $attr->{tab} = 'Sequencing Information';
     push @{$table->{attrs}}, ( $attr );

    # seq_center_pid number,
    $attr = new RelAttr( );
    $attr->{name} = 'seq_center_pid';
    $attr->{display_name} = 'Sequencing Center Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # seq_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_statuscv";
    $attr->{default_value} = 'In progress';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 31.1';
    $attr->{migs_name} = 'SEQUENCING STATUS';
    push @{$table->{attrs}}, ( $attr );

    # seq_quality varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'seq_quality';
#    $attr->{display_name} = 'Sequencing Quality';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from seq_qualitycv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing Information';
#    push @{$table->{attrs}}, ( $attr );

    # seq_status_link varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status_link';
    $attr->{display_name} = 'Seq Status Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # seq_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_comments';
    $attr->{display_name} = 'Sequencing Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # seq_depth varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_depth';
    $attr->{display_name} = 'Sequencing Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 31.2'; 
    $attr->{migs_name} = 'SEQUENCING COVERAGE';
    push @{$table->{attrs}}, ( $attr );

    # vector varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'vector';
    $attr->{display_name} = 'Vector';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 28.3';
    $attr->{migs_name} = 'VECTOR';
    push @{$table->{attrs}}, ( $attr );

    # library_method varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'library_method';
    $attr->{display_name} = 'Library Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 28.1';
    $attr->{migs_name} = 'LIBRARY SIZE'; 
    push @{$table->{attrs}}, ( $attr );

    # assembly_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'assembly_method';
    $attr->{display_name} = 'Assembly Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 30 ';
    $attr->{migs_name} = 'ASSEMBLY';
    push @{$table->{attrs}}, ( $attr );

    # binning_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'binning_method';
    $attr->{display_name} = 'Binning Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # gene_calling_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'gene_calling_method';
    $attr->{display_name} = 'Gene Calling Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # chromosome_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'chromosome_count';
    $attr->{display_name} = 'Chromosome Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 9';
    $attr->{migs_name} = 'NUMBER OF REPLICONS';
    push @{$table->{attrs}}, ( $attr );

    # plasmid_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'plasmid_count';
    $attr->{display_name} = 'Plasmid Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 10'; 
    $attr->{migs_name} = 'EXTRACHROMOSOMAL ELEMENTS'; 
    push @{$table->{attrs}}, ( $attr );

    # reads_count int
    $attr = new RelAttr( );
    $attr->{name} = 'reads_count';
    $attr->{display_name} = 'Reads Count';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 28.2';
    $attr->{migs_name} = 'NUMBER OF READS';
    push @{$table->{attrs}}, ( $attr );

    # contig_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'contig_count';
    $attr->{display_name} = 'Contig Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # scaffold_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'scaffold_count';
    $attr->{display_name} = 'Scaffold Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # singlet_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'singlet_count';
    $attr->{display_name} = 'Singlet Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # gene_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'gene_count';
    $attr->{display_name} = 'Gene Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # gc_perc numeric(5,2),
    $attr = new RelAttr( );
    $attr->{name} = 'gc_perc';
    $attr->{display_name} = 'GC Percent';
    $attr->{data_type} = 'number';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # est_size int,
    $attr = new RelAttr( );
    $attr->{name} = 'est_size';
    $attr->{display_name} = 'Estimated Size';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing Information';
    $attr->{migs_id} = 'MIGS 11'; 
    $attr->{migs_name} = 'ESTIMATED SIZE'; 
    push @{$table->{attrs}}, ( $attr );

    # units varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'units';
    $attr->{display_name} = 'Units';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from unitcv";
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # seq_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_country';
    $attr->{display_name} = 'Sequencing Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Sequencing Information';
    push @{$table->{attrs}}, ( $attr );

    # sample_isolation varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_isolation';
    $attr->{display_name} = 'Sample Isolation Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # energy_source varchar2(500),
##    $attr = new RelAttr( );
##    $attr->{name} = 'energy_source';
##    $attr->{display_name} = 'Energy Source';
##    $attr->{data_type} = 'char';
##    $attr->{length} = 500;
##    $attr->{is_required} = 0;
##    $attr->{tab} = 'Sample';
##    push @{$table->{attrs}}, ( $attr );

    # oxygen_req varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'oxygen_req';
#    $attr->{display_name} = 'Oxygen Requirement';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from oxygencv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sample';
#    $attr->{migs_id} = 'MIGS 22'; 
#    $attr->{migs_name} = 'RELATIONSHIP TO OXYGEN';
#    push @{$table->{attrs}}, ( $attr );

    # temp_range varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'temp_range';
    $attr->{display_name} = 'Temperature Range';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from temp_rangecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # temp varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'temp';
    $attr->{display_name} = 'Temperature';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # salinity varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'salinity';
    $attr->{display_name} = 'Salinity';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from salinitycv";
    $attr->{length} = 255;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # pressure varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pressure';
    $attr->{display_name} = 'Pressure';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # ph varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ph';
    $attr->{display_name} = 'pH';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_ncbi_taxid int,
    $attr = new RelAttr( );
    $attr->{name} = 'host_ncbi_taxid';
    $attr->{display_name} = 'Host Taxonomy ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_name';
    $attr->{display_name} = 'Host Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Host Metadata';
    $attr->{migs_id} = 'MIGS 16'; 
    $attr->{migs_name} = 'SPECIFIC HOST'; 
    push @{$table->{attrs}}, ( $attr );

#     # host_taxon_id varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'host_taxon_id';
#     $attr->{display_name} = 'Host Taxon ID';
#     $attr->{data_type} = 'int';
#     $attr->{length} = 40;
#     $attr->{is_required} = 0;
#     $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
#     $attr->{tab} = 'Host Metadata';
#     push @{$table->{attrs}}, ( $attr );

    # MRN varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'mrn';
    $attr->{display_name} = 'Medical Record Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_gender varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_gender';
    $attr->{display_name} = 'Host Gender';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_race varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_race';
    $attr->{display_name} = 'Host Race';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_age varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_age';
    $attr->{display_name} = 'Host Age';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_health_condition varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'host_health_condition';
    $attr->{display_name} = 'Host Health Condition';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Host Metadata';
    $attr->{migs_id} = 'MIGS 18'; 
    $attr->{migs_name} = 'HEALTH STATUS OF HOST';
    push @{$table->{attrs}}, ( $attr );

    # visit_num int,
    $attr = new RelAttr( );
    $attr->{name} = 'visit_num';
    $attr->{display_name} = 'Patient Visit Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 4;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_medication varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_medication';
    $attr->{display_name} = 'Host Medication';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_body_site varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'body_site';
    $attr->{display_name} = 'Host Body Site';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from body_sitecv order by cv_term";
    $attr->{length} = 255;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_body_subsite varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'body_subsite';
    $attr->{display_name} = 'Host Body Subsite';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from body_subsitecv order by cv_term";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_body_product varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'body_product';
    $attr->{display_name} = 'Host Body Product';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from body_productcv order by cv_term";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_spec int
    $attr = new RelAttr( );
    $attr->{name} = 'host_spec';
    $attr->{display_name} = 'Host Specificity or Range';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{filter_cond} = 1;
    $attr->{migs_id} = 'MIGS 17';
    $attr->{migs_name} = 'HOST SPECIFICITY OR RANGE';
    push @{$table->{attrs}}, ( $attr );

    # host_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'host_comments';
    $attr->{display_name} = 'Host Comments';
    $attr->{data_type} = 'char';
    $attr->{filter_cond} = 1;
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    push @{$table->{attrs}}, ( $attr );

    # seq_method varchar2(255),
    # change to multi-valued
#    $attr = new RelAttr( );
#    $attr->{name} = 'seq_method';
#    $attr->{display_name} = 'Sequencing Method';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from seq_methodcv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metagenome';
#    push @{$table->{attrs}}, ( $attr );

    # growth_conditions varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'growth_conditions';
    $attr->{display_name} = 'Growth Conditions';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Additional Sample Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

#     # pub_journal varchar2(100),
#     $attr = new RelAttr( );
#     $attr->{name} = 'pub_journal';
#     $attr->{display_name} = 'Publication Journal';
#     $attr->{data_type} = 'cv';
#     $attr->{length} = 512;
#     $attr->{is_required} = 0;
#     $attr->{cv_query} = "select cv_term from publication_journalcv";
#     $attr->{tab} = 'Sample Information';
#     push @{$table->{attrs}}, ( $attr );

#     # pub_vol varchar2(100),
#     $attr = new RelAttr( );
#     $attr->{name} = 'pub_vol';
#     $attr->{display_name} = 'Publication Vol.';
#     $attr->{data_type} = 'char';
#     $attr->{length} = 100;
#     $attr->{is_required} = 0;
#     $attr->{tab} = 'Sample Information';
#     push @{$table->{attrs}}, ( $attr );

#     # pub_link varchar2(500)
#     $attr = new RelAttr( );
#     $attr->{name} = 'pub_link';
#     $attr->{display_name} = 'Publication Link';
#     $attr->{data_type} = 'char';
#     $attr->{length} = 500;
#     $attr->{is_required} = 0;
#     $attr->{tab} = 'Sample Information';
#     push @{$table->{attrs}}, ( $attr );

    # contact int
    $attr = new RelAttr( );
    $attr->{name} = 'contact';
    $attr->{display_name} = 'Contact Info';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{header} = 'Database Update Info';
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # add_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'add_date';
    $attr->{display_name} = 'Add Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Last Modify Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Last Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Sample Information';
    push @{$table->{attrs}}, ( $attr );

    # pmo_project_id_number int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmo_project_id';
    $attr->{display_name} = 'PMO Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scientific_program varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'scientific_program';
    $attr->{display_name} = 'Scientific Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sci_progcv";
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_final_deliverable varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_final_deliverable';
    $attr->{display_name} = 'ITS Final Deliverable';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvjgi_its_final_deliverable";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_product_name varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_product_name';
    $attr->{display_name} = 'JGI Product Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scope_of_work varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'scope_of_work';
    $attr->{display_name} = 'JGI Scope of Work';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select scope from workscopecv order by scope";
#    $attr->{tab} = 'Sequencing Information';
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_program varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_program';
    $attr->{display_name} = 'JGI Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from jgi_funding_progcv";
    $attr->{length} = 256;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_year int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_year';
    $attr->{display_name} = 'JGI Funding Year';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_project_type varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_project_type';
    $attr->{display_name} = 'JGI Project Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 256;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_status varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_status';
    $attr->{display_name} = 'JGI Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_last_mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_last_mod_date';
    $attr->{display_name} = 'GPTS Last Update Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_comments varchar(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_comments';
    $attr->{display_name} = 'GPTS Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_dir_number int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_dir_number';
    $attr->{display_name} = 'JGI Directory Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_visibility varchar(64)
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_visibility';
    $attr->{display_name} = 'Visibility';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # its_spid int,
    $attr = new RelAttr( );
    $attr->{name} = 'its_spid';
    $attr->{display_name} = 'ITS Sequencing ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_env_sample_energy_source
###########################################################################
sub def_env_sample_energy_source {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_energy_source';
    $table->{display_name} = 'Energy Sources';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'energy_source';
    $attr->{display_name} = 'Energy Source';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from energy_sourcecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_env_sample_diseases
###########################################################################
sub def_env_sample_diseases {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_diseases';
    $table->{display_name} = 'Diseases';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 14';
    $table->{migs_name} = 'PATHOGENICITY'; 
    $table->{new_rows} = 5;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'diseases';
    $attr->{display_name} = 'Diseases';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from diseasecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_env_sample_habitat_type
###########################################################################
sub def_env_sample_habitat_type {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_habitat_type';
    $table->{display_name} = 'Habitat';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 6';
    $table->{migs_name} = 'HABITAT'; 
    $table->{new_rows} = 5;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'habitat_type';
    $attr->{display_name} = 'Habitat Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from habitatcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_env_sample_metabolism
###########################################################################
sub def_env_sample_metabolism {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_metabolism';
    $table->{display_name} = 'Metabolism';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'metabolism';
    $attr->{display_name} = 'Metabolism';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from metabolismcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_env_sample_phenotypes
###########################################################################
sub def_env_sample_phenotypes {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_phenotypes';
    $table->{display_name} = 'Phenotypes';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # phenotypes
    $attr = new RelAttr( );
    $attr->{name} = 'phenotypes';
    $attr->{display_name} = 'Phenotypes';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from phenotypecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_env_sample_misc_meta_data
###########################################################################
sub def_env_sample_misc_meta_data {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_misc_meta_data';
    $table->{display_name} = 'Misc Metadata and Data Links';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 32; MIGS 33';
    $table->{migs_name} = 'RELEVANT SOPS; e-RESOURCES';
    $table->{new_rows} = 8;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'meta_tag';
    $attr->{display_name} = 'Tag / Link Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'meta_value';
    $attr->{display_name} = 'Value / Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_env_sample_seq_method
###########################################################################
sub def_env_sample_seq_method {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_seq_method';
    $table->{display_name} = 'Sequencing Methods';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 29 '; 
    $table->{migs_name} = 'SEQUENCING METHOD'; 
    $table->{new_rows} = 3;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'seq_method';
    $attr->{display_name} = 'Sequencing Method';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_methodcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_env_sample_jgi_url
###########################################################################
sub def_env_sample_jgi_url {
    my $table = new RelTable( );
    $table->{name} = 'env_sample_jgi_url';
    $table->{display_name} = 'JGI URL';
    $table->{id} = 'sample_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # sample_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Sample ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_name';
    $attr->{display_name} = 'Database Name';
    $attr->{data_type} = 'char';
#    $attr->{cv_query} = "select cv_term from seq_methodcv";
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_id';
    $attr->{display_name} = 'Database ID';
    $attr->{data_type} = 'char';
#    $attr->{cv_query} = "select cv_term from seq_methodcv";
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
#    $attr->{cv_query} = "select cv_term from seq_methodcv";
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_Project_Info - Project_Info
###########################################################################
sub def_Project_Info {
    my $table = new RelTable( );
    $table->{name} = 'project_info';
    $table->{display_name} = 'Genome Project Information';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'ER Submission Project OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gold_stamp_id varchar2(50) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'gold_stamp_id';
    $attr->{display_name} = 'GOLD Stamp ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gold_id_old varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'gold_id_old';
    $attr->{display_name} = 'GOLD Stamp Old ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # legacy_gold_id varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'legacy_gold_id';
    $attr->{display_name} = 'Legacy GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gcat_id varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'gcat_id';
    $attr->{display_name} = 'GCAT ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # strain_info_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'strain_info_id';
    $attr->{display_name} = 'Strain Info ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{definition} = 'Definition -  Enter the id for this culture from straininfo.net.'; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # greengenes_id varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'greengenes_id';
    $attr->{display_name} = 'Greengenes ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the greengenes id for the 16s from greengenes.lbl.gov.'; 
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # OTU int
    $attr = new RelAttr( );
    $attr->{name} = 'otu';
    $attr->{display_name} = 'OTU';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the OTU id.'; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # id_16S varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'id_16S';
    $attr->{display_name} = '16S ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the 16s id.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # img_oid int,
    $attr = new RelAttr( );
    $attr->{name} = 'img_oid';
    $attr->{display_name} = 'IMG Object ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Ignore.  This field will become populated after you submit your genome ot IMG'; 
    $attr->{hmp_cond} = 1; 
#    $attr->{header} = 'External Links';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_id';
    $attr->{display_name} = 'NCBI Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Ignore.  Enter the GPID from NCBI.  THIS IS NOT THE TAXONOMY ID'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/sites/entrez?db=genomeprj';
    $attr->{migs_id} = 'MIGS 1.1 ';
    $attr->{migs_name} = 'PID';
    $attr->{hint} = 'This is NOT a NCBI taxonomy id.  Please go to http://www.ncbi.nlm.nih.gov/genomeprj for more info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_archive_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_archive_id';
    $attr->{display_name} = 'NCBI Archive ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the TraceDB id for your Sanger data.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    $attr->{migs_id} = 'MIGS 1.2 ';
    $attr->{migs_name} = 'TRACE ARCHIVE';
    push @{$table->{attrs}}, ( $attr );

    # short_read_archive_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'short_read_archive_id';
    $attr->{display_name} = 'Short Read Archive ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the SRA id for your reads.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
#    $attr->{migs_id} = 'MIGS 1.2 ';
#    $attr->{migs_name} = 'TRACE ARCHIVE';
    push @{$table->{attrs}}, ( $attr );

    # project_web_page varchar2(500),
#    $attr = new RelAttr( );
#    $attr->{name} = 'project_web_page';
#    $attr->{display_name} = 'Project Web Page';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{header} = 'Genome Project Information';
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # web_page_code int,
    $attr = new RelAttr( );
    $attr->{name} = 'web_page_code';
    $attr->{display_name} = 'GOLD Web Page Code';
    $attr->{data_type} = 'cv2';
    $attr->{cv_query} = "select term_oid, description from web_page_codecv where term_oid < 4 order by term_oid";
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 0;
    $attr->{definition} = 'Definition -  Select the correct code if you want this project assigned a GOLD id and become publicly available.'; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # proposal_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'proposal_name';
    $attr->{display_name} = 'Proposal Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the title of the proposal or the name of the umbrella project.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # display_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'display_name';
    $attr->{display_name} = 'Display Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism';
    $attr->{migs_id} = 'MIGS 3 <font color=red></font>';
    $attr->{migs_name} = 'PROJECT NAME';
    $attr->{hint} = 'Genus species strain.  Please remove "subsp." from the name';
    push @{$table->{attrs}}, ( $attr );

    # submitters_project_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'submitters_project_name';
    $attr->{display_name} = 'Submitters Project Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism';
    $attr->{definition} = 'Definition -  Enter your own personal project name.'; 
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_taxon_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_taxon_id';
    $attr->{display_name} = 'NCBI Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_superkingdom varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_superkingdom';
    $attr->{display_name} = 'NCBI Kingdom';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_phylum varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_phylum';
    $attr->{display_name} = 'NCBI Phylum';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_class varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_class';
    $attr->{display_name} = 'NCBI Class';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_order varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_order';
    $attr->{display_name} = 'NCBI Order';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_family varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_family';
    $attr->{display_name} = 'NCBI Family';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_genus';
    $attr->{display_name} = 'NCBI Genus';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_species';
    $attr->{display_name} = 'NCBI Species';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # project_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_type';
    $attr->{display_name} = 'Project Type';
    $attr->{data_type} = 'cv';
    $attr->{default_value} = 'Whole Genome Sequencing';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{cv_query} = "select cv_term from project_typecv";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_status';
    $attr->{display_name} = 'Project Status';
    $attr->{data_type} = 'cv';
    $attr->{default_value} = 'incomplete';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{cv_query} = "select cv_term from project_statuscv";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # availability varchar2(20) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'availability';
    $attr->{display_name} = 'Project Information Visibility';
    $attr->{data_type} = 'cv';   # 'list';
    $attr->{default_value} = 'Public';
    # $attr->{list_values} = "Public|Proprietary";
    $attr->{cv_query} = "select cv_term from availabilitycv";
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Project';
    $attr->{hint} = 'This applies to whether the genome project was publicly funded or is owned by a commercial entity'; 
    push @{$table->{attrs}}, ( $attr );

    # contact_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'contact_name';
    $attr->{display_name} = 'Contact Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the contact name for this project.'; 
    $attr->{tab} = 'Project';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # contact_email varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'contact_email';
    $attr->{display_name} = 'Contact Email';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the contact\'s email address for this project (e.g. contact@genomes.center.edu, john.doe@gmail.com).'; 
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # contact_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'contact_url';
    $attr->{display_name} = 'Contact URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the URL for this genome project\' website.'; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # funding_program varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'funding_program';
    $attr->{display_name} = 'Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  If this is a JGI or HMP project, enter the corresponding funding program.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from funding_programcv order by cv_term";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # proteomics data varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'proteomics_data';
    $attr->{display_name} = 'Proteomics Data';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{definition} = 'Definition -  Enter the proteomics data id.'; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # proteomics_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'proteomics_link_url';
    $attr->{display_name} = 'Proteomics URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{definition} = 'Definition -  Enter the proteomics data url.'; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # transcriptomics data varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'transcriptomics_data';
    $attr->{display_name} = 'Transcriptomics Data';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{definition} = 'Definition -  Enter the transcriptomics data id.'; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # transcriptomics_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'transcriptomics_link_url';
    $attr->{display_name} = 'Transcriptomics URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{definition} = 'Definition -  Enter the transcriptomics data id.'; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # locus_tag varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'locus_tag';
    $attr->{display_name} = 'Locus Tag';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gc_perc numeric(5,2),
    $attr = new RelAttr( );
    $attr->{name} = 'gc_perc';
    $attr->{display_name} = 'GC Percent';
    $attr->{data_type} = 'number';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # seq_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_statuscv";
    $attr->{default_value} = 'In progress';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.1';
    $attr->{migs_name} = 'SEQUENCING STATUS';
    push @{$table->{attrs}}, ( $attr );

    # seq_quality varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_quality';
    $attr->{display_name} = 'Sequencing Quality';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_qualitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # finishing_goal varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'finishing_goal';
    $attr->{display_name} = 'Finishing Goal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from project_goalcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # domain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'domain';
    $attr->{display_name} = 'Domain';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "ARCHAEAL|BACTERIAL|EUKARYAL|PLASMID|VIRUS";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{header} = 'Organism Information';
    $attr->{tab} = 'Organism';
    $attr->{migs_id} = 'MIGS 2 <font color=red>  </font>';
    $attr->{migs_name} = 'MIGS CHECK-LIST TYPE';
    push @{$table->{attrs}}, ( $attr );

    # phylogeny varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'phylogeny';
    $attr->{display_name} = 'Phylogeny';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from phylogenycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Please choose a phylogeny category from the list.'; 
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # habitat_category varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'habitat_category';
#    $attr->{display_name} = 'Habitat Category';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from habitat_categorycv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # common_name varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'common_name';
    $attr->{display_name} = 'Common Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Organism';
    $attr->{definition} = 'Definition -  Enter the organism\'s common name.'; 
    $attr->{hint} = '(ec. dog, fruit fly)';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_name varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_name';
    $attr->{display_name} = 'NCBI Project Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the project name as it has been registered at NCBI\'s bioproject database.'; 
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'genus';
    $attr->{display_name} = 'Genus';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the genus.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # genus_synonyms varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'genus_synonyms';
    $attr->{display_name} = 'Genus Synonyms';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter a genus synonym (if it exists) as listed in NCBI\'s taxonomy page for this node.'; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'species';
    $attr->{display_name} = 'Species';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the species without the genus (e.g. coli instead of Escherichia coli).'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # species_synonyms varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'species_synonyms';
    $attr->{display_name} = 'Species Synonyms';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter a species synonym (if it exists) as listed in NCBI\'s taxonomy page for this node.'; 
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # strain varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'strain';
    $attr->{display_name} = 'Strain';
    $attr->{definition} = 'Definition -  Enter the strain name.  You can find the strain name in straininfo.net.'; 
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # serovar varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'serovar';
    $attr->{display_name} = 'Serovar-Biovar';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the serovar/biovar if it is available.'; 
    $attr->{tab} = 'Organism';
    $attr->{migs_id} = 'MIGS 7';
    $attr->{migs_name} = 'INFRASPECIFIC GENETIC LINEAGE';
    push @{$table->{attrs}}, ( $attr );

    # culture_collection varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'culture_collection';
    $attr->{display_name} = 'Culture Collection ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the culture collection id.  You can find them at straininfo.net.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism';
    $attr->{migs_id} = 'MIGS 13 <font color=red>  </font>';
    $attr->{migs_name} = 'REFS FOR BIOMATERIAL';
    $attr->{hint} = '(ec. ATCC 22626, DSM 1250)';
    push @{$table->{attrs}}, ( $attr );

    # type_strain
    $attr = new RelAttr( );
    $attr->{name} = 'type_strain';
    $attr->{display_name} = 'Type Strain';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Select Yes if this is a type strain.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # biosafety_level
    $attr = new RelAttr( );
    $attr->{name} = 'biosafety_level';
    $attr->{display_name} = 'Biosafety Level';
    $attr->{data_type} = 'int';
    $attr->{length} = 1;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter the corresponding biosafety level number.'; 
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # organism type
    $attr = new RelAttr( );
    $attr->{name} = 'organism_type';
    $attr->{display_name} = 'Organism Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvorganism_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Oranism Type -  Synthesized, Natural, Hybrid, Genetically modified, Single Cell, Population enrichment.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # cultured?
    $attr = new RelAttr( );
    $attr->{name} = 'cultured';
    $attr->{display_name} = 'Cultured?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Select Yes if this is cultured.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # cultured type
    $attr = new RelAttr( );
    $attr->{name} = 'culture_type';
    $attr->{display_name} = 'Cultured-Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvculture_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Cultured Type -  Isolate or Co-Culture';
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # uncultured_type
    $attr = new RelAttr( );
    $attr->{name} = 'uncultured_type';
    $attr->{display_name} = 'Uncultured-Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvuncultured_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Uncultured Type -  Single Cell or Population enrichment.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # organism_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'organism_comments';
    $attr->{display_name} = 'Organism Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Enter any comments with additional info about the organism.'; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # isolation varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'isolation';
    $attr->{display_name} = 'Isolation Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 13 <font color=red>  </font>';
    $attr->{migs_name} = 'ISOLATION SOURCE';
    push @{$table->{attrs}}, ( $attr );

    # iso_source varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_source';
    $attr->{display_name} = 'Source of Isolate';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # iso_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_method';
    $attr->{display_name} = 'Method of Isolation';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # iso_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_comments';
    $attr->{display_name} = 'Isolation Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # iso_year varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_year';
    $attr->{display_name} = 'Collection Date';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 5 <font color=red>  </font>';
    $attr->{migs_name} = 'COLLECTION DATE';
    $attr->{hint} = '';
    push @{$table->{attrs}}, ( $attr );

    # iso_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_country';
    $attr->{display_name} = 'Isolation Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 5 <font color=red>  </font>';
    $attr->{migs_name} = 'COUNTRY';
    push @{$table->{attrs}}, ( $attr );

    # iso_pubmed_id int
    $attr = new RelAttr( );
    $attr->{name} = 'iso_pubmed_id';
    $attr->{display_name} = 'Isolation Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 12 <font color=red>  </font>';
    $attr->{migs_name} = 'REFS FOR BIOMATERIAL';
    push @{$table->{attrs}}, ( $attr );

    # geo_location varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'geo_location';
    $attr->{display_name} = 'Geographic Location';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{definition} = 'Definition -  Geographic location of where the dna was isolated from.'; 
    $attr->{hint} = '(ec Walnut Creek Ca, Amazon rainforest Equador)';
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # loc_coord varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'loc_coord';
#    $attr->{display_name} = 'Location Coordinates (to be removed)';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # latitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'latitude';
    $attr->{display_name} = 'Latitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 4.1 <font color=red>  </font>';
    $attr->{migs_name} = 'LATITUDE';
    $attr->{hint} = 'Please -  enter ONLY decimal coordinates.'; 
    $attr->{definition} = 'Definition - Latitude is measured from the equator, with positive values going north and negative values going south.'; 
    $attr->{tip} = 'latitude'; 
    push @{$table->{attrs}}, ( $attr );

    # longitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longitude';
    $attr->{display_name} = 'Longitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 4.2 <font color=red>  </font>';
    $attr->{migs_name} = 'LONGITUDE';
    $attr->{tip} = 'longitude'; 
    $attr->{hint} = 'Please - enter ONLY decimal coordinates.'; 
    $attr->{definition} = 'Definition -  Angular distance on the earth surface, measured east or west from the prime meridian at Greenwich, England, to the meridian passing through a position, expressed in degrees (or hours), minutes, and seconds.'; 
    push @{$table->{attrs}}, ( $attr );

    # coordinate_type
    $attr = new RelAttr( );
    $attr->{name} = 'coordinate_type';
    $attr->{display_name} = 'Lat/Long Information';
    $attr->{data_type} = 'cv';
    $attr->{length} = 32;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from cvdata_validity_type";
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # altitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'altitude';
    $attr->{display_name} = 'Altitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{definition} = 'Definition -  The height of a thing above sea level or above the earth\'s surface.'; 
    $attr->{hint} = 'Please - enter measurement unit like meters, cm etc.'; 
    $attr->{migs_id} = 'MIGS 4.3';
    $attr->{migs_name} = 'ALTITUDE';
    push @{$table->{attrs}}, ( $attr );

    # depth varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'depth';
    $attr->{display_name} = 'Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIGS 4.4';
    $attr->{migs_name} = 'DEPTH';
    push @{$table->{attrs}}, ( $attr );

    # oxygen_req varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'oxygen_req';
    $attr->{display_name} = 'Oxygen Requirement';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from oxygencv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'OrganMeta';
    $attr->{migs_id} = 'MIGS 22';
    $attr->{migs_name} = 'RELATIONSHIP TO OXYGEN';
    push @{$table->{attrs}}, ( $attr );

    # cell_shape varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cell_shape';
    $attr->{display_name} = 'Cell Shape';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cell_shapecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # motility varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'motility';
    $attr->{display_name} = 'Motility';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from motilitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # sporulation varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sporulation';
    $attr->{display_name} = 'Sporulation';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sporulationcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # temp_range varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'temp_range';
    $attr->{display_name} = 'Temperature Range';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from temp_rangecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # temp_optimum varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'temp_optimum';
    $attr->{display_name} = 'Temperature Optimum';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # salinity varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'salinity';
    $attr->{display_name} = 'Salinity';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from salinitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # pressure varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pressure';
    $attr->{display_name} = 'Pressure';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # ph varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ph';
    $attr->{display_name} = 'pH';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # cell_diameter varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cell_diameter';
    $attr->{display_name} = 'Cell Diameter';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # cell_length varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cell_length';
    $attr->{display_name} = 'Cell Length';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # color varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'color';
    $attr->{display_name} = 'Color';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from colorcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # gram_stain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'gram_stain';
    $attr->{display_name} = 'Gram Staining';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Gram+|Gram-";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # biotic_rel varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'biotic_rel';
    $attr->{display_name} = 'Biotic Relationships';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Symbiotic|Free living";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    $attr->{migs_id} = 'MIGS 15';
    $attr->{migs_name} = 'BIOTIC RELATIONSHIP';
    push @{$table->{attrs}}, ( $attr );

    # symbiotic_interaction varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'symbiotic_interaction';
    $attr->{display_name} = 'Symbiotic Physical Interaction';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Exosymbiotic|Endosymbiotic extracellular|Endosymbiotic intracellular";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # symbiotic_rel varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'symbiotic_rel';
    $attr->{display_name} = 'Symbiotic Relationship';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Parasitic|Mutualistic|Commensal|Syntrophic";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # symbiont varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'symbiont';
    $attr->{display_name} = 'Symbiont Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # symbiont_taxon_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'symbiont_taxon_id';
    $attr->{display_name} = 'Symbiont Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_name';
    $attr->{display_name} = 'Host Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'HostMeta';
    $attr->{migs_id} = 'MIGS 16';
    $attr->{migs_name} = 'SPECIFIC HOST';
    push @{$table->{attrs}}, ( $attr );

    # host_taxon_id varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_taxon_id';
    $attr->{display_name} = 'Host Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_gender varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_gender';
    $attr->{display_name} = 'Host Gender';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_race varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_race';
    $attr->{display_name} = 'Host Race';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_age varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_age';
    $attr->{display_name} = 'Host Age';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_health varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_health';
    $attr->{display_name} = 'Host Health';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    $attr->{migs_id} = 'MIGS 18';
    $attr->{migs_name} = 'HEALTH STATUS OF HOST';
    push @{$table->{attrs}}, ( $attr );

    # host_medication varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_medication';
    $attr->{display_name} = 'Host Medication';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

#     # body_sample_site varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_sample_site';
#     $attr->{display_name} = 'Primary Body Sample Site';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_sitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

#     # body_sample_subsite varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_sample_subsite';
#     $attr->{display_name} = 'Body Sample SubSite';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_subsitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );


#     # body_product varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_product';
#     $attr->{display_name} = 'Body Product';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_productcv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );


#     # additional_body_sample_site varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'additional_body_sample_site';
#     $attr->{display_name} = 'Additional Body Sample Site';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_sitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

    # host_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'host_comments';
    $attr->{display_name} = 'Host Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # seq_center_proj_id number,
    $attr = new RelAttr( );
    $attr->{name} = 'seq_center_proj_id';
    $attr->{display_name} = 'Sequencing Center Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # sample_oid number,
    $attr = new RelAttr( );
    $attr->{name} = 'sample_oid';
    $attr->{display_name} = 'Metagenome Sample OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # seq_status_link varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status_link';
    $attr->{display_name} = 'Seq Status Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

#     # bioproject_relevance varchar2(4000),
#     $attr = new RelAttr( );
#     $attr->{name} = 'bioproject_relevance';
#     $attr->{display_name} = 'NCBI BioProject Relevance';
#     $attr->{data_type} = 'cv';
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{cv_query} = "select relevance from bioproject_relevancecv order by relevance";
#     $attr->{tab} = 'Sequencing';
#     push @{$table->{attrs}}, ( $attr );

    # nucleic acid
    $attr = new RelAttr( );
    $attr->{name} = 'nucleic_acid_type';
    $attr->{display_name} = "Nucleic Acid Type";
    $attr->{data_type} = 'list';
    $attr->{list_values} = 'DNA|RNA';
    $attr->{length} = 16;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # library_method varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'library_method';
    $attr->{display_name} = 'Library Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 28.1';
    $attr->{migs_name} = 'LIBRARY SIZE';
    push @{$table->{attrs}}, ( $attr );

    # no_of_reads int
    $attr = new RelAttr( );
    $attr->{name} = 'no_of_reads';
    $attr->{display_name} = 'Number of Reads';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 28.2';
    $attr->{migs_name} = 'NUMBER OF READS';
    push @{$table->{attrs}}, ( $attr );

    # vector varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'vector';
    $attr->{display_name} = 'Vector';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 28.3';
    $attr->{migs_name} = 'VECTOR';
    push @{$table->{attrs}}, ( $attr );

    # binning_method varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'binning_method';
#    $attr->{display_name} = 'Binning Method';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # assembly_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'assembly_method';
    $attr->{display_name} = 'Assembly Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 30 <font color=red>  </font>';
    $attr->{migs_name} = 'ASSEMBLY';
    push @{$table->{attrs}}, ( $attr );

    # seq_depth varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_depth';
    $attr->{display_name} = 'Sequencing Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.2';
    $attr->{migs_name} = 'SEQUENCING COVERAGE';
    push @{$table->{attrs}}, ( $attr );

    # gene_calling_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'gene_calling_method';
    $attr->{display_name} = 'Gene Calling Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # scaffold_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'scaffold_count';
    $attr->{display_name} = 'Scaffold Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.3';
    $attr->{migs_name} = 'CONTIGS';
    push @{$table->{attrs}}, ( $attr );

    # contig_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'contig_count';
    $attr->{display_name} = 'Contig Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.3';
    $attr->{migs_name} = 'CONTIGS';
    push @{$table->{attrs}}, ( $attr );

    # est_size int,
    $attr = new RelAttr( );
    $attr->{name} = 'est_size';
    $attr->{display_name} = 'Estimated Size';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{hmp_cond} = 1; 
    $attr->{migs_id} = 'MIGS 11';
    $attr->{migs_name} = 'ESTIMATED SIZE';
    push @{$table->{attrs}}, ( $attr );

    # units varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'units';
    $attr->{display_name} = 'Units';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from unitcv";
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # genome_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'genome_count';
#    $attr->{display_name} = 'Community Diversity (est. number)';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # gene_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'gene_count';
    $attr->{display_name} = 'Gene Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );
    
    # chromosome_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'chromosome_count';
    $attr->{display_name} = 'Chromosome Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    $attr->{migs_id} = 'MIGS 9';
    $attr->{migs_name} = 'NUMBER OF REPLICONS';
    push @{$table->{attrs}}, ( $attr );

    # plasmid_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'plasmid_count';
    $attr->{display_name} = 'Plasmid Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    $attr->{migs_id} = 'MIGS 10';
    $attr->{migs_name} = 'EXTRACHROMOSOMAL ELEMENTS';
    push @{$table->{attrs}}, ( $attr );

# per Nikos, singlet count is for metagenome only
    # singlet_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'singlet_count';
#    $attr->{display_name} = 'Singlet Count';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

    # seq_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_country';
    $attr->{display_name} = 'Sequencing Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # completion_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'completion_date';
    $attr->{display_name} = 'Completion Date (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    $attr->{hint} = 'Date that genbank file was submitted.  Not the date NCBI completed submission';
    push @{$table->{attrs}}, ( $attr );

    # pubmed_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmid';
    $attr->{display_name} = 'Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{filter_cond} = 1;
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # doi_id varchar2(200),
    $attr = new RelAttr( );
    $attr->{name} = 'doi_id';
    $attr->{display_name} = 'DOI ID';
    $attr->{data_type} = 'char';
    $attr->{filter_cond} = 1;
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_journal varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'pub_journal';
    $attr->{display_name} = 'Publication Journal';
    $attr->{data_type} = 'cv';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from publication_journalcv";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_vol varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'pub_vol';
    $attr->{display_name} = 'Publication Vol.';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_link varchar2(500)
    $attr = new RelAttr( );
    $attr->{name} = 'pub_link';
    $attr->{display_name} = 'Publication Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # refseq_ids varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'refseq_ids';
    $attr->{display_name} = 'Refseq ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # refseq_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'refseq_link_url';
    $attr->{display_name} = 'Refseq link URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # map_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'map_link_url';
    $attr->{display_name} = 'NCBI Map link URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # growth_conditions varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'growth_conditions';
    $attr->{display_name} = 'Organism Growth Conditions';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{hint} = '';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # proj_desc varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'proj_desc';
    $attr->{display_name} = 'Project Description';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{hint} = '';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # information_name varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'information_name';
    $attr->{display_name} = 'Information Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # information_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'information_url';
    $attr->{display_name} = 'Information URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # contact_oid int
    $attr = new RelAttr( );
    $attr->{name} = 'contact_oid';
    $attr->{display_name} = 'IMG Contact';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # add_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'add_date';
    $attr->{display_name} = 'Add Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{header} = 'Database Update Info';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Last Modify Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Last Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # hmp_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_id';
    $attr->{display_name} = 'HMP ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # show_in_dacc
    $attr = new RelAttr( );
    $attr->{name} = 'show_in_dacc';
    $attr->{display_name} = 'Show in DACC?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_project_status';
    $attr->{display_name} = 'HMP Project Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from HMP_project_statuscv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # bei_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'bei_status';
    $attr->{display_name} = 'BEI Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from BEI_statuscv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_submit_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_submit_status';
    $attr->{display_name} = 'NCBI Submission Status';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from ncbi_submit_statuscv";
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

#     # isolate_selected
#     $attr = new RelAttr( );
#     $attr->{name} = 'isolate_selected';
#     $attr->{display_name} = 'Isolate Selected by Working Group';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from yesnocv";
#     $attr->{length} = 10;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{can_edit} = 1;
#     $attr->{tab} = 'HMP';
#     push @{$table->{attrs}}, ( $attr );

    # isolate_selected
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_isolate_selection_source';
    $attr->{display_name} = 'Isolate Suggestion Source';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from isolate_suggestion_sourcecv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

     # body_sample_site varchar2(255),
     $attr = new RelAttr( );
     $attr->{name} = 'hmp_isolation_bodysite';
     $attr->{display_name} = 'HMP Isolation Body Sample Site';
     $attr->{data_type} = 'cv';
     $attr->{cv_query} = "select cv_term from body_sitecv";
     $attr->{length} = 255;
     $attr->{is_required} = 0;
     $attr->{filter_cond} = 1;
     $attr->{hmp_cond} = 1; 
     $attr->{tab} = 'HMP';
     push @{$table->{attrs}}, ( $attr );

     # body_sample_subsite varchar2(255),
     $attr = new RelAttr( );
     $attr->{name} = 'hmp_isolation_bodysubsite';
     $attr->{display_name} = 'HMP Isolation Body Sample SubSite';
     $attr->{data_type} = 'cv';
     $attr->{cv_query} = "select cv_term from body_subsitecv";
     $attr->{length} = 255;
     $attr->{is_required} = 0;
     $attr->{filter_cond} = 1;
     $attr->{hmp_cond} = 1; 
     $attr->{tab} = 'HMP';
     push @{$table->{attrs}}, ( $attr );

    # project_goal varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_goal';
    $attr->{display_name} = 'HMP Finishing Goal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from project_goalcv";
    $attr->{length} = 255;
    $attr->{hmp_cond} = 1; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_iso_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_iso_comments';
    $attr->{display_name} = 'Source, Sample & Medical Relevance Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_iso_avail_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_iso_avail_comments';
    $attr->{display_name} = 'Isolate Availability Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_project_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_project_comments';
    $attr->{display_name} = 'Project Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # dna_received
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_dna_received';
    $attr->{display_name} = 'DNA Received';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_date_dna_received date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_date_dna_received';
    $attr->{display_name} = 'Date DNA Received (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_seq_begin_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_seq_begin_date';
    $attr->{display_name} = 'Date Sequencing Begins (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_draft_seq_completion_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_draft_seq_completion_date';
    $attr->{display_name} = 'Date Draft Sequencing Completed (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # homd_id varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'homd_id';
    $attr->{display_name} = 'Cross Reference ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # jgi_proposal_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_proposal_id';
    $attr->{display_name} = 'JGI Proposal ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # pmo_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmo_project_id';
    $attr->{display_name} = 'PMO Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scientific_program varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'scientific_program';
    $attr->{display_name} = 'Scientific Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sci_progcv";
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_final_deliverable varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_final_deliverable';
    $attr->{display_name} = 'ITS Final Deliverable';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvjgi_its_final_deliverable";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_product_name varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_product_name';
    $attr->{display_name} = 'JGI Product Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scope_of_work varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'scope_of_work';
    $attr->{display_name} = 'JGI Scope of Work';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select scope from workscopecv order by scope";
#    $attr->{tab} = 'Sequencing Information';
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_project_type varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_project_type';
    $attr->{display_name} = 'JGI Project Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 256;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_status varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_status';
    $attr->{display_name} = 'JGI Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_last_mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_last_mod_date';
    $attr->{display_name} = 'GPTS Last Update Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_comments varchar(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_comments';
    $attr->{display_name} = 'GPTS Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_program varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_program';
    $attr->{display_name} = 'JGI Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from jgi_funding_progcv";
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_year int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_year';
    $attr->{display_name} = 'JGI Funding Year';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_bioclassification_name
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_bioclassification_name';
    $attr->{display_name} = 'Bioclassification Name';
    $attr->{data_type} = 'int';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_dir_number int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_dir_number';
    $attr->{display_name} = 'JGI Directory Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_visibility varchar(64)
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_visibility';
    $attr->{display_name} = 'Visibility';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # its_spid int,
    $attr = new RelAttr( );
    $attr->{name} = 'its_spid';
    $attr->{display_name} = 'ITS Sequencing ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_body_products
###########################################################################
sub def_project_info_body_products {
    my $table = new RelTable( );
    $table->{name} = 'project_info_body_products';
    $table->{display_name} = 'Body Products';
    $table->{id} = 'project_oid';
    $table->{tab} = 'HostMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'body_product';
    $attr->{display_name} = 'Body Product';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from body_productcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_body_sites
###########################################################################
sub def_project_info_body_sites {
    my $table = new RelTable( );
    $table->{name} = 'project_info_body_sites';
    $table->{display_name} = 'Body Sites';
    $table->{id} = 'project_oid';
    $table->{tab} = 'HostMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'sample_body_site';
    $attr->{display_name} = 'Body Site';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from body_sitecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'sample_body_subsite';
    $attr->{display_name} = 'Body Subsite';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from body_subsitecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_cell_arrangement
###########################################################################
sub def_project_info_cell_arrangement {
    my $table = new RelTable( );
    $table->{name} = 'project_info_cell_arrangement';
    $table->{display_name} = 'Cell Arrangements';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'cell_arrangement';
    $attr->{display_name} = 'Cell Arrangement';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cell_arrcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_diseases
###########################################################################
sub def_project_info_diseases {
    my $table = new RelTable( );
    $table->{name} = 'project_info_diseases';
    $table->{display_name} = 'Diseases';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 14';
    $table->{migs_name} = 'PATHOGENICITY';
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'diseases';
    $attr->{display_name} = 'Diseases';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from diseasecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_habitat
###########################################################################
sub def_project_info_habitat {
    my $table = new RelTable( );
    $table->{name} = 'project_info_habitat';
    $table->{display_name} = 'Habitat';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 6 <font color=red>  </font>';
    $table->{migs_name} = 'HABITAT';
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'habitat';
    $attr->{display_name} = 'Habitat';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from habitatcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_metabolism
###########################################################################
sub def_project_info_metabolism {
    my $table = new RelTable( );
    $table->{name} = 'project_info_metabolism';
    $table->{display_name} = 'Metabolism';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'metabolism';
    $attr->{display_name} = 'Metabolism';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from metabolismcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_phenotypes
###########################################################################
sub def_project_info_phenotypes {
    my $table = new RelTable( );
    $table->{name} = 'project_info_phenotypes';
    $table->{display_name} = 'Phenotypes';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # phenotypes
    $attr = new RelAttr( );
    $attr->{name} = 'phenotypes';
    $attr->{display_name} = 'Phenotypes';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from phenotypecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_project_relevance
###########################################################################
sub def_project_info_project_relevance {
    my $table = new RelTable( );
    $table->{name} = 'project_info_project_relevance';
    $table->{display_name} = 'Project Relevance';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Project';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # project_relevance
    $attr = new RelAttr( );
    $attr->{name} = 'project_relevance';
    $attr->{display_name} = 'Project Relevance';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from relevancecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_seq_centers 
###########################################################################
sub def_project_info_seq_centers {
    my $table = new RelTable( );
    $table->{name} = 'project_info_seq_centers';
    $table->{display_name} = 'Sequencing Center';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # centers
    $attr = new RelAttr( );
    $attr->{name} = 'center';
    $attr->{display_name} = 'Sequencing Center';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 60;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_project_info_seq_method
###########################################################################
sub def_project_info_seq_method {
    my $table = new RelTable( );
    $table->{name} = 'project_info_seq_method';
    $table->{display_name} = 'Sequencing Methods';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Sequencing';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;
    $table->{migs_id} = 'MIGS 29 <font color=red>  </font>';
    $table->{migs_name} = 'SEQUENCING METHOD';

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # seq_method
    $attr = new RelAttr( );
    $attr->{name} = 'seq_method';
    $attr->{display_name} = 'Sequencing Method';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_methodcv";
    $attr->{length} = 255;
    $attr->{default_size} = 60;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_project_info_funding_agencies 
###########################################################################
sub def_project_info_funding_agencies {
    my $table = new RelTable( );
    $table->{name} = 'project_info_funding_agencies';
    $table->{display_name} = 'Funding Agencies';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # agency
    $attr = new RelAttr( );
    $attr->{name} = 'agency';
    $attr->{display_name} = 'Agency';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 60;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_collaborators
###########################################################################
sub def_project_info_collaborators {
    my $table = new RelTable( );
    $table->{name} = 'project_info_collaborators';
    $table->{display_name} = 'Collaborators';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Sequencing';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;
#    $table->{migs_id} = 'MIGS 29 <font color=red>  </font>';
#    $table->{migs_name} = 'SEQUENCING METHOD';

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # seq_method
    $attr = new RelAttr( );
    $attr->{name} = 'collaborator_id';
    $attr->{display_name} = 'Collaborator ID';
    $attr->{data_type} = 'number';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_data_links
###########################################################################
sub def_project_info_data_links {
    my $table = new RelTable( );
    $table->{name} = 'project_info_data_links';
    $table->{display_name} = 'Data Links (URLs)';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Project';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 10;
    $table->{migs_id} = 'MIGS 32; MIGS 33';
    $table->{migs_name} = 'RELEVANT SOPS; e-RESOURCES';

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'link_type';
    $attr->{display_name} = 'Link Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from link_typecv";
    $attr->{length} = 20;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

#    $attr = new RelAttr( );
#    $attr->{name} = 'db_name';
#    $attr->{display_name} = 'Source Name';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{default_size} = 30;
#    $attr->{is_required} = 0;
#    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_name';
    $attr->{display_name} = 'Source Name';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from db_namecv order by 1";
    $attr->{length} = 255;
    $attr->{default_size} = 30;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'id';
    $attr->{display_name} = 'ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_project_info_jgi_url
###########################################################################
sub def_project_info_jgi_url {
    my $table = new RelTable( );
    $table->{name} = 'project_info_jgi_url';
    $table->{display_name} = 'JGI URL';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Project';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_name';
    $attr->{display_name} = 'DB Name';
    $attr->{data_type} = 'char';
#    $attr->{cv_query} = "select cv_term from db_namecv order by 1";
    $attr->{length} = 80;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_id';
    $attr->{display_name} = 'DB ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_nprods_metadata
###########################################################################
sub def_project_info_nprods_metadata {
    my $table = new RelTable( );
    $table->{name} = 'project_info_nprods_metadata';
    $table->{display_name} = 'Natural Product';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Project';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # np_id int not null,
    $attr = new RelAttr( );
    $attr->{name} = 'np_id';
    $attr->{display_name} = 'NP ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'np_product_name';
    $attr->{display_name} = 'NP Product Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 256;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'np_product_link';
    $attr->{display_name} = 'NP Product Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'np_type';
    $attr->{display_name} = 'NP Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 128;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'np_activity';
    $attr->{display_name} = 'NP Activity';
    $attr->{data_type} = 'char';
    $attr->{length} = 128;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'bio_clusters';
    $attr->{display_name} = 'BioCluster';
    $attr->{data_type} = 'char';
    $attr->{length} = 4;
    $attr->{default_size} = 4;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'genbank_id';
    $attr->{display_name} = 'Genbank ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 128;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'img_biocluster_link';
    $attr->{display_name} = 'IMG BioCluster Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 256;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'evidence';
    $attr->{display_name} = 'NP Evidence';
    $attr->{data_type} = 'char';
    $attr->{length} = 20;
    $attr->{default_size} = 20;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_info_publications
###########################################################################
sub def_project_info_publications {
    my $table = new RelTable( );
    $table->{name} = 'project_info_publications';
    $table->{display_name} = 'Publications';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'journal';
    $attr->{display_name} = 'Journal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from publication_journalcv";
    $attr->{length} = 512;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'pmid';
    $attr->{display_name} = 'Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_energy_source
###########################################################################
sub def_project_info_energy_source {
    my $table = new RelTable( );
    $table->{name} = 'project_info_energy_source';
    $table->{display_name} = 'Energy Sources';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIGS 19';
    $table->{migs_name} = 'TROPHIC LEVEL';
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'energy_source';
    $attr->{display_name} = 'Energy Source';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from energy_sourcecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_project_cyano_metadata
###########################################################################
sub def_project_info_cyano_metadata {
    my $table = new RelTable( );
    $table->{name} = 'project_info_cyano_metadata';
    $table->{display_name} = 'Cyano Metadata';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # filaments varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'filaments';
    $attr->{display_name} = 'Filaments';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # filaments varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'filament_type';
    $attr->{display_name} = 'Filament Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{html_control} = 'checkbox';
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

#     # cysts varchar2(255)
#     $attr = new RelAttr( );
#     $attr->{name} = 'cysts';
#     $attr->{display_name} = 'Cysts';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from yesnocv";
#     $attr->{length} = 10;
# #    $attr->{default_size} = 80;
#     $attr->{is_required} = 0;
# #    $attr->{filter_cond} = 1;
#     $attr->{can_edit} = 1;
#     $attr->{tab} = 'Organism';
#     push @{$table->{attrs}}, ( $attr );

    # differentiated_cells varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'differentiated_cells';
    $attr->{display_name} = 'Differentiated Cells';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Akinetes|Hormogonia|Heterocysts";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # heterocysts_position varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'heterocysts_position';
    $attr->{display_name} = 'Position Of Heterocysts';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Terminal|Intercalary|Lateral";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # reproduction_mode varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'reproduction_mode';
    $attr->{display_name} = 'Mode Of Reproduction';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Binary fission only|Binary fission and multiple fission (baeocyte)|Multiple fission (baeocyte)|Unequal Binary Fission (Budding)";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # trichome varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'trichome';
    $attr->{display_name} = 'Trichome';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # trichome_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'trichome_type';
    $attr->{display_name} = 'Trichome Type';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Tapering|False branching|True branching";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # carotenoids varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'carotenoids';
    $attr->{display_name} = 'Carotenoids';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Zeaxanthin|Echinone|3' Hydroxy-echineneone|Myxoxanthophyll|Cryptoxanthin|Synechoxanthin|Myxoxanthin|Bioloxanthin|Unknown";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # pigments varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'pigments';
    $attr->{display_name} = 'Pigments';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Phycoerythrocynin|Phycocyanin|Allophycocyanin|Allophycoerythrin B|Chlorophyll A|Chlorophyll B|Chlorophyll C|Divinyl Chlorophyl A|Divinyl Chlorophyl B";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # phage_infectivity varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'phage_infectivity';
    $attr->{display_name} = 'Phage Infectivity';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # phage_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'phage_type';
    $attr->{display_name} = 'Phage Type';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "ASM1|SM-1|N-1|LPP1";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # restriction_enzyme varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'restriction_enzyme';
    $attr->{display_name} = 'Restriction Enzymes';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # toxin_production varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'toxin_production';
    $attr->{display_name} = 'Toxin Production';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # toxin_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'toxin_type';
    $attr->{display_name} = 'Toxins Type';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Hepatotoxins|Neurotoxins|Cytoxins|Dermatotoxins";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # secondary_metabolites varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'secondary_metabolites';
    $attr->{display_name} = 'Secondary Metabolites';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Microviridin";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # developmental_cycle varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'developmental_cycle';
    $attr->{display_name} = 'Developmental Cycle';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # branching varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'branching';
    $attr->{display_name} = 'Branching';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # gas_vesicles_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'gas_vesicles';
    $attr->{display_name} = 'Gas Vesicle';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # gas_vesicle_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'gas_vesicle_type';
    $attr->{display_name} = 'Gas Vesicles Type';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Polar|Dispersed";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # body_inclusion varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'body_inclusion';
    $attr->{display_name} = 'Diagnostic Inclusion Body Information';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Starch granules|Polyphosphate granules|Lipid droplets|PHA|Cyanophycin|Polyglucan|Glycogen granules";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # extracellular_structures varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'extracellular_structures';
    $attr->{display_name} = 'Extracellular Structures';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Mucilage sheath";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # bloom_forming varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'bloom_forming';
    $attr->{display_name} = 'Bloom Forming';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # pigment_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'pigment_type';
    $attr->{display_name} = 'Pigment Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # compl_chromatic_adapation varchar2(64)
    $attr = new RelAttr( );
    $attr->{name} = 'compl_chromatic_adaptation';
    $attr->{display_name} = 'Complementary chromatic adaptation (CCA)';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Confirmed|Potentially|Yes|No";
    $attr->{length} = 10;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    # medium_type varchar2(255)
    $attr = new RelAttr( );
    $attr->{name} = 'medium_type';
    $attr->{display_name} = 'Medium Types';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
#    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

### Metagenome project

###########################################################################
# def_meta_Project_Info - Project_Info
###########################################################################
sub def_meta_Project_Info {
    my $table = new RelTable( );
    $table->{name} = 'project_info';
    $table->{display_name} = 'Metagenome Study Information';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'ER Submission Study OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gold_stamp_id varchar2(50) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'gold_stamp_id';
    $attr->{display_name} = 'Study GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gold_id_old varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'gold_id_old';
    $attr->{display_name} = 'Old Study GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # legacy_gold_id varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'legacy_gold_id';
    $attr->{display_name} = 'Legacy GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gcat_id varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'gcat_id';
    $attr->{display_name} = 'GCAT ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # hmp_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_id';
    $attr->{display_name} = 'HMP ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # homd_id varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'homd_id';
    $attr->{display_name} = 'Cross Reference ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # strain_info_id int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'strain_info_id';
#    $attr->{display_name} = 'Strain Info ID';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Links';
#    push @{$table->{attrs}}, ( $attr );

    # greengenes_id varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'greengenes_id';
#    $attr->{display_name} = 'Greengenes ID';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Links';
#    push @{$table->{attrs}}, ( $attr );

    # img_oid int,
    $attr = new RelAttr( );
    $attr->{name} = 'img_oid';
    $attr->{display_name} = 'IMG/M Object ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{header} = 'External Links';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_id';
    $attr->{display_name} = 'NCBI Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/sites/entrez?db=genomeprj';
    $attr->{migs_id} = 'MIMS 1.1 <font color=red>  </font>';
    $attr->{migs_name} = 'PID'; 
    push @{$table->{attrs}}, ( $attr );

    # ncbi_archive_id int, 
 #    $attr = new RelAttr( ); 
#     $attr->{name} = 'ncbi_archive_id'; 
#     $attr->{display_name} = 'NCBI Archive ID'; 
#     $attr->{data_type} = 'int'; 
#     $attr->{length} = 40; 
#     $attr->{is_required} = 0; 
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'Project'; 
#     $attr->{migs_id} = 'MIMS 1.2 <font color=red>  </font>'; 
#     $attr->{migs_name} = 'TRACE ARCHIVE'; 
#     push @{$table->{attrs}}, ( $attr ); 

    # short_read_archive_id int, 
    $attr = new RelAttr( ); 
    $attr->{name} = 'short_read_archive_id'; 
    $attr->{display_name} = 'Short Read Archive ID'; 
    $attr->{data_type} = 'char'; 
    $attr->{length} = 20; 
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1; 
    $attr->{tab} = 'Project'; 
#    $attr->{migs_id} = 'MIMS 1.2 '; 
#    $attr->{migs_name} = 'TRACE ARCHIVE'; 
    push @{$table->{attrs}}, ( $attr ); 
 
    # project_web_page varchar2(500),
#    $attr = new RelAttr( );
#    $attr->{name} = 'project_web_page';
#    $attr->{display_name} = 'Project Web Page';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 1;
#    $attr->{header} = 'Genome Project Information';
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # web_page_code int,
    $attr = new RelAttr( );
    $attr->{name} = 'web_page_code';
    $attr->{display_name} = 'GOLD Web Page Code';
    $attr->{data_type} = 'cv2';
    $attr->{cv_query} = "select term_oid, description from web_page_codecv where term_oid = 4 order by term_oid";
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

#     # proposal_name 
#     $attr = new RelAttr( );
#     $attr->{name} = 'proposal_name';
#     $attr->{display_name} = 'Proposal Name';
#     $attr->{data_type} = 'char';
#     $attr->{length} = 1000;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'Metagenome';
#     push @{$table->{attrs}}, ( $attr );

    # display_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'display_name';
    $attr->{display_name} = 'Study Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Metagenome';
    $attr->{migs_id} = 'MIMS 3 <font color=red>  </font>';
    $attr->{migs_name} = 'PROJECT NAME';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # submitters_project_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'submitters_project_name';
    $attr->{display_name} = 'Submitter\'s Study Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Metagenome';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_taxon_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_taxon_id';
    $attr->{display_name} = 'NCBI Taxon ID (if any)';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'Metagenome';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_superdom varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_superkingdom';
#    $attr->{display_name} = 'NCBI Kingdom';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_phylum varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_phylum';
#    $attr->{display_name} = 'NCBI Phylum';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_class varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_class';
#    $attr->{display_name} = 'NCBI Class';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_order varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_order';
#    $attr->{display_name} = 'NCBI Order';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_family varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_family';
#    $attr->{display_name} = 'NCBI Family';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_genus varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_genus';
#    $attr->{display_name} = 'NCBI Genus';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_species varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'ncbi_species';
#    $attr->{display_name} = 'NCBI Species';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{can_edit} = 0;
#    $attr->{font_color} = 'darkgreen';
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # project_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_type';
    $attr->{display_name} = 'Project Type';
    $attr->{data_type} = 'cv';
    $attr->{default_value} = 'Whole Genome Sequencing';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{cv_query} = "select cv_term from project_typecv where cv_term in ( 'Metagenome', 'Metatranscriptome', 'Targeted Gene Survey')";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_status';
    $attr->{display_name} = 'Project Status';
    $attr->{data_type} = 'cv';
    $attr->{default_value} = 'incomplete';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{cv_query} = "select cv_term from project_statuscv";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # publication_status varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'publication_status';
#    $attr->{display_name} = 'Publication Status';
#    $attr->{data_type} = 'cv';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{cv_query} = "select cv_term from publication_statuscv";
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # availability varchar2(20) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'availability';
    $attr->{display_name} = 'Project Information Visibility';
    $attr->{data_type} = 'cv';   # 'list';
    $attr->{default_value} = 'Public';
    # $attr->{list_values} = "Public|Proprietary";
    $attr->{cv_query} = "select cv_term from availabilitycv";
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # contact_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'contact_name';
    $attr->{display_name} = 'Contact Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # contact_email varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'contact_email';
    $attr->{display_name} = 'Contact Email';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # contact_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'contact_url';
    $attr->{display_name} = 'Contact URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # funding_program varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'funding_program';
    $attr->{display_name} = 'Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{cv_query} = "select cv_term from funding_programcv order by cv_term";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # gc_perc numeric(5,2),
#    $attr = new RelAttr( );
#    $attr->{name} = 'gc_perc';
#    $attr->{display_name} = 'GC Percent';
#    $attr->{data_type} = 'number';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # seq_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_statuscv";
    $attr->{default_value} = 'In progress';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIMS 31.1'; 
    $attr->{migs_name} = 'SEQUENCING STATUS';
    push @{$table->{attrs}}, ( $attr );

    # seq_quality varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_quality';
    $attr->{display_name} = 'Sequencing Quality';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_qualitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # finishing_goal varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'finishing_goal';
    $attr->{display_name} = 'Finishing Goal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from project_goalcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # domain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'domain';
    $attr->{display_name} = 'Domain';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "MICROBIAL";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{header} = 'Organism Information';
    $attr->{tab} = 'Metagenome';
    $attr->{migs_id} = 'MIMS 2 <font color=red>  </font>';
    $attr->{migs_name} = 'MIMS CHECK-LIST TYPE';
    push @{$table->{attrs}}, ( $attr );

    # phylogeny varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'phylogeny';
#    $attr->{display_name} = 'Phylogeny';
#    $attr->{display_name} = 'Category (Phylogeny)';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from phylogenycv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{tab} = 'Metagenome';
#    push @{$table->{attrs}}, ( $attr );

    # ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem';
    $attr->{display_name} = 'Ecosystem';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvecosystem";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_category varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_category';
    $attr->{display_name} = 'Ecosystem Category';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvecosystem_category order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_type';
    $attr->{display_name} = 'Ecosystem Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvecosystem_type order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_subtype varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_subtype';
    $attr->{display_name} = 'Ecosystem Subtype';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvecosystem_subtype order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # specific_ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'specific_ecosystem';
    $attr->{display_name} = 'Specific Ecosystem';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvspecific_ecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{font_color} = 'darkgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

#     # habitat_category varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'habitat_category';
#     $attr->{display_name} = 'Habitat Category';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from habitat_categorycv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{tab} = 'Metagenome';
#     push @{$table->{attrs}}, ( $attr );

    # common_name varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'common_name';
#    $attr->{display_name} = 'Common Name';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Metagenome';
#    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_name varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_name';
    $attr->{display_name} = 'NCBI Project Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'genus';
    $attr->{display_name} = 'Habitat';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # genus_synonyms varchar2(4000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'genus_synonyms';
#    $attr->{display_name} = 'Genus Synonyms';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 4000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'species';
    $attr->{display_name} = 'Community';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # species_synonyms varchar2(4000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'species_synonyms';
#    $attr->{display_name} = 'Species Synonyms';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 4000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # strain varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'strain';
#    $attr->{display_name} = 'Strain';
    $attr->{display_name} = 'Location';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Metagenome';
    push @{$table->{attrs}}, ( $attr );

    # serovar varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'serovar';
#    $attr->{display_name} = 'Serovar-Biovar';
    $attr->{display_name} = 'Identifier';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Metagenome';
    $attr->{migs_id} = 'MIMS 7'; 
    $attr->{migs_name} = 'INFRASPECIFIC GENETIC LINEAGE';
    push @{$table->{attrs}}, ( $attr );

    # culture_collection varchar2(4000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'culture_collection';
#    $attr->{display_name} = 'Culture Collection';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 4000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metagenome';
#    $attr->{migs_id} = 'MIMS 13 ';
#    $attr->{migs_name} = 'REFS FOR BIOMATERIAL';
#    push @{$table->{attrs}}, ( $attr );

# remove, per Nikos
    # symbiont varchar2(500),
#    $attr = new RelAttr( );
#    $attr->{name} = 'symbiont';
#    $attr->{display_name} = 'Symbiont';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metagenome';
#    push @{$table->{attrs}}, ( $attr );

    # symbiont_taxon_id int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'symbiont_taxon_id';
#    $attr->{display_name} = 'Symbiont Taxon ID';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metagenome';
#    push @{$table->{attrs}}, ( $attr );

    # isolation varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'isolation';
    $attr->{display_name} = 'Isolation Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIMS 13 <font color=red>  </font>';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{migs_name} = 'ISOLATION SOURCE'; 
    push @{$table->{attrs}}, ( $attr );

    # iso_source varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_source';
    $attr->{display_name} = 'Source of Isolate';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # iso_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_comments';
    $attr->{display_name} = 'Isolation Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # iso_year varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_year';
    $attr->{display_name} = 'Collection Date';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIMS 5 <font color=red>  </font>'; 
    $attr->{migs_name} = 'COLLECTION DATE';
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # iso_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_country';
    $attr->{display_name} = 'Isolation Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIMS 5 <font color=red>  </font>'; 
    $attr->{migs_name} = 'COUNTRY'; 
    push @{$table->{attrs}}, ( $attr );

    # iso_pubmed_id int 
    $attr = new RelAttr( ); 
    $attr->{name} = 'iso_pubmed_id'; 
    $attr->{display_name} = 'Isolation Pubmed ID'; 
    $attr->{data_type} = 'int'; 
    $attr->{length} = 40; 
    $attr->{is_required} = 0; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta'; 
    $attr->{migs_id} = 'MIMS 12 <font color=red>  </font>'; 
    $attr->{migs_name} = 'REFS FOR BIOMATERIAL'; 
    push @{$table->{attrs}}, ( $attr ); 

    # geo_location varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'geo_location';
    $attr->{display_name} = 'Geographic Location';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 1;
    $attr->{definition} = 'Definition -  Geographic location of where the dna was isolated.'; 
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # loc_coord varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'loc_coord';
#    $attr->{display_name} = 'Location Coordinates';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metagenome';
#    push @{$table->{attrs}}, ( $attr );

    # latitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'latitude';
    $attr->{display_name} = 'Latitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIMS 4.1 <font color=red>  </font>'; 
    $attr->{migs_name} = 'LATITUDE';
    push @{$table->{attrs}}, ( $attr );

    # longitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longitude';
    $attr->{display_name} = 'Longitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIMS 4.2 <font color=red>  </font>';
    $attr->{migs_name} = 'LONGITUDE';
    push @{$table->{attrs}}, ( $attr );

    # coordinate_type
    $attr = new RelAttr( );
    $attr->{name} = 'coordinate_type';
    $attr->{display_name} = 'Lat/Long Information';
    $attr->{data_type} = 'cv';
    $attr->{length} = 32;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from cvdata_validity_type";
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    push @{$table->{attrs}}, ( $attr );

    # altitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'altitude';
    $attr->{display_name} = 'Altitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'EnvMeta';
    $attr->{migs_id} = 'MIMS 4.3'; 
    $attr->{migs_name} = 'ALTITUDE'; 
    push @{$table->{attrs}}, ( $attr );

    # depth varchar2(255), 
    $attr = new RelAttr( ); 
    $attr->{name} = 'depth'; 
    $attr->{display_name} = 'Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255; 
    $attr->{is_required} = 0; 
    $attr->{can_edit} = 1; 
    $attr->{tab} = 'EnvMeta'; 
    $attr->{migs_id} = 'MIMS 4.4'; 
    $attr->{migs_name} = 'DEPTH'; 
    push @{$table->{attrs}}, ( $attr ); 

    # energy_source varchar2(500),
#    $attr = new RelAttr( );
#    $attr->{name} = 'energy_source';
#    $attr->{display_name} = 'Energy Source';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Organism';
#    push @{$table->{attrs}}, ( $attr );

    # oxygen_req varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'oxygen_req';
    $attr->{display_name} = 'Oxygen Requirement';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from oxygencv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'OrganMeta';
    $attr->{migs_id} = 'MIMS 22'; 
    $attr->{migs_name} = 'RELATIONSHIP TO OXYGEN'; 
    push @{$table->{attrs}}, ( $attr );

    # cell_shape varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'cell_shape';
#    $attr->{display_name} = 'Cell Shape';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from cell_shapecv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # cell_arrangement varchar2(255),
    # change to multi-valued
#    $attr = new RelAttr( );
#    $attr->{name} = 'cell_arrangement';
#    $attr->{display_name} = 'Cell Arrangement';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from cell_arrcv";
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # motility varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'motility';
#    $attr->{display_name} = 'Motility';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from motilitycv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # sporulation varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'sporulation';
#    $attr->{display_name} = 'Sporulation';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from sporulationcv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # temp_range varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'temp_range';
    $attr->{display_name} = 'Temperature Range';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from temp_rangecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # temp_optimum varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'temp_optimum';
#    $attr->{display_name} = 'Temperature Optimum';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # salinity varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'salinity';
    $attr->{display_name} = 'Salinity';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from salinitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # pressure varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pressure';
    $attr->{display_name} = 'Pressure';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # ph varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ph';
    $attr->{display_name} = 'pH';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_name';
    $attr->{display_name} = 'Host Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'HostMeta';
    $attr->{migs_id} = 'MIMS 16';
    $attr->{migs_name} = 'SPECIFIC HOST';
    push @{$table->{attrs}}, ( $attr );

    # host_taxon_id int
    $attr = new RelAttr( );
    $attr->{name} = 'host_taxon_id';
    $attr->{display_name} = 'Host Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_spec int
    $attr = new RelAttr( );
    $attr->{name} = 'host_spec';
    $attr->{display_name} = 'Host Specificity or Range';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    $attr->{migs_id} = 'MIMS 17';
    $attr->{migs_name} = 'HOST SPECIFICITY OR RANGE';
    push @{$table->{attrs}}, ( $attr );

    # host_gender varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_gender';
    $attr->{display_name} = 'Host Gender';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_race varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_race';
    $attr->{display_name} = 'Host Race';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_age varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_age';
    $attr->{display_name} = 'Host Age';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_health varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_health';
    $attr->{display_name} = 'Host Health';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{migs_id} = 'MIMS 18';
    $attr->{migs_name} = 'HEALTH STATUS OF HOST';
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # host_medication varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_medication';
    $attr->{display_name} = 'Host Medication';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

#     # body_sample_site varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_sample_site';
#     $attr->{display_name} = 'Primary Body Sample Site';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_sitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

#     # body_product varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_product';
#     $attr->{display_name} = 'Body Product';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_productcv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );


#     # body_sample_subsite varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_sample_subsite';
#     $attr->{display_name} = 'Body Sample SubSite';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_subsitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

#     # additional_body_sample_site varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'additional_body_sample_site';
#     $attr->{display_name} = 'Additional Body Sample Site';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_sitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

    # host_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'host_comments';
    $attr->{display_name} = 'Host Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HostMeta';
    push @{$table->{attrs}}, ( $attr );

    # cell_diameter varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'cell_diameter';
#    $attr->{display_name} = 'Cell Diameter';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # cell_length varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'cell_length';
#    $attr->{display_name} = 'Cell Length';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # change to multi-valued
    # metabolism varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'metabolism';
#    $attr->{display_name} = 'Metabolism';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from metabolismcv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # color varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'color';
#    $attr->{display_name} = 'Color';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from colorcv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Metadata';
#    push @{$table->{attrs}}, ( $attr );

    # seq_status_link varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status_link';
    $attr->{display_name} = 'Seq Status Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # library_method varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'library_method';
    $attr->{display_name} = 'Library Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIMS 28.1';
    $attr->{migs_name} = 'LIBRARY SIZE';
    push @{$table->{attrs}}, ( $attr );

    # no_of_reads int 
    $attr = new RelAttr( ); 
    $attr->{name} = 'no_of_reads'; 
    $attr->{display_name} = 'Number of Reads'; 
    $attr->{data_type} = 'int'; 
    $attr->{length} = 40; 
    $attr->{is_required} = 0; 
    $attr->{tab} = 'Sequencing'; 
    $attr->{migs_id} = 'MIMS 28.2'; 
    $attr->{migs_name} = 'NUMBER OF READS'; 
    push @{$table->{attrs}}, ( $attr ); 
 
    # vector varchar2(1000), 
    $attr = new RelAttr( ); 
    $attr->{name} = 'vector'; 
    $attr->{display_name} = 'Vector'; 
    $attr->{data_type} = 'char'; 
    $attr->{length} = 1000; 
    $attr->{is_required} = 0; 
    $attr->{tab} = 'Sequencing'; 
    $attr->{migs_id} = 'MIMS 28.3'; 
    $attr->{migs_name} = 'VECTOR'; 
    push @{$table->{attrs}}, ( $attr ); 
 
    # binning_method varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'binning_method';
#    $attr->{display_name} = 'Binning Method';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # assembly_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'assembly_method';
    $attr->{display_name} = 'Assembly Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIMS 30 <font color=red>  </font>'; 
    $attr->{migs_name} = 'ASSEMBLY'; 
    push @{$table->{attrs}}, ( $attr );

    # seq_depth varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'seq_depth';
#    $attr->{display_name} = 'Sequencing Depth';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    $attr->{migs_id} = 'MIMS 31.2'; 
#    $attr->{migs_name} = 'SEQUENCING COVERAGE';
#    push @{$table->{attrs}}, ( $attr );

    # gene_calling_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'gene_calling_method';
    $attr->{display_name} = 'Gene Calling Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # contig_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'contig_count';
    $attr->{display_name} = 'Contig Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIMS 31.3'; 
    $attr->{migs_name} = 'CONTIGS'; 
    push @{$table->{attrs}}, ( $attr );

    # est_size int,
    $attr = new RelAttr( );
    $attr->{name} = 'est_size';
    $attr->{display_name} = 'Estimated Size';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIMS 11'; 
    $attr->{migs_name} = 'ESTIMATED SIZE'; 
    push @{$table->{attrs}}, ( $attr );

    # units varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'units';
    $attr->{display_name} = 'Units';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from unitcv";
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # genome_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'genome_count';
#    $attr->{display_name} = 'Community Diversity (est. number)';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # gene_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'gene_count';
    $attr->{display_name} = 'Gene Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );
    
    # chromosome_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'chromosome_count';
#    $attr->{display_name} = 'Chromosome Count';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    $attr->{migs_id} = 'MIMS 9'; 
#    $attr->{migs_name} = 'NUMBER OF REPLICONS'; 
#    push @{$table->{attrs}}, ( $attr );

    # plasmid_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'plasmid_count';
#    $attr->{display_name} = 'Plasmid Count';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    $attr->{migs_id} = 'MIMS 10'; 
#    $attr->{migs_name} = 'EXTRACHROMOSOMAL ELEMENTS'; 
#    push @{$table->{attrs}}, ( $attr );

    # singlet_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'singlet_count';
#    $attr->{display_name} = 'Singlet Count';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # seq_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_country';
    $attr->{display_name} = 'Sequencing Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # completion_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'completion_date';
    $attr->{display_name} = 'Completion Date (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pubmed_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmid';
    $attr->{display_name} = 'Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{filter_cond} = 1;
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_journal varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'pub_journal';
    $attr->{display_name} = 'Publication Journal';
    $attr->{data_type} = 'cv';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from publication_journalcv";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_vol varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'pub_vol';
    $attr->{display_name} = 'Publication Vol.';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_link varchar2(500)
    $attr = new RelAttr( );
    $attr->{name} = 'pub_link';
    $attr->{display_name} = 'Publication Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # refseq_ids varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'refseq_ids';
    $attr->{display_name} = 'Refseq ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # refseq_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'refseq_link_url';
    $attr->{display_name} = 'Refseq link URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # map_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'map_link_url';
    $attr->{display_name} = 'NCBI Map link URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # growth_conditions varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'growth_conditions';
    $attr->{display_name} = 'Organism Growth Conditions';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{hint} = '';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # proj_desc varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'proj_desc';
    $attr->{display_name} = 'Project Description';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 1;
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # wiki_link_url varchar2(500),
#    $attr = new RelAttr( );
#    $attr->{name} = 'wiki_link_url';
#    $attr->{display_name} = 'Wiki link URL';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Links';
#    push @{$table->{attrs}}, ( $attr );

    # information_name varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'information_name';
    $attr->{display_name} = 'Information Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # information_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'information_url';
    $attr->{display_name} = 'Information URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # contact_oid int
    $attr = new RelAttr( );
    $attr->{name} = 'contact_oid';
    $attr->{display_name} = 'IMG Contact';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # add_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'add_date';
    $attr->{display_name} = 'Add Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{header} = 'Database Update Info';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Last Modify Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Last Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # show_in_dacc
    $attr = new RelAttr( );
    $attr->{name} = 'show_in_dacc';
    $attr->{display_name} = 'Show in DACC?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_project_status';
    $attr->{display_name} = 'HMP Project Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from HMP_project_statuscv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # bei_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'bei_status';
    $attr->{display_name} = 'BEI Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from BEI_statuscv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_submit_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_submit_status';
    $attr->{display_name} = 'NCBI Submission Status';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from ncbi_submit_statuscv";
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

#     # isolate_selected
#     $attr = new RelAttr( );
#     $attr->{name} = 'isolate_selected';
#     $attr->{display_name} = 'Isolate Selected by Working Group';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from yesnocv";
#     $attr->{length} = 10;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{can_edit} = 1;
#     $attr->{tab} = 'HMP';
#     push @{$table->{attrs}}, ( $attr );

    # isolate_selected
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_isolate_selection_source';
    $attr->{display_name} = 'Isolate Suggestion Source';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from isolate_suggestion_sourcecv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # project_goal varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_goal';
    $attr->{display_name} = 'Finishing Goal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from project_goalcv";
    $attr->{length} = 255;
    $attr->{hmp_cond} = 1; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_iso_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_iso_comments';
    $attr->{display_name} = 'Source, Sample & Medical Relevance Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_iso_avail_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_iso_avail_comments';
    $attr->{display_name} = 'Isolate Availability Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_project_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_project_comments';
    $attr->{display_name} = 'Project Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # dna_received
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_dna_received';
    $attr->{display_name} = 'DNA Received';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_date_dna_received date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_date_dna_received';
    $attr->{display_name} = 'Date DNA Received (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_seq_begin_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_seq_begin_date';
    $attr->{display_name} = 'Date Sequencing Begins (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_draft_seq_completion_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_draft_seq_completion_date';
    $attr->{display_name} = 'Date Draft Sequencing Completed (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # jgi_proposal_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_proposal_id';
    $attr->{display_name} = 'JGI Proposal ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # pmo_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmo_project_id';
    $attr->{display_name} = 'PMO Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scientific_program varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'scientific_program';
    $attr->{display_name} = 'Scientific Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sci_progcv";
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_final_deliverable varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_final_deliverable';
    $attr->{display_name} = 'ITS Final Deliverable';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvjgi_its_final_deliverable";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_product_name varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_product_name';
    $attr->{display_name} = 'JGI Product Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scope_of_work varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'scope_of_work';
    $attr->{display_name} = 'JGI Scope of Work';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select scope from workscopecv order by scope";
#    $attr->{tab} = 'Sequencing Information';
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_project_type varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_project_type';
    $attr->{display_name} = 'JGI Project Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 256;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_status varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_status';
    $attr->{display_name} = 'JGI Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_last_mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_last_mod_date';
    $attr->{display_name} = 'GPTS Last Update Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_comments varchar(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_comments';
    $attr->{display_name} = 'GPTS Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_program varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_program';
    $attr->{display_name} = 'JGI Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from jgi_funding_progcv";
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_year int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_year';
    $attr->{display_name} = 'JGI Funding Year';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_dir_number int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_dir_number';
    $attr->{display_name} = 'JGI Directory Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_visibility varchar(64)
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_visibility';
    $attr->{display_name} = 'Visibility';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # its_spid int,
    $attr = new RelAttr( );
    $attr->{name} = 'its_spid';
    $attr->{display_name} = 'ITS Sequencing ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_cell_arrangement
###########################################################################
sub def_meta_project_info_cell_arrangement {
    my $table = new RelTable( );
    $table->{name} = 'project_info_cell_arrangement';
    $table->{display_name} = 'Cell Arrangements';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta'; 
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'cell_arrangement';
    $attr->{display_name} = 'Cell Arrangement';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cell_arrcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_diseases
###########################################################################
sub def_meta_project_info_diseases {
    my $table = new RelTable( );
    $table->{name} = 'project_info_diseases';
    $table->{display_name} = 'Diseases';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIMS 14';
    $table->{migs_name} = 'PATHOGENICITY'; 
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'diseases';
    $attr->{display_name} = 'Diseases';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from diseasecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_habitat
###########################################################################
sub def_meta_project_info_habitat {
    my $table = new RelTable( );
    $table->{name} = 'project_info_habitat';
    $table->{display_name} = 'Habitat';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIMS 6 <font color=red>  </font>';
    $table->{migs_name} = 'HABITAT'; 
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'habitat';
    $attr->{display_name} = 'Habitat';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from habitatcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_metabolism
###########################################################################
sub def_meta_project_info_metabolism {
    my $table = new RelTable( );
    $table->{name} = 'project_info_metabolism';
    $table->{display_name} = 'Metabolism';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'metabolism';
    $attr->{display_name} = 'Metabolism';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from metabolismcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_phenotypes
###########################################################################
sub def_meta_project_info_phenotypes {
    my $table = new RelTable( );
    $table->{name} = 'project_info_phenotypes';
    $table->{display_name} = 'Phenotypes';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # phenotypes
    $attr = new RelAttr( );
    $attr->{name} = 'phenotypes';
    $attr->{display_name} = 'Phenotypes';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from phenotypecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_project_relevance
###########################################################################
sub def_meta_project_info_project_relevance {
    my $table = new RelTable( );
    $table->{name} = 'project_info_project_relevance';
    $table->{display_name} = 'Project Relevance';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Project';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # project_relevance
    $attr = new RelAttr( );
    $attr->{name} = 'project_relevance';
    $attr->{display_name} = 'Project Relevance';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from relevancecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_seq_centers 
###########################################################################
sub def_meta_project_info_seq_centers {
    my $table = new RelTable( );
    $table->{name} = 'project_info_seq_centers';
    $table->{display_name} = 'Sequencing Center';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # centers
    $attr = new RelAttr( );
    $attr->{name} = 'center';
    $attr->{display_name} = 'Sequencing Center';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 60;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_meta_project_info_seq_method
###########################################################################
sub def_meta_project_info_seq_method {
    my $table = new RelTable( );
    $table->{name} = 'project_info_seq_method';
    $table->{display_name} = 'Sequencing Methods';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Sequencing';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIMS 29 <font color=red>  </font>';
    $table->{migs_name} = 'SEQUENCING METHOD';
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # seq_method
    $attr = new RelAttr( );
    $attr->{name} = 'seq_method';
    $attr->{display_name} = 'Sequencing Method';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_methodcv";
    $attr->{length} = 255;
    $attr->{default_size} = 60;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_meta_project_info_funding_agencies 
###########################################################################
sub def_meta_project_info_funding_agencies {
    my $table = new RelTable( );
    $table->{name} = 'project_info_funding_agencies';
    $table->{display_name} = 'Funding Agencies';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    # agency
    $attr = new RelAttr( );
    $attr->{name} = 'agency';
    $attr->{display_name} = 'Agency';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 60;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_data_links
###########################################################################
sub def_meta_project_info_data_links {
    my $table = new RelTable( );
    $table->{name} = 'project_info_data_links';
    $table->{display_name} = 'Data Links (URLs)';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIMS 32; MIMS 33';
    $table->{migs_name} = 'RELEVANT SOPS; e-RESOURCES';
    $table->{new_rows} = 10;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'link_type';
    $attr->{display_name} = 'Link Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from link_typecv";
    $attr->{length} = 20;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

#    $attr = new RelAttr( );
#    $attr->{name} = 'db_name';
#    $attr->{display_name} = 'Source Name';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{default_size} = 30;
#    $attr->{is_required} = 0;
#    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_name';
    $attr->{display_name} = 'Source Name';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from db_namecv order by 1";
    $attr->{length} = 255;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'id';
    $attr->{display_name} = 'ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_jgi_url
###########################################################################
sub def_meta_project_info_jgi_url {
    my $table = new RelTable( );
    $table->{name} = 'project_info_jgi_url';
    $table->{display_name} = 'JGI URL';
    $table->{id} = 'project_oid';
    $table->{tab} = 'Project';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 5;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_name';
    $attr->{display_name} = 'DB Name';
    $attr->{data_type} = 'char';
#    $attr->{cv_query} = "select cv_term from db_namecv order by 1";
    $attr->{length} = 80;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'db_id';
    $attr->{display_name} = 'DB ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{default_size} = 20;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'url';
    $attr->{display_name} = 'URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_info_publications
###########################################################################
sub def_meta_project_info_publications {
    my $table = new RelTable( );
    $table->{name} = 'project_info_publications';
    $table->{display_name} = 'Publications';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'journal';
    $attr->{display_name} = 'Journal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from publication_journalcv";
    $attr->{length} = 512;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'pmid';
    $attr->{display_name} = 'Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

###########################################################################
# def_meta_project_energy_source
###########################################################################
sub def_meta_project_info_energy_source {
    my $table = new RelTable( );
    $table->{name} = 'project_info_energy_source';
    $table->{display_name} = 'Energy Sources';
    $table->{id} = 'project_oid';
    $table->{tab} = 'OrganMeta';
    $table->{multipart_form} = 0;
    $table->{migs_id} = 'MIMS 19'; 
    $table->{migs_name} = 'TROPHIC LEVEL'; 
    $table->{new_rows} = 3;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'energy_source';
    $attr->{display_name} = 'Energy Source';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from energy_sourcecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{default_size} = 80;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Cell_arrCv
#######################################################################
sub def_Cell_arrCv {

    my $table = new RelTable( );
    $table->{name} = 'cell_arrcv';
    $table->{display_name} = 'Cell Arrangement CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_cell_arrangement|cell_arrangement";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Cell Arrangement';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Cell_shapeCv
#######################################################################
sub def_Cell_shapeCv {

    my $table = new RelTable( );
    $table->{name} = 'cell_shapecv';
    $table->{display_name} = 'Cell Shape CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|cell_shape";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Cell Shape';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_ColorCv - color CV
#######################################################################
sub def_ColorCv {

    my $table = new RelTable( );
    $table->{name} = 'colorcv';
    $table->{display_name} = 'Color CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|color";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Color';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Cultured_typeCv
#######################################################################
sub def_Cultured_typeCv {

    my $table = new RelTable( );
    $table->{name} = 'cvculture_type';
    $table->{display_name} = 'Uncultured Type CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|culture_type";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Cultured Type Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Bin_MethodCv - bin method CV
#######################################################################
sub def_Bin_MethodCv {

    my $table = new RelTable( );
    $table->{name} = 'bin_methodcv';
    $table->{display_name} = 'Bin Method CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "submission|bin_method";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Bin Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Body_SiteCv - body sample site CV
#######################################################################
sub def_Body_SiteCv {

    my $table = new RelTable( );
    $table->{name} = 'body_sitecv';
    $table->{display_name} = 'Body Sample Site CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_body_sites|body_sample_site";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Body Sample Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Body_SubSiteCv - body sample sub-site CV
#######################################################################
sub def_Body_SubSiteCv {

    my $table = new RelTable( );
    $table->{name} = 'body_subsitecv';
    $table->{display_name} = 'Body Sample SubSite CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_body_sites|body_sample_subsite";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Body Sample SubSite';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Body_ProductCv - body sample sub-site CV
#######################################################################
sub def_Body_ProductCv {

    my $table = new RelTable( );
    $table->{name} = 'body_productcv';
    $table->{display_name} = 'Body Product CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_body_products|body_product";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Body Product';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_CountryCv - country CV
#######################################################################
sub def_CountryCv {

    my $table = new RelTable( );
    $table->{name} = 'countrycv';
    $table->{display_name} = 'Country CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|seq_country,project_info|iso_country,contact|country,request_account|country";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Country Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_CollaboratorCv - collaborator CV
#######################################################################
sub def_CollaboratorCv {

    my $table = new RelTable( );
    $table->{name} = 'collaboratorcv';
    $table->{display_name} = 'Collaborator CV';
    $table->{id} = 'name';
    $table->{ref_list} = "project_info_collaborators|collaborator_id";

    # name
    my $attr = new RelAttr( );
    $attr->{name} = 'collaborator_id';
    $attr->{display_name} = 'Collaborator ID';
    $attr->{data_type} = 'number';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );
    $attr = new RelAttr( );
    $attr->{name} = 'name';
    $attr->{display_name} = 'Collaborator Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );
    $attr = new RelAttr( );
    $attr->{name} = 'link';
    $attr->{display_name} = 'Collaborator Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_SequencingCenterCv - Sequencing Center CV
#######################################################################
sub def_SequencingCenterCv {

    my $table = new RelTable( );
    $table->{name} = 'seq_centercv';
    $table->{display_name} = 'Sequencing Center CV';
    $table->{id} = 'name';
    $table->{ref_list} = "project_info|seq_center_id";

    # name
    my $attr = new RelAttr( );
    $attr->{name} = 'seq_center_id';
    $attr->{display_name} = 'Seq Center ID';
    $attr->{data_type} = 'number';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );
    $attr = new RelAttr( );
    $attr->{name} = 'name';
    $attr->{display_name} = 'Sequencing Center Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );
    $attr = new RelAttr( );
    $attr->{name} = 'link';
    $attr->{display_name} = 'Sequencing Center Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_DB_NameCv - db_name CV
#######################################################################
sub def_DB_NameCv {

    my $table = new RelTable( );
    $table->{name} = 'db_namecv';
    $table->{display_name} = 'Source Name CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_data_links|db_name";

    # cv_term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Source Name Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_DiseaseCv - disease CV
#######################################################################
sub def_DiseaseCv {

    my $table = new RelTable( );
    $table->{name} = 'diseasecv';
    $table->{display_name} = 'Disease CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_diseases|diseases,env_sample_diseases|diseases";

#    my $attr = new RelAttr( );
#    $attr->{name} = 'term_oid';
#    $attr->{display_name} = 'Disease Term OID';
#    $attr->{data_type} = 'char';
#    $attr->{is_required} = 255;
#    $attr->{can_edit} = 0;
#    $attr->{copy_from} = 'cv_term';
#    push @{$table->{attrs}}, ( $attr );

    # cv_term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Disease Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Energy_sourceCv - energy source CV
#######################################################################
sub def_Energy_sourceCv {

    my $table = new RelTable( );
    $table->{name} = 'energy_sourcecv';
    $table->{display_name} = 'Energy Source CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_energy_source|energy_source";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Energy Source Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Funding_programCv - funding program
#######################################################################
sub def_Funding_programCv {

    my $table = new RelTable( );
    $table->{name} = 'funding_programcv';
    $table->{display_name} = 'Funding Program CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|funding_program";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Funding Program Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_HabitatCv - habitat CV
#######################################################################
sub def_HabitatCv {

    my $table = new RelTable( );
    $table->{name} = 'habitatcv';
    $table->{display_name} = 'Habitat CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_habitat|habitat,env_sample_habitat_type|habitat_type";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Habitat Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    # envo_id
    $attr = new RelAttr( );
    $attr->{name} = 'envo_id';
    $attr->{display_name} = 'ENVO ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Habitat_CategoryCv
#######################################################################
sub def_Habitat_CategoryCv {

    my $table = new RelTable( );
    $table->{name} = 'habitat_categorycv';
    $table->{display_name} = 'Habitat Category CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|habitat_category";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Habitat Category Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_JGI_funding_progCv
#######################################################################
sub def_JGI_funding_progCv {

    my $table = new RelTable( );
    $table->{name} = 'jgi_funding_progcv';
    $table->{display_name} = 'JGI Funding Program CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|jgi_funding_program,env_sample|jgi_funding_program";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'JGI Funding Program';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_JGI_final_deliverableCv
#######################################################################
sub def_JGI_final_deliverableCv {

    my $table = new RelTable( );
    $table->{name} = 'cvjgi_its_final_deliverable';
    $table->{display_name} = 'JGI ITS Final Deliverable CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|jgi_final_deliverable";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'JGI ITS Final Deliverable';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_EcosystemCv
#######################################################################
sub def_EcosystemCv {

    my $table = new RelTable( );
    $table->{name} = 'cvecosystem';
    $table->{display_name} = 'Ecosystem CV';
    $table->{id} = 'ecosystem';
    $table->{ref_list} = "project_info|ecosystem";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'ecosystem';
    $attr->{display_name} = 'Ecosystem Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Ecosystem_CategoryCv
#######################################################################
sub def_Ecosystem_CategoryCv {

    my $table = new RelTable( );
    $table->{name} = 'cvecosystem_category';
    $table->{display_name} = 'Ecosystem Category CV';
    $table->{id} = 'ecosystem_category';
    $table->{ref_list} = "project_info|ecosystem_category";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_category';
    $attr->{display_name} = 'Ecosystem Category Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Ecosystem_TypeCv
#######################################################################
sub def_Ecosystem_TypeCv {

    my $table = new RelTable( );
    $table->{name} = 'cvecosystem_type';
    $table->{display_name} = 'Ecosystem Type CV';
    $table->{id} = 'ecosystem_type';
    $table->{ref_list} = "project_info|ecosystem_type";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_type';
    $attr->{display_name} = 'Ecosystem Type Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Ecosystem_SubtypeCv
#######################################################################
sub def_Ecosystem_SubtypeCv {

    my $table = new RelTable( );
    $table->{name} = 'cvecosystem_subtype';
    $table->{display_name} = 'Ecosystem Subtype CV';
    $table->{id} = 'ecosystem_subtype';
    $table->{ref_list} = "project_info|ecosystem_subtype";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_subtype';
    $attr->{display_name} = 'Ecosystem Subtype Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Specific_EcosystemCv
#######################################################################
sub def_Specific_EcosystemCv {

    my $table = new RelTable( );
    $table->{name} = 'cvspecific_ecosystem';
    $table->{display_name} = 'Specific Ecosystem CV';
    $table->{id} = 'specific_ecosystem';
    $table->{ref_list} = "project_info|specific_ecosystem";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'specific_ecosystem';
    $attr->{display_name} = 'Specific Ecosystem Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_MetabolismCv - metabolism CV
#######################################################################
sub def_MetabolismCv {

    my $table = new RelTable( );
    $table->{name} = 'metabolismcv';
    $table->{display_name} = 'Metabolism CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_metabolism|metabolism,env_sample_metabolism|metabolism";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Country Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_MotilityCv
#######################################################################
sub def_MotilityCv {

    my $table = new RelTable( );
    $table->{name} = 'motilitycv';
    $table->{display_name} = 'Motility CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|motility";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Motility';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_NCBI_Submit_StatusCv
#######################################################################
sub def_NCBI_Submit_StatusCv {

    my $table = new RelTable( );
    $table->{name} = 'ncbi_submit_statuscv';
    $table->{display_name} = 'NCBI Submission Status CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|ncbi_submit_status";

    # cv_term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'NCBI Submission Status Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Organism_typeCv: organism type
#######################################################################
sub def_Organism_typeCv {

    my $table = new RelTable( );
    $table->{name} = 'cv_organism_type';
    $table->{display_name} = 'Organism Type CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|organism_type";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Organism Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_OxygenCv - oxygen CV
#######################################################################
sub def_OxygenCv {

    my $table = new RelTable( );
    $table->{name} = 'oxygencv';
    $table->{display_name} = 'Oxygen Requirement CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|oxygen_req,env_sample|oxygen_req";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Oxygen Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_PhenotypeCv - phenotype CV
#######################################################################
sub def_PhenotypeCv {

    my $table = new RelTable( );
    $table->{name} = 'phenotypecv';
    $table->{display_name} = 'Phenotype CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_phenotypes|phenotypes,env_sample_phenotypes|phenotypes";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Phenotype Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_PhylogenyCv
#######################################################################
sub def_PhylogenyCv {

    my $table = new RelTable( );
    $table->{name} = 'phylogenycv';
    $table->{display_name} = 'Phylogeny CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|phylogeny";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Phylogeny Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Project_statusCv - project_status CV
#######################################################################
sub def_Project_statusCv {

    my $table = new RelTable( );
    $table->{name} = 'project_statuscv';
    $table->{display_name} = 'Project Status CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|project_status";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Project Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Project_typeCv - project_type CV
#######################################################################
sub def_Project_typeCv {

    my $table = new RelTable( );
    $table->{name} = 'project_typecv';
    $table->{display_name} = 'Project Type CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|project_type";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Project Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_RelevanceCv - Relevance CV
#######################################################################
sub def_RelevanceCv {

    my $table = new RelTable( );
    $table->{name} = 'relevancecv';
    $table->{display_name} = 'Relevance CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_project_relevance|project_relevance";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Relevance Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_SalinityCv
#######################################################################
sub def_SalinityCv {

    my $table = new RelTable( );
    $table->{name} = 'salinitycv';
    $table->{display_name} = 'Salinity CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|salinity,env_sample|salinity";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Salinity';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Sci_progCv
#######################################################################
sub def_Sci_progCv {

    my $table = new RelTable( );
    $table->{name} = 'sci_progcv';
    $table->{display_name} = 'Scientific Program CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|scientific_program,env_sample|scientific_program";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Scientific Program';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Seq_methodCv - seq_method CV
#######################################################################
sub def_Seq_methodCv {

    my $table = new RelTable( );
    $table->{name} = 'seq_methodcv';
    $table->{display_name} = 'Sequencing Method CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info_seq_method|seq_method,env_sample_seq_method|seq_method";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Sequencing Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Seq_centerCv - seq_center CV
#######################################################################
sub def_Seq_centerCv {

    my $table = new RelTable( );
    $table->{name} = 'seq_centercv';
    $table->{display_name} = 'Sequencing Center CV';
    $table->{id} = 'number';
    $table->{ref_list} = "project_info|seq_center_id";

    # seq_center_id
    my $attr = new RelAttr( );
    $attr->{name} = 'seq_center_id';
    $attr->{display_name} = 'Sequencing Center ID';
    $attr->{data_type} = 'number';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'name';
    $attr->{display_name} = 'Sequencing Center Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    $attr = new RelAttr( );
    $attr->{name} = 'link';
    $attr->{display_name} = 'Sequencing Center Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Seq_qualityCv - seq_quality CV
#######################################################################
sub def_Seq_qualityCv {

    my $table = new RelTable( );
    $table->{name} = 'seq_qualitycv';
    $table->{display_name} = 'Sequencing Quality CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|seq_quality";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Sequencing Quality';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_Seq_statusCv - seq_status CV
#######################################################################
sub def_Seq_statusCv {

    my $table = new RelTable( );
    $table->{name} = 'seq_statuscv';
    $table->{display_name} = 'Sequencing Status CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|seq_status";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_HMP_Project_StatusCv - seq_status CV
#######################################################################
sub def_HMP_Project_StatusCv {

    my $table = new RelTable( );
    $table->{name} = 'HMP_project_statuscv';
    $table->{display_name} = 'HMP Project Status CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|hmp_project_status";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'HMP Project Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_BEI_statusCv - BEI_status CV
#######################################################################
sub def_BEI_statusCv {

    my $table = new RelTable( );
    $table->{name} = 'BEI_statuscv';
    $table->{display_name} = 'BEI Status CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|BEI_status";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'BEI Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Publication_journalCv - Publication_journal CV
#######################################################################
sub def_Publication_journalCv {

    my $table = new RelTable( );
    $table->{name} = 'publication_journalcv';
    $table->{display_name} = 'Publication Journal CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|pub_journal";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Publication Journal';
    $attr->{data_type} = 'char';
    $attr->{length} = 512;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Project_goalCv - Project_goal CV
#######################################################################
sub def_Project_goalCv {

    my $table = new RelTable( );
    $table->{name} = 'project_goalcv';
    $table->{display_name} = 'Project (Finishing) Goal CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|project_goal";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Project Goal';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_SporulationCv
#######################################################################
sub def_SporulationCv {

    my $table = new RelTable( );
    $table->{name} = 'sporulationcv';
    $table->{display_name} = 'Sporulation CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|sporulation";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Sporulation';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Temp_rangeCv - temp range CV
#######################################################################
sub def_Temp_rangeCv {

    my $table = new RelTable( );
    $table->{name} = 'temp_rangecv';
    $table->{display_name} = 'Temperature Range CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|temp_range";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Temp Range Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#######################################################################
# def_Uncultured_typeCv
#######################################################################
sub def_Uncultured_typeCv {

    my $table = new RelTable( );
    $table->{name} = 'cvuncultured_type';
    $table->{display_name} = 'Uncultured Type CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|uncultured_type";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Uncultured Type Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}

#######################################################################
# def_UnitCv - unit CV
#######################################################################
sub def_UnitCv {

    my $table = new RelTable( );
    $table->{name} = 'unitcv';
    $table->{display_name} = 'Unit CV';
    $table->{id} = 'cv_term';
    $table->{ref_list} = "project_info|units,env_sample|units";

    # term
    my $attr = new RelAttr( );
    $attr->{name} = 'cv_term';
    $attr->{display_name} = 'Unit Term';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


#########################################################################
# def_Contact
#########################################################################
sub def_Contact {
    my $table = new RelTable( );
    $table->{name} = 'contact';
    $table->{display_name} = 'IMG Contact';
    $table->{id} = 'contact_oid';
    $table->{multipart_form} = 1;

    # contact_oid int not null
    my $attr = new RelAttr( );
    $attr->{name} = 'contact_oid';
    $attr->{display_name} = 'IMG Contact ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # username varchar(20),
    $attr = new RelAttr( );
    $attr->{name} = 'username';
    $attr->{display_name} = 'IMG User Name';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 20;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # name varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'name';
    $attr->{display_name} = 'Name';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # title varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'title';
    $attr->{display_name} = 'Title';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # department varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'department';
    $attr->{display_name} = 'Department';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # email varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'email';
    $attr->{display_name} = 'Email';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # phone varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'phone';
    $attr->{display_name} = 'Phone';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # organization varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'organization';
    $attr->{display_name} = 'Organization';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # address varchar(1000)
    $attr = new RelAttr( );
    $attr->{name} = 'address';
    $attr->{display_name} = 'Address';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # city varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'city';
    $attr->{display_name} = 'City';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # state varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'state';
    $attr->{display_name} = 'State';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # country varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'country';
    $attr->{display_name} = 'Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Contact Info';
    push @{$table->{attrs}}, ( $attr );

    # comments varchar(255)
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Comments';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # add_date date
    $attr = new RelAttr( );
    $attr->{name} = 'add_date';
    $attr->{display_name} = 'Add Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # super_user
    $attr = new RelAttr( );
    $attr->{name} = 'super_user';
    $attr->{display_name} = 'Super User?';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Yes|No";
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # is_group_account
    $attr = new RelAttr( );
    $attr->{name} = 'is_group_account';
    $attr->{display_name} = 'Is Group Account (for internal use)?';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Yes|No";
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # img_editor
    $attr = new RelAttr( );
    $attr->{name} = 'img_editor';
    $attr->{display_name} = 'Can Perform IMG Data Curation?';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Yes|No";
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # img_group int
    $attr = new RelAttr( );
    $attr->{name} = 'img_group';
    $attr->{display_name} = 'IMG Group (single)';
    $attr->{data_type} = 'cv2';
    $attr->{cv_query} = "select contact_oid, username from contact order by username, contact_oid";
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
#    $attr->{can_edit} = 0;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # jgi_user
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_user';
    $attr->{display_name} = 'JGI User?';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Yes|No";
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # img_editing_level varchar(2000)
    $attr = new RelAttr( );
    $attr->{name} = 'img_editing_level';
    $attr->{display_name} = 'IMG Editing Level';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # caliban_id int
    $attr = new RelAttr( );
    $attr->{name} = 'caliban_id';
    $attr->{display_name} = 'Caliban ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    # caliban_user_name varchar(512),
    $attr = new RelAttr( );
    $attr->{name} = 'caliban_user_name';
    $attr->{display_name} = 'Caliban User Name(s)';
    $attr->{data_type} = 'varchar';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Account Info';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}



###########################################################################
# def_Analysis_Project - Analysis Project
###########################################################################
sub def_Analysis_Project {
    my $table = new RelTable( );
    $table->{name} = 'gold_analysis_project';
    $table->{display_name} = 'Analysis Project Information';
    $table->{id} = 'project_oid';
    $table->{multipart_form} = 0;

    # project_oid int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'project_oid';
    $attr->{display_name} = 'Analysis Project ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 20;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # gold_id varchar2(50) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'goldId';
    $attr->{display_name} = 'GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # proposal name
    $attr = new RelAttr( );
    $attr->{name} = 'goldProposalName';
    $attr->{display_name} = 'Proposal Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the title of the proposal or the name of the umbrella project.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # analysis_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'analysisProjectName';
    $attr->{display_name} = 'Analysis Project Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    $attr->{migs_id} = 'MIGS 3 <font color=red></font>';
    $attr->{migs_name} = 'PROJECT NAME';
    $attr->{hint} = 'analysis name';
    push @{$table->{attrs}}, ( $attr );

    # project_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'goldAnalysisProjectType';
    $attr->{display_name} = 'Analysis Project Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # submission_type varchar2(20),
    $attr = new RelAttr( );
    $attr->{name} = 'submissionType';
    $attr->{display_name} = 'Submission Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 20;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # genome_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'genomeType';
    $attr->{display_name} = 'Genome Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # analysis_product_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'analysisProductName';
    $attr->{display_name} = 'Analysis Product Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'comments';
    $attr->{display_name} = 'Project Description/Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # visibility varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'visibility';
    $attr->{display_name} = 'Visibility';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # reviewStatus
    $attr = new RelAttr( );
    $attr->{name} = 'reviewStatus';
    $attr->{display_name} = 'Review Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{definition} = 'GOLD Review Status';
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # ITS AP ID
    $attr = new RelAttr( );
    $attr->{name} = 'itsSourceAnalysisProjectId';
    $attr->{display_name} = 'ITS Source Analysis Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # ITS AP ID
    $attr = new RelAttr( );
    $attr->{name} = 'itsAnalysisProjectId';
    $attr->{display_name} = 'ITS Analysis Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # ITS annotation AT ID
    $attr = new RelAttr( );
    $attr->{name} = 'itsAnnotationAtId';
    $attr->{display_name} = 'ITS Annotation Task ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # ITS assembly AT ID
    $attr = new RelAttr( );
    $attr->{name} = 'itsAssemblyAtId';
    $attr->{display_name} = 'ITS Assembly Task ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # pi_name varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'piName';
#    $attr->{display_name} = 'PI Name';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Contact Info';
#    $attr->{bg_color} = 'yellow';
#    push @{$table->{attrs}}, ( $attr );

    # pi_email varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'piEmail';
#    $attr->{display_name} = 'PI Email';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Contact Info';
#    $attr->{bg_color} = 'yellow';
#    push @{$table->{attrs}}, ( $attr );


    # pm_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pmName';
    $attr->{display_name} = 'PM Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # pm_email varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pmEmail';
    $attr->{display_name} = 'PM Email';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # calibanId
    $attr = new RelAttr( );
    $attr->{name} = 'calibanId';
    $attr->{display_name} = 'Caliban ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # submitter
    $attr = new RelAttr( );
    $attr->{name} = 'submitterContactOid';
    $attr->{display_name} = 'Submitter';
    $attr->{data_type} = 'int';
    $attr->{length} = 80;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_taxon_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiTaxonId';
    $attr->{display_name} = 'NCBI Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{filter_cond} = 1;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Info';
    $attr->{bg_color} = 'orange';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    push @{$table->{attrs}}, ( $attr );

    # domain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'domain';
    $attr->{display_name} = 'Domain';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_phylum varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiPhylum';
    $attr->{display_name} = 'NCBI Phylum';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_class varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiClass';
    $attr->{display_name} = 'NCBI Class';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_order varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiOrder';
    $attr->{display_name} = 'NCBI Order';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_family varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiFamily';
    $attr->{display_name} = 'NCBI Family';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiGenus';
    $attr->{display_name} = 'NCBI Genus';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiSpecies';
    $attr->{display_name} = 'NCBI Species';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem';
    $attr->{display_name} = 'Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_category varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystemCategory';
    $attr->{display_name} = 'Ecosystem Category';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_category from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_category order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystemType';
    $attr->{display_name} = 'Ecosystem Type';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_type from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_type order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_subtype varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystemSubtype';
    $attr->{display_name} = 'Ecosystem Subtype';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_subtype from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_subtype order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # specific_ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'specificEcosystem';
    $attr->{display_name} = 'Specific Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct specific_ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvspecific_ecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # img_oid int,
    # referenceImgOid
    $attr = new RelAttr( );
    $attr->{name} = 'imgDatasetId';
    $attr->{display_name} = 'IMG Dataset ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Ignore.  This field will become populated after you submit your genome ot IMG'; 
    $attr->{hmp_cond} = 1; 
#    $attr->{header} = 'External Links';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    # imgTaxonOid int,
    $attr = new RelAttr( );
    $attr->{name} = 'imgTaxonOid';
    $attr->{display_name} = 'IMG Object ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Ignore.  This field will become populated after you submit your genome ot IMG'; 
    $attr->{hmp_cond} = 1; 
#    $attr->{header} = 'External Links';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    # referenceImgOid
    $attr = new RelAttr( );
    $attr->{name} = 'referenceImgOid';
    $attr->{display_name} = 'Reference Genome';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Ignore.  This field will become populated after you submit your genome ot IMG'; 
#    $attr->{header} = 'External Links';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    # referenceGoldId
    $attr = new RelAttr( );
    $attr->{name} = 'referenceGoldId';
    $attr->{display_name} = 'Reference GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{definition} = 'reference GOLD Project';
#    $attr->{header} = 'External Links';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    # scaffoldCount
    $attr = new RelAttr( );
    $attr->{name} = 'scaffoldCount';
    $attr->{display_name} = 'Scaffold Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{definition} = 'scaffold count';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    # pipelineVersion
    $attr = new RelAttr( );
    $attr->{name} = 'pipelineVersion';
    $attr->{display_name} = 'Pipeline Version';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{definition} = 'pipeline version';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );


    # isGenePrimp
    $attr = new RelAttr( );
    $attr->{name} = 'isGenePrimp';
    $attr->{display_name} = 'Is Gene Primp?';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{definition} = 'is gene primp?';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    # genePrimpDate
    $attr = new RelAttr( );
    $attr->{name} = 'genePrimpDate';
    $attr->{display_name} = 'Gene Primp Date';
    $attr->{data_type} = 'char';
    $attr->{length} = 60;
    $attr->{is_required} = 0;
    $attr->{definition} = 'gene primp date';
    $attr->{tab} = 'Dataset Info';
    $attr->{bg_color} = 'lightgrey';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_Sequencing_Project - Project_Info
###########################################################################
sub def_Sequencing_Project {
    my $table = new RelTable( );
    $table->{name} = 'project';
    $table->{display_name} = 'Sequencing Project Information';
    $table->{id} = 'project_id';
    $table->{multipart_form} = 0;

    # project_id int not null,
    my $attr = new RelAttr( );
    $attr->{name} = 'projectId';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # project_oid int not null,
    $attr = new RelAttr( );
    $attr->{name} = 'projectOid';
    $attr->{display_name} = 'ER Submission Project OID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # gold_id varchar2(50)
    $attr = new RelAttr( );
    $attr->{name} = 'goldId';
    $attr->{display_name} = 'GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # legacy gold_id varchar2(50) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'legacyGoldId';
    $attr->{display_name} = 'Legacy GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # legacy gold_id_old varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'oldLegacyGoldId';
    $attr->{display_name} = 'GOLD Stamp Old ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # img_oid int,
    $attr = new RelAttr( );
    $attr->{name} = 'imgOid';
    $attr->{display_name} = 'IMG Object ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{definition} = 'Definition -  Ignore.  This field will become populated after you submit your genome ot IMG'; 
    $attr->{hmp_cond} = 1; 
#    $attr->{header} = 'External Links';
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiBioProjectId';
    $attr->{display_name} = 'NCBI BioProject ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Ignore.  Enter the GPID from NCBI.  THIS IS NOT THE TAXONOMY ID'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/sites/entrez?db=genomeprj';
    $attr->{migs_id} = 'MIGS 1.1 ';
    $attr->{migs_name} = 'PID';
    $attr->{hint} = 'This is NOT a NCBI taxonomy id.  Please go to http://www.ncbi.nlm.nih.gov/genomeprj for more info';
    push @{$table->{attrs}}, ( $attr );

    # display_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'displayName';
    $attr->{display_name} = 'Display Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    $attr->{migs_id} = 'MIGS 3 <font color=red></font>';
    $attr->{migs_name} = 'PROJECT NAME';
    $attr->{hint} = 'Genus species strain.  Please remove "subsp." from the name';
    push @{$table->{attrs}}, ( $attr );

    # submitters_project_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'submitterProjectName';
    $attr->{display_name} = 'Submitter Project Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    $attr->{definition} = 'Definition -  Enter your own personal project name.'; 
    $attr->{hint} = 'Required for IMG Submission';
    push @{$table->{attrs}}, ( $attr );

    # project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'projectStatus';
    $attr->{display_name} = 'Project Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # gpts_bioclassification_name
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_bioclassification_name';
    $attr->{display_name} = 'Bioclassification Name';
    $attr->{data_type} = 'int';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # jgi_dir_number int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_dir_number';
    $attr->{display_name} = 'JGI Directory Number';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # jgi_visibility varchar(64)
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_visibility';
    $attr->{display_name} = 'Visibility';
    $attr->{data_type} = 'char';
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # its_spid int,
    $attr = new RelAttr( );
    $attr->{name} = 'itsSpid';
    $attr->{display_name} = 'ITS Sequencing ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # pmo_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmoProjectId';
    $attr->{display_name} = 'PMO Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # itsProposalId int,
    $attr = new RelAttr( );
    $attr->{name} = 'itsProposalId';
    $attr->{display_name} = 'ITS Proposal ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # gptsProposalId int,
    $attr = new RelAttr( );
    $attr->{name} = 'gptsProposalId';
    $attr->{display_name} = 'GPTS Proposal ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # funding_program varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'fundingProgram';
    $attr->{display_name} = 'Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  If this is a JGI or HMP project, enter the corresponding funding program.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from funding_programcv order by cv_term";
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # gc_perc numeric(5,2),
    $attr = new RelAttr( );
    $attr->{name} = 'gcPercent';
    $attr->{display_name} = 'GC Percent';
    $attr->{data_type} = 'number';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # domain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'domain';
    $attr->{display_name} = 'Domain';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_phylum varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiPhylum';
    $attr->{display_name} = 'NCBI Phylum';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_class varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiClass';
    $attr->{display_name} = 'NCBI Class';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_order varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiOrder';
    $attr->{display_name} = 'NCBI Order';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_family varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiFamily';
    $attr->{display_name} = 'NCBI Family';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiGenus';
    $attr->{display_name} = 'NCBI Genus';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiSpecies';
    $attr->{display_name} = 'NCBI Species';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # strain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'strain';
    $attr->{display_name} = 'Strain';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'genus';
    $attr->{display_name} = 'GOLD Genus';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'species';
    $attr->{display_name} = 'GOLD Species';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # viralGroup varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'viralGroup';
    $attr->{display_name} = 'Viral Group';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # viralSubgroup varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'viralSubgroup';
    $attr->{display_name} = 'Viral Subgroup';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem';
    $attr->{display_name} = 'Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_category varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystemCategory';
    $attr->{display_name} = 'Ecosystem Category';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_category from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_category order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystemType';
    $attr->{display_name} = 'Ecosystem Type';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_type from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_type order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_subtype varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystemSubtype';
    $attr->{display_name} = 'Ecosystem Subtype';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_subtype from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_subtype order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # specific_ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'specificEcosystem';
    $attr->{display_name} = 'Specific Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct specific_ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvspecific_ecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # pi_name varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'piName';
#    $attr->{display_name} = 'PI Name';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Contact Info';
#    $attr->{bg_color} = 'yellow';
#    push @{$table->{attrs}}, ( $attr );

    # pi_email varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'piEmail';
#    $attr->{display_name} = 'PI Email';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Contact Info';
#    $attr->{bg_color} = 'yellow';
#    push @{$table->{attrs}}, ( $attr );

    # pm_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pmName';
    $attr->{display_name} = 'PM Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # pm_email varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pmEmail';
    $attr->{display_name} = 'PM Email';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # finishing_goal varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'finishingGoalId';
#    $attr->{display_name} = 'Finishing Goal';
#    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select cv_term from project_goalcv";
#    $attr->{length} = 255;
#    $attr->{is_required} = 0;
#    $attr->{filter_cond} = 1;
#    $attr->{hmp_cond} = 0; 
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

    # isolation varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'isolation';
    $attr->{display_name} = 'Isolation Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 13 <font color=red>  </font>';
    $attr->{migs_name} = 'ISOLATION SOURCE';
    push @{$table->{attrs}}, ( $attr );

    # iso_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'isoCountry';
    $attr->{display_name} = 'Isolation Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 5 <font color=red>  </font>';
    $attr->{migs_name} = 'COUNTRY';
    push @{$table->{attrs}}, ( $attr );

    # geo_location varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'geoLocation';
    $attr->{display_name} = 'Geographic Location';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{definition} = 'Definition -  Geographic location of where the dna was isolated from.'; 
    $attr->{hint} = '(ec Walnut Creek Ca, Amazon rainforest Equador)';
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # latitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'latitude';
    $attr->{display_name} = 'Latitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{migs_id} = 'MIGS 4.1 <font color=red>  </font>';
    $attr->{migs_name} = 'LATITUDE';
    $attr->{hint} = 'Please -  enter ONLY decimal coordinates.'; 
    $attr->{definition} = 'Definition - Latitude is measured from the equator, with positive values going north and negative values going south.'; 
    $attr->{tip} = 'latitude'; 
    push @{$table->{attrs}}, ( $attr );

    # longitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longitude';
    $attr->{display_name} = 'Longitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 4.2 <font color=red>  </font>';
    $attr->{migs_name} = 'LONGITUDE';
    $attr->{tip} = 'longitude'; 
    $attr->{hint} = 'Please - enter ONLY decimal coordinates.'; 
    $attr->{definition} = 'Definition -  Angular distance on the earth surface, measured east or west from the prime meridian at Greenwich, England, to the meridian passing through a position, expressed in degrees (or hours), minutes, and seconds.'; 
    push @{$table->{attrs}}, ( $attr );

    # altitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'altitude';
    $attr->{display_name} = 'Altitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{definition} = 'Definition -  The height of a thing above sea level or above the earth\'s surface.'; 
    $attr->{hint} = 'Please - enter measurement unit like meters, cm etc.'; 
    $attr->{migs_id} = 'MIGS 4.3';
    $attr->{migs_name} = 'ALTITUDE';
    push @{$table->{attrs}}, ( $attr );

    # depth varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'depth';
    $attr->{display_name} = 'Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 4.4';
    $attr->{migs_name} = 'DEPTH';
    push @{$table->{attrs}}, ( $attr );

    # clade varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'clade';
    $attr->{display_name} = 'Clade';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # ecotype varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'ecotype';
    $attr->{display_name} = 'Ecotype';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # longhurst_code varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'longhurstCode';
    $attr->{display_name} = 'Longhurst Code';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # longhurst_description varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longhurstDescription';
    $attr->{display_name} = 'Longhurst Description';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_name varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiBioProjectName';
    $attr->{display_name} = 'NCBI BioProject Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Enter the project name as it has been registered at NCBI\'s bioproject database.'; 
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # type_strain
    $attr = new RelAttr( );
    $attr->{name} = 'typeStrain';
    $attr->{display_name} = 'Type Strain';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Select Yes if this is a type strain.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # bioproject accession
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiBioProjectAccession';
    $attr->{display_name} = 'NCBI BioProject Accession';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # biosample accession
    $attr = new RelAttr( );
    $attr->{name} = 'ncbiBioSampleAccession';
    $attr->{display_name} = 'Biosample Accession';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # cultured?
    $attr = new RelAttr( );
    $attr->{name} = 'cultured';
    $attr->{display_name} = 'Cultured?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Select Yes if this is cultured.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # cultured type
    $attr = new RelAttr( );
    $attr->{name} = 'cultureType';
    $attr->{display_name} = 'Cultured Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvculture_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Cultured Type -  Isolate or Co-Culture';
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # uncultured_type
    $attr = new RelAttr( );
    $attr->{name} = 'unculturedType';
    $attr->{display_name} = 'Uncultured-Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvuncultured_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Uncultured Type -  Single Cell or Population enrichment.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # oxygen_req varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'oxygenReq';
    $attr->{display_name} = 'Oxygen Requirement';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from oxygencv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    $attr->{migs_id} = 'MIGS 22';
    $attr->{migs_name} = 'RELATIONSHIP TO OXYGEN';
    push @{$table->{attrs}}, ( $attr );

    # cell_shape varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cellShape';
    $attr->{display_name} = 'Cell Shape';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cell_shapecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # motility varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'motility';
    $attr->{display_name} = 'Motility';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from motilitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # sporulation varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sporulation';
    $attr->{display_name} = 'Sporulation';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sporulationcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # temp_range varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'tempRange';
    $attr->{display_name} = 'Temperature Range';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from temp_rangecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # salinity varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'salinity';
    $attr->{display_name} = 'Salinity';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from salinitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # pressure varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pressure';
    $attr->{display_name} = 'Pressure';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # ph varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ph';
    $attr->{display_name} = 'pH';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # sample_collect_temp varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sampleCollectionTemperature';
    $attr->{display_name} = 'Sample Collection Temperature';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # chlorophyll_concentration varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'chlorophyllConcentration';
    $attr->{display_name} = 'Chlorophyll Concentration';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # nitrate_concentration varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'nitrateConcentration';
    $attr->{display_name} = 'Nitrate Concentration';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # oxyegn_concentration varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'oxygenConcentration';
    $attr->{display_name} = 'Oxygen Concentration';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # salinity_concentration varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'salinityConcentration';
    $attr->{display_name} = 'Salinity Concentration';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );


    # cell_diameter varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cell_diameter';
    $attr->{display_name} = 'Cell Diameter';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # cell_length varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cell_length';
    $attr->{display_name} = 'Cell Length';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # color varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'color';
    $attr->{display_name} = 'Color';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from colorcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'OrganMeta';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # gram_stain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'gramStain';
    $attr->{display_name} = 'Gram Staining';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Gram+|Gram-";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # biotic_rel varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'bioticRel';
    $attr->{display_name} = 'Biotic Relationships';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Symbiotic|Free living";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{migs_id} = 'MIGS 15';
    $attr->{migs_name} = 'BIOTIC RELATIONSHIP';
    push @{$table->{attrs}}, ( $attr );

    # symbiotic_interaction varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'symbiotic_interaction';
    $attr->{display_name} = 'Symbiotic Physical Interaction';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Exosymbiotic|Endosymbiotic extracellular|Endosymbiotic intracellular";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    push @{$table->{attrs}}, ( $attr );

    # symbiotic_rel varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'symbiotic_rel';
    $attr->{display_name} = 'Symbiotic Relationship';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Parasitic|Mutualistic|Commensal|Syntrophic";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    push @{$table->{attrs}}, ( $attr );

    # symbiont varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'symbiont';
    $attr->{display_name} = 'Symbiont Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    push @{$table->{attrs}}, ( $attr );

    # symbiont_taxon_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'symbiont_taxon_id';
    $attr->{display_name} = 'Symbiont Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    push @{$table->{attrs}}, ( $attr );

    # host_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'hostName';
    $attr->{display_name} = 'Host Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    $attr->{migs_id} = 'MIGS 16';
    $attr->{migs_name} = 'SPECIFIC HOST';
    push @{$table->{attrs}}, ( $attr );

    # host_taxon_id varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_taxon_id';
    $attr->{display_name} = 'Host Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # host_gender varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'hostGender';
    $attr->{display_name} = 'Host Gender';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # host_race varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_race';
    $attr->{display_name} = 'Host Race';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # host_age varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_age';
    $attr->{display_name} = 'Host Age';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # host_health varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_health';
    $attr->{display_name} = 'Host Health';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    $attr->{migs_id} = 'MIGS 18';
    $attr->{migs_name} = 'HEALTH STATUS OF HOST';
    push @{$table->{attrs}}, ( $attr );

    # host_medication varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_medication';
    $attr->{display_name} = 'Host Medication';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

#     # body_sample_site varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_sample_site';
#     $attr->{display_name} = 'Primary Body Sample Site';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_sitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

#     # body_sample_subsite varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_sample_subsite';
#     $attr->{display_name} = 'Body Sample SubSite';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_subsitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );


#     # body_product varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'body_product';
#     $attr->{display_name} = 'Body Product';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_productcv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );


#     # additional_body_sample_site varchar2(255),
#     $attr = new RelAttr( );
#     $attr->{name} = 'additional_body_sample_site';
#     $attr->{display_name} = 'Additional Body Sample Site';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from body_sitecv";
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{tab} = 'HostMeta';
#     push @{$table->{attrs}}, ( $attr );

    # host_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'host_comments';
    $attr->{display_name} = 'Host Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # seq_center_proj_id number,
#    $attr = new RelAttr( );
#    $attr->{name} = 'seq_center_proj_id';
#    $attr->{display_name} = 'Sequencing Center Project ID';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

    # sample_oid number,
#    $attr = new RelAttr( );
#    $attr->{name} = 'sample_oid';
#    $attr->{display_name} = 'Metagenome Sample OID';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

    # seq_status_link varchar2(500),
#    $attr = new RelAttr( );
#    $attr->{name} = 'seq_status_link';
#    $attr->{display_name} = 'Seq Status Link';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 500;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

    # comments varchar2(4000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'comments';
#    $attr->{display_name} = 'Comments';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 4000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

#     # bioproject_relevance varchar2(4000),
#     $attr = new RelAttr( );
#     $attr->{name} = 'bioproject_relevance';
#     $attr->{display_name} = 'NCBI BioProject Relevance';
#     $attr->{data_type} = 'cv';
#     $attr->{length} = 255;
#     $attr->{is_required} = 0;
#     $attr->{cv_query} = "select relevance from bioproject_relevancecv order by relevance";
#     $attr->{tab} = 'Sequencing';
#     push @{$table->{attrs}}, ( $attr );

    # nucleic acid
    $attr = new RelAttr( );
    $attr->{name} = 'nucleic_acid_type';
    $attr->{display_name} = "Nucleic Acid Type";
    $attr->{data_type} = 'list';
    $attr->{list_values} = 'DNA|RNA';
    $attr->{length} = 16;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # library_method varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'library_method';
    $attr->{display_name} = 'Library Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 28.1';
    $attr->{migs_name} = 'LIBRARY SIZE';
    push @{$table->{attrs}}, ( $attr );

    # no_of_reads int
    $attr = new RelAttr( );
    $attr->{name} = 'no_of_reads';
    $attr->{display_name} = 'Number of Reads';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 28.2';
    $attr->{migs_name} = 'NUMBER OF READS';
    push @{$table->{attrs}}, ( $attr );

    # vector varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'vector';
    $attr->{display_name} = 'Vector';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 28.3';
    $attr->{migs_name} = 'VECTOR';
    push @{$table->{attrs}}, ( $attr );

    # binning_method varchar2(1000),
#    $attr = new RelAttr( );
#    $attr->{name} = 'binning_method';
#    $attr->{display_name} = 'Binning Method';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 1000;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # assembly_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'assembly_method';
    $attr->{display_name} = 'Assembly Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 30 <font color=red>  </font>';
    $attr->{migs_name} = 'ASSEMBLY';
    push @{$table->{attrs}}, ( $attr );

    # seq_depth varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_depth';
    $attr->{display_name} = 'Sequencing Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.2';
    $attr->{migs_name} = 'SEQUENCING COVERAGE';
    push @{$table->{attrs}}, ( $attr );

    # gene_calling_method varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'gene_calling_method';
    $attr->{display_name} = 'Gene Calling Method';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # scaffold_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'scaffold_count';
    $attr->{display_name} = 'Scaffold Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.3';
    $attr->{migs_name} = 'CONTIGS';
    push @{$table->{attrs}}, ( $attr );

    # contig_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'contig_count';
    $attr->{display_name} = 'Contig Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{migs_id} = 'MIGS 31.3';
    $attr->{migs_name} = 'CONTIGS';
    push @{$table->{attrs}}, ( $attr );

    # est_size int,
    $attr = new RelAttr( );
    $attr->{name} = 'est_size';
    $attr->{display_name} = 'Estimated Size';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Sequencing';
    $attr->{hmp_cond} = 1; 
    $attr->{migs_id} = 'MIGS 11';
    $attr->{migs_name} = 'ESTIMATED SIZE';
    push @{$table->{attrs}}, ( $attr );

    # units varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'units';
    $attr->{display_name} = 'Units';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from unitcv";
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # genome_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'genome_count';
#    $attr->{display_name} = 'Community Diversity (est. number)';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Project';
#    push @{$table->{attrs}}, ( $attr );

    # gene_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'gene_count';
    $attr->{display_name} = 'Gene Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );
    
    # chromosome_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'chromosome_count';
    $attr->{display_name} = 'Chromosome Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    $attr->{migs_id} = 'MIGS 9';
    $attr->{migs_name} = 'NUMBER OF REPLICONS';
    push @{$table->{attrs}}, ( $attr );

    # plasmid_count int,
    $attr = new RelAttr( );
    $attr->{name} = 'plasmid_count';
    $attr->{display_name} = 'Plasmid Count';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    $attr->{migs_id} = 'MIGS 10';
    $attr->{migs_name} = 'EXTRACHROMOSOMAL ELEMENTS';
    push @{$table->{attrs}}, ( $attr );

# per Nikos, singlet count is for metagenome only
    # singlet_count int,
#    $attr = new RelAttr( );
#    $attr->{name} = 'singlet_count';
#    $attr->{display_name} = 'Singlet Count';
#    $attr->{data_type} = 'int';
#    $attr->{length} = 40;
#    $attr->{is_required} = 0;
#    $attr->{tab} = 'Sequencing';
#    push @{$table->{attrs}}, ( $attr );

    # seq_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_country';
    $attr->{display_name} = 'Sequencing Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Sequencing';
    push @{$table->{attrs}}, ( $attr );

    # completion_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'completion_date';
    $attr->{display_name} = 'Completion Date (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    $attr->{hint} = 'Date that genbank file was submitted.  Not the date NCBI completed submission';
    push @{$table->{attrs}}, ( $attr );

    # pubmed_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmid';
    $attr->{display_name} = 'Pubmed ID';
    $attr->{data_type} = 'int';
    $attr->{filter_cond} = 1;
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # doi_id varchar2(200),
    $attr = new RelAttr( );
    $attr->{name} = 'doi_id';
    $attr->{display_name} = 'DOI ID';
    $attr->{data_type} = 'char';
    $attr->{filter_cond} = 1;
    $attr->{length} = 40;
    $attr->{default_size} = 30;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_journal varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'pub_journal';
    $attr->{display_name} = 'Publication Journal';
    $attr->{data_type} = 'cv';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select cv_term from publication_journalcv";
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_vol varchar2(100),
    $attr = new RelAttr( );
    $attr->{name} = 'pub_vol';
    $attr->{display_name} = 'Publication Vol.';
    $attr->{data_type} = 'char';
    $attr->{length} = 100;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # pub_link varchar2(500)
    $attr = new RelAttr( );
    $attr->{name} = 'pub_link';
    $attr->{display_name} = 'Publication Link';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # refseq_ids varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'refseq_ids';
    $attr->{display_name} = 'Refseq ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # refseq_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'refseq_link_url';
    $attr->{display_name} = 'Refseq link URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # map_link_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'map_link_url';
    $attr->{display_name} = 'NCBI Map link URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # growth_conditions varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'growth_conditions';
    $attr->{display_name} = 'Organism Growth Conditions';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{hint} = '';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # proj_desc varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'proj_desc';
    $attr->{display_name} = 'Project Description';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{hint} = '';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # information_name varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'information_name';
    $attr->{display_name} = 'Information Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # information_url varchar2(500),
    $attr = new RelAttr( );
    $attr->{name} = 'information_url';
    $attr->{display_name} = 'Information URL';
    $attr->{data_type} = 'char';
    $attr->{length} = 500;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Hidden';
    push @{$table->{attrs}}, ( $attr );

    # contact_oid int
    $attr = new RelAttr( );
    $attr->{name} = 'contact_oid';
    $attr->{display_name} = 'IMG Contact';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # add_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'add_date';
    $attr->{display_name} = 'Add Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{header} = 'Database Update Info';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Last Modify Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Last Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # hmp_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'hmpId';
    $attr->{display_name} = 'HMP ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # show_in_dacc
    $attr = new RelAttr( );
    $attr->{name} = 'show_in_dacc';
    $attr->{display_name} = 'Show in DACC?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_project_status';
    $attr->{display_name} = 'HMP Project Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from HMP_project_statuscv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # bei_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'bei_status';
    $attr->{display_name} = 'BEI Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from BEI_statuscv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_submit_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_submit_status';
    $attr->{display_name} = 'NCBI Submission Status';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{cv_query} = "select cv_term from ncbi_submit_statuscv";
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

#     # isolate_selected
#     $attr = new RelAttr( );
#     $attr->{name} = 'isolate_selected';
#     $attr->{display_name} = 'Isolate Selected by Working Group';
#     $attr->{data_type} = 'cv';
#     $attr->{cv_query} = "select cv_term from yesnocv";
#     $attr->{length} = 10;
#     $attr->{is_required} = 0;
#     $attr->{filter_cond} = 1;
#     $attr->{hmp_cond} = 1; 
#     $attr->{can_edit} = 1;
#     $attr->{tab} = 'HMP';
#     push @{$table->{attrs}}, ( $attr );

    # isolate_selected
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_isolate_selection_source';
    $attr->{display_name} = 'Isolate Suggestion Source';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from isolate_suggestion_sourcecv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

     # body_sample_site varchar2(255),
     $attr = new RelAttr( );
     $attr->{name} = 'hmp_isolation_bodysite';
     $attr->{display_name} = 'HMP Isolation Body Sample Site';
     $attr->{data_type} = 'cv';
     $attr->{cv_query} = "select cv_term from body_sitecv";
     $attr->{length} = 255;
     $attr->{is_required} = 0;
     $attr->{filter_cond} = 1;
     $attr->{hmp_cond} = 1; 
     $attr->{tab} = 'HMP';
     push @{$table->{attrs}}, ( $attr );

     # body_sample_subsite varchar2(255),
     $attr = new RelAttr( );
     $attr->{name} = 'hmp_isolation_bodysubsite';
     $attr->{display_name} = 'HMP Isolation Body Sample SubSite';
     $attr->{data_type} = 'cv';
     $attr->{cv_query} = "select cv_term from body_subsitecv";
     $attr->{length} = 255;
     $attr->{is_required} = 0;
     $attr->{filter_cond} = 1;
     $attr->{hmp_cond} = 1; 
     $attr->{tab} = 'HMP';
     push @{$table->{attrs}}, ( $attr );

    # project_goal varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_goal';
    $attr->{display_name} = 'HMP Finishing Goal';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from project_goalcv";
    $attr->{length} = 255;
    $attr->{hmp_cond} = 1; 
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_iso_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_iso_comments';
    $attr->{display_name} = 'Source, Sample & Medical Relevance Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_iso_avail_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_iso_avail_comments';
    $attr->{display_name} = 'Isolate Availability Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_project_comments varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_project_comments';
    $attr->{display_name} = 'Project Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # dna_received
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_dna_received';
    $attr->{display_name} = 'DNA Received';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_date_dna_received date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_date_dna_received';
    $attr->{display_name} = 'Date DNA Received (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_seq_begin_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_seq_begin_date';
    $attr->{display_name} = 'Date Sequencing Begins (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # hmp_draft_seq_completion_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_draft_seq_completion_date';
    $attr->{display_name} = 'Date Draft Sequencing Completed (DD-MON-YY)';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # homd_id varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'homd_id';
    $attr->{display_name} = 'Cross Reference ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # jgi_proposal_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_proposal_id';
    $attr->{display_name} = 'JGI Proposal ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scientific_program varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'scientific_program';
    $attr->{display_name} = 'Scientific Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sci_progcv";
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_final_deliverable varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_final_deliverable';
    $attr->{display_name} = 'ITS Final Deliverable';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvjgi_its_final_deliverable";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_product_name varchar(255),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_product_name';
    $attr->{display_name} = 'JGI Product Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # scope_of_work varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'scope_of_work';
    $attr->{display_name} = 'JGI Scope of Work';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{cv_query} = "select scope from workscopecv order by scope";
#    $attr->{tab} = 'Sequencing Information';
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_project_type varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_project_type';
    $attr->{display_name} = 'JGI Project Type';
    $attr->{data_type} = 'char';
    $attr->{length} = 256;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_status varchar(80),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_status';
    $attr->{display_name} = 'JGI Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_last_mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_last_mod_date';
    $attr->{display_name} = 'GPTS Last Update Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # gpts_comments varchar(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'gpts_comments';
    $attr->{display_name} = 'GPTS Comments';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_program varchar(256),
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_program';
    $attr->{display_name} = 'JGI Funding Program';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from jgi_funding_progcv";
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    # jgi_funding_year int,
    $attr = new RelAttr( );
    $attr->{name} = 'jgi_funding_year';
    $attr->{display_name} = 'JGI Funding Year';
    $attr->{data_type} = 'int';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'JGI';
    push @{$table->{attrs}}, ( $attr );

    ## ProPortal specific
    # proport_ocean varchar2(512),
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalOcean';
    $attr->{display_name} = 'ProPortal: Ocean';
    $attr->{data_type} = 'cv';
    $attr->{length} = 512;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_isolation varchar2(1024),
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalIsolation';
    $attr->{display_name} = 'ProPortal: Isolation';
    $attr->{data_type} = 'cv';
    $attr->{length} = 1024;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_station varchar2(1024),
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalStation';
    $attr->{display_name} = 'ProPortal: Station';
    $attr->{data_type} = 'cv';
    $attr->{length} = 1024;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_woa_temperature float
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalWoaTemperature';
    $attr->{display_name} = 'ProPortal: WOA Temperature';
    $attr->{data_type} = 'float';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_woa_salinity float
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalWoaSalinity';
    $attr->{display_name} = 'ProPortal: WOA Salinity';
    $attr->{data_type} = 'float';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_woa_dissolved_oxygen float
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalWoaDissolvedOxygen';
    $attr->{display_name} = 'ProPortal: WOA Dissolved Oxygen';
    $attr->{data_type} = 'float';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_woa_silicate float
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalWoaSilicate';
    $attr->{display_name} = 'ProPortal: WOA Silicate';
    $attr->{data_type} = 'float';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_woa_phosphate float
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalWoaPhosphate';
    $attr->{display_name} = 'ProPortal: WOA Phosphate';
    $attr->{data_type} = 'float';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );

    # proport_woa_nitrate float
    $attr = new RelAttr( );
    $attr->{name} = 'proPortalWoaNitrate';
    $attr->{display_name} = 'ProPortal: WOA Nitrate';
    $attr->{data_type} = 'float';
    $attr->{length} = 20;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'ProPortal Info';
    $attr->{bg_color} = 'aqua';
    push @{$table->{attrs}}, ( $attr );
    # end Proportal specific

    # seq_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sequencingStatus';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_statuscv";
    $attr->{default_value} = 'In progress';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing Info';
    $attr->{bg_color} = 'lightpink';
    $attr->{migs_id} = 'MIGS 31.1';
    $attr->{migs_name} = 'SEQUENCING STATUS';
    push @{$table->{attrs}}, ( $attr );

    # sequencing_strategy varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sequencingStrategy';
    $attr->{display_name} = 'Sequencing Strategy';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 0;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing Info';
    $attr->{bg_color} = 'lightpink';
    push @{$table->{attrs}}, ( $attr );

    # seq_quality varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sequencingQualityId';
    $attr->{display_name} = 'Sequencing Quality';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_qualitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing Info';
    $attr->{bg_color} = 'lightpink';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}


###########################################################################
# def_ImgGold_Sequencing_Project - Project_Info
###########################################################################
sub def_ImgGold_Sequencing_Project {
    my $table = new RelTable( );
    $table->{name} = 'gold_sequencing_project';
    $table->{display_name} = 'Sequencing Project Information';
    $table->{id} = 'gold_id';
    $table->{multipart_form} = 0;

    # gold_id varchar2(50)
    my $attr = new RelAttr( );
    $attr->{name} = 'gold_id';
    $attr->{display_name} = 'GOLD ID';
    $attr->{data_type} = 'char';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # contact_oid int
    $attr = new RelAttr( );
    $attr->{name} = 'contact_oid';
    $attr->{display_name} = 'Contact';
    $attr->{data_type} = 'int';
    $attr->{length} = 50;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # project_id int not null,
    $attr = new RelAttr( );
    $attr->{name} = 'project_id';
    $attr->{display_name} = 'Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_project_id';
    $attr->{display_name} = 'NCBI Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Ignore.  Enter the GPID from NCBI.  THIS IS NOT THE TAXONOMY ID'; 
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/sites/entrez?db=genomeprj';
    $attr->{migs_id} = 'MIGS 1.1 ';
    $attr->{migs_name} = 'PID';
    $attr->{hint} = 'This is NOT a NCBI taxonomy id.  Please go to http://www.ncbi.nlm.nih.gov/genomeprj for more info';
    push @{$table->{attrs}}, ( $attr );

    # display_name varchar2(1000) not null,
    $attr = new RelAttr( );
    $attr->{name} = 'display_name';
    $attr->{display_name} = 'Display Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project Info';
    $attr->{bg_color} = 'lightblue';
    $attr->{migs_id} = 'MIGS 3 <font color=red></font>';
    $attr->{migs_name} = 'PROJECT NAME';
    $attr->{hint} = 'Genus species strain.  Please remove "subsp." from the name';
    push @{$table->{attrs}}, ( $attr );

    # project_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'project_status';
    $attr->{display_name} = 'Project Status';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # its_spid int,
    $attr = new RelAttr( );
    $attr->{name} = 'its_spid';
    $attr->{display_name} = 'ITS Sequencing ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # pmo_project_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'pmo_project_id';
    $attr->{display_name} = 'PMO Project ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Project Info';
    push @{$table->{attrs}}, ( $attr );

    # domain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'domain';
    $attr->{display_name} = 'Domain';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_kingdom varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_kingdom';
    $attr->{display_name} = 'NCBI Kingdom';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_phylum varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_phylum';
    $attr->{display_name} = 'NCBI Phylum';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_class varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_class';
    $attr->{display_name} = 'NCBI Class';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_order varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_order';
    $attr->{display_name} = 'NCBI Order';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_family varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_family';
    $attr->{display_name} = 'NCBI Family';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_genus varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_genus';
    $attr->{display_name} = 'NCBI Genus';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ncbi_species varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ncbi_species';
    $attr->{display_name} = 'NCBI Species';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # strain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'strain';
    $attr->{display_name} = 'Strain';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # clade varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'clade';
    $attr->{display_name} = 'Clade';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{font_color} = 'darkgreen';
    $attr->{bg_color} = 'orange';
    $attr->{tab} = 'Organism Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem';
    $attr->{display_name} = 'Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_category varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_category';
    $attr->{display_name} = 'Ecosystem Category';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_category from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_category order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_type varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_type';
    $attr->{display_name} = 'Ecosystem Type';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_type from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_type order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # ecosystem_subtype varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'ecosystem_subtype';
    $attr->{display_name} = 'Ecosystem Subtype';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct ecosystem_subtype from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvecosystem_subtype order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # specific_ecosystem varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'specific_ecosystem';
    $attr->{display_name} = 'Specific Ecosystem';
    $attr->{data_type} = 'cv';
#    $attr->{cv_query} = "select distinct specific_ecosystem from metagenomic_class_nodes";
    $attr->{cv_query} = "select cv_term from cvspecific_ecosystem order by 1";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{hmp_cond} = 1; 
    $attr->{filter_cond} = 1;
    $attr->{bg_color} = 'lightgreen';
    $attr->{hint} = 'Required for IMG Submission';
    $attr->{tab} = 'Ecosystem Info';
    push @{$table->{attrs}}, ( $attr );

    # pi_name varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'pi_name';
#    $attr->{display_name} = 'PI Name';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Contact Info';
#    $attr->{bg_color} = 'yellow';
#    push @{$table->{attrs}}, ( $attr );

    # pi_email varchar2(255),
#    $attr = new RelAttr( );
#    $attr->{name} = 'pi_email';
#    $attr->{display_name} = 'PI Email';
#    $attr->{data_type} = 'char';
#    $attr->{length} = 255;
#    $attr->{is_required} = 1;
#    $attr->{filter_cond} = 1;
#    $attr->{tab} = 'Contact Info';
#    $attr->{bg_color} = 'yellow';
#    push @{$table->{attrs}}, ( $attr );

    # pm_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pm_name';
    $attr->{display_name} = 'PM Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # pm_email varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'pm_email';
    $attr->{display_name} = 'PM Email';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Contact Info';
    $attr->{bg_color} = 'yellow';
    push @{$table->{attrs}}, ( $attr );

    # date_collected varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'date_collected';
    $attr->{display_name} = 'Date Collected';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 13 <font color=red>  </font>';
    $attr->{migs_name} = 'ISOLATION SOURCE';
    push @{$table->{attrs}}, ( $attr );

    # isolation varchar2(4000),
    $attr = new RelAttr( );
    $attr->{name} = 'isolation';
    $attr->{display_name} = 'Isolation Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 4000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 13 <font color=red>  </font>';
    $attr->{migs_name} = 'ISOLATION SOURCE';
    push @{$table->{attrs}}, ( $attr );

    # iso_country varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'iso_country';
    $attr->{display_name} = 'Isolation Country';
    $attr->{data_type} = 'cv';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{can_edit} = 1;
    $attr->{cv_query} = "select cv_term from countrycv";
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 5 <font color=red>  </font>';
    $attr->{migs_name} = 'COUNTRY';
    push @{$table->{attrs}}, ( $attr );

    # geo_location varchar2(1000),
    $attr = new RelAttr( );
    $attr->{name} = 'geo_location';
    $attr->{display_name} = 'Geographic Location';
    $attr->{data_type} = 'char';
    $attr->{length} = 1000;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{definition} = 'Definition -  Geographic location of where the dna was isolated from.'; 
    $attr->{hint} = '(ec Walnut Creek Ca, Amazon rainforest Equador)';
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # latitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'latitude';
    $attr->{display_name} = 'Latitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{migs_id} = 'MIGS 4.1 <font color=red>  </font>';
    $attr->{migs_name} = 'LATITUDE';
    $attr->{hint} = 'Please -  enter ONLY decimal coordinates.'; 
    $attr->{definition} = 'Definition - Latitude is measured from the equator, with positive values going north and negative values going south.'; 
    $attr->{tip} = 'latitude'; 
    push @{$table->{attrs}}, ( $attr );

    # longitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longitude';
    $attr->{display_name} = 'Longitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{migs_id} = 'MIGS 4.2 <font color=red>  </font>';
    $attr->{migs_name} = 'LONGITUDE';
    $attr->{tip} = 'longitude'; 
    $attr->{hint} = 'Please - enter ONLY decimal coordinates.'; 
    $attr->{definition} = 'Definition -  Angular distance on the earth surface, measured east or west from the prime meridian at Greenwich, England, to the meridian passing through a position, expressed in degrees (or hours), minutes, and seconds.'; 
    push @{$table->{attrs}}, ( $attr );

    # altitude varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'altitude';
    $attr->{display_name} = 'Altitude';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{definition} = 'Definition -  The height of a sample above sea level or above the earth\'s surface.'; 
    $attr->{hint} = 'Please - enter measurement unit like meters, cm etc.'; 
    $attr->{migs_id} = 'MIGS 4.3';
    $attr->{migs_name} = 'ALTITUDE';
    push @{$table->{attrs}}, ( $attr );

    # depth varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'depth';
    $attr->{display_name} = 'Depth';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    $attr->{definition} = 'Definition -  The depth of a sample below sea level or below the earth\'s surface.'; 
    $attr->{hint} = 'Please - enter measurement unit like meters, m etc.'; 
    $attr->{migs_id} = 'MIGS 4.4';
    $attr->{migs_name} = 'DEPTH';
    push @{$table->{attrs}}, ( $attr );

    # clade varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'clade';
    $attr->{display_name} = 'Clade';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # ecotype varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'ecotype';
    $attr->{display_name} = 'Ecotype';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # longhurst_code varchar2(80),
    $attr = new RelAttr( );
    $attr->{name} = 'longhurst_code';
    $attr->{display_name} = 'Longhurst Code';
    $attr->{data_type} = 'char';
    $attr->{length} = 80;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # longhurst_description varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'longhurst_description';
    $attr->{display_name} = 'Longhurst Description';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Environmental Metadata';
    $attr->{bg_color} = 'gold';
    push @{$table->{attrs}}, ( $attr );

    # type_strain
    $attr = new RelAttr( );
    $attr->{name} = 'type_strain';
    $attr->{display_name} = 'Type Strain';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Definition -  Select Yes if this is a type strain.'; 
    $attr->{hmp_cond} = 1; 
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # bioproject accession
    $attr = new RelAttr( );
    $attr->{name} = 'bioproject_accession';
    $attr->{display_name} = 'Bioproject Accession';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # biosample accession
    $attr = new RelAttr( );
    $attr->{name} = 'biosample_accession';
    $attr->{display_name} = 'Biosample Accession';
    $attr->{data_type} = 'char';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # cultured?
    $attr = new RelAttr( );
    $attr->{name} = 'cultured';
    $attr->{display_name} = 'Cultured?';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from yesnocv";
    $attr->{length} = 10;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Select Yes if this is cultured.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # cultured type
    $attr = new RelAttr( );
    $attr->{name} = 'culture_type';
    $attr->{display_name} = 'Cultured-Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvculture_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Cultured Type -  Isolate or Co-Culture';
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # uncultured_type
    $attr = new RelAttr( );
    $attr->{name} = 'uncultured_type';
    $attr->{display_name} = 'Uncultured-Type';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cvuncultured_type";
    $attr->{length} = 64;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{definition} = 'Uncultured Type -  Single Cell or Population enrichment.'; 
    $attr->{hmp_cond} = 0;
    $attr->{can_edit} = 1;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # oxygen_req varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'oxygen_req';
    $attr->{display_name} = 'Oxygen Requirement';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from oxygencv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    $attr->{migs_id} = 'MIGS 22';
    $attr->{migs_name} = 'RELATIONSHIP TO OXYGEN';
    push @{$table->{attrs}}, ( $attr );

    # cell_shape varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'cell_shape';
    $attr->{display_name} = 'Cell Shape';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from cell_shapecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # motility varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'motility';
    $attr->{display_name} = 'Motility';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from motilitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # sporulation varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sporulation';
    $attr->{display_name} = 'Sporulation';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from sporulationcv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # temp_range varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'temp_range';
    $attr->{display_name} = 'Temperature Range';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from temp_rangecv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # salinity varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'salinity';
    $attr->{display_name} = 'Salinity';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from salinitycv";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # gram_stain varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'gram_stain';
    $attr->{display_name} = 'Gram Staining';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Gram+|Gram-";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{bg_color} = 'mediumpurple';
    push @{$table->{attrs}}, ( $attr );

    # biotic_rel varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'biotic_rel';
    $attr->{display_name} = 'Biotic Relationships';
    $attr->{data_type} = 'list';
    $attr->{list_values} = "Symbiotic|Free living";
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Organism Metadata';
    $attr->{migs_id} = 'MIGS 15';
    $attr->{migs_name} = 'BIOTIC RELATIONSHIP';
    push @{$table->{attrs}}, ( $attr );

    # host_name varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_name';
    $attr->{display_name} = 'Host Name';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    $attr->{migs_id} = 'MIGS 16';
    $attr->{migs_name} = 'SPECIFIC HOST';
    push @{$table->{attrs}}, ( $attr );

    # host_ncbi_taxid varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_ncbi_taxid';
    $attr->{display_name} = 'Host Taxon ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{url} = 'http://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/';
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # host_gender varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'host_gender';
    $attr->{display_name} = 'Host Gender';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # sample_body_site varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_body_site';
    $attr->{display_name} = 'Sample Body Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # sample_body_subsite varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'sample_body_subsite';
    $attr->{display_name} = 'Sample Body Site';
    $attr->{data_type} = 'char';
    $attr->{length} = 255;
    $attr->{is_required} = 0;
    $attr->{tab} = 'Host Metadata';
    $attr->{bg_color} = 'tan';
    push @{$table->{attrs}}, ( $attr );

    # add_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'add_date';
    $attr->{display_name} = 'Add Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{header} = 'Database Update Info';
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # mod_date date,
    $attr = new RelAttr( );
    $attr->{name} = 'mod_date';
    $attr->{display_name} = 'Last Modify Date';
    $attr->{data_type} = 'date';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # modified_by int
    $attr = new RelAttr( );
    $attr->{name} = 'modified_by';
    $attr->{display_name} = 'Last Modified By';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 0;
    $attr->{tab} = 'Project';
    push @{$table->{attrs}}, ( $attr );

    # hmp_id int,
    $attr = new RelAttr( );
    $attr->{name} = 'hmp_id';
    $attr->{display_name} = 'HMP ID';
    $attr->{data_type} = 'int';
    $attr->{length} = 40;
    $attr->{is_required} = 0;
    $attr->{can_edit} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 1; 
    $attr->{tab} = 'HMP';
    push @{$table->{attrs}}, ( $attr );

    # seq_status varchar2(255),
    $attr = new RelAttr( );
    $attr->{name} = 'seq_status';
    $attr->{display_name} = 'Sequencing Status';
    $attr->{data_type} = 'cv';
    $attr->{cv_query} = "select cv_term from seq_statuscv";
    $attr->{default_value} = 'In progress';
    $attr->{length} = 255;
    $attr->{is_required} = 1;
    $attr->{filter_cond} = 1;
    $attr->{hmp_cond} = 0; 
    $attr->{tab} = 'Sequencing Info';
    $attr->{bg_color} = 'lightpink';
    $attr->{migs_id} = 'MIGS 31.1';
    $attr->{migs_name} = 'SEQUENCING STATUS';
    push @{$table->{attrs}}, ( $attr );

    return $table;
}



1;

