#
# Save selection until save button is selected
#
# Issue what if the user goes away from the page
#
# Added export routine +BSJ 02/22/10
#
# $Id: Selection.pm,v 1.7 2010-08-10 06:07:47 bjacob Exp $
#

package Selection;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw( array_multisort array_filter);
use strict;
use Time::localtime; 
use CGI qw( :standard unescape );
use CGI::Session;
use CGI::Cookie;
use WebFunctions;
use WebEnv;
use Data::Dumper;
use JSON;
use Fcntl qw( :flock );


my $env                 = getEnv();
my $main_cgi            = $env->{main_cgi};
my $base_url            = $env->{base_url};
my $base_dir            = $env->{base_dir};
my $tmp_dir             = $env->{tmp_dir};
my $tmp_url             = $env->{tmp_url};
my $cgi_tmp_dir         = $env->{cgi_tmp_dir};

$| = 1;

sub dispatch {
    my $page = param("page");

    if($page eq "getCheckboxes") {
	print header( -type => "application/json" );
	#print header( -type => "text/plain" );    # uncomment for debugging
        getCheckboxes();
    } elsif($page eq "toggleCheckboxes") {
	print header( -type => "application/json" );
	#print header( -type => "text/plain" );    # uncomment for debugging
        toggleCheckboxes();
    } elsif($page eq "export") {
	printExcelHeader(param('table') . "$$" . "_" . lc(getSysDate())  . ".xls");
        export();
	exit 0;
    }
}

#
# Read checkbox elements from previously created tmpfile
#

sub getCheckboxes {
    my $tmpfile = param("tmpfile");
    my $chkState = param("chk"); # 1=checked; 0=unchecked
    my $init = param("init");    # 1=called by oIMGTable instantiation
    my $chkList = param("cl");   # list of dirty checkboxes
    my $filter = unescape(param('f')); # get URL unescaped param
    my $column = param('c');
    my $arrayStr = file2Str("$cgi_tmp_dir/$tmpfile");
    my $highLight = 0;

    $arrayStr = each %{{$arrayStr,0}};   #untaint the string
    my @fullArray = eval($arrayStr);
    my @checkBoxArray = @fullArray;
    @checkBoxArray = array_filter($column, $filter, $highLight, @fullArray) if $filter;

    my @arSelect = ( );

    if ($chkList) {
	my $json = new JSON;
	my $dirtyChks = $json->decode($chkList);
	return if (!$dirtyChks);
	my $col;

	for my $c ( keys %{$checkBoxArray[0]} ) {
	    $col = $c;
	    last if ($c =~ /(^Select$|^Selection$)/i);
	}

	for my $rowIndex ( keys %$dirtyChks ) {
	    my $isChecked = $dirtyChks->{$rowIndex};
	    my $chk = $checkBoxArray[$rowIndex - 1]->{$col};
	    updateCheckBox ($isChecked, $chk); # $chk gets updated
	    $checkBoxArray[$rowIndex - 1]->{$col} = $chk;
	    $fullArray[$rowIndex - 1]->{$col} = $chk;
	    #print $chk . "\n"; # for debugging
	}
    } else {
	#Read all hashes from the array in the tmpfile
	my $i = 1; # starting index 1 to coincide with _img_yuirow_id indexes
	for my $prt_array (@checkBoxArray) {
	    for my $col ( keys %$prt_array ) {
		if ($col =~ /(^Select$|^Selection$)/i) {
		    my $chk = $prt_array->{$col};

		    # do not update checkboxes when called by initial load
		    updateCheckBox ($chkState, $chk) if !$init;
		    if ($filter) {
			$arSelect[$i] = $chk;

		    } else {
			$arSelect[$prt_array->{'_img_yuirow_id'}] = $chk;
		    }
		    $fullArray[$prt_array->{'_img_yuirow_id'} - 1]->{$col} =
			$chk;
		    $i++;
		}
	    }
	}

	# Send a JSON array back to the browser
	my $json = new JSON;
	#$json->pretty;            # display JSON array aesthetically - uncomment for debugging JSON
	print ($json->encode(\@arSelect));
    }

    if (!$init) {  # don't update temp file if called by IMGTable (in selection.js) instantiation
	$Data::Dumper::Terse = 1;          # don't output variable names where feasible
	$Data::Dumper::Indent = 1;         # indent minimal
	my $sOutStr = Dumper( \@fullArray );
	$sOutStr =~ s/\[\n|\]\n//g;        #remove the lines with [ and ] to mimic original file
	str2File ($sOutStr, "$cgi_tmp_dir/$tmpfile"); #Overwrite the same tempfile with new checkbox values
    }
}

#
# Update markup for a checkbox based on whether it is checked or not
#

sub updateCheckBox {
    my ($chkState, $chk) = @_;

    # Insert the 'checked' attribute to the INPUT tag if called by Select All
    if ($chkState) {
	$chk =~ (s/input/input checked=\'checked\'/i) if $chk !~ /checked/;
    } else { # remove the phrase checked='checked' and variations
	$chk =~ s/\s*checked\s*=\s*'\s*checked\s*'//g;
	$chk =~ s/\s*checked//g; # just in case no match was found above
    }
    $_[1] = $chk;
}

#
# Export current table to a tab delimited file,
# and print a URL of this file to the browser
#

sub export {
    my $tmpfile = param("tmpfile");
    my $chkRows = param('rows');
    my $colHeads = param('columns');
    my ($sortCol, $sortDir) = split(/\|/, param('sort'));
    my $filter = unescape(param('f')); # get URL unescaped param
    my $column = param('c');
    my $arrayStr = file2Str("$cgi_tmp_dir/$tmpfile");  #read in the file
    my $highLight = 0;

    $arrayStr = each %{{$arrayStr,0}};  #untaint the string
    my @checkBoxArray = eval($arrayStr);
    #filter array if necessary with current filter terms
    @checkBoxArray = array_filter($column, $filter, $highLight, @checkBoxArray) if $filter;

    my $json = new JSON;
    my @arColHeads = @{$json->decode($colHeads)};
    my @tabRecs;

    my $tabStr = "";

    #Add column headers into output array
    for my $sColTitle (@arColHeads) {
	if ($sColTitle->{'label'} !~ /(^Select$|^Selection$)/i) {
	    $sColTitle->{'label'} =~ s/(<br>|<br\/>)/ /ig;;
	    $sColTitle->{'label'} =~ s/<[^>]*>//gs;
	    $tabStr .= $sColTitle->{'label'} . "\t";
	}
    }
    push(@tabRecs, $tabStr) ;


    # Special condition to print all records from tmpfile
    if ($chkRows eq "all") {
	# Sort rows according to current sort field and direction
	@checkBoxArray = array_multisort($sortCol, $sortDir, @checkBoxArray);

	for (my $i=0; $i < @checkBoxArray; $i++) {
	    $tabStr = "";
	    for my $sColTitle (@arColHeads) {
		if ($sColTitle->{'label'} !~ /(^Select$|^Selection$)/i) {
		    my $cellValue = $checkBoxArray[$i]->{$sColTitle->{'key'}};
		    $cellValue = (strTrim($cellValue) eq "&nbsp;") ? "" : $cellValue;
		    $tabStr .= $cellValue . "\t";
		}
	    }
	    push(@tabRecs, $tabStr);
	}
     } else {  # otherwise get the rows specified by the rows parameter
       my @arChkRows = @{$json->decode($chkRows)};

       # Populate an array of hashes with the selected rows
       my @selArray;
       for (my $i=0, my $j=0; $i < @arChkRows; $i++) {
	   if (defined ($arChkRows[$i]) && ($arChkRows[$i] =~ /checked/)) {
	      for my $sColTitle (@arColHeads) {
		  if ($sColTitle->{'label'} !~ /(^Select$|^Selection$)/i) {
		      $selArray[$j]{$sColTitle->{'key'}} = $checkBoxArray[($i-1)]->{$sColTitle->{'key'}};
		  }
	       }
	       $j++;
    	    }
	}
	# Sort rows according to current sort field and direction
	@selArray = array_multisort($sortCol, $sortDir, @selArray);

	# Append rows to array for printing
	for my $sCols (@selArray) {
	    $tabStr = "";
	    for my $sColTitle (@arColHeads) {
		if ($sColTitle->{'label'} !~ /(^Select$|^Selection$)/i) {
		    my $cellValue = $sCols->{$sColTitle->{'key'}};
		    $cellValue = (strTrim($cellValue) eq "&nbsp;") ? "" : $cellValue;
		    $tabStr .= $cellValue . "\t";
		}
	    }
	    push(@tabRecs, $tabStr);
	}
     }

     #Print each line from array back to browser
     foreach my $line (@tabRecs) {
        print $line . "\n";
    }
}

###############################################################
# array_multisort
# Return array after sorting; automatically distinguishes
# numbers and strings in incoming data and sorts accordingly
#
# **** Also used by json_proxy.pl *****
#
# -BSJ 11/13/09
###############################################################

sub array_multisort ($$@)
{
  my ($sortByCol, $sort_dir, @array) = @_;
  my @sorted = ( );
  my $numericCol = 1;    # assume column is numeric unless proven otherwise

  for (@array) {
      if (!isNumber($_->{$sortByCol})) {
	  if (strTrim($_->{$sortByCol}) eq "&nbsp;" ||
	     (strTrim($_->{$sortByCol}) eq "" )) {
	      $_->{$sortByCol} = 0; #set blanks to be zero
	      next;
	  }
	  $numericCol = 0;      # no longer a numeric column
	  last;
      }
  }

  # if there is even one non-numeric value in this column, do a case-insensitive text sort
  if ($sort_dir eq "desc") {
     @sorted = $numericCol ? sort {    $$b{$sortByCol }  <=>    $$a{$sortByCol}  } @array :
	                     sort { "\L$$b{$sortByCol }" cmp "\L$$a{$sortByCol}" } @array ;
  } else {
     @sorted = $numericCol ? sort {    $$a{$sortByCol }  <=>    $$b{$sortByCol}  } @array :
	                     sort { "\L$$a{$sortByCol }" cmp "\L$$b{$sortByCol}" } @array ;
  }
  return @sorted;
}

###############################################################
# array_search
# Return array after filtering; filterByCol is the current sort
# column
# Highlight the match text in bold green
#
# **** Also used by json_proxy.pl *****
#
# -BSJ 06/11/10
###############################################################

sub array_filter ($$@$) {
    my ($filterByCol, $filterTerm, $highLight, @array) = @_;
    my @filtered;
    my @colNames;

    if ($filterByCol eq 'all') {
	@colNames = keys %{$array[0]};
    } else {
	push @colNames, $filterByCol;
    }

    for (@array) {
	my $rowFiltered = 0;
	for my $col (@colNames) {
	    # Do not process column; next line checks for this
	    next if ($col =~ /Disp/);
	    # Check whether the filter column has a display value
	    my $filteredDisp = ($array[0]->{$col . "Disp"}) ? $col . "Disp" : $col;
	    next if ($filteredDisp eq "_img_yuirow_id"); # do not process column
	    next if ($filteredDisp =~ /(^Select$|^Selection$)/i); # do not process column

	    my $nonHTMLStr = $_->{$filteredDisp};
	    # Remove HTML and extract text
	    $nonHTMLStr =~ s/(<[^>]*>)//gi;
	    my @matches = $nonHTMLStr =~ /(\Q$filterTerm\E)/gi;

	    if (@matches > 0) {
		if ($highLight) {
		    for my $match (@matches) {
			#search and replace only text not within HTML tags
			$_->{$filteredDisp} =~
			    s/(\Q$match\E(?![^<>]*>))/<span style='color:green;font-weight:bold'>$match<\/span>/g;
		    }
		}
		$rowFiltered = 1;
	    }
       }
       push @filtered, $_ if $rowFiltered;
   }
   return @filtered;
}

#
# Toggle the selection of checkboxes
#

sub toggleCheckboxes {
    my $tmpfile = param("tmpfile");
    my $arrayStr = file2Str("$cgi_tmp_dir/$tmpfile");
    my $chkState;

    $arrayStr = each %{{$arrayStr,0}};   #untaint the string
    my @fullArray = eval($arrayStr);

    my @arSelect = ( );

	#Read all hashes from the array in the tmpfile
	my $i = 1; # starting index 1 to coincide with _img_yuirow_id indexes
	for my $prt_array (@fullArray) {
	    for my $col ( keys %$prt_array ) {
		if ($col =~ /(^Select$|^Selection$)/i) {
		    my $chk = $prt_array->{$col};
		    #if ($chk =~ /(\s*checked\s*=\s*'\s*checked\s*')|(\s*checked)/i) {
		    if ($chk =~ /\s+checked/gi) {
			$chkState = 0;
		    } else {
			$chkState = 1;
		    }
		    # do not update checkboxes when called by initial load
		    updateCheckBox ($chkState, $chk);
		    $arSelect[$prt_array->{'_img_yuirow_id'}] = $chk;
		    $fullArray[$prt_array->{'_img_yuirow_id'} - 1]->{$col} =
			$chk;
		    $i++;
		}
	    }
	}
	# Send a JSON array back to the browser
	my $json = new JSON;
	#$json->pretty;            # display JSON array aesthetically - uncomment for debugging JSON
	print ($json->encode(\@arSelect));

	$Data::Dumper::Terse = 1;          # don't output variable names where feasible
	$Data::Dumper::Indent = 1;         # indent minimal
	my $sOutStr = Dumper( \@fullArray );
	$sOutStr =~ s/\[\n|\]\n//g;        #remove the lines with [ and ] to mimic original file
	str2File ($sOutStr, "$cgi_tmp_dir/$tmpfile"); #Overwrite the same tempfile with new checkbox values
}

sub getSysDate { 
    my @months = ( 
                   "JAN", "FEB", "MAR", "APR", "MAY", "JUN", 
                   "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" 
	); 
    my $month = $months[ localtime->mon() ]; 
    my $year  = localtime->year() + 1900; 
    my $day   = localtime->mday(); 
    return sprintf( "%02d-%s-%04d", $day, $month, $year ); 
} 

sub printExcelHeader { 
    my ($fileName) = @_; 
    print "Content-type: application/vnd.ms-excel\n"; 
    print "Content-Disposition: inline;filename=$fileName\n"; 
    print "\n"; 
} 



1;

