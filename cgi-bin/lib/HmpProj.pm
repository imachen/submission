package HmpProj;

use strict; 
use warnings; 
use CGI qw(:standard); 
use Digest::MD5 qw( md5_base64); 
use CGI::Carp 'fatalsToBrowser'; 
use lib 'lib'; 
use WebFunctions; 
use RelSchema;
use ProjectInfo;
use TabHTML;

use POSIX;


my $default_max_row = 50;


#########################################################################
# dispatch 
#########################################################################
sub dispatch { 
    my ($page) = @_; 
 
    if ( $page eq 'newProject' ) {
        NewGoldProject();
    } 
    elsif ( $page eq 'dbNewGoldProject' ) {
        my $msg = ProjectInfo::dbNewProjectInfo(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'updateProject' ) {
        my $project_oid = param1('project_oid');
 
        if ( blankStr($project_oid) ) {
            printError("No project has been selected."); 
            print endform(); 
            return; 
        } 
 
        UpdateGoldProject($project_oid); 
    } 
    elsif ( $page eq 'dbUpdateGoldProject' ) { 
        my $msg = ProjectInfo::dbUpdateProject(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
	}
    } 
    elsif ( $page eq 'deleteProject' ) { 
        DeleteGoldProject(); 
    } 
    elsif ( $page eq 'dbDeleteGoldProject' ) {
        my $msg = ProjectInfo::dbDeleteProject(); 
 
        if ( ! blankStr($msg) ) {
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    } 
    elsif ( $page eq 'copyProject' ) {
        my $project_oid = param1('project_oid');
        CopyGoldProject($project_oid); 
    } 
    elsif ( $page eq 'showPage' ) {
	ShowPage();
    }
    elsif ( $page eq 'displayProject' ) {
        my $project_oid = param1('project_oid');
        ProjectInfo::DisplayProject($project_oid); 
    }
    elsif ( $page eq 'printableData' ) {
        my $project_oid = param1('project_oid');
        if ( $project_oid ) {
            ProjectInfo::ShowPrintableData($project_oid); 
        } 
    }
    elsif ( $page eq 'hmpPrintableData' ) {
        my $project_oid = param1('project_oid');
        if ( $project_oid ) { 
            ProjectInfo::ShowHmpPrintableData($project_oid);
        }
    } 
#    elsif ( $page eq 'displaySample' ) {
#        my $sample_oid = param1('sample_oid');
#        EnvSample::DisplaySample($sample_oid);
#    }
    elsif ( $page eq 'showCategory' ) {
	my $cat_code = '';
	if ( defined param1('cat_code') ) {
	    $cat_code = param1('cat_code');
	}

	ShowNextPage($cat_code);
    }
    elsif ( $page eq 'searchId' ) {
	SearchByID();
    }
    elsif ( $page eq 'searchIdResult' ) {
	ShowSearchIdResultPage();
    }
    elsif ( $page eq 'advSearch' ) {
	AdvancedSearch();
    }
    elsif ( $page eq 'advSearchResult' ) {
	ShowAdvSearchResultPage();
    }
    elsif ( $page eq 'filterProject' ) {
	FilterHmpProject();
    }
    elsif ( $page eq 'applyHmpFilter' ) {
        ApplyHmpFilter(); 

	ShowPage();
    } 
    elsif ( $page eq 'changeContact' ) {
        my $msg = ProjectInfo::dbChangeProjectContact();
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg);
          } 
        else {
            ShowNextPage();
        }
    }
    elsif ( $page eq 'grantEdit' ) {
	ProjectInfo::GrantEditPrivilege();
      } 
    elsif ( $page eq 'dbGrantEditPrivilege' ) {
        my $msg = ProjectInfo::dbGrantEditPrivilege(); 
 
        if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    } 
    elsif ( $page eq 'setGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        ProjectInfo::SetGoldStampId($project_oid); 
    } 
    elsif ( $page eq 'dbSetGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        my $gold_type = param1('gold_type'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = ProjectInfo::dbSetGoldStampId($project_oid, $gold_type,
						$gold_stamp_id); 
 
        if ( ! blankStr($msg) ) { 
	    WebFunctions::showErrorPage($msg); 
          } 
        else { 
	    ShowNextPage();
        } 
    } 
    elsif ( $page eq 'delGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        ProjectInfo::DeleteGoldStampId($project_oid); 
    } 
    elsif ( $page eq 'dbDelGoldStampId' ) { 
        my $project_oid = param1('project_oid'); 
        my $del_gold_id_option = param1('del_gold_id_option'); 
        my $gold_stamp_id = param1('gold_stamp_id'); 
        my $msg = ProjectInfo::dbDelGoldStampId($project_oid, $del_gold_id_option, 
						$gold_stamp_id);
 
        if ( ! blankStr($msg) ) {
	    WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        }
    } 
    elsif ( $page eq 'mergeGoldProj' ) {
	PrintMergeGoldProjPage();
    }
    elsif ( $page eq 'mergeProjects' ) { 
        my $project_oid = param1('project_oid'); 
        if ( blankStr($project_oid) ) { 
            printError("No project has been selected."); 
            print endform(); 
            return; 
        } 
 
        my $merged_project = param1('merged_project'); 
        if ( blankStr($merged_project) ) { 
            printError("Please select a project to merge."); 
            print endform(); 
            return; 
        } 
 
        if ( $project_oid == $merged_project ) { 
            printError("The two projects are the same. Please select a different project to merge."); 
            print endform(); 
            return; 
        } 
 
        ProjectInfo::MergeProjects($project_oid, $merged_project);
    } 
    elsif ( $page eq 'dbMergeProjects' ) {
        my $msg = ProjectInfo::dbMergeProjects(); 
 
        if ( ! blankStr($msg) ) { 
            WebFunctions::showErrorPage($msg);
          } 
        else { 
            ShowNextPage();
        } 
    } 
    else {
	ShowPage();
    }
}


#########################################################################
# ShowNextPage
##########################################################################
sub ShowNextPage {
    my ($new_code) = @_;

    my $page = param1('page');

    if ( $page eq 'showCategory' && length($new_code) > 0 ) {
	ShowCategoryPage($new_code);
    }
    elsif ( $page eq 'showCategory' && defined param1('cat_code') ) {
	my $cat_code = param1('cat_code');
	if ( length($cat_code) > 0 ) {
	    ShowCategoryPage($cat_code);
	}
	else {
	    ShowPage();
	}
    }
    else {
	ShowPage();
    }
}


#########################################################################
# ShowPage
##########################################################################
sub ShowPage {
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    elsif ( ! getIsHmpUser($contact_oid) ) {
	dienice("You are not HMP user.");
    }

    print "<h2>Human Microbiome Project</h2>\n";

    my $cnt = allHmpProjectCount($contact_oid);
    my $select_cnt = filteredHmpProjectCount($contact_oid);

    print "<p>Selected Project Count: $select_cnt (Total: $cnt)";
    if ( $select_cnt != $cnt ) {
	print nbsp(3);
	print "<font color='red'>(Search condition is on. Not all projects are displayed.)</font></p>\n";
    }
    else {
	print "</p>\n";
    }

    # save orderby param
    my $orderby = param1('hmpproj_orderby');
    my $desc = param1('project_desc');
 
    # max display 
    my $max_display = getSessionParam('hmp_proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    # display page numbers 
    my $curr_page = param1('hmpproj_page_no'); 
    my $url1 = url() . "?section=HmpProj&page=showPage&hmpproj_page_no=";
    my $url2 = "";
    if ( ! blankStr($orderby) ) { 
	$url2 .= "&hmpproj_orderby=$orderby"; 
	if ( ! blankStr($desc) ) { 
	    $url2 .= "&project_desc=$desc"; 
	} 
    } 
    DisplayProjPageNo($curr_page, $max_display, $select_cnt, $url1, $url2);

    printHmpProjButtons(1);

    if ( $select_cnt > 0 ) {
	listHmpProjects($contact_oid, $curr_page, '', $select_cnt); 

	printHmpProjButtons(1);
    }

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print endform();
}


#########################################################################
# DisplayProjPageNo
##########################################################################
sub DisplayProjPageNo {
    my ($curr_page, $per_page, $total_cnt, $url1, $url2) = @_;

    # make sure curr_page is defined
    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 

    # start and end pages
    my $cnt0 = 20;
    my $start = $cnt0 * floor(($curr_page-1) / $cnt0) + 1;
    my $end = $start + $cnt0 - 1;
    my $last = ceil($total_cnt / $per_page);
    if ( $end > $last ) {
	$end = $last;
    }
    my $link = "";

#    print "<p>curr_page: $curr_page, start: $start, end: $end, last: $last</p>\n";

    # show first?
    if ( $start > 1 ) {
        $link = "<a href='" . $url1 . "1" . $url2;
        $link .= "' >" . "First" . "</a>";
        print $link . nbsp(1);
    }

    # show previous?
    if ( $start > ($cnt0 + 1) ) {
	my $prev = $start - $cnt0;
        $link = "<a href='" . $url1 . $prev . $url2;
        $link .= "' >" . "Prev" . "</a>";
        print $link . nbsp(1);
    }

    my $i = 0; 
    my $page = 1; 
    for ( $page = $start; $page <= $end; $page++) {
        my $s = $page; 
        if ( $page == $curr_page ) { 
            $s = "<b>$page</b>"; 
        } 
        $link = "<a href='" . $url1 . $page . $url2;
        $link .= "' >" . $s . "</a>";
        print $link . nbsp(1);
    }   # end for loop

    # show next?
    $page = $start + $cnt0;
    if ( $page <= $last ) {
        $link = "<a href='" . $url1 . $page . $url2;
        $link .= "' >" . "Next" . "</a>";
        print $link . nbsp(1);
    }

    # show last
    if ( $last > $end ) {
        $link = "<a href='" . $url1 . $last . $url2;
        $link .= "' >" . "Last" . "</a>";
        print $link . nbsp(1);
    }
}


######################################################################### 
# getHmpCond - get HMP project query condition
########################################################################## 
sub getHmpCond2 {
    my $cond = "p.project_oid in (select pipr.project_oid from project_info_project_relevance pipr where pipr.project_relevance = 'Human Microbiome Project (HMP)')";

    return $cond;
}


######################################################################### 
# allHmpProjectCount - count the number of projects 
########################################################################## 
sub allHmpProjectCount { 
    my ($contact_oid) = @_; 

    my $dbh=WebFunctions::Connect_IMG; 

    my $hmp_cond = getHmpCond();
    my $sql = "select count(*) from project_info p where " .
	$hmp_cond;

    my $cur=$dbh->prepare($sql); 
    $cur->execute();
    my ( $cnt ) = $cur->fetchrow_array();
    $cur->finish(); 
    $dbh->disconnect();
 
    if ( ! $cnt ) {
        return 0; 
    } 
    return $cnt; 
} 

 
######################################################################### 
# filteredHmpProjectCount - count the number of filtered projects
########################################################################## 
sub filteredHmpProjectCount {
    my ($contact_oid) = @_; 
 
    my $dbh=WebFunctions::Connect_IMG; 

    my $hmp_cond = getHmpCond();
    my $filter_cond = ProjectInfo::projectFilterCondition('hmp_');
    my $sql = "select count(*) from project_info p where " .
	$hmp_cond;

    # filter condition
    if ( ! blankStr($filter_cond) ) {
	$sql .= " and " . $filter_cond;
    } 

    # print "<p>SQL: $sql</p>\n";

    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 



#########################################################################
# getGoldWebPageCode
#
# (Note: need to use term_oid+1 because code starts with 0)
##########################################################################
sub getGoldWebPageCode {
    my %h;

    my $dbh = Connect_IMG();
    my $sql = "select term_oid+1, description from web_page_codecv";
    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
 
    for (;;) { 
        my ( $id, $val ) = $cur->fetchrow( );
	last if !$id;

	$h{$id} = $val;
    }
    $cur->finish();
    $dbh->disconnect();

    return %h;
}

#########################################################################
# ShowCategoryPage
##########################################################################
sub ShowCategoryPage {
    my ($new_code) = @_;

    my $code = $new_code;
    if ( length($code) == 0 ) {
	$code = param1('cat_code');
	if ( length($code) == 0 ) {
	    ShowPage();
	    return;
	}
    }

    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid();
 
    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
#    my $isAdmin = getIsAdmin($contact_oid); 

    my $db_code = $code;
    $db_code =~ s/\_/ /g;

    print "<h2>HMP $db_code Projects</h2>\n";

#    print "<p><font color='red'>Under Construction</font></p>\n";

    if ( param1('page') ) {
	print hiddenVar('page', param1('page'));
    }
    else {
	print hiddenVar('page', 'showCategory');
    }

 
    my $cat_sql = "";
    my $cond1 = getHmpCond();
    if ( $code eq 'Unclassified' ) {
        $cat_sql = qq{ 
            select count(*) from project_info p 
                where p.body_sample_site is null
		and $cond1
            }; 
    }
    else {
	$db_code =~ s/'/''/g;   # replace ' with ''

	$cat_sql = qq{
	    select count(*) from project_info p
		where body_sample_site = '$db_code'
		and $cond1
	    };
    }

    my $category_count = db_getValue($cat_sql);
    print "<p><font color='blue'>(Count: $category_count)</font></p>\n";

    my $curr_page = param1('category_page_no');
    my $orderby = param1('hmpproj_orderby'); 
    my $desc = param1('project_desc'); 
    my $max_display = getSessionParam('hmp_proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    if ( blankStr($curr_page) || $curr_page <= 0 ) { 
        $curr_page = 1; 
    } 

    # save parameters
    print hiddenVar('cat_code', $code);
    print hiddenVar('category_page_no', $curr_page);
    if ( ! blankStr($orderby) ) {
	print hiddenVar('hmpproj_orderby', $orderby);
    }
    if ( ! blankStr($desc) ) {
	print hiddenVar('project_desc', $desc);
    }

    # display page numbers
    my $url1 = url() . "?section=HmpProj&page=showCategory" . 
	    "&cat_code=$code" .
            "&category_page_no=";
    my $url2 = "";
    if ( ! blankStr($orderby) ) { 
	$url2 .= "&hmpproj_orderby=$orderby"; 
	if ( ! blankStr($desc) ) { 
	    $url2 .= "&project_desc=$desc"; 
	} 
    } 
    DisplayProjPageNo($curr_page, $max_display, $category_count,
		      $url1, $url2);

#    my $i = 0; 
#    my $page = 1; 
#    while ( $i < $category_count ) { 
#        my $s = $page; 
#        if ( $page == $curr_page ) { 
#            $s = "<b>$page</b>"; 
#        } 
#        my $link = "<a href='" . url() . 
#            "?section=GoldProj&page=showCategory" . 
#	    "&web_code=$code" .
#            "&category_page_no=$page";
#
#        if ( ! blankStr($orderby) ) { 
#            $link .= "&goldproj_orderby=$orderby"; 
#            if ( ! blankStr($desc) ) { 
#                $link .= "&project_desc=$desc"; 
#            } 
#        } 

#        $link .= "' >" . $s . "</a>"; 
#        print $link . nbsp(1); 
#        $i += $max_display; 
#        $page++; 
#        if ( $page > 1000 ) {
#	    print " ...\n";
#            last; 
#        } 
#    } 
 
    printHmpProjButtons(0);

    listHmpProjects($contact_oid, $curr_page, $code, $category_count);

    printHmpProjButtons(0);

#    printAdditionalProjSection();

    # Home 
    print "<p>\n";
    printHomeLink(); 
 
    print endform();
}


#########################################################################
# listHmpProjects - list projects 
##########################################################################
sub listHmpProjects {
    my ($contact_oid, $curr_page, $code, $cnt) = @_; 

    my $cond = " where " . getHmpCond();

    if ( length($code) == 0 ) {
	my $filter_cond = ProjectInfo::projectFilterCondition('hmp_');
	if ( ! blankStr($filter_cond) ) {
	    $cond .= " and " . $filter_cond;
	}
    }
    elsif ( $code eq 'Unclassified' ) {
	$cond .= " and p.body_sample_site is null";
    }
    else {
	my $db_code = $code;
	$db_code =~ s/'/''/g;   # replace ' with ''
	$db_code =~ s/\_/ /g;

	$cond .= " and p.body_sample_site = '$db_code'";
    }

    my $selected_proj = param1('project_oid');
 
    # max display 
    my $max_display = getSessionParam('hmp_proj_filter:max_display'); 
    if ( blankStr($max_display) ) {
        $max_display = $default_max_row;
    } 

    my $dbh=WebFunctions::Connect_IMG;
    my $orderby = param1('hmpproj_orderby');
    my $desc = param1('project_desc');
    if ( blankStr($orderby) ) {
        $orderby = "p.project_oid $desc";
    }
    else {
        if ( $orderby eq 'project_oid' ) {
            $orderby = "p.project_oid $desc"; 
        } 
        else {
            $orderby = "p." . $orderby . " $desc, p.project_oid";
        } 
    } 
 
    my $sql = qq{
        select p.project_oid, p.hmp_id, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.ncbi_project_id, p.body_sample_site,
	p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
            $cond
            order by $orderby
        }; 
 
    if ( $contact_oid == 312 ) {
        print "<p>SQL: $sql</p>";
    } 
 
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<h5>Click the column name to have the data order by the selected column. Click (Rev) to order by the same column in reverse order.</h5>\n";
    print "<p>\n";
    print "<table class='img' border='1'>\n";
#    print "<th class='img'>Selection</th>\n";

    print "<th class='img'>" . 
        getHmpProjOrderByLink('project_oid', 'Project ID', $code, $cnt, 0) .
        "<br/>" . 
        getHmpProjOrderByLink('project_oid', '(Rev)', $code, $cnt, 1) .
        "</th>\n";

    print "<th class='img'>" . 
        getHmpProjOrderByLink('hmp_id', 'HMP ID', $code, $cnt, 0) .
        "<br/>" . 
        getHmpProjOrderByLink('hmp_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n";

    print "<th class='img'>" . 
        getHmpProjOrderByLink('display_name', 'Project Display Name', $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('display_name', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 
    print "<th class='img'>" .
        getHmpProjOrderByLink('gold_stamp_id', 'GOLD ID', $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('gold_stamp_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 
 
    print "<th class='img'>" .
        getHmpProjOrderByLink('phylogeny', 'Phylogeny', $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('phylogeny', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>" .
        getHmpProjOrderByLink('ncbi_project_id', 'NCBI Project ID',
			       $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('ncbi_project_id', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

    print "<th class='img'>" .
        getHmpProjOrderByLink('body_sample_site', 'Body Sample Site',
			       $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('body_sample_site', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 
 
#    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>" .
        getHmpProjOrderByLink('contact_name', 'Contact Name',
			       $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('contact_name', '(Rev)', $code, $cnt, 1) .
        "</th>\n"; 

#    print "<th class='img'>IMG Contact</th>\n";

    print "<th class='img'>" . 
        getHmpProjOrderByLink('add_date', 'Add Date', $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('add_date', '(Rev)', $code, $cnt, 1) .
        "</th>\n";
    print "<th class='img'>" . 
        getHmpProjOrderByLink('mod_date', 'Last Mod Date', $code, $cnt, 0) .
        "<br/>" .
        getHmpProjOrderByLink('mod_date', '(Rev)', $code, $cnt, 1).
        "</th>\n";
 
    my $cnt2 = 0; 
    my $disp_cnt = 0;
    my $skip = $max_display * ($curr_page - 1);
    my $cpp_sql = "select count(*) from contact_project_permissions where contact_oid = $contact_oid and project_permissions = ";

    for (;;) { 
        my ( $proj_id, $hmp_id, $proj_name, $gold_stamp_id, 
             $phylo, $ncbi_project_id, $body_sample_site,
	     $add_date, $c_name, $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 <= $skip ) {
            next; 
        } 
 
        $disp_cnt++; 
        if ( $disp_cnt > $max_display ) {
            last;
        } 
 
        print "<tr class='img'>\n";
 
#        print "<td class='img'>\n";

	# check edit permission
#	print "<input type='radio' "; 
#	print "name='project_oid' value='$proj_id'";
#	if ( $proj_id == $selected_proj ) {
#	    print " checked ";
#	}
#	print "/>";
#        print "</td>\n"; 

        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);

	PrintAttribute($hmp_id);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
	if ( $ncbi_project_id ) {
	    PrintAttribute(getNcbiProjLink($ncbi_project_id)); 
	}
	else {
	    PrintAttribute($ncbi_project_id);
	}

        PrintAttribute($body_sample_site); 
        PrintAttribute($c_name); 
 
#        if ( $contact_list{$c_oid} ) {
#            PrintAttribute($contact_list{$c_oid});
#        }
#        else { 
#            my $contact_str = db_getContactName($c_oid);
#            $contact_list{$c_oid} = $contact_str;
#            PrintAttribute($contact_str);
#        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 
 
    $cur->finish(); 
    $dbh->disconnect(); 

    return $cnt2;
}


##########################################################################
# getHmpProjOrderByLink
##########################################################################
sub getHmpProjOrderByLink {
    my ($attr, $label, $code, $category_count, $desc) = @_;
 
    my $link = "<a href='" . url() . "?section=HmpProj";

    if ( length($code) > 0 ) {
	$link .= "&page=showCategory&web_code=$code";
    }
    else {
	$link .= "&page=showPage";
    }

    $link .= "&category_page_no=1&hmpproj_orderby=$attr";
    if ( $desc ) { 
        $link .= "&project_desc=desc";
    } 
    else {
        $link .= "&project_desc=asc";
    } 
    $link .= "&goldproj_page_no=1' >" . escapeHTML($label) . "</a>";
 
    return $link; 
} 


######################################################################### 
# printHmpProjButtons
######################################################################### 
sub printHmpProjButtons {
    my ($with_filter) = @_;

    print "<br/>\n";

    if ( $with_filter ) {
	# project selection filter 
	print "&nbsp; \n"; 
	print '<input type="submit" name="_section_HmpProj:filterProject" value="Search Projects" class="smbutton" />';
    }
}


######################################################################### 
# NewGoldProject 
######################################################################### 
sub NewGoldProject { 
 
    my $url=url(); 
    print startform(-name=>'newProject',-method=>'post',action=>"$url"); 

#    if ( param1('next_page') ) {
#	print hiddenVar('next_page', param1('next_page'));
#    }

    print hiddenVar('page_name', 'newProject');
    my %db_val; 
    if ( defined param1('web_code') ) {
        $db_val{'web_page_code'} = param1('web_code');
    }

    # tab view 
    TabHTML::printTabAPILinks("newProject"); 
    my @tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", "#tab5", "#tab6" ); 
 
    my @tabNames = 
        ( "Organism Info (*)", "Project Info (*)", 
          "Sequencing Info", "Environment Metadata",
	  "Host Metadata", "Organism Metadata" ); 
 
    TabHTML::printTabDiv( "newProject", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    ProjectInfo::printProjectTab("Organism", "", \%db_val); 
    print "</p></div>\n"; 
 
    # tab 2 
    print "<div id='tab2'><p>\n"; 
    ProjectInfo::printProjectTab("Project", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_project_relevance', '');
    ProjectInfo::printProjectSetValTab ('project_info_data_links', '');
    print "</p></div>\n";
 
    # tab 3 
    print "<div id='tab3'><p>\n"; 
    ProjectInfo::printProjectTab("Links", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_seq_method', '');

    print "</p></div>\n";
 
    # tab 4
    print "<div id='tab4'><p>\n";
    ProjectInfo::printProjectTab("Metadata", "", \%db_val);
    print "</p></div>\n"; 

    # tab 5
    print "<div id='tab5'><p>\n"; 
    ProjectInfo::printProjectTab("HostMeta", "", \%db_val); 
    print "</p></div>\n";

    # tab 6 
    print "<div id='tab6'><p>\n"; 
    ProjectInfo::printProjectTab("OrganMeta", "", \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_cell_arrangement', '');
    ProjectInfo::printProjectSetValTab ('project_info_diseases', ''); 
    ProjectInfo::printProjectSetValTab ('project_info_habitat', '');
    ProjectInfo::printProjectSetValTab ('project_info_metabolism', '');
    ProjectInfo::printProjectSetValTab ('project_info_phenotypes', '');
    ProjectInfo::printProjectSetValTab ('project_info_energy_source', '');
    print "</p></div>\n";
 
    TabHTML::printTabDivEnd();
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:dbNewGoldProject" value="Add Project" class="medbutton" />';

    printHomeLink(); 
 
    print endform();
} 
 
######################################################################### 
# UpdateGoldProject 
######################################################################### 
sub UpdateGoldProject { 
    my ($project_oid) = @_;

    my %db_val = ProjectInfo::SelectProjectInfo($project_oid);
 
    my $url=url(); 
    print startform(-name=>'updateProject',-method=>'post',action=>"$url"); 

    print "<h2>Update Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid);
#    if ( param1('next_page') ) {
#	print hiddenVar('next_page', param1('next_page'));
#    }

    # save parameters
    for my $p ('page', 'web_code', 'category_page_no',
	       'goldproj_orderby', 'project_desc') {
	if ( param1($p) ) {
	    print hiddenVar($p, param1($p));
	}
	elsif ( $p eq 'web_code' ) {
	    print hiddenVar('web_code', 0);
	}
    }

    # tab view 
    TabHTML::printTabAPILinks("updateProject"); 
    my @tabIndex = ( "#tab1", "#tab2", "#tab3", "#tab4", "#tab5", "#tab6"); 
 
    my @tabNames = 
        ( "Organism Info (*)", "Project Info (*)", 
          "Sequencing Info", "Environment Metadata",
	  "Host Metadata", "Organism Metadata"); 
 
    TabHTML::printTabDiv( "updateProject", \@tabIndex, \@tabNames ); 
 
    # tab 1 
    print "<div id='tab1'><p>\n"; 
    ProjectInfo::printProjectTab("Organism", $project_oid, \%db_val); 
    print "</p></div>\n"; 
 
    # tab 2 
    print "<div id='tab2'><p>\n"; 
    ProjectInfo::printProjectTab("Project", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_project_relevance', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_data_links', $project_oid);
    print "</p></div>\n";
 
    # tab 3 
    print "<div id='tab3'><p>\n"; 
    ProjectInfo::printProjectTab("Sequencing", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_seq_method', $project_oid);
    print "</p></div>\n";
 
    # tab 4
    print "<div id='tab4'><p>\n";
    ProjectInfo::printProjectTab("EnvMeta", $project_oid, \%db_val);
    print "</p></div>\n"; 

    # tab 5
    print "<div id='tab5'><p>\n"; 
    ProjectInfo::printProjectTab("HostMeta", $project_oid, \%db_val); 
    print "</p></div>\n"; 

    # tab 6 
    print "<div id='tab6'><p>\n"; 
    ProjectInfo::printProjectTab("OrganMeta", $project_oid, \%db_val); 
    ProjectInfo::printProjectSetValTab ('project_info_cell_arrangement', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_diseases', $project_oid); 
    ProjectInfo::printProjectSetValTab ('project_info_habitat', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_metabolism', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_phenotypes', $project_oid);
    ProjectInfo::printProjectSetValTab ('project_info_energy_source', $project_oid);
    print "</p></div>\n"; 
 
    TabHTML::printTabDivEnd();
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:dbUpdateGoldProject" value="Update Project" class="meddefbutton" />';
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:showCategory" value="Cancel" class="smbutton" />';

    printHomeLink(); 
 
    print endform();
} 
 

############################################################################# 
# DeleteGoldProject - delete project 
############################################################################# 
sub DeleteGoldProject { 
    my $url=url(); 
 
    print startform(-name=>'deleteProject',-method=>'post',action=>"$url"); 
 
    my $project_oid = param1('project_oid'); 
    if ( blankStr($project_oid) ) { 
        printError("No project has been selected."); 
        print endform(); 
        return; 
    } 
 
    print "<h2>Delete Project $project_oid</h2>\n"; 
    print hidden('project_oid', $project_oid); 
#    if ( param1('next_page') ) {
#	print hiddenVar('next_page', param1('next_page'));
#    }

    # save parameters
    for my $p ('web_code', 'category_page_no',
	       'goldproj_orderby', 'project_desc') {
	if ( defined param1($p) ) {
	    print hiddenVar($p, param1($p));
	}
    }

    # check whether there are any FK 
    # check submission 
    my $cnt = db_getValue("select count(*) from submission where project_info = $project_oid"); 
    if ( $cnt > 0 ) { 
        printError("This project is linked to an IMG submission. You cannot delete a submitted project."); 
        print endform(); 
        return; 
    } 
 
    # show project, if any 
    ProjectInfo::showProjectSample($project_oid); 
 
    print "<p>Please click the Delete button to confirm the deletion, or cancel the deletion.</p>\n"; 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:dbDeleteGoldProject" value="Delete" class="smdefbutton" />'; 
    print "&nbsp; \n"; 
    print '<input type="submit" name="_section_GoldProj:showCategory" value="Cancel" class="smbutton" />'; 
 
    print "<p>\n"; 
    printHomeLink(); 
 
    print endform(); 
} 


#########################################################################
# CopyGoldProject 
######################################################################### 
sub CopyGoldProject { 
    my ($project_oid) = @_;
 
    # check project oid 
    if ( blankStr($project_oid) ) {
        printError("No project has been selected."); 
        print endform(); 
        return;
    }
 
    my $gold_stamp_id = db_getValue("select gold_stamp_id from project_info where project_oid = $project_oid");
 
    my $url=url(); 
    print startform(-name=>'copyProject',-method=>'post',action=>"$url");
 
    print "<h2>Copy Project $project_oid</h2>\n";
    print hiddenVar('project_oid', $project_oid); 
 
    my $web_code = param1('web_code');
    if ( length($web_code) > 0 ) {
	print hiddenVar('web_code', $web_code);
    }

    # get new project oid 
    my $new_proj_oid = 0;
    if ( $gold_stamp_id ) {
        # GOLD project
        # use higher range project oid for GOLD entry
        $new_proj_oid = db_findMaxID('project_info', 'project_oid') + 1;
    } 
    else { 
        $new_proj_oid = ProjectInfo::getNewProjectId();
    }

    if ( $new_proj_oid <= 0 ) {
        # error checking -- this shouldn't happen 
        printError("Incorrect new project ID value: $new_proj_oid"); 
        print endform(); 
        return;
    } 
 
    print "<h3>New Project ID: $new_proj_oid</h3>\n";
 
#    $new_proj_oid = 2; 
    my @sqlList = ();
 
    my $contact_oid = getContactOid();
    my $ins = "insert into project_info(project_oid, contact_oid, add_date";
    my $sel = "select $new_proj_oid, $contact_oid, sysdate";
 
    # get Project_Info definition 
    my $def_project = def_Project_Info();
    my @attrs = @{$def_project->{attrs}};
    for my $attr ( @attrs ) { 
        if ( $attr->{name} =~ /^ncbi\_/ ) {
            # ok - NCBI taxon info
        } 
        elsif ( ! $attr->{can_edit} ) { 
            # skip non-editable attribute
            next;
        }
 
        $ins .= ", " . $attr->{name}; 
        $sel .= ", " . $attr->{name};
    } 
 
    my $sql = $ins . ") " . $sel . " from project_info " .
        "where project_oid = " . $project_oid;
    push @sqlList, ( $sql ); 
 
    # set-valued attributes
    my @tables = getProjectAuxTables();
    for my $tname ( @tables ) { 
        my $def_aux = def_Class($tname); 
        if ( ! $def_aux ) { 
            next; 
        } 
 
        my @aux_attrs = @{$def_aux->{attrs}};
        $ins = "insert into $tname (" . $def_aux->{id};
        $sel = "select $new_proj_oid";
        for my $attr2 ( @aux_attrs ) {
            if ( $attr2->{name} eq $def_aux->{id} ) {
                next; 
            } 
 
            $ins .= ", " . $attr2->{name};
            $sel .= ", " . $attr2->{name}; 
        } 
        $sql = $ins . ") " . $sel . " from " . $tname .
            " where " . $def_aux->{id} . " = " . $project_oid;
        push @sqlList, ( $sql );
    } 
 
    db_sqlTrans(\@sqlList); 
 
    print "<p>New project has been created by copying project " .
        $project_oid . ".</p>\n";
 
    print "<p>Note: Samples of this project (if any) are not copied.</p>\n";
 
    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:showCategory" value="OK" class="smbutton" />'; 
 
    print "<p>\n";
    printHomeLink();
 
    print endform(); 
} 


############################################################################# 
# SearchByID - search by GOLD Stamp ID
############################################################################# 
sub SearchByID {
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h3>Search GOLD Projects by GOLD Stamp ID</h3>\n"; 

    print "<p>Enter a GOLD Stamp ID or select from the list.</p>\n";

    print "<h4>Enter a GOLD Stamp ID</h4>\n";
    print "<input type='text' name='gold_stamp_id_1' value='" .
	"' size='10' maxLength='10'/>\n";

    print "<h4>Select a GOLD Stamp ID</h4>\n";
    my @gold_ids = db_getValues("select gold_stamp_id from project_info order by gold_stamp_id");
    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
    for my $id0 ( @gold_ids ) {
        print "    <option value='$id0' >$id0</option>\n";
    }
    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:searchIdResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print endform(); 

}


#########################################################################
# ShowSearchIdResultPage
# 
# admin does not have condition on contact_oid
##########################################################################
sub ShowSearchIdResultPage {
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Stamp ID Result Page</h2>\n"; 

#    print hiddenVar('next_page', 'searchIdResult');

    my $contact_oid = getContactOid();

    if ( ! $contact_oid ) {
        dienice("Unknown username / password");
    } 
    my $isAdmin = getIsAdmin($contact_oid); 

    my $cond = "";
    my $gold_stamp_id_1 = param1('gold_stamp_id_1');
    $gold_stamp_id_1 =~ s/'/''/g;   # replace ' with '', just in case
    my $gold_stamp_id_2 = param1('gold_stamp_id_2');
    $gold_stamp_id_2 =~ s/'/''/g;   # replace ' with '', just in case

    if ( ! blankStr($gold_stamp_id_1) ) {
	$cond = " where lower(p.gold_stamp_id) in ( '" .
	    lc($gold_stamp_id_1) . "'";
	if ( ! blankStr($gold_stamp_id_2) ) {
	    $cond .= ", '" . lc($gold_stamp_id_2) . "'";
	}
	$cond .= " )";
    }
    elsif ( ! blankStr($gold_stamp_id_2) ) {
	$cond = " where lower(p.gold_stamp_id) = '" .
	    lc($gold_stamp_id_2) . "'";
    }
    else {
        printError("Please enter or select a GOLD Stamp ID."); 
        print endform(); 
        return;
    }

    if ( $isAdmin ne 'Yes' ) {
        $cond = " and p.contact_oid = $contact_oid";
    } 
 
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
            $cond
        }; 
 
 
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>Project ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Phylogeny</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 
        if ( $cnt2 > $default_max_row ) {
            last;   # just in case
        } 
 
        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";
    }
    print "</table>\n"; 

    print "<font color='blue'>(Number of projects found: $cnt2)</font>\n";

    $cur->finish(); 
    $dbh->disconnect(); 

    if ( $cnt2 > 0 ) {
	printGoldProjButtons();

	printAdditionalProjSection();
    }

    print "<p>\n";
    printHomeLink();
 
    print endform(); 
}


############################################################################# 
# AdvancedSearch - advanced search
############################################################################# 
sub AdvancedSearch {
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    print "<h2>Search GOLD Projects by Query Condition</h2>\n"; 

    print "<h5>Text searches are based on case-insensitive substring match.</h5>\n";

    # gold stamp ID 
    ProjectInfo::printFilterCond('GOLD Stamp ID', 'gold_filter:gold_stamp_id',
				 'text', '', '', '');

    # common_name
    ProjectInfo::printFilterCond('Common Name', 'gold_filter:common_name', 
				 'text', '', '', '');

    # ncbi_project_id
    ProjectInfo::printFilterCond('NCBI Project ID',
				 'gold_filter:ncbi_project_id', 
				 'number', '', '', '');

    # ncbi_project_name 
    ProjectInfo::printFilterCond('NCBI Project Name',
				 'gold_filter:ncbi_project_name', 
				 'text', '', '', '');

    # gold web page code 
    ProjectInfo::printFilterCond('GOLD Web Page Code', 'gold_filter:web_page_code',
				 'select', 'query',
				 "select term_oid+1, description " .
				 "from web_page_codecv order by term_oid",
				 '');

    # project type 
    ProjectInfo::printFilterCond('Project Type', 'gold_filter:project_type', 
				 'select', 'query', 
				 'select cv_term from project_typecv order by cv_term', 
				 '');
 
    # project status 
    ProjectInfo::printFilterCond('Project Status', 'gold_filter:project_status', 
				 'select', 'query', 
				 'select cv_term from project_statuscv order by cv_term', 
				 '');
 
    # domain 
    ProjectInfo::printFilterCond('Domain', 'gold_filter:domain',
				 'select', 'list',
				 'ARCHAEAL|BACTERIAL|EUKARYAL|PLASMID|VIRUS',
				 '');
 
    # seq status 
    ProjectInfo::printFilterCond('Sequencing Status', 'gold_filter:seq_status',
				 'select', 'query', 
				 'select cv_term from seq_statuscv order by cv_term',
				 '');
 
    # seq method 
    ProjectInfo::printFilterCond('Sequencing Method', 'gold_filter:seq_method',
				 'select', 'query',
				 'select cv_term from seq_methodcv order by cv_term',
				 '');
 
    # availability: Proprietary, Public
    ProjectInfo::printFilterCond('Availability', 'gold_filter:availability', 
				 'select', 'list', 'Proprietary|Public', '');

    # phylogeny 
    ProjectInfo::printFilterCond('Phylogeny', 'gold_filter:phylogeny', 
				 'select', 'query', 
				 'select cv_term from phylogenycv order by cv_term', 
				 '');

    # project display name 
    ProjectInfo::printFilterCond('Project Display Name', 'gold_filter:display_name', 
				 'text', '', '', '');
 
    # genus 
    ProjectInfo::printFilterCond('Genus', 'gold_filter:genus', 
				 'text', '', '', '');
 
    # species 
    ProjectInfo::printFilterCond('Species', 'gold_filter:species', 
				 'text', '', '', '');
 
    # common_name 
    ProjectInfo::printFilterCond('Common Name', 'gold_filter:common_name', 
				 'text', '', '', '');
 
    # add date
    ProjectInfo::printFilterCond('Add Date', 'gold_filter:add_date',
				 'date', '', '', '');
 
    # mod date
    ProjectInfo::printFilterCond('Last Mod Date', 'gold_filter:mod_date',
				 'date', '', '', '');

#    print "<p>GOLD Stamp ID:\n";
#    print nbsp(3);
#    print "<input type='text' name='gold_stamp_id' value='" .
#	"' size='10' maxLength='10'/>\n";

#    print "<h4>Select a GOLD Stamp ID</h4>\n";
#    my @gold_ids = db_getValues("select gold_stamp_id from project_info order by gold_stamp_id");
#    print "<select name='gold_stamp_id_2' class='img' size='1'>\n";
#    for my $id0 ( @gold_ids ) {
#        print "    <option value='$id0' >$id0</option>\n";
#    }
#    print "</select>\n"; 

    print "<p>\n"; 
    print '<input type="submit" name="_section_GoldProj:advSearchResult" value="Search" class="smbutton" />'; 

    print "<p>\n";
    printHomeLink();
 
    print endform(); 
}


######################################################################### 
# goldFilterParams - get all gold project filter parameters 
######################################################################### 
sub goldFilterParams { 
    my @all_params = ( 'gold_filter:project_type', 
                       'gold_filter:project_status', 
                       'gold_filter:domain', 
                       'gold_filter:seq_status', 
                       'gold_filter:seq_method', 
                       'gold_filter:availability', 
                       'gold_filter:phylogeny', 
                       'gold_filter:display_name', 
                       'gold_filter:genus', 
                       'gold_filter:species', 
                       'gold_filter:common_name', 
                       'gold_filter:ncbi_project_name', 
                       'gold_filter:ncbi_project_id', 
                       'gold_filter:gold_stamp_id', 
                       'gold_filter:add_date', 
                       'gold_filter:mod_date', 
                       'gold_filter:web_page_code' ); 
 
    return @all_params; 
} 


######################################################################### 
# goldFilterCondition 
########################################################################## 
sub goldFilterCondition { 
    my $cond = ""; 
 
    # filter condition ??? 
    my @all_params = goldFilterParams(); 
    for my $p0 ( @all_params ) { 
        my $cond1 = ""; 
	my ($tag, $fld_name) = split(/\:/, $p0); 

	my $op_name = $p0 . ":op";
	my $op1 = "=";
	if ( param1($op_name) ) {
	    $op1 = param1($op_name);
	}
	if ( $op1 eq 'is null' || $op1 eq 'is not null' ) {
	    $cond1 ="p.$fld_name $op1";
	}
        elsif ( param1($p0) ) { 
            my @vals = split(/\,/, param1($p0)); 
 
            if ( lc($fld_name) eq 'display_name' ||
                 lc($fld_name) eq 'genus' || 
                 lc($fld_name) eq 'species' ||
                 lc($fld_name) eq 'common_name' ||
                 lc($fld_name) eq 'ncbi_project_name' ||
		 lc($fld_name) eq 'gold_stamp_id' ) {
                my $s1 = param1($p0); 
                if ( length($s1) > 0 ) { 
                    $cond1 = "lower(p.$fld_name) like '%" . lc($s1) . "%'";
                } 
            } 
            elsif ( lc($fld_name) eq 'ncbi_project_id' ) {
                my $s1 = param1($p0); 
                if ( length($s1) > 0 && isInt($s1) ) { 
                    $cond1 = "p.$fld_name $op1 $s1";
                } 
            } 
            elsif ( lc($fld_name) eq 'seq_method' ) {
                my $s1 = param1($p0);
                if ( length($s1) > 0 ) { 
                    $cond1 = "p.project_oid in (select pism.project_oid " .
                        "from project_info_seq_method pism " .
                        "where lower(pism.$fld_name) = '" . lc($s1) . "')";
                } 
            }
            elsif ( lc($fld_name) eq 'web_page_code' ) {
                for my $s2 ( @vals ) {
                    my $i = $s2 - 1;
                    if ( $i >= 0 ) { 
                        $cond1 = "p.$fld_name = $i";
                    } 
                } 
            } 
            elsif ( lc($fld_name) eq 'add_date' ||
                    lc($fld_name) eq 'mod_date' ) {
                my $op_name = $p0 . ":op"; 
                my $op1 = param1($op_name);
                my $d1 = param1($p0);
                if ( !blankStr($op1) && !blankStr($d1) ) {
                    $cond1 = "p.$fld_name $op1 '" . $d1 . "'";
                } 
            }
            else { 
                for my $s2 ( @vals ) { 
                    $s2 =~ s/'/''/g;   # replace ' with ''
                    if ( blankStr($cond1) ) { 
                        $cond1 = "p.$fld_name in ('" . $s2 . "'";
                    } 
                    else { 
                        $cond1 .= ", '" . $s2 . "'";
                    }
                } 
 
                if ( ! blankStr($cond1) ) {
                    $cond1 .= ")"; 
                } 
            } 
        } 
 
        if ( ! blankStr($cond1) ) { 
            if ( blankStr($cond) ) { 
                $cond = $cond1; 
            } 
            else {
                $cond .= " and " . $cond1;
            } 
        } 
    }  # end for p0 
 
    return $cond; 
} 


##########################################################################
# selectedGoldProjCount
##########################################################################
sub selectedGoldProjCount { 
    my ($contact_oid, $filter_cond) = @_; 
 
    my $dbh = Connect_IMG();
 
    my $sql = "select count(*) from project_info p"; 
    if ( $contact_oid ) { 
        $sql .= " where p.contact_oid = $contact_oid"; 
        if ( ! blankStr($filter_cond) ) { 
            $sql .= " and " . $filter_cond; 
        } 
    } 
    else { 
        if ( ! blankStr($filter_cond) ) { 
            $sql .= " where " . $filter_cond; 
        } 
    } 

    my $cur=$dbh->prepare($sql); 
    $cur->execute(); 
    my ( $cnt ) = $cur->fetchrow_array(); 
    $cur->finish(); 
    $dbh->disconnect(); 
 
    if ( ! $cnt ) { 
        return 0; 
    } 
    return $cnt; 
} 


########################################################################## 
# ShowAdvSearchResultPage
########################################################################## 
sub ShowAdvSearchResultPage{
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Search Result</h2>\n"; 

    my $filter_cond = goldFilterCondition(); 
 
    my $select_cnt = 0;
    if ( $isAdmin eq 'Yes' ) {
        $select_cnt = selectedGoldProjCount( '', $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 
    else { 
        $select_cnt = selectedGoldProjCount($contact_oid, $filter_cond );
        print "<p>Selected GOLD Project Count: $select_cnt</p>\n";
    } 

    if ( $select_cnt == 0 ) {
	print "<p>\n";
	printHomeLink();

	print endform(); 
	return;
    }

    printGoldProjButtons();

    # show results
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = qq{
        select p.project_oid, p.display_name, p.gold_stamp_id,
        p.phylogeny, p.add_date, p.contact_name, p.mod_date, p.contact_oid
            from project_info p 
        }; 

    if ( ! blankStr($filter_cond) ) {
	$sql .= " where " . $filter_cond;
    }

    print "<p>\n";
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my %contact_list;
 
    print "<p>\n";
    print "<table class='img' border='1'>\n";
    print "<th class='img'>Selection</th>\n";
    print "<th class='img'>Project ID</th>\n";
    print "<th class='img'>Project Display Name</th>\n";
    print "<th class='img'>GOLD ID</th>\n";
    print "<th class='img'>Phylogeny</th>\n";
    print "<th class='img'>Contact Name</th>\n";
    print "<th class='img'>IMG Contact</th>\n";
    print "<th class='img'>Add Date</th>\n";
    print "<th class='img'>Last Mod Date</th>\n";

    my $cnt2 = 0; 

    for (;;) { 
        my ( $proj_id, $proj_name, $gold_stamp_id, 
             $phylo, $add_date, $c_name, 
             $mod_date, $c_oid ) = 
		 $cur->fetchrow_array(); 
        if ( ! $proj_id ) {
            last; 
        } 
 
        $cnt2++; 

        print "<tr class='img'>\n";
 
        print "<td class='img'>\n";
        print "<input type='radio' "; 
        print "name='project_oid' value='$proj_id' />";
        print "</td>\n"; 
 
        my $proj_link = getProjectLink($proj_id);
        PrintAttribute($proj_link);
 
        PrintAttribute($proj_name); 
        my $gold_link = getGoldLink($gold_stamp_id);
        PrintAttribute($gold_link);
 
        PrintAttribute($phylo); 
        PrintAttribute($c_name); 
 
        if ( $contact_list{$c_oid} ) {
            PrintAttribute($contact_list{$c_oid});
        }
        else { 
            my $contact_str = db_getContactName($c_oid);
            $contact_list{$c_oid} = $contact_str;
            PrintAttribute($contact_str);
        }
 
        PrintAttribute($add_date); 
        PrintAttribute($mod_date);
        print "</tr>\n";

        if ( $cnt2 >= 1000 ) {
            last;
        } 
    }
    print "</table>\n"; 

    if ( $cnt2 < $select_cnt ) {
	print "<font color='blue'>(Too many rows: only $cnt2 rows displayed)</font>\n";
    }
    else {
	print "<font color='blue'>(Number of rows displayed: $cnt2)</font>\n";
    }

    $cur->finish(); 
    $dbh->disconnect(); 

    printGoldProjButtons();

    printAdditionalProjSection();

    print "<p>\n";
    printHomeLink();
 
    print endform(); 
}


########################################################################## 
# printAdditionalProjSection
########################################################################## 
sub printAdditionalProjSection {
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 

    # allow admin to change IMG contact 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
        print "<h3>Change IMG Contact or Grant Edit Privilege for Selected Project</h3>\n"; 
        print "<p>New IMG Contact:\n"; 
        print "<select name='new_contact' class='img' size='1'>\n"; 
 
        my $sql2 = "select contact_oid, username from contact order by username, contact_oid"; 
        my $dbh2 = Connect_IMG_Contact(); 
        my $cur2=$dbh2->prepare($sql2); 
        $cur2->execute(); 
 
        for (my $j2 = 0; $j2 <= 10000; $j2++) { 
            my ($id2, $name2) = $cur2->fetchrow_array(); 
            if ( ! $id2 ) { 
                last; 
            } 
 
            print "    <option value='$id2'"; 
            if ( $contact_oid == $id2 ) { 
                print " selected "; 
            } 
            print ">$name2 (OID: $id2)</option>\n"; 
        } 
        print "</select>\n"; 
        $cur2->finish(); 
        $dbh2->disconnect(); 
 
        print nbsp(3); 
        print '<input type="submit" name="_section_GoldProj:changeContact" value="Change Contact" class="medbutton" />'; 
 
        print "<p>\n"; 
        print '<input type="submit" name="_section_GoldProj:grantEdit" value="Grant Edit Privilege" class="medbutton" />'; 
    } 

    # allow admin to change GOLD id 
    if ( $isAdmin eq 'Yes' ) { 
        print hr; 
 
        print "<h3>Assign or Change GOLD Stamp ID for Selected Project</h3>\n";
        print "<p>Note: GCAT ID and HMP ID (for HMP projects only) will be automatically assigned when the selected project gets a GOLD Stamp ID. GCAT ID and HMP ID (if any) will be automatically deleted when the project GOLD Stamp ID is deleted.</p>\n"; 
	print "<br/>\n";
        print '<input type="submit" name="_section_GoldProj:setGoldStampId" value="Assign GOLD Stamp ID" class="medbutton" />';
        print "&nbsp; \n";
        print '<input type="submit" name="_section_GoldProj:delGoldStampId" value="Delete GOLD Stamp ID" class="medbutton" />';
    } 
 
    # allow admin to merge projects
    if ( $isAdmin eq 'Yes' ) {
	print hr;
	print "<h3>Merge Projects</h3>\n"; 
	print "<p>Merge the selected project with another project.</p>\n";
	print "<p>\n";
	print '<input type="submit" name="_section_GoldProj:mergeGoldProj" value="Merge Projects" class="medbutton" />';
    }
}


######################################################################### 
# PrintMergeGoldProjPage
######################################################################### 
sub PrintMergeGoldProjPage {
    my $url=url(); 
 
    print startform(-name=>'mainForm',-method=>'post',action=>"$url"); 
 
    my $contact_oid = getContactOid(); 
 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $isAdmin = getIsAdmin($contact_oid); 
 
    print "<h2>Merge Projects</h2>\n"; 

    my $project_oid = param1('project_oid');
    if ( ! $project_oid ) {
        printError("Please select a project first."); 
        print endform(); 
        return;
    }
    print "<h3>Selected Project: $project_oid</h3>\n";
    print hiddenVar('project_oid', $project_oid);

    print "<p>Merge the selected project with the following project:</p>\n"; 
 
    my $sql2 = "select project_oid, display_name from project_info " . 
        "order by project_oid"; 
    my $dbh2 = Connect_IMG (); 
    my $cur2=$dbh2->prepare($sql2); 
    $cur2->execute(); 
 
    print "<select name='merged_project' class='img' size='1'>\n"; 
    for (my $j2 = 0; $j2 <= 100000; $j2++) {
        my ($id2, $name2) = $cur2->fetchrow_array();
        if ( ! $id2 ) { 
            last; 
        } 
 
	if ( $id2 == $project_oid ) {
	    next;
	}

        print "    <option value='$id2'>";
        print "$id2 - $name2</option>\n";
    } 
    $cur2->finish(); 
    $dbh2->disconnect(); 
    print "</select>\n"; 
 
    print "<p>\n";
    print '<input type="submit" name="_section_GoldProj:mergeProjects" value="Merge Projects" class="medbutton" />';

    print "<p>\n";
    printHomeLink();
 
    print endform(); 
} 
 

##########################################################################
# FilterHmpProject - set filter on displaying projects
#             so that there won't be too many to display
##########################################################################
sub FilterHmpProject { 
    my $url=url(); 
 
    print startform(-name=>'filterProject',-method=>'post',action=>"$url");
 
    my $contact_oid = getContactOid(); 
    if ( ! $contact_oid ) { 
        dienice("Unknown username / password"); 
    } 
 
    my $uname = db_getContactName($contact_oid); 
 
    print "<h3>Set HMP Filter for $uname</h3>\n";
 
    # max display 
    my $max_display = getSessionParam('hmp_proj_filter:max_display');
    if ( blankStr($max_display) ) { 
        $max_display = "$default_max_row"; 
    } 
    print "<p>Maximal Rows of Display:\n"; 
    print nbsp(3); 
    print "<select name='hmp_proj_filter:max_display' class='img' size='1'>\n";
    for my $cnt0 ( '50', '80', '100', '200',
                   '300', '500', '800', '1000', '2000',
                   '3000', '4000', '5000', '10000' ) {
        print "    <option value='$cnt0' ";
        if ( $cnt0 eq $max_display ) {
            print " selected ";
        }
        print ">$cnt0</option>\n";
    }
    print "</select>\n";
 
    my $def_project = def_Project_Info();
    my @attrs = @{$def_project->{attrs}};

    my @tabs =  ( 'Organism', 'Project', 'Sequencing',
		  'EnvMeta', 'HostMeta', 'OrganMeta' );

    # tab display label
    my %tab_display = (
		       'Organism' => 'Organism Info',
		       'Metagenome' => 'Metagenome Info',
		       'Project' => 'Project Info',
		       'Sequencing' => 'Sequencing Info',
		       'EnvMeta' => 'Environment Metadata',
		       'HostMeta' => 'Host Metadata',
		       'OrganMeta' => 'Organism Metadata' );

    my @aux_tables = getProjectAuxTables();

    print "<table class='img' border='1'>\n";
    for my $tab ( @tabs ) {
	my $tab_label = $tab;
	if ( $tab_display{$tab} ) {
	    $tab_label = $tab_display{$tab};
	}

        print "<tr class='img' >\n";
        print "  <th class='subhead' colspan='3' align='right' bgcolor='lightblue'>" .
            "<font color='darkblue'>" . $tab_label . "</font></th>\n";
        print "</tr>\n";

	for my $k ( @attrs ) { 
	    if ( $k->{tab} ne $tab ) {
		next;
	    }
	    elsif ( ! $k->{filter_cond} ) {
		next;
	    }

	    # show filter condition
	    my $filter_name = "hmp_proj_filter:" . $k->{name};
	    my $ui_type = 'text';
	    my $select_type = '';
	    my $select_method = '';

	    if ( $k->{data_type} eq 'cv' ) {
		$ui_type = 'select';
		$select_type = 'query';
		$select_method = $k->{cv_query};
	    }
	    elsif ( $k->{data_type} eq 'list' ) {
		$ui_type = 'select';
		$select_type = 'list';
		$select_method = $k->{list_values};
	    }
	    elsif ( $k->{data_type} eq 'int' ||
		    $k->{data_type} eq 'number' ) {
		$ui_type = 'number';
	    }
	    elsif ( $k->{data_type} eq 'date' ) {
		$ui_type = 'date';
	    }
	    ProjectInfo::printFilterCondRow($k->{display_name}, $filter_name,
			       $ui_type, $select_type, $select_method,
			       getSessionParam($filter_name));
	}  # end for my k

	for my $k2 ( @aux_tables ) {
	    my $def_aux = def_Class($k2);
	    if ( ! $def_aux ) {
		next;
	    }
	    if ( $def_aux->{tab} ne $tab ) {
		next;
	    }

            # special condition for data links 
            if ( $k2 eq 'project_info_data_links' ) { 
		my $hmp_cond = getHmpCond();
                for my $typ ( 'Funding', 'Seq Center' ) { 
                    my $filter_name = "hmp_proj_filter:" . $k2 . ":" . $typ; 
                    my $filter_disp = "Data Links: " . $typ; 
                    my $filter_sql = qq{ 
                        select distinct p.db_name 
                            from project_info_data_links p
                            where p.link_type = '$typ' 
                            and p.db_name is not null 
			    and $hmp_cond
                            order by 1 
                        }; 
                    # $filter_sql = "select cv_term from db_namecv"; 
		  ProjectInfo::printFilterCondRow($filter_disp, 
                                                    $filter_name, 
                                                    'select', 'query', 
                                                    $filter_sql, 
						  getSessionParam($filter_name)); 
                } 
            } 

	    my @aux_attrs = @{$def_aux->{attrs}};
	    for my $k3 ( @aux_attrs ) {
		if ( ! $k3->{filter_cond} ) {
		    next;
		}

		my $filter_name = "hmp_proj_filter:" . $k3->{name};
		ProjectInfo::printFilterCondRow($k3->{display_name},
						$filter_name,
						'select', 'query',
						$k3->{cv_query},
				   getSessionParam($filter_name));
	    }
	}  # end for my k2
    } # end for my tab
    print "</table>\n";

    # buttons
    print "<p>\n"; 
    print '<input type="submit" name="_section_HmpProj:applyHmpFilter" value="Apply Project Filter" class="medbutton" />'; 
 
    print "<p>\n";
 
    printHomeLink(); 
 
    print endform();
} 


###########################################################################
# printFilterCond - print filter condition 
###########################################################################
sub printFilterCond { 
    my ($title, $param_name, $ui_type, $select_type, $select_method,
        $def_val_str) = @_; 

  ProjectInfo::printFilterCond($title, $param_name, $ui_type, $select_type,
			       $select_method, $def_val_str);
}


########################################################################## 
# ApplyHmpFilter - save filter info into database and apply filter 
########################################################################## 
sub ApplyHmpFilter { 

    # max display 
    my $max_display = param1('hmp_proj_filter:max_display'); 
    if ( ! blankStr($max_display) ) { 
        setSessionParam('hmp_proj_filter:max_display', $max_display); 
    } 
 
    # show filter selection 
    my @all_params = ProjectInfo::projectFilterParams('hmp_'); 
    for my $p0 ( @all_params ) { 
        if ( $p0 eq 'hmp_proj_filter:add_date' || 
             $p0 eq 'hmp_proj_filter:mod_date' ) { 
            # date 
            my $op_name = $p0 . ":op"; 
            my $op1 = param1($op_name); 
            my $d1 = param1($p0); 
            if ( !blankStr($d1) && !blankStr($op1) ) { 
                if ( ! isDate($d1) ) { 
                    print "<p>Incorrect Date (" . escapeHTML($d1) . 
                        ") -- Filter condition is ignored.</p>\n"; 
                } 
                else { 
                    setSessionParam($op_name, $op1); 
                    setSessionParam($p0, $d1); 
                } 
            } 
            else { 
                # clear condition 
                setSessionParam($op_name, ''); 
                setSessionParam($p0, ''); 
            } 

	    next;
        } 

	# op
	my $op_name = $p0 . ":op"; 
	if ( param1($op_name) ) {
	    setSessionParam($op_name, param1($op_name));
	}

	# value
        if ( param($p0) ) {
            # filter
            my @options = param($p0);
 
            my $s0 = "";
            for my $s1 ( @options ) {
                if ( blankStr($s0) ) { 
                    $s0 = $s1;
                } 
                else {
                    $s0 .= "," . $s1;
                }
            } 
            setSessionParam($p0, $s0); 
        } 
        else { 
            setSessionParam($p0, ''); 
        } 

	# type?
	my $type_name = $p0 . ":type"; 
	my $type_val = param1($type_name); 
	if ( !blankStr($type_val) ) {
	    setSessionParam($type_name, $type_val);
	}
    }  # end for p0
} 



1;



