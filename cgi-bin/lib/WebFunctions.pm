package WebFunctions;

require Exporter;
@ISA = qw{ Exporter };
@EXPORT = qw{
    getCgi
	getSessionId 
	getSession 
	getSessionParam 
	setSessionParam 
	getContactOid
	resetContactOid
	getContactUserName
	Connect_IMG
	Connect_IMG_Contact
	Connect_IMG_ER
	Connect_IMG_MI
	Connect_IMG_EXT
	checkAccess
	checkHmpAccess
	encode 
	decode 
	pwDecode 
	blockRobots
	StartPage
	ClosePage
	getSection
	sanitizeInt
	sanitizeStr
	printHomeLink
	printHint
	printNote
	printError
	printStatusLine
	showErrorPage
	showInfoPage
	printHash
	dienice
	blankStr
	strTrim
        fileSize
	isInt
        isIntList
	isNumber
	isDate
	isEmail
	removeQuote
	nbsp
	alink
	escHtml
	param1
	paramMatch
	hiddenVar
	saveAppHiddenVar
	saveHiddenVar
	saveProjectPageInfo
	clearProjectPageInfo
	saveSubmissionPageInfo
	clearSubmissionPageInfo
	webLog
	webLogNew
	webErrLog
	wunlink
	lastPathTok
	checkPath
	checkTmpPath
	validPathName
	checkFileName
	validFileName
	str2File
	file2Str
	newReadFileHandle
	newWriteFileHandle
	newAppendFileHandle
	currDateTime
	compareDate
	PrintAttribute
	PrintCell
	inArray
	inIntArray
	getNCBITaxonInfo
	db_findMaxID
	db_getValue
	db_getValues
	db_getValues2
	db_getValuesToString
	db_getSetAttr
	db_getContact
	db_getContactName
	db_getContactEmail
	db_sqlTrans
	db_con_sqlTrans
	db_er_sqlTrans
	getIsAdmin
	getIsJgiUser
	getIsHmpUser
	getIsHmpProject
	getCanEditImgGold
	getCanUpdateProject
	getUploadDir
	getAttrDispName
	getProjectLink
	getMProjectLink
	getProjectOrderByLink
	getSampleLink
	getSampleOrderByLink
	getSubmissionLink
	getSubmissionOrderByLink
	getContactLink
	getGoldLink
	getGoldDbUrl
	getGoldLogUrl
	getHomdLink
	getImgGoldLink
	getImgERUrl
	getImgMIUrl
	getImgHmpLink
	getNcbiProjLink
	getNcbiTaxonLink
	getGreengenesLink
        isSubmissionAdmin
        sendEmail
	printTooltip
	printTooltipScript
	printCellTooltip
	getNewCounterVal
	getNewGcatId
	getNewHmpId
	DisplaySetAttr
	getHmpCond
	getProjCond
	GetMetagenomeClassification
        getNewImgGoldMetadata
        getNewGoldMetadata
        getNewGoldField
        getGoldAnalysisProject
        postGoldAnalysisProject
        getGoldAnalysisField
        getGoldAPRole
        isJgiProject
        isJgiSample
        isJgiAnalysis
        ShowDataSetSelectionSection
        getDataSetDisplayName
        webErrorHeader
    };

use strict;
#use warnings;
use Time::localtime;
use LWP::Simple qw(!head); 
use CGI qw(:standard);
use CGI::Session;
use MIME::Base64 qw( encode_base64 decode_base64 );
use Digest::MD5 qw( md5_base64);
use Mail::Sendmail;
use WebEnv;
use DBI;
use Data::Dumper;
use FileHandle;
use REST::Client;
use JSON;

use CGI::Carp qw( carpout set_message  );
use CGI::Cookie;
use Cwd;
use File::Path qw(make_path);

# Force flush
$| = 1;

my $env = getEnv();
my $main_cgi             = $env->{main_cgi};
my $base_url = $env->{ base_url };
my $base_cgi_url = $env->{ base_cgi_url };
my $web_log_file = $env->{ web_log_file };
my $err_log_file = $env->{ err_log_file };
my $verbose = $env->{verbose};

#if( $ENV{ GATEWAY_INTERFACE } &&
#    $ENV{ GATEWAY_INTERFACE } ne "" ) {
#    my $err_fh = newAppendFileHandle( $err_log_file, "a" );
#    if( !defined( $err_fh ) ) {
#	die( "Unable to write '$err_log_file'\n" );
#    }
#    carpout( $err_fh );
#}

my $tmp_dir = $env->{ tmp_dir };
my $cgi_tmp_dir = $env->{ cgi_tmp_dir };
my $dev_site = $env->{ dev_site };

if ( ! $dev_site && $cgi_tmp_dir ne "" && !-e $cgi_tmp_dir ) {

    # lets make sure the directories exists
    umask 0002;
    make_path( $cgi_tmp_dir, { mode => 0775 } );
}

    $ENV{TMP}     = "/app/tmp";
    $ENV{TEMP}    = "/app/tmp";
    $ENV{TEMPDIR} = "/app/tmp";
    $ENV{TMPDIR}  = "/app/tmp";
$CGITempFile::TMPDIRECTORY = $TempFile::TMPDIRECTORY = '/app/tmp';

my $cgi = new CGI;

$CGITempFile::TMPDIRECTORY = $TempFile::TMPDIRECTORY = '/app/tmp';
    $ENV{TMP}     = "/app/tmp";
    $ENV{TEMP}    = "/app/tmp";
    $ENV{TEMPDIR} = "/app/tmp";
    $ENV{TMPDIR}  = "/app/tmp";


#my $g_session = new CGI::Session( "driver:File", $cgi, 
#			  { Directory => $cgi_tmp_dir } );

#my @tmps = split( /\//, $base_url );
my $cookie_name = "CGISESSID_submit";
if($env->{keycloak}) {
    $cookie_name = "CGISESSID_KC";
    
} 

CGI::Session->name($cookie_name);    # override default cookie name CGISESSID


my $cookie_sid = $cgi->cookie($cookie_name) || undef;

=comment
NEW 2021-09-17 for keycloak 
All login IMG web sites share the same cgi session directory (mer, abc private, submit).
See IMG's WebSessiom.pm

This is "sessions private" storage and session params
To use it, ONLY use getSessionParam and setSessionParam

IT'S NOT the cgi tmp directory!
  
DO NOT use directory /.../cfs/cdirs/webfs/img/scratch/cgi_session
FOR ANYTHING ELSE!!!!!!!
     
- Ken        
=cut 
my $g_session = '';
    $g_session = CGI::Session->load(undef, $cookie_sid, { Directory => '/tmp/cgi' });

#new CGI::Session( undef, $cgi, { Directory => $cgi_tmp_dir } );

if($g_session->is_empty || $g_session->is_expired) {
    $g_session = $g_session->new();
}

$g_session->expire("+12h");


# creates cgi session cookie
#
# input
# cookie value - the default is cgi session id
# expire time: default is 2 hours idle time
#
# +45m
# +90m expire after 90 minutes
# +24h - 24 hour cookie
# +1d - one day
# +6M   6 months from now
# +1y   1 year from now
#
# $session->expire("+1d");
# - ken
sub makeCookieSession {
    my $value = getSessionId();

    my $cookie = CGI::Cookie->new(
            -name   => $cookie_name,
            -value  => $value,
            -expires => '+12h'
    );
    return $cookie;
}

# make a img_kc cookie, to knows user was login using another IMG site 
# eg submit or abc 
sub makeCookieIMGKc {
    my($kc_id) = @_;
    
    my $cookie = CGI::Cookie->new(
            -name   => 'img_kc',
            -value  => $kc_id,
            -path    => '/',
            -expires => '+12h'
    );
    return $cookie;    
}

sub getCookieName {

    return $cookie_name;
}

sub getCgi { 
    return $cgi; 
} 
 

############################################################################
# getSessionId - Get session ID.
############################################################################
sub getSessionId {
    return $g_session->id( );
}

############################################################################
# getSession - Get session cookie
############################################################################
sub getSession {
    return $g_session;
}

############################################################################
# getSessionParam - Get session parameter.
############################################################################
sub getSessionParam {
    my( $arg ) = @_;

    if ( $g_session->param( $arg ) ) {
	return $g_session->param( $arg );
    }
    else {
	return "";
    }
}

############################################################################
# setSessionParam - Get session parameter.
############################################################################
sub setSessionParam {
    my( $arg, $val ) = @_;
#    print "set " . $g_session->id() . ": " . $arg . " => " . $val . "\n";

    $g_session->param( $arg, $val );
}

############################################################################
# getContactOid - Get currenct contact_oid for user restricted site.
############################################################################
sub getContactOid {
    my $contact_oid = getSessionParam ('contact_oid');
##    if ( $contact_oid == 312 ) {
##	$contact_oid = 123727;
##	return $contact_oid;
##    }

    if($contact_oid == 3038) {
        # select * from contact where email = 'camillaln@gmail.com';
        #return 100605;
    }

    return $contact_oid;
}

############################################################################
# resetContactOid - Compare base_url's.  If new one for the first time
#  reset contact_oid so old user is not carried over to next site.
############################################################################
sub resetContactOid {
    return;
    
}


############################################################################
# getContactUserName - Get currenct contact username
############################################################################
sub getContactUserName {
    return getSessionParam ('contact_username');
}


############################################################################
# Connect_IMG
############################################################################
sub Connect_IMG
{
   my ($db) = @_;
    if ( ! $db ) {
    $db = 'imgsg';
    }
    my $dbs_ref = $env->{db_connect};
    if ( ! $dbs_ref ) {
    return;
    }

    my $db_ref = $dbs_ref->{$db};
    if ( ! $db_ref ) {
    return;
    }

    # use the new test database imgsg_dev
    my $user2 = $db_ref->{user};
    my $pw2 = decode_base64( $db_ref->{pw} );
    my $service2 = $db_ref->{service};

    my $ora_host = $db_ref->{ora_host};
    my $ora_port = $db_ref->{ora_port};
    my $ora_sid = $db_ref->{ora_sid};

    # switch production databases?
    if ( $db eq 'er' || $db eq 'imger' ||
     $db eq 'mer' || $db eq 'imgmer' ) {
    my $ora_config_file = $env->{img_er_oracle_config};
    if ( $ora_config_file && (-e $ora_config_file) ) {
        my $s = file2Str($ora_config_file);
        my @str = split(/\n/, $s);
        for my $s2 ( @str ) {
        if ( $s2 =~ /^\#/ ) {
            next;
        }

        if ( $s2 =~ /gemini1/ ) {
            $service2 = "gemini1pdb";
            $ora_sid = "gemini1pdb";
        }
        elsif ( $s2 =~ /gemini2/ ) {
            $service2 = "gemini2pdb";
            $ora_sid = "gemini2pdb";
        }
        }
    }
    }

#    if($env->{localhost_ken}) {
#        $ora_port = "1525";
#        $ora_host = "localhost";
#        $pw2 = decode_base64( 'aW1nc2dfZGV2OTg3' );
#    }

    my $dsn2 = "dbi:Oracle:" . $service2;

#    if($ora_port ne "") {
        #$dsn2 = "dbi:Oracle:host=$ora_host;port=$ora_port;sid=$ora_sid";
#    }

    my $dbh2 = DBI->connect( $dsn2, $user2, $pw2 );
    if( !defined( $dbh2 ) ) {
    die( "cannot login to database $db <br/>  $DBI::errstr\n" );
    }
    $dbh2->{ LongReadLen } = 50000;
    $dbh2->{ LongTruncOk } = 1;
    return $dbh2;
    

}

############################################################################
# Connect_IMG_Contact (connection for contact and request_account)
############################################################################
sub Connect_IMG_Contact
{
    return Connect_IMG('imgsg');
}

######################################################################
# connect to IMG ER
######################################################################
sub Connect_IMG_ER
{
    return Connect_IMG('imger');
}

######################################################################
# connect to IMG/M ER
######################################################################
sub Connect_IMG_MI
{
    return Connect_IMG('imger');
}

######################################################################
# connect to img_ext
######################################################################
sub Connect_IMG_EXT
{
    return Connect_IMG('imgext');
}


#gets the list of sequencing centers from the database
# returns an array with the aa of the sequencing centers
# a hash with the aa and name of the sequencing center
# the aa of JGI
sub getSeqCenters{
 	my $dbh=&Connect_IMG;
	my $index=1;
 	my $default=1;
 	my @seq_centers;
 	my %seq_centers;
 	
 	push @seq_centers, "other";
	$seq_centers{"other"}='other';
	$default=0;
	
 	my $sql=qq{select distinct seq_center
 			from gold
			where seq_center is not null
			order by seq_center asc
 			};
webLog("$sql\n"); 			
 	my $cur=$dbh->prepare($sql);
 	$cur->execute();
 	while(my ($seq_center)=$cur->fetchrow_array()){
		push @seq_centers,$index;
 		$seq_centers{$index}=$seq_center;
 		#if ($seq_center eq 'JGI') {$default = $index;}
 		$index++;
 	}
 	$cur->finish;
 	$dbh->disconnect();
 	

#	my @seq_centers=('1','2','3','4','5','6','7');
#	my %seq_centers=('1'=>'TIGR','2'=>'JGI',
#				'3'=>'Sanger Institute','4'=>'JellyBean factory',
#				'5'=>'Mickey Mouse','6'=>'Donald Duck',
#				'7'=>'Uncle Benn');
#	my $default=2;
	#make room for 'other'
	
	return(\@seq_centers,\%seq_centers,$default);

}


#########################################################################
# checkAccess
#########################################################################
sub checkAccess{
    my $contact_oid = getContactOid();
    if ( $contact_oid ) {
	return $contact_oid;
    }

    my $username="";
    $username=param('username') if defined(param('username'));

    my $password="";
    $password=param('pwd') if defined(param('pwd'));

    my $md5pwd=md5_base64($password);

    # public/public cannot use submission tool
    if ( blankStr($username) || lc($username) eq 'public' ) {
	return "";
    }

    $username =~ s/'/''/g;    # replace ' with ''
    $md5pwd =~ s/'/''/g;      # replace ' with ''

    my $dbh = Connect_IMG_Contact();

    my $sql=qq{
	select contact_oid, name, super_user
	    from contact 
	    where username = '$username' and password='$md5pwd'
	};
webLog("$sql\n");	
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($c_oid, $name, $isAdmin) =
	$cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( $c_oid ) {
	setSessionParam('contact_oid', $c_oid);
	setSessionParam('isAdmin', $isAdmin);
	setSessionParam('contact_is_admin', $isAdmin);
	setSessionParam('contact_username', $username);
	return $c_oid;
    }

    return "";
}


#########################################################################
# checkHmpAccess
#########################################################################
sub checkHmpAccess{
    my $contact_oid = getContactOid();
    if ( $contact_oid ) {
	return $contact_oid;
    }

    my $username="";
    $username=param('username') if defined(param('username'));

    my $password="";
    $password=param('pwd') if defined(param('pwd'));

    my $md5pwd=md5_base64($password);

    # public/public cannot use submission tool
    if ( blankStr($username) || lc($username) eq 'public' ) {
	return "";
    }

    $username =~ s/'/''/g;    # replace ' with ''
    $md5pwd =~ s/'/''/g;      # replace ' with ''

    my $dbh = Connect_IMG_Contact();

    my $sql=qq{
	select contact_oid, name, super_user
	    from contact 
	    where username = '$username' and password='$md5pwd'
	};
webLog("$sql\n");	
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($c_oid, $name, $isAdmin) =
	$cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( $c_oid ) {
	if ( getIsHmpUser($c_oid) ) {
	    setSessionParam('contact_oid', $c_oid);
	    setSessionParam('contact_username', $username);
	    return $c_oid;
	}
    }

    return "";
}


############################################################################
# encode - Encode base64 
############################################################################
sub encode {
    my( $s ) = @_; 
    my $b64 = encode_base64( $s ); 
    chop $b64; 
    return $b64;
} 


############################################################################ 
# decode - Decode base64 
############################################################################
sub decode { 
    my( $b64 ) = @_; 
    my $s = decode_base64( $b64 );
    return $s; 
} 

############################################################################
# pwDecode - Password decode if encoded. 
############################################################################
sub pwDecode { 
    my( $pw ) = @_;
    my( $tag, @toks ) = split( /:/, $pw );
    if( $tag eq "encoded" ) {
	my $val = join( ':', @toks ); 
	return decode( $val );
    } 
    else { 
	return $pw;
    } 
} 

 
############################################################################ 
# blockRobots - Block robots from using this script. 
#   (robots.txt doesn't always work; so a little brute force ...) 
############################################################################ 
sub blockRobots { 
    ## .htaccess and robots.txt does not work; we force it. 
    my $http_user_agent = $ENV{ HTTP_USER_AGENT }; 
    webLog( "HTTP_USER_AGENT='$http_user_agent'\n" ); 
    my $remote_addr = $ENV{ REMOTE_ADDR }; 
    webLog( "REMOTE_ADDR=$remote_addr\n" ); 
    my $page = param( "page" ); 
    my $bot_patterns = $env->{ bot_patterns }; 
    my $match = 0; 
    if( defined( $bot_patterns ) ) { 
        for my $pattern( @$bot_patterns ) { 
            if( $http_user_agent =~ /$pattern/ ) { 
                $match = 1; 
                last; 
            } 
        }
    }
    my $allow_hosts = $env->{ allow_hosts }; 
 
    if( defined( $allow_hosts ) ) { 
        my @parts0 = split( /\./, $remote_addr );
        my $n = @parts0;
        for my $allow_host( @$allow_hosts ) {
            my @parts1 = split( /\./, $allow_host );
            my $allPartsMatch = 1; 
            for( my $i = 0; $i < $n; $i++ ) {
                my $part0 = $parts0[ $i ];
                my $part1 = $parts1[ $i ];
                if( $part0 ne $part1 && $part1 ne "*" ) {
                    $allPartsMatch = 0;
                    last;
                }
            }
            if( $allPartsMatch ) {
                # Nullify bot pattern match 
                $match = 0; 
                webLog( "'$remote_addr' allowed by '$allow_host' rule\n" );
                last; 
            } 
        } 
    } 
    if( $match && $page ne "home" && $page ne "help" && $page ne "uiMap" ) {
        print header( -status => '403 Forbidden' );
        print "<html>\n"; 
        print "<head>\n"; 
        print "<title>403 Forbidden</title>\n";
        print "</head>\n";
        print "<body>\n"; 
        print "<h1>Forbidden</h1>\n"; 
        print "$http_user_agent.<br/>\n"; 
        print "Bots don't have permission.\n";
        print "</body>\n";
        print "</html>\n";
        webLog( "== Exit for HTTP_USER_AGENT $http_user_agent\n" );
        exit 0;
    } 
}
 

#######################################################################
# StartPage
#######################################################################
sub StartPage{
    print header;

    print start_html(	-title => "IMG Expert Review Submission",
			-style=> "htd/img.css");
    print qq{
	<div id="jgilogo">
	    <a href="http://www.jgi.doe.gov">
	    <img src="$base_url/images/jgi_home_logo.gif" width="169" height="93" border="0" alt="logo" />
	    </a></div>

	    <div id="title">
	    <img src="$base_url/images/jgi_home_header.gif" width="800" height="50" border="0" alt="IMG: Integrated Microbial Genomes" title="IMG: Expert Review Submission." />
	    </div>
	};

    my $t1 = "gl1";
    my $t2 = "gl2";
    my $t3 = "gl3";
    my $t4 = "gl4";

    my $section = getSection();
    if ( $section eq 'Submission' ) {
	$t2 = "gl2on";
    }
    elsif ( $section eq 'Project' ) {
	$t3 = "gl3on";
    }
    elsif ( $section eq 'EnvSample' ) {
	$t4 = "gl4on";
    }
    else {
	$t1 = "gl1on";
    }

    print qq{
	<table cellspacing='0' cellpadding='0' border='0' id='globalLink'>
	    <tr>
	    <td colspan='11' id='spacerrow'></td>
	    </tr>
	    <tr>
	    <td id='$t1'><a href='main.cgi' class='glink'>IMG Home</a></td>
	    <td id='$t2'><a href='main.cgi?section=Submission&page=showPage' class='glink'>Profile</a></td>

	    <td id='$t3'><a href='main.cgi?section=Project&page=showPage' class='glink'>Project</a></td>
	    <td id='$t4'><a href='main.cgi?section=EnvSample&page=Sample' class='glink'>Samples</a></td>
	    <td width='58' class='glink' id='gl0'>&nbsp;</td>
	    </tr>
	    </table>
	};

    print qq{
	    <div id="content">
	};
}


############################################################################
# ClosePage
############################################################################
sub ClosePage{
	print end_html;
	print "</div>";
	
}

############################################################################
# getSection - Get section from other mappings, such as from submit().
############################################################################
sub getSection {
    my $section = param1( "section" );
    if ( $section ) {
        return $section;
    }
 
    # From submit button naming convention
    #  section_<sectionName>_<action>, not URL link.
    my $p = paramMatch( "^_section" );
    my $page = "";
    my $action = "";
    if( $p ne "" ) {
        my( $x, $tag, $val, $s1, $s2 ) = split( /_/, $p );
        ($section, $page) = split(/\:/, $val);
	if ( $s1 ) {
	    $action = $s1;
	}
    }
    ## Force setting.
    param( "section", $section );
    param( "page" , $page);
    if ( $action ) {
	param( "action", $action );
    }
    return $section;
}


sub sanitizeInt
{
        my $int=$_[0];
        if (!defined($int)){$int=0;}
        $int=~/^(\d+)/;
        $int=$1;
        if (!defined($int)){$int=0;}
        return $int;
}

sub sanitizeStr
{
        my $str=$_[0];
        if(!defined($str)){$str="";}
        #only letters, numbers and underscores are allowed
        $str=~/(^[0-9,a-z,A-Z,_]+)/;
        $str=$1;
        if(!defined($str)){$str="";}
        return $str;
}




sub printNote {
	my @strings=@_;
	print p;
	foreach my $string(@strings) {
			print "$string <br>\n";
	}
	print p;
}

sub printError {
	my @strings=@_;
	print p;
	foreach my $string(@strings) {
			print h4("$string <br>\n");
	}
	print p;
}

############################################################################
# printStatusLine - Show status line in the UI.
############################################################################
sub printStatusLine {
    my( $s, $z_index ) = @_;
    my $zidx = 1;
    if( $z_index > 1 || blankStr( $s ) ) {
	$zidx = 2;
    }
    print "<div id='status_line_z$zidx'>\n";
    if( $s =~ /Loading/ || $s =~ /Checking/ ) {
	print "<font color='red'><blink>$s</blink></font>\n";
    }
    else {
	print "$s\n";
    }
    print "</div>\n";
}


sub showErrorPage {
    my ( $msg ) = @_;
##    my $url=url(); 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$main_cgi");
    print "<h4>Error: $msg </h4>"; 
    print end_form(); 
}

sub showInfoPage {
    my ( $msg ) = @_;
##    my $url=url(); 
    print start_form(-name=>'mainForm',-method=>'post',action=>"$main_cgi");
    print "<h4>$msg</h4>"; 
    print end_form(); 
}


sub printHash {
    my ( $h_ref ) = @_;
    my %h = %$h_ref;

    for my $k (keys %h) {
	print "<p>$k ==> " . $h{$k} . "</p>\n";
    }
}

sub dienice
{
        my ($message)=@_;
        print h3($message);
        print end_html();
        exit;
}


############################################################################
# printHomeLink - Home and Logout
############################################################################
sub printHomeLink {
    print hr; 

#    print '<input type="submit" name="userProfile" value="Home"' .
#	' class="smbutton" />';
#    print "&nbsp; \n";
    print '<input type="submit" name="_section_logout" value="Logout" class="smbutton" />';
    print "\n";
    print "<p><a href='http://www.jgi.doe.gov/whoweare/accessibility.html' target='view_link'>Accessibility/Section 508</a>\n";
}


############################################################################
# printHint - Print hint box with message.
############################################################################
sub printHint {
    my( $txt ) = @_;
#    print "<div id='hint'>\n";

    print "<tr>\n";
    print "<th>\n";
    print "<img src='$base_url/images/hint.gif' " . 
       "width='56' height='22' alt='Hint' />";
    print "</th>\n";
    print "<td align='left'>\n";
    print "&nbsp; " . $txt;
    print "</td>\n";
    print "</tr>\n";

#    print "</div>\n";
#    print "<div class='clear'></div>\n";
}

###########################################################################
# blankStr - Is blank string.  Return 1=true or 0=false.
###########################################################################
sub blankStr {
    my $s = shift;

    if ( $s ) {
	if ($s =~ /^[ \t\n]+$/ || $s eq "") {
	    return 1;
	}
	else {
	    return 0;
	}
    }
    else {
	return 1;
    }
}

###########################################################################
# strTrim - Trim string of preceding and ending spaces.
###########################################################################
sub strTrim {
    my $s = shift;

    $s =~ s/^\s+//;
    $s =~ s/\s+$//;
    return $s;
}

############################################################################
# fileSize - return file size of file name
############################################################################
sub fileSize { 
    my ($fileName) = @_; 
    my $rfh = newReadFileHandle( $fileName, "fileSize", 1 ); 
    return 0 if !$rfh; 
    my ( $dev, $ino, $mode, $nlink, $uid, $gid, $rdev, $size, 
	 $atime, $mtime, $ctime, $blksize, $blocks ) = stat($rfh); 
    close $rfh; 
    return $size; 
} 

#############################################################################
# isInt - Is integer.
#############################################################################
sub isInt {
    my $s = shift;
 
    if( $s =~ /^\-{0,1}[0-9]+$/) {
	return 1;
    }
    elsif( $s =~ /^\+{0,1}[0-9]+$/) {
	return 1;
    }
    else {
	return 0;
    }
}

#############################################################################
# isIntList - List of integers?
#############################################################################
sub isIntList {
    my $s = shift;
 
    my @arr = split(/\,/, $s);
    if ( scalar(@arr) == 0 ) {
	return 0;
    }

    for my $a1 ( @arr ) {
	$a1 = strTrim($a1);
	if ( blankStr($a1) ) {
	    return 0;
	}

	if ( length($a1) == 0 ) {
	    return 0;
	}

	if ( ! isInt($a1) ) {
	    return 0;
	}
    }

    return 1;
}

#############################################################################
# isNumber - Is interger, or number with decimal point.
#############################################################################
sub isNumber {
    my $s = shift;

    if( isInt( $s ) ) {
	return 1;
    }
    if( $s =~ /^\-{0,1}[0-9]*\.[0-9]+$/) {
	return 1;
    }
    elsif( $s =~ /^\+{0,1}[0-9]*\.[0-9]+$/) {
	return 1;
    }
    else {
	return 0;
    }
}

#############################################################################
# isDate - DD-MON-YY
#############################################################################
sub isDate {
    my $val = shift;

    # check date 
    my ($d1, $d2, $d3) = split(/\-/, $val);
    if ( blankStr($d1) || blankStr($d2) || blankStr($d3) ||
	 !isInt($d1) || !isInt($d3) ) {
	return 0;
    } 
 
    my @months = ('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 
		  'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC' );
    if ( !inArray(uc($d2), @months) ) {
	return 0;
    } 

    return 1;
}


#############################################################################
# isEmail - is a correct email address?
#############################################################################
sub isEmail {
    my $val = shift;

    if ( blankStr($val) ) {
	return 0;
    }

    if ( $val =~ /([a-zA-Z0-9\_\.\-]+)\@([a-zA-Z0-9\_\.\-]+)/ ) {
	my $e_name = $1;
	my $e_addr = $2;
	if ( blankStr($e_name) || blankStr($e_addr) ) {
	    return 0;
	}

	return 1;
    }
    else {
	return 0;
    }
}


#############################################################################
# removeQuote
#############################################################################
sub removeQuote {
    my $s = shift;

    $s = strTrim($s);

    if ( blankStr($s) ) {
	return $s;
    }

    if ( $s =~ /^\'(.+)\'$/ ) {
	return $1;
    }
    elsif ( $s =~ /^\"(.+)\"$/ ) {
	return $1;
    }

    return $s;
}


############################################################################
# nbsp - "space" character in HTML.
############################################################################
sub nbsp {
    my( $cnt ) = @_;
    my $s;
    for( my $i = 0; $i < $cnt; $i++ ) {
	$s .= "&nbsp; ";
    }
    return $s;
}

############################################################################
# alink - Anchor link.
############################################################################
sub alink {
    my( $url, $text, $target, $isHtmlText ) = @_;
    my $t;
    $t = "target=$target" if $target ne "";
#    $t = "target=$linkTarget" if $linkTarget ne "";

    my $s = "";
    if ( $t ) {
	$s = "<a href='$url' $t>";
    }
    else {
	$s = "<a href='$url'>";
    }

    if( $isHtmlText ) {
	$s .= $text;
    }
    else {
	$s .= escHtml( $text );
    }
    $s .= "</a>";
    return $s;
}


############################################################################
# escHtml - Escape HTML with HTML space. 
############################################################################
sub escHtml { 
    my( $s ) = @_; 
    return nbsp( 1 ) if blankStr( $s );
    return escapeHTML( $s );
} 
 

############################################################################
# param1 - my version of param
############################################################################
sub param1 {
    my ( $p ) = @_;

    if ( defined param($p) ) {
	return param($p);
    }
    else {
	return "";
    }
}

############################################################################
# paramMatch - One of the parameters matchees this substring.
############################################################################
sub paramMatch {
    my( $pattern ) = @_;

    my @all_params = $cgi->param;
    for my $p( @all_params ) {
	if( $p =~ /$pattern/ ) {
	       #my $pval = param( $p );
           #return $pval;
	    return $p;
	}
    }
    return "";
}


############################################################################
# hiddenVar - Hidden variable.
############################################################################
sub hiddenVar {
    my( $tag, $val ) = @_;
    my $val2 = escapeHTML( $val );
    my $s = "<input type='hidden' id='$tag' name='$tag' value='$val2' />\n";
    return $s;
}

############################################################################
# saveAppHiddenVar - save application hidden variables
############################################################################
sub saveAppHiddenVar {
    my @appVar = ( 'contact_oid', 'userstr',
		   'filter:database', 'filter:status',
		   'filter:comp_submission_id',
		   'filter:val_submission_id',
		   'filter:comp_submission_date',
		   'filter:val_submission_date');

    for my $tag ( @appVar ) {
	saveHiddenVar($tag);
    }
}

############################################################################
# saveHiddenVar - save hidden variable with specified tag
############################################################################
sub saveHiddenVar {
    my ( $tag ) = @_;

    my @saved = ();

    if ( param($tag) ) {
	my @vals = param($tag);

	for my $v (@vals) {
	    if ( ! blankStr($v) ) {
		if ( inArray($v, @saved) ) {
		    # already saved
		    next;
		}
		else {
		    push @saved, ( $v );
		    print hiddenVar($tag, $v);
		    # print "<p>*** Save $tag => $v</p>\n";
		}
	    }
	}
    }
}


############################################################################
# saveProjectPageInfo -- save project browsing history
############################################################################
sub saveProjectPageInfo {
    my @pageParams = ( 'browse:goldproj_page_no',
		       'browse:goldproj_orderby',
		       'browse:project_desc' );

    for my $p1 ( @pageParams ) {
	if ( getSessionParam($p1) ) {
	    setSessionParam($p1, getSessionParam($p1));
	}
    }
}

############################################################################
# clearProjectPageInfo -- clear project browsing history
############################################################################
sub clearProjectPageInfo {

    my @pageParams = ( 'browse:goldproj_page_no',
		       'browse:goldproj_orderby',
		       'browse:project_desc' );

    for my $p1 ( @pageParams ) {
	print setSessionParam($p1, '');
    }
}


############################################################################
# saveSubmissionPageInfo -- save submission browsing history
############################################################################
sub saveSubmissionPageInfo {
    my @pageParams = ( 'browse:submission_page_no',
		       'browse:submission_orderby',
		       'browse:submission_desc' );

    for my $p1 ( @pageParams ) {
	if ( getSessionParam($p1) ) {
	    setSessionParam($p1, getSessionParam($p1));
	}
    }
}

############################################################################
# clearSubmissionPageInfo -- clear submission browsing history
############################################################################
sub clearSubmissionPageInfo {
    # set default parameters
    setSessionParam( 'browse:submission_page_no', 1);
    setSessionParam( 'browse:submission_orderby', '');
    setSessionParam( 'browse:submission_desc', 'asc');
}

############################################################################ 
# wunlink - Web version of unlink, check path. 
############################################################################ 
sub wunlink { 
    my( $path ) = @_; 
   ## Sometimes in clustalw the current directory is changed to tmp. 
    $path = checkPath( $path ); 
   #my $fname = lastPathTok( $path ); 
   #my $fname2 = validFileName( $fname ); 
    webLog( "unlink '$path'\n" ); 
    unlink( $path ); 
} 

############################################################################# 
# lastPathTok - Last path token in file path, i.e, the file name. 
############################################################################# 
sub lastPathTok { 
    my( $path ) = @_; 
    my @toks = split( /\//, $path ); 
    my $i; 
    my @toks2; 
    foreach $i( @toks ) { 
        next if $i eq ""; 
        push( @toks2, $i ); 
    } 
    my $nToks = @toks2; 
    return $toks2[$nToks-1]; 
} 

############################################################################
# checkPath - Check path for invalid characters.
############################################################################
sub checkPath {
    my( $path ) = @_;
    ## Catch bad pattern first.
    my @toks = split( /\//, $path );
    for my $t( @toks ) {
	next if $t eq ""; # for double slashes
	if( $t !~ /^[a-zA-Z0-9_\.\-\~]+$/ || $t eq ".." ) {
	    dienice( "checkPath:1: invalid path '$path' tok='$t'\n" );
	}
    }
    ## Untaint.
    $path =~ /([a-zA-Z0-9\_\.\-\/]+)/;
    my $path2 = $1;
    if( $path2 eq "" ) {
	dienice( "checkPath:2: invalid path '$path2'\n" );
	
    }
    return $path2;
}

############################################################################ 
# checkTmpPath - Wrap temp path for safety.  An additional 
#   check for writing (or reading) to (from) temp directory. 
############################################################################ 
sub checkTmpPath { 
    my( $path ) = @_; 
    if( $path !~ /^$tmp_dir/ &&  $path !~ /^$cgi_tmp_dir/ ) { 
      webLog( "checkTmpPath: expected full temp directory " . 
              "'$tmp_dir' or '$cgi_tmp_dir'; got path '$path'\n" ); 
      exit( -1 ); 
  } 
    $path = checkPath( $path ); 
    my $fname = lastPathTok( $path ); 
    my $fname2 = validFileName( $fname ); 
    return $path; 
} 
 

############################################################################
# validPathName - Check path for invalid characters.
############################################################################
sub validPathName {
    my( $path ) = @_;
    ## Catch bad pattern first.
    my @toks = split( /\//, $path );
    for my $t( @toks ) {
	next if $t eq ""; # for double slashes
	if( $t !~ /^[a-zA-Z0-9_\.\-\~]+$/ || $t eq ".." ) {
	    return "";
	}
    }
    ## Untaint.
    $path =~ /([a-zA-Z0-9\_\.\-\/]+)/;
    my $path2 = $1;
    if( $path2 eq "" ) {
	return "";
    }
    return $path2;
}

# 
# file name cannot have the following chars
# .. \ / ~
# 
sub checkFileName { 
    my( $fname ) = @_; 
    if($fname =~ /\\/ || $fname =~ /\.\./ || $fname =~ /\//
       || $fname =~ /~/) {
        #return "bad";
        dienice( "Invalid filename: $fname\n" );
    } 
    return $fname; 
} 


############################################################################
# validFileName - Check for valid file name w/o full path.
############################################################################
sub validFileName {
    my( $fname ) = @_;
    $fname =~ /([a-zA-Z0-9\._]+)/;
    my $fname2 = $1;
    if( $fname2 eq "" ) {
	dienice( "validFileName: invalid file name '$fname'\n" );
	
    }
    return $fname2;
}

############################################################################# 
# str2File - Write string to file. 
############################################################################# 
sub str2File { 
    my ($str, $file) = @_; 
    my $wfh = newWriteFileHandle( $file, "str2File" ); 
    print $wfh $str; 
    close $wfh; 
} 
 
########################################################################### 
# file2Str - Convert file contents to string. 
########################################################################### 
sub file2Str { 
    my $file = shift; 
 
    my $rfh = newReadFileHandle( $file, "file2Str" ); 
    my $line = "";
    my $s = "";
    while ($line = $rfh->getline( ) ) { 
        $s .= $line; 
    } 
    close $rfh; 
    return $s; 
} 


############################################################################
# webLog - Do logging to file. (append)
############################################################################
sub webLog {
    my( $s ) = @_;
    return if (!$verbose);
    my $afh = newAppendFileHandle( $web_log_file, "webLog" );
    print $afh $s;
    close $afh;
}


############################################################################
# webLogNew - Do logging to file. (write)
############################################################################
sub webLogNew {
    my( $s ) = @_;
    return if (!$verbose);
    my $afh = newWriteFileHandle( $web_log_file, "webLog" );
    print $afh $s;
    close $afh;
}


############################################################################
# webErrLog - Do logging to STDERR to file.
############################################################################
sub webErrLog {
    my( $s ) = @_;
    my $afh = newAppendFileHandle( $err_log_file, "webErrLog" );
    print $afh $s;
    close $afh;
}


############################################################################
# newReadFileHandle - Security wrapper for new FileHandle.
############################################################################
sub newReadFileHandle { 
    my( $path, $func, $noExit ) = @_;
 
    $func = "newReadFileHandle" if $func eq "";
    $path = checkPath( $path );
    my $fh = new FileHandle( $path, "r" );
    if( !$fh && !$noExit ) {
	webLog( "$func: cannot read '$path'\n" );
	exit( -1 );
    } 
    return $fh; 
} 


############################################################################
# newWriteFileHandle - Security wrapper for new FileHandle.
############################################################################
sub newWriteFileHandle {
    my( $path, $func, $noExit ) = @_;

    $func = "newWriteFileHandle" if $func eq "";
    $path = checkPath( $path );
    my $fh = new FileHandle( $path, "w" );
    if( !$fh && !$noExit ) {
	webLog( "$func: cannot write '$path'\n" );
	exit( -1 );
    }
    return $fh;
}


############################################################################
# newAppendFileHandle - Security wrapper for new FileHandle.
############################################################################
sub newAppendFileHandle {
    my( $path, $func, $noExit ) = @_;

    $func = "newAppendFileHandle" if $func eq "";
    $path = checkPath( $path );
    my $fh = new FileHandle( $path, "a" );
    if( !$fh && !$noExit ) {
	webLog( "$func: cannot append '$path'\n" );
	exit( -1 );
    }
    return $fh;
}

#############################################################################
# currDateTime - Get current date time string.
#############################################################################
sub currDateTime {
  my $s = sprintf ("%d/%d/%d %d:%d:%d",
    localtime->mon () + 1,
    localtime->mday (),
    localtime->year () + 1900,
    localtime->hour (),
    localtime->min (),
		   localtime->sec ());
  return $s;
}


#############################################################################
# compareDate -- compare date1 and date2
#
# -1: date1 is earlier than date2
# 0: date1 and date2 are the same
# 1: date1 is later than date2
#############################################################################
sub compareDate {
    my ($date1, $date2) = @_;

    # check for null
    if ( blankStr($date1) ) {
	# no date1
	if ( blankStr($date2) ) {
	    return 0;
	}
	else {
	    return -1;
	}
    }
    else {
	if ( blankStr($date2) ) {
	    return 1;
	}
    }

    my %months = (
		  JAN => 1,
		  FEB => 2,
		  MAR => 3,
		  APR => 4,
		  MAY => 5,
		  JUN => 6,
		  JUL => 7,
		  AUG => 8,
		  SEP => 9,
		  OCT => 10,
		  NOV => 11,
		  DEC => 12,
		  );

    # compare two not-null dates
    my ($day1, $mo1, $yr1) = split(/\-/, $date1);
    my ($day2, $mo2, $yr2) = split(/\-/, $date2);

    # compare year
    if ( $yr1 < $yr2 ) {
	return -1;
    }
    elsif ( $yr1 > $yr2 ) {
	return 1;
    }

    # compare month
    my $m1 = 0;
    if ( $months{$mo1} ) {
	$m1 = $months{$mo1};
    }
    my $m2 = 0;
    if ( $months{$mo2} ) {
	$m2 = $months{$mo2};
    }
    if ( $m1 < $m2 ) {
	return -1;
    }
    elsif ( $m1 > $m2 ) {
	return 1;
    }

    # compare day
    if ( $day1 < $day2 ) {
	return -1;
    }
    elsif ( $day1 > $day2 ) {
	return 1;
    }

    return 0;
}


###########################################################################
# PrintAttribute
###########################################################################
sub PrintAttribute {
    my ( $val ) = @_;

    if ( defined($val) ) {
	print "<td class='img' align='left'>$val</td>\n";
    }
    else {
	print "<td class='img' align='left'> </td>\n";
    }
}


###########################################################################
# PrintCell
###########################################################################
sub PrintCell {
    my ( $val, $color ) = @_;

    if ( $val ) {
	print "<td class='img' align='left'>";
	if ( $color ) {
	    print "<font color='$color'>";
	}
	print escapeHTML($val);
	if ( $color ) {
	    print "</font>";
	}
	print "</td>\n";
    }
    else {
	print "<td class='img' align='left'> </td>\n";
    }
}


############################################################################
# inArray - is $val in @arr?
#
# trim leading and trailing blanks
############################################################################
sub inArray {
    my ($val, @arr) = @_;

    $val = strTrim($val);

    for my $i ( @arr ) {
	$i = strTrim($i);

	if ( $val eq $i ) {
	    return 1;
	}
    }

    return 0;
}


############################################################################
# inIntArray - is $val in @arr?
############################################################################
sub inIntArray {
    my ($val, @arr) = @_;

    for my $i ( @arr ) {
	if ( $val == $i ) {
	    return 1;
	}
    }

    return 0;
}

########################################################################### 
# getNCBITaxonInfo 
########################################################################### 
sub getNCBITaxonInfo { 
    my ( $taxon_id ) = @_; 
 
    my %lineage; 
    my @ranks = ( 'superkingdom', 'phylum', 'class', 'order', 
                  'family', 'genus', 'species' ); 
 
    # check taxon id 
    if ( ! isInt($taxon_id) || $taxon_id < 0 ) { 
        for my $r ( @ranks ) { 
            $lineage{$r} = 'Error!!!'; 
        } 
        return %lineage; 
    }
 
    my $env = getEnv(); 
    my $url = $env->{ ncbi_eutils_url } .
        "db=taxonomy&id=" . $taxon_id . "&report=xml";
 
    my $result = get($url); 
#webLog("url: $url\n");    
#webLog("result: $result\n");    
    $result =~ s/&amp;apos;/'/g;   # replace with '
    $result =~ s/&lt;/\</g;
    $result =~ s/&gt;/\>/g;
 
    if ( ! ($result =~ /\<Taxon\>/) ) {
        # not found
        for my $r ( @ranks ) { 
            $lineage{$r} = 'Error!!!';
        } 
        return %lineage; 
    } 

    for my $rank ( @ranks ) {
        if ( $result =~ /\<ScientificName\>(.+)\<\/ScientificName\>\s*\<Rank\>$rank\<\/Rank\>/ ) { 
            $lineage{$rank} = $1;
        } 
        elsif ( $result =~ /\<ScientificName\>(.+)\<\/ScientificName\>\s*\<ParentTaxId\>(\d+)\<\/ParentTaxId\>\s*\<Rank\>$rank\<\/Rank\>/ ) {
            $lineage{$rank} = $1;
        } 
    } 
 
    return %lineage;
} 


############################################################################
# db_findMaxID - find the max ID of a table
############################################################################
sub db_findMaxID { 
    my ($table_name, $attr_name) = @_;
 
    my $dbh = Connect_IMG();

    # SQL statement
    my $sql = "select max($attr_name) from $table_name";
webLog("$sql\n");    
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my $max_id = 0; 
    for (;;) { 
        my ( $val ) = $cur->fetchrow( );
        last if !$val;
 
        # set max ID 
        $max_id = $val; 
    } 

    $cur->finish();
    $dbh->disconnect();

    return $max_id; 
} 


############################################################################ 
# db_getValue - execute a query to get a single value
#
# (only the first value is returned)
############################################################################ 
sub db_getValue { 
    my ($sql) = @_; 

    my $dbh = Connect_IMG();

webLog("300-- $sql\n");
    
    my $cur=$dbh->prepare($sql) or die("$sql $!  $?");
    $cur->execute(); 
 
    my ($val) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect();

    if ( $val ) {
	return $val;
    }

    return "";
} 

############################################################################ 
# db_getValues - execute a query to get set values
############################################################################ 
sub db_getValues { 
    my ($sql) = @_; 

    my $dbh = Connect_IMG();

webLog("310-- $sql\n");
    
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my @vals = ();
    my $cnt = 0;

    for (;;) { 
        my ($val) = $cur->fetchrow( ); 
        last if !$val; 

	if ( $cnt > 10000 ) {
	    last;
	}

	if ( !blankStr($val) ) {
	    push @vals, ( $val );
	}

	$cnt++;
    } 

    $cur->finish(); 
    $dbh->disconnect();

    return @vals;
} 


############################################################################ 
# db_getValues2 - execute a query to get set values
#
# (separate comma-delimited strings to multiple values)
############################################################################ 
sub db_getValues2 { 
    my ($sql) = @_; 

    my $dbh = Connect_IMG();

webLog("320-- $sql\n");
    
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my @vals = ();
    my $cnt = 0;

    for (;;) { 
        my ($val) = $cur->fetchrow( ); 
        last if !$val; 

	if ( $cnt > 10000 ) {
	    last;
	}

	if ( $val =~ /\,/ ) {
	    my @v2 = split(/\,/, $val);
	    for my $s2 ( @v2 ) {
		$s2 = strTrim($s2);
		if ( !blankStr($s2) && !inArray($s2, @vals) ) {
		    push @vals, ( $s2);
		}
	    }
	}
	elsif ( !blankStr($val) && !inArray($val, @vals) ) {
	    push @vals, ( $val );
	}

	$cnt++;
    } 

    $cur->finish(); 
    $dbh->disconnect();

    return @vals;
} 

############################################################################ 
# db_getValuesToString
#
# get set values. return values as a command separated string
############################################################################ 
sub db_getValuesToString { 
    my ($sql) = @_; 

    my $dbh = Connect_IMG();

webLog("330-- $sql\n");

    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my $str = "";
    my $cnt = 0;

    for (;;) { 
        my ($val) = $cur->fetchrow( ); 
        last if !$val; 

	if ( $cnt > 10000 ) {
	    last;
	}

	if ( !blankStr($val) ) {
	    if ( blankStr($str) ) {
		$str = $val;
	    }
	    else {
		$str .= ", " . $val;
	    }
	}

	$cnt++;
    } 

    $cur->finish(); 
    $dbh->disconnect();

    return $str;
} 


############################################################################ 
# db_getSetAttr - get set values.
#
# project_oid: project OID value
# aux_name: set-valued table name 
# t1: field/attr separator 
# t2: record/row separator 
############################################################################ 
sub db_getSetAttr {
    my ($project_oid, $aux_name, $t1, $t2) = @_;

    if ( blankStr($t1) ) {
	$t1 = "|";
    }
    if ( blankStr($t2) ) {
	$t2 = ",";
    }

    my $attr_val = "";

    my $def_aux = RelSchema::def_Class($aux_name); 
    if ( ! $def_aux ) { 
        return $attr_val;
    } 
 
    my $id_attr = $def_aux->{id}; 
    my $sql = "select $id_attr"; 
    my $order_by = " order by $id_attr"; 
 
    my @aux_attrs = @{$def_aux->{attrs}}; 
    for my $attr ( @aux_attrs ) { 
        if ( $attr->{name} eq $id_attr ) { 
            next; 
        } 
 
        $sql .= ", " . $attr->{name}; 
        $order_by .= ", " . $attr->{name}; 
    } 
 
    $sql .= " from " . $aux_name . " where " . $id_attr .
        " = " . $project_oid . $order_by;
    my $dbh = Connect_IMG(); 

webLog("350-- $sql\n");

    my $cur = $dbh->prepare($sql);
    $cur->execute();
 
    my $j = 0; 
    for ($j = 0;$j <= 100000; $j++) {
        my ($id_val, @vals) = $cur->fetchrow_array();
        last if !$id_val; 
 
        if ( $j == 0 ) { 
            # first value
        } 
        else { 
            # print row separator 
	    $attr_val .= $t2;
        } 
 
        for (my $k = 0; $k < scalar(@vals); $k++ ) {
            my $val = $vals[$k]; 
	    $attr_val .= $val;

            if ( $k < scalar(@vals) - 1 ) {
                $attr_val .= $t1;
            } 
        }
    } 
 
    $cur->finish(); 
    $dbh->disconnect();

    return $attr_val;
}


############################################################################ 
# db_getContact - get contact info
#
############################################################################ 
sub db_getContact { 
    my ($c_id) = @_; 

    if ( ! $c_id ) {
	return "";
    }

    my $contact_info = "";
    
    my $dbh = Connect_IMG_Contact();

    my $sql = "select contact_oid, username, email from contact " .
	"where contact_oid = $c_id";
	webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my ($oid, $name, $email) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect();

    if ( $oid ) {
	   $contact_info = $name . " (" . $email . ")";
    }

    return $contact_info;
} 


############################################################################ 
# db_getContactName - get contact name
#
############################################################################ 
sub db_getContactName { 
    my ($c_id) = @_; 

    if ( ! $c_id ) {
	return "";
    }

    my $contact_info = "";
    
    my $dbh = Connect_IMG_Contact();

    my $sql = "select contact_oid, username from contact " .
	"where contact_oid = $c_id";
	webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my ($oid, $name) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect();

    if ( $oid ) {
	    $contact_info = $name;
    }

    return $contact_info;
} 

############################################################################ 
# db_getContactEmail - get contact email
#
############################################################################ 
sub db_getContactEmail { 
    my ($c_id) = @_; 

    if ( ! $c_id ) {
	return "";
    }

    my $contact_info = "";
    my $dbh = Connect_IMG_Contact();

    my $sql = "select contact_oid, email from contact " .
	"where contact_oid = $c_id";
	webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute(); 
 
    my ($oid, $email) = $cur->fetchrow( ); 
    $cur->finish(); 
    $dbh->disconnect();

    if ( $oid ) {
	$contact_info = $email;
	
    }

    return $contact_info;
} 


############################################################################
# db_sqlTrans - perform an SQL transaction
############################################################################
sub db_sqlTrans {
    my ( $sqlList_ref ) = @_;

    # login
    my $dbh = Connect_IMG();
    $dbh->{ AutoCommit } = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for my $sql ( @$sqlList_ref ) { 
	    $last_sql++;

webLog("340-- $sql\n");

	    my $cur = $dbh->prepare( $sql ) ||
		dienice( "execSql: cannot preparse statement: $DBI::errstr\n" );
	    $cur->execute( ) ||
		dienice( "execSql: cannot execute: $DBI::errstr\n" );
        }
    }; 

    if ($@) {
	$dbh->rollback();
	$dbh->disconnect();
	return $last_sql;
    }

    $dbh->commit();
    $dbh->disconnect();

    return 0;
}


############################################################################
# db_con_sqlTrans - perform an SQL transaction in img contact
############################################################################
sub db_con_sqlTrans {
    my ( $sqlList_ref ) = @_;

    # login
    my $dbh = Connect_IMG_Contact();
    $dbh->{ AutoCommit } = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for my $sql ( @$sqlList_ref ) { 
	    $last_sql++;
webLog("$sql\n");
	    my $cur = $dbh->prepare( $sql ) ||
		dienice( "execSql: cannot preparse statement: $DBI::errstr\n" );
	    $cur->execute( ) ||
		dienice( "execSql: cannot execute: $DBI::errstr\n" );
        }
    }; 

    if ($@) {
	$dbh->rollback();
	$dbh->disconnect();
	return $last_sql;
    }

    $dbh->commit();
    $dbh->disconnect();

    return 0;
}

############################################################################
# db_er_sqlTrans - perform an SQL transaction in img/er
############################################################################
sub db_er_sqlTrans {
    my ( $sqlList_ref ) = @_;

    # login
    my $dbh = Connect_IMG_ER();
    $dbh->{ AutoCommit } = 0;

    my $last_sql = 0;

    # perform database update
    eval {
        for my $sql ( @$sqlList_ref ) { 
	    $last_sql++;
webLog("$sql\n");
	    my $cur = $dbh->prepare( $sql ) ||
		dienice( "execSql: cannot preparse statement: $DBI::errstr\n" );
	    $cur->execute( ) ||
		dienice( "execSql: cannot execute: $DBI::errstr\n" );
        }
    }; 

    if ($@) {
	$dbh->rollback();
	$dbh->disconnect();
	return $last_sql;
    }

    $dbh->commit();
    $dbh->disconnect();

    return 0;
}


##########################################################################
# getIsAdmin - whether the user is an administrator
##########################################################################
sub getIsAdmin {
    my ($contact_oid) = @_;

    if($contact_oid == 3038) {
       #return 'No'; 
    }

    my $same_user = 0;
    my $adm = '';

    if ( getSessionParam ('contact_oid') == $contact_oid ) {
	# same user
	$same_user = 1;
	$adm = getSessionParam ('contact_is_admin' );
	if ( $adm ) {
	    return $adm;
	}
    }

    my $dbh = Connect_IMG_Contact();

    my $sql=qq{
	select contact_oid, super_user
	    from contact 
	    where contact_oid = $contact_oid
	};
	webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($c_oid, $isAdmin) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if(defined($c_oid)){
	return $isAdmin;
    }

    if ( blankStr($isAdmin) ) {
	$isAdmin = 'No';
    }

    if ( $same_user && blankStr($adm) ) {
	setSessionParam('contact_is_admin', $isAdmin);
    }

    return $isAdmin;
}


##########################################################################
# getIsJgiUser - whether the user is a JGI user
##########################################################################
sub getIsJgiUser {
    my ($contact_oid) = @_;

    my $dbh = Connect_IMG_Contact();

    my $sql=qq{
	select contact_oid, jgi_user
	    from contact 
	    where contact_oid = $contact_oid
	};
	webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($c_oid, $isJgiUser) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if(defined($c_oid)){
	   return $isJgiUser;
    }

    return 'No';
}


############################################################################ 
# getIsHmpUser
############################################################################ 
sub getIsHmpUser {
    my( $contact_oid ) = @_; 
    return 0 if $contact_oid eq ""; 

    if ( getIsAdmin($contact_oid) eq 'Yes' ) {
	return 1;
    }

    my $dbh = Connect_IMG_Contact();
 
    my $sql = qq{ 
       select img_editing_level 
       from contact 
       where contact_oid = $contact_oid 
   }; 
   webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my( $editing_level ) = $cur->fetchrow( ); 
    $cur->finish( ); 
    $dbh->disconnect();

    if ( ! $editing_level ) { 
        return 0; 
    } 
    elsif ( $editing_level =~ /hmp\-user/ ) { 
        return 1; 
    } 
 
    return 0; 
} 

############################################################################ 
# getIsHmProject
############################################################################ 
sub getIsHmpProject {
    my( $project_oid ) = @_; 

    if ( ! $project_oid ) {
	return 0;
    }

    my $dbh = Connect_IMG();
 
    my $cond = getHmpCond();
    my $sql = qq{ 
       select p.project_oid
       from project_info p
       where project_oid = $project_oid
       and $cond
   }; 
webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my( $id0 ) = $cur->fetchrow( ); 
    $cur->finish( ); 
    $dbh->disconnect();

    if ( $id0 ) {
        return 1; 
    } 
 
    return 0; 
} 

############################################################################ 
# getCanEditImgGold
############################################################################ 
sub getCanEditImgGold {
    my( $contact_oid ) = @_; 
    return 0 if $contact_oid eq ""; 

    if ( getIsAdmin($contact_oid) eq 'Yes' ) {
	return 1;
    }

    my $dbh = Connect_IMG_Contact();
 
    my $sql = qq{ 
       select img_editing_level 
       from contact 
       where contact_oid = $contact_oid 
   }; 
   webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my( $editing_level ) = $cur->fetchrow( ); 
    $cur->finish( ); 
    $dbh->disconnect();

    if ( ! $editing_level ) { 
        return 0; 
    } 
    elsif ( $editing_level =~ /img\-gold/ ) { 
        return 1; 
    } 
 
    return 0; 
} 


############################################################################ 
# getCanUpdateProject
############################################################################ 
sub getCanUpdateProject {
    my( $contact_oid, $project_oid ) = @_; 
    return 0 if $contact_oid eq ""; 
    return 0 if ! $project_oid;

    if ( getIsAdmin($contact_oid) eq 'Yes' ) {
	return 1;
    }

    my $dbh = Connect_IMG_Contact();
 
    my $sql = qq{ 
       select img_editing_level 
       from contact 
       where contact_oid = $contact_oid 
   }; 
   webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my( $editing_level ) = $cur->fetchrow( ); 
    $cur->finish( ); 
    $dbh->disconnect();

    if ( ! $editing_level ) { 
	# no privilege
    } 
    elsif ( $editing_level =~ /img\-gold/ ) { 
        return 1; 
    } 

    my $cnt = db_getValue("select count(*) from project_info p where p.project_oid = $project_oid and (p.contact_oid = $contact_oid or p.project_oid in (select cpp.project_permissions from contact_project_permissions cpp where cpp.contact_oid = $contact_oid))");
    if ( $cnt ) {
	return 1;
    }

    return 0; 
} 


############################################################################ 
# isSubmissionAdmin
############################################################################ 
sub isSubmissionAdmin {
    my( $contact_oid ) = @_; 
    return 'No' if $contact_oid eq ""; 

    if ( getIsAdmin($contact_oid) eq 'Yes' ) {
	return 'Yes';
    }

    my $dbh = Connect_IMG_Contact();
 
    my $sql = qq{ 
       select img_editing_level 
       from contact 
       where contact_oid = $contact_oid 
   }; 
   webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my( $editing_level ) = $cur->fetchrow( ); 
    $cur->finish( ); 
    $dbh->disconnect();

    if ( ! $editing_level ) { 
	# no privilege
    } 
    elsif ( $editing_level =~ /img\-submit/ ) { 
        return 'Yes'; 
    } 

    return 'No';
}


##########################################################################
# getUploadDir
##########################################################################
sub getUploadDir {
    return "/home/img2/imachen/submission";
}


##########################################################################
# getAttrDispName
##########################################################################
sub getAttrDispName {
    my ( $attr ) = @_;

    if ( $attr eq 'submission_id' ) {
	return 'Submission ID';
    }
    elsif ( $attr eq 'project_info' ) {
	return 'ER Submission Project ID';
    }
    elsif ( $attr eq 'analysis_project_id' ) {
	return 'Analysis Project ID';
    }
    elsif ( $attr eq 'display_name' ) {
	return 'Project Name';
    }
    elsif ( $attr eq 'seq_status' ) {
	return 'Seq Status';
    }
    elsif ( $attr eq 'gold_stamp_id' ) {
	return 'GOLD ID';
    }
    elsif ( $attr eq 'img_taxon_oid' ) {
	return 'IMG Taxon OID';
    }
    elsif ( $attr eq 'submission_date' ) {
	return 'Submission Date';
    }
    elsif ( $attr eq 'status' ) {
	return 'Status';
    }
    elsif ( $attr eq 'approval_status' ) {
	return 'Approval Status';
    }
    elsif ( $attr eq 'img_dev_flag' ) {
	return 'Submitted to Development DB?';
    }
    elsif ( $attr eq 'gene_calling_flag' ) {
	return 'Gene Calling?';
    }
    elsif ( $attr eq 'img_product_flag' ) {
	return 'Product Name Assignment?';
    }

    return $attr;
}


##########################################################################
# getProjectLink
##########################################################################
sub getProjectLink {
    my ($p_oid) = @_;

    my $link = "<a href='" . $main_cgi .
	"?section=ProjectInfo&page=displayProject" .
	"&project_oid=$p_oid' >" . $p_oid . "</a>";

    return $link;
}

##########################################################################
# getMProjectLink
##########################################################################
sub getMProjectLink {
    my ($p_oid) = @_;

    my $link = "<a href='" . $main_cgi .
	"?section=MProjectInfo&page=displayProject" .
	"&project_oid=$p_oid' >" . $p_oid . "</a>";

    return $link;
}

##########################################################################
# getProjectOrderByLink
##########################################################################
sub getProjectOrderByLink {
    my ($attr, $label, $desc) = @_;

    my $link = "<a href='" . $main_cgi .
	"?section=ProjectInfo&page=showProject&project_orderby=$attr";
    if ( $desc ) {
	$link .= "&project_desc=desc";
    }
    else {
	$link .= "&project_desc=asc";
    }
    $link .= "project_page_no=1' >" . escapeHTML($label) . "</a>";

    return $link;
}

##########################################################################
# getSampleLink
##########################################################################
sub getSampleLink {
    my ($s_oid) = @_;

    my $link = "<a href='" . $main_cgi .
	"?section=EnvSample&page=displaySample" .
	"&sample_oid=$s_oid' >" . $s_oid . "</a>";

    return $link;
}

##########################################################################
# getSampleOrderByLink
##########################################################################
sub getSampleOrderByLink {
    my ($attr, $label, $desc) = @_;

    my $link = "<a href='" . $main_cgi .
	"?section=EnvSample&page=showSamples&sample_orderby=$attr";
    if ( $desc ) {
	$link .= "&sample_desc=desc";
    }
    else {
	$link .= "&sample_desc=asc";
    }
    $link .= "&sample_page_no=1' >" . escapeHTML($label) . "</a>";

    return $link;
}

##########################################################################
# getSubmissionLink
##########################################################################
sub getSubmissionLink {
    my ($s_oid, $database) = @_;

    my $section = 'ERSubmission';
    if ( $database eq 'IMG/M ER' ) {
	$section = 'MSubmission';
    }

    my $link = "<a href='" . $main_cgi .
	"?section=$section&page=displaySubmission" .
	"&submission_id=$s_oid' >" . $s_oid . "</a>";

    return $link;
}

##########################################################################
# getSubmissionOrderByLink
# sort: 1: asc, 0: not sorted, -1: desc
##########################################################################
sub getSubmissionOrderByLink {
    my ($attr, $label, $link_info, $database, $sort) = @_;

    my $section = "ERSubmission";
    if ( $database eq 'IMG/M ER' ) {
	$section = "MSubmission";
    }

    if ( param1('section') && param1('section') eq 'SubmitStats' ) {
	$section = 'SubmitStats';
    }

    my $sort_icon = $base_url . "/images/sort_icon.png";
    my $up_icon = $base_url . "/images/up_icon.png";
    my $down_icon = $base_url . "/images/down_icon.png";

    my $sort_title = "click to sort in ascending order"; 

    my $link = escapeHTML($label) . 
	" <a href='" . $main_cgi . "?" . $link_info;
    $link .= "&submission_orderby=$attr";
    my $icon2 = $sort_icon;

    if ( $sort < 0 ) {
	$link .= "&submission_desc=asc";
	$icon2 = $up_icon;
    }
    elsif ( $sort > 0 ) {
	$link .= "&submission_desc=desc";
	$icon2 = $down_icon;
	$sort_title = "click to sort in descending order";
    }
    else {
	$link .= "&submission_desc=asc";
    }

    $link .= "&submission_page_no=1'>" .
	"<img src='" . $icon2 .
	"' width='11' height='11' border='0'" .
	" alt='$sort_title' title='$sort_title' />" . "</a>";

    return $link;
}

##########################################################################
# getContactLink
##########################################################################
sub getContactLink {
    my ($c_oid, $c_name) = @_;

    my $link = "<a href='" . $main_cgi .
	"?section=UserTool&page=showContactInfo" .
	"&selected_contact_oid=$c_oid' >" .
	escapeHTML($c_name) . "</a>";

    return $link;
}

##########################################################################
# getGoldLink
##########################################################################
sub getGoldLink {
    my ($gold_id) = @_;

#    my $gold_url = "http://genomesonline.org/cgi-bin/GOLD/bin/GOLDCards.cgi?goldstamp=$gold_id"; 
    my $gold_url = "https://gold.jgi.doe.gov/";
    if ( $gold_id =~ /Gs/ ) {
	$gold_url .= "study?id=" . $gold_id;
    }
    elsif ( $gold_id =~ /Gp/ ) {
	$gold_url .= "projects?id=" . $gold_id;
    }
    elsif ( $gold_id =~ /Ga/ ) {
	$gold_url .= "analysis_projects?id=" . $gold_id;
    }

    my $gold_link = "<a href='" . $gold_url . "' target='view_link'>" . 
	$gold_id . "</a>"; 

    return $gold_link;
}

##########################################################################
# getGoldDbUrl
##########################################################################
sub getGoldDbUrl {

    my $gold_db_url = "<a href='" . $env->{ gold_db_url } .
	"' target='view_link'>" . "gold.db" . "</a>"; 

    return $gold_db_url;
}

##########################################################################
# getGoldLogUrl
##########################################################################
sub getGoldLogUrl {

    my $gold_log_url = "<a href='" . $env->{ gold_log_url } .
	"' target='view_link'>" . "Log" . "</a>"; 

    return $gold_log_url;
}

##########################################################################
# getHomdLink
##########################################################################
sub getHomdLink {
    my ($homd_id) = @_;

    my $h_id = '';
    if ( $homd_id =~ /(\d+)/ ) {
	$h_id = $1;
    }

    if ( length($h_id) > 0 ) {
	my $homd_url = "http://www.homd.org/taxon="; 
	my $homd_link = "<a href='" . $homd_url . 
	    $h_id . "' target='view_link'>" . 
	    $homd_id . "</a>"; 

	return $homd_link;
    }
    else {
	return $homd_id;
    }
}

##########################################################################
# getImgGoldLink
##########################################################################
sub getImgGoldLink {

    my $img_gold_url = "$base_cgi_url/gold.cgi";
    my $img_gold_link = "<a href='" . $img_gold_url . 
	"' target='view_link'>" . "IMG-GOLD" . "</a>"; 

    return $img_gold_link;
}

##########################################################################
# getImgERUrl
##########################################################################
sub getImgERUrl {

#    my $img_er_url = "http://imgweb.jgi-psf.org/cgi-bin/img_er_v280/main.cgi";
    my $img_er_url = "https://img.jgi.doe.gov/mer/";

    return $img_er_url;
}

##########################################################################
# getImgMIUrl
##########################################################################
sub getImgMIUrl {

    my $img_mi_url = "https://img.jgi.doe.gov/mer/";

    return $img_mi_url;
}


##########################################################################
# getGoldLink
##########################################################################
sub getImgHmpLink {
    my ($taxon_oid) = @_;

    my $img_hmp_base_url = $base_cgi_url;
    $img_hmp_base_url =~ s/img\_er\_submit/img\_hmp/;
    my $img_hmp_url = "$img_hmp_base_url/main.cgi?section=TaxonDetail&page=taxonDetail&taxon_oid=";

    my $img_hmp_link = "<a href='" . $img_hmp_url . 
	$taxon_oid . "' target='view_link'>" . 
	$taxon_oid . "</a>"; 

    return $img_hmp_link;
}


##########################################################################
# getNcbiProjLink
##########################################################################
sub getNcbiProjLink {
    my ($ncbi_proj_id) = @_;

    if ( blankStr($ncbi_proj_id) ) {
	return "";
    }

    my $ncbi_proj_url = "http://www.ncbi.nlm.nih.gov/sites/entrez?Db=genomeprj&cmd=ShowDetailView&TermToSearch=";
    my $ncbi_proj_link = "<a href='" . $ncbi_proj_url . 
	$ncbi_proj_id . "' target='view_link'>" . 
	$ncbi_proj_id . "</a>"; 

    return $ncbi_proj_link;
}

##########################################################################
# getNcbiTaxonLink
##########################################################################
sub getNcbiTaxonLink {
    my ($ncbi_taxon_id) = @_;

    if ( blankStr($ncbi_taxon_id) ) {
	return "";
    }

    my $ncbi_taxon_url = "http://www.ncbi.nlm.nih.gov/" .
	"sites/entrez?db=taxonomy&cmd=Search&dopt=DocSum&term=" .
	$ncbi_taxon_id . "[uid]";
    my $ncbi_taxon_link = "<a href='" . $ncbi_taxon_url . 
	"' target='view_link'>" . 
	$ncbi_taxon_id . "</a>"; 

    return $ncbi_taxon_link;
}


##########################################################################
# getGreengenesLink
##########################################################################
sub getGreengenesLink {
    my ($g_id) = @_;

    if ( blankStr($g_id) ) {
	return "";
    }

    my $greengenes_url = "http://greengenes.lbl.gov/cgi-bin/" .
	"show_one_record_v2.pl?prokMSA_id=" . $g_id;

    my $greengenes_link = "<a href='" . $greengenes_url . 
	"' target='view_link'>" . 
	$g_id . "</a>"; 

    return $greengenes_link;
}


##########################################################################
# sendEmail - send email
##########################################################################
sub sendEmail {
    my ($mail_from, $mail_to, $subj, $data) = @_;

    if ( blankStr($mail_from) || blankStr($mail_to) ) {
	return;
    }

    my $SMTP_SERVER = 'smtp.lbl.gov'; 
 
    my %mail = ( To      => $mail_to,
		 From    => $mail_from,
		 Subject => $subj,
		 Message => $data
		 );
    $mail{smtp} = $SMTP_SERVER;
    sendmail(%mail);
}

##########################################################################
# printTooltip
##########################################################################
sub printTooltip {
    my ($disp_name, $tooltip) = @_;

    print "<a href=\"#\" onmouseover=\"return escape('<div>" .
	escapeHTML($tooltip) . "</div>')\"> ";
    print escapeHTML($disp_name); 
    print "</a>\n";

    printTooltipScript();
}


##########################################################################
# printTooltipScript
##########################################################################
sub printTooltipScript {
    my $base_url = $env->{ base_url };
    print "<script type=\"text/javascript\" " .
	"src=\"" . $base_url . "/wz_tooltip.js\">" .
	"</script>\n";
}


##########################################################################
# getNewCounterVal
##########################################################################
sub getNewCounterVal {
    my ($counter_id) = @_;

    if ( blankStr($counter_id) ) {
	return "";
    }

    my $dbh = Connect_IMG();

    my $sql = "select max(gold_stamp_id) from project_info" .
	" where gold_stamp_id like '" . $counter_id . "%'";
	webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($id0) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( blankStr($id0) || length($id0) < 3 ) {
	return "";
    }
    my $val0 = substr($id0, 2, length($id0)-2);
    if ( ! isInt($val0) ) {
	return "";
    }
    if ( $val0 <= 0 || $val0 >= 99999 ) {
	return "";
    }

    $val0++;
    my $new_id = $counter_id . sprintf("%05d", $val0);
}


##########################################################################
# printCellTooltip
##########################################################################
sub printCellTooltip {
    my ($disp_name, $tooltip, $size) = @_;

    print "  <th class='subhead' align='right' ";
    if ( $size ) {
#	print "width='70' ";
    }
    if ( $tooltip ) {
	print " title='" . escapeHTML($tooltip) . "'>";

	# print "<a href='#'>$disp_name</a>\n";
	print "<a>$disp_name</a>\n"; 
    }
    elsif ( $disp_name ) {
	print ">" . escapeHTML($disp_name);
    }
    else {
	print ">\n";
    }

    print "</th>\n";
}


##########################################################################
# getNewGcatId
##########################################################################
sub getNewGcatId {

    my $dbh = Connect_IMG();

    my $sql = "select max(gcat_id) from project_info";
    webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($id0) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( blankStr($id0) || length($id0) < 6 ) {
	return "";
    }
    my $val0 = substr($id0, 0, 6);
    if ( ! isInt($val0) ) {
	return "";
    }
    if ( $val0 <= 0 || $val0 >= 999999 ) {
	return "";
    }

    $val0++;
    my $new_id = sprintf("%06d", $val0) . "_GCAT";

    return $new_id;
}


##########################################################################
# getNewHmpId
##########################################################################
sub getNewHmpId {

    my $dbh = Connect_IMG();

    my $sql = "select max(hmp_id) from project_info";
    webLog("$sql\n");
    my $cur=$dbh->prepare($sql);
    $cur->execute();
    my($id0) = $cur->fetchrow_array();
    $cur->finish();
    $dbh->disconnect();

    if ( blankStr($id0) ) {
	return 9000;
    }

    $id0++;
    if ( $id0 < 9000 ) {
	return 9000;
    }

    return $id0;
}


##########################################################################
# DisplaySetAttr 
# 
# aux_name: set-valued table name
# t1: field/attr separator
# t2: record/row separator
##########################################################################
sub DisplaySetAttr { 
    my ($oid, $aux_name, $t1, $t2) = @_;

    require RelSchema;
    my $def_aux = RelSchema::def_Class($aux_name); 
    if ( ! $def_aux ) { 
        return; 
    } 
 
    my $id_attr = $def_aux->{id};
    my $sql = "select $id_attr";
    my $order_by = " order by $id_attr"; 
 
    my @aux_attrs = @{$def_aux->{attrs}};
    for my $attr ( @aux_attrs ) {
        if ( $attr->{name} eq $id_attr ) {
            next; 
        } 
 
        $sql .= ", " . $attr->{name};
        $order_by .= ", " . $attr->{name};
    } 
 
    $sql .= " from " . $aux_name . " where " . $id_attr .
        " = " . $oid . $order_by;
    my $dbh = Connect_IMG();
    webLog("$sql\n");
    my $cur = $dbh->prepare($sql);
    $cur->execute();
 
    my $j = 0; 
    for ($j = 0;$j <= 100000; $j++) {
        my ($id_val, @vals) = $cur->fetchrow_array();
        last if !$id_val;
 
        if ( $j == 0 ) {
            # first value
            print "<tr class='img' >\n";
            print "  <th class='subhead' align='right'>" . 
                $def_aux->{display_name} . "</th>\n";
            print "  <td class='img'   align='left'>";
        } 
        else { 
            # print row separator
            print $t2; 
        } 
 
        for (my $k = 0; $k < scalar(@vals); $k++ ) {
            my $val = $vals[$k]; 
 
            if ( $val =~ /^http\:\/\// ||
                 $val =~ /^https\:\/\// ) {
                # url
                print alink($val, 'URL', 'target', 1);
            } 
            else { 
                print escapeHTML($val);
            } 
            if ( $k < scalar(@vals) - 1 ) {
                print $t1; 
            } 
        } 
    } 

 
    if ( $j > 0 ) { 
	print "</td></tr>\n";
    }
 
    $cur->finish(); 
    $dbh->disconnect(); 
} 


#########################################################################
# getHmpCond - get HMP project query condition
##########################################################################
sub getHmpCond { 
    my ($pref) = @_;

    if ( blankStr($pref) ) {
	$pref = 'p';
    }

    my $cond = "$pref.project_oid in (select p2.project_oid from project_info p2 where p2.show_in_dacc = 'Yes')"; 
    return $cond; 
} 
 

#########################################################################
# getProjCond - get IMG-GOLD genome project query condition
##########################################################################
sub getProjCond { 
    my ($pref, $metag) = @_;

    if ( blankStr($pref) ) {
	$pref = 'p';
    }

    my $cond = "$pref.domain != 'MICROBIAL'";

    # metagenome project?
    if ( $metag ) {
	$cond = "$pref.domain = 'MICROBIAL'";
    }

    return $cond; 
} 



#########################################################################
# GetMetagenomeClassification - get IMG-GOLD genome project query condition
##########################################################################
sub GetMetagenomeClassification { 
    my ($pref, $metag) = @_;

    if ( blankStr($pref) ) {
	$pref = 'p';
    }

    my $cond = "$pref.domain != 'MICROBIAL'";

    # metagenome project?
    if ( $metag ) {
	$cond = "$pref.domain = 'MICROBIAL'";
    }

    return $cond; 
} 


################################################################
# getNewImgGoldMetadata: call GOLD API (GET)
################################################################
sub getNewImgGoldMetadata {
    my ($gold_id, $type) = @_;

    if ( ! $gold_id ) {
	return "";
    }

    if ( ! $type ) {
	if ( $gold_id =~ /Ga/ ) {
	    $type = 'analysis_project';
	}
	elsif ( $gold_id =~ /Gp/ ) {
	    $type = 'sequencing_project';
	}
	elsif ( $gold_id =~ /Gs/ ) {
	    $type = 'study';
	}
    }

    if ( ! $type ) {
	return "";
    }

    if ( $type eq 'analysis_project' ) {
	my %gold2img = (
	    "itsAnalysisProjectId" => "its_analysis_project_id",
	    "itsAssemblyAtId" => "its_assembly_at_id",
	    "itsAnnotationAtId" => "its_annotation_at_id",
	    "analysisProjectName" => "analysis_project_name",
	    "itsAnalysisProjectName" => "its_analysis_project_name",
	    "analysisProductName" => "analysis_product_name",
	    "ncbiTaxonId" => "ncbi_tax_id",
	    "visibility" => "visibility",
	    "status" => "status",
	    "domain" => "domain",
	    "ncbiPhylum" => "ncbi_phylum",
	    "ncbiClass" => "ncbi_class",
	    "ncbiOrder" => "ncbi_order",
	    "ncbiFamily" => "ncbi_family",
	    "ncbiGenus" => "ncbi_genus",
	    "ncbiSpecies" => "ncbi_species",
	    "genus" => "genus",
	    "species" => "species",
	    "goldPhylogeny" => "gold_phylogeny",
	    "goldAnalysisProjectType" => "gold_analysis_project_type",
	    "genomeType" => "genome_type",
	    "imgTaxonOid" => "img_dataset_id",
	    "submissionType" => "submission_type",
	    "studyGoldId" => "study_gold_id",
	    "goldProposalName" => "gold_proposal_name",
	    "ecosystem" => "ecosystem",
	    "ecosystemCategory" => "ecosystem_category",
	    "ecosystemType" => "ecosystem_type",
	    "ecosystemSubtype" => "ecosystem_subtype",
	    "specificEcosystem" => "specific_ecosystem",
	    "piName" => "pi_name",
	    "piEmail" => "pi_email",
	    "submitterContactOid" => "submitter_contact_oid",
	    "calibanId" => "caliban_id",
	    "submitterEmail" => "submitter_email",
	    "specimen" => "specimen",
	    "comments" => "comments",
	    "submissionId" => "submission_id",
	    "isAssembled" => "is_assembled",
	    "isDecontamination" => "is_decontamination",
	    "isGenePrimp" => "is_gene_primp",
	    "contigCount" => "contig_count",
	    "scaffoldCount" => "scaffold_count",
	    "locusTag" => "locus_tag",
	    "cultured" => "cultured", 
	    "jgiSequenced" => "jgi_sequenced",
	    "referenceGoldId" => "reference_gold_id",
	    "reviewStatus" => "review_status", 
	    "assemblyMethod" => "assembly_method", 
	    "sequencingDepth" => "sequencing_depth",
	    "imgUse" => "img_use"
	    );
	my @flds = (keys %gold2img);
	my $dbh=WebFunctions::Connect_IMG;
	my $sql = "select gold_id";
	for my $s2 ( @flds ) {
	    $sql .= ", " . $gold2img{$s2};
	}
	$sql .= " from gold_analysis_project where gold_id = ? ";
	my $cur=$dbh->prepare($sql);
	$cur->execute($gold_id);
	my ( $id2, @rest ) = $cur->fetchrow_array();
	$cur->finish();

	if ( $id2 ) {
	    my $res = "{\"goldId\":\"$id2\"";
	    my $j = 0;
	    for my $s2 ( @flds ) {
		$res .= ",\"$s2\":";
		if ( $j < scalar(@rest) ) {
		    my $fld_val = $rest[$j];
		    if ( length($fld_val) == 0 ) {
			$res .= "null";
		    }
		    elsif ( isInt($fld_val) ) {
			$res .= $rest[$j];
		    }
		    else {
			$res .= '"' . $rest[$j] . '"';
		    }
		    $j++;
		}
	    }

	    ## genbanks
	    $res .= ",\"genbanks\":[";
	    my $is_first = 1;
	    $sql = "select gold_id, genbank_id, assembly_accession from gold_analysis_project_genbanks where gold_id = ?";
	    $cur=$dbh->prepare($sql);
	    $cur->execute($gold_id);
	    for (;;) {
		my ( $id3, $genbank_id, $acc ) = $cur->fetchrow_array();
		last if ! $id3;

		if ( $is_first ) {
		    $is_first = 0;
		}
		else {
		    $res .= ",";
		}

		$res .= "{\"genbankId\":";
		if ( $genbank_id ) {
		    $res .= '"' . $genbank_id . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"assemblyAccession\":";
		if ( $acc ) {
		    $res .= '"' . $acc . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= "}";
	    }
	    $cur->finish();
	    $res .= "]";

	    ## projects
	    $res .= ",\"projects\":[";
	    my $is_first = 1;
	    $sql = "select gold_id, its_spid, project_oid, sample_oid, sp_gold_id, pmo_project_id from gold_analysis_project_lookup2 where gold_id = ?";
	    $cur=$dbh->prepare($sql);
	    $cur->execute($gold_id);
	    for (;;) {
		my ( $id3, $its_spid, $pid, $sid, $sp_gold_id, $pmo_id ) = $cur->fetchrow_array();
		last if ! $id3;

		if ( $is_first ) {
		    $is_first = 0;
		}
		else {
		    $res .= ",";
		}

		$res .= "{\"itsSpid\":";
		if ( $its_spid ) {
		    $res .= '"' . $its_spid . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"pmoProjectId\":";
		if ( $pmo_id ) {
		    $res .= $pmo_id;
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"projectOid\":";
		if ( $pid ) {
		    $res .= $pid;
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"sampleOid\":";
		if ( $sid ) {
		    $res .= $sid;
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"goldId\":";
		if ( $sp_gold_id ) {
		    $res .= '"' . $sp_gold_id . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= "}";
	    }
	    $cur->finish();
	    $res .= "]";

	    ## users
	    $res .= ",\"users\":[";
	    my $is_first = 1;
	    $sql = "select gold_id, name, email, role, caliban_id from gold_analysis_project_users where gold_id = ?";
	    $cur=$dbh->prepare($sql);
	    $cur->execute($gold_id);
	    for (;;) {
		my ( $id3, $name3, $email3, $role3, $caliban_id3 ) = $cur->fetchrow_array();
		last if ! $id3;

		if ( $is_first ) {
		    $is_first = 0;
		}
		else {
		    $res .= ",";
		}

		$res .= "{\"name\":";
		if ( $name3 ) {
		    $res .= '"' . $name3 . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"email\":";
		if ( $email3 ) {
		    $res .= '"' . $email3 . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"role\":";
		if ( $role3 ) {
		    $res .= '"' . $role3 . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= ",\"email\":";
		if ( $caliban_id3 ) {
		    $res .= '"' . $caliban_id3 . '"';
		}
		else {
		    $res .= "null";
		}
		$res .= "}";
	    }
	    $cur->finish();
	    $res .= "]";

	    $res .= "}";

	    $dbh->disconnect();
	    return $res;
	}
    }

    return "";
}

################################################################
# getNewGoldMetadata: call GOLD API (GET)
################################################################
sub getNewGoldMetadata {
    my ($gold_id, $type) = @_;

    if ( ! $gold_id ) {
	return "";
    }

    if ( ! $type ) {
	if ( $gold_id =~ /Ga/ ) {
	    $type = 'analysis_project';
	}
	elsif ( $gold_id =~ /Gp/ ) {
	    $type = 'sequencing_project';
	}
	elsif ( $gold_id =~ /Gs/ ) {
	    $type = 'study';
	}
    }

    if ( ! $type ) {
	return "";
    }

##    my $username = 'jgi_img'; 
##    my $password = '&@f8&dJ';

    my $username = 'jgi_img_ui'; 
    my $password = 'H*#FJ2a';

    my $headers = { 
	Content_Type  => 'application/json', 
	Accept        => 'application/json', 
	Authorization => 'Basic ' . 
	    encode_base64( $username . ':' . $password ) 
    }; 
 
    my $client = REST::Client->new(); 

#    my $request = "https://gpweb08.nersc.gov:8443/gold_prod/rest/analysis_project/" . $gold_id;
    my $request = "https://gold.jgi.doe.gov/rest/" . $type .
	"/img/" . $gold_id;

    $client->GET( $request, $headers ); 
 
    my $response_code = $client->responseCode();

    if ( $response_code =~ /^2/ ) {
	return $client->responseContent(); 
    }
    else {
	return "";
    }
}

################################################################
# getNewGoldField: call GOLD analysis field
################################################################
sub getNewGoldField {
    my ($gold_id, $fld_name) = @_;

    if ( ! $gold_id || ! $fld_name) {
	return "";
    }

    my $res = getNewGoldMetadata($gold_id);
    if ( ! $res || blankStr($res) ) { 
        return "";
    }
 
    my $decode = decode_json($res); 
    if ( ! $decode ) {
	return "";
    }

    return $decode->{$fld_name};
}


################################################################
# getGoldAnalysisProject: call GOLD analysis project API (GET)
################################################################
sub getGoldAnalysisProject {
    my ($gold_id) = @_;

    my $res = getNewImgGoldMetadata($gold_id, 'analysis_project');
    if ( $res ) {
	return $res;
    }

    return getNewGoldMetadata($gold_id, 'analysis_project');
}

##################################################################
# postGoldAnalysisProject: call GOLD analysis project API (POST)
##################################################################
sub postGoldAnalysisProject {
    my ($data) = @_;

    if ( ! $data ) {
	return ("", "ERROR: No data");
    }

##    my $username = 'jgi_img'; 
##    my $password = '&@f8&dJ';

    my $username = 'jgi_img_ui'; 
    my $password = 'H*#FJ2a';

    my $headers = { 
	Content_Type  => 'application/json', 
	Accept        => '*/*', 
	Authorization => 'Basic ' . 
	    encode_base64( $username . ':' . $password ) 
    }; 
 
    my $client = REST::Client->new(); 

#    $client->POST('https://gpweb08.nersc.gov:8443/gold_prod/rest/analysis_project',
    $client->POST('https://gold.jgi.doe.gov/rest/analysis_project',
		  ($data,$headers));
 
    # decode the response in perl
#    my $response = decode_json ($client->responseContent());
    # no need to decode
    my $response_code = $client->responseCode();
    my $response = $client->responseContent();

#    if ( $response_code == 201 && $response =~ /^Ga/ ) {
    if ( $response =~ /^Ga/ ) {
	return ($response, "");
    }
    else {
	return ("", $response_code);
    }
}


################################################################
# getGoldAnalysisField: call GOLD analysis field
################################################################
sub getGoldAnalysisField {
    my ($gold_id, $fld_name) = @_;

    if ( ! $gold_id || ! $fld_name) {
	return "";
    }

    my $res = getGoldAnalysisProject($gold_id); 
    if ( ! $res || blankStr($res) ) { 
        return "";
    }
 
    my $decode = decode_json($res); 
    if ( ! $decode ) {
	return "";
    }

    return $decode->{$fld_name};
}

#######################################################
# getGoldAPRole
#######################################################
sub getGoldAPRole {
    my ($contact_oid, $gold_ap_id) = @_;

    if ( ! $contact_oid || ! $gold_ap_id ) {
	return "";
    }

    my $isAdmin = getIsAdmin($contact_oid);
    if ( $isAdmin eq 'Yes' ) {
	return "superuser";
    }

    ## check IMG-GOLD database first
    my $dbh=WebFunctions::Connect_IMG;
    my $sql = "select email, caliban_id from contact where contact_oid = ?";
    my $cur=$dbh->prepare($sql);
    $cur->execute($contact_oid);
    my ( $email, $user_caliban_id ) = $cur->fetchrow_array();
    $cur->finish();

##   if ( $contact_oid == 312 || $contact_oid == 123727 ) {
##	print "<p>*** $contact_oid, $email, $user_caliban_id\n";
##    }

    $sql = "select gold_id from gold_analysis_project where gold_id = ?";
    $cur=$dbh->prepare($sql);
    $cur->execute($gold_ap_id);
    my ( $id2 ) = $cur->fetchrow_array();
    $cur->finish();
    my $role = "";
    if ( $id2 ) {
        ## check IMG-GOLD database only
        $sql = "select gold_id, name, email, role, caliban_id from gold_analysis_project_users where gold_id = ?";
        $cur=$dbh->prepare($sql);
        $cur->execute($gold_ap_id);
        for (;;) {
            my ( $id3, $name3, $email3, $role3, $caliban_id3 ) = $cur->fetchrow_array();
            last if ! $id3;

##	    if ( $contact_oid == 312 || $contact_oid == 123727 ) {
##		print "<p>$email3, $email, $caliban_id3, $user_caliban_id\n";
##	    }

            if ( (lc($email3) eq lc($email)) || $caliban_id3 == $user_caliban_id ) {
                $role = $role3;
                last;
            }
        }
        $cur->finish();
    }
    $dbh->disconnect();
    if ( $role ) {
	return $role;
    }

    my $res = getGoldAnalysisProject($gold_ap_id);
    if ( ! $res || blankStr($res) ) {
        return "";
    }

    require RelSchema;
    my $def_project = RelSchema::def_Analysis_Project();
    my $decode = decode_json($res);

    ## Per Reddy, we don't use GOLD AP visibility (4/19/2017)
#    if ( $decode->{'visibility'} eq 'public' ||
#	 $decode->{'visibility'} eq 'Yes' ) {
#	return "public";
#    }

    if ( $decode->{'submitterContactOid'} == $contact_oid ) {
	return "submitter";
    }

##    my $email = db_getValue("select email from contact where contact_oid = $contact_oid");

    if ( $decode->{'piEmail'} &&
	 lc($email) eq lc ($decode->{'piEmail'}) ) {
	return "PI";
    }

    # check users
    my $key = 'users';
    my $val = $decode->{$key};
    my @arr = @$val;
    for my $a3 ( @arr ) {
	my $role = $a3->{'role'};
	if ( ! $role ) {
	    $role = "other";
	}
	if ( lc($email) eq lc ($a3->{'email'}) ) {
	    return $role;
	}
	if ( $a3->{'calibanId'} == $contact_oid ) {
	    return $role;
	}
    }

    return "";
}

########################################################
# isJgiProject
########################################################
sub isJgiProject {
    my ($dbh, $project_oid) = @_;

    if ( ! $project_oid ||
	 ! isInt($project_oid) ) {
	return 0;
    }

    my $sql = "select its_spid, pmo_project_id from project_info " .
	"where project_oid = ?";
    my $cur=$dbh->prepare($sql);
    $cur->execute($project_oid);
    my($its_spid, $pmo_project_id) = $cur->fetchrow_array();
    $cur->finish();

    if ( $its_spid || $pmo_project_id ) {
	return 1;
    }

    my $is_jgi = 0;
    $sql = "select project_oid, db_name " .
	"from project_info_data_links " .
	"where link_type = 'Seq Center' " .
	"and project_oid = ? " .
	"and db_name is not null";
    my $cur=$dbh->prepare($sql);
    $cur->execute($project_oid);
    for (;;) {
	my($id2, $seq_center) = $cur->fetchrow_array();
	last if ! $id2;
        if ( $seq_center =~ /JGI/ || 
             $seq_center =~ /Joint Genome Institute/ ) {
	    $is_jgi = 1;
	    last;
	}
    }
    $cur->finish();

    return $is_jgi;
}

########################################################
# isJgiSample
########################################################
sub isJgiSample {
    my ($dbh, $sample_oid) = @_;

    if ( ! $sample_oid ||
	 ! isInt($sample_oid) ) {
	return 0;
    }

    my $sql = "select its_spid, pmo_project_id, seq_center_name " .
	"from env_sample " .
	"where sample_oid = ?";
    my $cur=$dbh->prepare($sql);
    $cur->execute($sample_oid);
    my($its_spid, $pmo_project_id, $seq_center) = $cur->fetchrow_array();
    $cur->finish();

    if ( $its_spid || $pmo_project_id ) {
	return 1;
    }

    if ( $seq_center =~ /JGI/ || 
	 $seq_center =~ /Joint Genome Institute/ ) {
	return 1;
    }

    return 0;
}


################################################################
# isJgiAnalysis
################################################################
sub isJgiAnalysis {
    my ($dbh, $gold_ap_id) = @_;

    if ( ! $gold_ap_id ) {
	return 0;
    }

    my $res = getGoldAnalysisProject($gold_ap_id); 
    if ( ! $res || blankStr($res) ) { 
        return 0;
    }
 
    my $decode = decode_json($res); 
    if ( ! $decode ) {
	return 0;
    }

    my $its_spid = $decode->{'itsAnalysisProjectId'};
    if ( $its_spid ) {
	return 1;
    }

    return 0;
}


##################################################################
# ShowDataSetSelectionSection
##################################################################
sub ShowDataSetSelectionSection {
    my ($isAdmin, $database, $new_url) = @_;

    print "<div style='background-color:lightgreen; border:1px solid darkgreen;'>\n";

    print "<p><b>Data Type:</b> \n";
    print nbsp(3);
    print qq{
      <select name='openselect' 
          onchange="window.location='$new_url&database=' + this.value;" 
          style="width:200px;">
      };
    my @db_list = ( 'IMG ER', 'IMG/M ER' );

    ## use default
    my $default_db_option = getSessionParam('sub_disp:img_database');
    if ( ! $database ) {
	$database = $default_db_option;
    }
    ## default to ER
    if ( ! $database ) {
	$database = 'IMG ER';
    }

    if ( $isAdmin eq 'Yes' ) {
	push @db_list, ('IMG_ER_RNASEQ', 'IMG_MER_RNASEQ', 
			'IMG Methylomics');
    }

    for my $db_name ( @db_list ) {
	print "    <option value='$db_name' ";
	if ( $db_name eq $database ) {
	    print " selected "; 
	} 

	print ">" . getDataSetDisplayName($db_name) . "</option>\n";
    }
    print "</select>\n"; 
    print "</div>\n";

    return $database;
}


####################################################################
# getDataSetDisplayName
####################################################################
sub getDataSetDisplayName {
    my ($name) = @_;

    if ( $name eq 'IMG ER' ) {
	return "Genome";
    }
    elsif ( $name eq 'IMG/M ER' ) {
	return "Metagenome";
    }
    elsif ( $name eq 'IMG_ER_RNASEQ' ) {
	return "Transcriptome";
    }
    elsif ( $name eq 'IMG_MER_RNASEQ' ) {
	return "Metatranscriptome";
    }
    elsif ( $name eq 'IMG Methylomics' ) {
	return "Methylome";
    }
    else {
	return $name;
    }
}

sub webErrorHeader {
    my ($text, $noExit) = @_;
    print header( -type => "text/html" );
    print "<br>\n";
    
    print escapeHTML($text);
    
    exit 0 if(!$noExit);
}


1;
