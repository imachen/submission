#!/bin/sh 
# Control the environment from here for security and other reasons.
#PATH=""
#export PATH
#/usr/local/bin/perl -I`pwd` -T request.pl 
/usr/common/usg/languages/perl/5.16.0/bin/perl -I`pwd` -T  request.pl

if [ $? != "0" ] 
then
   echo "<font color='red'>"
   echo "ERROR: Perl taint (-T) security violation or other error." 
   echo "Check web server error log for details."
   echo "</font>"
fi
