#!/usr/bin/perl
##########################################################################
# Main dispatch handler.
##########################################################################
use strict;
use CGI qw( :standard  );
use CGI::Session;
use Data::Dumper;
use Digest::MD5 qw( md5_base64 );

$| = 1;

use lib 'lib';
use WebEnv;
use WebFunctions;
use Submission;
use EnvSample;
use ProjectInfo;
use UserTool;
use SubmitStats;

umask 0002;

my $env         = getEnv();
my $main_cgi    = $env->{main_cgi};
my $verbose     = $env->{verbose};
my $base_url    = $env->{base_url};
my $base_dir    = $env->{base_dir};
my $cgi_url     = $env->{cgi_url};
my $cgi_dir     = $env->{cgi_dir};
my $tmp_dir     = $env->{tmp_dir};
my $site_pw_md5 = $env->{site_pw_md5};

# blockRobots( );

if ( $verbose >= 5 ) {
    webLog "start main 1\n";
}

############################################################################
# main
############################################################################
my $cgi = new CGI;

## Set up session management.
my $session = getSession();
$session->expire("+12h");
resetContactOid();
my $session_id  = $session->id();
my $contact_oid = getContactOid();

# my $super_user = getSuperUser( );
my $cookie = cookie( CGISESSID => $session_id );

if ( $verbose >= 5 ) {
    webLog "start main 2\n";
}

if ( $verbose >= 2 ) {
    webLog "session_id='$session_id'\n";
    webLog "contact_oid='$contact_oid'\n" if $contact_oid ne "";

    #    webLog "super_user='$super_user'\n" if $super_user ne "";
}

### Session Cookie stuff to preserve states.

if ( $verbose >= 5 ) {
    webLog "start main 3\n";
}

if ( !blankStr($site_pw_md5) ) {
    my $pw_prev_md5 = getSessionParam("site_pw_md5");
    my $pw_curr     = param("site_password");
    my $pw_curr_md5 = md5_base64($pw_curr);
    if (    ( blankStr($pw_curr) && $pw_prev_md5 ne $site_pw_md5 )
         || ( !blankStr($pw_curr) && $pw_curr_md5 ne $site_pw_md5 ) )
    {
        printAppHeader("Home");
        printRequestAcctPage();

        #       printLoginPage( );
        ClosePage();
        exit 0;
    } else {
        setSessionParam( "site_pw_md5", $site_pw_md5 );
    }
}

if ( $verbose >= 5 ) {
    webLog "start main 4\n";
}

if (    $contact_oid eq ""
     && param("page") ne "questions"
     && param("page") ne "requestAcct"
     && param("page") ne "submitRequest"
     && !( param("submitRequest") ne "" ) )
{
    my $username = param("username");
    my $password = param("password");
    if ( blankStr($username) ) {
        printAppHeader("Home");
        printRequestAcctPage();

        #       printLoginPage();
        ClosePage();
        exit(0);
    } else {
    }
}

if ( $verbose >= 5 ) {
    webLog "start main 5\n";
}

## Site password set up.
my $pw          = param("site_password");
my $site_md5_pw = md5_base64($pw);
setSessionParam( "site_md5_pw", $site_md5_pw );

if ( $verbose >= 5 ) {
    webLog "start main 6\n";
}

############################################################
# main viewer dispatch
############################################################
# if( param( ) ) {

if ( $verbose >= 5 ) {
    webLog "start main 7\n";
}

## Section dispatches.
my $section = getSection();
my $page    = getPage();
my $action  = getAction();

if ( $verbose >= 5 ) {
    webLog "Section: $section, Page: $page\n";
}

# login
if ( $section eq 'login' ) {
    my $contact_oid = checkAccess();

    if ( !$contact_oid ) {
        webLogNew "Incorrect Login " . currDateTime() . "\n";

        printAppHeader("Home");
        printErrorPage("Unknown username / password");
        exit 0;
    }

    if ( $verbose >= 2 ) {
        webLogNew "New Login Session " . currDateTime() . "\n";
        webLog "session_id='$session_id'\n";
        webLog "contact_oid='$contact_oid'\n" if $contact_oid ne "";
    }

    # filter out cancelled submission (per Nikos' request)
    setSessionParam( 'sub_filter:status', '1,2,3,4,5,6,7,8,9,10,11,12,13,14' );
}

if ( !$action || $action ne 'noHeader' ) {
    printAppHeader($section);
}

if ( $section eq 'logout' ) {
    setSessionParam( 'contact_oid',      '' );
    setSessionParam( 'contact_is_admin', '' );
    clearSubmissionPageInfo();
} elsif ( $section eq 'EnvSample' ) {
    EnvSample::dispatch($page);
} elsif ( $section eq 'UserTool' ) {
    UserTool::dispatch($page);
} elsif ( $section eq 'SubmitStats' ) {
    SubmitStats::dispatch($page);
} elsif (    $section eq 'Submission'
          || $section eq 'ERSubmission'
          || $section eq 'MSubmission' )
{
    Submission::dispatch($page);
} elsif ( $section eq 'ProjectInfo' ) {
    ProjectInfo::dispatch($page);
} elsif ( $section eq 'requestAcct' ) {
    if ( $page eq 'requestAcct' ) {
        printRequestAcctPage();
    } elsif ( $page eq 'submitRequest' ) {
        my $msg = processAcctRequest();

        print "<div id='content'>\n";
        if ( blankStr($msg) ) {
            $msg =
"Your request has been successfully submitted. We will contact you through email. Please do not submit more requests.";
            WebFunctions::showInfoPage($msg);
        } else {
            WebFunctions::showErrorPage($msg);
        }
        print "</div>\n";
    }
} else {
    printHomePage();
}

#}
#else {
#   printHomePage( );
#}

ClosePage();

exit 0;

############################################################################
# printAppHeader - Show top menu and other web UI framework header code.
#   Inputs:
#     current - Highlight current top menu item
#     noMenu - Do not show top menu items
############################################################################
sub printAppHeader {
    my ($current) = @_;

    print header( -type => "text/html", -cookie => $cookie );

    if (    $current eq 'Submission'
         || $current eq 'UserTool'
         || $current eq 'SubmitStats'
         || $current eq 'ERSubmission'
         || $current eq 'MSubmission'
         || $current eq 'ProjectInfo'
         || $current eq 'EnvSample' )
    {
        print start_html(
            -title => "Expert Review Submission",

            #                            -style=> "$base_url/img.css");
            -style => "$base_url/imgsub2.css"
        );
    } else {
        print start_html( -title => "Expert Review Submission",
                          -style => "$base_url/imgsubmit.css" );
    }

    print qq{
        <div id="jgi_logo2">
            <a href="https://www.jgi.doe.gov">
            <img src="/images/logo-JGI-IMG-ERMER.png" width="480" height="70" border="0" alt="IMG ER and MER Submission Site" />
            </a></div>
    };

    printTabMenu($current);
}

############################################################################
# printTabMenu - Print tab menu with active part highlighted.
############################################################################
sub printTabMenu {
    my ($current) = @_;

    my $contact_oid = getContactOid();
    my $isAdmin     = 'No';
    if ($contact_oid) {
        $isAdmin = getIsAdmin($contact_oid);
    }

    #   my $dbh = dbLogin( );
    #   my $isEditor = isImgEditor( $dbh, $contact_oid );
    #   $dbh->disconnect( );

    my $gl0 = "gl0";
    my $gl1 = "gl1";
    my $gl2 = "gl2";
    my $gl3 = "gl3";
    my $gl4 = "gl4";
    my $gl5 = "gl5";
    my $gl6 = "gl6";
    my $g20 = "g20";

    if (    blankStr($current)
         || $current eq "Home"
         || $current eq 'login'
         || $current eq 'requestAcct' )
    {
        $gl1 = "gl1on";
    } elsif ( $current eq "ERSubmission" ) {
        $gl2 = "gl2on";
    } elsif ( $current eq "MSubmission" ) {
        $gl3 = "gl3on";
    }

    #   elsif( $current eq "Submission" ) {
    #      $gl2 = "gl2on";
    #   }
    elsif ( $current eq "ProjectInfo" ) {
        $gl0 = "gl0on";
    } elsif ( $current eq "EnvSample" ) {
        $gl0 = "gl0on";
    } elsif ( $current eq "UserTool" ) {
        $gl5 = "gl5on";
    } elsif ( $current eq "SubmitStats" ) {
        $gl6 = "gl6on";
    }

    print qq{ 
        <table cellspacing='0' cellpadding='0' border='0' id='globalLink'> 
            <tr> 
            <td colspan='6' id='spacerrow'></td> 
            </tr> 
            <tr> 
            <td id='$gl1'><a href='main.cgi?section=Home&page=Home' class='glink'>Expert Review Submission Home</a></td> 
            <td id='$gl2'><a href='main.cgi?section=ERSubmission&page=showERPage' class='glink'>Isolate \& Single Cell Genome Submissions</a></td> 
            <td id='$gl3'><a href='main.cgi?section=MSubmission&page=showMPage' class='glink'>Metagenome Submissions</a></td> 
    };

#   print qq{
#       <td id='$gl3'><a href='main.cgi?section=ProjectInfo&page=showPage' class='glink'>Projects</a></td>
#      <td id='$gl4'><a href='main.cgi?section=EnvSample&page=Sample' class='glink'>Samples</a></td>
#       };

    if ( $isAdmin eq 'Yes' ) {
        print qq{
       <td id='$gl5'><a href='main.cgi?section=UserTool&page=UserTool' class='glink'>Tools</a></td> 
       };
    }

    print qq{ 
       <td id='$gl6'><a href='main.cgi?section=SubmitStats&page=SubmitStatus' class='glink'>Statistics</a></td> 
};

    print qq{
       <td width='58' class='glink' id='gl0'>&nbsp;</td> 
       <td id='$g20'><a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>User Guide</a></td> 
       };

#   print qq{
#       <td width='58' class='glink' id='gl0'>&nbsp;</td>
#      <td id='$g20'><a href='main.cgi?section=UserGuide&page=UserGuide' class='glink'>User Guide</a></td>
#       };

    print qq{
       </tr> 
       </table> 
       };

    if (    $gl0 eq 'gl0on'
         || $gl2 eq 'gl2on'
         || $gl3 eq 'gl3on'
         || $gl4 eq 'gl4on'
         || $gl5 eq 'gl5on'
         || $gl6 eq 'gl6on' )
    {
        print qq{ 
            <div id="content">
            };
    }
}

############################################################################
# printHomePage - Print home page.
############################################################################
sub printHomePage {

    my $url = url();

    #    print "<h1>Expert Review Submission Home Page</h1>\n";

    #    print "<p>\n";
    my $announcement =
      db_getValue("select notes from announcement where type = 'Home Page'");

    #    print "<p>$announcement</p>\n";

    #    print hr;

    my $img_er_link = "<a href=\"" . getImgERUrl() . "\">IMG ER</a>";
    my $img_mi_link = "<a href=\"" . getImgMIUrl() . "\">IMG/M ER</a>";

    print qq{ 
            <div id="bluearrow"></div>
            };

    print qq{ 
        <div id="intro"><img src="$base_url/images/jgi_top_corners.gif" width="496" height="10" alt="" /> 
 
            <table> 
            <tr> 
            <td> 
        The <strong>Microbial Genome & Metagenome Expert Review Data
        Submission</strong> site allows scientists to submit genome datasets
        to IMG ER or metagenome datasets to IMG/M ER in order to
        analyze and curate them in the context of a large set of reference
        public genomes and metagenomes.

        <p>$announcement</p>

            </td>
 
            <td> 
        <img src="$base_url/images/img_er_submit.jpg" width="119" height="220" class="genomes" alt="img_er_submit" title="IMG ER Submission" />
            </td> 
            </tr> 
 
            </table>
 
            <img class="bottom" src="$base_url/images/jgi_bottom_corners.gif" width="496" height="10" alt="" />
 
            </div>
        };

    print qq{
        <div id="stats">
            <img src="$base_url/images/jgi_home_table.gif" width="165" height="61" alt="" />

            <h2><strong>IMG Databases</strong></h2>

        <p></p>

        You can check your loaded genomes in IMG by clicking 
        the following links:

        <ul>
        <li> $img_er_link </li>
        <li> $img_mi_link </li>
        </ul>

            <p>Report all problems to:
            <br/> 
            <a href=\"mailto:IMAChen\@lbl.gov\">Amy Chen</a></p>
 
            <br/> 
            <p> 
            <a href=\"$url?section=logout&page=logout\">Logout</a>
            </div> 
        };

    #    print qq{
    #   <div id="content">
    #       $announcement
    #       </div>
    #   };

    #    print qq{
    #   <p>
    #       You can check your loaded genomes in IMG by clicking
    #       the following links:
    #       <ul>
    #       <li> $img_er_link </li>
    #       <li> $img_mi_link </li>
    #       </ul>
    #       </p>
    #   };

    #    print hr;

    #    print "<p>(Report all problems to: ";
    #    print "<a href=\"mailto:IMAChen\@lbl.gov\">Amy Chen</a>)</p>\n";

#    print "<h2><font color='red'>Warning: Submission Tool is unstable due to GOLD data loading.</font></h2>\n";

    #    my $contact_oid = getContactOid( );
    #    if ( $contact_oid ) {
    #   print "<p>\n";
    #   printHomeLink();
    #    }
}

############################################################################
# getPage - Wrapper for page parameter, in case POST instead of GET
#   method is used.  This is an adaptor function.
############################################################################
sub getPage {
    my $page = param1("page");
    return $page;
}

############################################################################
# getAction
############################################################################
sub getAction {
    my $action = param1("action");
    return $action;
}

############################################################################
# printLoginPage - Show site login prompt.
############################################################################
sub printLoginPage {
    print qq{ 
        <div id="content"> 
        };

    my $url = url();

    print startform( -name => 'loginName', -method => 'post',
                     action => "$url" );

    print
"<h2>Welcome to IMG ER and IMG/M ER Submission Site. Please log in.</h2>\n";

#    print p("Submission site is temporarily closed due to database maintenance work.");
#    return;

    print "<p>Username:\n";
    print nbsp(2);
    print textfield( -name => 'username', -size => 20, -maxlength => 100 );
    print br;

    print br;

    print "<p>Password:\n";
    print nbsp(2);
    print password_field( -name => 'pwd', -size => 20, -maxlength => 100 );
    print br;

    print br;
    print
'<input type="submit" name="_section_login" value="Login" class="smbutton" />';
    print endform();

    print hr;

    print "<h2>For New Users</h2>\n";

    print qq{
    In order to submit a dataset to IMG ER or IMG/M ER, you need to:
    <ol>
        <li>Get account via
      <a href='https://img.jgi.doe.gov/request'>Request Account</a>.
        Only one account is needed to access the IMG ER & IMG/M ER 
        submission site, IMG ER and IMG/M ER. </li>
        <li>Prepare files for submission (for details see section 3 
          of the 
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>).</li>
        <li>Select submission type (IMG ER or IMG/M ER)</li>
        </ol>
    };

    print qq{
    <h5>For IMG ER:</h5>

        <ol>
        <li>Find or define in IMG-GOLD the genome project to be associated 
        with the submitted dataset (for details see section 4.1 of the
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Submit the genome dataset and provide data processing requirements, 
        including gene prediction, EC prediction, product name assignment 
        (for details see section 4.2 of the 
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Check the status of your submission.</li>
        </ol>
    };

    print qq{

    <h5>For IMG/M ER:</h5>

    <ol>
        <li>Find or define in IMG-GOLD the metagenome project and 
        sample to be associated with the submitted dataset 
        (for details see section 5.1of the 
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Submit the metagenome dataset and provide data processing 
        requirements, including gene prediction, EC prediction, 
        product name assignment (for details see section 5.2 of the
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Check the status of your submission.</li>
        </ol>
    };

    #    print "<p>If you are not a registered user, ";
##    print '<input type="submit" name="requestAcct" value="Request Account" class="medbutton" />';

    #    print '<A href="' . $url . '?section=requestAcct&page=requestAcct">' .
    #        'request an account here.</A>';
    print "\n";

}

############################################################################
# printErrorPage - Show error message
############################################################################
sub printErrorPage {
    my ($err_msg) = @_;

    print qq{ 
        <div id="content"> 
        };

    my $url = url();

    print startform( -name => 'errMsg', -method => 'post', action => "$url" );

    print "<h2>Error: $err_msg</h2>\n";

    print endform();
}

############################################################################
# getRequestAcctAttr
############################################################################
sub getRequestAcctAttr {
    my @attrs = (
        "name\tYour Name (First Name and Last Name)\tchar\t80\tY",
        "title\tTitle\tchar\t80\tY",
        "department\tDepartment or Division\tchar\t255\tY",
        "organization\tOrganization or Affiliation\tchar\t255\tY",
        "email\tYour Email\tchar\t255\tY",
        "phone\tPhone Number\tchar\t80\tY",
        "address\tAddress\tchar\t255\tN",
        "city\tCity\tchar\t80\tY",
        "state\tState\tchar\t80\tN",
        "country\tCountry\tchar\t80\tY",
"username\tPreferred Login Name (8-30 characters; no special characters)\tchar\t30\tY",
        "group_name\tIMG Group (if known)\tchar\t80\tN",
        "comments\tReason(s) for Request\ttext\t60\tY"
    );

    return @attrs;
}

############################################################################
# printRequestAcctPage
############################################################################
sub printRequestAcctPage {
    print qq{ 
        <div id="content"> 
        };

    #generate a form
    my $url = url();
    print start_form( -method => 'post',
                      -action => $url );

    print "<h1>Request Account</h1>\n";

#    print "<p>This function is temporarily unavailable due to database maintenance work.</p>\n";
#    return;

    print hiddenVar( 'submitRequest', 1 );

    print qq{ 
            An account is needed for accessing 
            <b>all Integrated Microbial Genome (IMG) data marts</b>,
            namely <b>IMG</b> (public genome datasets),
            <b>IMG/M</b> (public metagenome datasets),
            <b>IMG/ER</b> (public and user private genome data sets),
            <b>IMG/M ER</b> (public and user private metagenome datasets).
            <br/><br/>
            An account is also needed to access the
            <b>Microbial Genome & Metagenome Expert Review
        Data Submission</b> site, which allows scientists to
            submit genome datasets to <b>IMG/ER</b>
            or metagenome datasets to <b>IMG/M ER</b>
            in order to analyze and curate them in the context
            of a large set of reference public genomes and metagenomes.
        <br/><br/>

            Fields marked with an * are <b><u>mandatory</u></B> 
            and need to be filled comprehensively clearly, 
            without using abbreviations. 
            Please provide a <b><u>clear and detailed reason</u></b>
            in English for requesting an account. 
            Requests that do not comply with these requirements 
            will be <b><u>disregarded</u></b>.
        <br/><br/>

            <font color='red'>Please remember your requested login name.
            Also, IMG does not allow using special characters in 
            login name.</font>
            <br/><br/>
        };

    my @attrs = getRequestAcctAttr();

    print "<table class='img' border='1'>\n";

    for my $k (@attrs) {
        my ( $attr_name, $disp_name, $data_type, $len, $is_required ) =
          split( /\t/, $k );

        if ( !$disp_name ) {
            $disp_name = $attr_name;
        }

        my $size = 80;
        if ( $len && $size > $len ) {
            $size = $len;
        }

        my $attr_val = "";

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        if ( $is_required eq 'Y' ) {
            print "*";
        }
        print escapeHTML($disp_name);
        print "</th>\n";

        if ( $attr_name eq 'country' ) {
            my @vals        = db_getValues("select cv_term from countrycv");
            my $default_val = 'USA';
            print "  <td class='img'   align='left'>\n";
            print "<select name='$attr_name' class='img' size='1'>\n";
            for my $val (@vals) {
                print "   <option value='" . escapeHTML($val) . "' ";
                if ( $val eq $default_val ) {
                    print " selected ";
                }
                print ">" . escapeHTML($val) . "</option>\n";
            }
            print "</select></td>\n";
        } elsif ( $data_type eq 'text' ) {
            print "  <td class='img'   align='left'>"
              . "<textarea name='$attr_name' cols='$size' rows='10'>"
              . "</textarea>"
              . "</td>\n";
        } else {
            print "  <td class='img'   align='left'>"
              . "<input type='text' name='$attr_name' value='"
              . escapeHTML($attr_val)
              . "' size='$size'"
              . " maxLength='$len'/>"
              . "</td>\n";
        }

        print "</tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    print submit(
                  -name  => "_section_requestAcct:submitRequest",
                  -value => "Submit",
                  -class => "smdefbutton"
    );
    print "</p>\n";

    print "<hr>\n";
    print
"<p><a href='http://www.jgi.doe.gov/whoweare/accessibility.html' target='view_link'>Accessibility/Section 508</a>\n";

    print end_form();
}

############################################################################
# processAcctRequest
############################################################################
sub processAcctRequest {

    my $msg = "";

    my @attrs = getRequestAcctAttr();

    my $email          = "";
    my $preferred_name = "";
    my $uname          = "";

    for my $k (@attrs) {
        my ( $attr_name, $disp_name, $data_type, $len, $is_required ) =
          split( /\t/, $k );

        if ( $is_required eq 'Y' ) {
            if ( !param1($attr_name) ) {
                $msg = "Please enter a value in '" . $disp_name . "'.";
                return $msg;
            }
        }

        if ( $attr_name eq 'name' ) {
            my $val  = param1($attr_name);
            my @vals = split( / /, $val );
            if ( scalar(@vals) < 2 ) {
                $msg = "Please enter both First Name and Last Name.";
                return $msg;
            }
            $uname = $val;
        } elsif ( $attr_name eq 'username' ) {
            my $val = param1($attr_name);
            if ( !$val || length($val) < 8 || length($val) > 30 ) {
                $msg = "Preferred Login Name must be 8-30 characters.";
                return $msg;
            }

            if ( $val =~ m/[^a-zA-Z0-9\_\-]/ ) {
                $msg = "Login name cannot include special characters.";
                return $msg;
            }
            $preferred_name = $val;
        } elsif ( $attr_name eq 'email' ) {
            $email = param1($attr_name);
        }
    }

    if ( !$email ) {
        $msg = "Please enter your email address.";
        return $msg;
    }

    my $dbh = Connect_IMG_Contact();

    if ( !$dbh ) {
        $msg = "Cannot login to Oracle database";
        return $msg;
    }

    $email          = lc($email);
    $preferred_name = lc($preferred_name);
    $uname          = lc($uname);
    my $sql =
"select count(*) from contact where lower(email) = ? or lower(username) = ? or lower(name) = ? ";
    my $cur = $dbh->prepare($sql);
    $cur->execute( $email, $preferred_name, $uname );
    my ($cnt0) = $cur->fetchrow();
    $cur->finish();

    if ($cnt0) {
        $dbh->disconnect();
        $msg =
"You already have an IMG account. Please contact us if you encounter any login problems.";
        return $msg;
    }

    $sql =
"select count(*) from request_account where lower(email) = ? and (sysdate - request_date) < 1 and status = 'new request'";
    $cur = $dbh->prepare($sql);
    $cur->execute($email);
    my ($cnt1) = $cur->fetchrow();
    $cur->finish();
    if ($cnt1) {
        $dbh->disconnect();
        $msg =
"You already have an IMG account request pending. Please wait for our review.";
        return $msg;
    }

    my $sql = "select max(request_oid) from request_account";
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    if ( !$cur ) {
        $dbh->disconnect();
        $msg = "Cannot access to REQUEST_ACCOUNT table";
        return $msg;
    }

    my ($max_id) = $cur->fetchrow();
    $cur->finish();
    if ( !$max_id ) {
        $max_id = 1;
    } else {
        $max_id++;
    }

    my $ins  = "insert into request_account (request_oid, request_date, status";
    my $vals = "values ($max_id, sysdate, 'new request'";

    my @attrs = getRequestAcctAttr();

    for my $k (@attrs) {
        my ( $attr_name, $disp_name, $data_type, $len, $is_required ) =
          split( /\t/, $k );

        if ( param($attr_name) ) {
            $ins .= ", $attr_name";
            my $v = param($attr_name);
            if ( $attr_name eq 'username' ) {
                $v =~ s/ /\./g;
            }
            $v =~ s/'/''/g;    # replace ' with ''
            $vals .= ", '" . $v . "'";
        }
    }

    $sql = $ins . ") " . $vals . ")";
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    $dbh->disconnect();

    return $msg;
}
