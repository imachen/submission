#!/usr/bin/perl
############################################################################
# This is used to get XML data objects from server.
# It follows the same logic as main.pl and inner.pl
# see xml.cgi
#

# $Id: xml.pl,v 1.36 2012-08-31 20:36:50 klchu Exp $

############################################################################
use strict;
use CGI qw( :standard );
use CGI::Session;

use lib 'lib'; 
use WebFunctions;
use WebEnv;
use Selection;

$| = 1;

umask 0002;

my $env                  = getEnv();
my $base_dir             = $env->{base_dir};
my $default_timeout_mins = $env->{default_timeout_mins};
$default_timeout_mins = 5 if $default_timeout_mins eq "";


# new stuff below
$ENV{TMPDIR}='/app/tmp';
$ENV{TMP}='/app/tmp';
$ENV{TEMP}='/app/tmp';
$ENV{TEMPDIR}='/app/tmp';

$CGITempFile::TMPDIRECTORY = $TempFile::TMPDIRECTORY = '/app/tmp';

# blockRobots();

# timeout( 60 * $default_timeout_mins );

############################################################################
# main
############################################################################

my $cgi     = WebFunctions::getCgi();
my $section = param("section");

if ( $section eq "tooltip" ) {
    my $filename = param('filename');
    print header( -type => "text/html" );
    
    my $file = $base_dir . '/doc/tooltips/' . $filename;
    if ( -e $file ) {
        my $str = file2Str($file);
        print $str;
    }    

} elsif ( $section eq "PhylumTree" ) {

    # xml header
    print header( -type => "text/xml" );
    require PhylumTree;
    PhylumTree::dispatch();

} elsif ( $section eq "BinTree" ) {

    # xml header
    print header( -type => "text/xml" );
    require BinTree;
    BinTree::dispatch();

} elsif ( $section eq "TestTree" ) {

    # xml header
    print header( -type => "text/xml" );

    # FOR TESTING
    require TestTree;
    TestTree::dispatch();

} elsif ( $section eq "BarChartImage" ) {

    # xml header
    print header( -type => "text/xml" );
    require BarChartImage;
    BarChartImage::dispatch();

} elsif ( $section eq "TaxonList" ) {

    # xml header
    print header( -type => "text/xml" );
    require TaxonList;
    TaxonList::dispatch();

} elsif ( $section eq "IMGProteins" ) {

    # xml header
    print header( -type => "text/xml" );
    require IMGProteins;
    IMGProteins::dispatch();

} elsif ( $section eq "RNAStudies" ) {

    # xml header
    print header( -type => "text/xml" );
    require RNAStudies;
    RNAStudies::dispatch();

} elsif ( $section eq "PathwayMaps" ) {

    # xml header
    print header( -type => "text/xml" );
    require PathwayMaps;
    PathwayMaps::dispatch();

} elsif ( $section eq "TableUtil" ) {

    # xml header
    print header( -type => "text/xml" );
    require TableUtil;
    TableUtil::dispatch();

} elsif ( $section eq "GenomeListFilter" ) {

    # xml header
    print header( -type => "text/xml" );
    require GenomeListFilter;
    GenomeListFilter::dispatch();

} elsif ( $section eq "FindGenomesByMetadata" ) {

    # xml header
    print header( -type => "text/xml" );
    require FindGenomesByMetadata;
    FindGenomesByMetadata::dispatch();

} elsif ( $section eq "FunctionAlignment" ) {

    # xml header
    print header( -type => "text/xml" );
    require FunctionAlignment;
    FunctionAlignment::dispatch();

} elsif ( $section eq "Artemis" ) {

    # xml header
    #print header( -type => "text/xml" );
    require Artemis;
    Artemis::dispatch();

} elsif ( $section eq "ACT" ) {

    require ACT;
    ACT::dispatch();

} elsif ( $section eq "TreeFile" ) {

    # text/html
    print header( -type => "text/html" );
    require TreeFile;
    TreeFile::dispatch();

} elsif ( $section eq "Selection" ) {

    # text/html
#    require Selection;
    
    Selection::dispatch();

} elsif ( $section eq "TreeFileMgr" ) {

    # text/html
    print header( -type => "text/html" );
    require TreeFileMgr;
    TreeFileMgr::dispatch();

} elsif ( $section eq "GeneCassetteSearch" ) {

    # text/html
    print header( -type => "text/html" );
    require GeneCassetteSearch;
    GeneCassetteSearch::dispatch();

} elsif ( $section eq "GeneDetail" ) {

    # xml header
    print header( -type => "text/xml" );
    require GeneDetail;
    GeneDetail::dispatch();

} elsif ( $section eq "Cart" ) {

    require Cart;
    Cart::dispatch();
} elsif ( $section eq "Check" || $section eq "check" ) {
    require Check;
    Check::dispatch();

} elsif ( $section eq "RadialPhyloTree" ) {
    require RadialPhyloTree;
    RadialPhyloTree::dispatch();

} elsif ( $section eq "Workspace" ) {
    # text header
    print header( -type => "text/html" );
    require Workspace;
    Workspace::dispatch();

} elsif ( $section eq "MessageFile" ) {

    #
    # ajax general message check - see header.js and main.pl footer section
    #
    print header( -type => "text/html" );
    my $message_file = $env->{message_file};
    if ( $message_file ne "" && -e $message_file ) {

        my $str = file2Str($message_file);
        print $str;
    }

} else {
    print header( -type => "text/plain" );
    print "Unknown section='$section'\n";
}
exit 0;

