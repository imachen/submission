#!/bin/bash 

export OPENSSL_DIR=/usr/common/usg/utilities/openssl/1.1.0c
export OPENSSL_LIB=/usr/common/usg/utilities/openssl/1.1.0c/lib
export OPENSSL_INC=/usr/common/usg/utilities/openssl/1.1.0c/include
export PERL5LIB=/usr/common/usg/utilities/git/1.8.3.1/lib
export GCC_DIR=/usr/common/usg/languages/gcc/4.8.3
export PYTHON_DIR=/usr/common/usg/languages/python/2.7.4
export LD_LIBRARY_PATH=/usr/common/usg/utilities/openssl/1.1.0c/lib:/usr/common/usg/languages/gcc/4.8.3/lib64:/usr/common/usg/languages/gcc/4.8.3/lib:/usr/common/usg/languages/perl/5.16.0/lib:/usr/common/usg/languages/python/2.7.4/lib:/usr/common/usg/languages/R/3.1.2/lib64/R/lib:/usr/common/usg/utilities/curl/7.37.1/lib:/usr/common/usg/utility_libraries/readline/6.2/lib:/usr/common/jgi/development/argtable/2.13/lib:/usr/common/usg/utilities/mysql/5.0.96_1/lib/mysql:/usr/common/jgi/phylogenetics/arb/5.5/lib:/usr/common/usg/utility_libraries/boost/gnu4.8/1.50.0/lib:/usr/common/jgi/utilities/fastbit/1.3.9/lib:/usr/common/usg/utilities/sqlite3/3.8.2/lib:/usr/common/usg/utility_libraries/ImageMagick/6.8.0-10/lib:/usr/common/jgi/oracle_client/11.2.0.3.0/client_1/lib:/usr/common/usg/languages/python/3.5.2/lib:/usr/common/usg/languages/perl/5.16.0/lib:/usr/common/usg/utilities/git/1.8.3.1/lib:/usr/common/usg/languages/gcc/4.6.4/lib64:/usr/common/usg/languages/gcc/4.6.4/lib:/usr/syscom/nsg/lib
export PATH=/usr/common/usg/utilities/openssl/1.1.0c/bin:/usr/common/usg/languages/gcc/4.8.3/bin:/usr/common/usg/languages/perl/5.16.0/bin:/usr/common/usg/languages/python/2.7.4/bin:/usr/common/usg/languages/R/3.1.2/bin:/usr/common/usg/utilities/curl/7.37.1/bin:/usr/common/usg/languages/java/jdk/oracle/1.8.0_31_x86_64/bin:/usr/common/jgi/aligners/clustal-omega/1.1.0/bin:/usr/common/jgi/aligners/clustalw/2.1/bin:/usr/common/usg/utilities/mysql/5.0.96_1/bin:/usr/common/jgi/frameworks/EMBOSS/6.4.0/bin:/usr/common/jgi/phylogenetics/arb/5.5/bin:/usr/common/jgi/aligners/mummer/3.23/bin:/usr/common/jgi/utilities/fastbit/1.3.9/bin:/usr/common/jgi/math/gnuplot/4.2.2/bin:/usr/common/usg/utilities/sqlite3/3.8.2/bin:/usr/common/usg/utility_libraries/ImageMagick/6.8.0-10/bin:/usr/common/jgi/oracle_client/11.2.0.3.0/client_1/bin:/usr/common/usg/languages/python/3.5.2/bin:/usr/common/usg/languages/perl/5.16.0/bin:/usr/common/usg/utilities/git/1.8.3.1/bin:/usr/common/usg/languages/gcc/4.6.4/bin:/usr/common/usg/bin:/usr/common/mss/bin:/usr/common/nsg/bin:/usr/syscom/nsg/bin:/usr/share/Modules/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/global/homes/k/klchu/bin
export PERL_DIR=/usr/common/usg/languages/perl/5.16.0
export PYTHONPATH=/usr/common/usg/utilities/git/1.8.3.1/lib/python2.7
export PYTHON_VER=python2.7.4
export ARBHOME=/usr/common/jgi/phylogenetics/arb/5.5
export ARB_DIR=/usr/common/jgi/phylogenetics/arb/5.5
export ARGTABLE_DIR=/usr/common/jgi/development/argtable/2.13
export BOOST_DIR=/usr/common/usg/utility_libraries/boost/gnu4.8/1.50.0
export BOOST_ROOT=/usr/common/usg/utility_libraries/boost/gnu4.8/1.50.0
export CC=gcc
export CLUSTALW_DIR=/usr/common/jgi/aligners/clustalw/2.1
export CLUSTAL_OMEGA_DIR=/usr/common/jgi/aligners/clustal-omega/1.1.0
export CURL_DIR=/usr/common/usg/utilities/curl/7.37.1
export CXX=g++
export EMBOSS_DIR=/usr/common/jgi/frameworks/EMBOSS/6.4.0
export F77=gfortran
export F90=gfortran
export FASTBIT_DIR=/usr/common/jgi/utilities/fastbit/1.3.9
export FC=gfortran
export GNUPLOT_DIR=/usr/common/jgi/math/gnuplot/4.2.2
export IMAGEMAGICK_PATH=/usr/common/usg/utility_libraries/ImageMagick/6.8.0-10
export JAVA_HOME=/usr/common/usg/languages/java/jdk/oracle/1.8.0_31_x86_64
export MUMMER_DIR=/usr/common/jgi/aligners/mummer/3.23
export MYSQL_DIR=/usr/common/usg/utilities/mysql/5.0.96_1
export MYSQL_VER=mysql5.0.96_1
export NERSC_PRGENV=gnu4.8
export NERSC_PRGENV_USERMODULES=argtable/2.13:boost/1.50.0
export ORACLE_HOME=/usr/common/jgi/oracle_client/11.2.0.3.0/client_1
export ORACLE_JDK_DIR=/usr/common/usg/languages/java/jdk/oracle/1.8.0_31_x86_64
export ORACLE_JDK_VER=1.8.0_31
export PKGCONFIG_PATH=/usr/common/usg/utilities/sqlite3/3.8.2/lib/pkgconfig
export READLINE_DIR=/usr/common/usg/utility_libraries/readline/6.2
export R_DIR=/usr/common/usg/languages/R/3.1.2
export SQLITE3_DIR=/usr/common/usg/utilities/sqlite3/3.8.2
export TNS_ADMIN=/usr/common/jgi/oracle_client/11.2.0.3.0/client_1/network/admin


export TMP="/local/scratch"
export TEMP="/local/scratch"
export TEMPDIR="/local/scratch"
export TMPDIR="/local/scratch"
export SQLITE_TMPDIR="/local/scratch"

PERL5LIB=`pwd`
export PERL5LIB

/usr/common/usg/languages/perl/5.16.0/bin/perl -T inner.pl


if [ $? != "0" ] 
then
   echo "<font color='red'>"
   echo "ERROR: Perl taint (-T) security violation or other error." 
   echo "Check web server error log for details."
   echo "</font>"
fi
