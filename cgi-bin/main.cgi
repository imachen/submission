#!/usr/bin/perl
##########################################################################
# Main dispatch handler.
#
# I think this is for er submit - ken
#
# $Id: main.pl,v 1.9 2013-04-29 16:06:15 imachen Exp $
##########################################################################
use strict;

BEGIN {
    $ENV{TMP}     = "/app/tmp";
    $ENV{TEMP}    = "/app/tmp";
    $ENV{TEMPDIR} = "/app/tmp";
    $ENV{TMPDIR}  = "/app/tmp";
}

$ENV{TMP}     = "/app/tmp";
$ENV{TEMP}    = "/app/tmp";
$ENV{TEMPDIR} = "/app/tmp";
$ENV{TMPDIR}  = "/app/tmp";

use CGI qw( :standard  );
use CGI::Session;
use CGI::Carp qw( carpout set_message fatalsToBrowser );
$ENV{TMP}     = "/app/tmp";
$ENV{TEMP}    = "/app/tmp";
$ENV{TEMPDIR} = "/app/tmp";
$ENV{TMPDIR}  = "/app/tmp";

use Data::Dumper;
use Digest::MD5 qw( md5_base64 );

$| = 1;

#use perl5lib;
use lib 'lib';
use WebEnv;
use WebFunctions;
use Submission;
use EnvSample;
use ProjectInfo;
use UserTool;
use SubmitStats;

umask 0002;

my $env            = getEnv();
my $main_cgi       = $env->{main_cgi};
my $verbose        = $env->{verbose};
my $base_url       = $env->{base_url};
my $base_dir       = $env->{base_dir};
my $cgi_url        = $env->{cgi_url};
my $cgi_dir        = $env->{cgi_dir};
my $tmp_dir        = $env->{tmp_dir};
my $cgi_tmp_dir    = $env->{cgi_tmp_dir};
my $site_pw_md5    = $env->{site_pw_md5};
my $user_guide_url = $env->{user_guide_url};
my $workspace_dir  = $env->{workspace_dir};
my $keycloak       = $env->{keycloak};

# new stuff below
$ENV{TMP}     = "/app/tmp";
$ENV{TEMP}    = "/app/tmp";
$ENV{TEMPDIR} = "/app/tmp";
$ENV{TMPDIR}  = "/app/tmp";

$CGITempFile::TMPDIRECTORY = $TempFile::TMPDIRECTORY = '/app/tmp';

blockRobots();

if ( $verbose >= 5 ) {
    webLog "start main 1: $cgi_tmp_dir\n";
    webLog "base_url: $base_url\n";
    webLog "cgi_url: $cgi_url\n";
}

############################################################################
# main
############################################################################
my $cgi = getCgi();

$CGITempFile::TMPDIRECTORY = $TempFile::TMPDIRECTORY = '/app/tmp';

## Set up session management.
my $session = getSession();
my $session_id  = $session->id();
my $contact_oid = getContactOid();

my $cookie    = WebFunctions::makeCookieSession();
my $cookie_kc = ''; 

if ( !blankStr($site_pw_md5) ) {
    my $pw_prev_md5 = getSessionParam("site_pw_md5");
    my $pw_curr     = param("site_password");
    my $pw_curr_md5 = md5_base64($pw_curr);
    if (    ( blankStr($pw_curr) && $pw_prev_md5 ne $site_pw_md5 )
         || ( !blankStr($pw_curr) && $pw_curr_md5 ne $site_pw_md5 ) )
    {
        printAppHeader("Home");
        printLoginPage();
        ClosePage();
        exit 0;
    } else {
        setSessionParam( "site_pw_md5", $site_pw_md5 );
    }
}

if ( $verbose >= 5 ) {
    webLog "start main 4\n";
}

# sso
# sso Caliban
# cookie name: jgi_return, value: url, domain: jgi.doe.gov
my $sso_enabled     = $env->{sso_enabled};
my $sso_url         = $env->{sso_url};
my $sso_domain      = $env->{sso_domain};
my $sso_cookie_name = $env->{sso_cookie_name};       # jgi_return cookie name
my $jgi_return_url  = "";
my $oldLogin        = getSessionParam("oldLogin");
$oldLogin = 0 if $oldLogin eq "";
$oldLogin = 1 if param('oldLogin') eq 'true';
$oldLogin = 0 if param('oldLogin') eq 'false';

if ( param('oldLogin') eq 'true' || $oldLogin ) {
    setSessionParam( "oldLogin", 1 );
    $oldLogin = 1;
} else {
    setSessionParam( "oldLogin", 0 );
    $oldLogin = 0;
}

if ($keycloak) {

    # new keycloak login
    require Caliban;

    # does the cookie kc_session exists from Portal
    my $kcsession = Caliban::getKc_session();
    my $img_kc    = Caliban::getImg_kc();
    my $section = getSection();
    my $page    = getPage();

        if ( $section eq 'CalibanLoginReturn' ) {
            param( 'page', 'loginreturn' );
            
            # read token
            Caliban::verifyKeyCloak();
            my $kc_id = getSessionParam("kc_id");
            $cookie_kc = WebFunctions::makeCookieIMGKc($kc_id); 
            
            
        } elsif ( $section eq 'CalibanLogoutReturn' ) {
            # user logout
            param( 'page', 'logoutreturn' );
            Caliban::logout();
            printAppHeader("Home");
            Caliban::printSsoForm();
            ClosePage();
            exit(0);
        } elsif($contact_oid && !$img_kc) {
            # user logged OUT of another img site
            param( 'page', 'logoutreturn' );
            Caliban::logout();
            printAppHeader("Home");
            Caliban::printSsoForm();
            ClosePage();
            exit(0);            
        } elsif ( !$contact_oid &&  $img_kc) {
            # user login from another img site eg mer
            printAppHeader("Home");
            Caliban::keycloalRedirect();
            exit(0);
        } elsif ( !$contact_oid && $kcsession) {
            # user login from portal
            printAppHeader("Home");
            Caliban::keycloalRedirect();
            exit(0);     
        } elsif ( !$contact_oid ) {
            printAppHeader("Home");
            Caliban::printSsoForm();
            ClosePage();
            exit(0);
        } elsif($contact_oid) {                  
            my $ans = getSessionParam('backchannel_logout') // '';
            
            webLog("=== ans === $ans \n");
            
            if($ans eq 'yes') {
                # back channel logged out

                print header( -type   => "text/html");         
                Caliban::logout();
                exit(0);                  
            }
        }


} else {

    if ( !$oldLogin && $sso_enabled ) {
        require Caliban;
        if ( !$contact_oid ) {
            my $dbh_main = Connect_IMG_Contact();
            my $ans      = Caliban::validateUser($dbh_main);
            $dbh_main->disconnect();

            if ( !$ans ) {
                printAppHeader("Home");
                Caliban::printSsoForm();
                ClosePage();
                exit(0);
            }

            ## load submission filter
            Submission::loadSubmissionFilter();
        }

        # TODO logout in genome portal i still have contact oid
        # I have to fix and relogin
        my $ans = Caliban::isValidSession();
        if ( !$ans ) {
            Caliban::logout();
            printAppHeader("Home");
            Caliban::printSsoForm();
            ClosePage();
            exit(0);
        }

    } else {
        if (    $contact_oid eq ""
             && param("page") ne "questions"
             && param("page") ne "requestAcct"
             && param("page") ne "submitRequest"
             && !( param("submitRequest") ne "" ) )
        {
            my $username = param("username");
            my $password = param("password");
            if ( blankStr($username) ) {
                printAppHeader("Home");
                printLoginPage();
                ClosePage();
                exit(0);
            } else {
            }
        }
    }

}

if ( $verbose >= 5 ) {
    webLog "start main 5\n";
}

## Site password set up.
my $pw          = param("site_password");
my $site_md5_pw = md5_base64($pw);
setSessionParam( "site_md5_pw", $site_md5_pw );

if ( $verbose >= 5 ) {
    webLog "start main 6\n";
}

############################################################
# main viewer dispatch
############################################################
# if( param( ) ) {

if ( $verbose >= 5 ) {
    webLog "start main 7\n";
}

## Section dispatches.
my $section = getSection();
my $page    = getPage();
my $action  = getAction();

if ( $verbose >= 5 ) {
    webLog "Section: $section, Page: $page\n";
}

# login
if ( $section eq 'login' ) {
    my $contact_oid = checkAccess();

    if ( !$contact_oid ) {
        webLogNew "Incorrect Login" . currDateTime() . "\n";

        printAppHeader("Home");
        printErrorPage("Unknown username / password");
        exit 0;
    }

    if ( $verbose >= 2 ) {
        webLogNew "New Login Session " . currDateTime() . "\n";
        webLog "session_id='$session_id'\n";
        webLog "contact_oid='$contact_oid'\n" if $contact_oid ne "";
    }

    # set default filter condition
    # filter out cancelled submission (per Nikos' request)
    my $status_str = db_getValuesToString(
"select term_oid from submission_statuscv where term_oid != 90 order by 1"
    );

#       setSessionParam('sub_filter:status', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18');
    setSessionParam( 'sub_filter:status', $status_str );

    setSessionParam( 'sub_filter:comp_submission_date', '>=' );
    setSessionParam( 'sub_filter:val_submission_date',  '01-JAN-22' );

    # set saved filter condition
    if ( $workspace_dir && $contact_oid ) {
        my $file_name =
          $workspace_dir . "/" . $contact_oid . "/" . "mySubmissionFilter";
        if ( -e $file_name ) {
            my $fh = newReadFileHandle( $file_name, "mySubmissionFilter" );
            if ($fh) {
                while ( my $cond1 = $fh->getline() ) {
                    chomp($cond1);
                    my ( $tag, $val ) = split( /\t/, $cond1, 2 );
                    setSessionParam( $tag, $val );
                }
                close($fh);
            }
        }
    }
}

if ( !$action || $action ne 'noHeader' ) {
    printAppHeader($section);
}

if ( $section eq 'logout' ) {
    setSessionParam( 'contact_oid',      '' );
    setSessionParam( 'contact_is_admin', '' );
    clearSubmissionPageInfo();

    # sso
    if ( !$oldLogin && $sso_enabled ) {
        require Caliban;
        Caliban::logout(1);
    }
} elsif ( $section eq 'EnvSample' ) {
    EnvSample::dispatch($page);
} elsif (    $section eq 'UserTool'
          || $section eq 'FAQ' )
{
    UserTool::dispatch($page);
} elsif ( $section eq 'SubmitStats' ) {
    SubmitStats::dispatch($page);
} elsif (    $section eq 'Submission'
          || $section eq 'NewSubmission'
          || $section eq 'FilterSubmission'
          || $section eq 'Methylomics'
          || $section eq 'RnaSeq'
          || $section eq 'MerRnaSeq'
          || $section eq 'ERSubmission'
          || $section eq 'MSubmission' )
{
    Submission::dispatch($page);
} elsif ( $section eq 'ProjectInfo' ) {
    ProjectInfo::dispatch($page);
} elsif ( $section eq 'requestAcct' ) {
    if ( $page eq 'requestAcct' ) {
        printRequestAcctPage();
    } elsif ( $page eq 'submitRequest' ) {
        my $msg = processAcctRequest();

        print "<div id='content'>\n";
        if ( blankStr($msg) ) {
            $msg =
"Your request has been successfully submitted. We will contact you through email.";
            WebFunctions::showInfoPage($msg);
        } else {
            WebFunctions::showErrorPage($msg);
        }
        print "</div>\n";
    }
} else {
    my $apId = param1('apId');
    if ($apId) {
        $section = 'NewSubmission';
        $page    = 'newMain';
        printAppHeader($section);
        Submission::dispatch($page);
    } else {
        printHomePage();
    }
}

#}
#else {
#   printHomePage( );
#}

ClosePage();

exit 0;

############################################################################
# printAppHeader - Show top menu and other web UI framework header code.
#   Inputs:
#     current - Highlight current top menu item
#     noMenu - Do not show top menu items
############################################################################
sub printAppHeader {
    my ($current) = @_;

    # TODO sso
    my $cookie_return;
    if ($sso_enabled) {
        my $url = $env->{cgi_url} . "/" . $env->{main_cgi};
          $cookie_return =
          CGI::Cookie->new(
                            -name   => $sso_cookie_name,
                            -value  => $url,
                            -domain => $sso_domain
          );
    }

    if(!$cookie_kc) {
        my $kc_id = getSessionParam("kc_id") // '';
        $cookie_kc = WebFunctions::makeCookieIMGKc($kc_id); 
    }

    if ( $cookie_return ne "" ) {
        
        print header( -type   => "text/html",
                      -cookie => [ $cookie, $cookie_return, $cookie_kc ] );
    } else {
        print header( -type   => "text/html",
                      -cookie => [ $cookie,  $cookie_kc ] );

    }

    #print header( -type => "text/html", -cookie => $cookie );

    if (    $current eq 'Submission'
         || $current eq 'NewSubmission'
         || $current eq 'FilterSubmission'
         || $current eq 'UserTool'
         || $current eq 'FAQ'
         || $current eq 'SubmitStats'
         || $current eq 'ERSubmission'
         || $current eq 'MSubmission'
         || $current eq 'Methylomics'
         || $current eq 'RnaSeq'
         || $current eq 'MerRnaSeq'
         || $current eq 'ProjectInfo'
         || $current eq 'EnvSample' )
    {
        print start_html( -title => "Expert Review Submission",
                          -style => "$base_url/imgsub2.css" );
    } else {
        print start_html( -title => "Expert Review Submission",
                          -style => "$base_url/imgsubmit.css" );
    }

    print qq{ 
        <div id="jgi_logo2">
            <a href="https://www.jgi.doe.gov">
            <img src="/images/logo-JGI-IMG-ERMER.png" width="480" height="70" border="0" alt="IMG ER and MER Submission Site" />
            </a></div> 
    };

    printTabMenu($current);
}

############################################################################
# printTabMenu - Print tab menu with active part highlighted.
############################################################################
sub printTabMenu {
    my ($current) = @_;

    my $contact_oid = getContactOid();
    my $isAdmin     = 'No';
    if ($contact_oid) {
        $isAdmin = getIsAdmin($contact_oid);
    }

    #   my $dbh = dbLogin( );
    #   my $isEditor = isImgEditor( $dbh, $contact_oid );
    #   $dbh->disconnect( );

    my $gl0 = "gl0";
    my $gl1 = "gl1";
    my $gl2 = "gl2";
    my $gl3 = "gl3";
    my $gl4 = "gl4";
    my $gl5 = "gl5";
    my $gl6 = "gl6";
    my $gl7 = "gl7";
    my $gl8 = "gl8";
    my $gl9 = "gl9";
    my $g20 = "g20";

    if (    blankStr($current)
         || $current eq "Home"
         || $current eq 'login'
         || $current eq 'requestAcct' )
    {
        $gl1 = "gl1on";
    } elsif ( $current eq "ERSubmission" ) {
        $gl2 = "gl2on";
    } elsif ( $current eq "MSubmission" ) {
        $gl2 = "gl2on";
    } elsif ( $current eq "NewSubmission" ) {
        $gl3 = "gl3on";
    } elsif ( $current eq "FilterSubmission" ) {
        $gl4 = "gl4on";
    } elsif ( $current eq "RnaSeq" ) {

        #        $gl7 = "gl7on";
        $gl2 = "gl2on";
    } elsif ( $current eq "MerRnaSeq" ) {

        #        $gl8 = "gl8on";
        $gl2 = "gl2on";
    } elsif ( $current eq "Methylomics" ) {

        #        $gl9 = "gl9on";
        $gl2 = "gl2on";
    } elsif ( $current eq "Submission" ) {
        $gl2 = "gl2on";
    } elsif ( $current eq "ProjectInfo" ) {

        #        $gl0 = "gl0on";
        $gl2 = "gl2on";
    } elsif ( $current eq "EnvSample" ) {

        #        $gl0 = "gl0on";
        $gl2 = "gl2on";
    } elsif ( $current eq "UserTool" ) {
        $gl5 = "gl5on";
    } elsif ( $current eq "FAQ" ) {
        $g20 = "g20on";
    } elsif ( $current eq "SubmitStats" ) {
        $gl6 = "gl6on";
    }

    my $submitAdmin = isSubmissionAdmin($contact_oid);

    print qq{ 
        <table cellspacing='0' cellpadding='0' border='0' id='globalLink'> 
            <tr> 
            <td colspan='6' id='spacerrow'></td> 
            </tr> 
            <tr> 
            <td id='$gl1'><a href='main.cgi?section=Home&page=Home' class='glink'>Submission Home</a></td> 
            <td id='$gl2'><a href='main.cgi?section=ERSubmission&page=showERPage' class='glink'>Submitted Datasets</a></td> 
            <td id='$gl3'><a href='main.cgi?section=NewSubmission&page=newMain' class='glink'>New Submission</a></td> 
    };
    print qq{
       <td id='$gl4'><a href='main.cgi?section=FilterSubmission&page=filterSubmission' class='glink'>Filter</a></td> 
       };

    if ( $submitAdmin eq 'Yes' ) {
        print qq{
       <td id='$gl5'><a href='main.cgi?section=UserTool&page=UserTool' class='glink'>Tools</a></td> 
       };
    }

    print qq{ 
       <td id='$gl6'><a href='main.cgi?section=SubmitStats&page=SubmitStatus' class='glink'>Statistics</a></td> 
};

#    if ( $contact_oid ) {
#   print qq{
#           <td width='58' class='glink' id='gl0'>&nbsp;</td>
#           <td id='$g20'><a href='$user_guide_url' target='view_link'>User Guide</a></td>
#        };
#
#    }
#    else {
#   print qq{
#           <td width='58' class='glink' id='gl0'>&nbsp;</td>
#           <td id='$g20'>User Guide</td>
#        };
#    }

    print qq{
       <td width='58' class='glink' id='gl0'>&nbsp;</td>
       <td id='$g20'><a href='/docs/submission/index.html' class='glink'>FAQ</a></td>
       };

    print qq{
       </tr> 
       </table> 
       };

    if (    $gl0 eq 'gl0on'
         || $gl2 eq 'gl2on'
         || $gl3 eq 'gl3on'
         || $gl4 eq 'gl4on'
         || $gl5 eq 'gl5on'
         || $gl6 eq 'gl6on'
         || $gl7 eq 'gl7on'
         || $gl8 eq 'gl8on'
         || $gl9 eq 'gl9on'
         || $g20 eq 'g20on' )
    {
        print qq{ 
            <div id="content">
            };
    }
}

############################################################################
# printTabMenu - Print tab menu with active part highlighted.
############################################################################
sub printTabMenu_old {
    my ($current) = @_;

    my $contact_oid = getContactOid();
    my $isAdmin     = 'No';
    if ($contact_oid) {
        $isAdmin = getIsAdmin($contact_oid);
    }

    #   my $dbh = dbLogin( );
    #   my $isEditor = isImgEditor( $dbh, $contact_oid );
    #   $dbh->disconnect( );

    my $gl0 = "gl0";
    my $gl1 = "gl1";
    my $gl2 = "gl2";
    my $gl3 = "gl3";
    my $gl4 = "gl4";
    my $gl5 = "gl5";
    my $gl6 = "gl6";
    my $gl7 = "gl7";
    my $gl8 = "gl8";
    my $gl9 = "gl9";
    my $g20 = "g20";

    if (    blankStr($current)
         || $current eq "Home"
         || $current eq 'login'
         || $current eq 'requestAcct' )
    {
        $gl1 = "gl1on";
    } elsif ( $current eq "ERSubmission" ) {
        $gl2 = "gl2on";
    } elsif ( $current eq "MSubmission" ) {
        $gl3 = "gl3on";
    } elsif ( $current eq "FilterSubmission" ) {
        $gl4 = "gl4on";
    } elsif ( $current eq "RnaSeq" ) {
        $gl7 = "gl7on";
    } elsif ( $current eq "MerRnaSeq" ) {
        $gl8 = "gl8on";
    } elsif ( $current eq "Methylomics" ) {
        $gl9 = "gl9on";
    }

    #   elsif( $current eq "Submission" ) {
    #      $gl2 = "gl2on";
    #   }
    elsif ( $current eq "ProjectInfo" ) {
        $gl0 = "gl0on";
    } elsif ( $current eq "EnvSample" ) {
        $gl0 = "gl0on";
    } elsif ( $current eq "UserTool" ) {
        $gl5 = "gl5on";
    } elsif ( $current eq "SubmitStats" ) {
        $gl6 = "gl6on";
    }

    my $submitAdmin = isSubmissionAdmin($contact_oid);
    if ( $submitAdmin eq 'Yes' ) {
        print qq{ 
        <table cellspacing='0' cellpadding='0' border='0' id='globalLink'> 
            <tr> 
            <td colspan='6' id='spacerrow'></td> 
            </tr> 
            <tr> 
            <td id='$gl1'><a href='main.cgi?section=Home&page=Home' class='glink'>Submission Home</a></td> 
            <td id='$gl2'><a href='main.cgi?section=ERSubmission&page=showERPage' class='glink'>Isolate \& Single Cell Genomes</a></td> 
            <td id='$gl3'><a href='main.cgi?section=MSubmission&page=showMPage' class='glink'>Metagenomes</a></td> 
    };
        print qq{
       <td id='$gl7'><a href='main.cgi?section=RnaSeq&page=showRnaSeq' class='glink'>RNA Seq</a></td> 
       <td id='$gl8'><a href='main.cgi?section=MerRnaSeq&page=showMerRnaSeq' class='glink'>Meta RNA Seq</a></td> 
       <td id='$gl9'><a href='main.cgi?section=Methylomics&page=showMethylomics' class='glink'>Methylomics</a></td> 
       };
        print qq{
       <td id='$gl5'><a href='main.cgi?section=UserTool&page=UserTool' class='glink'>Tools</a></td> 
       };
    } else {
        print qq{ 
        <table cellspacing='0' cellpadding='0' border='0' id='globalLink'> 
            <tr> 
            <td colspan='6' id='spacerrow'></td> 
            </tr> 
            <tr> 
            <td id='$gl1'><a href='main.cgi?section=Home&page=Home' class='glink'>Expert Review Submission Home</a></td> 
            <td id='$gl2'><a href='main.cgi?section=ERSubmission&page=showERPage' class='glink'>Isolate \& Single Cell Genome Submissions</a></td> 
            <td id='$gl3'><a href='main.cgi?section=MSubmission&page=showMPage' class='glink'>Metagenome Submissions</a></td> 
    };
    }

    print qq{ 
       <td id='$gl6'><a href='main.cgi?section=SubmitStats&page=SubmitStatus' class='glink'>Statistics</a></td> 
};

    if ($contact_oid) {
        print qq{
           <td width='58' class='glink' id='gl0'>&nbsp;</td> 
            <td id='$g20'><a href='$user_guide_url' target='view_link'>User Guide</a></td> 
        };
    } else {
        print qq{
           <td width='58' class='glink' id='gl0'>&nbsp;</td> 
            <td id='$g20'>User Guide</td> 
        };
    }

#   print qq{
#       <td width='58' class='glink' id='gl0'>&nbsp;</td>
#      <td id='$g20'><a href='main.cgi?section=UserGuide&page=UserGuide' class='glink'>User Guide</a></td>
#       };

    print qq{
       </tr> 
       </table> 
       };

    if (    $gl0 eq 'gl0on'
         || $gl2 eq 'gl2on'
         || $gl3 eq 'gl3on'
         || $gl4 eq 'gl4on'
         || $gl5 eq 'gl5on'
         || $gl6 eq 'gl6on'
         || $gl7 eq 'gl7on'
         || $gl8 eq 'gl8on'
         || $gl9 eq 'gl9on' )
    {
        print qq{ 
            <div id="content">
            };
    }
}

############################################################################
# printHomePage - Print home page.
############################################################################
sub printHomePage {

    my $url = $main_cgi;

    #    print "<h1>Expert Review Submission Home Page</h1>\n";

    #    print "<p>\n";
    my $announcement =
      db_getValue("select notes from announcement where type = 'Home Page'");

    #    print "<p>$announcement</p>\n";

    #    print hr;

    my $img_er_link = "<a href=\"" . getImgERUrl() . "\">IMG ER</a>";
    my $img_mi_link = "<a href=\"" . getImgMIUrl() . "\">IMG/M ER</a>";

    print qq{ 
            <div id="bluearrow"></div>
            };

=comment
 <p style="width:100%;">
 <span style="color:red;font-weight:bold;font-size:14px">
Thank you for making IMG submission so popular! <br>
Due to the high volume of submissions and a current system limitation, 
we have a backlog of 3000+ submissions. 
It is not necessary to submit additional tickets regarding these problems 
- we are aware of the issue and are working on it.
We will not be able to answer e-mails regarding the status of specific submissions at this time. 
</span>
 </p>
 <br>
 <br>
=cut

    print qq{ 
        <div id="intro"><img src="$base_url/images/jgi_top_corners.gif" width="496" height="10" alt="" /> 

            <table> 
            <tr> 
            <td> 
        The <strong>Microbial Genome & Metagenome Expert Review Data
        Submission</strong> site allows scientists to submit genome datasets
        to IMG ER or metagenome datasets to IMG/M ER in order to
        analyze and curate them in the context of a large set of reference
        public genomes and metagenomes.

        <p>$announcement</p>

            </td>
 
            <td> 
        <img src="$base_url/images/img_er_submit.jpg" width="119" height="220" class="genomes" alt="img_er_submit" title="IMG ER Submission" />
            </td> 
            </tr> 
 
            </table>
 
            <img class="bottom" src="$base_url/images/jgi_bottom_corners.gif" width="496" height="10" alt="" />
 
            </div>
        };

    print qq{
        <div id="stats">
            <img src="$base_url/images/jgi_home_table.gif" width="165" height="61" alt="" />

            <h2><strong>IMG Databases</strong></h2>

        <p></p>

        You can check your loaded genomes in IMG by clicking 
        the following links:

        <ul>
        <li> $img_er_link </li>
        <li> $img_mi_link </li>
        </ul>

            <p>Report all problems to:
            <br/> 
            <a href=\"/cgi-bin/m/main.cgi?section=Questions\">IMG Group</a></p>
 
            <br/> 

            <p>Please check <a href=\"/docs/submission/index.html\">FAQ</a>
            before <br/>asking any questions.</p>

            <br/>

            <p> 
            <a href=\"$url?section=logout&page=logout\">Logout</a>
            <br/>
            <a href='http://www.jgi.doe.gov/whoweare/accessibility.html' target='view_link'>Accessibility/Section 508</a>
            </div> 
        };

    #    print qq{
    #   <div id="content">
    #       $announcement
    #       </div>
    #   };

    #    print qq{
    #   <p>
    #       You can check your loaded genomes in IMG by clicking
    #       the following links:
    #       <ul>
    #       <li> $img_er_link </li>
    #       <li> $img_mi_link </li>
    #       </ul>
    #       </p>
    #   };

    #    print hr;

    #    print "<p>(Report all problems to: ";
    #    print "<a href=\"mailto:IMAChen\@lbl.gov\">Amy Chen</a>)</p>\n";

#    print "<h2><font color='red'>Warning: Submission Tool is unstable due to GOLD data loading.</font></h2>\n";

    #    my $contact_oid = getContactOid( );
    #    if ( $contact_oid ) {
    #   print "<p>\n";
    #   printHomeLink();
    #    }
}

############################################################################
# getPage - Wrapper for page parameter, in case POST instead of GET
#   method is used.  This is an adaptor function.
############################################################################
sub getPage {
    my $page = param1("page");
    return $page;
}

############################################################################
# getAction
############################################################################
sub getAction {
    my $action = param1("action");
    return $action;
}

############################################################################
# printLoginPage - Show site login prompt.
############################################################################
sub printLoginPage {
    print qq{ 
        <div id="content"> 
        };

    my $url = $main_cgi;

    print start_form(
                      -name   => 'loginName',
                      -method => 'post',
                      action  => "$url"
    );

    print
"<p><font color='red'>LBNL/JGI Holiday Shutdown: 12/24/2018 - 1/1/2019.</font>\n";

#    print "<p><font color='red'>Due to system configuration problem, we are not able to accept new submissions at this moment. Sorry for the inconvenience.</font>\n";

    Submission::printNewGoldMsg();

    Submission::printHint3(
"IMG no longer accepts unassembled 454 reads submissions starting 1/1/2016." );

    print "<h2>Welcome to IMG Submission Site. Please log in.</h2>\n";

    my $apId = param1('apId');
    if ($apId) {
        print hiddenVar( 'apId', $apId );
    }

##    print p("Submission site is temporarily closed due to database maintenance work.");
##    return;

    print "<p>Username:\n";
    print nbsp(2);
    print textfield( -name => 'username', -size => 20, -maxlength => 100 );
    print br;

    print br;

    print "<p>Password:\n";
    print nbsp(2);
    print password_field( -name => 'pwd', -size => 20, -maxlength => 100 );
    print br;

    print br;
    print
'<input type="submit" name="_section_login" value="Login" class="smbutton" />';
    print end_form();

    my $url = "main.cgi?oldLogin=false";

    #$url = alink($url, "NEW Single Sign On");
    print qq{
    

<br/>
<br/>
<span style="color: #B1B1B1; text-decoration: line-through;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
&nbsp;&nbsp; <b>OR</b> &nbsp;&nbsp;
<span style="color: #B1B1B1; text-decoration: line-through;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
<br/>
Sign in with:
<br/>
<input class="smbutton" type="button" value="NEW JGI Single Sign On" onclick="javascript:window.open('$url', '_self');">


    };

    print "<hr>\n";
    print
"<p><a href='http://www.jgi.doe.gov/whoweare/accessibility.html' target='view_link'>Accessibility/Section 508</a>\n";

    print hr;

    print "<h2>For New Users</h2>\n";
    my $img_url = getImgMIUrl();
    print qq{
        In order to submit a dataset to IMG ER or IMG/M ER, you need to:
        <ol>
            <li>Go to <a href='$img_url' target='view_link'>IMG/M ER</a>
            to fill out an IMG account request and get yourself
            familiar with IMG functionalities first. </li>
            <li>Prepare files for submission (for details see
           <a href='$user_guide_url' target='view_link'>
              User Guide</a>).</li>
            </ol>
        };

    return;

    print qq{
    <h5>For IMG ER:</h5>

        <ol>
        <li>Find or define in IMG-GOLD the genome project to be associated 
        with the submitted dataset (for details see section 4.1 of the
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Submit the genome dataset and provide data processing requirements, 
        including gene prediction, EC prediction, product name assignment 
        (for details see section 4.2 of the 
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Check the status of your submission.</li>
        </ol>
    };

    print qq{

    <h5>For IMG/M ER:</h5>

    <ol>
        <li>Find or define in IMG-GOLD the metagenome project and 
        sample to be associated with the submitted dataset 
        (for details see section 5.1of the 
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Submit the metagenome dataset and provide data processing 
        requirements, including gene prediction, EC prediction, 
        product name assignment (for details see section 5.2 of the
       <a href='$base_url/doc/Submission_User_Guide.pdf' target='view_link'>
          User Guide</a>);</li>
        <li>Check the status of your submission.</li>
        </ol>
    };

    #    print "<p>If you are not a registered user, ";
##    print '<input type="submit" name="requestAcct" value="Request Account" class="medbutton" />';

    #    print '<A href="' . $url . '?section=requestAcct&page=requestAcct">' .
    #        'request an account here.</A>';
    print "\n";

    print "<hr>\n";
    print
"<p><a href='http://www.jgi.doe.gov/whoweare/accessibility.html' target='view_link'>Accessibility/Section 508</a>\n";
}

############################################################################
# printErrorPage - Show error message
############################################################################
sub printErrorPage {
    my ($err_msg) = @_;

    print qq{ 
        <div id="content"> 
        };

    my $url = $main_cgi;

    print start_form( -name => 'errMsg', -method => 'post', action => "$url" );

    print "<h2>Error: $err_msg</h2>\n";

    print end_form();
}

############################################################################
# getRequestAcctAttr
############################################################################
sub getRequestAcctAttr {
    my @attrs = (
                "name\tYour Name (First Name and Last Name)\tchar\t80\tY",
                "title\tTitle\tchar\t80\tY",
                "department\tDepartment or Division\tchar\t255\tY",
                "organization\tOrganization or Affiliation\tchar\t255\tY",
                "email\tYour Email\tchar\t255\tY",
                "phone\tPhone Number\tchar\t80\tY",
                "address\tAddress\tchar\t255\tN",
                "city\tCity\tchar\t80\tY",
                "state\tState\tchar\t80\tN",
                "country\tCountry\tchar\t80\tY",
                "username\tPreferred Login Name (8-30 characters)\tchar\t30\tY",
                "group_name\tIMG Group (if known)\tchar\t80\tN",
                "comments\tReason(s) for Request\ttext\t60\tY"
    );

    return @attrs;
}

############################################################################
# printRequestAcctPage
############################################################################
sub printRequestAcctPage {
    print qq{ 
        <div id="content"> 
        };

    #generate a form
    my $url = $main_cgi;
    print start_form( -method => 'post',
                      -action => $url );

    print "<h1>Request Account</h1>\n";

#    print "<p>This function is temporarily unavailable due to database maintenance work.</p>\n";
#    return;

    print hiddenVar( 'submitRequest', 1 );

    print qq{ 
            An account is needed <b><u>only</u></b> for accessing the 
            <b>Integrated Microbial Genomes - Expert Review (IMG/ER)</b> 
            system, <b>Microbial Genome & Metagenome Expert Review 
        Data Submission</b> system,
        and the educational <b>IMG/EDU</b> system. 
            <b>No account is needed for accessing IMG.</b> 
        <br/><br/>

            <b>IMG/ER</b> provides support to individual scientists or 
            group of scientists for functional annotation and 
            curation of microbial genomes of interest, usually prior 
            to their release to Genbank, in the context of all 
            public genomes available in IMG. 
            Access to genomes undergoing curation in <b>IMG/ER</b> is 
            restricted to the scientists who curate them. 
        <br/><br/>

        The <b>Microbial Genome & Metagenome Expert Review Data
        Submission</b> site allows scientists to submit genome datasets
        to <b>IMG/ER</b> or metagenome datasets to 
        <b>IMG/M ER</b> in order to
        analyze and curate them in the context of a large set of reference
        public genomes and metagenomes.
        <br/><br/>

            <b>IMG/EDU</b> provides support for training and teaching 
            functional annotation of microbial genomes using specific 
            microbial genomes in the comparative context of all the 
            genomes available in IMG. 
            Existing or new genomes can be used for annotation with 
            <b>IMG/EDU</b>, with the annotations restricted to specific 
            training groups or classes or made publicly available. 
        <br/><br/>
            Please provide a <b><u>clear and detailed reason</u></b> 
            in English for requesting an account and using 
            <b>IMG/ER</b>, <b>ER Data Submission</b> or <b>IMG/EDU</b>.
        <br/><br/>
        };

    print "<p>Fields marked with an * are required.</p><br/><br/>\n";

    my @attrs = getRequestAcctAttr();

    print "<table class='img' border='1'>\n";

    for my $k (@attrs) {
        my ( $attr_name, $disp_name, $data_type, $len, $is_required ) =
          split( /\t/, $k );

        if ( !$disp_name ) {
            $disp_name = $attr_name;
        }

        my $size = 80;
        if ( $len && $size > $len ) {
            $size = $len;
        }

        my $attr_val = "";

        print "<tr class='img' >\n";
        print "  <th class='subhead' align='right'>";
        if ( $is_required eq 'Y' ) {
            print "*";
        }
        print escapeHTML($disp_name);
        print "</th>\n";

        if ( $attr_name eq 'country' ) {
            my @vals        = db_getValues("select cv_term from countrycv");
            my $default_val = 'USA';
            print "  <td class='img'   align='left'>\n";
            print "<select name='$attr_name' class='img' size='1'>\n";
            for my $val (@vals) {
                print "   <option value='" . escapeHTML($val) . "' ";
                if ( $val eq $default_val ) {
                    print " selected ";
                }
                print ">" . escapeHTML($val) . "</option>\n";
            }
            print "</select></td>\n";
        } elsif ( $data_type eq 'text' ) {
            print "  <td class='img'   align='left'>"
              . "<textarea name='$attr_name' cols='$size' rows='10'>"
              . "</textarea>"
              . "</td>\n";
        } else {
            print "  <td class='img'   align='left'>"
              . "<input type='text' name='$attr_name' value='"
              . escapeHTML($attr_val)
              . "' size='$size'"
              . " maxLength='$len'/>"
              . "</td>\n";
        }

        print "</tr>\n";
    }

    print "</table>\n";

    print "<p>\n";
    print submit(
                  -name  => "_section_requestAcct:submitRequest",
                  -value => "Submit",
                  -class => "smdefbutton"
    );
    print "</p>\n";

    print end_form();
}

############################################################################
# processAcctRequest
############################################################################
sub processAcctRequest {

    my $msg = "";

    my @attrs = getRequestAcctAttr();

    for my $k (@attrs) {
        my ( $attr_name, $disp_name, $data_type, $len, $is_required ) =
          split( /\t/, $k );

        if ( $is_required eq 'Y' ) {
            if ( !param1($attr_name) ) {
                $msg = "Please enter a value in '" . $disp_name . "'.";
                return $msg;
            }
        }

        if ( $attr_name eq 'name' ) {
            my $val  = param1($attr_name);
            my @vals = split( / /, $val );
            if ( scalar(@vals) < 2 ) {
                $msg = "Please enter both First Name and Last Name.";
                return $msg;
            }
        } elsif ( $attr_name eq 'username' ) {
            my $val = param1($attr_name);
            if ( !$val || length($val) < 8 || length($val) > 30 ) {
                $msg = "Preferred Login Name must be 8-30 characters.";
                return $msg;
            }
        }
    }

    my $dbh = Connect_IMG_Contact();

    if ( !$dbh ) {
        $msg = "Cannot login to Oracle database";
        return $msg;
    }

    my $sql = "select max(request_oid) from request_account";
    my $cur = $dbh->prepare($sql);
    $cur->execute();

    if ( !$cur ) {
        $dbh->disconnect();
        $msg = "Cannot access to REQUEST_ACCOUNT table";
        return $msg;
    }

    my ($max_id) = $cur->fetchrow();
    $cur->finish();
    if ( !$max_id ) {
        $max_id = 1;
    } else {
        $max_id++;
    }

    my $ins  = "insert into request_account (request_oid, request_date, status";
    my $vals = "values ($max_id, sysdate, 'new request'";

    my @attrs = getRequestAcctAttr();

    for my $k (@attrs) {
        my ( $attr_name, $disp_name, $data_type, $len, $is_required ) =
          split( /\t/, $k );

        if ( param($attr_name) ) {
            $ins .= ", $attr_name";
            my $v = param($attr_name);
            if ( $attr_name eq 'username' ) {
                $v =~ s/ /\./g;
            }
            $v =~ s/'/''/g;    # replace ' with ''
            $vals .= ", '" . $v . "'";
        }
    }

    $sql = $ins . ") " . $vals . ")";
    my $cur = $dbh->prepare($sql);
    $cur->execute();
    $dbh->disconnect();

    return $msg;
}
